package de.darwinspl.importer.ui.wizards.featureide;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.ICoreRunnable;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.IWorkbench;

import de.christophseidl.util.ecore.EcoreIOUtil;
import de.darwinspl.common.eclipse.ui.DwDialogUtil;
import de.darwinspl.constraint.DwConstraintModel;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.importer.DwImporterMetrics;
import de.darwinspl.importer.FeatureModelConstraintsTuple;
import de.darwinspl.importer.evolution.DwFeatureModelEvolutionImporter2;
import de.darwinspl.importer.evolution.check.DwModelSanityCheck;
import de.darwinspl.importer.evolution.check.ModelCheckException;
import de.darwinspl.importer.featureide.FeatureIDEFeatureModelAndConstraintsImporter;
import de.darwinspl.importer.ui.wizards.DwDateSelectionWizardPage;
import de.darwinspl.importer.ui.wizards.DwFeatureModelWizardImportedFilePage;
import de.darwinspl.importer.ui.wizards.DwFileSelectionWizardPage;

public class FeatureIDEFeatureModelImporterWizard extends Wizard implements IImportWizard {

	protected DwFileSelectionWizardPage fileSelectionWizardPage;
	protected DwDateSelectionWizardPage dateSelectionWizardPage;
	protected DwFeatureModelWizardImportedFilePage destinationFilePage;
	
	protected IStructuredSelection selection;
	protected IWorkbench workbench;
	
	public FeatureIDEFeatureModelImporterWizard() {
		super();
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.workbench = workbench;
		this.selection = selection;
		
		setWindowTitle("New Feature Model");

		fileSelectionWizardPage = new DwFileSelectionWizardPage(
				"FeatureIDE Feature Model Selection",
				"Please select one or more FeatureIDE feature models to import. They will be merged into one Temporal Feature Model (.tfm).",
				new String[] {"xml"},
				"FeatureIDE Feature Model",
				selection);
		
		dateSelectionWizardPage = new DwDateSelectionWizardPage(
				"FeatureIDE Date Selection",
				"Please select dates for your snapshot model(s).",
				fileSelectionWizardPage);
		
		destinationFilePage = new DwFeatureModelWizardImportedFilePage(selection, fileSelectionWizardPage);
	}
	
	@Override
	public boolean performFinish() {
		IFile featureModelFile = destinationFilePage.getFeatureModelFile();
		IFile constraintModelFile = destinationFilePage.getConstraintModelFile();
		
		Job job = Job.create("Importing FeatureIDE Models to DarwinSPL...", (ICoreRunnable) monitor -> {
			monitor.beginTask("Importing input models...", fileSelectionWizardPage.getSelectedFiles().size());
			
			Map<FeatureModelConstraintsTuple, Date> models = new HashMap<>();
			for(IResource selectedFile : fileSelectionWizardPage.getSelectedFiles()) {
				IFile fileIFile = (IFile) selectedFile;
				
				Date date = dateSelectionWizardPage.getDateForModel(fileIFile);
				
				FeatureIDEFeatureModelAndConstraintsImporter snapshotsImporter = new FeatureIDEFeatureModelAndConstraintsImporter(date);
				FeatureModelConstraintsTuple featureModelTupelSnapshot = snapshotsImporter.importFeatureModel(fileIFile);
				models.put(featureModelTupelSnapshot, date);
				monitor.worked(1);
				if(monitor.isCanceled())
					return;
			}

			monitor.beginTask("Resolving references...", models.size());
			Map<Date, FeatureModelConstraintsTuple> darwinSPLModels = new HashMap<Date, FeatureModelConstraintsTuple>();
			for (Entry<FeatureModelConstraintsTuple, Date> entry : models.entrySet()) {
				darwinSPLModels.put(entry.getValue(), entry.getKey());
				
				DwConstraintModel constraintModel = entry.getKey().getConstraintModel();
				if(constraintModel != null) {
					EcoreUtil.resolveAll(entry.getKey().getConstraintModel());				
				}
				
				monitor.worked(1);
				if(monitor.isCanceled())
					return;
			}
			
			DwTemporalFeatureModel evolutionFeatureModel = null;
			DwConstraintModel evolutionConstraintModel = null;

			DwFeatureModelEvolutionImporter2 evolutionImporter = new DwFeatureModelEvolutionImporter2();
			FeatureModelConstraintsTuple evolutionTuple;
			try {
				evolutionTuple = evolutionImporter.importFeatureModelEvolutionSnapshots(darwinSPLModels, monitor);
			} catch (Exception e) {
				e.printStackTrace();
				
				Display.getDefault().asyncExec(() -> {
					String message = e.getMessage();
					if(message == null || message.trim().equals(""))
						message = "Something went wrong!";
					
					DwDialogUtil.openErrorDialog("Error during merge process", "Merging of snapshot feature models failed.\n\n" + message);
				});
				return;
			}
			
			if(evolutionTuple == null)
				return;
			
			evolutionFeatureModel = evolutionTuple.getFeatureModel();
			evolutionConstraintModel = evolutionTuple.getConstraintModel();

			DwModelSanityCheck evolutionImporterCheck = new DwModelSanityCheck();
			try {
				evolutionImporterCheck.performCheck(evolutionFeatureModel, darwinSPLModels, monitor);
			} catch(ModelCheckException e) {
				e.printStackTrace();
				
				Display.getDefault().asyncExec(() -> {
					String message = e.getMessage();
					if(message == null || message.trim().equals(""))
						message = "Something went wrong!";
					
					DwDialogUtil.openErrorDialogWithCopyButton("Import process generated unequal TFM", "The imported TFM does not match the input evolution history.\n\n" + message);
				});
			}
			
			// Store persistently
			monitor.beginTask("Storing models...", 2);
			
			if(!EcoreIOUtil.saveModelAs(evolutionFeatureModel, featureModelFile))
				throw new RuntimeException("Saving the evolution-aware Feature Model failed.");
			monitor.worked(1);
			
			if(!EcoreIOUtil.saveModelAs(evolutionConstraintModel, constraintModelFile))
				throw new RuntimeException("Saving the evolution-aware Constraint Model failed.");
			monitor.worked(1);
			
			DwImporterMetrics metrics = new DwImporterMetrics();
			StringBuilder metricStringBuilder = new StringBuilder();
			
			List<Date> sortedDateList = new ArrayList<Date>(darwinSPLModels.keySet());
			sortedDateList.remove(null);
			Collections.sort(sortedDateList);
			
			for(int i=0;i<sortedDateList.size();i++) {
				Date date = sortedDateList.get(i);
				DwTemporalFeatureModel versionFeatureModel = darwinSPLModels.get(date).getFeatureModel();
				int versionFeatureCount = metrics.countFeatures(versionFeatureModel);
				int versionGroupCount = metrics.countGroups(versionFeatureModel);
				int versionConstraintCount = metrics.countConstraints(darwinSPLModels.get(date).getConstraintModel());
				
				metricStringBuilder.append("Metrics for model version ");
				metricStringBuilder.append(i);
				metricStringBuilder.append(" at date ");
				metricStringBuilder.append(date.toString());
				metricStringBuilder.append(":\n");
				metricStringBuilder.append("  Number of features: ");
				metricStringBuilder.append(versionFeatureCount);
				metricStringBuilder.append("\n");
				metricStringBuilder.append("  Number of groups: ");
				metricStringBuilder.append(versionGroupCount);
				metricStringBuilder.append("\n");
				metricStringBuilder.append("  Number of constraints: ");
				metricStringBuilder.append(versionConstraintCount);
				metricStringBuilder.append("\n\n");
			}
			
			metricStringBuilder.append("=================================================\n\n");
		
			int featureCount = metrics.countFeatures(evolutionFeatureModel);
			int groupCount = metrics.countGroups(evolutionFeatureModel);
			int constraintCount = metrics.countConstraints(evolutionConstraintModel);
			
			metricStringBuilder.append("Metrics for generated, evolution-aware model:\n");
			metricStringBuilder.append("  Number of features: ");
			metricStringBuilder.append(featureCount);
			metricStringBuilder.append("\n");
			metricStringBuilder.append("  Number of groups: ");
			metricStringBuilder.append(groupCount);
			metricStringBuilder.append("\n");
			metricStringBuilder.append("  Number of constraints: ");
			metricStringBuilder.append(constraintCount);

			Display.getDefault().asyncExec(() -> {
				DwDialogUtil.openInfoDialogWithCopyButton("Conversion finished", metricStringBuilder.toString());
			});
		});
		
		job.setPriority(10);
		job.schedule();
		return true;
	}
	
	@Override
    public String getWindowTitle() {
        return "DarwinSPL: Import FeatureIDE Feature Model";
    }

	@Override
	public void addPages() {
		addPage(fileSelectionWizardPage);
		addPage(dateSelectionWizardPage);
		addPage(destinationFilePage);
	}
	
}
