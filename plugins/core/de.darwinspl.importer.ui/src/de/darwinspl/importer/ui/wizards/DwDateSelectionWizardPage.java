package de.darwinspl.importer.ui.wizards;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

import de.darwinspl.common.eclipse.ui.DwDateDialog;
import de.darwinspl.temporal.util.DwDateResolverUtil;

public class DwDateSelectionWizardPage extends WizardPage {
	
	private static class DateInfoContainer {
		Button assignInitialStateDateButton;
		Button assignNewDateButton;
		Label fileDateLabel;
		
		Date assignedDate;
	}
	
	
	
	protected Shell shell;
	protected ScrolledComposite scrolledComposite;
	protected Composite container;
	
	private DwFileSelectionWizardPage fileSelectionWizardPage;
	
	private Map<IResource, DateInfoContainer> dateInformation;
	
	
	
	public DwDateSelectionWizardPage(String pageName, String description, DwFileSelectionWizardPage fileSelectionWizardPage) {
		super(pageName);
		setDescription(description);
		this.fileSelectionWizardPage = fileSelectionWizardPage;
		this.dateInformation = new HashMap<>();
	}
	


	@Override
	public boolean isPageComplete() {
		for(DateInfoContainer info : dateInformation.values())
			if(info.assignedDate == null)
				return false;
		
		return true;
	}
	
	@Override
	public void setVisible(boolean visible) {
		if(visible)
			doCreateControl();
		super.setVisible(visible);
	}

	@Override
	public void createControl(Composite parent) {
		this.shell = parent.getShell();
		scrolledComposite = new ScrolledComposite(parent, SWT.V_SCROLL | SWT.H_SCROLL);
		doCreateControl();
	}
	
	public void doCreateControl() {
		if(scrolledComposite == null)
			return;
		
//		scrolledComposite.setBackground(new Color(Display.getDefault(), 150, 200, 150));
		scrolledComposite.setLayout(new GridLayout(1, false));
		
		container = new Composite(scrolledComposite, SWT.NONE);
		container.setLayoutData(new GridData(SWT.CENTER, SWT.BEGINNING, true, false, 1, 1));
//		container.setBackground(new Color(Display.getDefault(), 200, 150, 150));
		GridLayout containerLayout = new GridLayout(4, false);
		containerLayout.verticalSpacing = 10;
		containerLayout.horizontalSpacing = 20;
        container.setLayout(containerLayout);
        
        for(IResource selectedFile : fileSelectionWizardPage.getSelectedFiles()) {
        	DateInfoContainer info = this.dateInformation.get(selectedFile);
        	boolean newEntry = false;
        	if(info == null) {
        		info = new DateInfoContainer();
        		this.dateInformation.put(selectedFile, info);
        		newEntry = true;
        	}
        	
        	Label fileNameLabel = new Label(container, SWT.LEFT);
        	fileNameLabel.setText(selectedFile.getName());
        	fileNameLabel.setLayoutData(new GridData(SWT.BEGINNING , SWT.CENTER, true, false, 1, 1));
        	
        	info.assignInitialStateDateButton = new Button(container, SWT.PUSH);
        	info.assignInitialStateDateButton.setText("Assign Initial Date");
        	info.assignInitialStateDateButton.setToolTipText("Sets the date for this snapshot as the negative infinite date used to mark the start of an evolution plan. The initial state can be assigned to no more than one snapshot.");
        	info.assignInitialStateDateButton.setLayoutData(new GridData(SWT.END , SWT.CENTER, false, false, 1, 1));
        	info.assignInitialStateDateButton.addListener(SWT.Selection, new Listener() {
				public void handleEvent(Event event) {
					if(event.type == SWT.Selection) {
						Date newDate = DwDateResolverUtil.INITIAL_STATE_DATE;
						for(DateInfoContainer info : dateInformation.values()) {
							if(event.widget.equals(info.assignInitialStateDateButton)) {
								updateInformationDate(info, newDate);
								break;
							}
						}
					}
				}
			});
        	
        	info.assignNewDateButton = new Button(container, SWT.PUSH);
        	info.assignNewDateButton.setText("Assign New Date");
        	info.assignNewDateButton.setLayoutData(new GridData(SWT.END , SWT.CENTER, false, false, 1, 1));
        	info.assignNewDateButton.addListener(SWT.Selection, new Listener() {
				public void handleEvent(Event event) {
					if(event.type == SWT.Selection) {
						DwDateDialog dateDialog = new DwDateDialog(shell, null);
						dateDialog.open();
						Date newDate = dateDialog.getValue();
						
						if(newDate != null) {
							for(DateInfoContainer info : dateInformation.values()) {
								if(event.widget.equals(info.assignNewDateButton)) {
									updateInformationDate(info, newDate);
									break;
								}
							}
						}
					}
				}
			});
        	
        	info.fileDateLabel = new Label(container, SWT.LEFT);
        	info.fileDateLabel.setSize(200, 50);
        	info.fileDateLabel.setLayoutData(new GridData(SWT.BEGINNING , SWT.CENTER, false, false, 1, 1));
        	
        	if(newEntry)
        		updateInformationDate(info, getDefaultDateForFile(selectedFile));
        	else
        		updateInformationDate(info, info.assignedDate);
        	
        	Label separatorLabel = new Label(container, SWT.SEPARATOR | SWT.HORIZONTAL);
        	separatorLabel.setLayoutData(new GridData(SWT.FILL , SWT.CENTER, true, false, 4, 1));
        }
        
        container.setSize(container.computeSize(600, SWT.DEFAULT));
        scrolledComposite.setContent(container);
        
		setControl(scrolledComposite);
		setPageComplete(isPageComplete());
	}
	
	public Date getDateForModel(IFile fileIFile) {
		DateInfoContainer info = this.dateInformation.get(fileIFile);
		if(info != null)
			return info.assignedDate;
		throw new RuntimeException("Could not retrieve a date for file \"" + fileIFile.getName() + "\".");
	}
	
	
	
	private void updateInformationDate(DateInfoContainer info, Date newDate) {
		if(newDate == null) {
			info.fileDateLabel.setText("<please specify a date>");
		}
		else if(newDate.equals(DwDateResolverUtil.INITIAL_STATE_DATE)) {
			for(IResource resource : dateInformation.keySet()) {
				DateInfoContainer dateInfo = dateInformation.get(resource);
				if(dateInfo.assignedDate.equals(DwDateResolverUtil.INITIAL_STATE_DATE)) {
					updateInformationDate(dateInfo, getDefaultDateForFile(resource));
				}
			}
			
			info.assignedDate = newDate;
			info.fileDateLabel.setText(formatDate(newDate));
		}
		else {
			info.assignedDate = newDate;
			info.fileDateLabel.setText(formatDate(newDate));
		}
		
		setPageComplete(isPageComplete());
	}
	
	private String formatDate(Date date) {
		return DwDateResolverUtil.deresolveDate(date).replace("T", " - ");
	}
	
	private Date getDefaultDateForFile(IResource selectedFile) {
		String fileName = selectedFile.getName();
		String fileNameWithoutExtension = fileName.substring(0, fileName.lastIndexOf("."));
		
		Date resolvedDate = DwDateResolverUtil.resolveDate(fileNameWithoutExtension);
		
		return resolvedDate == null ? null : resolvedDate;
	}
	
}
