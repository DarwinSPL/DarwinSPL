package de.darwinspl.importer.ui.wizards;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.model.WorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

public class DwFileSelectionWizardPage extends WizardPage {
	
	protected Composite container;
	private CheckboxTreeViewer treeViewer;
	
	protected IStructuredSelection initialSelection;
	private List<IResource> selectedSourceFiles = new ArrayList<>();

	protected String[] fileExtensionFilter;
	
	public DwFileSelectionWizardPage(String pageName, String description, String[] fileExtensionFilter, String modelName, IStructuredSelection selection) {
		super(pageName);
		setDescription(description);
		this.fileExtensionFilter = fileExtensionFilter;
		
		this.initialSelection = selection;
	}

	@Override
	public void createControl(Composite parent) {
		container = new Composite(parent, SWT.NONE);
		FillLayout layout = new FillLayout();
		layout.marginHeight = 1;
		layout.marginWidth = 1;
        container.setLayout(layout);
        container.setBackground(new Color(Display.getDefault(), 150, 150, 150));
		
        treeViewer = new CheckboxTreeViewer(container, SWT.FILL);
        treeViewer.setContentProvider(new WorkbenchContentProvider());
        treeViewer.setLabelProvider(new WorkbenchLabelProvider());
        treeViewer.setInput(ResourcesPlugin.getWorkspace().getRoot());
        
        treeViewer.addCheckStateListener(new ICheckStateListener() {
			@Override
			public void checkStateChanged(CheckStateChangedEvent event) {
				evaluateCheckedElement((IResource) event.getElement(), event.getChecked());
			}
		});

		Iterator<?> selectionIterator = this.initialSelection.iterator();
        Object[] initiallyExpandedElements = this.getExpandedPath(selectionIterator);
        if(initiallyExpandedElements != null) {
        	treeViewer.setExpandedElements(initiallyExpandedElements);
        	treeViewer.setSelection(null);
        }

		selectionIterator = this.initialSelection.iterator();
        while(selectionIterator.hasNext()) {
        	Object elementToBeChecked = selectionIterator.next();
        	treeViewer.setChecked(elementToBeChecked, true);
			evaluateCheckedElement((IResource) elementToBeChecked, true);
        }
		
		setControl(container);
	}
	
	private void evaluateCheckedElement(IResource checkedResource, boolean isChecked) {
		if(isChecked) {
			boolean fileExtensionFound = false;
			for(String fileExtension : fileExtensionFilter) {
				if(fileExtension.equals(checkedResource.getFileExtension())) {
					fileExtensionFound = true;
					break;
				}
			}
			if(fileExtensionFound)
				selectedSourceFiles.add(checkedResource);
			else
				treeViewer.setChecked(checkedResource, false);
		}
		else {
			selectedSourceFiles.remove(checkedResource);
		}
		
		setPageComplete(isPageComplete());
	}

	private Object[] getExpandedPath(Iterator<?> selectionIterator) {
		Map<String, Object> expandedElements = new HashMap<>();
		
		while(selectionIterator.hasNext()) {
			Object selection = selectionIterator.next();
			
			IProject project = ((IResource) selection).getProject();
			String[] pathSegments = ((IResource) selection).getProjectRelativePath().segments();
			
			IContainer currentContainer = project;
			expandedElements.put(project.getFullPath().toString(), project);
			for(int i=1; i<=pathSegments.length-1; i++) {
				IResource newResource = currentContainer.findMember(pathSegments[i-1]);
				expandedElements.put(newResource.getFullPath().toString(), newResource);
				if(newResource instanceof IContainer) {
					currentContainer = (IContainer) newResource;
				}
				else {
					break;
				}
			}
		}
        
        return expandedElements.values().toArray();
	}

	public List<IResource> getSelectedFiles() {
		return selectedSourceFiles;
	}
	
	@Override
	public boolean isPageComplete() {
		return this.selectedSourceFiles != null && this.selectedSourceFiles.size() > 0;
	}
	
}
