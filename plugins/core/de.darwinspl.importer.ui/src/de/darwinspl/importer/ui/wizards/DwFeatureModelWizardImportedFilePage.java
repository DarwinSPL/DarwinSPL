package de.darwinspl.importer.ui.wizards;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.dialogs.WizardNewFileCreationPage;

import de.christophseidl.util.eclipse.ResourceUtil;
import de.darwinspl.feature.util.custom.DwFeatureUtil;

public class DwFeatureModelWizardImportedFilePage extends WizardNewFileCreationPage {
	
	DwFileSelectionWizardPage fileSelectionWizardPage;

	public DwFeatureModelWizardImportedFilePage(IStructuredSelection selection, DwFileSelectionWizardPage fileSelectionWizardPage) {
		super("New DarwinSPL Feature Model Wizard", selection);
		
//		setTitle("New DarwinSPL Feature Model Wizard");
		setDescription("Select where to store the imported Temporal Feature Model");
		setFileName("EvolutionModel" + "." + DwFeatureUtil.getFeatureModelFileExtensionForXmi());
		
		this.fileSelectionWizardPage = fileSelectionWizardPage;
	}

	@Override
	protected boolean validatePage() {
		return true;
	}
	
	private String getFilePathAsString() {
		IPath containerFullPath = getContainerFullPath();
		String filename = getFileName();
		IPath filePath = containerFullPath.append(filename);
		return filePath.toString();
	}

	public IFile getFeatureModelFile() {
		String filePathString = getFilePathAsString();
		IFile file = ResourceUtil.getLocalFile(filePathString);
		String fileExtension = file.getFileExtension();
		
		//Ensure that the file has the right extension
		if (fileExtension.equalsIgnoreCase(DwFeatureUtil.getFeatureModelFileExtensionForXmi())) {
			return file;
		}
		
		return ResourceUtil.getLocalFile(filePathString + "." + DwFeatureUtil.getFeatureModelFileExtensionForXmi());
	}

	public IFile getConstraintModelFile() {
		String filePathString = getFilePathAsString();
		IFile file = ResourceUtil.getLocalFile(filePathString);
		String fileExtension = file.getFileExtension();
		
		//Ensure that the file has the right extension
		if (fileExtension.equalsIgnoreCase(DwFeatureUtil.getFeatureModelFileExtensionForXmi())) {
			return ResourceUtil.getLocalFile(filePathString + "_ctc");
		}
		
		return ResourceUtil.getLocalFile(filePathString + "." + DwFeatureUtil.getConstraintModelFileExtensionForXmi());
	}
	
}
