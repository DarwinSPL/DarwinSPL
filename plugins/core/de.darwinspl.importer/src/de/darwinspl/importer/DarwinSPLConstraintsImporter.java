package de.darwinspl.importer;

import java.util.List;
import java.util.Map;

import de.darwinspl.constraint.DwConstraintModel;
import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwTemporalFeatureModel;

/**
 * 
 * @author Michael Nieke
 *
 * @param <T> Type of source Feature Model
 * @param <R> Type of features of source feature model
 * @param <S> Type of constraints of source model
 */
public interface DarwinSPLConstraintsImporter <T,R,S> {

	public DwConstraintModel importConstraints(DwTemporalFeatureModel featureModel, Map<R, DwFeature> featureMapping, T originalFeatureModel, List<S> constraints);
	
}
