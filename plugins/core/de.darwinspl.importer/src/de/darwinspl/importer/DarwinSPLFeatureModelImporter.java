package de.darwinspl.importer;

import de.darwinspl.feature.DwTemporalFeatureModel;
import org.eclipse.core.resources.IFile;

public interface DarwinSPLFeatureModelImporter<T> {

	public DwTemporalFeatureModel importFeatureModel(T featureModel);
	public DwTemporalFeatureModel importFeatureModel(String pathToFile);
	public DwTemporalFeatureModel importFeatureModel(IFile file);
	
}
