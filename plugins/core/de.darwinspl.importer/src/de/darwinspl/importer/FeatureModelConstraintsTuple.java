package de.darwinspl.importer;

import de.darwinspl.constraint.DwConstraintModel;
import de.darwinspl.feature.DwTemporalFeatureModel;

public class FeatureModelConstraintsTuple extends Tuple<DwTemporalFeatureModel, DwConstraintModel> {

	protected DwTemporalFeatureModel featureModel;
	protected DwConstraintModel constraintModel;
	
	public FeatureModelConstraintsTuple(DwTemporalFeatureModel featureModel, DwConstraintModel constraintModel) {
		super(featureModel, constraintModel);
		setFeatureModel(featureModel);
		setConstraintModel(constraintModel);
	}

	public DwTemporalFeatureModel getFeatureModel() {
		return x;
	}

	public void setFeatureModel(DwTemporalFeatureModel featureModel) {
		this.featureModel = featureModel;
	}

	public DwConstraintModel getConstraintModel() {
		return y;
	}

	public void setConstraintModel(DwConstraintModel constraintModel) {
		this.constraintModel = constraintModel;
	}
	
	
}
