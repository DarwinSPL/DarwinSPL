package de.darwinspl.importer;

import org.eclipse.emf.ecore.EObject;

import de.darwinspl.constraint.DwConstraint;
import de.darwinspl.constraint.DwConstraintModel;
import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwTemporalFeatureModel;

public class DwImporterMetrics {

	public int countFeatures(DwTemporalFeatureModel evolutionFeatureModel) {
		return countElements(evolutionFeatureModel, 0, DwFeature.class);
	}

	public int countGroups(DwTemporalFeatureModel evolutionFeatureModel) {
		return countElements(evolutionFeatureModel, 0, DwGroup.class);
	}

	public int countConstraints(DwConstraintModel evolutionConstraintModel) {
		return countElements(evolutionConstraintModel, 0, DwConstraint.class);
	}
	
	
	
	private int countElements(EObject element, int count, Class<?> searchedType) {
		if(searchedType.isInstance(element))
			count ++;
		
		for(EObject child : element.eContents())
			count = countElements(child, count, searchedType);
		
		return count;
	}

}
