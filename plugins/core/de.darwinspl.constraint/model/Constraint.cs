SYNTAXDEF tfm_ctc
FOR <http://www.darwinspl.de/constraint/2.0>
START DwConstraintModel

IMPORTS {
	expression : <http://www.darwinspl.de/expression/2.0> WITH SYNTAX expression <../../de.darwinspl.expression/model/Expression.cs>
	
}

OPTIONS {
	reloadGeneratorModel = "false";
	usePredefinedTokens = "false";
	
	editorName = "Constraint Editor (DarwinSPL)";
	newFileWizardCategory = "de.darwinspl.newwizards.Category";
	newFileWizardName = "DarwinSPL Feature Model Constraints (*.tfm_ctc)";
	
	disableNewProjectWizard = "true";
	disableBuilder = "true";
	disableLaunchSupport = "true";
	disableDebugSupport = "true";
	
	overrideUIManifest = "false";
	overrideUIPluginXML = "false";
	
	overrideNewFileContentProvider = "false";
	
	useClassicPrinter = "true";
}

TOKENS {
//	DEFINE DATE $($ + INTEGER_LITERAL + $'/'$ + INTEGER_LITERAL + $'/'$ + INTEGER_LITERAL + $( 'T'$ + INTEGER_LITERAL + $':'$ + INTEGER_LITERAL + $(':' $ + INTEGER_LITERAL + $)?$ + $)?)$;
//	DEFINE INTEGER_LITERAL $('0'..'9')+ $;
	
	DEFINE PATH_TO_FILE $'<' ('A'..'Z'|'a'..'z'|'0'..'9'|'_'|' '|'/')+ '.tfm>'$;
}

TOKENSTYLES {
	"PATH_TO_FILE" COLOR #325985, ITALIC;
}



RULES {
	DwConstraintModel ::=
		"feature" "model" featureModel[PATH_TO_FILE] !0!0
		(constraints !0)*
	;

	DwConstraint ::= rootExpression (validity)?;
	
	@SuppressWarnings(explicitSyntaxChoice)
	DwTemporalInterval ::= "[" (
									from[DATE] "-" to[DATE] |
									from[DATE] "-" "eternity" |
									"initial state" "-" to[DATE] |
									"initial state" "-" "eternity"
								)
							"]";
}
