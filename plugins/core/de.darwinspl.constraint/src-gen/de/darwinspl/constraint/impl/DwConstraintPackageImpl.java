/**
 */
package de.darwinspl.constraint.impl;

import de.darwinspl.constraint.DwConstraint;
import de.darwinspl.constraint.DwConstraintFactory;
import de.darwinspl.constraint.DwConstraintModel;
import de.darwinspl.constraint.DwConstraintPackage;

import de.darwinspl.datatypes.DwDatatypesPackage;

import de.darwinspl.expression.DwExpressionPackage;

import de.darwinspl.feature.DwFeaturePackage;

import de.darwinspl.temporal.DwTemporalModelPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DwConstraintPackageImpl extends EPackageImpl implements DwConstraintPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwConstraintModelEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.darwinspl.constraint.DwConstraintPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private DwConstraintPackageImpl() {
		super(eNS_URI, DwConstraintFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link DwConstraintPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static DwConstraintPackage init() {
		if (isInited) return (DwConstraintPackage)EPackage.Registry.INSTANCE.getEPackage(DwConstraintPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredDwConstraintPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		DwConstraintPackageImpl theDwConstraintPackage = registeredDwConstraintPackage instanceof DwConstraintPackageImpl ? (DwConstraintPackageImpl)registeredDwConstraintPackage : new DwConstraintPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		DwDatatypesPackage.eINSTANCE.eClass();
		DwExpressionPackage.eINSTANCE.eClass();
		DwFeaturePackage.eINSTANCE.eClass();
		DwTemporalModelPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theDwConstraintPackage.createPackageContents();

		// Initialize created meta-data
		theDwConstraintPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theDwConstraintPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(DwConstraintPackage.eNS_URI, theDwConstraintPackage);
		return theDwConstraintPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwConstraint() {
		return dwConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwConstraint_RootExpression() {
		return (EReference)dwConstraintEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwConstraintModel() {
		return dwConstraintModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwConstraintModel_FeatureModel() {
		return (EReference)dwConstraintModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwConstraintModel_Constraints() {
		return (EReference)dwConstraintModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwConstraintFactory getDwConstraintFactory() {
		return (DwConstraintFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		dwConstraintModelEClass = createEClass(DW_CONSTRAINT_MODEL);
		createEReference(dwConstraintModelEClass, DW_CONSTRAINT_MODEL__FEATURE_MODEL);
		createEReference(dwConstraintModelEClass, DW_CONSTRAINT_MODEL__CONSTRAINTS);

		dwConstraintEClass = createEClass(DW_CONSTRAINT);
		createEReference(dwConstraintEClass, DW_CONSTRAINT__ROOT_EXPRESSION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		DwFeaturePackage theDwFeaturePackage = (DwFeaturePackage)EPackage.Registry.INSTANCE.getEPackage(DwFeaturePackage.eNS_URI);
		DwTemporalModelPackage theDwTemporalModelPackage = (DwTemporalModelPackage)EPackage.Registry.INSTANCE.getEPackage(DwTemporalModelPackage.eNS_URI);
		DwExpressionPackage theDwExpressionPackage = (DwExpressionPackage)EPackage.Registry.INSTANCE.getEPackage(DwExpressionPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		dwConstraintEClass.getESuperTypes().add(theDwTemporalModelPackage.getDwSingleTemporalIntervalElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(dwConstraintModelEClass, DwConstraintModel.class, "DwConstraintModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwConstraintModel_FeatureModel(), theDwFeaturePackage.getDwTemporalFeatureModel(), null, "featureModel", null, 1, 1, DwConstraintModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwConstraintModel_Constraints(), this.getDwConstraint(), null, "constraints", null, 0, -1, DwConstraintModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwConstraintEClass, DwConstraint.class, "DwConstraint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwConstraint_RootExpression(), theDwExpressionPackage.getDwExpression(), null, "rootExpression", null, 1, 1, DwConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //DwConstraintPackageImpl
