/**
 */
package de.darwinspl.constraint.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import de.darwinspl.constraint.DwConstraint;
import de.darwinspl.constraint.DwConstraintFactory;
import de.darwinspl.constraint.DwConstraintModel;
import de.darwinspl.constraint.DwConstraintPackage;
import de.darwinspl.constraint.util.custom.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DwConstraintFactoryImpl extends EFactoryImpl implements DwConstraintFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DwConstraintFactory init() {
		try {
			DwConstraintFactory theDwConstraintFactory = (DwConstraintFactory)EPackage.Registry.INSTANCE.getEFactory(DwConstraintPackage.eNS_URI);
			if (theDwConstraintFactory != null) {
				return theDwConstraintFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DwConstraintFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwConstraintFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case DwConstraintPackage.DW_CONSTRAINT_MODEL: return createDwConstraintModel();
			case DwConstraintPackage.DW_CONSTRAINT: return createDwConstraint();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwConstraint createDwConstraint() {
		DwConstraintImpl dwConstraint = new DwConstraintImpl();
		return dwConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwConstraintModel createDwConstraintModel() {
		DwConstraintModelImpl dwConstraintModel = new DwConstraintModelImpl();
		return dwConstraintModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwConstraintPackage getDwConstraintPackage() {
		return (DwConstraintPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DwConstraintPackage getPackage() {
		return DwConstraintPackage.eINSTANCE;
	}

} //DwConstraintFactoryImpl
