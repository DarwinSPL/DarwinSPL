/**
 */
package de.darwinspl.constraint.impl;

import de.darwinspl.constraint.DwConstraint;
import de.darwinspl.constraint.DwConstraintPackage;

import de.darwinspl.expression.DwExpression;

import de.darwinspl.temporal.impl.DwSingleTemporalIntervalElementImpl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.constraint.impl.DwConstraintImpl#getRootExpression <em>Root Expression</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwConstraintImpl extends DwSingleTemporalIntervalElementImpl implements DwConstraint {
	/**
	 * The cached value of the '{@link #getRootExpression() <em>Root Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootExpression()
	 * @generated
	 * @ordered
	 */
	protected DwExpression rootExpression;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwConstraintPackage.Literals.DW_CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwExpression getRootExpression() {
		return rootExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRootExpression(DwExpression newRootExpression, NotificationChain msgs) {
		DwExpression oldRootExpression = rootExpression;
		rootExpression = newRootExpression;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DwConstraintPackage.DW_CONSTRAINT__ROOT_EXPRESSION, oldRootExpression, newRootExpression);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRootExpression(DwExpression newRootExpression) {
		if (newRootExpression != rootExpression) {
			NotificationChain msgs = null;
			if (rootExpression != null)
				msgs = ((InternalEObject)rootExpression).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DwConstraintPackage.DW_CONSTRAINT__ROOT_EXPRESSION, null, msgs);
			if (newRootExpression != null)
				msgs = ((InternalEObject)newRootExpression).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DwConstraintPackage.DW_CONSTRAINT__ROOT_EXPRESSION, null, msgs);
			msgs = basicSetRootExpression(newRootExpression, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwConstraintPackage.DW_CONSTRAINT__ROOT_EXPRESSION, newRootExpression, newRootExpression));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwConstraintPackage.DW_CONSTRAINT__ROOT_EXPRESSION:
				return basicSetRootExpression(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwConstraintPackage.DW_CONSTRAINT__ROOT_EXPRESSION:
				return getRootExpression();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwConstraintPackage.DW_CONSTRAINT__ROOT_EXPRESSION:
				setRootExpression((DwExpression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwConstraintPackage.DW_CONSTRAINT__ROOT_EXPRESSION:
				setRootExpression((DwExpression)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwConstraintPackage.DW_CONSTRAINT__ROOT_EXPRESSION:
				return rootExpression != null;
		}
		return super.eIsSet(featureID);
	}

} //DwConstraintImpl
