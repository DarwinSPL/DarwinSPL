/**
 */
package de.darwinspl.constraint;

import de.darwinspl.expression.DwExpression;

import de.darwinspl.temporal.DwSingleTemporalIntervalElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.constraint.DwConstraint#getRootExpression <em>Root Expression</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.constraint.DwConstraintPackage#getDwConstraint()
 * @model
 * @generated
 */
public interface DwConstraint extends DwSingleTemporalIntervalElement {
	/**
	 * Returns the value of the '<em><b>Root Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Expression</em>' containment reference.
	 * @see #setRootExpression(DwExpression)
	 * @see de.darwinspl.constraint.DwConstraintPackage#getDwConstraint_RootExpression()
	 * @model containment="true" required="true"
	 * @generated
	 */
	DwExpression getRootExpression();

	/**
	 * Sets the value of the '{@link de.darwinspl.constraint.DwConstraint#getRootExpression <em>Root Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root Expression</em>' containment reference.
	 * @see #getRootExpression()
	 * @generated
	 */
	void setRootExpression(DwExpression value);

} // DwConstraint
