/**
 */
package de.darwinspl.constraint;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.darwinspl.constraint.DwConstraintPackage
 * @generated
 */
public interface DwConstraintFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DwConstraintFactory eINSTANCE = de.darwinspl.constraint.impl.DwConstraintFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Dw Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Constraint</em>'.
	 * @generated
	 */
	DwConstraint createDwConstraint();

	/**
	 * Returns a new object of class '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Model</em>'.
	 * @generated
	 */
	DwConstraintModel createDwConstraintModel();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	DwConstraintPackage getDwConstraintPackage();

} //DwConstraintFactory
