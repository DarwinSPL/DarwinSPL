/**
 */
package de.darwinspl.constraint;

import de.darwinspl.temporal.DwTemporalModelPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.darwinspl.constraint.DwConstraintFactory
 * @model kind="package"
 * @generated
 */
public interface DwConstraintPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "constraint";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.darwinspl.de/constraint/2.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "constraint";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DwConstraintPackage eINSTANCE = de.darwinspl.constraint.impl.DwConstraintPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.darwinspl.constraint.impl.DwConstraintImpl <em>Dw Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.constraint.impl.DwConstraintImpl
	 * @see de.darwinspl.constraint.impl.DwConstraintPackageImpl#getDwConstraint()
	 * @generated
	 */
	int DW_CONSTRAINT = 1;

	/**
	 * The meta object id for the '{@link de.darwinspl.constraint.impl.DwConstraintModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.constraint.impl.DwConstraintModelImpl
	 * @see de.darwinspl.constraint.impl.DwConstraintPackageImpl#getDwConstraintModel()
	 * @generated
	 */
	int DW_CONSTRAINT_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONSTRAINT_MODEL__FEATURE_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONSTRAINT_MODEL__CONSTRAINTS = 1;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONSTRAINT_MODEL_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONSTRAINT_MODEL_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONSTRAINT__ID = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Validity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONSTRAINT__VALIDITY = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__VALIDITY;

	/**
	 * The feature id for the '<em><b>Root Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONSTRAINT__ROOT_EXPRESSION = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Dw Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONSTRAINT_FEATURE_COUNT = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONSTRAINT___CREATE_ID = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONSTRAINT___IS_VALID__DATE = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___IS_VALID__DATE;

	/**
	 * The operation id for the '<em>Get From</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONSTRAINT___GET_FROM = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_FROM;

	/**
	 * The operation id for the '<em>Get To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONSTRAINT___GET_TO = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_TO;

	/**
	 * The number of operations of the '<em>Dw Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONSTRAINT_OPERATION_COUNT = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link de.darwinspl.constraint.DwConstraint <em>Dw Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Constraint</em>'.
	 * @see de.darwinspl.constraint.DwConstraint
	 * @generated
	 */
	EClass getDwConstraint();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.constraint.DwConstraint#getRootExpression <em>Root Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Root Expression</em>'.
	 * @see de.darwinspl.constraint.DwConstraint#getRootExpression()
	 * @see #getDwConstraint()
	 * @generated
	 */
	EReference getDwConstraint_RootExpression();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.constraint.DwConstraintModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see de.darwinspl.constraint.DwConstraintModel
	 * @generated
	 */
	EClass getDwConstraintModel();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.constraint.DwConstraintModel#getFeatureModel <em>Feature Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Feature Model</em>'.
	 * @see de.darwinspl.constraint.DwConstraintModel#getFeatureModel()
	 * @see #getDwConstraintModel()
	 * @generated
	 */
	EReference getDwConstraintModel_FeatureModel();

	/**
	 * Returns the meta object for the containment reference list '{@link de.darwinspl.constraint.DwConstraintModel#getConstraints <em>Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constraints</em>'.
	 * @see de.darwinspl.constraint.DwConstraintModel#getConstraints()
	 * @see #getDwConstraintModel()
	 * @generated
	 */
	EReference getDwConstraintModel_Constraints();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DwConstraintFactory getDwConstraintFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.darwinspl.constraint.impl.DwConstraintImpl <em>Dw Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.constraint.impl.DwConstraintImpl
		 * @see de.darwinspl.constraint.impl.DwConstraintPackageImpl#getDwConstraint()
		 * @generated
		 */
		EClass DW_CONSTRAINT = eINSTANCE.getDwConstraint();

		/**
		 * The meta object literal for the '<em><b>Root Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_CONSTRAINT__ROOT_EXPRESSION = eINSTANCE.getDwConstraint_RootExpression();

		/**
		 * The meta object literal for the '{@link de.darwinspl.constraint.impl.DwConstraintModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.constraint.impl.DwConstraintModelImpl
		 * @see de.darwinspl.constraint.impl.DwConstraintPackageImpl#getDwConstraintModel()
		 * @generated
		 */
		EClass DW_CONSTRAINT_MODEL = eINSTANCE.getDwConstraintModel();

		/**
		 * The meta object literal for the '<em><b>Feature Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_CONSTRAINT_MODEL__FEATURE_MODEL = eINSTANCE.getDwConstraintModel_FeatureModel();

		/**
		 * The meta object literal for the '<em><b>Constraints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_CONSTRAINT_MODEL__CONSTRAINTS = eINSTANCE.getDwConstraintModel_Constraints();

	}

} //DwConstraintPackage
