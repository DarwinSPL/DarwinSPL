/**
 */
package de.darwinspl.constraint;

import de.darwinspl.feature.DwTemporalFeatureModel;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.constraint.DwConstraintModel#getFeatureModel <em>Feature Model</em>}</li>
 *   <li>{@link de.darwinspl.constraint.DwConstraintModel#getConstraints <em>Constraints</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.constraint.DwConstraintPackage#getDwConstraintModel()
 * @model
 * @generated
 */
public interface DwConstraintModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Model</em>' reference.
	 * @see #setFeatureModel(DwTemporalFeatureModel)
	 * @see de.darwinspl.constraint.DwConstraintPackage#getDwConstraintModel_FeatureModel()
	 * @model required="true"
	 * @generated
	 */
	DwTemporalFeatureModel getFeatureModel();

	/**
	 * Sets the value of the '{@link de.darwinspl.constraint.DwConstraintModel#getFeatureModel <em>Feature Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature Model</em>' reference.
	 * @see #getFeatureModel()
	 * @generated
	 */
	void setFeatureModel(DwTemporalFeatureModel value);

	/**
	 * Returns the value of the '<em><b>Constraints</b></em>' containment reference list.
	 * The list contents are of type {@link de.darwinspl.constraint.DwConstraint}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraints</em>' containment reference list.
	 * @see de.darwinspl.constraint.DwConstraintPackage#getDwConstraintModel_Constraints()
	 * @model containment="true"
	 * @generated
	 */
	EList<DwConstraint> getConstraints();

} // DwConstraintModel
