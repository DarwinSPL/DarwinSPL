package de.darwinspl.constraint.util.custom;

public class DwConstraintUtil {

	private static String FILE_EXTENSION_CONCRETE_SYNTAX = "tfm_ctc";
	private static String FILE_EXTENSION_ABSTRACT_SYNTAX = "dwconstraint_xmi";
	
	public static String getFileExtensionForConcreteSyntax() {
		return FILE_EXTENSION_CONCRETE_SYNTAX;
	}
	public static String getFileExtensionForAbstractSyntax() {
		return FILE_EXTENSION_ABSTRACT_SYNTAX;
	}
	
	
}
