package de.darwinspl.variant;

public class DwUnsupportedFileExtensionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1213238533233992596L;

	public DwUnsupportedFileExtensionException(String message) {
		super(message);
	}

	

}
