package de.darwinspl.variant;

import java.util.Date;
import java.util.List;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;

import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.configuration.DwConfiguration;
import de.darwinspl.mapping.DwMappingModel;

public interface DwVariantDeriver {

	/**
	 * 
	 * @return Name that should be shown if users are to choose from multiple
	 *         derivers.
	 */
	public String getName();

	/**
	 * 
	 * @return without ".", e.g., "decore" and not ".decore"
	 */
	public List<String> supportsFileExtensions();

	/**
	 * Does not check for compatibility of file extensions. This is done by
	 * "DwVariantDerivation". Variant derivation should always be done via the
	 * DwVariantDerivation.
	 * 
	 * @param featureModel
	 * @param mappingModel
	 * @param configuration
	 * @param artifacts
	 * @param date
	 * @return success
	 */
	public void deriveVariant(DwTemporalFeatureModel featureModel, DwMappingModel mappingModel,
			DwConfiguration configuration, List<IFile> artifacts, Date date, IFolder targetFolder) throws DwVariantDerivationUnsuccessfullException;
}
