package de.darwinspl.variant;

public class DwVariantDerivationUnsuccessfullException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2584279765717999447L;
	
	
	public DwVariantDerivationUnsuccessfullException (String message) {
		super(message);
	}

}
