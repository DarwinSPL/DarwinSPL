package de.darwinspl.variant;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.analyses.DwAnalysesUtil;
import de.darwinspl.feature.configuration.DwConfiguration;
import de.darwinspl.feature.configuration.util.DwConfigurationUtil;
import de.darwinspl.mapping.DwMapping;
import de.darwinspl.mapping.DwMappingModel;
import de.darwinspl.mapping.util.DwMappingUtil;
import de.darwinspl.mapping.util.DwNonExistentFileReferenceException;

public class DwVariantDerivation {

	private static final String VARIANT_DERIVER_ID = "de.darwinspl.variant.deriver";

	public static List<DwVariantDeriver> getRegisteredVariantDerivers() {

		List<DwVariantDeriver> variantDerivers = new ArrayList<>();

		IConfigurationElement[] config = Platform.getExtensionRegistry()
				.getConfigurationElementsFor(VARIANT_DERIVER_ID);
		try {
			for (IConfigurationElement e : config) {
				final Object o = e.createExecutableExtension("class");
				if (o instanceof DwVariantDeriver) {
					variantDerivers.add((DwVariantDeriver) o);
				}
			}
		} catch (CoreException ex) {
			System.out.println(ex.getMessage());
		}

		return variantDerivers;
	}

	public static void deriveVariant(DwVariantDeriver variantDeriver, DwTemporalFeatureModel featureModel,
			DwMappingModel mappingModel, DwConfiguration configuration, Date date, IFolder targetFolder)
			throws DwUnsupportedFileExtensionException, DwNonExistentFileReferenceException,
			DwVariantDerivationUnsuccessfullException {

		List<IFile> artifacts = new ArrayList<>();

		List<DwFeature> selectedFeatures = DwConfigurationUtil
				.getSelectedFeatures(configuration.getFeatureConfigurations());

		for (DwMapping mapping : mappingModel.getMappings()) {
			if (DwAnalysesUtil.isSatisfiedByFeatureSelection(mapping.getCondition(), selectedFeatures)) {
				// This already checks for existence and throws an exception otherwise
				artifacts.addAll(DwMappingUtil.resolveFilesFromMapping(mapping));
			}
		}

		// check if file extensions match the variant derivers
		checkFileExtensionSupport(variantDeriver, artifacts);

		variantDeriver.deriveVariant(featureModel, mappingModel, configuration, artifacts, date, targetFolder);

	}

	private static void checkFileExtensionSupport(DwVariantDeriver variantDeriver, List<IFile> artifacts)
			throws DwUnsupportedFileExtensionException {
		for (IFile artifact : artifacts) {
			String extension = artifact.getFileExtension();
			if (!variantDeriver.supportsFileExtensions().contains(extension)) {
				throw new DwUnsupportedFileExtensionException("Variant Deriver " + variantDeriver.getName()
						+ " does not support extension of file " + artifact.getName());
			}
		}
	}

}
