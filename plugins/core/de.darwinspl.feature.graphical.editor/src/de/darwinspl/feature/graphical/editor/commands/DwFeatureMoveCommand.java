package de.darwinspl.feature.graphical.editor.commands;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.commands.Command;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.evolution.operation.factory.DwSimpleEvolutionOperationFactory;
import de.darwinspl.feature.evolution.operation.interpreter.DwEvolutionOperationInterpreter;
import de.darwinspl.feature.graphical.base.editor.DwBasicGraphicalEditorBase;
import de.darwinspl.feature.operation.DwFeatureMoveOperation;

public class DwFeatureMoveCommand extends Command {
	
	DwFeatureMoveOperation moveEvoOp;
	
	public DwFeatureMoveCommand(DwFeature feature, Object targetObject, DwBasicGraphicalEditorBase editor) {
		DwSimpleEvolutionOperationFactory operationFactory = new DwSimpleEvolutionOperationFactory();
		moveEvoOp = operationFactory.createDwFeatureMoveOperation(feature.getFeatureModel(), feature, (EObject) targetObject, editor.getCurrentSelectedDate(), editor.isLastDateSelected());
	}
	
	@Override
	public void execute() {
		DwEvolutionOperationInterpreter.execute(moveEvoOp);
	}

	@Override
	public void undo() {
		DwEvolutionOperationInterpreter.undo(moveEvoOp);
	}

}
