package de.darwinspl.feature.graphical.editor.policy;

import org.eclipse.draw2d.Label;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.DirectEditPolicy;
import org.eclipse.gef.requests.DirectEditRequest;
import org.eclipse.jface.viewers.CellEditor;

import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.graphical.base.editparts.DwFeatureVersionEditPart;
import de.darwinspl.feature.graphical.base.figures.DwFeatureVersionFigure;
import de.darwinspl.feature.graphical.editor.commands.DwFeatureVersionRenameCommand;

public class DwFeatureVersionDirectEditPolicy extends DirectEditPolicy {

	@Override
	protected Command getDirectEditCommand(DirectEditRequest request) {
		DwFeatureVersionEditPart editPart = getHost();
		DwFeatureVersion version = editPart.getModel();
		
		CellEditor cellEditor = request.getCellEditor();
		String newName = (String) cellEditor.getValue();
		
		return new DwFeatureVersionRenameCommand(version, newName);
	}

	@Override
	protected void showCurrentEditValue(DirectEditRequest request) {
		DwFeatureVersionFigure figure = getHostFigure();
		
		CellEditor cellEditor = request.getCellEditor();
		String currentEditValue = (String) cellEditor.getValue();

		Label label = figure.getLabel();
		label.setText(currentEditValue);
	}

	@Override
	public DwFeatureVersionEditPart getHost() {
		return (DwFeatureVersionEditPart) super.getHost();
	}
	
	@Override
	protected DwFeatureVersionFigure getHostFigure() {
		return (DwFeatureVersionFigure) super.getHostFigure();
	}

}
