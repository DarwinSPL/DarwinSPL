package de.darwinspl.feature.graphical.editor.wizard;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.dialogs.WizardNewFileCreationPage;

import de.christophseidl.util.eclipse.ResourceUtil;
import de.darwinspl.feature.util.custom.DwFeatureUtil;

public class DwTemporalFeatureModelWizardNewFileCreationPage extends WizardNewFileCreationPage {

	public DwTemporalFeatureModelWizardNewFileCreationPage(String pageId, IStructuredSelection selection) {
		super(pageId, selection);
		
		setTitle("DarwinSPL Feature Model");
		setDescription("Create a new temporal Feature Model.");
		setFileName("FeatureModel." + DwFeatureUtil.getFeatureModelFileExtensionForXmi());

		// Try and get the resource selection to determine a current directory for the file dialog.
		//
		if (selection != null && !selection.isEmpty()) {
			// Get the resource...
			//
			Object selectedElement = selection.iterator().next();
			
			if (selectedElement instanceof IResource) {
				// Get the resource parent, if its a file.
				//
				IResource selectedResource = (IResource)selectedElement;
				if (selectedResource.getType() == IResource.FILE) {
					selectedResource = selectedResource.getParent();
				}

				// This gives us a directory...
				//
				if (selectedResource instanceof IFolder || selectedResource instanceof IProject) {
					// Set this for the container.
					//
					setContainerFullPath(selectedResource.getFullPath());

					// Make up a unique new name here.
					//
					String defaultModelBaseFilename = selectedResource.getName();
					String defaultModelFilenameExtension = DwFeatureUtil.getFeatureModelFileExtensionForXmi();
					String modelFilename = defaultModelBaseFilename + "." + defaultModelFilenameExtension;
					for (int i = 1; ((IContainer)selectedResource).findMember(modelFilename) != null; ++i) {
						modelFilename = defaultModelBaseFilename + i + "." + defaultModelFilenameExtension;
					}
					setFileName(modelFilename);
				}
			}
		}
	}
	
	@Override
	protected boolean validatePage() {
		return true;
	}

	public IFile getModelFile() {
		IPath containerFullPath = getContainerFullPath();
		String filename = getFileName();
		IPath filePath = containerFullPath.append(filename);
		String filePathString = filePath.toString();
		IFile file = ResourceUtil.getLocalFile(filePathString);
		String fileExtension = file.getFileExtension();
		
		//Ensure that the file has the right extension
		if (fileExtension.equalsIgnoreCase(DwFeatureUtil.getFeatureModelFileExtensionForXmi())) {
			return file;
		}
		
		return ResourceUtil.getLocalFile(filePathString + "." + DwFeatureUtil.getFeatureModelFileExtensionForXmi());
	}

}
