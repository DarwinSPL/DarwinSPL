package de.darwinspl.feature.graphical.editor.wizard;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;

import de.christophseidl.util.eclipse.ResourceUtil;
import de.christophseidl.util.ecore.EcoreIOUtil;
import de.christophseidl.util.ecore.EcoreResolverUtil;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.configuration.DwConfiguration;
import de.darwinspl.feature.configuration.util.DwConfigurationUtil;
import de.darwinspl.mapping.DwMappingModel;
import de.darwinspl.mapping.util.DwMappingUtil;
import de.darwinspl.variant.DwVariantDerivation;
import de.darwinspl.variant.DwVariantDeriver;

public class DwVariantDerivationModelInputWizardPage extends WizardPage implements SelectionListener {

	private enum ModelType {CONFIGURATION , MAPPING_MODEL}
	
	private final static String TITLE = "Variant Derivation Model Selection";
	private final static String DESCRIPTION = "Input file selection for variant derivation.";

	private DwTemporalFeatureModel featureModel;
	private DwMappingModel mappingModel;
	private DwConfiguration configuration;
	private DwVariantDeriver selectedVariantDeriver;
	private IFolder variantTargetFolder;
	
	private Map<Integer, DwVariantDeriver> indexToVariantDeriverMap;
	
	private boolean configurationFileLoaded;
	
	public DwVariantDerivationModelInputWizardPage(DwTemporalFeatureModel featureModel) {
		super(TITLE);
		setTitle(TITLE);
		setDescription(DESCRIPTION);
		this.featureModel = featureModel;
		configurationFileLoaded = false;
		indexToVariantDeriverMap = new HashMap<>();
	}

	public DwTemporalFeatureModel getFeatureModel() {
		return featureModel;
	}

	public DwMappingModel getMappingModel() {
		return mappingModel;
	}
//
//	private void setMappingModel(DwMappingModel mappingModel) {
//		this.mappingModel = mappingModel;
//	}

	public DwConfiguration getConfiguration() {
		return configuration;
	}
//
//	private void setConfiguration(DwConfiguration configuration) {
//		this.configuration = configuration;
//	}
	
	public boolean isConfigurationFileLoaded() {
		return this.configurationFileLoaded;
	}
	
	public IFolder getVariantTargetFolder() {
		return this.variantTargetFolder;
	}
	
	public DwVariantDeriver getVariantDeriver() {
		return this.selectedVariantDeriver;
	}

	private Composite container;

//	private Button featureModelButton;
//	private Label featureModelLabel;
	
	private Button mappingModelButton;
	private Label mappingModelLabel;

	private Button configurationFileCheckbox;
	
	private Button configurationButton;
	private Label configurationLabel;
	
	private Label variantDeriverLabel;
	private Combo variantDeriverCombo;
	
	private Button targetFolderButton;
	private Label targetFolderLabel;
	
	@Override
	public void createControl(Composite parent) {
		container = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 2;

//		featureModelButton = new Button(container, SWT.PUSH);
//		featureModelButton.setText("Select a feature model.");
//		featureModelButton.addSelectionListener(this);
//
//		featureModelLabel = new Label(container, SWT.NONE);
//		featureModelLabel.setText("No feature model selected.");

		mappingModelButton = new Button(container, SWT.PUSH);
		mappingModelButton.setText("Select a mapping model.");
		mappingModelButton.addSelectionListener(this);

		mappingModelLabel = new Label(container, SWT.NONE);
		mappingModelLabel.setText("No mapping model selected.");
		

		configurationFileCheckbox = new Button(container, SWT.CHECK);
		configurationFileCheckbox.setText("Select configuration from a file");
		configurationFileCheckbox.addSelectionListener(this);

		new Label(container, SWT.NONE);

		configurationButton = new Button(container, SWT.PUSH);
		configurationButton.setText("Select a configuration.");
		configurationButton.addSelectionListener(this);
		configurationButton.setEnabled(false);

		configurationLabel = new Label(container, SWT.NONE);
		configurationLabel.setText("No configuration selected.");
		configurationLabel.setEnabled(false);
		
		variantDeriverLabel = new Label(container, SWT.NONE);
		variantDeriverLabel.setText("Please select a variant deriver:");
		
		
		List<DwVariantDeriver> variantDerivers = DwVariantDerivation.getRegisteredVariantDerivers();
		String[] variantDeriverComboItems = new String[variantDerivers.size()];
		int index = 0;
		for (DwVariantDeriver variantDeriver : variantDerivers) {
			indexToVariantDeriverMap.put(index, variantDeriver);
			variantDeriverComboItems[index] = variantDeriver.getName();
			index++;
		}
		
		variantDeriverCombo = new Combo(container, SWT.NONE);
		variantDeriverCombo.setItems(variantDeriverComboItems);
		variantDeriverCombo.addSelectionListener(this);
		
		targetFolderButton = new Button(container, SWT.PUSH);
		targetFolderButton.setText("Select variant target folder.");
		targetFolderButton.addSelectionListener(this);
		
		targetFolderLabel = new Label(container, SWT.NONE);
		targetFolderLabel.setText("No target folder selected.");
		
        // required to avoid an error in the system
        setControl(container);
        setPageComplete(false);
	}

	@Override
	public void widgetSelected(SelectionEvent e) {
		if (e.getSource().equals(mappingModelButton)) {
			EObject object = loadModel(ModelType.MAPPING_MODEL);
			if (object instanceof DwMappingModel) {
				DwMappingModel selectedMappingModel = (DwMappingModel) object;
				EcoreUtil.resolveAll(selectedMappingModel);
				if (selectedMappingModel.getFeatureModel().equals(featureModel)) {
					this.mappingModel = selectedMappingModel;
					mappingModelLabel.setText(EcoreIOUtil.getFile(mappingModel).getName());
					checkCompleted();
				}
				else {
					// TODO warning
				}
			}
		}
		else if (e.getSource().equals(configurationButton)) {
			EObject object = loadModel(ModelType.CONFIGURATION);
			if (object instanceof DwConfiguration) {
				DwConfiguration selectedConfiguration = (DwConfiguration) object;
				EcoreUtil.resolveAll(selectedConfiguration);
				if (selectedConfiguration.getFeatureModel().equals(featureModel)) {
					this.configuration = selectedConfiguration;
					configurationLabel.setText(EcoreIOUtil.getFile(configuration).getName());
					checkCompleted();
				}
				else {
					// TODO warning
				}
			}
		}
		else if (e.getSource().equals(configurationFileCheckbox)) {
			if (configurationFileCheckbox.getSelection()) {
				configurationButton.setEnabled(true);
				configurationLabel.setEnabled(true);
				configurationFileLoaded = true;
			}
			else {
				configurationButton.setEnabled(false);
				configurationLabel.setEnabled(false);
				configurationFileLoaded = false;
			}
			
			checkCompleted();
		}
		else if (e.getSource().equals(variantDeriverCombo)) {
			int selectionIndex = variantDeriverCombo.getSelectionIndex();
			if (selectionIndex >= 0) {
				selectedVariantDeriver = indexToVariantDeriverMap.get(selectionIndex);
			}
			else {
				selectedVariantDeriver = null;
			}
			checkCompleted();
		}
		else if (e.getSource().equals(targetFolderButton)) {
			IFolder folder = selectFolder();
			variantTargetFolder = folder;
			targetFolderLabel.setText(variantTargetFolder.getFullPath().toString());
			checkCompleted();
		}
		else {
			System.err.println("Error, no matching model type.");
		}
	}
	
	private void checkCompleted() {
		if (mappingModel == null) {
			setPageComplete(false);
			return;
		}
		
		if (configurationFileCheckbox.getSelection() && configuration == null) {
			setPageComplete(false);
			return;
		}
		
		if (selectedVariantDeriver == null) {
			setPageComplete(false);
			return;
		}
		
		if (variantTargetFolder == null) {
			setPageComplete(false);
			return;
		}
		
		if (variantTargetFolder == null) {
			setPageComplete(false);
			return;
		}
		
		setPageComplete(true);
	}
	
	private EObject loadModel(ModelType modelType) {
		String fileExtension = "";
		
		FileDialog fileDialog = new FileDialog(getShell(), SWT.OPEN);
		switch (modelType) {
		case CONFIGURATION:
		    fileDialog.setFilterNames(new String[] { "DarwinSPL Configuration"});
		    fileExtension = DwConfigurationUtil.getConfigurationModelFileExtensionForXmi();
			break;
		case MAPPING_MODEL:
		    fileDialog.setFilterNames(new String[] { "DarwinSPL Mapping Model"});
			fileExtension = DwMappingUtil.getFILE_EXTENSION_FOR_CONCRETE_SYNTAX();
			break;
		}
	    fileDialog.setFilterExtensions(new String[] { "*."+fileExtension});
	    
	    IFile featureModelFile = EcoreIOUtil.getFile(featureModel);
	    
	    String filterPath = ResourceUtil.createAbsolutePath(featureModelFile.getParent().getFullPath()).toOSString();
	    fileDialog.setFilterPath(filterPath); // Windows path
	    
	    String featureModelFileName = ResourceUtil.getBaseFilename(featureModelFile);
	    fileDialog.setFileName(featureModelFileName+"."+fileExtension);
	    
	    String modelFilePath = fileDialog.open();
	    
	    if (modelFilePath == null) {
	    	return null;
	    }
	    
	    File modelFile = new File(modelFilePath);
	    URI uri = URI.createFileURI(modelFile.getAbsolutePath());
	    
	    EObject loadedModel = EcoreIOUtil.loadModel(uri, featureModel.eResource().getResourceSet());
	    
	    EcoreResolverUtil.resolveReferencedModels(loadedModel);
	    return loadedModel;
		
	}
	
	private IFolder selectFolder() {
		DirectoryDialog directoryDialog = new DirectoryDialog(getShell());

	    IFile featureModelFile = EcoreIOUtil.getFile(featureModel);
	    
	    String filterPath = ResourceUtil.createAbsolutePath(featureModelFile.getParent().getFullPath()).toOSString();
		
		directoryDialog.setFilterPath(filterPath);
		
		String folderPath = directoryDialog.open();
		
		if (folderPath == null) {
			return null;
		}
		
		IPath filePath = new Path(folderPath);
		
		IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
//		IPath workspacePath = workspaceRoot.getFullPath();
		IPath workspacePath = workspaceRoot.getLocation();
		
		IPath relativeFilePath = filePath.makeRelativeTo(workspacePath);
		
		return workspaceRoot.getFolder(relativeFilePath);
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		// TODO Auto-generated method stub
		
	}

}
