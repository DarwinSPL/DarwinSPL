package de.darwinspl.feature.graphical.editor.commands;

import org.eclipse.gef.commands.Command;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureTypeEnum;
import de.darwinspl.feature.evolution.operation.factory.DwSimpleEvolutionOperationFactory;
import de.darwinspl.feature.evolution.operation.interpreter.DwEvolutionOperationInterpreter;
import de.darwinspl.feature.graphical.base.editor.DwBasicGraphicalEditorBase;
import de.darwinspl.feature.operation.DwFeatureTypeChangeOperation;

public class DwFeatureChangeVariationTypeCommand extends Command {
		
	DwFeatureTypeChangeOperation evoOp;
	
	public DwFeatureChangeVariationTypeCommand(DwFeature feature, DwFeatureTypeEnum variationType, DwBasicGraphicalEditorBase editor) {
		DwSimpleEvolutionOperationFactory operationFactory = new DwSimpleEvolutionOperationFactory();
		evoOp = operationFactory.createDwFeatureTypeChangeOperation(feature.getFeatureModel(), feature, variationType, editor.getCurrentSelectedDate(), editor.isLastDateSelected());
	}
	
	@Override
	public void execute() {
		DwEvolutionOperationInterpreter.execute(evoOp);
	}

	@Override
	public void undo() {
		DwEvolutionOperationInterpreter.undo(evoOp);
	}
	
}
