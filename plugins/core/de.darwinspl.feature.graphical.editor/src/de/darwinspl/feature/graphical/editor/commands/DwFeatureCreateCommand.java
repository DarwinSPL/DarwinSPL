package de.darwinspl.feature.graphical.editor.commands;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.commands.Command;

import de.darwinspl.feature.evolution.operation.factory.DwSimpleEvolutionOperationFactory;
import de.darwinspl.feature.evolution.operation.interpreter.DwEvolutionOperationInterpreter;
import de.darwinspl.feature.graphical.base.editor.DwBasicGraphicalEditorBase;
import de.darwinspl.feature.operation.DwFeatureCreateOperation;

public class DwFeatureCreateCommand extends Command {

	private static final DwSimpleEvolutionOperationFactory operationFactory = new DwSimpleEvolutionOperationFactory();
	
	private DwFeatureCreateOperation createEvoOp;
	
	

	public DwFeatureCreateCommand(Object parentObject, DwBasicGraphicalEditorBase editor) {
		init(parentObject, editor, false);
	}
	
	public DwFeatureCreateCommand(Object parentObject, DwBasicGraphicalEditorBase editor, boolean forceCreateParentGroup) {
		init(parentObject, editor, forceCreateParentGroup);
	}

	private void init(Object parentObject, DwBasicGraphicalEditorBase editor, boolean forceCreateParentGroup) {
		createEvoOp = operationFactory.createDwFeatureCreateOperation(editor.getFeatureModel(), (EObject) parentObject, forceCreateParentGroup, editor.getCurrentSelectedDate(), editor.isLastDateSelected());
	}
	
	
	
	@Override
	public void execute() {
		DwEvolutionOperationInterpreter.execute(createEvoOp);
	}

	@Override
	public void undo() {
		DwEvolutionOperationInterpreter.undo(createEvoOp);
	}
}
