package de.darwinspl.feature.graphical.editor.action;

import java.util.Date;

import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.editor.commands.DwFeatureVersionCreateCommand;
import de.darwinspl.feature.util.custom.DwFeatureVersionUtil;

public class DwFeatureVersionCreateSuccessorOnNewBranchAction extends DwAbstractFeatureVersionCreateAction {
	
	public static final String ID = DwFeatureVersionCreateSuccessorOnNewBranchAction.class.getCanonicalName();
	
	public DwFeatureVersionCreateSuccessorOnNewBranchAction(DwGraphicalEditor editor) {
		super(editor);
	}

	@Override
	protected String createText() {
		return "Create Successor Version On New Branch";
	}
	
	@Override
	protected String createID() {
		return ID;
	}
	
	@Override
	protected String createIconPath() {
		return "icons/ActionCreateSuccessorVersionOnNewBranch.png";
	}
	
	@Override
	protected boolean acceptsSelectedModel(Object selectedModel) {
		Date date = this.getDateFromEditor();
		
		if (selectedModel instanceof DwFeatureVersion) {
			DwFeatureVersion version = (DwFeatureVersion) selectedModel;
			
			if (!DwFeatureVersionUtil.isLastVersionOnBranch(version, date)) {
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	protected DwFeatureVersionCreateCommand doCreateCommand(DwFeatureVersion version, Object acceptedModel) {
		DwFeatureVersion selectedVersion = (DwFeatureVersion) acceptedModel;
		return new DwFeatureVersionCreateCommand(version, selectedVersion, true, false);
	}

}
