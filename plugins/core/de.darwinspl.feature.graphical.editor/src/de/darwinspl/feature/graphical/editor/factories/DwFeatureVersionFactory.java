package de.darwinspl.feature.graphical.editor.factories;

import org.eclipse.gef.requests.CreationFactory;

import de.darwinspl.feature.DwFeatureFactory;
import de.darwinspl.feature.DwFeatureVersion;

public class DwFeatureVersionFactory implements CreationFactory {
	private static final String defaultNumber = "NewVersion";
	
	@Override
	public DwFeatureVersion getNewObject() {
		DwFeatureVersion version = DwFeatureFactory.eINSTANCE.createDwFeatureVersion();
		
		version.setNumber(defaultNumber);
		
		return version;
	}

	@Override
	public Object getObjectType() {
		return DwFeatureVersion.class;
	}

}
