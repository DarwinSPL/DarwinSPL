package de.darwinspl.feature.graphical.editor.policy;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.ComponentEditPolicy;
import org.eclipse.gef.requests.GroupRequest;

import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.graphical.editor.DwEditorGraphicalViewer;
import de.darwinspl.feature.graphical.editor.commands.DwGroupDeleteCommand;

public class DwGroupComponentEditPolicy extends ComponentEditPolicy {
	@Override
	protected Command createDeleteCommand(GroupRequest deleteRequest) {
		EditPart editPart = getHost();
		DwGroup group = (DwGroup) editPart.getModel();
		DwEditorGraphicalViewer viewer = (DwEditorGraphicalViewer) editPart.getViewer();
		
		//TODO check for right editor editpart
		return new DwGroupDeleteCommand(group, viewer.getGraphicalEditor());
	}

}
