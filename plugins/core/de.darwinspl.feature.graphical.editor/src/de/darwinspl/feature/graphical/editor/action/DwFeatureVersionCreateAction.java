package de.darwinspl.feature.graphical.editor.action;

import java.util.Date;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.editor.commands.DwFeatureVersionCreateCommand;
import de.darwinspl.feature.util.custom.DwFeatureUtil;

public class DwFeatureVersionCreateAction extends DwAbstractFeatureVersionCreateAction {
	
	public static final String ID = DwFeatureVersionCreateAction.class.getCanonicalName();
	
	public DwFeatureVersionCreateAction(DwGraphicalEditor editor) {
		super(editor);
	}

	@Override
	protected String createText() {
		return "Create Version";
	}
	
	@Override
	protected String createID() {
		return ID;
	}
	
	@Override
	protected String createIconPath() {
		return "icons/ActionCreateVersion.png";
	}
	
	@Override
	protected boolean acceptsSelectedModel(Object selectedModel) {
		if (selectedModel instanceof DwFeature) {
			DwFeature feature = (DwFeature) selectedModel;

			Date date = this.getDateFromEditor();
			if (!DwFeatureUtil.hasVersions(feature, date)) {
				return true;
			}
		}
		
		//NOTE: Intended not to call super as this should only be useable on features. 
		return false;
	}
	
	@Override
	protected DwFeatureVersionCreateCommand doCreateCommand(DwFeatureVersion version, Object acceptedModel) {
		return new DwFeatureVersionCreateCommand(version, acceptedModel);
	}

}
