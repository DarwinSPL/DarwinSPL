package de.darwinspl.feature.graphical.editor.action;

import java.util.Date;

import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupTypeEnum;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.util.custom.DwFeatureUtil;

public class DwGroupMakeAlternativeAction extends DwAbstractGroupTypeChangeAction {
	
	public static final String ID = DwGroupMakeAlternativeAction.class.getCanonicalName();

	public DwGroupMakeAlternativeAction(DwGraphicalEditor editor) {
		super(editor);
	}

	@Override
	protected String getGroupTypeText() {
		return "Alternative";
	}

	@Override
	protected String createID() {
		return ID;
	}

	@Override
	protected String createIconPath() {
		return "icons/ActionMakeGroupAlternative.png";
	}

	@Override
	protected boolean checkIfGroupTypeIsAlreadySet(DwGroup group, Date date) {
		return DwFeatureUtil.isAlternative(group, date);
	}

	@Override
	protected DwGroupTypeEnum getTargetGroupTypeEnum() {
		return DwGroupTypeEnum.ALTERNATIVE; 
	}

}
