package de.darwinspl.feature.graphical.editor.commands.stack;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.command.AbortExecutionException;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.commands.ForwardUndoCompoundCommand;
import org.eclipse.gef.ui.properties.SetPropertyValueCommand;
import org.eclipse.ui.views.properties.IPropertySource;

import de.darwinspl.feature.graphical.editor.DwEditorGraphicalEditor;

public class DwCommandStack extends CommandStack {
	
	public void execute(Command command) {
		try {
//			if(command instanceof ForwardUndoCompoundCommand) {
//				List<Command> allCommands = unrollCompoundCommand((CompoundCommand) command);
//				Command firstCommand = allCommands.get(0);
//				if(firstCommand instanceof SetPropertyValueCommand) {
//					super.execute(firstCommand);
//					return;
//				}
//			}
			
//			DwEditorGraphicalEditor editor = 
			
			super.execute(command);
		}
		catch(AbortExecutionException e) {
			// do nothing!
		}
	}
	
	private List<Command> unrollCompoundCommand(CompoundCommand compound) {
		List<Command> result = new LinkedList<>(); 
		
		for(Object child : compound.getCommands()) {
			if(child instanceof CompoundCommand)
				result.addAll(unrollCompoundCommand((CompoundCommand) child));
			else if(child instanceof Command)
				result.add((Command) child);
		}
		
		return result;
	}
	
}
