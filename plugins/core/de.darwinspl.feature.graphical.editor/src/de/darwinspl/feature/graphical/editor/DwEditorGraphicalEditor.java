package de.darwinspl.feature.graphical.editor;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.ICoreRunnable;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.KeyHandler;
import org.eclipse.gef.KeyStroke;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.gef.ui.actions.DirectEditAction;
import org.eclipse.gef.ui.actions.GEFActionConstants;
import org.eclipse.gef.ui.parts.GraphicalViewerKeyHandler;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;

import de.christophseidl.util.eclipse.ResourceUtil;
import de.christophseidl.util.ecore.EcoreIOUtil;
import de.darwinspl.constraint.DwConstraintModel;
import de.darwinspl.constraint.util.custom.DwConstraintUtil;
import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.analyses.DwAnalysesUtil;
import de.darwinspl.feature.configuration.DwConfiguration;
import de.darwinspl.feature.configuration.DwFeatureConfiguration;
import de.darwinspl.feature.configuration.DwFeatureSelection;
import de.darwinspl.feature.configuration.util.DwConfigurationUtil;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalViewer;
import de.darwinspl.feature.graphical.base.factories.DwEditPartFactory;
import de.darwinspl.feature.graphical.editor.action.DwCopyIdAction;
import de.darwinspl.feature.graphical.editor.action.DwExportImageAction;
import de.darwinspl.feature.graphical.editor.action.DwFeatureCreateAction;
import de.darwinspl.feature.graphical.editor.action.DwFeatureCreateInNewGroupAction;
import de.darwinspl.feature.graphical.editor.action.DwFeatureMakeMandatoryAction;
import de.darwinspl.feature.graphical.editor.action.DwFeatureMakeOptionalAction;
import de.darwinspl.feature.graphical.editor.action.DwFeatureVersionCreateAction;
import de.darwinspl.feature.graphical.editor.action.DwGroupMakeAlternativeAction;
import de.darwinspl.feature.graphical.editor.action.DwGroupMakeAndAction;
import de.darwinspl.feature.graphical.editor.action.DwGroupMakeOrAction;
import de.darwinspl.feature.graphical.editor.action.DwManageValidityAction;
import de.darwinspl.feature.graphical.editor.commands.stack.DwEditDomain;
import de.darwinspl.feature.graphical.editor.editparts.DwEditorFeatureEditPart;
import de.darwinspl.feature.graphical.editor.factories.DwEditorEditPartFactory;
import de.darwinspl.feature.graphical.editor.wizard.DwVariantDerivationWizard;
import de.darwinspl.feature.impl.custom.DwFeatureModelResource;
import de.darwinspl.feature.util.custom.DwSimpleFeatureFactoryCustom;
import de.darwinspl.temporal.DwTemporalInterval;
import de.darwinspl.temporal.DwTemporalModelFactory;

public class DwEditorGraphicalEditor extends DwGraphicalEditor {

	private DwEditorContextMenuProvider contextMenuProvider;

	private boolean editingActivated;

	private List<DwFeature> selectedFeatures;

//	private DwConfiguration currentConfiguration;

	public DwEditorGraphicalEditor() {
		editingActivated = true;
		selectedFeatures = new ArrayList<DwFeature>();

//		currentConfiguration = DwConfigurationUtil.createNewConfiguration();
	}

	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		super.init(site, input);
	}

	@Override
	protected DwTemporalFeatureModel doLoadDataModel(IFile file) {
		Job loadModelJob = Job.create("Loading \"" + file.getName() + "\" inside the DarwinSPL Editor",
				(ICoreRunnable) monitor -> {
					DwTemporalFeatureModel model = super.doLoadDataModel(file);

					Display.getDefault().asyncExec(() -> {
						setDataModel(model);
						refreshVisuals();
					});
				});

		loadModelJob.schedule();
		return DwSimpleFeatureFactoryCustom
				.createNewFeatureModel(DwSimpleFeatureFactoryCustom.createNewFeature("Loading Model ..."));
	}

	@Override
	protected DefaultEditDomain createEditDomain() {
		return new DwEditDomain(this);
	}

	@Override
	protected DwEditPartFactory createEditPartFactory() {
		return new DwEditorEditPartFactory(this);
	}

	@Override
	protected DwGraphicalViewer doCreateGraphicalViewer() {
		return new DwEditorGraphicalViewer(this);
	}

//	@Override
//	public void doSave(IProgressMonitor monitor) {
//		monitor.beginTask("Saving DarwinSPL model...", 2);
//		super.doSave(monitor);
//		monitor.worked(1);
//		DwEvolutionOperationModelResourceUtil.saveOperationsModel(this.getFeatureModel());
//		monitor.done();
//	}

	@Override
	protected void configureGraphicalViewer() {
		super.configureGraphicalViewer();

//		registerActions();
		createContextMenu();
		enableKeyboardShortcuts();
	}

	@Override
	protected void initializeGraphicalViewer() {
		super.initializeGraphicalViewer();
		this.getSite().setSelectionProvider(this.getGraphicalViewer());

//		getGraphicalViewer().addDragSourceListener(new AbstractTransferDragSourceListener(getGraphicalViewer()) {
//			@Override
//			public void dragSetData(DragSourceEvent event) {
//				System.out.println("drag");
//			}
//		});

	}

	@Override
	@SuppressWarnings("unchecked")
	protected void registerActions(ActionRegistry actionRegistry) {
		super.registerActions(actionRegistry);

//		ActionRegistry actionRegistry = getActionRegistry();

		// TODO: Look into getSelectionActions() and UpdateAction
		IAction directEditAction = new DirectEditAction(this);
		actionRegistry.registerAction(directEditAction);
		@SuppressWarnings("rawtypes")
		List selectionActions = getSelectionActions();
		selectionActions.add(directEditAction.getId());

		actionRegistry.registerAction(new DwFeatureCreateAction(this));
		actionRegistry.registerAction(new DwFeatureCreateInNewGroupAction(this));
		actionRegistry.registerAction(new DwFeatureVersionCreateAction(this));

		actionRegistry.registerAction(new DwFeatureMakeOptionalAction(this));
		actionRegistry.registerAction(new DwFeatureMakeMandatoryAction(this));

		// TODO versions require re-integration. Operation interpreter and consistency
		// checking.
//		actionRegistry.registerAction(new DwFeatureVersionCreatePredecessorAction(this));
//		actionRegistry.registerAction(new DwFeatureVersionCreateSuccessorAction(this));
//		actionRegistry.registerAction(new DwFeatureVersionCreateSuccessorOnNewBranchAction(this));

		actionRegistry.registerAction(new DwGroupMakeAndAction(this));
		actionRegistry.registerAction(new DwGroupMakeOrAction(this));
		actionRegistry.registerAction(new DwGroupMakeAlternativeAction(this));

		actionRegistry.registerAction(new DwManageValidityAction(this));
		actionRegistry.registerAction(new DwCopyIdAction(this));
		actionRegistry.registerAction(new DwExportImageAction(this));
	}

	protected void createContextMenu() {
		DwGraphicalViewer viewer = getViewer();
		ActionRegistry actionRegistry = getActionRegistry();

		contextMenuProvider = new DwEditorContextMenuProvider(viewer, actionRegistry);
		contextMenuProvider.setFeatureModelEditingActive(true);
		viewer.setContextMenu(contextMenuProvider);
	}

	protected void enableEditingMode() {
		editingActivated = true;
//		DwGraphicalViewer viewer = getViewer();
//		viewer.setContextMenu(null);
		contextMenuProvider.setFeatureModelEditingActive(true);

		IAction directEditAction = getActionRegistry().getAction(GEFActionConstants.DIRECT_EDIT);
		getViewer().getKeyHandler().put(KeyStroke.getPressed(SWT.F2, SWT.NONE), directEditAction);
	}

	/**
	 * 
	 * @return True if editing mode is disabled and potentially unsaved changes have
	 *         been saved. False if editing mode is not disabled as user declined to
	 *         save unsaved changes.
	 */
	protected boolean disableEditingMode() {
		if (this.isDirty()) {

			MessageBox dialog = new MessageBox(getShell(), SWT.ICON_INFORMATION | SWT.OK | SWT.CANCEL);
			dialog.setText("Unsaved Changes");
			dialog.setMessage(
					"There are unsaved changes to the feature model. Do you want to continue disabling the editing mode and save the changes?");

			// open dialog and await user selection
			int returnCode = dialog.open();
			if (returnCode == SWT.CANCEL) {
				return false;
			}

			this.doSave(null);
		}

		editingActivated = false;
//		DwGraphicalViewer viewer = getViewer();
//		viewer.setContextMenu(null);
		contextMenuProvider.setFeatureModelEditingActive(false);
		getViewer().getKeyHandler().remove(KeyStroke.getPressed(SWT.F2, SWT.NONE));

		return true;
	}

	/**
	 * returns true if deselecting is confirmed by the user
	 * 
	 * @return
	 */
	public boolean discardConfiguration() {
		if (selectedFeatures.isEmpty()) {
			return true;
		}

		MessageBox dialog = new MessageBox(getShell(), SWT.ICON_WARNING | SWT.OK | SWT.CANCEL);
		dialog.setText("Resetting Configuration");
		dialog.setMessage("If you proceed, the current configuration will be discarded.\n Do you want to proceed?");

		// open dialog and await user selection
		int returnCode = dialog.open();
		if (returnCode == SWT.CANCEL) {
			return false;
		}

		EditPart contentsEditPart = getViewer().getContents();
		deselectAllFeatures(contentsEditPart);

		return true;
	}

	protected void deselectAllFeatures(EditPart parentEditPart) {
		for (Object childEditPartObject : parentEditPart.getChildren()) {
			if (childEditPartObject instanceof EditPart) {

				if (childEditPartObject instanceof DwEditorFeatureEditPart) {
					DwEditorFeatureEditPart featureEditPart = (DwEditorFeatureEditPart) childEditPartObject;
					featureEditPart.setFeatureSelected(false);
				}

				deselectAllFeatures((EditPart) childEditPartObject);
			}
		}

	}

	/**
	 * This is currently not updating the visuals, aka the feature edit parts
	 * 
	 * @param feature
	 */
	public void selectFeature(DwFeature feature) {
		selectedFeatures.add(feature);
	}

	/**
	 * This is currently not updating the visuals, aka the feature edit parts
	 * 
	 * @param feature
	 */
	public void deselectFeature(DwFeature feature) {
		selectedFeatures.remove(feature);
	}

	private void enableKeyboardShortcuts() {
		DwGraphicalViewer viewer = getViewer();
		ActionRegistry actionRegistry = getActionRegistry();

		// TODO: This isn't working as expected as action is null
		// Include keyboard shortcuts
		KeyHandler keyHandler = viewer.getKeyHandler();

		if (keyHandler == null) {
			keyHandler = new GraphicalViewerKeyHandler(viewer);
			viewer.setKeyHandler(keyHandler);
		}

		IAction directEditAction = actionRegistry.getAction(GEFActionConstants.DIRECT_EDIT);
		keyHandler.put(KeyStroke.getPressed(SWT.F2, SWT.NONE), directEditAction);
	}

	@Override
	protected ContextMenuProvider createContextMenuProvider(GraphicalViewer viewer, ActionRegistry actionRegistry) {
//		System.err.println("DwEditorGraphicalEditor.createContextMenuProvider(...) wurde aufgerufen. TODO: Implement!");
		return new DwEditorContextMenuProvider(viewer, actionRegistry);
	}

	@Override
	public void dispose() {
		super.dispose();
		DwFeatureModelResource res = (DwFeatureModelResource) this.getFeatureModel().eResource();
		if (res != null)
			res.discard();
	}

	public boolean isEditingActivated() {
		return editingActivated;
	}

	public boolean saveConfiguration() {

		FileDialog fileDialog = new FileDialog(getShell(), SWT.SAVE);
		fileDialog.setFilterNames(new String[] { "DarwinSPL Configuration" });
		fileDialog.setFilterExtensions(new String[] { "*.dwconfiguration" });
		fileDialog.setOverwrite(true);

		String filterPath = ResourceUtil.createAbsolutePath(getInputFile().getParent().getFullPath()).toOSString();
		fileDialog.setFilterPath(filterPath); // Windows path

		String featureModelFileName = ResourceUtil.getBaseFilename(getInputFile());
		fileDialog.setFileName(featureModelFileName + ".dwconfiguration");

		String configurationFilePath = fileDialog.open();

		if (configurationFilePath == null) {
			return false;
		}

		IFile configurationFile = ResourceUtil.makeFileRelativeToWorkspace(configurationFilePath);

		DwConfiguration configuration = createConfigurationFromSelectedFeatures();

		EcoreIOUtil.saveModelAs(configuration, configurationFile, getFeatureModel().eResource().getResourceSet());
		System.out.println("Saved configuration to: " + configurationFilePath);

		return true;
	}

	private DwConfiguration createConfigurationFromSelectedFeatures() {
		DwConfiguration configuration = DwConfigurationUtil.createNewConfiguration();
		DwTemporalInterval interval = DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
		interval.setFrom(getCurrentSelectedDate());
		interval.setTo(null);
		configuration.getValidities().add(interval);

		configuration.setFeatureModel(getFeatureModel());

		for (DwFeature selectedFeature : selectedFeatures) {
			DwConfigurationUtil.selectFeature(selectedFeature, configuration, getCurrentSelectedDate());
		}

		return configuration;
	}

	public boolean loadConfiguration() {

		FileDialog fileDialog = new FileDialog(getShell(), SWT.OPEN);
		fileDialog.setFilterNames(new String[] { "DarwinSPL Configuration" });
		fileDialog.setFilterExtensions(
				new String[] { "*." + DwConfigurationUtil.getConfigurationModelFileExtensionForXmi() });

		String filterPath = ResourceUtil.createAbsolutePath(getInputFile().getParent().getFullPath()).toOSString();
		fileDialog.setFilterPath(filterPath); // Windows path

		String featureModelFileName = ResourceUtil.getBaseFilename(getInputFile());
		fileDialog.setFileName(
				featureModelFileName + "." + DwConfigurationUtil.getConfigurationModelFileExtensionForXmi());

		String configurationFilePath = fileDialog.open();

		if (configurationFilePath == null) {
			return false;
		}

		IFile configurationFile = ResourceUtil.makeFileRelativeToWorkspace(configurationFilePath);

		EObject loadedModel = EcoreIOUtil.loadModel(configurationFile, getFeatureModel().eResource().getResourceSet());
		if (loadedModel instanceof DwConfiguration) {
			DwConfiguration configuration = (DwConfiguration) loadedModel;
			if (configuration.isValid(getCurrentSelectedDate())) {

				for (DwFeatureConfiguration featureConfiguration : configuration.getFeatureConfigurations()) {
					if (!featureConfiguration.isValid(getCurrentSelectedDate())) {
						continue;
					}

					if (featureConfiguration instanceof DwFeatureSelection) {
						selectedFeatures.add(featureConfiguration.getFeature());
					}
				}
			} else {
				System.out.println("Loaded configuration was not valid at selected date.");
				return false;
			}

			if (selectedFeatures.isEmpty()) {
				System.out.println("No features selected, configuration was empty.");
				return true;
			}

			markAllFeaturesFromLoadedConfiguration(getViewer().getContents());
		} else {
			System.out.println("Loaded model was no instance of DwConfiguration.");
			return false;
		}

		return true;
	}

	private void markAllFeaturesFromLoadedConfiguration(EditPart editPart) {
		for (Object editPartChildObject : editPart.getChildren()) {
			if (editPartChildObject instanceof EditPart) {
				EditPart editPartChild = (EditPart) editPartChildObject;

				if (editPartChild instanceof DwEditorFeatureEditPart) {
					DwEditorFeatureEditPart featureEditPart = (DwEditorFeatureEditPart) editPartChild;
					if (selectedFeatures.contains(featureEditPart.getModel())) {
						featureEditPart.setFeatureSelected(true);
					}
				}

				markAllFeaturesFromLoadedConfiguration(editPartChild);
			}
		}
	}

	public void checkConfigurationValidity() {
		int iconStyle;
		String message;

		if (doCheckConfigurationValidity()) {
			iconStyle = SWT.ICON_WORKING;
			message = "The configuration is valid!";
		} else {
			iconStyle = SWT.ICON_ERROR;
			message = "The configuration is not valid!";
		}

		MessageBox messageBox = new MessageBox(getShell(), iconStyle | SWT.OK);
		messageBox.setMessage(message);
		messageBox.setText("Configuration Validity Checked:");
		messageBox.open();
	}

	private boolean doCheckConfigurationValidity() {
		DwConfiguration configuration = createConfigurationFromSelectedFeatures();

		List<DwConstraintModel> constraintModels = new ArrayList<>();

		IFile featureModelFile = EcoreIOUtil.getFile(getFeatureModel());
		IProject featureModelProject = featureModelFile.getProject();
		try {
			IResource[] members = featureModelProject.members();
			List<IFile> constraintModelFiles = getConstraintModelFiles(members);

			for (IFile constraintModelFile : constraintModelFiles) {
				EObject eObject = EcoreIOUtil.loadModel(constraintModelFile,
						getFeatureModel().eResource().getResourceSet());
				if (eObject != null && eObject instanceof DwConstraintModel) {
					DwConstraintModel constraintModel = (DwConstraintModel) eObject;
					if (constraintModel.getFeatureModel().equals(getFeatureModel())) {
						constraintModels.add(constraintModel);
					}
				}
			}
		} catch (CoreException e) {
			e.printStackTrace();
		}

		return DwAnalysesUtil.checkConfigurationValidity(getFeatureModel(), constraintModels, configuration,
				getCurrentSelectedDate());
	}

	private List<IFile> getConstraintModelFiles(IResource[] members) {
		List<IFile> constraintModelFiles = new ArrayList<>();

		for (int i = 0; i < members.length; i++) {
			IResource member = members[i];
			if (member instanceof IFile) {
				IFile memberFile = (IFile) member;
				String fileExtension = memberFile.getFileExtension();
				if (fileExtension.equals(DwConstraintUtil.getFileExtensionForAbstractSyntax())
						|| fileExtension.equals(DwConstraintUtil.getFileExtensionForConcreteSyntax())) {
					constraintModelFiles.add(memberFile);
				}
			} else if (member instanceof IFolder) {
				IFolder folder = (IFolder) member;
				try {
					constraintModelFiles.addAll(getConstraintModelFiles(folder.members()));
				} catch (CoreException e) {
					e.printStackTrace();
				}
			}
		}

		return constraintModelFiles;
	}

	public void deriveVariant() {
		if (!doCheckConfigurationValidity()) {
			MessageBox messageBox = new MessageBox(getShell(), SWT.ICON_ERROR | SWT.OK);
			messageBox.setMessage("The configuration is not valid!");
			messageBox.setText("Configuration Validity Checked:");
			messageBox.open();
			return;
		}

		WizardDialog wizardDialog = new WizardDialog(getShell(), new DwVariantDerivationWizard(getFeatureModel(),
				createConfigurationFromSelectedFeatures(), getCurrentSelectedDate()));
		if (wizardDialog.open() == Window.OK) {
			System.out.println("Ok pressed");
			// TODO variant derivation successfull
		} else {
			System.out.println("Cancel pressed");
		}
	}
}
