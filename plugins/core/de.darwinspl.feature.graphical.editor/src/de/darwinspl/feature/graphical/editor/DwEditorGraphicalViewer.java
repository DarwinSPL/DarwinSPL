package de.darwinspl.feature.graphical.editor;

import java.util.Date;

import org.eclipse.swt.events.SelectionEvent;

import de.darwinspl.feature.graphical.base.editor.DwGraphicalViewer;

public class DwEditorGraphicalViewer extends DwGraphicalViewer {

	public DwEditorGraphicalViewer(DwEditorGraphicalEditor editor) {
		super(editor);
	}

	@Override
	public DwEditorGraphicalEditor getGraphicalEditor() {
		return (DwEditorGraphicalEditor) super.getGraphicalEditor();
	}

	@Override
	protected void registerListeners() {
		super.registerListeners();
	}

	public void widgetSelected(SelectionEvent e) {
		if (e == null || e.getSource() == null) {
			return;
		} else if (e.getSource().equals(configurationModeButton)) {
			DwEditorGraphicalEditor editor = getGraphicalEditor();
			
			if (configurationModeButton.getSelection()) {
				if (editor.disableEditingMode()) {
					deriveVariantButton.setEnabled(true);
					checkConfigurationValidityButton.setEnabled(true);
					saveConfigurationButton.setEnabled(true);
					loadConfigurationButton.setEnabled(true);
				}
				else {
					configurationModeButton.setSelection(false);
					return;
				}
				
			}
			else {
				if (editor.discardConfiguration()) {
					editor.enableEditingMode();
					
					deriveVariantButton.setEnabled(false);
					checkConfigurationValidityButton.setEnabled(false);
					saveConfigurationButton.setEnabled(false);
					loadConfigurationButton.setEnabled(false);
				}
				else {
					configurationModeButton.setSelection(true);
					return;
				}
				
			}
		}
		else if (e.getSource().equals(deriveVariantButton)) {
			getGraphicalEditor().deriveVariant();
		}
		else if (e.getSource().equals(checkConfigurationValidityButton)) {
			getGraphicalEditor().checkConfigurationValidity();
		}
		else if (e.getSource().equals(saveConfigurationButton)) {
			getGraphicalEditor().saveConfiguration();
		}
		else if (e.getSource().equals(loadConfigurationButton)) {
			getGraphicalEditor().loadConfiguration();
		}
//	configurationModeButton.
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void dateChanged(Date newDate, Date oldDate, Object source) {
		// TODO date is currently still changed in the slider.
		
		if (getGraphicalEditor().discardConfiguration()) {
			super.dateChanged(newDate, oldDate, source);			
		}
		else {
			return;
		}
		
	}

}
