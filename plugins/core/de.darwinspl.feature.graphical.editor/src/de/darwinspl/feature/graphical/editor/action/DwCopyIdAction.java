package de.darwinspl.feature.graphical.editor.action;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

import org.eclipse.gef.commands.Command;

import de.darwinspl.feature.graphical.base.action.DwCommandAction;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.temporal.DwTemporalElement;

public class DwCopyIdAction extends DwCommandAction {
	
	public static final String ID = DwCopyIdAction.class.getCanonicalName();
	
	public DwCopyIdAction(DwGraphicalEditor editor) {
		super(editor);
	}

	@Override
	protected String createText() {
		return "Copy element ID to clipboard";
	}
	
	@Override
	protected String createID() {
		return ID;
	}
	
	@Override
	protected String createIconPath() {
		return "icons/ActionGetId.png";
	}
	
	@Override
	protected boolean acceptsSelectedModel(Object selectedModel) {
		return (selectedModel instanceof DwTemporalElement);
	}
	
	@Override
	protected void execute(Object acceptedModel) {
		String id = ((DwTemporalElement) acceptedModel).getId(); 
		
//		int result = DwDialogManager.openDialog(
//				"DarwinSPL ID",
//				id,
//				MessageDialog.INFORMATION,
//				new String[] { "Copy to clipboard", "Close" }
//			);
		
		StringSelection stringSelection = new StringSelection(id);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(stringSelection, null);
	}
	
	@Override
	protected Command createCommand(Object acceptedModel) {
		return null;
	}
	
	@Override
	public void updateEnabledState() {
		super.updateEnabledState();
	}

}
