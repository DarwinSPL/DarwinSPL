package de.darwinspl.feature.graphical.editor.action;

import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.editor.commands.DwFeatureVersionCreateCommand;

public class DwFeatureVersionCreateSuccessorAction extends DwAbstractFeatureVersionCreateAction {
	
	public static final String ID = DwFeatureVersionCreateSuccessorAction.class.getCanonicalName();
	
	public DwFeatureVersionCreateSuccessorAction(DwGraphicalEditor editor) {
		super(editor);
	}

	@Override
	protected String createText() {
		return "Create Successor Version";
	}
	
	@Override
	protected String createID() {
		return ID;
	}
	
	@Override
	protected String createIconPath() {
		return "icons/ActionCreateSuccessorVersion.png";
	}
	
	@Override
	protected DwFeatureVersionCreateCommand doCreateCommand(DwFeatureVersion version, Object acceptedModel) {
		DwFeatureVersion selectedVersion = (DwFeatureVersion) acceptedModel;
		return new DwFeatureVersionCreateCommand(version, selectedVersion, true);
	}

}
