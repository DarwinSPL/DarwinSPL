package de.darwinspl.feature.graphical.editor.editparts;

import org.eclipse.draw2d.Label;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.jface.viewers.TextCellEditor;

import de.christophseidl.util.gef.util.LabelCellEditorLocator;
import de.christophseidl.util.gef.util.LabelDirectEditManager;
import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.editparts.DwFeatureEditPart;
import de.darwinspl.feature.graphical.base.figures.DwFeatureFigure;
import de.darwinspl.feature.graphical.editor.DwEditorGraphicalEditor;
import de.darwinspl.feature.graphical.editor.figures.DwEditorFeatureFigure;
import de.darwinspl.feature.graphical.editor.policy.DwFeatureComponentEditPolicy;
import de.darwinspl.feature.graphical.editor.policy.DwFeatureDirectEditPolicy;
import de.darwinspl.feature.graphical.editor.policy.DwFeatureLayoutEditPolicy;

public class DwEditorFeatureEditPart extends DwFeatureEditPart {

	public DwEditorFeatureEditPart(DwEditorGraphicalEditor graphicalEditor) {
		super(graphicalEditor);
	}

	@Override
	protected DwEditorFeatureFigure createFigure() {
		DwFeature feature = getModel();
		DwEditorGraphicalEditor graphicalEditor = (DwEditorGraphicalEditor) getGraphicalEditor();
		return new DwEditorFeatureFigure(feature, graphicalEditor);
	}
	
	@Override
	public DwEditorFeatureFigure getFigure() {
		return (DwEditorFeatureFigure) super.getFigure();
	}
	
	@Override
	protected void createEditPolicies() {
		super.createEditPolicies();
		
		installEditPolicy(EditPolicy.LAYOUT_ROLE, new DwFeatureLayoutEditPolicy());
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE, new DwFeatureDirectEditPolicy());
		installEditPolicy(EditPolicy.COMPONENT_ROLE, new DwFeatureComponentEditPolicy());
	}
	
	@Override
	public void performRequest(Request request) {
		System.out.println(request.getType());
		
		if (request.getType() == RequestConstants.REQ_DIRECT_EDIT) {
			performDirectEditing();
		}
		if(request.getType() == RequestConstants.REQ_OPEN) {
			performDirectEditing();
	    }
	}
	
	// TODO here I have to look for double click for selecting a feature in a config
	protected void performDirectEditing() {
		
		DwGraphicalEditor editor = getGraphicalEditor();
		if (editor instanceof DwEditorGraphicalEditor) {
			DwEditorGraphicalEditor dwEditor = (DwEditorGraphicalEditor) editor;
			if (!dwEditor.isEditingActivated()) {
				toggleFeatureSelected();
				
				DwFeature feature = getModel();
				
				if (getFigure().isSelected()) {
					dwEditor.selectFeature(feature);
				} else {
					dwEditor.deselectFeature(feature);
				}
				return;
			}
		}
		
		DwFeatureFigure featureFigure = getFigure();
		Label label = featureFigure.getLabel();
		
		LabelDirectEditManager manager = new LabelDirectEditManager(this, TextCellEditor.class, new LabelCellEditorLocator(label), label);
		
	    manager.show();
	}
	
	public void toggleFeatureSelected() {
		DwFeatureFigure featureFigure = getFigure();
		boolean featureSelected = featureFigure.isSelected();
		setFeatureSelected(!featureSelected);
	}
	
	public void setFeatureSelected(boolean selected) {
		DwFeatureFigure featureFigure = getFigure();
		featureFigure.setSelected(selected);
	}
}
