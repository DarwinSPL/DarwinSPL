package de.darwinspl.feature.graphical.editor.action;

import org.eclipse.gef.commands.Command;

import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.graphical.base.action.DwCommandAction;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.editor.commands.DwFeatureVersionCreateCommand;
import de.darwinspl.feature.graphical.editor.factories.DwVersionFactory;

public abstract class DwAbstractFeatureVersionCreateAction extends DwCommandAction {
	private static final DwVersionFactory versionFactory = new DwVersionFactory();
	
	public DwAbstractFeatureVersionCreateAction(DwGraphicalEditor editor) {
		super(editor);
	}

	@Override
	protected Command createCommand(Object acceptedModel) {
		DwFeatureVersion version = versionFactory.getNewObject();
		return doCreateCommand(version, acceptedModel);
	}
	
	@Override
	protected boolean acceptsSelectedModel(Object selectedModel) {
		if (selectedModel instanceof DwFeatureVersion) {
			return true;
		}
		
		return false;
	}
	
	protected abstract DwFeatureVersionCreateCommand doCreateCommand(DwFeatureVersion version, Object acceptedModel);

}
