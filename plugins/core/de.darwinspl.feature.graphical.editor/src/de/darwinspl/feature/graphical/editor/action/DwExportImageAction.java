package de.darwinspl.feature.graphical.editor.action;

import de.darwinspl.feature.graphical.base.action.DwAction;
import de.darwinspl.feature.graphical.base.editor.DwBasicGraphicalEditorBase;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalViewer;

public class DwExportImageAction extends DwAction {

	public static final String ID = DwExportImageAction.class.getCanonicalName();
	
	public DwExportImageAction(DwGraphicalEditor editor) {
		super(editor);
	}
	
	@Override
	protected String createText() {
		return "Export Image";
	}

	@Override
	protected String createID() {
		return ID;
	}
	
	@Override
	protected String createIconPath() {
		return "icons/ActionExportImage.png";
	}
	
	@Override
	public void run() {
		DwBasicGraphicalEditorBase abstractEditor = getEditor();
		
		if(abstractEditor instanceof DwGraphicalEditor) {
			DwGraphicalEditor editor = (DwGraphicalEditor) abstractEditor;
			DwGraphicalViewer viewer = editor.getViewer();
			viewer.exportImage();

//			IFile imageFile = viewer.exportImage();
//			if(imageFile != null) {
//				IWorkbenchPartSite site = editor.getSite();
//				Shell shell = site.getShell();
//				
//				String title = "Export Image";
//				IPath imageFilePath = imageFile.getFullPath();
//				String message = "Exported image to \"" + imageFilePath + "\".";
//				
//				MessageDialog.openInformation(shell, title, message);
//			}
		}
	}

}
