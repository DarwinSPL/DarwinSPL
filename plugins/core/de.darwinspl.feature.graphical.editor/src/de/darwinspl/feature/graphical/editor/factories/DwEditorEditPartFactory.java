package de.darwinspl.feature.graphical.editor.factories;

import org.eclipse.gef.EditPart;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.graphical.base.factories.DwEditPartFactory;
import de.darwinspl.feature.graphical.editor.DwEditorGraphicalEditor;
import de.darwinspl.feature.graphical.editor.editparts.DwEditorFeatureEditPart;
import de.darwinspl.feature.graphical.editor.editparts.DwEditorFeatureVersionEditPart;
import de.darwinspl.feature.graphical.editor.editparts.DwEditorGroupEditPart;
import de.darwinspl.feature.graphical.editor.editparts.DwEditorTemporalFeatureModelEditPart;

public class DwEditorEditPartFactory extends DwEditPartFactory {

	public DwEditorEditPartFactory(DwEditorGraphicalEditor graphicalEditor) {
		super(graphicalEditor);
	}

	protected EditPart doCreateEditPart(Object model) {
		DwEditorGraphicalEditor graphicalEditor = getGraphicalEditor();
		
		if (model instanceof DwTemporalFeatureModel) {
			return new DwEditorTemporalFeatureModelEditPart(graphicalEditor);
		}
		
		if (model instanceof DwFeature) {
			return new DwEditorFeatureEditPart(graphicalEditor);
		}
		
		if (model instanceof DwGroup) {
			return new DwEditorGroupEditPart(graphicalEditor);
		}
		
		if (model instanceof DwFeatureVersion) {
			return new DwEditorFeatureVersionEditPart(graphicalEditor);
		}
		
		// TODO
//		if(model instanceof DwFeatureAttribute) {
//			return new DwFeatureAttributeEditPart(editor);
//		}
		
		return super.doCreateEditPart(model);
	}

	@Override
	protected DwEditorGraphicalEditor getGraphicalEditor() {
		return (DwEditorGraphicalEditor) super.getGraphicalEditor();
	}

}
