package de.darwinspl.feature.graphical.editor.editparts;

import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.graphical.base.figures.DwTemporalFeatureModelFigure;
import de.darwinspl.feature.graphical.editor.DwEditorGraphicalEditor;

public class DwEditorTemporalFeatureModelFigure extends DwTemporalFeatureModelFigure {

	public DwEditorTemporalFeatureModelFigure(DwTemporalFeatureModel featureModel, DwEditorGraphicalEditor graphicalEditor) {
		super(featureModel, graphicalEditor);
	}

}
