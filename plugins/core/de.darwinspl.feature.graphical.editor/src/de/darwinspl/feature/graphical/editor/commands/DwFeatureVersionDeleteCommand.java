package de.darwinspl.feature.graphical.editor.commands;

import java.util.Date;
import java.util.List;

import org.eclipse.gef.commands.Command;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.temporal.DwTemporalInterval;
import de.darwinspl.temporal.util.DwEvolutionUtil;

public class DwFeatureVersionDeleteCommand extends Command {
	private DwFeatureVersion version;
	
	private DwFeature parentFeature;
	
	private Date date;
	
	private Date oldValidUntil;
	
	private List<DwFeatureVersion> oldSupersedingVersions;
	
	public DwFeatureVersionDeleteCommand(DwFeatureVersion version) {
		this.version = version;
		
		// TODO evolutionize
		date = null;
	}
	
	@Override
	public void execute() {
		parentFeature = version.getFeature();
		
		DwTemporalInterval validVersionInterval = DwEvolutionUtil.getValidIntervalForDate(version, date);

//		oldValidUntil = version.getValidUntil();
		oldValidUntil = validVersionInterval.getTo();

//		version.setValidUntil(date);
		validVersionInterval.setTo(date);
		
//		if((version.getValidSince() == null && version.getValidUntil() == null) || (version.getValidSince() != null && version.getValidUntil() != null && version.getValidSince().equals(version.getValidUntil()))) {
		if((validVersionInterval.getFrom() == null && validVersionInterval.getTo() == null) || (validVersionInterval.getFrom() != null && validVersionInterval.getTo() != null && validVersionInterval.getFrom().equals(validVersionInterval.getTo()))) {
			// Remove version from feature as it is not valid at any point in time.
			
			oldSupersedingVersions = version.getSupersedingVersions();
			DwFeatureVersion supersededVersion = version.getSupersededVersion();
			
			supersededVersion.getSupersedingVersions().addAll(oldSupersedingVersions);
			
			parentFeature.getVersions().remove(version);
		}
	}
	
	@Override
	public void undo() {
//		version.setValidUntil(oldValidUntil);
		DwEvolutionUtil.getValidIntervalForDate(version, date).setTo(oldValidUntil);
		
		if(!parentFeature.getVersions().contains(version)) {
			parentFeature.getVersions().add(version);
			
			version.getSupersedingVersions().addAll(oldSupersedingVersions);
		}
	}

}
