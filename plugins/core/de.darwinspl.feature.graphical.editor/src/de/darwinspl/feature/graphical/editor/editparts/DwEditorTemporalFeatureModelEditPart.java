package de.darwinspl.feature.graphical.editor.editparts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPolicy;

import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.graphical.base.editparts.DwFeatureModelEditPart;
import de.darwinspl.feature.graphical.editor.DwEditorGraphicalEditor;
import de.darwinspl.feature.graphical.editor.policy.DwFeatureModelLayoutEditPolicy;

public class DwEditorTemporalFeatureModelEditPart extends DwFeatureModelEditPart {
	public DwEditorTemporalFeatureModelEditPart(DwEditorGraphicalEditor graphicalEditor) {
		super(graphicalEditor);
	}

	@Override
	protected IFigure createFigure() {
		DwTemporalFeatureModel featureModel = getModel();
		DwEditorGraphicalEditor graphicalEditor = (DwEditorGraphicalEditor) getGraphicalEditor();
		return new DwEditorTemporalFeatureModelFigure(featureModel, graphicalEditor);
	}

	@Override
	protected void createEditPolicies() {
		super.createEditPolicies();
		
		installEditPolicy(EditPolicy.LAYOUT_ROLE, new DwFeatureModelLayoutEditPolicy());
	}

}
