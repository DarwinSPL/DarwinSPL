package de.darwinspl.feature.graphical.editor.commands;

import org.eclipse.gef.commands.Command;

import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupTypeEnum;
import de.darwinspl.feature.evolution.operation.factory.DwSimpleEvolutionOperationFactory;
import de.darwinspl.feature.evolution.operation.interpreter.DwEvolutionOperationInterpreter;
import de.darwinspl.feature.graphical.base.editor.DwBasicGraphicalEditorBase;
import de.darwinspl.feature.operation.DwGroupTypeChangeOperation;

public class DwGroupChangeVariationTypeCommand extends Command {

	DwGroupTypeChangeOperation evoOp;

	public DwGroupChangeVariationTypeCommand(DwGroup group, DwGroupTypeEnum variationType, DwBasicGraphicalEditorBase editor) {
		DwSimpleEvolutionOperationFactory operationFactory = new DwSimpleEvolutionOperationFactory();
		evoOp = operationFactory.createDwGroupTypeChangeOperation(group.getFeatureModel(), group, variationType, editor.getCurrentSelectedDate(), editor.isLastDateSelected());
	}

	@Override
	public void execute() {
		DwEvolutionOperationInterpreter.execute(evoOp);
	}

	@Override
	public void undo() {
		DwEvolutionOperationInterpreter.undo(evoOp);
	}
}
