package de.darwinspl.feature.graphical.editor.policy;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gef.requests.GroupRequest;

import de.darwinspl.common.eclipse.util.DwLogger;
import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupComposition;
import de.darwinspl.feature.graphical.base.dnd.DwDropBetweenTarget;
import de.darwinspl.feature.graphical.base.dnd.DwDropOnTarget;
import de.darwinspl.feature.graphical.base.dnd.DwDropTarget;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalViewer;
import de.darwinspl.feature.graphical.base.editparts.DwFeatureModelEditPart;
import de.darwinspl.feature.graphical.base.layouter.feature.DwFeatureLayouterManager;
import de.darwinspl.feature.graphical.editor.DwEditorGraphicalEditor;
import de.darwinspl.feature.graphical.editor.commands.DwFeatureMoveCommand;
import de.darwinspl.feature.graphical.editor.editparts.DwEditorFeatureEditPart;
import de.darwinspl.temporal.util.DwEvolutionUtil;

public class DwFeatureModelLayoutEditPolicy extends DwAbstractLayoutEditPolicy {

	public DwFeatureModelLayoutEditPolicy() {
		super(true, false);
	}

	/*
	 * 1 = _e4351048-bd81-42d0-8b90-288c4c967403 2 =
	 * _f857b5ce-c1b3-49f2-bd0f-4565751a8bc4 3 =
	 * _ae475374-53c1-4f26-b13e-5039ab4fb254
	 */

	@Override
	protected Command getOrphanChildrenCommand(Request request) {
		DwFeatureModelEditPart editPart = (DwFeatureModelEditPart) getHost();
		editPart.activate();
//		IFigure featureModelFigure = editPart.getFigure();
//
//		DwGraphicalEditor graphicalEditor = editPart.getGraphicalEditor();
//		DwGraphicalViewer graphicalViewer = graphicalEditor.getViewer();
//		
//		//New position should be mouse position in coordinate system of feature model
//		Point newPosition = graphicalEditor.getCurrentMousePosition();
//		featureModelFigure.translateToRelative(newPosition);
//		
//		DwEditorFeatureEditPart featureEditPart = (DwEditorFeatureEditPart) ((GroupRequest) request).getEditParts().get(0);
//		DwFeature feature = featureEditPart.getModel();
//			
//		DwFeatureLayouterManager featureLayouterManager = graphicalEditor.getFeatureLayouterManager();
//		DwDropTarget dropTarget = featureLayouterManager.determineDropTarget(newPosition.x, newPosition.y);
//		
//		if (dropTarget instanceof DwDropOnTarget) {
//			DwDropOnTarget dropOnTarget = (DwDropOnTarget) dropTarget;
//			
//			DwFeature targetFeature = (DwFeature) dropOnTarget.getObject();
//			
//			if (targetFeature != feature) {
//				DwGraphicalEditor editor = editPart.getGraphicalEditor();
//				
//				return new DwFeatureMoveCommand(feature, targetFeature, editor);
//			}
//		}
//
//		graphicalViewer.setDropTargetMarkerVisible(false);
		return null;
	}

	@Override
	protected Command createChangeConstraintCommand(ChangeBoundsRequest request, EditPart child, Object constraint) {
		DwFeatureModelEditPart editPart = (DwFeatureModelEditPart) getHost();
		IFigure featureModelFigure = editPart.getFigure();

		DwGraphicalEditor graphicalEditor = editPart.getGraphicalEditor();
		DwGraphicalViewer graphicalViewer = graphicalEditor.getViewer();

		if (graphicalEditor instanceof DwEditorGraphicalEditor) {
			DwEditorGraphicalEditor editor = (DwEditorGraphicalEditor) graphicalEditor;
			if (!editor.isEditingActivated()) {
				System.out.println("Moving prevented");
				return null;
			}
		}

		// New position should be mouse position in coordinate system of feature model
		Point newPosition = request.getLocation();
		featureModelFigure.translateToRelative(newPosition);

		Object model = child.getModel();

		if (model instanceof DwFeature) {
			DwFeature feature = (DwFeature) model;

			DwFeatureLayouterManager featureLayouterManager = graphicalEditor.getFeatureLayouterManager();
			DwDropTarget dropTarget = featureLayouterManager.determineDropTarget(newPosition.x, newPosition.y);

			if (dropTarget instanceof DwDropBetweenTarget) {
				DwDropBetweenTarget dropBetweenTarget = (DwDropBetweenTarget) dropTarget;

				DwFeature featureToTheLeft = (DwFeature) dropBetweenTarget.getObject1();
				DwFeature featureToTheRight = (DwFeature) dropBetweenTarget.getObject2();

				if (featureToTheLeft != feature && featureToTheRight != feature) {
//					Rectangle leftBounds = featureLayouterManager.getBoundsOfFeature(featureToTheLeft);
//					Rectangle rightBounds = featureLayouterManager.getBoundsOfFeature(featureToTheRight);

//					int offset = 4;
//					int x1 = leftBounds.x + leftBounds.width - 1;
//					int x2 = rightBounds.x;

//					int width = offset;
//					int height = Math.max(leftBounds.height, rightBounds.height) + 2 * offset;
//					int x = x1 + (x2 - x1 - (width - 1)) / 2;
//					int y = Math.min(leftBounds.y, rightBounds.y) - offset;

//					Rectangle bounds = new Rectangle(x, y, width, height);
//					graphicalViewer.setDropTargetMarkerBounds(bounds);
//					graphicalViewer.setDropTargetMarkerVisible(true);

					DwGroupComposition parentGroupRelation = DwEvolutionUtil.getValidTemporalElement(
							featureToTheLeft.getGroupMembership(), graphicalEditor.getCurrentSelectedDate());
					DwGroup parentGroup = parentGroupRelation.getCompositionOf();

					return new DwFeatureMoveCommand(feature, parentGroup, graphicalEditor);
				}
			} else if (dropTarget instanceof DwDropOnTarget) {
//				DwDropOnTarget dropOnTarget = (DwDropOnTarget) dropTarget;
//				
//				DwFeature targetFeature = (DwFeature) dropOnTarget.getObject();
//				
//				if (targetFeature != feature) {
//					DwGraphicalEditor editor = editPart.getGraphicalEditor();
//					
//					return new DwFeatureMoveCommand(feature, targetFeature, editor);
//				}
			} else if (dropTarget != null) {
				DwLogger.logWarning(
						"Unexpected DropTarget in DwFeatureModelLayoutEditPolicy.createChangeConstraintCommand(...): "
								+ dropTarget);
			}
		}

		graphicalViewer.setDropTargetMarkerVisible(false);
		return null;
	}

}
