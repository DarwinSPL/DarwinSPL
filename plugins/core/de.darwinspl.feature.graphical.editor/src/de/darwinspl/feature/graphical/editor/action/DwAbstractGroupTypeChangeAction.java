package de.darwinspl.feature.graphical.editor.action;

import java.util.Date;

import org.eclipse.gef.commands.Command;

import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupTypeEnum;
import de.darwinspl.feature.graphical.base.action.DwCommandAction;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.editor.commands.DwGroupChangeVariationTypeCommand;

public abstract class DwAbstractGroupTypeChangeAction extends DwCommandAction {
	
	public DwAbstractGroupTypeChangeAction(DwGraphicalEditor editor) {
		super(editor);
	}
	
	protected abstract String getGroupTypeText();
	
	@Override
	protected String createText() {
		return "Make Group "+getGroupTypeText();
	}

	@Override
	protected abstract String createID();
	
	@Override
	protected abstract String createIconPath();
	
	@Override
	protected boolean acceptsSelectedModel(Object selectedModel) {
		if (selectedModel instanceof DwGroup) {
			DwGroup group = (DwGroup) selectedModel;
			
			Date date = this.getDateFromEditor();
			if (!checkIfGroupTypeIsAlreadySet(group, date)) {
				return true;
			}
			
		}
		return false;
	}
	
	protected abstract boolean checkIfGroupTypeIsAlreadySet(DwGroup group, Date date);

	@Override
	protected Command createCommand(Object acceptedModel) {
		DwGroup group = (DwGroup) acceptedModel;
		return new DwGroupChangeVariationTypeCommand(group, getTargetGroupTypeEnum(), getEditor());
	}
	
	protected abstract DwGroupTypeEnum getTargetGroupTypeEnum();

}
