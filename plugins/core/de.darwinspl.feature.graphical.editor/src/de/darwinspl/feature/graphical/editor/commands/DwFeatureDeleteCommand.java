package de.darwinspl.feature.graphical.editor.commands;

import org.eclipse.gef.commands.Command;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.evolution.operation.factory.DwSimpleEvolutionOperationFactory;
import de.darwinspl.feature.evolution.operation.interpreter.DwEvolutionOperationInterpreter;
import de.darwinspl.feature.graphical.base.editor.DwBasicGraphicalEditorBase;
import de.darwinspl.feature.operation.DwFeatureDeleteOperation;

public class DwFeatureDeleteCommand extends Command {

	DwFeatureDeleteOperation deleteEvoOp;
	
	public DwFeatureDeleteCommand(DwFeature feature, DwBasicGraphicalEditorBase editor) {
		DwSimpleEvolutionOperationFactory operationFactory = new DwSimpleEvolutionOperationFactory();
		deleteEvoOp = operationFactory.createDwFeatureDeleteOperation(feature.getFeatureModel(), feature, editor.getCurrentSelectedDate(), editor.isLastDateSelected());
	}
	
	@Override
	public void execute() {
		DwEvolutionOperationInterpreter.execute(deleteEvoOp);
	}

	@Override
	public void undo() {
		DwEvolutionOperationInterpreter.undo(deleteEvoOp);
	}

}
