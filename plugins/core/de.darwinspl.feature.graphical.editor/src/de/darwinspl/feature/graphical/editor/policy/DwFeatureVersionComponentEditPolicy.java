package de.darwinspl.feature.graphical.editor.policy;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.ComponentEditPolicy;
import org.eclipse.gef.requests.GroupRequest;

import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.graphical.editor.commands.DwFeatureVersionDeleteCommand;

public class DwFeatureVersionComponentEditPolicy extends ComponentEditPolicy {

	@Override
	protected Command createDeleteCommand(GroupRequest deleteRequest) {
		EditPart editPart = getHost();
		DwFeatureVersion version = (DwFeatureVersion) editPart.getModel();
		
		return new DwFeatureVersionDeleteCommand(version);
	}

}
