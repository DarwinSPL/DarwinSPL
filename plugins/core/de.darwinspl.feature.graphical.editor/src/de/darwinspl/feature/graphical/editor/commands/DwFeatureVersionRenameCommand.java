package de.darwinspl.feature.graphical.editor.commands;

import org.eclipse.gef.commands.Command;

import de.darwinspl.feature.DwFeatureVersion;

public class DwFeatureVersionRenameCommand extends Command {

	private DwFeatureVersion version;
	private String newNumber;
	private String oldNumber;

	public DwFeatureVersionRenameCommand(DwFeatureVersion version, String newNumber) {
		this.version = version;
		this.newNumber = newNumber;
	}
	
	@Override
	public void execute() {
		oldNumber = version.getNumber();
		version.setNumber(newNumber);
	}

	@Override
	public void undo() {
		version.setNumber(oldNumber);
	}
}
