package de.darwinspl.feature.graphical.editor.action;

import java.util.Date;

import org.eclipse.gef.commands.Command;

import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupTypeEnum;
import de.darwinspl.feature.graphical.base.action.DwCommandAction;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.editor.commands.DwGroupChangeVariationTypeCommand;
import de.darwinspl.feature.util.custom.DwFeatureUtil;

public class DwGroupMakeAndAction extends DwCommandAction {
	public static final String ID = DwGroupMakeAndAction.class.getCanonicalName();
	
	public DwGroupMakeAndAction(DwGraphicalEditor editor) {
		super(editor);
	}
	
	@Override
	protected String createText() {
		return "Make Group And";
	}

	@Override
	protected String createID() {
		return ID;
	}
	
	@Override
	protected String createIconPath() {
		return "icons/ActionMakeGroupAnd.png";
	}
	
	@Override
	protected boolean acceptsSelectedModel(Object selectedModel) {
		Date date = this.getDateFromEditor();
		
		if (selectedModel instanceof DwGroup) {
			DwGroup group = (DwGroup) selectedModel;
			
			if (!DwFeatureUtil.isAnd(group, date)) {
				return true;
			}
			
		}
		return false;
	}

	@Override
	protected Command createCommand(Object acceptedModel) {
		DwGroup group = (DwGroup) acceptedModel;
		return new DwGroupChangeVariationTypeCommand(group, DwGroupTypeEnum.AND, getEditor());
	}

}
