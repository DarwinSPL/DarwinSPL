package de.darwinspl.feature.graphical.editor.editparts;

import org.eclipse.draw2d.Label;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.jface.viewers.TextCellEditor;

import de.christophseidl.util.gef.util.LabelCellEditorLocator;
import de.christophseidl.util.gef.util.LabelDirectEditManager;
import de.darwinspl.feature.graphical.base.editparts.DwFeatureVersionEditPart;
import de.darwinspl.feature.graphical.base.figures.DwFeatureVersionFigure;
import de.darwinspl.feature.graphical.editor.DwEditorGraphicalEditor;
import de.darwinspl.feature.graphical.editor.policy.DwFeatureVersionComponentEditPolicy;
import de.darwinspl.feature.graphical.editor.policy.DwFeatureVersionDirectEditPolicy;
import de.darwinspl.feature.graphical.editor.policy.DwFeatureVersionLayoutEditPolicy;

public class DwEditorFeatureVersionEditPart extends DwFeatureVersionEditPart {

	public DwEditorFeatureVersionEditPart(DwEditorGraphicalEditor graphicalEditor) {
		super(graphicalEditor);
	}

	@Override
	protected void createEditPolicies() {
		super.createEditPolicies();
		
		installEditPolicy(EditPolicy.LAYOUT_ROLE, new DwFeatureVersionLayoutEditPolicy());
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE, new DwFeatureVersionDirectEditPolicy());
		installEditPolicy(EditPolicy.COMPONENT_ROLE, new DwFeatureVersionComponentEditPolicy());
	}
	
	@Override
	public void performRequest(Request request) {
		if (request.getType() == RequestConstants.REQ_DIRECT_EDIT) {
			performDirectEditing();
		}
	}
	
	protected void performDirectEditing() {
		DwFeatureVersionFigure figure = getFigure();
		Label label = figure.getLabel();
		
		LabelDirectEditManager manager = new LabelDirectEditManager(this, TextCellEditor.class, new LabelCellEditorLocator(label), label);
		
	    manager.show();
	}
}
