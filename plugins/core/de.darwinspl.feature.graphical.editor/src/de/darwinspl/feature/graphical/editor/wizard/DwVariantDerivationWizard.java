package de.darwinspl.feature.graphical.editor.wizard;

import java.util.Date;

import org.eclipse.core.resources.IFolder;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.configuration.DwConfiguration;
import de.darwinspl.mapping.DwMappingModel;
import de.darwinspl.mapping.util.DwNonExistentFileReferenceException;
import de.darwinspl.variant.DwUnsupportedFileExtensionException;
import de.darwinspl.variant.DwVariantDerivation;
import de.darwinspl.variant.DwVariantDerivationUnsuccessfullException;
import de.darwinspl.variant.DwVariantDeriver;

public class DwVariantDerivationWizard extends Wizard {

	protected DwVariantDerivationModelInputWizardPage modelInputPage;

	private DwTemporalFeatureModel featureModel;
	private DwConfiguration configurationFromEditor;
	private Date date;

	public DwVariantDerivationWizard(DwTemporalFeatureModel featureModel, DwConfiguration configurationFromEditor,
			Date date) {
		super();
		setNeedsProgressMonitor(true);
		this.featureModel = featureModel;
		this.configurationFromEditor = configurationFromEditor;
	}

	@Override
	public String getWindowTitle() {
		return "Derive a variant.";
	}

	@Override
	public void addPages() {
		modelInputPage = new DwVariantDerivationModelInputWizardPage(featureModel);
		addPage(modelInputPage);
	}

	@Override
	public boolean performFinish() {
		DwConfiguration configuration;
		if (modelInputPage.isConfigurationFileLoaded()) {
			configuration = modelInputPage.getConfiguration();
		} else {
			configuration = configurationFromEditor;
		}

		DwMappingModel mappingModel = modelInputPage.getMappingModel();
		IFolder variantTargetFolder = modelInputPage.getVariantTargetFolder();
		DwVariantDeriver variantDeriver = modelInputPage.getVariantDeriver();

		try {
			DwVariantDerivation.deriveVariant(variantDeriver, featureModel, mappingModel, configuration, date,
					variantTargetFolder);
			showSuccessMessage();
		} catch (DwUnsupportedFileExtensionException | DwNonExistentFileReferenceException
				| DwVariantDerivationUnsuccessfullException e) {
			e.printStackTrace();
			showErrorMessage(e);
		}

		return true;
	}

	private void showErrorMessage(Exception e) {
		MessageBox messageBox = new MessageBox(getShell(), SWT.ICON_ERROR | SWT.OK);
		messageBox.setMessage(e.getMessage());
		messageBox.open();
	}
	
	private void showSuccessMessage() {
		MessageBox messageBox = new MessageBox(getShell(), SWT.ICON_WORKING | SWT.OK);
		messageBox.setMessage("Variant sucessfully generated!");
		messageBox.open();
	}

}
