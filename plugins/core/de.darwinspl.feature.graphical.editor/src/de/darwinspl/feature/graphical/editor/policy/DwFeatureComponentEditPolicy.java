package de.darwinspl.feature.graphical.editor.policy;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.ComponentEditPolicy;
import org.eclipse.gef.requests.GroupRequest;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.graphical.editor.DwEditorGraphicalViewer;
import de.darwinspl.feature.graphical.editor.commands.DwFeatureDeleteCommand;
import de.darwinspl.feature.util.custom.DwFeatureUtil;

public class DwFeatureComponentEditPolicy extends ComponentEditPolicy {
	
	@Override
	protected Command createDeleteCommand(GroupRequest deleteRequest) {
		EditPart editPart = getHost();
		DwFeature feature = (DwFeature) editPart.getModel();
		DwEditorGraphicalViewer viewer = (DwEditorGraphicalViewer) editPart.getViewer();
		
		if(DwFeatureUtil.isRootFeature(feature, viewer.getGraphicalEditor().getCurrentSelectedDate()))
			// the selected feature is a root feature
			return null;
		
		return new DwFeatureDeleteCommand(feature, viewer.getGraphicalEditor());
	}

}
