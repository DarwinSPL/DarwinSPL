package de.darwinspl.feature.graphical.editor.policy;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.ChangeBoundsRequest;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.graphical.base.dnd.DwDropOnTarget;
import de.darwinspl.feature.graphical.base.dnd.DwDropTarget;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalViewer;
import de.darwinspl.feature.graphical.base.layouter.feature.DwFeatureLayouterManager;
import de.darwinspl.feature.graphical.editor.DwEditorGraphicalEditor;
import de.darwinspl.feature.graphical.editor.commands.DwFeatureMoveCommand;
import de.darwinspl.feature.graphical.editor.editparts.DwEditorFeatureEditPart;

public class DwFeatureLayoutEditPolicy extends DwAbstractLayoutEditPolicy {

	public DwFeatureLayoutEditPolicy() {
		super(true, true);
	}
	
	@Override
	protected Command getAddCommand(Request request) {
		DwEditorFeatureEditPart editPart = (DwEditorFeatureEditPart) getHost();
		IFigure featureModelFigure = editPart.getFigure();

		DwGraphicalEditor graphicalEditor = editPart.getGraphicalEditor();
		DwGraphicalViewer graphicalViewer = graphicalEditor.getViewer();
		
		if (graphicalEditor instanceof DwEditorGraphicalEditor) {
			DwEditorGraphicalEditor editor = (DwEditorGraphicalEditor) graphicalEditor;
			if (!editor.isEditingActivated()) {
				System.out.println("Moving prevented");
				return null;
			}
		}

		//New position should be mouse position in coordinate system of feature model
		Point mousePosition = ((ChangeBoundsRequest) request).getLocation();
		mousePosition = mousePosition.getTranslated(((ChangeBoundsRequest) request).getMoveDelta().getNegated());
		featureModelFigure.translateToRelative(mousePosition);
			
		DwFeatureLayouterManager featureLayouterManager = graphicalEditor.getFeatureLayouterManager();
		DwDropTarget dropTarget = featureLayouterManager.determineDropTarget(mousePosition.x, mousePosition.y);
		
		DwFeature targetFeature = editPart.getModel();
		
		if (dropTarget instanceof DwDropOnTarget) {
			DwDropOnTarget dropOnTarget = (DwDropOnTarget) dropTarget;
			
			DwFeature featureToBeMoved = (DwFeature) dropOnTarget.getObject();
			
			if (targetFeature != featureToBeMoved) {
				return new DwFeatureMoveCommand(featureToBeMoved, targetFeature, graphicalEditor);
			}
		}

		graphicalViewer.setDropTargetMarkerVisible(false);
		return null;
	}

}
