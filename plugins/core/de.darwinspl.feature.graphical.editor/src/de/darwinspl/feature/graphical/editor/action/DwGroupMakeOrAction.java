package de.darwinspl.feature.graphical.editor.action;

import java.util.Date;

import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupTypeEnum;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.util.custom.DwFeatureUtil;

public class DwGroupMakeOrAction extends DwAbstractGroupTypeChangeAction {
	
	public static final String ID = DwGroupMakeOrAction.class.getCanonicalName();

	public DwGroupMakeOrAction(DwGraphicalEditor editor) {
		super(editor);
	}

	@Override
	protected String getGroupTypeText() {
		return "Or";
	}

	@Override
	protected String createID() {
		return ID;
	}

	@Override
	protected String createIconPath() {
		return "icons/ActionMakeGroupOr.png";
	}

	@Override
	protected boolean checkIfGroupTypeIsAlreadySet(DwGroup group, Date date) {
		return DwFeatureUtil.isOr(group, date);
	}

	@Override
	protected DwGroupTypeEnum getTargetGroupTypeEnum() {
		return DwGroupTypeEnum.OR; 
	}

}
