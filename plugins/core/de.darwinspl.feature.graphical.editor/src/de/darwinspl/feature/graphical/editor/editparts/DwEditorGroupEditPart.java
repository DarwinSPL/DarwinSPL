package de.darwinspl.feature.graphical.editor.editparts;

import org.eclipse.gef.EditPolicy;

import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.editparts.DwGroupEditPart;
import de.darwinspl.feature.graphical.editor.policy.DwGroupComponentEditPolicy;
import de.darwinspl.feature.graphical.editor.policy.DwGroupLayoutEditPolicy;

public class DwEditorGroupEditPart extends DwGroupEditPart {


	public DwEditorGroupEditPart(DwGraphicalEditor graphicalEditor) {
		super(graphicalEditor);
	}

	@Override
	protected void createEditPolicies() {
		super.createEditPolicies();
		
		installEditPolicy(EditPolicy.LAYOUT_ROLE, new DwGroupLayoutEditPolicy());
		installEditPolicy(EditPolicy.COMPONENT_ROLE, new DwGroupComponentEditPolicy());
	}
}
