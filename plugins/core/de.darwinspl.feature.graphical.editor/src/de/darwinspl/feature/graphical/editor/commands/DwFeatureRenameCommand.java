package de.darwinspl.feature.graphical.editor.commands;

import org.eclipse.gef.commands.Command;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.evolution.operation.factory.DwSimpleEvolutionOperationFactory;
import de.darwinspl.feature.evolution.operation.interpreter.DwEvolutionOperationInterpreter;
import de.darwinspl.feature.graphical.base.editor.DwBasicGraphicalEditorBase;
import de.darwinspl.feature.operation.DwFeatureRenameOperation;

public class DwFeatureRenameCommand extends Command {

	DwFeatureRenameOperation evoOp;
	
	public DwFeatureRenameCommand(DwFeature feature, String newName, DwBasicGraphicalEditorBase editor) {
		DwSimpleEvolutionOperationFactory operationFactory = new DwSimpleEvolutionOperationFactory();
		evoOp = operationFactory.createDwFeatureRenameOperation(feature.getFeatureModel(), feature, newName, editor.getCurrentSelectedDate(), editor.isLastDateSelected());
	}

	@Override
	public void execute() {
		DwEvolutionOperationInterpreter.execute(evoOp);
	}

	@Override
	public void undo() {
		DwEvolutionOperationInterpreter.undo(evoOp);
	}
}
