package de.darwinspl.feature.graphical.editor;

import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.gef.ui.actions.GEFActionConstants;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.ui.actions.ActionFactory;

import de.darwinspl.feature.graphical.base.action.DwAction;
import de.darwinspl.feature.graphical.editor.action.DwExportImageAction;
import de.darwinspl.feature.graphical.editor.action.DwFeatureCreateAction;
import de.darwinspl.feature.graphical.editor.action.DwFeatureCreateInNewGroupAction;
import de.darwinspl.feature.graphical.editor.action.DwFeatureMakeMandatoryAction;
import de.darwinspl.feature.graphical.editor.action.DwFeatureMakeOptionalAction;
import de.darwinspl.feature.graphical.editor.action.DwFeatureVersionCreateAction;
import de.darwinspl.feature.graphical.editor.action.DwFeatureVersionCreatePredecessorAction;
import de.darwinspl.feature.graphical.editor.action.DwFeatureVersionCreateSuccessorAction;
import de.darwinspl.feature.graphical.editor.action.DwFeatureVersionCreateSuccessorOnNewBranchAction;
import de.darwinspl.feature.graphical.editor.action.DwCopyIdAction;
import de.darwinspl.feature.graphical.editor.action.DwGroupMakeAlternativeAction;
import de.darwinspl.feature.graphical.editor.action.DwGroupMakeAndAction;
import de.darwinspl.feature.graphical.editor.action.DwGroupMakeOrAction;
import de.darwinspl.feature.graphical.editor.action.DwManageValidityAction;

public class DwEditorContextMenuProvider extends ContextMenuProvider {
	 
    private ActionRegistry actionRegistry;
    private boolean featureModelEditingActive;
 
    public DwEditorContextMenuProvider(EditPartViewer viewer, ActionRegistry actionRegistry) {
        super(viewer);
        this.actionRegistry = actionRegistry;
        featureModelEditingActive = true;
    }
 
    @Override
    public void buildContextMenu(IMenuManager menuManager) {
        GEFActionConstants.addStandardActionGroups(menuManager);
 
        if (featureModelEditingActive) {
        	menuManager.appendToGroup(GEFActionConstants.GROUP_UNDO, actionRegistry.getAction(ActionFactory.UNDO.getId()));
        	menuManager.appendToGroup(GEFActionConstants.GROUP_UNDO, actionRegistry.getAction(ActionFactory.REDO.getId()));
        	
        	
        	menuManager.appendToGroup(GEFActionConstants.GROUP_EDIT, actionRegistry.getAction(DwFeatureCreateAction.ID));
        	menuManager.appendToGroup(GEFActionConstants.GROUP_EDIT, actionRegistry.getAction(DwFeatureCreateInNewGroupAction.ID));
        	menuManager.appendToGroup(GEFActionConstants.GROUP_EDIT, actionRegistry.getAction(DwFeatureMakeOptionalAction.ID));
        	menuManager.appendToGroup(GEFActionConstants.GROUP_EDIT, actionRegistry.getAction(DwFeatureMakeMandatoryAction.ID));
        	menuManager.appendToGroup(GEFActionConstants.GROUP_EDIT, new Separator());
        	
        	// TODO reintegrate versions
//        menuManager.appendToGroup(GEFActionConstants.GROUP_EDIT, actionRegistry.getAction(DwFeatureVersionCreateAction.ID));
//        menuManager.appendToGroup(GEFActionConstants.GROUP_EDIT, actionRegistry.getAction(DwFeatureVersionCreatePredecessorAction.ID));
//        menuManager.appendToGroup(GEFActionConstants.GROUP_EDIT, actionRegistry.getAction(DwFeatureVersionCreateSuccessorAction.ID));
//        menuManager.appendToGroup(GEFActionConstants.GROUP_EDIT, actionRegistry.getAction(DwFeatureVersionCreateSuccessorOnNewBranchAction.ID));
//        menuManager.appendToGroup(GEFActionConstants.GROUP_EDIT, new Separator());
        	
        	menuManager.appendToGroup(GEFActionConstants.GROUP_EDIT, actionRegistry.getAction(DwGroupMakeAndAction.ID));
        	menuManager.appendToGroup(GEFActionConstants.GROUP_EDIT, actionRegistry.getAction(DwGroupMakeOrAction.ID));
        	menuManager.appendToGroup(GEFActionConstants.GROUP_EDIT, actionRegistry.getAction(DwGroupMakeAlternativeAction.ID));
        	menuManager.appendToGroup(GEFActionConstants.GROUP_EDIT, new Separator());
        	
        	// TODO implement manage validity
//        	menuManager.appendToGroup(GEFActionConstants.GROUP_EDIT, actionRegistry.getAction(DwManageValidityAction.ID));
        }
        
        menuManager.appendToGroup(GEFActionConstants.GROUP_EDIT, actionRegistry.getAction(DwCopyIdAction.ID));
        menuManager.appendToGroup(GEFActionConstants.GROUP_EDIT, actionRegistry.getAction(DwExportImageAction.ID));
        
        registerListeners(menuManager);
        updateMenuManagerEnabledState(menuManager);
    }
    
    private void registerListeners(IMenuManager menuManager) {
        menuManager.addMenuListener(new IMenuListener() {
			@Override
			public void menuAboutToShow(IMenuManager menuManager) {
				updateMenuManagerEnabledState(menuManager);
			}
		});
    }
    
    private void updateMenuManagerEnabledState(IMenuManager menuManager) {
		IContributionItem[] items = menuManager.getItems();
		
		if (items != null) {
			for (IContributionItem item : items) {
				if (item instanceof ActionContributionItem) {
					ActionContributionItem actionContributionItem = (ActionContributionItem) item;
					IAction rawAction = actionContributionItem.getAction();
					
					if (rawAction instanceof DwAction) {
						DwAction action = (DwAction) rawAction;
						action.updateEnabledState();
					}
					
				}
			}
		}
    }

	public void setFeatureModelEditingActive(boolean featureModelEditingActivated) {
		this.featureModelEditingActive = featureModelEditingActivated;
	}

}
