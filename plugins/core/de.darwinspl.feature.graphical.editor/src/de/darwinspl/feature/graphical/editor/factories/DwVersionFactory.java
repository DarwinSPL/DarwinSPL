package de.darwinspl.feature.graphical.editor.factories;

import org.eclipse.gef.requests.CreationFactory;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureFactory;
import de.darwinspl.feature.DwFeatureVersion;

public class DwVersionFactory implements CreationFactory {
	private static final String defaultNumber = "NewVersion";
	
	@Override
	public DwFeatureVersion getNewObject() {
		DwFeatureVersion feature = DwFeatureFactory.eINSTANCE.createDwFeatureVersion();
		
		feature.setNumber(defaultNumber);
		
		return feature;
	}

	@Override
	public Object getObjectType() {
		return DwFeature.class;
	}

}
