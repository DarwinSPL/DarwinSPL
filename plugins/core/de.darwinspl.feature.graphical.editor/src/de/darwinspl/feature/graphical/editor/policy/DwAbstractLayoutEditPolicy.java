package de.darwinspl.feature.graphical.editor.policy;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.XYLayoutEditPolicy;
import org.eclipse.gef.requests.CreateRequest;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.graphical.base.editparts.DwAbstractEditPart;
import de.darwinspl.feature.graphical.editor.commands.DwFeatureCreateCommand;
import de.darwinspl.feature.graphical.editor.commands.DwFeatureVersionCreateCommand;

public class DwAbstractLayoutEditPolicy extends XYLayoutEditPolicy {
	private boolean allowFeatureCreation;
	private boolean allowVersionCreation;
	
	
	public DwAbstractLayoutEditPolicy(boolean allowFeatureCreation, boolean allowVersionCreation) {
		this.allowFeatureCreation = allowFeatureCreation;
		this.allowVersionCreation = allowVersionCreation;
	}
	
	@Override
	protected Command getCreateCommand(CreateRequest request) {
		Object newObject = request.getNewObject();
		
		if (allowFeatureCreation && newObject instanceof DwFeature) {
			DwFeature feature = (DwFeature) newObject;

			DwAbstractEditPart editPart = (DwAbstractEditPart) getHost();
			Object parentObject = editPart.getModel();
			
			//TODO make separate commands for creating and adding features
			return new DwFeatureCreateCommand(parentObject, editPart.getGraphicalEditor());
		}
		
		if (allowVersionCreation && newObject instanceof DwFeatureVersion) {
			DwFeatureVersion version = (DwFeatureVersion) newObject;

			DwAbstractEditPart editPart = (DwAbstractEditPart) getHost();
			Object parentObject = editPart.getModel();
			
			return new DwFeatureVersionCreateCommand(version, parentObject);
		}
		
		return null;
	}

	@Override
	protected EditPolicy createChildEditPolicy(EditPart child) {
		return new DwSelectionEditPolicy(child);
	}
}
