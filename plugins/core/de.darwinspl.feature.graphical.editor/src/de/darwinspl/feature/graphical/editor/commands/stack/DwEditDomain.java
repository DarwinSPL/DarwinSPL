package de.darwinspl.feature.graphical.editor.commands.stack;

import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.ui.IEditorPart;

public class DwEditDomain extends DefaultEditDomain {

	public DwEditDomain(IEditorPart editorPart) {
		super(editorPart);
		this.setCommandStack(new DwCommandStack());
	}

}
