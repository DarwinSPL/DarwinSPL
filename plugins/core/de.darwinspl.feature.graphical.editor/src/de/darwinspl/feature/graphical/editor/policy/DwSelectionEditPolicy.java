package de.darwinspl.feature.graphical.editor.policy;

import org.eclipse.draw2d.FigureListener;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.RootEditPart;
import org.eclipse.gef.editpolicies.ResizableEditPolicy;

import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.editparts.DwAbstractEditPart;
import de.darwinspl.feature.graphical.base.editparts.DwFeatureModelEditPart;
import de.darwinspl.feature.graphical.base.editparts.DwRootEditPart;
import de.darwinspl.feature.graphical.base.figures.DwAbstractFigure;
import de.darwinspl.feature.graphical.base.util.DwGraphicalEditorTheme;

public class DwSelectionEditPolicy extends ResizableEditPolicy {
	private RectangleFigure selectionMarker;
	private EditPart editPart;
	
	public DwSelectionEditPolicy(EditPart editPart) {
		this.editPart = editPart;
		
		selectionMarker = createSelectionMarker();
		
		registerListeners();
	}
	
	private void registerListeners() {
		if (editPart instanceof GraphicalEditPart) {
			GraphicalEditPart graphicalEditPart = (GraphicalEditPart) editPart;
			
			IFigure figure = graphicalEditPart.getFigure();
			figure.addFigureListener(new FigureListener() {
				
				@Override
				public void figureMoved(IFigure source) {
					updateSelectionMarker();					
				}
			});
		}
	}
	
	private RectangleFigure createSelectionMarker() {
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		
		RectangleFigure selectionMarker = new RectangleFigure();
		
		selectionMarker.setFill(false);
		selectionMarker.setForegroundColor(theme.getSelectionSecondaryColor());
		selectionMarker.setLineWidth(theme.getLineWidth());
		
		return selectionMarker;
	}
	
	@Override
	protected void showSelection() {
		updateSelectionMarker();
		showSelectionMarker();
	}
	
	@Override
	protected void hideSelection() {
		hideSelectionFigure();
	}
	
	protected void updateSelectionMarker() {
		//TODO: primary and normal
		//TODO: Focus and not
		
		if (editPart instanceof DwAbstractEditPart) {
			DwAbstractEditPart abstractEditPart = (DwAbstractEditPart) editPart;
			IFigure rawFigure = abstractEditPart.getFigure();
			
			if (rawFigure instanceof DwAbstractFigure) {
				DwAbstractFigure figure = (DwAbstractFigure) rawFigure;
				
				Rectangle selectionMarkerBounds = figure.getSelectionMarkerBounds();
				
				DwFeatureModelEditPart featureModelEditPart = abstractEditPart.getFeatureModelEditPart();
				
				if (featureModelEditPart != null) {
					IFigure featureModelFigure = featureModelEditPart.getFigure();
					
					featureModelFigure.translateToRelative(selectionMarkerBounds);
					selectionMarker.setBounds(selectionMarkerBounds);
				}
			}
		}
	}

	protected void showSelectionMarker() {
		IFigure selectionMarkerLayer = getSelectionMarkerLayer();
		selectionMarkerLayer.add(selectionMarker);
	}
	
	protected void hideSelectionFigure() {
		IFigure selectionMarkerLayer = getSelectionMarkerLayer();
		
		if (selectionMarker.getParent() == selectionMarkerLayer) {
			selectionMarkerLayer.remove(selectionMarker);
		}
	}
	
	private IFigure getSelectionMarkerLayer() {
		//Draw the selection behind the actual elements.
		RootEditPart rawRootEditPart = editPart.getRoot();
		
		if (rawRootEditPart instanceof DwRootEditPart) {
			DwRootEditPart rootEditPart = (DwRootEditPart) rawRootEditPart;
			IFigure layer = rootEditPart.getLayer(DwRootEditPart.SELECTION_LAYER);
			return layer;
		}
		
		return null;
	}

}
