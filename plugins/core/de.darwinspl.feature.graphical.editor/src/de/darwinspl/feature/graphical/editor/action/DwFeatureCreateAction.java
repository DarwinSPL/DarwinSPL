package de.darwinspl.feature.graphical.editor.action;

import java.util.Date;

import org.eclipse.gef.commands.Command;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.graphical.base.action.DwCommandAction;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.editor.commands.DwFeatureCreateCommand;
import de.darwinspl.feature.util.custom.DwFeatureUtil;

public class DwFeatureCreateAction extends DwCommandAction {
	
	public static final String ID = DwFeatureCreateAction.class.getCanonicalName();
	
	public DwFeatureCreateAction(DwGraphicalEditor editor) {
		super(editor);
	}

	@Override
	protected String createText() {
		return "Create Feature";
	}
	
	@Override
	protected String createID() {
		return ID;
	}
	
	@Override
	protected String createIconPath() {
		return "icons/ActionCreateFeature.png";
	}
	
	@Override
	protected boolean acceptsSelectedModel(Object selectedModel) {
		if (selectedModel instanceof DwFeature || selectedModel instanceof DwGroup) {
			return true;
		}
		
		if (selectedModel instanceof DwTemporalFeatureModel) {
			DwTemporalFeatureModel featureModel = (DwTemporalFeatureModel) selectedModel;
			
			Date date = this.getDateFromEditor();
			DwFeature rootFeature = DwFeatureUtil.getRootFeature(featureModel, date);
			
			if (rootFeature == null) {
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	protected Command createCommand(Object acceptedModel) {
		return new DwFeatureCreateCommand(acceptedModel, getEditor(), false);
	}
	
	@Override
	public void updateEnabledState() {
		super.updateEnabledState();
	}

}
