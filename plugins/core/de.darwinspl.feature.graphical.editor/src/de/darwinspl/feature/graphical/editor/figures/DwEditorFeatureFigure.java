package de.darwinspl.feature.graphical.editor.figures;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.graphical.base.figures.DwFeatureFigure;
import de.darwinspl.feature.graphical.editor.DwEditorGraphicalEditor;

public class DwEditorFeatureFigure extends DwFeatureFigure {

	public DwEditorFeatureFigure(DwFeature feature, DwEditorGraphicalEditor graphicalEditor) {
		super(feature, graphicalEditor);
	}

}
