package de.darwinspl.feature.graphical.editor.commands;

import org.eclipse.gef.commands.Command;

import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.evolution.operation.factory.DwSimpleEvolutionOperationFactory;
import de.darwinspl.feature.evolution.operation.interpreter.DwEvolutionOperationInterpreter;
import de.darwinspl.feature.graphical.base.editor.DwBasicGraphicalEditorBase;
import de.darwinspl.feature.operation.DwGroupDeleteOperation;

public class DwGroupDeleteCommand extends Command {

	DwGroupDeleteOperation deleteEvoOp;

	public DwGroupDeleteCommand(DwGroup group, DwBasicGraphicalEditorBase editor) {
		DwSimpleEvolutionOperationFactory operationFactory = new DwSimpleEvolutionOperationFactory();
		deleteEvoOp = operationFactory.createDwGroupDeleteOperation(group.getFeatureModel(), group, editor.getCurrentSelectedDate(), editor.isLastDateSelected());
	}

	@Override
	public void execute() {
		DwEvolutionOperationInterpreter.execute(deleteEvoOp);
	}

	@Override
	public void undo() {
		DwEvolutionOperationInterpreter.undo(deleteEvoOp);
	}

}
