package de.darwinspl.feature.graphical.editor.policy;

import org.eclipse.draw2d.Label;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.DirectEditPolicy;
import org.eclipse.gef.requests.DirectEditRequest;
import org.eclipse.jface.viewers.CellEditor;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.editparts.DwFeatureEditPart;
import de.darwinspl.feature.graphical.base.figures.DwFeatureFigure;
import de.darwinspl.feature.graphical.editor.commands.DwFeatureRenameCommand;

public class DwFeatureDirectEditPolicy extends DirectEditPolicy {

	@Override
	protected Command getDirectEditCommand(DirectEditRequest request) {
		DwFeatureEditPart editPart = getHost();
		DwFeature feature = editPart.getModel();
		DwGraphicalEditor editor = editPart.getGraphicalEditor();
		
		CellEditor cellEditor = request.getCellEditor();
		String newName = (String) cellEditor.getValue();
		return new DwFeatureRenameCommand(feature, newName, editor);
	}

	@Override
	protected void showCurrentEditValue(DirectEditRequest request) {
		DwFeatureFigure figure = getHostFigure();
		
		CellEditor cellEditor = request.getCellEditor();
		String currentEditValue = (String) cellEditor.getValue();

		Label label = figure.getLabel();
		label.setText(currentEditValue);
	}

	@Override
	public DwFeatureEditPart getHost() {
		return (DwFeatureEditPart) super.getHost();
	}
	
	@Override
	protected DwFeatureFigure getHostFigure() {
		return (DwFeatureFigure) super.getHostFigure();
	}

}
