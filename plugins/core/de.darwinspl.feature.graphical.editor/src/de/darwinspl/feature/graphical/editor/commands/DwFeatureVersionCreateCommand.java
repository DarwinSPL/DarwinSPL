package de.darwinspl.feature.graphical.editor.commands;

import java.util.Date;

import org.eclipse.gef.commands.Command;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.util.custom.DwFeatureVersionUtil;

public class DwFeatureVersionCreateCommand extends Command {
	private DwFeatureVersion version;
	private Object parentObject;
	
	private boolean wireAndAddAfter;
	private boolean useSameBranch;
	
	public DwFeatureVersionCreateCommand(DwFeatureVersion version, Object parentObject) {
		initialize(version, parentObject, true, true);
	}
	
	public DwFeatureVersionCreateCommand(DwFeatureVersion version, DwFeatureVersion parentVersion, boolean wireAndAddAfter) {
		initialize(version, parentVersion, wireAndAddAfter, true);
	}
	
	public DwFeatureVersionCreateCommand(DwFeatureVersion version, DwFeatureVersion parentVersion, boolean wireAndAddAfter, boolean useNewBranch) {
		initialize(version, parentVersion, wireAndAddAfter, useNewBranch);
	}
	
	private void initialize(DwFeatureVersion version, Object parentObject, boolean wireAndAddAfter, boolean useSameBranch) {
		this.version = version;
		this.parentObject = parentObject;
		this.wireAndAddAfter = wireAndAddAfter;
		this.useSameBranch = useSameBranch;
	}
	
	@Override
	public void execute() {
		if (parentObject instanceof DwFeature) {
			DwFeature parentFeature = (DwFeature) parentObject;
			addToParentFeature(parentFeature);
		}
		
		if (parentObject instanceof DwFeatureVersion) {
			DwFeatureVersion parentVersion = (DwFeatureVersion) parentObject;
			addToParentVersion(parentVersion);
		}
	}

	@Override
	public void undo() {
		DwFeatureVersionUtil.unwireAndRemoveVersion(version);
	}
	
	protected void addToParentFeature(DwFeature parentFeature) {
		// TODO evolutionize
		Date date = new Date();
		
		DwFeatureVersion selectedVersion = DwFeatureVersionUtil.getLastVersionOnMostRecentBranch(parentFeature, date);
		doAddToParentFeature(parentFeature, selectedVersion);
	}
	
	protected void addToParentVersion(DwFeatureVersion parentVersion) {
		DwFeature parentFeature = parentVersion.getFeature();
		doAddToParentFeature(parentFeature, parentVersion);
	}

	protected void doAddToParentFeature(DwFeature parentFeature, DwFeatureVersion selectedVersion) {
		if (selectedVersion == null) {
			DwFeatureVersionUtil.wireVersionAsRoot(version, parentFeature);
		} else {
			if (wireAndAddAfter) {
				if (useSameBranch) {
					DwFeatureVersionUtil.wireVersionAfter(version, selectedVersion);
				} else {
					DwFeatureVersionUtil.wireVersionAfterOnNewBranch(version, selectedVersion);
				}
			} else {
				if (useSameBranch) {
					DwFeatureVersionUtil.wireVersionBefore(version, selectedVersion);
				} else {
					DwFeatureVersionUtil.wireVersionBefore(version, selectedVersion, true);
				}
			}
		}
		
		DwFeatureVersionUtil.addVersion(version, parentFeature);
	}

}
