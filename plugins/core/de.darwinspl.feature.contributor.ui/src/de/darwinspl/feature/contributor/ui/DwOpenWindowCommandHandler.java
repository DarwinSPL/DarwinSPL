package de.darwinspl.feature.contributor.ui;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import de.darwinspl.feature.contributor.preferences.DwContributorPreferencesManager;

public class DwOpenWindowCommandHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		Shell shell = window.getShell();
		
		DwContributorTitleAreaDialog dialog = new DwContributorTitleAreaDialog(shell);
		int result = dialog.open();

		if(result == 0)
			DwContributorPreferencesManager.setContributor(dialog.getName(), dialog.getMail());
		
		return null;
	}
}
