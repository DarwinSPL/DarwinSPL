package de.darwinspl.feature.contributor.ui;

import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import de.christophseidl.util.eclipse.ui.JFaceUtil;
import de.darwinspl.feature.contributor.preferences.DwContributorPreferencesManager;

public class DwContributorTitleAreaDialog extends TitleAreaDialog {

    private Text nameText;
    private Text mailText;

    private String name;
    private String mail;

    public DwContributorTitleAreaDialog(Shell parentShell) {
        super(parentShell);
    }

    @Override
    public void create() {
        super.create();
        

        ImageDescriptor imageDescriptor = JFaceUtil.getImageDescriptorFromClassBundle("icons/ContributorTitle.png", getClass());
        Image headerImage = new Image(Display.getDefault(), imageDescriptor.getImageData(100));
        
        setTitleImage(headerImage);
        getTitleImageLabel().getLayoutData();
        
        setTitle("DarwinSPL Contributor Settings");
        setMessage("Set up your name and e-mail address here.", IMessageProvider.INFORMATION);
        this.getShell().setSize(600,  250);
    }

    @Override
    protected Control createDialogArea(Composite parent) {
        Composite area = (Composite) super.createDialogArea(parent);
        Composite container = new Composite(area, SWT.NONE);
        container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        GridLayout layout = new GridLayout(2, false);
        layout.horizontalSpacing = 10;
        layout.marginWidth = 20;
        layout.marginHeight = 20;
        container.setLayout(layout);

        createName(container);
        createMail(container);

        return area;
    }

    private void createName(Composite container) {
        Label label = new Label(container, SWT.NONE);
        label.setText("Name");

        GridData layoutData = new GridData();
        layoutData.grabExcessHorizontalSpace = true;
        layoutData.horizontalAlignment = GridData.FILL;

        nameText = new Text(container, SWT.BORDER);
        nameText.setLayoutData(layoutData);
        nameText.setText(DwContributorPreferencesManager.getContributorName());
    }

    private void createMail(Composite container) {
        Label label = new Label(container, SWT.NONE);
        label.setText("E-Mail");

        GridData layoutData = new GridData();
        layoutData.grabExcessHorizontalSpace = true;
        layoutData.horizontalAlignment = GridData.FILL;
        
        mailText = new Text(container, SWT.BORDER);
        mailText.setLayoutData(layoutData);
        mailText.setText(DwContributorPreferencesManager.getContributorId());
    }



    @Override
    protected boolean isResizable() {
        return false;
    }

    // save content of the Text fields because they get disposed
    // as soon as the Dialog closes
    private void saveInput() {
        mail = mailText.getText();
        name = nameText.getText();
    }

    @Override
    protected void okPressed() {
        saveInput();
        super.okPressed();
    }

    public String getName() {
        return name;
    }

    public String getMail() {
        return mail;
    }
}