/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.ui;

import org.eclipse.jface.action.IAction;

public class FmecOutlinePageTypeSortingAction extends de.darwinspl.fmec.resource.fmec.ui.AbstractFmecOutlinePageAction {
	
	public FmecOutlinePageTypeSortingAction(de.darwinspl.fmec.resource.fmec.ui.FmecOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Group types", IAction.AS_CHECK_BOX);
		initialize("icons/group_types_icon.gif");
	}
	
	public void runInternal(boolean on) {
		getTreeViewerComparator().setGroupTypes(on);
		getTreeViewer().refresh();
	}
	
}
