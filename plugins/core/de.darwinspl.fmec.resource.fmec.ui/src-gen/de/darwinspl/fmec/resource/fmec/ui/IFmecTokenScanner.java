/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.ui;

import org.eclipse.jface.text.rules.ITokenScanner;

public interface IFmecTokenScanner extends ITokenScanner {
	
	public String getTokenText();
	
}
