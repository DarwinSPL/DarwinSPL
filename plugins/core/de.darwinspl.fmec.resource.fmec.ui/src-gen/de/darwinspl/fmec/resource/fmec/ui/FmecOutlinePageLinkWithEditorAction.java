/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.ui;

import org.eclipse.jface.action.IAction;

public class FmecOutlinePageLinkWithEditorAction extends de.darwinspl.fmec.resource.fmec.ui.AbstractFmecOutlinePageAction {
	
	public FmecOutlinePageLinkWithEditorAction(de.darwinspl.fmec.resource.fmec.ui.FmecOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Link with Editor", IAction.AS_CHECK_BOX);
		initialize("icons/link_with_editor_icon.gif");
	}
	
	public void runInternal(boolean on) {
		getTreeViewer().setLinkWithEditor(on);
	}
	
}
