/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.ui;

import org.eclipse.emf.ecore.EObject;

public class FmecHoverTextProvider implements de.darwinspl.fmec.resource.fmec.IFmecHoverTextProvider {
	
	private de.darwinspl.fmec.resource.fmec.ui.FmecDefaultHoverTextProvider defaultProvider = new de.darwinspl.fmec.resource.fmec.ui.FmecDefaultHoverTextProvider();
	
	public String getHoverText(EObject container, EObject referencedObject) {
		// Set option overrideHoverTextProvider to false and customize this method to
		// obtain custom hover texts.
		return defaultProvider.getHoverText(referencedObject);
	}
	
	public String getHoverText(EObject object) {
		// Set option overrideHoverTextProvider to false and customize this method to
		// obtain custom hover texts.
		return defaultProvider.getHoverText(object);
	}
	
}
