/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.ui;

import java.util.List;

/**
 * A class which can be overridden to customize code completion proposals.
 */
public class FmecProposalPostProcessor {
	
	public List<de.darwinspl.fmec.resource.fmec.ui.FmecCompletionProposal> process(List<de.darwinspl.fmec.resource.fmec.ui.FmecCompletionProposal> proposals) {
		// the default implementation does returns the proposals as they are
		return proposals;
	}
	
}
