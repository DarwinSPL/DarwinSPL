/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.ui;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.widgets.Display;

public abstract class AbstractFmecOutlinePageAction extends Action {
	
	private String preferenceKey = this.getClass().getSimpleName() + ".isChecked";
	
	private de.darwinspl.fmec.resource.fmec.ui.FmecOutlinePageTreeViewer treeViewer;
	
	public AbstractFmecOutlinePageAction(de.darwinspl.fmec.resource.fmec.ui.FmecOutlinePageTreeViewer treeViewer, String text, int style) {
		super(text, style);
		this.treeViewer = treeViewer;
	}
	
	public void initialize(String imagePath) {
		ImageDescriptor descriptor = de.darwinspl.fmec.resource.fmec.ui.FmecImageProvider.INSTANCE.getImageDescriptor(imagePath);
		setDisabledImageDescriptor(descriptor);
		setImageDescriptor(descriptor);
		setHoverImageDescriptor(descriptor);
		boolean checked = de.darwinspl.fmec.resource.fmec.ui.FmecUIPlugin.getDefault().getPreferenceStore().getBoolean(preferenceKey);
		valueChanged(checked, false);
	}
	
	@Override
	public void run() {
		if (keepState()) {
			valueChanged(isChecked(), true);
		} else {
			runBusy(true);
		}
	}
	
	public void runBusy(final boolean on) {
		BusyIndicator.showWhile(Display.getCurrent(), new Runnable() {
			public void run() {
				runInternal(on);
			}
		});
	}
	
	public abstract void runInternal(boolean on);
	
	private void valueChanged(boolean on, boolean store) {
		setChecked(on);
		runBusy(on);
		if (store) {
			de.darwinspl.fmec.resource.fmec.ui.FmecUIPlugin.getDefault().getPreferenceStore().setValue(preferenceKey, on);
		}
	}
	
	public boolean keepState() {
		return true;
	}
	
	public de.darwinspl.fmec.resource.fmec.ui.FmecOutlinePageTreeViewer getTreeViewer() {
		return treeViewer;
	}
	
	public de.darwinspl.fmec.resource.fmec.ui.FmecOutlinePageTreeViewerComparator getTreeViewerComparator() {
		return (de.darwinspl.fmec.resource.fmec.ui.FmecOutlinePageTreeViewerComparator) treeViewer.getComparator();
	}
	
}
