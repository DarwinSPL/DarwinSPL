/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.ui;

import org.eclipse.jface.action.IAction;

public class FmecOutlinePageCollapseAllAction extends de.darwinspl.fmec.resource.fmec.ui.AbstractFmecOutlinePageAction {
	
	public FmecOutlinePageCollapseAllAction(de.darwinspl.fmec.resource.fmec.ui.FmecOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Collapse all", IAction.AS_PUSH_BUTTON);
		initialize("icons/collapse_all_icon.gif");
	}
	
	public void runInternal(boolean on) {
		if (on) {
			getTreeViewer().collapseAll();
		}
	}
	
	public boolean keepState() {
		return false;
	}
	
}
