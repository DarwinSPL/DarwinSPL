/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.ui;

import java.util.Collection;
import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

/**
 * This class can be used to initialize default preference values.
 */
public class FmecPreferenceInitializer extends AbstractPreferenceInitializer {
	
	public void initializeDefaultPreferences() {
		
		initializeDefaultSyntaxHighlighting();
		initializeDefaultBrackets();
		initializeDefaultsContentAssist();
		
		IPreferenceStore store = de.darwinspl.fmec.resource.fmec.ui.FmecUIPlugin.getDefault().getPreferenceStore();
		// Set default value for matching brackets
		store.setDefault(de.darwinspl.fmec.resource.fmec.ui.FmecPreferenceConstants.EDITOR_MATCHING_BRACKETS_COLOR, "192,192,192");
		store.setDefault(de.darwinspl.fmec.resource.fmec.ui.FmecPreferenceConstants.EDITOR_MATCHING_BRACKETS_CHECKBOX, true);
		
	}
	
	protected void initializeDefaultBrackets() {
		IPreferenceStore store = de.darwinspl.fmec.resource.fmec.ui.FmecUIPlugin.getDefault().getPreferenceStore();
		initializeDefaultBrackets(store, new de.darwinspl.fmec.resource.fmec.mopp.FmecMetaInformation());
	}
	
	protected void initializeDefaultBrackets(IPreferenceStore store, de.darwinspl.fmec.resource.fmec.IFmecMetaInformation metaInformation) {
		String languageId = metaInformation.getSyntaxName();
		// set default brackets
		de.darwinspl.fmec.resource.fmec.ui.FmecBracketSet bracketSet = new de.darwinspl.fmec.resource.fmec.ui.FmecBracketSet();
		final Collection<de.darwinspl.fmec.resource.fmec.IFmecBracketPair> bracketPairs = metaInformation.getBracketPairs();
		if (bracketPairs != null) {
			for (de.darwinspl.fmec.resource.fmec.IFmecBracketPair bracketPair : bracketPairs) {
				bracketSet.addBracketPair(bracketPair.getOpeningBracket(), bracketPair.getClosingBracket(), bracketPair.isClosingEnabledInside(), bracketPair.isCloseAfterEnter());
			}
		}
		store.setDefault(languageId + de.darwinspl.fmec.resource.fmec.ui.FmecPreferenceConstants.EDITOR_BRACKETS_SUFFIX, bracketSet.serialize());
	}
	
	public void initializeDefaultSyntaxHighlighting() {
		IPreferenceStore store = de.darwinspl.fmec.resource.fmec.ui.FmecUIPlugin.getDefault().getPreferenceStore();
		initializeDefaultSyntaxHighlighting(store, new de.darwinspl.fmec.resource.fmec.mopp.FmecMetaInformation());
	}
	
	protected void initializeDefaultSyntaxHighlighting(IPreferenceStore store, de.darwinspl.fmec.resource.fmec.mopp.FmecMetaInformation metaInformation) {
		String languageId = metaInformation.getSyntaxName();
		String[] tokenNames = metaInformation.getSyntaxHighlightableTokenNames();
		if (tokenNames == null) {
			return;
		}
		for (int i = 0; i < tokenNames.length; i++) {
			String tokenName = tokenNames[i];
			de.darwinspl.fmec.resource.fmec.IFmecTokenStyle style = metaInformation.getDefaultTokenStyle(tokenName);
			if (style != null) {
				String color = getColorString(style.getColorAsRGB());
				setProperties(store, languageId, tokenName, color, style.isBold(), true, style.isItalic(), style.isStrikethrough(), style.isUnderline());
			} else {
				setProperties(store, languageId, tokenName, "0,0,0", false, false, false, false, false);
			}
		}
	}
	
	private void initializeDefaultsContentAssist() {
		IPreferenceStore store = de.darwinspl.fmec.resource.fmec.ui.FmecUIPlugin.getDefault().getPreferenceStore();
		store.setDefault(de.darwinspl.fmec.resource.fmec.ui.FmecPreferenceConstants.EDITOR_CONTENT_ASSIST_ENABLED, de.darwinspl.fmec.resource.fmec.ui.FmecPreferenceConstants.EDITOR_CONTENT_ASSIST_ENABLED_DEFAULT);
		store.setDefault(de.darwinspl.fmec.resource.fmec.ui.FmecPreferenceConstants.EDITOR_CONTENT_ASSIST_DELAY, de.darwinspl.fmec.resource.fmec.ui.FmecPreferenceConstants.EDITOR_CONTENT_ASSIST_DELAY_DEFAULT);
		store.setDefault(de.darwinspl.fmec.resource.fmec.ui.FmecPreferenceConstants.EDITOR_CONTENT_ASSIST_TRIGGERS, de.darwinspl.fmec.resource.fmec.ui.FmecPreferenceConstants.EDITOR_CONTENT_ASSIST_TRIGGERS_DEFAULT);
	}
	
	protected void setProperties(IPreferenceStore store, String languageID, String tokenName, String color, boolean bold, boolean enable, boolean italic, boolean strikethrough, boolean underline) {
		store.setDefault(de.darwinspl.fmec.resource.fmec.ui.FmecSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, de.darwinspl.fmec.resource.fmec.ui.FmecSyntaxColoringHelper.StyleProperty.BOLD), bold);
		store.setDefault(de.darwinspl.fmec.resource.fmec.ui.FmecSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, de.darwinspl.fmec.resource.fmec.ui.FmecSyntaxColoringHelper.StyleProperty.COLOR), color);
		store.setDefault(de.darwinspl.fmec.resource.fmec.ui.FmecSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, de.darwinspl.fmec.resource.fmec.ui.FmecSyntaxColoringHelper.StyleProperty.ENABLE), enable);
		store.setDefault(de.darwinspl.fmec.resource.fmec.ui.FmecSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, de.darwinspl.fmec.resource.fmec.ui.FmecSyntaxColoringHelper.StyleProperty.ITALIC), italic);
		store.setDefault(de.darwinspl.fmec.resource.fmec.ui.FmecSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, de.darwinspl.fmec.resource.fmec.ui.FmecSyntaxColoringHelper.StyleProperty.STRIKETHROUGH), strikethrough);
		store.setDefault(de.darwinspl.fmec.resource.fmec.ui.FmecSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, de.darwinspl.fmec.resource.fmec.ui.FmecSyntaxColoringHelper.StyleProperty.UNDERLINE), underline);
	}
	
	protected String getColorString(int[] colorAsRGB) {
		if (colorAsRGB == null) {
			return "0,0,0";
		}
		if (colorAsRGB.length != 3) {
			return "0,0,0";
		}
		return colorAsRGB[0] + "," +colorAsRGB[1] + ","+ colorAsRGB[2];
	}
	
}

