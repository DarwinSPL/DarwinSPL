/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.ui;


/**
 * This class is only generated for backwards compatiblity. The original contents
 * of this class have been moved to class
 * de.darwinspl.fmec.resource.fmec.mopp.FmecAntlrTokenHelper.
 */
public class FmecAntlrTokenHelper {
	// This class is intentionally left empty.
}
