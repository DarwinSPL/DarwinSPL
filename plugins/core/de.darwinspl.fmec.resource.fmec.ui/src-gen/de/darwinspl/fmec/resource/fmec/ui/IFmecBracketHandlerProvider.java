/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.ui;


/**
 * A provider for BracketHandler objects.
 */
public interface IFmecBracketHandlerProvider {
	
	/**
	 * Returns the bracket handler.
	 */
	public de.darwinspl.fmec.resource.fmec.ui.IFmecBracketHandler getBracketHandler();
	
}
