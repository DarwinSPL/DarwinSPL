/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.ui;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;

/**
 * An adapter from the Eclipse
 * <code>org.eclipse.jface.text.rules.ITokenScanner</code> interface to the
 * generated lexer.
 */
public class FmecTokenScanner implements de.darwinspl.fmec.resource.fmec.ui.IFmecTokenScanner {
	
	private de.darwinspl.fmec.resource.fmec.IFmecTextScanner lexer;
	private de.darwinspl.fmec.resource.fmec.IFmecTextToken currentToken;
	private List<de.darwinspl.fmec.resource.fmec.IFmecTextToken> nextTokens;
	private int offset;
	private String languageId;
	private IPreferenceStore store;
	private de.darwinspl.fmec.resource.fmec.ui.FmecColorManager colorManager;
	private de.darwinspl.fmec.resource.fmec.IFmecTextResource resource;
	
	/**
	 * <p>
	 * Creates a new FmecTokenScanner. Uses the preference store belonging to the
	 * corresponding de.darwinspl.fmec.resource.fmec.ui.FmecUIPlugin.
	 * </p>
	 * 
	 * @param resource The resource to scan
	 * @param colorManager A manager to obtain color objects
	 */
	public FmecTokenScanner(de.darwinspl.fmec.resource.fmec.IFmecTextResource resource, de.darwinspl.fmec.resource.fmec.ui.FmecColorManager colorManager) {
		this(resource, colorManager, (de.darwinspl.fmec.resource.fmec.ui.FmecUIPlugin.getDefault() == null ? null : de.darwinspl.fmec.resource.fmec.ui.FmecUIPlugin.getDefault().getPreferenceStore()));
	}
	
	/**
	 * <p>
	 * Creates a new FmecTokenScanner.
	 * </p>
	 * 
	 * @param resource The resource to scan
	 * @param colorManager A manager to obtain color objects
	 * @param preferenceStore The preference store to retrieve the defined token colors
	 */
	public FmecTokenScanner(de.darwinspl.fmec.resource.fmec.IFmecTextResource resource, de.darwinspl.fmec.resource.fmec.ui.FmecColorManager colorManager, IPreferenceStore preferenceStore) {
		this.resource = resource;
		this.colorManager = colorManager;
		this.lexer = new de.darwinspl.fmec.resource.fmec.mopp.FmecMetaInformation().createLexer();
		this.languageId = new de.darwinspl.fmec.resource.fmec.mopp.FmecMetaInformation().getSyntaxName();
		this.store = preferenceStore;
		this.nextTokens = new ArrayList<de.darwinspl.fmec.resource.fmec.IFmecTextToken>();
	}
	
	public int getTokenLength() {
		return currentToken.getLength();
	}
	
	public int getTokenOffset() {
		return offset + currentToken.getOffset();
	}
	
	public IToken nextToken() {
		boolean isOriginalToken = true;
		if (!nextTokens.isEmpty()) {
			currentToken = nextTokens.remove(0);
			isOriginalToken = false;
		} else {
			currentToken = lexer.getNextToken();
		}
		if (currentToken == null || !currentToken.canBeUsedForSyntaxHighlighting()) {
			return Token.EOF;
		}
		
		if (isOriginalToken) {
			splitCurrentToken();
		}
		
		TextAttribute textAttribute = null;
		String tokenName = currentToken.getName();
		if (tokenName != null) {
			de.darwinspl.fmec.resource.fmec.IFmecTokenStyle staticStyle = getStaticTokenStyle();
			// now call dynamic token styler to allow to apply modifications to the static
			// style
			de.darwinspl.fmec.resource.fmec.IFmecTokenStyle dynamicStyle = getDynamicTokenStyle(staticStyle);
			if (dynamicStyle != null) {
				textAttribute = getTextAttribute(dynamicStyle);
			}
		}
		
		return new Token(textAttribute);
	}
	
	public void setRange(IDocument document, int offset, int length) {
		this.offset = offset;
		try {
			lexer.setText(document.get(offset, length));
		} catch (BadLocationException e) {
			// ignore this error. It might occur during editing when locations are outdated
			// quickly.
		}
	}
	
	public String getTokenText() {
		return currentToken.getText();
	}
	
	public int[] convertToIntArray(RGB rgb) {
		if (rgb == null) {
			return null;
		}
		return new int[] {rgb.red, rgb.green, rgb.blue};
	}
	
	public de.darwinspl.fmec.resource.fmec.IFmecTokenStyle getStaticTokenStyle() {
		String tokenName = currentToken.getName();
		String enableKey = de.darwinspl.fmec.resource.fmec.ui.FmecSyntaxColoringHelper.getPreferenceKey(languageId, tokenName, de.darwinspl.fmec.resource.fmec.ui.FmecSyntaxColoringHelper.StyleProperty.ENABLE);
		if (store == null) {
			return null;
		}
		
		boolean enabled = store.getBoolean(enableKey);
		if (!enabled) {
			return null;
		}
		
		String colorKey = de.darwinspl.fmec.resource.fmec.ui.FmecSyntaxColoringHelper.getPreferenceKey(languageId, tokenName, de.darwinspl.fmec.resource.fmec.ui.FmecSyntaxColoringHelper.StyleProperty.COLOR);
		RGB foregroundRGB = PreferenceConverter.getColor(store, colorKey);
		RGB backgroundRGB = null;
		boolean bold = store.getBoolean(de.darwinspl.fmec.resource.fmec.ui.FmecSyntaxColoringHelper.getPreferenceKey(languageId, tokenName, de.darwinspl.fmec.resource.fmec.ui.FmecSyntaxColoringHelper.StyleProperty.BOLD));
		boolean italic = store.getBoolean(de.darwinspl.fmec.resource.fmec.ui.FmecSyntaxColoringHelper.getPreferenceKey(languageId, tokenName, de.darwinspl.fmec.resource.fmec.ui.FmecSyntaxColoringHelper.StyleProperty.ITALIC));
		boolean strikethrough = store.getBoolean(de.darwinspl.fmec.resource.fmec.ui.FmecSyntaxColoringHelper.getPreferenceKey(languageId, tokenName, de.darwinspl.fmec.resource.fmec.ui.FmecSyntaxColoringHelper.StyleProperty.STRIKETHROUGH));
		boolean underline = store.getBoolean(de.darwinspl.fmec.resource.fmec.ui.FmecSyntaxColoringHelper.getPreferenceKey(languageId, tokenName, de.darwinspl.fmec.resource.fmec.ui.FmecSyntaxColoringHelper.StyleProperty.UNDERLINE));
		return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(convertToIntArray(foregroundRGB), convertToIntArray(backgroundRGB), bold, italic, strikethrough, underline);
	}
	
	public de.darwinspl.fmec.resource.fmec.IFmecTokenStyle getDynamicTokenStyle(de.darwinspl.fmec.resource.fmec.IFmecTokenStyle staticStyle) {
		de.darwinspl.fmec.resource.fmec.mopp.FmecDynamicTokenStyler dynamicTokenStyler = new de.darwinspl.fmec.resource.fmec.mopp.FmecDynamicTokenStyler();
		dynamicTokenStyler.setOffset(offset);
		de.darwinspl.fmec.resource.fmec.IFmecTokenStyle dynamicStyle = dynamicTokenStyler.getDynamicTokenStyle(resource, currentToken, staticStyle);
		return dynamicStyle;
	}
	
	public TextAttribute getTextAttribute(de.darwinspl.fmec.resource.fmec.IFmecTokenStyle tokeStyle) {
		int[] foregroundColorArray = tokeStyle.getColorAsRGB();
		Color foregroundColor = null;
		if (colorManager != null) {
			foregroundColor = colorManager.getColor(new RGB(foregroundColorArray[0], foregroundColorArray[1], foregroundColorArray[2]));
		}
		int[] backgroundColorArray = tokeStyle.getBackgroundColorAsRGB();
		Color backgroundColor = null;
		if (backgroundColorArray != null) {
			RGB backgroundRGB = new RGB(backgroundColorArray[0], backgroundColorArray[1], backgroundColorArray[2]);
			if (colorManager != null) {
				backgroundColor = colorManager.getColor(backgroundRGB);
			}
		}
		int style = SWT.NORMAL;
		if (tokeStyle.isBold()) {
			style = style | SWT.BOLD;
		}
		if (tokeStyle.isItalic()) {
			style = style | SWT.ITALIC;
		}
		if (tokeStyle.isStrikethrough()) {
			style = style | TextAttribute.STRIKETHROUGH;
		}
		if (tokeStyle.isUnderline()) {
			style = style | TextAttribute.UNDERLINE;
		}
		return new TextAttribute(foregroundColor, backgroundColor, style);
	}
	
	/**
	 * Tries to split the current token if it contains task items.
	 */
	public void splitCurrentToken() {
		final String text = currentToken.getText();
		final String name = currentToken.getName();
		final int line = currentToken.getLine();
		final int charStart = currentToken.getOffset();
		final int column = currentToken.getColumn();
		
		List<de.darwinspl.fmec.resource.fmec.mopp.FmecTaskItem> taskItems = new de.darwinspl.fmec.resource.fmec.mopp.FmecTaskItemDetector().findTaskItems(text, line, charStart);
		
		// this is the offset for the next token to be added
		int offset = charStart;
		int itemBeginRelative;
		List<de.darwinspl.fmec.resource.fmec.IFmecTextToken> newItems = new ArrayList<de.darwinspl.fmec.resource.fmec.IFmecTextToken>();
		for (de.darwinspl.fmec.resource.fmec.mopp.FmecTaskItem taskItem : taskItems) {
			int itemBegin = taskItem.getCharStart();
			int itemLine = taskItem.getLine();
			int itemColumn = 0;
			
			itemBeginRelative = itemBegin - charStart;
			// create token before task item
			String textBefore = text.substring(offset - charStart, itemBeginRelative);
			int textBeforeLength = textBefore.length();
			newItems.add(new de.darwinspl.fmec.resource.fmec.mopp.FmecTextToken(name, textBefore, offset, textBeforeLength, line, column, true));
			
			// create token for the task item itself
			offset = offset + textBeforeLength;
			String itemText = taskItem.getKeyword();
			int itemTextLength = itemText.length();
			newItems.add(new de.darwinspl.fmec.resource.fmec.mopp.FmecTextToken(de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyleInformationProvider.TASK_ITEM_TOKEN_NAME, itemText, offset, itemTextLength, itemLine, itemColumn, true));
			
			offset = offset + itemTextLength;
		}
		
		if (!taskItems.isEmpty()) {
			// create token after last task item
			String textAfter = text.substring(offset - charStart);
			newItems.add(new de.darwinspl.fmec.resource.fmec.mopp.FmecTextToken(name, textAfter, offset, textAfter.length(), line, column, true));
		}
		
		if (!newItems.isEmpty()) {
			// replace tokens
			currentToken = newItems.remove(0);
			nextTokens = newItems;
		}
		
	}
}
