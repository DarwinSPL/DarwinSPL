/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.ui;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.jface.action.IAction;

public class FmecOutlinePageActionProvider {
	
	public List<IAction> getActions(de.darwinspl.fmec.resource.fmec.ui.FmecOutlinePageTreeViewer treeViewer) {
		// To add custom actions to the outline view, set the
		// 'overrideOutlinePageActionProvider' option to <code>false</code> and modify
		// this method.
		List<IAction> defaultActions = new ArrayList<IAction>();
		defaultActions.add(new de.darwinspl.fmec.resource.fmec.ui.FmecOutlinePageLinkWithEditorAction(treeViewer));
		defaultActions.add(new de.darwinspl.fmec.resource.fmec.ui.FmecOutlinePageCollapseAllAction(treeViewer));
		defaultActions.add(new de.darwinspl.fmec.resource.fmec.ui.FmecOutlinePageExpandAllAction(treeViewer));
		defaultActions.add(new de.darwinspl.fmec.resource.fmec.ui.FmecOutlinePageAutoExpandAction(treeViewer));
		defaultActions.add(new de.darwinspl.fmec.resource.fmec.ui.FmecOutlinePageLexicalSortingAction(treeViewer));
		defaultActions.add(new de.darwinspl.fmec.resource.fmec.ui.FmecOutlinePageTypeSortingAction(treeViewer));
		return defaultActions;
	}
	
}
