/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.ui;


/**
 * The preference page to set the occurrence highlighting is not used anymore.
 * This empty class is only generated to override old existing preference page
 * code to avoid compile errors.
 */
public class FmecOccurrencePreferencePage {
}
