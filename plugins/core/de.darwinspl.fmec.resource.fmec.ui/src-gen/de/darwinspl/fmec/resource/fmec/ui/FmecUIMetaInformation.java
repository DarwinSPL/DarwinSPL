/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.ui;

import org.eclipse.core.resources.IResource;

public class FmecUIMetaInformation extends de.darwinspl.fmec.resource.fmec.mopp.FmecMetaInformation {
	
	public de.darwinspl.fmec.resource.fmec.IFmecHoverTextProvider getHoverTextProvider() {
		return new de.darwinspl.fmec.resource.fmec.ui.FmecHoverTextProvider();
	}
	
	public de.darwinspl.fmec.resource.fmec.ui.FmecImageProvider getImageProvider() {
		return de.darwinspl.fmec.resource.fmec.ui.FmecImageProvider.INSTANCE;
	}
	
	public de.darwinspl.fmec.resource.fmec.ui.FmecColorManager createColorManager() {
		return new de.darwinspl.fmec.resource.fmec.ui.FmecColorManager();
	}
	
	/**
	 * @deprecated this method is only provided to preserve API compatibility. Use
	 * createTokenScanner(de.darwinspl.fmec.resource.fmec.IFmecTextResource,
	 * de.darwinspl.fmec.resource.fmec.ui.FmecColorManager) instead.
	 */
	public de.darwinspl.fmec.resource.fmec.ui.FmecTokenScanner createTokenScanner(de.darwinspl.fmec.resource.fmec.ui.FmecColorManager colorManager) {
		return (de.darwinspl.fmec.resource.fmec.ui.FmecTokenScanner) createTokenScanner(null, colorManager);
	}
	
	public de.darwinspl.fmec.resource.fmec.ui.IFmecTokenScanner createTokenScanner(de.darwinspl.fmec.resource.fmec.IFmecTextResource resource, de.darwinspl.fmec.resource.fmec.ui.FmecColorManager colorManager) {
		return new de.darwinspl.fmec.resource.fmec.ui.FmecTokenScanner(resource, colorManager);
	}
	
	public de.darwinspl.fmec.resource.fmec.ui.FmecCodeCompletionHelper createCodeCompletionHelper() {
		return new de.darwinspl.fmec.resource.fmec.ui.FmecCodeCompletionHelper();
	}
	
	@SuppressWarnings("rawtypes")
	public Object createResourceAdapter(Object adaptableObject, Class adapterType, IResource resource) {
		return new de.darwinspl.fmec.resource.fmec.ui.debug.FmecLineBreakpointAdapter();
	}
	
}
