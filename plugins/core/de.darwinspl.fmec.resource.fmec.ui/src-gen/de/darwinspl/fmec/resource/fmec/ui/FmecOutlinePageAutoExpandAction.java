/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.ui;

import org.eclipse.jface.action.IAction;

public class FmecOutlinePageAutoExpandAction extends de.darwinspl.fmec.resource.fmec.ui.AbstractFmecOutlinePageAction {
	
	public FmecOutlinePageAutoExpandAction(de.darwinspl.fmec.resource.fmec.ui.FmecOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Auto expand", IAction.AS_CHECK_BOX);
		initialize("icons/auto_expand_icon.gif");
	}
	
	public void runInternal(boolean on) {
		getTreeViewer().setAutoExpand(on);
		getTreeViewer().refresh();
	}
	
}
