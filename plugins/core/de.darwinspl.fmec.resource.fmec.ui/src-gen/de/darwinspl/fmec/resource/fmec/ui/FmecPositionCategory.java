/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.ui;


/**
 * An enumeration of all position categories.
 */
public enum FmecPositionCategory {
	BRACKET, DEFINITION, PROXY;
}
