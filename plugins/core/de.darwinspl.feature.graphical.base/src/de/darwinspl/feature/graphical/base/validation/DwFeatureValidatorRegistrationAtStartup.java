package de.darwinspl.feature.graphical.base.validation;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.ui.IStartup;

import de.christophseidl.util.ecore.validation.adapter.ValidatorAdapter;
import de.darwinspl.feature.DwFeaturePackage;

public class DwFeatureValidatorRegistrationAtStartup implements IStartup {

	@Override
	public void earlyStartup() {
		EPackage ePackage = DwFeaturePackage.eINSTANCE;
		EValidator.Registry.INSTANCE.put(ePackage, new ValidatorAdapter());
	}

}
