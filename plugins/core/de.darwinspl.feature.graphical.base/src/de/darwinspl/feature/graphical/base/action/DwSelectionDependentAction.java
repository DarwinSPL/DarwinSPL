package de.darwinspl.feature.graphical.base.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.RootEditPart;

import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalViewer;
import de.darwinspl.feature.graphical.base.editparts.DwFeatureModelEditPart;

public abstract class DwSelectionDependentAction extends DwAction {
	
	private boolean allowMultipleSelection;
	
	public DwSelectionDependentAction(DwGraphicalEditor editor) {
		this(editor, false);
	}
	
	public DwSelectionDependentAction(DwGraphicalEditor editor, boolean allowMultipleSelection) {
		super(editor);
		this.allowMultipleSelection = allowMultipleSelection;
		
		updateEnabledState();
	}
	
	@Override
	public void run() {
		List<Object> acceptedModels = getAcceptedModels();
		
		for (Object acceptedModel : acceptedModels) {
			execute(acceptedModel);
			
			if (!allowMultipleSelection) {
				//Only perform once
				return;
			}
		}
	}
	
	protected abstract void execute(Object acceptedModel);
	
	protected List<?> getSelectedEditParts() {
		DwGraphicalEditor editor = getEditor();
		DwGraphicalViewer viewer = editor.getViewer();
		List<?> selectedEditParts = viewer.getSelectedEditParts();
		
		if (selectedEditParts != null && !selectedEditParts.isEmpty()) {
			return selectedEditParts;
		}

		DwFeatureModelEditPart featureModelEditPart = findFeatureModelEditPart();
		
		if (featureModelEditPart != null) {
			return Collections.singletonList(featureModelEditPart);
		}
		
		return null;
	}
	
	private DwFeatureModelEditPart findFeatureModelEditPart() {
		DwGraphicalEditor editor = getEditor();
		DwGraphicalViewer viewer = editor.getViewer();
		RootEditPart rootEditPart = viewer.getRootEditPart();
		List<?> children = rootEditPart.getChildren();
		
		for (Object child : children) {
			if (child instanceof DwFeatureModelEditPart) {
				return (DwFeatureModelEditPart) child;
			}
		}
		
		return null;
	}
	
	private List<Object> getAcceptedModels() {
		List<Object> acceptedModels = new ArrayList<Object>();
		List<?> editParts = getSelectedEditParts();
		
		for (Object rawEditPart : editParts) {
			if (rawEditPart instanceof EditPart) {
				EditPart editPart = (EditPart) rawEditPart;
				Object selectedModel = editPart.getModel();
				
				if (acceptsSelectedModel(selectedModel)) {
					acceptedModels.add(selectedModel);
				}
			}
		}
		
		return acceptedModels;
	}
	
	protected abstract boolean acceptsSelectedModel(Object selectedModel);

	@Override
	public void updateEnabledState() {
		boolean enabled = determineEnabledState();
		setEnabled(enabled);
	}
	
	protected boolean determineEnabledState() {
		List<?> editParts = getSelectedEditParts();
		
		if (editParts == null || editParts.isEmpty()) {
			return false;
		}
		
		if (!allowMultipleSelection && editParts.size() != 1) {
			return false;
		}
		
		List<Object> acceptedModels = getAcceptedModels();
		
		if (acceptedModels.isEmpty()) {
			return false;
		}
		
		return true;
	}

}
