package de.darwinspl.feature.graphical.base.editparts;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gef.EditPart;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.figures.DwTemporalFeatureModelFigure;
import de.darwinspl.feature.graphical.base.layouter.feature.DwFeatureLayouterManager;
import de.darwinspl.feature.graphical.base.util.DwAbstractAdapter;
import de.darwinspl.feature.util.custom.DwFeatureUtil;

public class DwFeatureModelEditPart extends DwAbstractEditPartWithAdapter {

	public DwFeatureModelEditPart(DwGraphicalEditor graphicalEditor) {
		super(graphicalEditor);
	}

	@Override
	protected DwAbstractAdapter createAdapter(DwAbstractEditPartWithAdapter editPart) {
		return new DwAbstractAdapter(editPart) {
			@Override
			protected void react(Notification notification) {
				refresh();
				refreshVisualsRecursively(DwFeatureModelEditPart.this);
				super.react(notification);
			}
			
			@Override
			protected void notifyFeatureModel() {
				//This IS the feature model, no more notifications.
				//NOTE: Do not call super as it sends out a notification to the feature model,
				//which would cause an infinite loop.
			}
			
			@Override
			public boolean isAdapterForType(Object type) {
				return type.equals(DwTemporalFeatureModel.class);
			}
		};
	}
	

	@Override
	public void refresh() {
		DwGraphicalEditor graphicalEditor = getGraphicalEditor();
		DwFeatureLayouterManager featureLayouterManager = graphicalEditor.getFeatureLayouterManager();
		featureLayouterManager.update();
		
		super.refresh();
	}
	
	protected static void refreshVisualsRecursively(EditPart editPart) {
		if (editPart instanceof DwAbstractEditPart) {
			DwAbstractEditPart abstractEditPart = (DwAbstractEditPart) editPart;
			abstractEditPart.refreshVisuals();
		}
		
		List<?> children = editPart.getChildren();
		
		for (Object child : children) {
			if (child instanceof EditPart) {
				EditPart childEditPart = (EditPart) child;
				refreshVisualsRecursively(childEditPart);
			}
		}
	}
	
	@Override
	protected IFigure createFigure() {
		DwTemporalFeatureModel featureModel = getModel();
		DwGraphicalEditor graphicalEditor = getGraphicalEditor();
		return new DwTemporalFeatureModelFigure(featureModel, graphicalEditor);
	}

	@Override
	public DwTemporalFeatureModel getModel() {
		return (DwTemporalFeatureModel) super.getModel(); 
	}
	
	/**
	 * Called from outside. Responsible for building a list with all model elements. Gathers elements and handles also the initial layout step!
	 * 
	 * @return A list with all model elements.
	 */
	@Override
	protected List<Object> getModelChildren() {
		DwTemporalFeatureModel featureModel = getModel();
		List<Object> modelChildren = new ArrayList<Object>();

		traverseModel(modelChildren, featureModel);
		
		return modelChildren;
	}

	/**
	 * 
	 * @param modelChildren
	 * @param featureModel
	 * @return True if at least one model element (the root feature) is available.
	 */
	private boolean traverseModel(List<Object> modelChildren, DwTemporalFeatureModel featureModel) {
		// TODO make evolution-aware
		modelChildren.clear();
		
		Date date = getGraphicalEditor().getCurrentSelectedDate();
		
		DwFeature rootFeature = DwFeatureUtil.getRootFeature(featureModel, date);

		if (rootFeature == null) {
			return false;
		}

		// get root feature
		modelChildren.add(rootFeature);

		// iterate the groups
		List<DwGroup> groups = DwFeatureUtil.getChildGroupsOfFeature(rootFeature, date);
		
		if (!groups.isEmpty()) {
			// get all groups (have to be listed before features to be drawn behind them)
			collectGroups(modelChildren, rootFeature);
			
			// get all features with versions
			collectFeatures(modelChildren, groups);
		}

		return true;
	}

	
	/**
	 * This method iterates the model elements in order to gather the features. The versions are added separately in the feature.
	 * 
	 * @param modelChildren
	 * @param groups
	 */
	private void collectFeatures(List<Object> modelChildren, List<DwGroup> groups) {
		Date date = getGraphicalEditor().getCurrentSelectedDate();
		
		for (DwGroup group : groups) {
			for (DwFeature feature : DwFeatureUtil.getChildFeaturesOfGroup(group, date)) {
				modelChildren.add(feature);
				collectFeatures(modelChildren, DwFeatureUtil.getChildGroupsOfFeature(feature, date));
			}
		}
	}
	
	/**
	 * This method iterates the tree in order to gather the group types
	 * 
	 * @param modelChildren
	 * @param rootFeature
	 */
	private void collectGroups(List<Object> modelChildren, DwFeature rootFeature) {
		Date date = getGraphicalEditor().getCurrentSelectedDate();
		
		for (DwGroup group : DwFeatureUtil.getChildGroupsOfFeature(rootFeature, date)) {
			//Need ALL groups here - even those that do not have a graphical representation (for marking their bounds).
			modelChildren.add(group);

			for (DwFeature currentFeature : DwFeatureUtil.getChildFeaturesOfGroup(group, date)) {
				collectGroups(modelChildren, currentFeature);
			}
		}
	}

}
