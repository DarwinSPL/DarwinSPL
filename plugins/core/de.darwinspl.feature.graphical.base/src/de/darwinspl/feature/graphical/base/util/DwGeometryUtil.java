package de.darwinspl.feature.graphical.base.util;

import java.awt.geom.Rectangle2D;
import java.util.Date;
import java.util.List;

import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Path;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.figures.DwFeatureFigure;
import de.darwinspl.feature.graphical.base.layouter.feature.DwFeatureLayouterManager;
import de.darwinspl.feature.graphical.base.layouter.version.DwFeatureVersionLayouterManager;
import de.darwinspl.feature.graphical.base.layouter.version.DwFeatureVersionTreeLayouter;
import de.darwinspl.feature.util.custom.DwFeatureUtil;

public class DwGeometryUtil {
	
	public static int calculateFeatureWidth(DwFeature feature, Date date) {
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		int rawFeatureWidth = theme.getFeatureMinimumWidth();
		
		//Label width
		String featureName = DwFeatureUtil.getName(feature, date).getName();
		int labelWidth = DwGeometryUtil.getTextWidth(featureName, theme.getFeatureFont());
		rawFeatureWidth = Math.max(rawFeatureWidth, labelWidth);
		
		//Version tree width
		if (!feature.getVersions().isEmpty()) {
			DwFeatureVersionTreeLayouter versionTreeLayouter = DwFeatureVersionLayouterManager.getLayouter(feature);
			Rectangle versionTreeBounds = versionTreeLayouter.getTreeBounds();
			int versionTreeWidth = versionTreeBounds.width;
			rawFeatureWidth = Math.max(rawFeatureWidth, versionTreeWidth);
		}
		
		return rawFeatureWidth + 2 * theme.getPrimaryMargin();
	}
	
	public static int calculateFeatureHeight(DwFeature feature, Date date) {
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		int versionAreaHeight = calculateVersionAreaHeight(feature, date);
		
		int featureHeight = theme.getFeatureNameAreaHeight() + versionAreaHeight;
		
		if (DwFeatureFigure.variationTypeCircleVisible(feature, date)) {
			featureHeight += theme.getFeatureVariationTypeExtent();
		}
		
		return featureHeight;
	}
	
	private static int calculateVersionAreaHeight(DwFeature feature, Date date) {
		List<DwFeatureVersion> versions = feature.getVersions();
		
		//Empty version areas are not drawn. Hence, they are calculated as height 0.
		if (versions.isEmpty()) {
			return 0;
		}
		
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		
		//Start with default height
		int rawVersionAreaHeight = theme.getFeatureVersionAreaDefaultHeight();
		
		
		//See if individual height is greater
		DwFeatureVersionTreeLayouter versionTreeLayouter = DwFeatureVersionLayouterManager.getLayouter(feature);
		Rectangle versionTreeBounds = versionTreeLayouter.getTreeBounds();
		int versionTreeHeight = versionTreeBounds.height;
		
		rawVersionAreaHeight = Math.max(rawVersionAreaHeight, versionTreeHeight);
		
		
		//See if the height of another version area on that tree level is greater 
		int highestVersionAreaHightOnSameLevel = findHighestVersionAreaHightOnSameLevel(feature, date);
		rawVersionAreaHeight = Math.max(rawVersionAreaHeight, highestVersionAreaHightOnSameLevel);
		
		return rawVersionAreaHeight + 2 * theme.getPrimaryMargin();
	}
	
	private static int findHighestVersionAreaHightOnSameLevel(DwFeature feature, Date date) {
		List<DwFeature> featuresOnSameTreeLevel = DwFeatureUtil.findFeaturesOnSameTreeLevel(feature, date);
		
		int highestVersionAreaHightOnSameLevel = 0;
		
		for (DwFeature featureOnSameTreeLevel : featuresOnSameTreeLevel) {
			List<DwFeatureVersion> versions = featureOnSameTreeLevel.getVersions();
			
			if (!versions.isEmpty()) {
				DwFeatureVersionTreeLayouter versionTreeLayouter = DwFeatureVersionLayouterManager.getLayouter(featureOnSameTreeLevel);
				
				Rectangle versionTreeBounds = versionTreeLayouter.getTreeBounds();
				int versionTreeHeight = versionTreeBounds.height;
				
				highestVersionAreaHightOnSameLevel = Math.max(highestVersionAreaHightOnSameLevel, versionTreeHeight);
			}
		}
		
		return highestVersionAreaHightOnSameLevel;
	}
	
	public static int calculateVersionWidth(DwFeatureVersion version) {
		String versionNumber = version.getNumber();
		return calculateVersionWidth(versionNumber);
	}
	
	public static int calculateVersionWidth(String versionNumber) {
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		
		int labelWidth = DwGeometryUtil.getTextWidth(versionNumber, theme.getVersionFont());
		int triangleWidth = theme.getVersionTriangleEdgeLength();
		return Math.max(labelWidth, triangleWidth) + 2 * theme.getSecondaryMargin();	
	}
	
	public static int calculateVersionHeight(DwFeatureVersion version) {
		String versionNumber = version.getNumber();
		return calculateVersionHeight(versionNumber);
	}
	
	public static int calculateVersionHeight(String versionNumber) {
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		
		int labelHeight = DwGeometryUtil.getTextHeight(versionNumber, theme.getVersionFont());
		int triangleHeight = theme.getVersionTriangleEdgeLength();
		return triangleHeight + theme.getSecondaryMargin() + labelHeight;
	}
	
	
	public static Point getStartCoordinate(DwFeature feature, DwFeatureLayouterManager layouterManager) {
		Rectangle bounds = layouterManager.getBoundsOfFeature(feature);
		
		int x = bounds.x + (bounds.width / 2);
		int y = bounds.y + bounds.height;

		return new Point(x, y);
	}
	
	public static Point getEndCoordinate(DwFeature feature, DwFeatureLayouterManager layouterManager, Date date) {
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		
		Rectangle bounds = layouterManager.getBoundsOfFeature(feature);
		
		int x = bounds.x + (bounds.width / 2);
		int y = bounds.y;
		
		if (DwFeatureFigure.variationTypeCircleVisible(feature, date)) {
			y += theme.getFeatureVariationTypeExtent() / 2;
		}
		
		return new Point(x, y);
	}
	
	public static int getTextWidth(String text, Font font) {
		Label label = new Label();
		
		label.setText(text);
		label.setFont(font);
		
		return label.getPreferredSize().width;
	}

	public static int getTextHeight(String text, Font font) {
		Label label = new Label();
		
		label.setText(text);
		label.setFont(font);

		return label.getPreferredSize().height;
	}
	
	public static double calculateRotationAngleForLineWithEndpoints(Point startPoint, Point endPoint) {
		double deltaX = endPoint.x - startPoint.x;
		double deltaY = endPoint.y - startPoint.y;
		
		return Math.atan2(deltaY, deltaX);
	}
	
	public static Rectangle createBoundsFromPath(Path path, float lineWidth) {
		float[] rawBounds = new float[4];
		path.getBounds(rawBounds);
		return new Rectangle((int) Math.floor(rawBounds[0] - lineWidth / 2f) , (int) Math.floor(rawBounds[1] - lineWidth / 2f), (int) Math.ceil(rawBounds[2] + lineWidth), (int) Math.ceil(rawBounds[3] + lineWidth));
	}
	
	public static Rectangle rectangle2DToDraw2DRectangle(Rectangle2D nodeBounds) {
		return new Rectangle((int) nodeBounds.getX(), (int) nodeBounds.getY(), (int) nodeBounds.getWidth(), (int) nodeBounds.getHeight());
	}
}
