package de.darwinspl.feature.graphical.base.editparts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Rectangle;

import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.figures.DwGroupFigure;
import de.darwinspl.feature.graphical.base.util.DwAbstractAdapter;

public class DwGroupEditPart extends DwAbstractEditPartWithAdapter {

	public DwGroupEditPart(DwGraphicalEditor graphicalEditor) {
		super(graphicalEditor);
	}

	@Override
	protected DwAbstractAdapter createAdapter(DwAbstractEditPartWithAdapter editPart) {
		return new DwAbstractAdapter(editPart) {
			@Override
			public boolean isAdapterForType(Object type) {
				return type.equals(DwGroup.class);
			}
		};
	}

	@Override
	public DwGroup getModel() {
		return (DwGroup) super.getModel();
	}
	
	@Override
	protected IFigure createFigure() {
		DwGroup group = (DwGroup) getModel();
		DwGraphicalEditor graphicalEditor = getGraphicalEditor();
		return new DwGroupFigure(group, graphicalEditor);
	}
	
	@Override
	public DwGroupFigure getFigure() {
		return (DwGroupFigure) super.getFigure();
	}
	
	@Override
	public void refreshVisuals() {
		DwGroupFigure figure = getFigure();
		
		figure.update();
		
		DwFeatureModelEditPart parent = (DwFeatureModelEditPart) getParent();
		Rectangle bounds = figure.getBounds();
		parent.setLayoutConstraint(this, figure, bounds);
	}

}
