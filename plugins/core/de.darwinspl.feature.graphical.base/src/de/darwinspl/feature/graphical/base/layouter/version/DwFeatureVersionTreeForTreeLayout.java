package de.darwinspl.feature.graphical.base.layouter.version;

import java.util.List;

import org.abego.treelayout.util.AbstractTreeForTreeLayout;

import de.darwinspl.feature.DwFeatureVersion;

public class DwFeatureVersionTreeForTreeLayout extends AbstractTreeForTreeLayout<DwFeatureVersion> {


	public DwFeatureVersionTreeForTreeLayout(DwFeatureVersion initialVersion) {
		super(initialVersion);
	}

	@Override
	public List<DwFeatureVersion> getChildrenList(DwFeatureVersion version) {
		return version.getSupersedingVersions();
	}

	@Override
	public DwFeatureVersion getParent(DwFeatureVersion version) {
		return version.getSupersededVersion();
	}
		

}
