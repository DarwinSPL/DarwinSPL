package de.darwinspl.feature.graphical.base.layouter.feature;

import org.abego.treelayout.NodeExtentProvider;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.util.DwGeometryUtil;

public class DwFeatureNodeExtentProvider implements NodeExtentProvider<DwFeature> {
	
	private DwGraphicalEditor editor;
	
	public DwFeatureNodeExtentProvider(DwGraphicalEditor editor) {
		this.editor = editor;
	}
	
	@Override
	public double getWidth(DwFeature feature) {
		return DwGeometryUtil.calculateFeatureWidth(feature, editor.getCurrentSelectedDate());
	}

	@Override
	public double getHeight(DwFeature feature) {
		return DwGeometryUtil.calculateFeatureHeight(feature, editor.getCurrentSelectedDate());
	}

}
