package de.darwinspl.feature.graphical.base.slider;

import java.util.Date;

public interface DwDateChangeListener {

	public void dateChanged(Date newDate, Date oldDate, Object source);
	
}
