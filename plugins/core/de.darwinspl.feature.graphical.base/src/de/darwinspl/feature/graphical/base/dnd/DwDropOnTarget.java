package de.darwinspl.feature.graphical.base.dnd;

public class DwDropOnTarget extends DwDropTarget {
	private Object object;
	
	public DwDropOnTarget(Object object) {
		this.object = object;
	}

	public Object getObject() {
		return object;
	}

}
