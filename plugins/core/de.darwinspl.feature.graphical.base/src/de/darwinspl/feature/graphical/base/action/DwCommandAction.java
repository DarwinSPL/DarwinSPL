package de.darwinspl.feature.graphical.base.action;

import org.eclipse.gef.commands.Command;

import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;

public abstract class DwCommandAction extends DwSelectionDependentAction {
	
	public DwCommandAction(DwGraphicalEditor editor, boolean allowMultipleSelection) {
		super(editor, allowMultipleSelection);
	}

	public DwCommandAction(DwGraphicalEditor editor) {
		super(editor);
	}
	
//	@Override
//	public boolean isEnabled() {
//		return getEditor().isLastDateSelected();
//	}

	@Override
	protected void execute(Object acceptedModel) {
		Command command = createCommand(acceptedModel);
		
		DwGraphicalEditor editor = getEditor();
		editor.executeCommand(command);
	}
	
	protected abstract Command createCommand(Object acceptedModel);

}
