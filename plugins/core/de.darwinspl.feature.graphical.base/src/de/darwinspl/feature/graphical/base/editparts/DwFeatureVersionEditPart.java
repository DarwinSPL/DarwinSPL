package de.darwinspl.feature.graphical.base.editparts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.notify.Notification;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeaturePackage;
import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.figures.DwFeatureVersionFigure;
import de.darwinspl.feature.graphical.base.util.DwAbstractAdapter;

public class DwFeatureVersionEditPart extends DwAbstractEditPartWithAdapter {
	
	public DwFeatureVersionEditPart(DwGraphicalEditor graphicalEditor) {
		super(graphicalEditor);
	}

	@Override
	protected DwAbstractAdapter createAdapter(DwAbstractEditPartWithAdapter editPart) {
		return new DwAbstractAdapter(editPart) {
			
			@Override
			protected void react(Notification notification) {
				if (versionNumberChanged(notification)) {
					//Notify the parent feature of changes, e.g., when a name is changed that extends the version area.
					DwFeatureVersion version = getModel();
					DwFeature feature = version.getFeature();
					fireDummyNotification(feature, version);
				}
				
				super.react(notification);
			}
			
			@Override
			public boolean isAdapterForType(Object type) {
				return type.equals(DwFeatureVersion.class);
			}
		};
	}

	private boolean versionNumberChanged(Notification notification) {
		int eventType = notification.getEventType();
		
		if (eventType == Notification.SET || eventType == Notification.UNSET) {
			if (notification.getFeature() == DwFeaturePackage.eINSTANCE.getDwFeatureVersion_Number()) {
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public DwFeatureVersion getModel() {
		return (DwFeatureVersion) super.getModel();
	}

	@Override
	public DwFeatureVersionFigure getFigure() {
		return (DwFeatureVersionFigure) super.getFigure();
	}

	@Override
	protected IFigure createFigure() {
		DwFeatureVersion version = getModel();
		DwGraphicalEditor graphicalEditor = getGraphicalEditor();
		return new DwFeatureVersionFigure(version, graphicalEditor);
	}

	@Override
	public void refreshVisuals() {
		DwFeatureVersionFigure figure = getFigure();
		figure.update();
	}

}
