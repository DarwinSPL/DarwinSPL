package de.darwinspl.feature.graphical.base.figures;

import java.util.Date;
import java.util.List;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.figures.shapes.DwLineWithArrowShape;
import de.darwinspl.feature.graphical.base.layouter.feature.DwFeatureLayouterManager;
import de.darwinspl.feature.graphical.base.layouter.version.DwFeatureVersionLayouterManager;
import de.darwinspl.feature.graphical.base.layouter.version.DwFeatureVersionTreeLayouter;
import de.darwinspl.feature.graphical.base.util.DwDrawingUtil;
import de.darwinspl.feature.graphical.base.util.DwGeometryUtil;
import de.darwinspl.feature.graphical.base.util.DwGraphicalEditorTheme;
import de.darwinspl.feature.util.custom.DwFeatureUtil;

public class DwFeatureFigure extends DwAbstractFigure {
	private DwFeature feature;

	private Label label;
	
	private boolean selected;
	
	public DwFeatureFigure(DwFeature feature, DwGraphicalEditor graphicalEditor) {
		super(graphicalEditor);
		
		this.feature = feature;

		setLayoutManager(new XYLayout());
		
		createChildFigures();
		update();
	}

	private void createChildFigures() {
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		
		label = new Label();
		label.setFont(theme.getFeatureFont());
		label.setForegroundColor(theme.getFeatureFontColor());
		add(label);
	}

	@Override
	protected boolean useLocalCoordinates() {
		return true;
	}
	
	@Override
	protected Rectangle getEffectiveBounds() {
		Rectangle bounds = getBounds();
		Rectangle effectiveBounds = bounds.getCopy();
		
		if (variationTypeCircleVisible(feature, getGraphicalEditor().getCurrentSelectedDate())) {
			DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
			
			int offset = theme.getFeatureVariationTypeExtent();
			
			effectiveBounds.y += offset;
			effectiveBounds.height -= offset;
		}
		
		return effectiveBounds;
	}
	
	private Rectangle calculateVariationTypeCircleBounds() {
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		Rectangle bounds = getBounds();
		
		int offset = theme.getFeatureVariationTypeExtent();
		
		int x = bounds.x + (bounds.width - offset) / 2;
		int y = bounds.y;
		int width = offset;
		int height = offset;
		
		return new Rectangle(x, y, width, height);
	}
	
	private int getEffectiveVariationTypeExtent() {
		if (variationTypeCircleVisible(feature, getGraphicalEditor().getCurrentSelectedDate())) {
			DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
			return theme.getFeatureVariationTypeExtent();
		}
		
		return 0;
	}
	
	private Rectangle calculateNameAreaBounds() {
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		Rectangle bounds = getBounds();
		Rectangle nameAreaBounds = bounds.getCopy();
		
		nameAreaBounds.y += getEffectiveVariationTypeExtent();
		nameAreaBounds.height = theme.getFeatureNameAreaHeight();
		
		return nameAreaBounds;
	}
	
	private Rectangle calculateVersionAreaBounds() {
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		Rectangle bounds = getBounds();
		Rectangle versionAreaBounds = bounds.getCopy();
		
		int variationTypeAndNameAreaHeight = getEffectiveVariationTypeExtent() + theme.getFeatureNameAreaHeight();
		
		versionAreaBounds.y += variationTypeAndNameAreaHeight;
		versionAreaBounds.height -= variationTypeAndNameAreaHeight;
		
		return versionAreaBounds;
	}
	
	@Override
	public void update() {
		super.update();
		
		updateContent();
		resizeToContent();
		updateLocation();

		//This is necessary to reflect changes in variation type.
		repaint();
	}
	
	private void updateContent() {
		String featureName = DwFeatureUtil.getName(feature, getGraphicalEditor().getCurrentSelectedDate()).getName();
		label.setText(featureName);
	}
	
	private void resizeToContent() {
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		
		Date date = getGraphicalEditor().getCurrentSelectedDate();
		
		int width = DwGeometryUtil.calculateFeatureWidth(feature, date);
		int height = DwGeometryUtil.calculateFeatureHeight(feature, date);
		
		setSize(width, height);
		
		//Label size
		Rectangle nameAreaBounds = calculateNameAreaBounds();
		Dimension preferredLabelSize = label.getPreferredSize();
		
		int labelWidth = nameAreaBounds.width;
		int labelHeight = preferredLabelSize.height;
		int labelX = 0;
		int labelY = getEffectiveVariationTypeExtent() + (theme.getFeatureNameAreaHeight() - labelHeight) / 2;
		
		Rectangle labelBounds = new Rectangle(labelX, labelY, labelWidth, labelHeight);
		label.setBounds(labelBounds);
		setConstraint(label, labelBounds);
	}
	
	protected void updateLocation() {
		DwGraphicalEditor graphicalEditor = getGraphicalEditor();
		DwFeatureLayouterManager layouterManager = graphicalEditor.getFeatureLayouterManager();
		Rectangle boundsOfFeature = layouterManager.getBoundsOfFeature(feature);
		Point location = new Point(boundsOfFeature.x, boundsOfFeature.y);
		setLocation(location);
	}
	
	@Override
	public void paintFigure(Graphics graphics) {
		if (variationTypeCircleVisible(feature, getGraphicalEditor().getCurrentSelectedDate())) {
			paintVariationTypeCircle(graphics);
		}
		
		if (versionAreaIsVisible(feature)) {
			paintVersionAreaBackground(graphics);
			paintVersionConnections(graphics);
		}
		
		paintNameAreaBackground(graphics);
	}
	
	private void paintVariationTypeCircle(Graphics graphics) {
		Date date = getGraphicalEditor().getCurrentSelectedDate();
		
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		
		Color light = feature.isMandatory(date) ? theme.getFeatureMandatoryPrimaryColor() : theme.getFeatureOptionalPrimaryColor();
		Color dark = feature.isMandatory(date) ? theme.getFeatureMandatorySecondaryColor() : theme.getFeatureOptionalSecondaryColor();

		Rectangle variationTypeCircleBounds = calculateVariationTypeCircleBounds();

		//Compensate for line width
		int lineWidth = theme.getLineWidth();
		variationTypeCircleBounds.expand(-lineWidth, -lineWidth);
		
		DwDrawingUtil.gradientFillEllipsis(graphics, variationTypeCircleBounds, light, dark);
		DwDrawingUtil.outlineEllipsis(graphics, variationTypeCircleBounds, theme.getLineColor());
	}
	
	private void paintNameAreaBackground(Graphics graphics) {
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		Rectangle nameAreaBounds = calculateNameAreaBounds();
		
		//Compensate for line width
		int lineWidth = theme.getLineWidth();
		int halfLineWidth = (int) Math.ceil(lineWidth / 2);
		
		nameAreaBounds.x += halfLineWidth;
		nameAreaBounds.width -= lineWidth;
		
		if (!variationTypeCircleVisible(feature, getGraphicalEditor().getCurrentSelectedDate())) {
			nameAreaBounds.y += halfLineWidth;
			nameAreaBounds.height -= lineWidth;
		} else {
			nameAreaBounds.height -= halfLineWidth;
		}
		
		Color backgroundPrimaryColor = theme.getFeatureNameAreaPrimaryColor();
		Color backgroundSecondaryColor = theme.getFeatureNameAreaSecondaryColor();
		if (isSelected()) {
			backgroundPrimaryColor = theme.getSelectionPrimaryColor();
			backgroundSecondaryColor = theme.getSelectionSecondaryColor();
		}
		
		DwDrawingUtil.gradientFillRectangle(graphics, nameAreaBounds, backgroundPrimaryColor, backgroundSecondaryColor);
		DwDrawingUtil.outlineRectangle(graphics, nameAreaBounds, theme.getLineColor());
	}
	
	private void paintVersionAreaBackground(Graphics graphics) {
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		Rectangle versionAreaBounds = calculateVersionAreaBounds();
		
		//Compensate for line width
		int halfLineWidth = (int) Math.ceil(theme.getLineWidth() / 2);
		versionAreaBounds.expand(new Insets(0, -halfLineWidth, -halfLineWidth, -halfLineWidth));
		
		DwDrawingUtil.gradientFillRectangle(graphics, versionAreaBounds, theme.getFeatureVersionAreaPrimaryColor(), theme.getFeatureVersionAreaSecondaryColor());
		
		paintVersionMarks(graphics);
		
		DwDrawingUtil.outlineRectangle(graphics, versionAreaBounds, theme.getLineColor());
	}
	
	protected void paintVersionMarks(Graphics graphics) {
		DwFeature feature = getFeature();
		List<DwFeatureVersion> versions = feature.getVersions();

		DwGraphicalEditor graphicalEditor = getGraphicalEditor();
		
		for (DwFeatureVersion version : versions) {
			Rectangle versionMarkRectangle = getVersionMarkRectangle(version);
			
			boolean versionHasError = graphicalEditor.hasError(version);
			boolean versionHasWarning = graphicalEditor.hasWarning(version);
			
			if (versionHasError) {
				DwDrawingUtil.drawProblem(graphics, versionMarkRectangle, this, true);
			} else if (versionHasWarning) {
				DwDrawingUtil.drawProblem(graphics, versionMarkRectangle, this, false);
			}
		}
	}
	
	protected Rectangle getVersionMarkRectangle(DwFeatureVersion version) {
		DwFeature feature = getFeature();
		DwFeatureVersionTreeLayouter versionTree = DwFeatureVersionLayouterManager.getLayouter(feature);
		Rectangle versionBounds = versionTree.getBounds(version, getGraphicalEditor().getCurrentSelectedDate());
		
		return versionBounds;
	}
	
	private void paintVersionConnections(Graphics graphics) {
		List<DwFeatureVersion> versions = feature.getVersions();
		DwLineWithArrowShape arrow = new DwLineWithArrowShape();
		
		for (DwFeatureVersion version : versions) {
			//From the right of this version to the left of the superseding version
			Point versionPoint = calculateVersionConnectionAnchorRight(version);
			
			List<DwFeatureVersion> predecessors = version.getSupersedingVersions();
			
			for (DwFeatureVersion predecessor : predecessors) {
				Point predecessorPoint = calculateVersionConnectionAnchorLeft(predecessor);
				
				//Line with arrow head
				arrow.setStartPoint(versionPoint);
				arrow.setEndPoint(predecessorPoint);
				arrow.paint(graphics);
			}
		}
	}
	
	private Point calculateVersionConnectionAnchorLeft(DwFeatureVersion version) {
		return doCalculateVersionConnectionAnchorRight(version, false);
	}

	private Point calculateVersionConnectionAnchorRight(DwFeatureVersion version) {
		return doCalculateVersionConnectionAnchorRight(version, true);
	}
	
	private Point doCalculateVersionConnectionAnchorRight(DwFeatureVersion version, boolean right) {
		DwFeature feature = version.getFeature();
		
		if (feature == null) {
			//For robustness. Should not be called in regular operation.
			return new Point(10, 10);
		}
		
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		DwFeatureVersionTreeLayouter versionTree = DwFeatureVersionLayouterManager.getLayouter(feature);

		Rectangle versionBounds = versionTree.getBounds(version, getGraphicalEditor().getCurrentSelectedDate());
		
		int versionX = versionBounds.x;
		
		if (right) {
			versionX += theme.getSecondaryMargin() + versionBounds.width;
		} else {
			versionX -= theme.getSecondaryMargin();
		}
		
		int versionY = versionBounds.y + (theme.getVersionTriangleEdgeLength() / 2);
		
		
		//Offset with bounds as absolute coordinates are required.
		Rectangle bounds = getBounds();
		
		return new Point(bounds.x + versionX, bounds.y + versionY);
	}
	
	public static boolean variationTypeCircleVisible(DwFeature feature, Date date) {
		if (feature == null) {
			return false;
		}
		
		boolean isRoot = !DwFeatureUtil.isRootFeature(feature.getFeatureModel(), feature, date);
		boolean hasStandardVariabilityType = feature.isOptional(date) || feature.isMandatory(date);
		
		DwGroup group = DwFeatureUtil.getParentGroupOfFeature(feature, date);
		
		boolean isPartOfPaintedStandardGroup = false;
		boolean isOnlyFeatureInGroup = false;
		if(group != null) {
			isPartOfPaintedStandardGroup = (group.isAlternative(date) || group.isOr(date));
			isOnlyFeatureInGroup = (DwFeatureUtil.getChildFeaturesOfGroup(group, date).size() == 1);
		}
		
		boolean shouldBeHidden = isPartOfPaintedStandardGroup && !isOnlyFeatureInGroup && feature.isOptional(date);
		
		return (isRoot && hasStandardVariabilityType && !shouldBeHidden);
	}
	
	public static boolean versionAreaIsVisible(DwFeature feature) {
		List<DwFeatureVersion> versions = feature.getVersions();
		return !versions.isEmpty();
	}

	protected DwFeature getFeature() {
		return feature;
	}

	public Label getLabel() {
		return label;
	}
	
	public void setSelected(boolean selected) {
		this.selected = selected;
		update();
	}
	
	public boolean isSelected() {
		return this.selected;
	}
}
