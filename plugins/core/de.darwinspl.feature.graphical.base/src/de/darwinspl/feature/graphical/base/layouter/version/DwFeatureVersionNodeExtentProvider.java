package de.darwinspl.feature.graphical.base.layouter.version;

import org.abego.treelayout.NodeExtentProvider;

import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.graphical.base.util.DwGeometryUtil;

public class DwFeatureVersionNodeExtentProvider implements NodeExtentProvider<DwFeatureVersion> {

	@Override
	public double getWidth(DwFeatureVersion version) {
		return DwGeometryUtil.calculateVersionWidth(version);
	}
	
	@Override
	public double getHeight(DwFeatureVersion version) {
		return DwGeometryUtil.calculateVersionHeight(version);
	}

}
