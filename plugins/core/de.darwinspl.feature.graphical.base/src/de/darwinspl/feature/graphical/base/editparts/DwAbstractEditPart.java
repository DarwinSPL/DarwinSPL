package de.darwinspl.feature.graphical.base.editparts;

import java.util.Observable;
import java.util.Observer;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertySource;

import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.properties.sources.DwTemporalElementPropertySource;
import de.darwinspl.temporal.DwTemporalElement;

public abstract class DwAbstractEditPart extends AbstractGraphicalEditPart implements IPropertySource, Observer {
	
	private DwGraphicalEditor graphicalEditor;
	protected DwTemporalElementPropertySource wrappedPropertySource;
	
	public DwAbstractEditPart(DwGraphicalEditor graphicalEditor) {
		this.graphicalEditor = graphicalEditor;
		this.updatePropertySource();
	}

	@Override
	protected void createEditPolicies() {
	}

	public DwGraphicalEditor getGraphicalEditor() {
		return graphicalEditor;
	}
	
	//NOTE: Need to expose this for external adapter, which cannot be refactored due to type check and the limits of type erasure in generics.
	@Override
	public void refreshVisuals() {
		super.refreshVisuals();
	}
	
	public DwFeatureModelEditPart getFeatureModelEditPart() {
		if (this instanceof DwFeatureModelEditPart) {
			return (DwFeatureModelEditPart) this;
		}
		
		EditPart parent = getParent();
		
		if (parent instanceof DwAbstractEditPart) {
			DwAbstractEditPart parentAbstractEditPart = (DwAbstractEditPart) parent;
			
			return parentAbstractEditPart.getFeatureModelEditPart();
		}
		
		return null;
	}
	
	// IPropertySource methods
	
	public void updatePropertySource() {
		this.setPropertySource(this.getModel());
	}
	
	public void setPropertySource(Object e) {
		if(e instanceof DwTemporalElement)
			this.wrappedPropertySource = new DwTemporalElementPropertySource((DwTemporalElement) e, this.graphicalEditor.getCurrentSelectedDate(), this.graphicalEditor.isLastDateSelected());
		else
			this.wrappedPropertySource = new DwTemporalElementPropertySource(null, this.graphicalEditor.getCurrentSelectedDate(), this.graphicalEditor.isLastDateSelected());
		
		this.wrappedPropertySource.addObserver(this);
	}
	
	@Override
	public Object getEditableValue() {
		return wrappedPropertySource.getEditableValue();
	}

	@Override
	public IPropertyDescriptor[] getPropertyDescriptors() {
		return wrappedPropertySource.getPropertyDescriptors();
	}

	@Override
	public Object getPropertyValue(Object id) {
		return wrappedPropertySource.getPropertyValue(id);
	}

	@Override
	public boolean isPropertySet(Object id) {
		return wrappedPropertySource.isPropertySet(id);
	}

	@Override
	public void resetPropertyValue(Object id) {
		wrappedPropertySource.resetPropertyValue(id);
	}

	@Override
	public void setPropertyValue(Object id, Object value) {
		wrappedPropertySource.setPropertyValue(id, value);
	}
	
	@Override
	public void update(Observable obs, Object arg) {
		this.graphicalEditor.getViewer().getDateScale().doUpdate();
	}
	
}
