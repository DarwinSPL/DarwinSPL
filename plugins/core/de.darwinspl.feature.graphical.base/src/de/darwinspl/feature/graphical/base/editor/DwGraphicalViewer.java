package de.darwinspl.feature.graphical.base.editor;

import java.util.Date;

import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.Viewport;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.ui.parts.GraphicalViewerImpl;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.graphical.base.editparts.DwAbstractEditPart;
import de.darwinspl.feature.graphical.base.slider.DwDateChangeListener;
import de.darwinspl.feature.graphical.base.slider.DwGraphicalModelDateScale;
import de.darwinspl.feature.graphical.base.util.DwGraphicalEditorTheme;
import de.darwinspl.feature.graphical.base.util.export.ExportExtensionPointUtil;

public abstract class DwGraphicalViewer extends GraphicalViewerImpl implements DwDateChangeListener, SelectionListener {
	private DwGraphicalEditor editor;
	private FigureCanvas figureCanvas;
	
	private RectangleFigure dropTargetMarker;
	
	protected DwGraphicalModelDateScale dateScale;
	
	protected org.eclipse.swt.widgets.Composite configurationButtonsComposite;
	protected Button configurationModeButton;
	protected Button checkConfigurationValidityButton;
	protected Button deriveVariantButton;
	protected Button saveConfigurationButton;
	protected Button loadConfigurationButton;
	
	private Object lastSelection;
	
	
	public DwGraphicalViewer(DwGraphicalEditor editor) {
		this.editor = editor;
		
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		
		dropTargetMarker = new RectangleFigure();
		dropTargetMarker.setForegroundColor(theme.getLineColor());
		dropTargetMarker.setLineWidth(theme.getLineWidth());
		setDropTargetMarkerBounds(new Rectangle());
		
//		addDropTargetListener(new AbstractTransferDropTargetListener(this) {
//			@Override
//			protected void updateTargetRequest() {
//				System.out.println("DropTargetListener kriegt Request zum Updaten seines Target (WTF?): " + this);
//			}
//
//			@Override
//			public void drop(DropTargetEvent event) {
//				super.drop(event);
//				System.out.println("DropTargetListener will event droppen: " + event);
//				setDropTargetMarkerVisible(false);
//			}
//		});
	}
	
	public void setDropTargetMarkerBounds(Rectangle bounds) {
		dropTargetMarker.setBounds(bounds);
	}
	
	public void setDropTargetMarkerVisible(boolean visible) {
		@SuppressWarnings("deprecation")
		IFigure rootFigure = getRootFigure();
		
		if (visible) {
			rootFigure.add(dropTargetMarker);
		}
		
		if (!visible && dropTargetMarker.getParent() == rootFigure) {
			rootFigure.remove(dropTargetMarker);
		}
	}

	public DwGraphicalEditor getGraphicalEditor() {
		return editor;
	}

	public void exportImage() {
		ExportExtensionPointUtil.handleExport(this);
	}
	
	// Modified
	@Override
	public Control createControl(Composite parent) {
		parent.setBackground(new Color(Display.getDefault(), 255, 255, 255));
		
		{
			dateScale = new DwGraphicalModelDateScale(parent);
			dateScale.addDateChangeListener(this);
			if(editor != null) {
				DwTemporalFeatureModel featureModel = editor.getFeatureModel();
				if(featureModel != null) {
					dateScale.setModel(featureModel);
				}
			}
			GridData dateScaleLayoutData = new GridData(SWT.FILL, SWT.FILL, true, false);
//			dateScaleLayoutData.minimumHeight = 100;
//			dateScaleLayoutData.heightHint = 50;
			dateScale.setLayoutData(dateScaleLayoutData);
		}

		{
			configurationButtonsComposite = new Composite(parent, SWT.NO_FOCUS);
			configurationButtonsComposite.setLayout(new GridLayout(5, false));
			GridData configurationButtonsCompositeLayoutData = new GridData(SWT.FILL, SWT.TOP, true, false);
			configurationButtonsComposite.setLayoutData(configurationButtonsCompositeLayoutData);
			
			configurationModeButton = new Button(configurationButtonsComposite, SWT.TOGGLE);
			configurationModeButton.setText("Configuration Mode");
			GridData configurationModeButtonLayoutData = new GridData(SWT.RIGHT, SWT.TOP, false, false);
			configurationModeButton.setLayoutData(configurationModeButtonLayoutData);
			configurationModeButton.addSelectionListener(this);
			
			checkConfigurationValidityButton = new Button(configurationButtonsComposite, SWT.PUSH);
			checkConfigurationValidityButton.setText("Check Configuration Valditiy");
			GridData checkConfigurationValidityButtonLayoutData = new GridData(SWT.RIGHT, SWT.TOP, false, false);
			checkConfigurationValidityButton.setLayoutData(checkConfigurationValidityButtonLayoutData);
			checkConfigurationValidityButton.addSelectionListener(this);
			checkConfigurationValidityButton.setEnabled(false);
			
			saveConfigurationButton = new Button(configurationButtonsComposite, SWT.PUSH);
			saveConfigurationButton.setText("Save Configuration");
			GridData saveConfigurationButtonLayoutData = new GridData(SWT.RIGHT, SWT.TOP, false, false);
			saveConfigurationButton.setLayoutData(saveConfigurationButtonLayoutData);
			saveConfigurationButton.addSelectionListener(this);
			saveConfigurationButton.setEnabled(false);
			
			loadConfigurationButton = new Button(configurationButtonsComposite, SWT.PUSH);
			loadConfigurationButton.setText("Load Configuration");
			GridData loadConfigurationButtonLayoutData = new GridData(SWT.RIGHT, SWT.TOP, false, false);
			loadConfigurationButton.setLayoutData(loadConfigurationButtonLayoutData);
			loadConfigurationButton.addSelectionListener(this);
			loadConfigurationButton.setEnabled(false);
			
			deriveVariantButton = new Button(configurationButtonsComposite, SWT.PUSH);
			deriveVariantButton.setText("Derive Variant");
			GridData deriveVariantButtonLayoutData = new GridData(SWT.RIGHT, SWT.TOP, false, false);
			deriveVariantButton.setLayoutData(deriveVariantButtonLayoutData);
			deriveVariantButton.addSelectionListener(this);
			deriveVariantButton.setEnabled(false);
		}
		
		{
			createAndSetFigureCanvas(parent);
			
			setControl(figureCanvas);
			
			GridData figureCanvasLayoutData = new GridData(SWT.FILL, SWT.FILL, true, true);
			figureCanvas.setLayoutData(figureCanvasLayoutData);
		}
		

		
		hookRootFigure();
		registerListeners();
		
		parent.pack();
		
		return figureCanvas;
	}
	
	protected void registerListeners() {
//		figureCanvas.addDragDetectListener(new DragDetectListener() {
//			
//			@Override
//			public void dragDetected(DragDetectEvent e) {
//				System.out.println("Da wurde was gedraggt: " + e);
//			}
//		});
	}
	
	/**
	 * Has to call {@link #createAndSetFigureCanvas(Composite) createAndSetFigureCanvas()}.
	 */
//	protected Composite createMainComposite(Composite parent) {
//		Composite composite = new Composite(parent, SWT.NONE);
//		composite.setLayout(new GridLayout(1, true));
//		createAndSetFigureCanvas(composite);
//		return composite;
//	}
	
	protected FigureCanvas createAndSetFigureCanvas(Composite parent) {
		FigureCanvas figureCanvas = createFigureCanvas(parent);
		this.figureCanvas = figureCanvas;
		
		return figureCanvas;
	}
	
	protected FigureCanvas createFigureCanvas(Composite parent) {
		return new FigureCanvas(parent, getLightweightSystem());
	}
	
	// Modified
	public FigureCanvas getFigureCanvas() {
		return figureCanvas;
	}

	// Copied
	@Override
	public void reveal(EditPart part) {
		super.reveal(part);
		Viewport port = getFigureCanvas().getViewport();
		IFigure target = ((GraphicalEditPart) part).getFigure();
		Rectangle exposeRegion = target.getBounds().getCopy();
		target = target.getParent();
		while (target != null && target != port) {
			target.translateToParent(exposeRegion);
			target = target.getParent();
		}
		exposeRegion.expand(5, 5);

		Dimension viewportSize = port.getClientArea().getSize();

		Point topLeft = exposeRegion.getTopLeft();
		Point bottomRight = exposeRegion.getBottomRight().translate(viewportSize.getNegated());
		Point finalLocation = new Point();
		if (viewportSize.width < exposeRegion.width) {
			finalLocation.x = Math.min(bottomRight.x, Math.max(topLeft.x, port.getViewLocation().x));
		} else {
			finalLocation.x = Math.min(topLeft.x, Math.max(bottomRight.x, port.getViewLocation().x));
		}

		if (viewportSize.height < exposeRegion.height) {
			finalLocation.y = Math.min(bottomRight.y, Math.max(topLeft.y, port.getViewLocation().y));
		} else {
			finalLocation.y = Math.min(topLeft.y, Math.max(bottomRight.y, port.getViewLocation().y));
		}

		getFigureCanvas().scrollSmoothTo(finalLocation.x, finalLocation.y);
	}

	// Copied
	@Override
	protected void hookRootFigure() {
		@SuppressWarnings("deprecation")
		IFigure rootFigure = getRootFigure();

		if (getFigureCanvas() == null) {
			return;
		}
		if (rootFigure instanceof Viewport) {
			getFigureCanvas().setViewport((Viewport) rootFigure);
		} else {
			getFigureCanvas().setContents(rootFigure);
		}
	}
	
	public DwGraphicalModelDateScale getDateScale() {
		return dateScale;
	}

	@Override
	public void dateChanged(Date newDate, Date oldDate, Object source) {
		if (this.editor != null && this.editor.getViewer() != null) {
			this.editor.refreshVisuals();
		}
		
		// Update the selection to keep track of a certain feature
		this.setSelection(findUpdatedEditPart());
	}
	
	private ISelection findUpdatedEditPart() {
		if(this.lastSelection != null) {
			for(Object child : this.getContents().getChildren()) {
				if(child instanceof DwAbstractEditPart) {
					if(((DwAbstractEditPart) child).getModel().equals(this.lastSelection)) {
						StructuredSelection updatedSelection = new StructuredSelection(child);
						return updatedSelection;
					}
				}
			}
		}
		return StructuredSelection.EMPTY;
	}

	@Override
	public ISelection getSelection() {
		ISelection currentSelection = super.getSelection();
		if(!currentSelection.isEmpty()) {
			if(currentSelection instanceof StructuredSelection) {
				StructuredSelection currentStructuredSelection = ((StructuredSelection) currentSelection);
				if(selection.size() == 0) {
					if(currentStructuredSelection.getFirstElement() instanceof DwAbstractEditPart) {
						if(((DwAbstractEditPart) currentStructuredSelection.getFirstElement()).getModel() instanceof DwTemporalFeatureModel) {
//							this.lastSelection = null;
							return StructuredSelection.EMPTY;
						}
					}
				}
			}
			
			for(Object s : selection) {
				if(s instanceof DwAbstractEditPart) {
					((DwAbstractEditPart) s).updatePropertySource();
					this.lastSelection = ((DwAbstractEditPart) s).getModel();
				}
			}
		}
		
		return currentSelection;
	}
	
	protected Shell getShell() {
		return PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
	}
	
//	public void widgetSelected(SelectionEvent e) {
//		if(e == null || e.getSource() == null) {
//			return;
//		}
//		else if (e.getSource().equals(configurationModeButton)) {
//			System.out.println("Configuration mode "+configurationModeButton.getSelection());
//		}
////		configurationModeButton.
//	}
//	
//
//	public void widgetDefaultSelected(SelectionEvent e) {
//
////		System.out.println("Widget default selected");
//	}
	
}






