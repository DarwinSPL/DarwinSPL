package de.darwinspl.feature.graphical.base.util.export;

import java.io.IOException;

import org.eclipse.core.resources.IFile;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IFileEditorInput;

import de.christophseidl.util.eclipse.ResourceUtil;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalViewer;

public abstract class DwAbstractImageExporter implements DwImageExporter {
	
	public DwAbstractImageExporter() {
	}
	
	
	
	abstract public void exportImage(DwGraphicalViewer viewer) throws IOException;


	
	protected IFile createImageFileFromEditorInput(DwGraphicalViewer viewer, String extension) {
		DwGraphicalEditor graphicalEditor = viewer.getGraphicalEditor();
		
		IEditorInput input = graphicalEditor.getEditorInput();
		if (input instanceof IFileEditorInput) {
			IFileEditorInput fileInput = (IFileEditorInput) input;
			IFile featureModelFile = fileInput.getFile();
			IFile imageFile = ResourceUtil.deriveFile(featureModelFile, extension);
			return imageFile;
		}
		
		return null;
	}
	
}
