package de.darwinspl.feature.graphical.base.layouter.version;

import java.awt.geom.Rectangle2D;
import java.awt.geom.Rectangle2D.Double;
import java.util.Date;
import java.util.Map;

import org.abego.treelayout.TreeForTreeLayout;
import org.abego.treelayout.TreeLayout;
import org.abego.treelayout.util.DefaultConfiguration;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.figures.DwFeatureFigure;
import de.darwinspl.feature.graphical.base.util.DwGeometryUtil;
import de.darwinspl.feature.graphical.base.util.DwGraphicalEditorTheme;
import de.darwinspl.feature.util.custom.DwFeatureVersionUtil;

public class DwFeatureVersionTreeLayouter {
	private TreeLayout<DwFeatureVersion> treeLayout;
	
	private static final int offsetX = calculateOffsetX();
	private static final int offsetY = calculateOffsetY();
	
	
	private static int calculateOffsetX() {
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		return theme.getPrimaryMargin();
	}
	
	private static int calculateOffsetY() {
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		return theme.getFeatureVariationTypeExtent() + theme.getFeatureNameAreaHeight() + theme.getPrimaryMargin();
	}
	
	public DwFeatureVersionTreeLayouter(DwFeature feature) {
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		DwFeatureVersion initialVersion = DwFeatureVersionUtil.getRootVersion(feature);
		
		TreeForTreeLayout<DwFeatureVersion> tree = new DwFeatureVersionTreeForTreeLayout(initialVersion);
		DwFeatureVersionNodeExtentProvider extentProvider = new DwFeatureVersionNodeExtentProvider();

		DefaultConfiguration<DwFeatureVersion> configuration = new DefaultConfiguration<DwFeatureVersion>(theme.getVersionExtentX(), theme.getVersionExtentY()) {
			@Override
			public Location getRootLocation() {
				return Location.Left;
			}
		};
		
		treeLayout = new TreeLayout<DwFeatureVersion>(tree, extentProvider, configuration);
	}

	/**
	 * @return A point representing the upper left point of the rectangle representing the bounds.
	 */
	public Point getCoordinates(DwFeatureVersion version, Date date) {
		Rectangle2D.Double nodeBounds = getNodeBounds(version, date);
		
		return new Point((int) nodeBounds.getX(), (int) nodeBounds.getY());
	}

	public Rectangle getBounds(DwFeatureVersion version, Date date) {
		Rectangle2D.Double nodeBounds = getNodeBounds(version, date);
		return DwGeometryUtil.rectangle2DToDraw2DRectangle(nodeBounds);
	}
	
	private Rectangle2D.Double adjustNodeBounds(Rectangle2D.Double nodeBounds, DwFeatureVersion version, Date date) {
		Rectangle2D.Double adjustedNodeBounds = (Rectangle2D.Double) nodeBounds.clone();
		
		adjustedNodeBounds.x += offsetX;
		adjustedNodeBounds.y += offsetY;
		
		DwFeature feature = version.getFeature();
		
		if (!DwFeatureFigure.variationTypeCircleVisible(feature, date)) {
			DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
			adjustedNodeBounds.y -= theme.getFeatureVariationTypeExtent();
		}
		
		return adjustedNodeBounds;
	}

	private Rectangle2D.Double getNodeBounds(DwFeatureVersion version, Date date) {
		Map<DwFeatureVersion, Double> elements = treeLayout.getNodeBounds();
		Rectangle2D.Double nodeBounds = elements.get(version);
		
		return adjustNodeBounds(nodeBounds, version, date);
	}
	
	public Rectangle getTreeBounds() {
		Rectangle2D rawBounds = treeLayout.getBounds();
		return DwGeometryUtil.rectangle2DToDraw2DRectangle(rawBounds);
	}

}
