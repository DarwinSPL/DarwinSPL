package de.darwinspl.feature.graphical.base.factories;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.editparts.DwFeatureEditPart;
import de.darwinspl.feature.graphical.base.editparts.DwFeatureModelEditPart;
import de.darwinspl.feature.graphical.base.editparts.DwFeatureVersionEditPart;
import de.darwinspl.feature.graphical.base.editparts.DwGroupEditPart;

public class DwEditPartFactory implements EditPartFactory {

	protected DwGraphicalEditor editor;
	
	public DwEditPartFactory(DwGraphicalEditor editor) {
		this.editor = editor;
	}
	
	@Override
	public EditPart createEditPart(EditPart context, Object model) {
		EditPart part = doCreateEditPart(model);

		if (part != null) {
			part.setModel(model);
		}

		return part;
	}
	
	protected EditPart doCreateEditPart(Object model) {
		if (model instanceof DwTemporalFeatureModel) {
			return new DwFeatureModelEditPart(editor);
		}
		
		if (model instanceof DwFeature) {
			return new DwFeatureEditPart(editor);
		}
		
		if (model instanceof DwGroup) {
			return new DwGroupEditPart(editor);
		}
		
		if (model instanceof DwFeatureVersion) {
			return new DwFeatureVersionEditPart(editor);
		}
		
		// TODO
//		if(model instanceof DwFeatureAttribute) {
//			return new DwFeatureAttributeEditPart(editor);
//		}
		
		return null;
	}

	protected DwGraphicalEditor getGraphicalEditor() {
		return editor;
	}

}
