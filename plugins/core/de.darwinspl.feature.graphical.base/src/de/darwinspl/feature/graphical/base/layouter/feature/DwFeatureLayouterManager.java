package de.darwinspl.feature.graphical.base.layouter.feature;

import java.awt.geom.Rectangle2D;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import org.abego.treelayout.TreeForTreeLayout;
import org.abego.treelayout.TreeLayout;
import org.abego.treelayout.util.DefaultConfiguration;
import org.eclipse.draw2d.geometry.Rectangle;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.graphical.base.dnd.DwDropBetweenTarget;
import de.darwinspl.feature.graphical.base.dnd.DwDropOnTarget;
import de.darwinspl.feature.graphical.base.dnd.DwDropTarget;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.figures.DwFeatureFigure;
import de.darwinspl.feature.graphical.base.util.DwGeometryUtil;
import de.darwinspl.feature.graphical.base.util.DwGraphicalEditorTheme;
import de.darwinspl.feature.util.custom.DwFeatureUtil;

public class DwFeatureLayouterManager {
	private static final int offsetX = calculateOffsetX();
	private static final int offsetY = calculateOffsetY();
	
	protected DwGraphicalEditor editor;
	
	private static int calculateOffsetX() {
		//Margin to ensure selection of features is drawn correctly.
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		return theme.getPrimaryMargin() + 1;
	}
	
	private static int calculateOffsetY() {
		//Root feature never has the variation type circle drawn.
		return 0; //-DEFeatureModelEditorConstants.FEATURE_VARIATION_TYPE_EXTENT;
	}
	
	private DwTemporalFeatureModel featureModel;
	private TreeLayout<DwFeature> treeLayout;

	public DwFeatureLayouterManager(DwTemporalFeatureModel featureModel, DwGraphicalEditor editor) {
		this.featureModel = featureModel;
		this.editor = editor;
		update();
	}
	
	public void update() {
		Date date = editor.getCurrentSelectedDate();
		
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		
		DwFeature rootFeature = DwFeatureUtil.getRootFeature(featureModel, date);
		
		if (rootFeature == null) {
			return;
		}
		
		TreeForTreeLayout<DwFeature> tree = new DwFeatureTreeForTreeLayout(rootFeature, editor);
		DwFeatureNodeExtentProvider extents = new DwFeatureNodeExtentProvider(editor);
		DefaultConfiguration<DwFeature> configuration = new DefaultConfiguration<DwFeature>(theme.getFeatureExtentY(), theme.getFeatureExtentX()) {
			@Override
			public AlignmentInLevel getAlignmentInLevel() {
				return AlignmentInLevel.TowardsRoot;
			}
		};
		
		treeLayout = new TreeLayout<DwFeature>(tree, extents, configuration);
	}
	
	public Rectangle getBoundsOfFeature(DwFeature feature) {
		Rectangle2D.Double nodeBounds = getNodeBounds(feature);
		return DwGeometryUtil.rectangle2DToDraw2DRectangle(nodeBounds);
	}
	
	public Rectangle getBoundsOfModel() {
		Rectangle2D nodeBounds = treeLayout.getBounds();
		return DwGeometryUtil.rectangle2DToDraw2DRectangle(nodeBounds);
	}
	
	private Rectangle2D.Double getNodeBounds(DwFeature feature) {
		Map<DwFeature, Rectangle2D.Double> elements = treeLayout.getNodeBounds();
		Rectangle2D.Double nodeBounds = elements.get(feature);
		
		if (nodeBounds == null) {
			//For newly created elements
			nodeBounds = new Rectangle2D.Double(0, 0, 0, 0);
		}
		
		return adjustNodeBounds(nodeBounds, feature);
	}
	
	public DwDropTarget determineDropTarget(int x, int y) {
		Map<DwFeature, Rectangle2D.Double> nodeBounds = treeLayout.getNodeBounds();
		DwFeature featureOnTheRightSide = null;
		double smallestDistanceToFeatureOnTheRightSide = 0;
		
		DwFeature featureOnTheLeftSide = null;
		double smallestDistanceToFeatureOnTheLeftSide = 0;
		
		for (Entry<DwFeature, Rectangle2D.Double> entry : nodeBounds.entrySet()) {
			DwFeature feature = entry.getKey();
			Rectangle2D.Double nodeBound = entry.getValue();
			
			nodeBound = adjustNodeBounds(nodeBound, feature);
			
			//Check if on same level
			double y1 = nodeBound.getY();
			
			//TODO: Take the max height of this level instead
//			treeLayout.getSizeOfLevel(1);
			double y2 = y1 + nodeBound.getHeight() - 1;
			
			if (y >= y1 && y <= y2) {
				double x1 = nodeBound.x;
				double x2 = x1 + nodeBound.width - 1;
				
				//Check if over feature
				if (x >= x1 && x <= x2) {
					return new DwDropOnTarget(feature);
				}
				
				if (x < x1) {
					double distanceToFeatureOnTheRightSide = x1 - x;
					
					if (featureOnTheRightSide == null || distanceToFeatureOnTheRightSide < smallestDistanceToFeatureOnTheRightSide) {
						featureOnTheRightSide = feature;
						smallestDistanceToFeatureOnTheRightSide = distanceToFeatureOnTheRightSide;
					}
				}

				if (x > x2) {
					double distanceToFeatureOnTheLeftSide = x - x2;
					
					if (featureOnTheLeftSide == null || distanceToFeatureOnTheLeftSide < smallestDistanceToFeatureOnTheLeftSide) {
						featureOnTheLeftSide = feature;
						smallestDistanceToFeatureOnTheLeftSide = distanceToFeatureOnTheLeftSide;
					}
				}
			}
		}
		
		if (featuresOnSidesBelongToSameGroup(featureOnTheLeftSide, featureOnTheRightSide, editor.getCurrentSelectedDate())) {
			return new DwDropBetweenTarget(featureOnTheLeftSide, featureOnTheRightSide);
		}
		
		return null;
	}
	
	private static boolean featuresOnSidesBelongToSameGroup(DwFeature featureOnTheLeftSide, DwFeature featureOnTheRightSide, Date date) {
		if (featureOnTheLeftSide == null || featureOnTheRightSide == null) {
			return false;
		}
		
		DwGroup leftParentGroup = DwFeatureUtil.getParentGroupOfFeature(featureOnTheLeftSide, date);
		DwGroup rightParentGroup = DwFeatureUtil.getParentGroupOfFeature(featureOnTheRightSide, date);
		
		if (leftParentGroup == null || rightParentGroup == null) {
			return false;
		}
		
		return leftParentGroup == rightParentGroup;
	}
	
	private Rectangle2D.Double adjustNodeBounds(Rectangle2D.Double nodeBounds, DwFeature feature) {
		Rectangle2D.Double adjustedNodeBounds = (Rectangle2D.Double) nodeBounds.clone();
		
		adjustedNodeBounds.x += offsetX;
		adjustedNodeBounds.y += offsetY;
		
		if (!DwFeatureFigure.variationTypeCircleVisible(feature, editor.getCurrentSelectedDate())) {
			DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
			adjustedNodeBounds.y += theme.getFeatureVariationTypeExtent();
		}
		
		return adjustedNodeBounds;
	}
}
