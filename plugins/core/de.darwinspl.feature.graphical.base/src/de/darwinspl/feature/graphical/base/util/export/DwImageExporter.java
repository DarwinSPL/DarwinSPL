package de.darwinspl.feature.graphical.base.util.export;

import java.io.IOException;

import org.eclipse.core.resources.IFile;

import de.darwinspl.feature.graphical.base.editor.DwGraphicalViewer;

public interface DwImageExporter {

	public void exportImage(DwGraphicalViewer viewer) throws IOException;
	
}
