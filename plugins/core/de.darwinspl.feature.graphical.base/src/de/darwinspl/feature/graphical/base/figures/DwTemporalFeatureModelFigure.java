package de.darwinspl.feature.graphical.base.figures;

import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.FreeformLayout;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.ecore.EObject;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.figures.DwFeatureFigure;
import de.darwinspl.feature.graphical.base.layouter.feature.DwFeatureLayouterManager;
import de.darwinspl.feature.graphical.base.util.DwDrawingUtil;
import de.darwinspl.feature.graphical.base.util.DwGraphicalEditorTheme;

public class DwTemporalFeatureModelFigure extends FreeformLayer {
	private DwTemporalFeatureModel featureModel;
	private DwGraphicalEditor graphicalEditor;
	
	public DwTemporalFeatureModelFigure(DwTemporalFeatureModel featureModel, DwGraphicalEditor graphicalEditor) {
		this.featureModel = featureModel;
		this.graphicalEditor = graphicalEditor;
		
		setLayoutManager(new FreeformLayout());
	}
	
	@Override
	protected void paintFigure(Graphics graphics) {
//		super.paintFigure(graphics);
//		
//		paintFeatureAndGroupMarks(graphics);
	}
	
	private void paintFeatureAndGroupMarks(Graphics graphics) {
		Iterator<EObject> iterator = featureModel.eAllContents();

		// TODO why on earth is there an iterator that stupidly iterates over all,
		// features, completely ignoring all temporal validities and stuff !?!?!?!?!
		// I am pretty sure this is non sense!
		while (iterator.hasNext()) {
			EObject element = iterator.next();
			
			if (element instanceof DwFeature) {
				DwFeature feature = (DwFeature) element;
				Rectangle featureMarkRectangle = getFeatureMarkRectangle(feature);
				
				drawFeatureMarks(feature, graphics);
				
				boolean featureHasError = graphicalEditor.hasError(feature);
				boolean featureHasWarning = graphicalEditor.hasWarning(feature);

				if (featureHasError) {
					DwDrawingUtil.drawProblem(graphics, featureMarkRectangle, this, true);
				} else if (featureHasWarning) {
					DwDrawingUtil.drawProblem(graphics, featureMarkRectangle, this, false);
				}
			}
			
			if (element instanceof DwGroup) {
				DwGroup group = (DwGroup) element;
				Rectangle groupMarkRectangle = getGroupMarkRectangle(group);
				
				//It might be that the figure never was visible
				//so there is no rectangle around it either.
				if (groupMarkRectangle != null) {
					boolean groupHasError = graphicalEditor.hasError(group);
					boolean groupHasWarning = graphicalEditor.hasWarning(group);
					
					if (groupHasError) {
						DwDrawingUtil.drawProblem(graphics, groupMarkRectangle, this, true);
					} else if (groupHasWarning) {
						DwDrawingUtil.drawProblem(graphics, groupMarkRectangle, this, false);
					}
				}
			}
		}
	}
	
	protected void drawFeatureMarks(DwFeature feature, Graphics graphics) {
		DwGraphicalEditor graphicalEditor = getGraphicalEditor();
		
		Rectangle featureMarkRectangle = getFeatureMarkRectangle(feature);
		
		boolean featureHasError = graphicalEditor.hasError(feature);
		boolean featureHasWarning = graphicalEditor.hasWarning(feature);
		
		if (featureHasError) {
			DwDrawingUtil.drawProblem(graphics, featureMarkRectangle, this, true);
		} else if (featureHasWarning) {
			DwDrawingUtil.drawProblem(graphics, featureMarkRectangle, this, false);
		}
	}
	
	protected Rectangle getFeatureMarkRectangle(DwFeature feature) {
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		
		DwFeatureLayouterManager layouterManager = graphicalEditor.getFeatureLayouterManager();
		Rectangle featureMarkRectangle = layouterManager.getBoundsOfFeature(feature);
		
		
		if (DwFeatureFigure.variationTypeCircleVisible(feature, getGraphicalEditor().getCurrentSelectedDate())) {
			int increment = (theme.getFeatureVariationTypeExtent() - 1);
			featureMarkRectangle.y += increment;
			featureMarkRectangle.height -= increment;
		}
		
		
		return featureMarkRectangle;
	}
	
	private Rectangle getGroupMarkRectangle(DwGroup searchedGroup) {
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		List<?> children = getChildren();
		
		for (Object child : children) {
			if (child instanceof DwGroupFigure) {
				DwGroupFigure groupFigure = (DwGroupFigure) child;
				DwGroup group = groupFigure.getGroup();
				
				if (group == searchedGroup) {
					Rectangle groupMarkRectangle = groupFigure.getBounds().getCopy();
					groupMarkRectangle.expand(theme.getSecondaryMargin(), theme.getSecondaryMargin());
					return groupMarkRectangle;
				}
			}
		}
		
		return null;
	}

	protected DwTemporalFeatureModel getFeatureModel() {
		return featureModel;
	}

	protected DwGraphicalEditor getGraphicalEditor() {
		return graphicalEditor;
	}
}
