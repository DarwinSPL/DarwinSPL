package de.darwinspl.feature.graphical.base.util.export;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;

import de.darwinspl.feature.graphical.base.editor.DwGraphicalViewer;

public class ExportExtensionPointUtil {
	
	private static final String EXTENSION_POINT_ID = "de.darwinspl.feature.graphical.base.image_export";

	public static void handleExport(DwGraphicalViewer viewer) {
		IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor(EXTENSION_POINT_ID);
		
		Map<String, DwImageExporter> exporters = new HashMap<>();
		
        for (IConfigurationElement e : config) {
			try {
				String name = e.getAttribute("name");
				Object o = e.createExecutableExtension("implementation");
	            if (o instanceof DwImageExporter)
	            	exporters.put(name, (DwImageExporter) o);
			} catch (CoreException e1) {
				e1.printStackTrace();
			}
        }

    	try {
	        if(exporters.size() == 1) {
	        	exporters.get(exporters.keySet().toArray()[0]).exportImage(viewer);
	        }
	        else if(exporters.size() > 1) {
	        	DwExportDialog dialog = new DwExportDialog(viewer.getGraphicalEditor().getSite().getShell(), exporters);
	        	if(dialog.open() == 0)
					dialog.getSelectedExproter().exportImage(viewer);
	        }
	        return;
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
		throw new RuntimeException("Exported failed: No exporters registered.");
	}

}
