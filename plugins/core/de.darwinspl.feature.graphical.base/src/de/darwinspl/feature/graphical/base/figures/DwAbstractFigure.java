package de.darwinspl.feature.graphical.base.figures;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.geometry.Rectangle;

import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.util.DwGraphicalEditorTheme;

public abstract class DwAbstractFigure extends Figure {
	protected DwGraphicalEditor graphicalEditor;
	
	public DwAbstractFigure(DwGraphicalEditor graphicalEditor) {
		this.graphicalEditor = graphicalEditor;
	}
	
	public void update() {
	}

	protected DwGraphicalEditor getGraphicalEditor() {
		return graphicalEditor;
	}
	
	protected Rectangle getEffectiveBounds() {
		return getBounds();
	}
	
	public Rectangle getSelectionMarkerBounds() {
		Rectangle selectionBounds = getEffectiveBounds().getCopy();
		
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		selectionBounds.expand(theme.getSecondaryMargin(), theme.getSecondaryMargin());
		
		translateToAbsolute(selectionBounds);
		
		return selectionBounds;
	}
}
