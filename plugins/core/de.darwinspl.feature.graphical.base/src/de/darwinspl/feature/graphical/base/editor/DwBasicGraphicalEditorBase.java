package de.darwinspl.feature.graphical.base.editor;

import java.util.Date;

import de.darwinspl.feature.DwTemporalFeatureModel;

/**
 * 
 * All DarwinSPL Editors must implement this interface.
 * Commands and Actions will then be able to work for the editor.
 * Nice. 
 *
 */

public interface DwBasicGraphicalEditorBase {
	
	public Date getCurrentSelectedDate();
	public boolean isLastDateSelected();
	
	public DwTemporalFeatureModel getFeatureModel();
	
	public DwGraphicalViewer getViewer();
	
}
