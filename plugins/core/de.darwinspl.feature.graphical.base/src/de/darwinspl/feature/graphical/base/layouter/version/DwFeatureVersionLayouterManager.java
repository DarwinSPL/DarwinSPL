package de.darwinspl.feature.graphical.base.layouter.version;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureVersion;

public class DwFeatureVersionLayouterManager {
	private static DwFeatureVersionLayouterManager instance = null;
	
	
	protected static DwFeatureVersionLayouterManager getInstance() {
		if (instance == null) {
			instance = new DwFeatureVersionLayouterManager();
		}
		
		
		return instance;
	}
	
	
	private Map<DwFeature, DwFeatureVersionTreeLayouter> featureToLayouterMap;

	private DwFeatureVersionLayouterManager() {
		featureToLayouterMap = new HashMap<DwFeature, DwFeatureVersionTreeLayouter>();
	}

	
	/**
	 * This method retrieves the appropriate layouter from a map. If there is no such layouter, a new one will be created and returned.
	 */
	public static DwFeatureVersionTreeLayouter getLayouter(DwFeature feature) {
		return getInstance().doGetLayouter(feature);
	}
	
	private DwFeatureVersionTreeLayouter doGetLayouter(DwFeature feature) {
		if (!featureToLayouterMap.containsKey(feature)) {
			doUpdateLayouter(feature);
		}
		
		return featureToLayouterMap.get(feature);
	}
	
	public static void updateLayouter(DwFeature feature) {
		getInstance().doUpdateLayouter(feature);
	}
	
	private void doUpdateLayouter(DwFeature feature) {
		List<DwFeatureVersion> versions = feature.getVersions();
		
		if (!versions.isEmpty()) {
			featureToLayouterMap.put(feature, new DwFeatureVersionTreeLayouter(feature));
		}
	}
}
