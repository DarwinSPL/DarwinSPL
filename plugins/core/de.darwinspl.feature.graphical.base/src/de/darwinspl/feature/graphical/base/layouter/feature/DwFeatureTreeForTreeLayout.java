package de.darwinspl.feature.graphical.base.layouter.feature;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.abego.treelayout.util.AbstractTreeForTreeLayout;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.util.custom.DwFeatureUtil;

public class DwFeatureTreeForTreeLayout extends AbstractTreeForTreeLayout<DwFeature> {

	protected DwGraphicalEditor editor;
	
	public DwFeatureTreeForTreeLayout(DwFeature root, DwGraphicalEditor editor) {
		super(root);
		
		this.editor = editor;
	}

	@Override
	public List<DwFeature> getChildrenList(DwFeature feature) {
		Date selectedDate = editor.getCurrentSelectedDate();
		
		List<DwFeature> children = new ArrayList<DwFeature>();

		if (feature == null) {
			return children;
		}
		
		children = DwFeatureUtil.getChildFeaturesOfFeature(feature, selectedDate);

		return children;
	}

	@Override
	public DwFeature getParent(DwFeature node) {
		return DwFeatureUtil.getParentFeatureOfFeature(node, editor.getCurrentSelectedDate());
	}

}
