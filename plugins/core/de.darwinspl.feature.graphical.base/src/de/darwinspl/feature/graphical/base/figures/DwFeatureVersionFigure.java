package de.darwinspl.feature.graphical.base.figures;

import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.figures.shapes.DwTriangleShape;
import de.darwinspl.feature.graphical.base.layouter.version.DwFeatureVersionLayouterManager;
import de.darwinspl.feature.graphical.base.layouter.version.DwFeatureVersionTreeLayouter;
import de.darwinspl.feature.graphical.base.util.DwGeometryUtil;
import de.darwinspl.feature.graphical.base.util.DwGraphicalEditorTheme;

public class DwFeatureVersionFigure extends DwAbstractFigure {
	private DwFeatureVersion version;
	
	private DwTriangleShape triangle;
	private Label label;

	public DwFeatureVersionFigure(DwFeatureVersion version, DwGraphicalEditor graphicalEditor) {
		super(graphicalEditor);
		
		this.version = version;
		
		setLayoutManager(new XYLayout());
		
		createChildFigures();
		update();
	}

	private void createChildFigures() {
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		
		triangle = new DwTriangleShape();
		triangle.setSize(theme.getVersionTriangleEdgeLength(), theme.getVersionTriangleEdgeLength());
		add(triangle);
		
		label = new Label();
		label.setLabelAlignment(PositionConstants.CENTER);
		label.setFont(theme.getVersionFont());
		label.setForegroundColor(theme.getVersionFontColor());
		add(label);
	}
	
	@Override
	protected boolean useLocalCoordinates() {
		return true;
	}
	
	@Override
	public void update() {
		super.update();
		
		updateContent();
		resizeToContent();
		updateLocation();
	}
	
	private void updateContent() {
		String number = version.getNumber();
		label.setText(number);
	}
	
	private void resizeToContent() {
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		
		int width = DwGeometryUtil.calculateVersionWidth(version);
		int height = DwGeometryUtil.calculateVersionHeight(version);
		setSize(width, height);

		
		//Calculate position for version shape
		int versionShapeX = (width - theme.getVersionTriangleEdgeLength()) / 2;
		int versionShapeY = 0;

		triangle.setLocation(new Point(versionShapeX, versionShapeY));

		
		//Calculate position for label
		Dimension preferredLabelDimension = label.getPreferredSize();
		label.setSize(preferredLabelDimension);
		
		int labelX = (width - preferredLabelDimension.width) / 2;
		int labelY = theme.getVersionTriangleEdgeLength() + theme.getSecondaryMargin();
		
		label.setLocation(new Point(labelX, labelY));
	}
	
	private void updateLocation() {
		DwFeature feature = version.getFeature();
		
		if (feature == null) {
			return;
		}
		
		DwFeatureVersionTreeLayouter versionTreeLayouter = DwFeatureVersionLayouterManager.getLayouter(feature);

		Point location = versionTreeLayouter.getCoordinates(version, getGraphicalEditor().getCurrentSelectedDate());
		setLocation(location);
	}

	public Label getLabel() {
		return label;
	}

}
