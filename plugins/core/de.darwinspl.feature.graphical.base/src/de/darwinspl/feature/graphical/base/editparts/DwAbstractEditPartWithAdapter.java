package de.darwinspl.feature.graphical.base.editparts;

import java.util.List;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.ecore.EObject;

import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.util.DwAbstractAdapter;

public abstract class DwAbstractEditPartWithAdapter extends DwAbstractEditPart {
	private DwAbstractAdapter adapter;
	
	public DwAbstractEditPartWithAdapter(DwGraphicalEditor graphicalEditor) {
		super(graphicalEditor);
		
		adapter = createAdapter(this);
	}
	
	protected abstract DwAbstractAdapter createAdapter(DwAbstractEditPartWithAdapter editPart);
	
	@Override
	public void activate() {
		if (!isActive()) {
			EObject eObject = getModel();
			List<Adapter> adapters = eObject.eAdapters();
			adapters.add(adapter);
		}
		
		super.activate();
	}

	@Override
	public void deactivate() {
		if (isActive()) {
			EObject eObject = getModel();
			List<Adapter> adapters = eObject.eAdapters();
			adapters.remove(adapter);
		}

		super.deactivate();
	}
	
	@Override
	public EObject getModel() {
		return (EObject) super.getModel();
	}

}
