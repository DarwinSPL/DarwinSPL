package de.darwinspl.feature.graphical.base.editor;


import java.util.Date;
import java.util.EventObject;

import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.gef.RootEditPart;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import de.christophseidl.util.gef.editor.EcoreGraphicalEditor;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.graphical.base.editparts.DwRootEditPart;
import de.darwinspl.feature.graphical.base.factories.DwEditPartFactory;
import de.darwinspl.feature.graphical.base.layouter.feature.DwFeatureLayouterManager;
import de.darwinspl.feature.graphical.base.slider.DwGraphicalModelDateScale;
import de.darwinspl.feature.graphical.base.util.DwGraphicalEditorTheme;
import de.darwinspl.temporal.util.DwEvolutionUtil;

public abstract class DwGraphicalEditor extends EcoreGraphicalEditor<DwTemporalFeatureModel> implements DwBasicGraphicalEditorBase {
	private static DwGraphicalEditorTheme theme = new DwGraphicalEditorTheme();
	private DwFeatureLayouterManager featureLayouterManager;
	
	public org.eclipse.draw2d.geometry.Point getCurrentMousePosition() {
		Display display = Display.getDefault();
		org.eclipse.swt.graphics.Point point = getGraphicalViewer().getControl().toControl(display.getCursorLocation());
		FigureCanvas figureCanvas = (FigureCanvas) getGraphicalViewer().getControl();
		org.eclipse.draw2d.geometry.Point location = figureCanvas.getViewport().getViewLocation();
		return new org.eclipse.draw2d.geometry.Point(point.x + location.x, point.y + location.y);
	}
	
	@Override
	protected void setDataModel(DwTemporalFeatureModel featureModel) {
		super.setDataModel(featureModel);
		featureLayouterManager = new DwFeatureLayouterManager(featureModel, this);
		
		DwGraphicalModelDateScale dateScale = getDateScale();
		if(dateScale != null) {
			dateScale.setModel(featureModel);
		}
	}
	
	@Override
	protected Layout createGraphicalViewerCompositeLayout() {
		GridLayout layout = new GridLayout(1, true);
		layout.horizontalSpacing = 0;
		layout.verticalSpacing = 0;
		return layout;
	}

	@Override
	protected RootEditPart createRootEditPart() {
		return new DwRootEditPart();
	}
	
	public DwTemporalFeatureModel getFeatureModel() {
		return getDataModel();
	}

	public static DwGraphicalEditorTheme getTheme() {
		return theme;
	}
	
	
	public Date getCurrentSelectedDate() {
		DwGraphicalModelDateScale dateScale = getDateScale();
		Date date = null;
		if (dateScale != null) {
			date = dateScale.getCurrentSelectedDate();
		}
		else {
			DwTemporalFeatureModel featureModel = getFeatureModel();
			if (featureModel != null) {
				date = DwEvolutionUtil.getClosestEvolutionDate(featureModel, new Date());
			}
		}
		
		return date;
	}
	
	protected DwGraphicalModelDateScale getDateScale() {
		DwGraphicalViewer viewer = getViewer();
		if(viewer != null) {
			DwGraphicalModelDateScale dateScale = viewer.getDateScale();
			if(dateScale != null) {
				return dateScale;
			}
		}
		
		return null;
	}

	public DwFeatureLayouterManager getFeatureLayouterManager() {
		return featureLayouterManager;
	}

	public boolean isLastDateSelected() {
		DwGraphicalModelDateScale dateScale = getDateScale();
		if (dateScale != null) {
			return dateScale.isLastDateSelected();
		}
		
		return false;
	}

	public DwGraphicalViewer getViewer() {
		return (DwGraphicalViewer) super.getGraphicalViewer();
	}
	
//	@Override
//	protected abstract DwGraphicalViewer doCreateGraphicalViewer();
	
	@Override
	protected void registerActions(ActionRegistry actionRegistry) {
		super.registerActions(actionRegistry);
		
//		ActionRegistry actionRegistry = getActionRegistry();
	}
	
	@Override
	protected DwEditPartFactory createEditPartFactory() {
		return new DwEditPartFactory(this);
	}
	
	
	@Override
	public void commandStackChanged(EventObject event) {
		super.commandStackChanged(event);
		this.refreshVisuals();
	}
	

	@Override
	public void refreshVisuals() {
		this.getViewer().setContents(this.getFeatureModel());
		super.refreshVisuals();
	}
	
	protected Shell getShell() {
		return PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
	}
	
	// Evolution Slider
	
//	// TODO not editable if Long.MIN_VALUE or Long.MAX_VALUE
//	public List<Date> getAvailableDates() {
//		if(availableDates == null) {
//			availableDates = DwEvolutionUtil.collectDates(getFeatureModel());
//			if(availableDates.size() > 0) {
//				Date beforeFirstEvolutionStep = new Date(Long.MIN_VALUE);
//				availableDates.add(0, beforeFirstEvolutionStep);
//			}
//		}
//		return availableDates;
//	}
//	
//	/**
//	 * Register listeners to react on widget changes from the time line like the date slider
//	 * or the add date dialog
//	 */
//	public void registerControlListeners(){
//		// Left button to select an individual date
//		currentDateButton.addListener(SWT.Selection, new Listener(){
//			public void handleEvent(Event event){
//				DwDateDialog dialog = new DwDateDialog(getEditorSite().getShell(), getCurrentSelectedDate());
//				dialog.open();
//				if(dialog.getReturnCode()==0){
//					Date newDate = dialog.getValue();
//					if(!getAvailableDates().contains(newDate)) {
//						getAvailableDates().add(newDate);
//						Collections.sort(getAvailableDates());
//						
//						int size = getAvailableDates().size();
//						evolutionSlider.setMaximum(size-1);
//						evolutionSlider.setEnabled(size > 1);
//
//						int index = getAvailableDates().indexOf(newDate);
//						evolutionSlider.setSelection(index);
//						setCurrentSelectedDate(newDate);
//
//						if(index == getAvailableDates().size()-1)
//							lastDateSelected = true;
//					}
//				}
//			}
//		});
//
//		// Slider to select a given date
//		evolutionSlider.addListener(SWT.Selection, this);
//
//		// Button that adds a new date to the model
//		addDateButton.addListener(SWT.Selection, new Listener() {
//			public void handleEvent(Event event) {
//				Date date = new Date();
//				Calendar cal = new GregorianCalendar();
//				cal.setTime(date);
//				cal.set(Calendar.HOUR_OF_DAY, 0);
//				cal.set(Calendar.MINUTE, 0);
//				cal.set(Calendar.SECOND, 0);
//				cal.set(Calendar.MILLISECOND, 0);
//				date = cal.getTime();
//				DwDateDialog dialog = new DwDateDialog(null, date);
//				dialog.open();
//				Date value = dialog.getValue();
//
//				if(value != null){
//					getAvailableDates().add(value);
//
//					int size = getAvailableDates().size();
//					evolutionSlider.setMaximum(size-1);
//					evolutionSlider.setEnabled(size > 1);
//				}				
//			}	
//		});
//		
//		resetDatesButton.addListener(SWT.Selection, new Listener() {
//
//			@Override
//			public void handleEvent(Event event) {
//				List<Date> dates = DwEvolutionUtil.collectDates(getFeatureModel());
//				if(dates.size() > 0) {
//					Date beforeFirstEvolutionStep = new Date(Long.MIN_VALUE);
//					dates.add(0, beforeFirstEvolutionStep);
//				}
//				
//				List<Date> availableDates = getAvailableDates();
//				availableDates.clear();
//				availableDates.addAll(dates);
//				
//				int size = dates.size();
//				evolutionSlider.setMaximum(size-1);
//				evolutionSlider.setEnabled(size > 1);
//				
////				scale.update();
//				
//				setCurrentSelectedDateToMostActualDate();
//				setCurrentSelectedDate(getCurrentSelectedDate());
//			}
//			
//		});
//	}
//
//	/**
//	 * Creates all widgets (slider and add date button) to the editor to perform date changes.
//	 * @param parent
//	 */
//	public void createSliderControl(final Composite parent){
//		List<Date> dates = getAvailableDates();
//
//		int size = dates.size(); 
//		buttonGroup = new Composite(parent, SWT.NONE);
//		RowLayout rowLayout = new RowLayout(SWT.HORIZONTAL);
//		rowLayout.justify = true;
//		buttonGroup.setLayout(rowLayout);
//
//		currentDateButton = new Button(buttonGroup, SWT.PUSH);
//
//		if(dates.size() > 0)
//			currentDateButton.setText(dates.get(0).toString());
//		else{
//			currentDateButton.setText((new Date()).toString());
//		}
//
//		evolutionSlider = new Scale(buttonGroup, SWT.FILL);
//		evolutionSlider.setMinimum(0);
//		evolutionSlider.setMaximum(size-1);
//		evolutionSlider.setLayoutData(new RowData(300, SWT.DEFAULT));
//		evolutionSlider.setEnabled(size > 1);
//		evolutionSlider.setSelection(dates.indexOf(getCurrentSelectedDate()));
//
//		addDateButton = new Button(buttonGroup, SWT.PUSH);
//		addDateButton.setText("Add Date");
//		
//		resetDatesButton = new Button(buttonGroup, SWT.PUSH);
//		resetDatesButton.setText("Reset Dates");
//
//		if(dates.size() > 0) {
//			setCurrentSelectedDate(getCurrentSelectedDate());
//		}
//		else{
//			Date now = new Date();
//			dates.add(now);
//			setCurrentSelectedDate(now);
//
//			lastDateSelected = true;
//		}		
//
//		/**
//		 * register control listener for visualization bug if a side editor was added
//		 */
//		parent.addControlListener(new ControlListener(){
//
//			@Override
//			public void controlMoved(ControlEvent e) {
//			}
//
//			@Override
//			public void controlResized(ControlEvent e) {
//				parent.update();
//			}
//
//		});
//	}
//	
//
//	/**
//	 * Changes the selected date from the feature model and force a rerendering of the editor
//	 * to show the feature model at this date.
//	 * 
//	 * @param currentSelectedDate the desired date for displaying the feature model
//	 */
//	public void setCurrentSelectedDate(Date currentSelectedDate) {
//		this.currentSelectedDate = currentSelectedDate;
//
//		if(currentSelectedDate.getTime() == Long.MIN_VALUE){
//			if(getAvailableDates().size() > 0) {
//				currentDateButton.setText("before "+ getAvailableDates().get(1).toString());				
//			}
//		}
////		else if(currentSelectedDate.getTime() == Long.MAX_VALUE){
////			currentSelectedDate.setText("after "+ getAvailableDates().get(getAvailableDates().size()-2).toString());
////		}
//		else {
//			currentDateButton.setText(currentSelectedDate.toString());
//		}
//
//		int index = getAvailableDates().indexOf(currentSelectedDate);
//		if(index >= 0 && index == getAvailableDates().size()-1) {
//			lastDateSelected = true;			
//		}
//		else {
//			lastDateSelected = false;
//		}
//	}
//	
//
//	/**
//	 * Changes the date to the most actual date from now. 
//	 * Does not cause a rerendering of the feature model.
//	 */
//	protected void setCurrentSelectedDateToMostActualDate(){
//		List<Date> dates = HyEvolutionUtil.collectDates(modelWrapped.getModel());
//		Date currentDate = new Date();
//		Date closestDate = new Date(Long.MIN_VALUE);
//		for(Date date : dates){
//			if(date.before(currentDate)){
//				if(date.after(closestDate))
//					closestDate = date;
//			}
//		}
//
//		lastDateSelected = true;
//		currentSelectedDate = closestDate;	
//	}
	
//	public DEConstraintModel getConstraintModel() {
//		return constraintModel;
//	}
	
	// Zooming
//	protected KeyHandler sharedKeyHandler;
//	
//	/**
//	 * Adds zooming support with the keys ctrl & +/-.
//	 * @return the keyhandler to handle the zomming shortcuts
//	 */
//	protected KeyHandler getCommonKeyHandler() {
//		ZoomManager manager = (ZoomManager) getGraphicalViewer().getProperty(ZoomManager.class.toString());
//
//		if (sharedKeyHandler == null) {
//			sharedKeyHandler = new KeyHandler();
//			sharedKeyHandler.put(KeyStroke.getPressed('+', SWT.KEYPAD_ADD, SWT.CONTROL),
//					new ZoomInAction(manager));
//			sharedKeyHandler.put(KeyStroke.getPressed('-', SWT.KEYPAD_SUBTRACT, SWT.CONTROL),
//					new ZoomOutAction(manager));
//		}
//
//		return sharedKeyHandler;
//	}
//	
//	/**
//	 * Adds zoom support to the zoom manager via the mouse wheel. 
//	 */
//	protected void enableZoomWithMouseWheel() {
//		// Zoom
//		ZoomManager manager = (ZoomManager) getGraphicalViewer().getProperty(
//				ZoomManager.class.toString());
//		if (manager != null) {
//			manager.setZoom(1);
//		}
//
//
//		// Scroll-wheel Zoom
//		getGraphicalViewer().setProperty(
//				MouseWheelHandler.KeyGenerator.getKey(SWT.MOD1),
//				MouseWheelZoomHandler.SINGLETON);
//	}
//	
//	@Override
//	protected void initializeGraphicalViewer() {
//		super.initializeGraphicalViewer();
//		enableZoomWithMouseWheel();
//	}
//	
//	/**
//	 * Register the zoom in and zoom out action to the zoom manager of the current graphical editor.
//	 */
//	@Override
//	protected void configureGraphicalViewer() {
//		super.configureGraphicalViewer();
//
//		ZoomManager manager = (ZoomManager) getGraphicalViewer().getProperty(ZoomManager.class.toString());
//
//		getActionRegistry().registerAction(new ZoomInAction(manager));
//		getActionRegistry().registerAction(new ZoomOutAction(manager));
//	}	
//	
//	/**
//	 * Returns the zoom manager for the current editor in case there is one.
//	 */
//	@Override
//	public Object getAdapter(@SuppressWarnings("rawtypes") Class type) {
//		if(type == ZoomManager.class) {
//			return getGraphicalViewer().getProperty(ZoomManager.class.toString());			
//		}
//		else {
//			return super.getAdapter(type);
//		}
//
//	}	
}
