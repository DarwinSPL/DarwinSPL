package de.darwinspl.feature.graphical.base.slider;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Scale;

import de.christophseidl.util.eclipse.ui.JFaceUtil;
import de.darwinspl.common.eclipse.ui.DwDateDialog;
import de.darwinspl.temporal.util.DwDateResolverUtil;
import de.darwinspl.temporal.util.DwEvolutionUtil;

/**
 * This is a custom {@link Composite} to display a Slider Control for multiple
 * model editors. It will fetch the supplied model's contained dates and show
 * them on a dynamic {@link Scale}. It also implements functionality to add user
 * picked dates to a model (if applicable). Use the supplied getters to access
 * the current selected Date for editor display purposes.
 *
 */
public class DwGraphicalModelDateScale extends Composite {

	private static final String CHANGE_DATE_BUTTON_DEFAULT_TEXT = "Change Selected Date";
	private static final String ADD_DATE_BUTTON_TEXT = "Add Date";
	private static final String RESET_DATES_BUTTON_TEXT = "Clean Up Dates (Remove Unsued Timepoints)";

	public static final String SELECTED_DATE_PROPERTY = "selectedDate";
	
	// helles blau: 235, 241, 246
	private static final Color BACKGROUND_COLOR = new Color(Display.getDefault(), 255-8, 255-8, 255-8);

	private CLabel currentDateLabel;
	private Button changeDateButton;
	private Button addDateButton;
	private Button resetDatesButton;
	private Scale scale;
	private List<Date> dates;

	// TODO the "updateCount" variable is a dirty little trick that enables
	// to "detect" whether the model finished loading. Something similar
	// (but more sensible) is required to set the date scale to the most
	// current date. 
	private int updateCount = 0;
	private EObject model;
	private Date currentSelectedDate;
	
	private List<DwDateChangeListener> dateChangeListeners;

	public DwGraphicalModelDateScale(Composite parent) {
		super(parent, SWT.BORDER_DASH);

		this.setBackground(BACKGROUND_COLOR);
		this.dates = new LinkedList<Date>();
		this.dateChangeListeners = new LinkedList<DwDateChangeListener>();

		// the layout for this composite
		GridLayout layout = new GridLayout(7, false);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		layout.verticalSpacing = 0;
		this.setLayout(layout);

		GridData vSeparatorLayoutData = new GridData(GridData.FILL_HORIZONTAL);
		vSeparatorLayoutData.horizontalSpan = 7;
		GridData hSeparatorLayoutData = new GridData(GridData.FILL_VERTICAL);
		hSeparatorLayoutData.heightHint = 0;

		Label vSeparatorTop = new Label(this, SWT.HORIZONTAL | SWT.SEPARATOR);
		vSeparatorTop.setLayoutData(vSeparatorLayoutData);
		Label hSeparatorLeft = new Label(this, SWT.VERTICAL | SWT.SEPARATOR);
		hSeparatorLeft.setLayoutData(hSeparatorLayoutData);
		
		// the Button displaying the current scale Date selection.
		changeDateButton = new Button(this, SWT.PUSH);
		changeDateButton.setToolTipText(CHANGE_DATE_BUTTON_DEFAULT_TEXT);
        ImageDescriptor changeDateImageDescriptor = JFaceUtil.getImageDescriptorFromClassBundle("icons/change_date.png", getClass());
        Image changeDateImage = new Image(Display.getDefault(), changeDateImageDescriptor.getImageData(100));
		changeDateButton.setImage(changeDateImage);

		// the add date Button
		addDateButton = new Button(this, SWT.PUSH);
		addDateButton.setToolTipText(ADD_DATE_BUTTON_TEXT);
        ImageDescriptor addDateImageDescriptor = JFaceUtil.getImageDescriptorFromClassBundle("icons/add_date.png", getClass());
        Image addDateImage = new Image(Display.getDefault(), addDateImageDescriptor.getImageData(100));
		addDateButton.setImage(addDateImage);

		resetDatesButton = new Button(this, SWT.PUSH);
		resetDatesButton.setToolTipText(RESET_DATES_BUTTON_TEXT);
        ImageDescriptor resetDatesImageDescriptor = JFaceUtil.getImageDescriptorFromClassBundle("icons/clean_dates.png", getClass());
        Image resetDatesImage = new Image(Display.getDefault(), resetDatesImageDescriptor.getImageData(100));
		resetDatesButton.setImage(resetDatesImage);

		// the scale
		scale = new Scale(this, SWT.HORIZONTAL);
		GridData scaleLayoutData = new GridData(GridData.FILL, GridData.CENTER, true, true);
		scaleLayoutData.minimumWidth = 150;
		scale.setLayoutData(scaleLayoutData);
		
		// The current Date
		currentDateLabel = new CLabel(this, SWT.CENTER);
		GridData currentDateLabelLayoutData = new GridData(SWT.RIGHT, SWT.FILL, false, true);
		currentDateLabelLayoutData.widthHint = 250;
		currentDateLabelLayoutData.heightHint = 60;
		currentDateLabel.setLayoutData(currentDateLabelLayoutData);
//		currentDateLabel.setText("");

		Label hSeparatorRight = new Label(this, SWT.VERTICAL | SWT.SEPARATOR);
		hSeparatorRight.setLayoutData(hSeparatorLayoutData);
		Label vSeparatorBot = new Label(this, SWT.HORIZONTAL | SWT.SEPARATOR);
		vSeparatorBot.setLayoutData(vSeparatorLayoutData);
		
		this.pack();
		parent.pack();

		setSelectedDateToClosestActualDate(new Date());
		registerControlListeners();
	}

	public void setModel(EObject model) {
		this.model = model;
		doUpdate();
	}

	/**
	 * registers listeners for the scale and the buttons
	 */
	private void registerControlListeners() {
		checkWidget();
		// Left button to select an individual date
		changeDateButton.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {

				DwDateDialog dialog = new DwDateDialog(null, getCurrentSelectedDate());
				dialog.open();
				if (dialog.getReturnCode() == 0) {
					Date newDate = dialog.getValue();
					setSelectedDateToClosestActualDate(newDate);
				}
			}
		});

		// Slider to select a given date.
		scale.addListener(SWT.Selection, new Listener() {

			// Set date on slider move
			@Override
			public void handleEvent(Event event) {
				setCurrentSelectedDate(dates.get(scale.getSelection()));
			}
		});

		// Only for feature model editors
		// Button that adds a new date to the model
		addDateButton.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				Date date = new Date();
				Calendar cal = new GregorianCalendar();
				cal.setTime(date);
				cal.set(Calendar.HOUR_OF_DAY, 0);
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);
				cal.set(Calendar.MILLISECOND, 0);
				date = cal.getTime();
				DwDateDialog dialog = new DwDateDialog(null, date);
				dialog.open();
				Date value = dialog.getValue();

				if (value != null) {
					if (dates.size() == 0)
						dates.add(DwDateResolverUtil.INITIAL_STATE_DATE);
					dates.add(value);
					Collections.sort(dates);

					setCurrentSelectedDate(value);
					updateScale();
					updateCurrentDateButtonText();
					updateScalePosition();
				}
			}
		});

		resetDatesButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				doUpdate();
			}
		});
	}

	/**
	 * Forces an update of the scale's current date list to the dates contained in
	 * the model object, then updates scale bounds. Note: This removes all manually
	 * added dates.
	 */
	public void doUpdate() {
		refreshDatesList(model);
		
		if(updateCount++ == 1)
			if (dates.size() != 0)
				setSelectedDateToClosestActualDate(new Date());
			else
				currentSelectedDate = DwDateResolverUtil.INITIAL_STATE_DATE;

		updateScale();

		updateCurrentDateButtonText();
		updateScalePosition();
	}

	/**
	 * Takes the model as parameter and extracts all Date Objects into this
	 * Composites Date list.
	 * 
	 * @param model
	 */
	private void refreshDatesList(EObject model) {
		checkWidget();
		this.dates = DwEvolutionUtil.collectDates(model);

		if (dates.size() > 0) {
			if(!dates.contains(DwDateResolverUtil.INITIAL_STATE_DATE)) {
				dates.add(DwDateResolverUtil.INITIAL_STATE_DATE);
			}
		}

		Collections.sort(dates);
	}

	/**
	 * Updates the scale bounds.
	 */
	private void updateScale() {
		if (scale != null) {
			int size = dates.size();
			scale.setMaximum(size - 1);
			scale.setEnabled(size > 1);
//			currentDateButton.pack();
//			scale.getParent().pack();
		}
	}

	/**
	 * Sets the current scale Date to this Date. Updates the button texts and scale,
	 * if possible.
	 * 
	 * @param date
	 */
	private void setCurrentSelectedDate(Date date) {
		checkWidget();
		Date oldDate = currentSelectedDate;
		currentSelectedDate = date;
		notifyListeners(date, oldDate);

		updateCurrentDateButtonText();
		updateScalePosition();

		updateScale();
	}

	private void updateCurrentDateButtonText() {
		if (currentSelectedDate == null) {
			currentDateLabel.setText(CHANGE_DATE_BUTTON_DEFAULT_TEXT);
		} else if (currentSelectedDate.getTime() == DwDateResolverUtil.INITIAL_STATE_DATE_LONG) {
			if (dates.size() > 1)
				currentDateLabel.setText("Before " + dates.get(1).toString());

		} else if (currentSelectedDate.getTime() == DwDateResolverUtil.ETERNITY_DATE_LONG) {
			currentDateLabel.setText("After " + dates.get(dates.size() - 2).toString());
		} else {
			currentDateLabel.setText(currentSelectedDate.toString());
		}
	}

	private void updateScalePosition() {
		if (scale != null && currentSelectedDate != null) {
			scale.setSelection(dates.indexOf(currentSelectedDate));
		}
	}

	/**
	 * Changes the date to the most actual existing date from the selected date.
	 * Does not cause a rerendering of the model.
	 */
	private void setSelectedDateToClosestActualDate(Date selectedDate) {
		checkWidget();
		setCurrentSelectedDate(DwEvolutionUtil.getClosestEvolutionDate(dates, selectedDate));
	}

	/**
	 * Returns the underlying model EObject.
	 * 
	 * @return EObject
	 */
	public EObject getModel() {
		return this.model;
	}

	/**
	 * Returns the date this scale is currently set to.
	 * 
	 * @return Date
	 */
	public Date getCurrentSelectedDate() {
		return this.currentSelectedDate;
	}

	private void notifyListeners(Date newDate, Date oldDate) {
		for (DwDateChangeListener listener : dateChangeListeners) {
			listener.dateChanged(newDate, oldDate, this);
		}
	}

	/**
	 * Add a listener to this scale to listen for changes in the currently selected
	 * date.
	 * 
	 * @param newListener the listener to add
	 */
	public void addDateChangeListener(DwDateChangeListener newListener) {
		this.dateChangeListeners.add(newListener);
	}

	/**
	 * Removes the specified date change listener from this instance.
	 * 
	 * @param listener the listener to remove
	 */
	public void removeDateChangeListener(DwDateChangeListener listener) {
		this.dateChangeListeners.remove(listener);
	}

	public boolean isLastDateSelected() {
		if (dates.isEmpty()) {
			return true;
		} else {
			if (currentSelectedDate == null) {
				return false;
			} else {
				if (dates.indexOf(currentSelectedDate) == dates.size() - 1) {
					return true;
				} else {
					return false;
				}
			}
		}
	}

}
