package de.darwinspl.feature.graphical.base.figures;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Path;
import org.eclipse.swt.widgets.Display;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.layouter.feature.DwFeatureLayouterManager;
import de.darwinspl.feature.graphical.base.util.DwDrawingUtil;
import de.darwinspl.feature.graphical.base.util.DwGeometryUtil;
import de.darwinspl.feature.graphical.base.util.DwGraphicalEditorTheme;
import de.darwinspl.feature.util.custom.DwFeatureUtil;

public class DwGroupFigure extends DwAbstractFigure {
	private DwGroup group;
	
	private Path groupTypeArc;
	private List<Path> lines;
	
	public DwGroupFigure(DwGroup group, DwGraphicalEditor graphicalEditor) {
		super(graphicalEditor);
		
		this.group = group;
		lines = new ArrayList<Path>();
		
		setLayoutManager(new XYLayout());
		update();
	}

	private void createGroupTypeArc() {
		Date date = getGraphicalEditor().getCurrentSelectedDate();
		
		groupTypeArc = null;
		
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		List<DwFeature> features = DwFeatureUtil.getChildFeaturesOfGroup(group, date);
		
		if (!groupTypeVisible()) {
			return;
		}
		
		DwFeature firstFeature = features.get(0);
		DwFeature lastFeature = features.get(features.size() - 1);
		
		DwGraphicalEditor graphicalEditor = getGraphicalEditor();
		DwFeatureLayouterManager featureLayouterManager = graphicalEditor.getFeatureLayouterManager();
		
		DwFeature parentOfGroup = DwFeatureUtil.getParentFeatureOfGroup(group, date);
		Point originPoint = DwGeometryUtil.getStartCoordinate(parentOfGroup, featureLayouterManager);
		Point leftLineEndPoint = DwGeometryUtil.getEndCoordinate(firstFeature, featureLayouterManager, date);
		Point rightLineEndPoint = DwGeometryUtil.getEndCoordinate(lastFeature, featureLayouterManager, date);
		
		Point intersectionPointOfLeftLineAndCircle = scaleLineEndPoint(originPoint, leftLineEndPoint, theme.getGroupSymbolRadius());
		

		int circleX = originPoint.x - theme.getGroupSymbolRadius();
		int circleY = originPoint.y - theme.getGroupSymbolRadius();
		
		int circleWidth = 2 * theme.getGroupSymbolRadius();
		int circleHeight = 2 * theme.getGroupSymbolRadius();
		
		
		//Here is the problem: Trigonometric functions assume a Cartesian coordinate system with y coordinate extending towards the top.
		//However, drawing canvas has a Cartesian coordinate system where y extends towards the bottom.
		//Hence, coordinates have to be mirrored along the y-axis before being used as input to the trigonometric functions.
		
		Point mirroredOriginPoint = originPoint.getCopy();
		mirroredOriginPoint.y = -mirroredOriginPoint.y;
		
		Point mirroredLeftLineEndPoint = leftLineEndPoint.getCopy();
		mirroredLeftLineEndPoint.y = -mirroredLeftLineEndPoint.y;
		
		Point mirroredRightLineEndPoint = rightLineEndPoint.getCopy();
		mirroredRightLineEndPoint.y = -mirroredRightLineEndPoint.y;
		
		
		double rawAngle1 = DwGeometryUtil.calculateRotationAngleForLineWithEndpoints(mirroredOriginPoint, mirroredLeftLineEndPoint);
		double rawAngle2 = DwGeometryUtil.calculateRotationAngleForLineWithEndpoints(mirroredOriginPoint, mirroredRightLineEndPoint);
		
		
		float angle1 = (float) Math.toDegrees(rawAngle1);
		float angle2 = (float) Math.toDegrees(rawAngle2);
		
		
		Display display = Display.getCurrent();
		groupTypeArc = new Path(display);
		
		groupTypeArc.moveTo(originPoint.x, originPoint.y);
		groupTypeArc.lineTo(intersectionPointOfLeftLineAndCircle.x, intersectionPointOfLeftLineAndCircle.y);
		groupTypeArc.addArc(circleX, circleY, circleWidth, circleHeight, angle1, angle2 - angle1);
		groupTypeArc.close();
	}
	
	private void createLines() {
		Date date = getGraphicalEditor().getCurrentSelectedDate();
		
		lines.clear();
		
		DwFeature parentFeature = DwFeatureUtil.getParentFeatureOfGroup(group, date);
		
		if (parentFeature == null) {
			return;
		}
		
		DwGraphicalEditor graphicalEditor = getGraphicalEditor();
		DwFeatureLayouterManager featureLayouterManager = graphicalEditor.getFeatureLayouterManager();
		
		Point startPoint = DwGeometryUtil.getStartCoordinate(parentFeature, featureLayouterManager);
		List<DwFeature> features = DwFeatureUtil.getChildFeaturesOfGroup(group, date);
		
		for (DwFeature feature : features) {
			Point endPoint = DwGeometryUtil.getEndCoordinate(feature, featureLayouterManager, date);
			
			Path line = createLine(startPoint, endPoint);
			lines.add(line);
		}
	}
	
	private static Path createLine(Point p1, Point p2) {
		Display display = Display.getCurrent();
		Path line = new Path(display);
		
		line.moveTo(p1.x, p1.y);
		line.lineTo(p2.x, p2.y);
		
		return line;
	}
	
	private static Point scaleLineEndPoint(Point startPoint, Point endPoint, double targetLength) {
		double vectorX = endPoint.x - startPoint.x;
		double vectorY = endPoint.y - startPoint.y;
		double currentLength = Math.sqrt(vectorX * vectorX + vectorY * vectorY);
		
		double scaleFactor = targetLength / currentLength;
		
		double finalX = startPoint.x + scaleFactor * vectorX;
		double finalY = startPoint.y + scaleFactor * vectorY;
		
		return new Point((int) Math.round(finalX), (int) Math.round(finalY));
	}
	
	@Override
	public void paintFigure(Graphics graphics) {
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		
		//Draw lines
		for (Path line : lines) {
			DwDrawingUtil.outlinePath(graphics, line, theme.getLineColor());
		}
		
		//Draw group type
		if (groupTypeVisible() && groupTypeArc != null) {
			Color light = group.isAlternative(getGraphicalEditor().getCurrentSelectedDate()) ? theme.getGroupAlternativePrimaryColor() : theme.getGroupOrPrimaryColor();
			Color dark = group.isAlternative(getGraphicalEditor().getCurrentSelectedDate()) ? theme.getGroupAlternativeSecondaryColor() : theme.getGroupOrSecondaryColor();
			
			DwDrawingUtil.gradientFillPath(graphics, groupTypeArc, light, dark);
			DwDrawingUtil.outlinePath(graphics, groupTypeArc, theme.getLineColor());
		}
	}
	
	@Override
	public void update() {
		super.update();
		
		createGroupTypeArc();
		createLines();
		updateBounds();
		
		//This is necessary to reflect changes in variation type.
		repaint();
	}
	
	private void updateBounds() {
		DwGraphicalEditorTheme theme = DwGraphicalEditor.getTheme();
		int lineWidth = theme.getLineWidth();
		
		Rectangle bounds = groupTypeArc != null ? DwGeometryUtil.createBoundsFromPath(groupTypeArc, lineWidth) : null;
		
		for (Path line : lines) {
			Rectangle lineBounds = DwGeometryUtil.createBoundsFromPath(line, lineWidth);
			
			if (bounds == null) {
				bounds = lineBounds;
			} else {
				bounds.union(lineBounds);
			}
		}
		
		if (bounds == null) {
			bounds = new Rectangle(0, 0, 0, 0);
		}
		else {
			// Minimum width
			if(bounds.width < theme.getMinimumLineBoundsWidth()) {
				int delta = theme.getMinimumLineBoundsWidth() - bounds.width;
				bounds.width += delta;
				bounds.x -= delta/2;
			}
		}
		
		setBounds(bounds);
	}
	
	private boolean groupTypeVisible() {
		if (group.isAlternative(getGraphicalEditor().getCurrentSelectedDate()) || group.isOr(getGraphicalEditor().getCurrentSelectedDate())) {
			List<DwFeature> features = DwFeatureUtil.getChildFeaturesOfGroup(group, getGraphicalEditor().getCurrentSelectedDate());
			
			return features.size() >= 2;
		}
		
		return false;
	}

	@Override
	protected Rectangle getEffectiveBounds() {
		Rectangle bounds = getBounds();
		Rectangle effectiveBounds = bounds.getCopy();
		
//		if (!groupTypeVisible()) {
//			int offset = (DwGraphicalEditor.getTheme().getFeatureVariationTypeExtent() - 1) / 2;
//			
//			effectiveBounds.x -= offset;
//			effectiveBounds.width += 2 * offset;
//		}
		
		return effectiveBounds;
	}
	
	public DwGroup getGroup() {
		return group;
	}
}
