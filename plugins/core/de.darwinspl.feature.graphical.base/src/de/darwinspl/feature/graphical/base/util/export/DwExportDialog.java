package de.darwinspl.feature.graphical.base.util.export;

import java.util.Map;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.osgi.service.prefs.BackingStoreException;

public class DwExportDialog extends MessageDialog {
	
	private Map<String, DwImageExporter> allExporters;
	private DwImageExporter selectedExporter;
	
	private Combo comboSimple;
	
	
	
	private IEclipsePreferences pref;
	private String lastExporterChoice;

	public DwExportDialog(Shell shell, Map<String, DwImageExporter> exporters) {
		super(shell, "Select Image Exporter", null, "Following image exporters were found", MessageDialog.NONE, new String[] { "Continue", "Cancel" }, 0);
		
		pref = InstanceScope.INSTANCE.getNode("de.darwinspl.export");
		this.allExporters = exporters;
	}

	
	@Override
	protected Control createContents(Composite parent) {
		lastExporterChoice = pref.get("lastExporter", null);
		
		Control control = super.createContents(parent);
		
		if(lastExporterChoice == null)
			getButton(IDialogConstants.OK_ID).setEnabled(false);
		else
			selectedExporter = allExporters.get(lastExporterChoice);

		return control;
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);

		// comboSimple = new Combo(parent, SWT.SIMPLE | SWT.BORDER);
		comboSimple = new Combo(parent, SWT.DROP_DOWN | SWT.BORDER | SWT.READ_ONLY);

		for (String exporterName : allExporters.keySet()) {
			comboSimple.add(exporterName);
		}
		
		if(lastExporterChoice != null) {
			comboSimple.select(comboSimple.indexOf(lastExporterChoice));
		}

		comboSimple.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				lastExporterChoice = comboSimple.getItem(comboSimple.getSelectionIndex());
				pref.put("lastExporter", lastExporterChoice);
				try {
					pref.flush();
				} catch (BackingStoreException e1) {
					e1.printStackTrace();
				}
				
				selectedExporter = allExporters.get(lastExporterChoice);
				getButton(IDialogConstants.OK_ID).setEnabled(true);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

		});

		return container;
	}

	public DwImageExporter getSelectedExproter() {
		return selectedExporter;
	}
	
}
