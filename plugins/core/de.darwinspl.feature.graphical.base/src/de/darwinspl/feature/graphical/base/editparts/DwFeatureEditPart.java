package de.darwinspl.feature.graphical.base.editparts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.notify.Notification;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.figures.DwFeatureFigure;
import de.darwinspl.feature.graphical.base.layouter.version.DwFeatureVersionLayouterManager;
import de.darwinspl.feature.graphical.base.util.DwAbstractAdapter;

public class DwFeatureEditPart extends DwAbstractEditPartWithAdapter {
	
	public DwFeatureEditPart(DwGraphicalEditor graphicalEditor) {
		super(graphicalEditor);
	}

	@Override
	protected DwAbstractAdapter createAdapter(DwAbstractEditPartWithAdapter editPart) {
		return new DwAbstractAdapter(editPart) {
			
			@Override
			protected void react(Notification notification) {
				refresh();
//				refreshChildren();
				
				super.react(notification);
			}
			
			@Override
			public boolean isAdapterForType(Object type) {
				return type.equals(DwFeature.class);
			}
		};
	}
	
	@Override
	public DwFeature getModel() {
		return (DwFeature) super.getModel();
	}
	
	@Override
	protected IFigure createFigure() {
		DwFeature feature = getModel();
		DwGraphicalEditor graphicalEditor = (DwGraphicalEditor) getGraphicalEditor();
		return new DwFeatureFigure(feature, graphicalEditor);
	}

	@Override
	public DwFeatureFigure getFigure() {
		return (DwFeatureFigure) super.getFigure();
	}
	
	@Override
	protected List<?> getModelChildren() {
		List<DwFeatureVersion> children = new ArrayList<DwFeatureVersion>();
		
		DwFeature feature = getModel();
		List<DwFeatureVersion> versions = feature.getVersions();
		children.addAll(versions);
		
		return children;
	}
	
	@Override
	public void refreshVisuals() {
		DwFeatureFigure figure = (DwFeatureFigure) getFigure();
		figure.update();
	}

	@Override
	protected void refreshChildren() {
		DwFeature feature = getModel();
		DwFeatureVersionLayouterManager.updateLayouter(feature);
		
		super.refreshChildren();
	}

}
