package de.darwinspl.exporter.svg;

import java.awt.Color;
import java.awt.Font;
import java.io.IOException;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FontDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import de.christophseidl.util.eclipse.ResourceUtil;

public class DwSVGExportDialog extends Dialog {

	private static IFile imageFile;

	private DwSVGProfileManager profileManager;
	private static String activeProfileName;
	private static DwSVGProfile activeProfile;

	private Button fgColorButton;
	private Button bgColorButton;
	private Color currentFgColor;
	private Color currentBgColor;
	
	private Button fontButton;
	private Font currentFont;
	
	private Combo activeProfileCombo;
	private Text newProfileText;
	private Button saveNewProfileButton;

	private Button storeEntireHistoryCheckBox;
	private boolean storeEntireHistory;
	private Text fileNameText;
	private String fileName;
	
	private Combo layoutModeCombo;
	
	private Text globalMarginText;
	private Text lineWidthText;
	private Text featureNodeTextMarginXText;
	private Text featureNodeTextMarginYText;
	private Text featureNodeMinimumWidthText;
	private Text roundBoxesRadiusText;
	private Text gapText;
	private Text gapBetweenLevelsText;
	private Text featureVariationTypeCircleRadiusText;
	private Text groupVariationTypeArcRadiusText;

//	private Button convertTextToPathsCheckBox;
	private Button saveAsPDFCheckBox;
	
	

	public DwSVGExportDialog(Shell parentShell, IFile file) {
		super(parentShell);
		
		profileManager = new DwSVGProfileManager();
		
		imageFile = file;
		if(activeProfile == null) {
			activeProfileName = profileManager.getDefaultProfileName();
			activeProfile = profileManager.getDefaultProfile();
		}
	}
	
	
	
	@Override
	protected boolean isResizable() {
		return false;
	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("DarwinSPL SVG Export Settings");
	}
	
	

	@Override
	public void create() {
		super.create();
		fileNameText.selectAll();
		fileNameText.setSelection(0);
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, "Export", true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		
		GridLayout containerLayout = new GridLayout(2, false);
		containerLayout.marginTop = 10;
		containerLayout.verticalSpacing = 10;
		containerLayout.horizontalSpacing = 30;
		containerLayout.marginHeight = 10;
		containerLayout.marginWidth = 20;
		container.setLayout(containerLayout);
		
		// Create elements
		
		createLabel(container, "Active Settings Profile:");
		activeProfileCombo = createCombo(container);
		newProfileText = createText(container);
		newProfileText.setMessage("New Profile Name");
		saveNewProfileButton = createButton(container, "Save New Profile");
		saveNewProfileButton.setToolTipText("SVG Export Setting Profiles are stored within your Eclipse workspace.");

		createSeperator(container);
		
		Label fileNameLabel = createLabel(container, "File Name (*.svg):");
		fileNameLabel.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false, 2, 1));
		fileNameText = createText(container);
		fileNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		
		storeEntireHistoryCheckBox = createCheckBoxButton(container, "Export entire evolution history (will create one graphic for each time point)");
		
		createSeperator(container);

		createLabel(container, "Layout Mode:");
		layoutModeCombo = createCombo(container, "Top-Down", "Left-Right");
		createLabel(container, "Image Margin:");
		globalMarginText = createText(container);
		createLabel(container, "Line Width:");
		lineWidthText = createText(container);
		
		createSeperator(container);

		createLabel(container, "Text and Line Color:");
		fgColorButton = createButton(container, "Change Color");
		createLabel(container, "Fill Color:");
		bgColorButton = createButton(container, "Change Color");
		
		createSeperator(container);

		createLabel(container, "Feature Name Font:");
		fontButton = createButton(container);
		createLabel(container, "Feature Name Padding X:");
		featureNodeTextMarginXText = createText(container);
		createLabel(container, "Feature Name Padding Y:");
		featureNodeTextMarginYText = createText(container);
		
		createSeperator(container);
		
		createLabel(container, "Feature Edge Radius:");
		roundBoxesRadiusText = createText(container);
		createLabel(container, "Minimum Feature Width:");
		featureNodeMinimumWidthText = createText(container);
		
		createSeperator(container);
		
		createLabel(container, "Gap Between Features:");
		gapText = createText(container);
		createLabel(container, "Gap Between Tree Levels:");
		gapBetweenLevelsText = createText(container);
		
		createSeperator(container);

		createLabel(container, "Feature Type Circle Radius:");
		featureVariationTypeCircleRadiusText = createText(container);
		createLabel(container, "Group Type Arc Radius:");
		groupVariationTypeArcRadiusText = createText(container);
		
		createSeperator(container);

		saveAsPDFCheckBox = createCheckBoxButton(container, "Also Save as PDF");
//		convertTextToPathsCheckBox = createCheckBoxButton(container, "Convert Text to Paths");
		
		// Set initial values
		
		initProfileArea();
		
		fileNameText.setText(imageFile.getName());
		fileNameText.selectAll();
		fileNameText.setSelection(0);
		
		updateDataMask();
		
		// Register button listeners
		
		activeProfileCombo.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				activeProfileName = activeProfileCombo.getItem(activeProfileCombo.getSelectionIndex());
				activeProfile = profileManager.getProfile(activeProfileName);
				if(activeProfile == null) {
					activeProfile = new DwSVGProfile();
					updateProfileArea();
				}
				
				newProfileText.setEnabled(false);
				updateDataMask();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		
		newProfileText.removeListener(SWT.CHANGED, newProfileText.getListeners(SWT.CHANGED)[0]);
		newProfileText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				String newProfileName = newProfileText.getText();
				if(newProfileName != null && newProfileName.trim() != "")
					saveNewProfileButton.setEnabled(true);
				else
					saveNewProfileButton.setEnabled(false);
			}
		});
		
		saveNewProfileButton.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				switch (e.type) {
				case SWT.Selection:
					storeActiveProfile();
					activeProfileName = newProfileText.getText().trim();
					profileManager.storeProfile(activeProfileName, activeProfile);
					
					if(activeProfileCombo.indexOf(activeProfileName) == -1)
						activeProfileCombo.add(activeProfileName);
					activeProfileCombo.select(activeProfileCombo.indexOf(activeProfileName));
					break;
				}
			}
		});
		
		storeEntireHistoryCheckBox.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				storeEntireHistory = storeEntireHistoryCheckBox.getSelection();
				fileNameText.setEnabled(!storeEntireHistory);
				fileNameLabel.setEnabled(!storeEntireHistory);
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		
		layoutModeCombo.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				updateProfileArea();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		
		fgColorButton.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				switch (e.type) {
				case SWT.Selection:
					ColorDialog dialog = new ColorDialog(getParentShell());
					RGB newColor = dialog.open();
					if(newColor != null) {
						currentFgColor = new Color(newColor.red, newColor.green, newColor.blue);
						updateColorButtons();
						updateProfileArea();
					}
					break;
				}
			}
		});
		
		bgColorButton.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				switch (e.type) {
				case SWT.Selection:
					ColorDialog dialog = new ColorDialog(getParentShell());
					RGB newColor = dialog.open();
					if(newColor != null) {
						currentBgColor = new Color(newColor.red, newColor.green, newColor.blue);
						updateColorButtons();
						updateProfileArea();
					}
					break;
				}
			}
		});
		
		fontButton.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				switch (e.type) {
				case SWT.Selection:
					FontDialog fontDialog = new FontDialog(getParentShell());
					fontDialog.setFontList(new FontData[] { new FontData(currentFont.getName(), currentFont.getSize(), currentFont.getStyle()) });
					FontData newFontData = fontDialog.open();
					if(newFontData != null && !newFontData.getName().equals("")) {
						updateFontButton(newFontData.getName(), newFontData.getStyle(), newFontData.getHeight());
//						parent.pack();
//						parent.layout();
						updateProfileArea();
					}
					break;
				}
			}
		});
		
		return container;
	}
	
	

	private void initProfileArea() {
		Map<String, DwSVGProfile> allProfiles = profileManager.getProfiles();
		activeProfileCombo.add("*");
		for(String profileName : allProfiles.keySet())
			activeProfileCombo.add(profileName);
		
		String activeProfileName = profileManager.getProfileName(activeProfile);
		if(activeProfileName == null)
			activeProfileCombo.select(0);
		else
			activeProfileCombo.select(activeProfileCombo.indexOf(activeProfileName));

		newProfileText.setEnabled(false);
		saveNewProfileButton.setEnabled(false);
	}
	
	private void updateProfileArea() {
		activeProfileCombo.select(0);
		newProfileText.setText(activeProfileName);
		newProfileText.setEnabled(true);
	}
	
	private void updateDataMask() {
		updateFontButton(activeProfile.font.getName(), activeProfile.font.getStyle(), activeProfile.font.getSize());

		currentBgColor = activeProfile.backgroundColor;
		currentFgColor = activeProfile.foregroundColor;
		updateColorButtons();

		layoutModeCombo.select(activeProfile.topDownOrientation ? 0 : 1);
		globalMarginText.setText(String.valueOf(activeProfile.globalMargin));
		lineWidthText.setText(String.valueOf(activeProfile.lineWidth));
		featureNodeTextMarginXText.setText(String.valueOf(activeProfile.featureNodeTextMarginX));
		featureNodeTextMarginYText.setText(String.valueOf(activeProfile.featureNodeTextMarginY));
		featureNodeMinimumWidthText.setText(String.valueOf(activeProfile.featureNodeMinimumWidth));
		roundBoxesRadiusText.setText(String.valueOf(activeProfile.roundBoxesRadius));
		gapText.setText(String.valueOf(activeProfile.gap));
		gapBetweenLevelsText.setText(String.valueOf(activeProfile.gapBetweenLevels));
		featureVariationTypeCircleRadiusText.setText(String.valueOf(activeProfile.featureVariationTypeCircleRadius));
		groupVariationTypeArcRadiusText.setText(String.valueOf(activeProfile.groupVariationTypeArcRadius));
		saveAsPDFCheckBox.setSelection(activeProfile.saveAsPDF);
//		convertTextToPathsCheckBox.setSelection(activeProfile.convertTextToPaths);
	}
	
	private void updateColorButtons() {
		fgColorButton.setBackground(new org.eclipse.swt.graphics.Color(Display.getCurrent(), currentBgColor.getRed(), currentBgColor.getGreen(), currentBgColor.getBlue()));
		fgColorButton.setForeground(new org.eclipse.swt.graphics.Color(Display.getCurrent(), currentFgColor.getRed(), currentFgColor.getGreen(), currentFgColor.getBlue()));

		bgColorButton.setBackground(new org.eclipse.swt.graphics.Color(Display.getCurrent(), currentBgColor.getRed(), currentBgColor.getGreen(), currentBgColor.getBlue()));
		bgColorButton.setForeground(new org.eclipse.swt.graphics.Color(Display.getCurrent(), currentFgColor.getRed(), currentFgColor.getGreen(), currentFgColor.getBlue()));
	}
	


	private void updateFontButton(String fontName, int fontStyle, int fontHeight) {
		currentFont = new Font(fontName, fontStyle, fontHeight);
		
		fontButton.setText(currentFont.getName());
		fontButton.setFont(new org.eclipse.swt.graphics.Font(Display.getCurrent(), fontName, fontStyle, fontHeight));
	}

	private Combo createCombo(Composite container, String ... entries) {
		Combo combo = new Combo(container, SWT.DROP_DOWN | SWT.READ_ONLY | SWT.SIMPLE);
		for(String entry : entries)
			combo.add(entry);
		
		GridData layoutData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		combo.setLayoutData(layoutData);
		
		return combo;
	}

	private Button createCheckBoxButton(Composite container, String label) {
		Button button = new Button(container, SWT.CHECK | SWT.BEGINNING);
		button.setText(label);

		GridData layoutData = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		button.setLayoutData(layoutData);
		
		button.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				updateProfileArea();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		
		return button;
	}

	private Button createButton(Composite container, String text) {
		Button button = createButton(container);
		button.setText(text);
		return button;
	}

	private Button createButton(Composite container) {
		Button button = new Button(container, SWT.PUSH | SWT.CENTER);
		
		GridData layoutData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		layoutData.minimumWidth = 80;
		button.setLayoutData(layoutData);
		
		button.addListener(SWT.CHANGED, new Listener() {
			@Override
			public void handleEvent(Event event) {
				updateProfileArea();
			}
		});
		
		return button;
	}

	private Label createLabel(Composite container, String text) {
		Label label = new Label(container, SWT.LEFT);
		label.setText(text);
		
		GridData layoutData = new GridData(SWT.BEGINNING, SWT.CENTER, false, false, 1, 1);
		label.setLayoutData(layoutData);
		
		return label;
	}
	
	private Text createText(Composite container) {
		Text text = new Text(container, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		
		GridData layoutData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		text.setLayoutData(layoutData);
		
		text.addListener(SWT.CHANGED, new Listener() {
			@Override
			public void handleEvent(Event event) {
				updateProfileArea();
			}
		});
		
		return text;
	}
	
	private void createSeperator(Composite container) {
		Label seperatorLabel = new Label(container, SWT.SEPARATOR | SWT.HORIZONTAL);
		
		GridData layoutData = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		seperatorLabel.setLayoutData(layoutData);
	}
	
	
	
	@Override
	protected void okPressed() {
		storeActiveProfile();
		super.okPressed();
	}
	
	
	
	private void storeActiveProfile() {
		activeProfile = new DwSVGProfile();
		
		activeProfile.font = currentFont;
		activeProfile.saveAsPDF = this.saveAsPDFCheckBox.getSelection();
//		activeProfile.convertTextToPaths = this.convertTextToPathsCheckBox.getSelection();
		
		activeProfile.backgroundColor = currentBgColor;
		activeProfile.foregroundColor = currentFgColor;
		
		fileName = fileNameText.getText();
		if(!fileName.endsWith(".svg"))
			fileName += ".svg";

		activeProfile.topDownOrientation = layoutModeCombo.getSelectionIndex() == 0 ? true : false;
		
		activeProfile.globalMargin = Integer.valueOf(globalMarginText.getText());
		activeProfile.lineWidth = Float.valueOf(lineWidthText.getText());
		activeProfile.featureNodeTextMarginX = Integer.valueOf(featureNodeTextMarginXText.getText());
		activeProfile.featureNodeTextMarginY = Integer.valueOf(featureNodeTextMarginYText.getText());
		activeProfile.featureNodeMinimumWidth = Integer.valueOf(featureNodeMinimumWidthText.getText());
		activeProfile.roundBoxesRadius = Integer.valueOf(roundBoxesRadiusText.getText());
		activeProfile.gap = Integer.valueOf(gapText.getText());
		activeProfile.gapBetweenLevels = Integer.valueOf(gapBetweenLevelsText.getText());
		activeProfile.featureVariationTypeCircleRadius = Integer.valueOf(featureVariationTypeCircleRadiusText.getText());
		activeProfile.groupVariationTypeArcRadius = Integer.valueOf(groupVariationTypeArcRadiusText.getText());
	}
	
	
	
	public DwSVGProfile getActiveProfile() {
		return activeProfile;
	}
	
	/**
	 * 
	 * @return the output folder or file to write to
	 * @throws CoreException if folder creation fails
	 */
	public IResource getOutputResource() throws IOException {
		if(storeEntireHistory) {
			IFolder folder = imageFile.getParent().getFolder(new Path("exported_history"));
			try {
				if(folder.exists()) {
					folder.delete(true, null);
				}
				folder.create(true, false, null);
			} catch (CoreException e) {
				throw new IOException("Creation of output folder failed.");
			}
			
			return folder;
		}
		else {
			return ResourceUtil.getFileInSameContainer(imageFile, fileName);
		}
	}

}















