package de.darwinspl.exporter.svg;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.abego.treelayout.util.AbstractTreeForTreeLayout;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.util.custom.DwFeatureUtil;

public class DwFeatureTreeForTreeLayoutSVG extends AbstractTreeForTreeLayout<DwFeature> {

	protected Date date;
	
	public DwFeatureTreeForTreeLayoutSVG(DwFeature root, Date date) {
		super(root);
		
		this.date = date;
	}

	@Override
	public List<DwFeature> getChildrenList(DwFeature feature) {
		List<DwFeature> children = new ArrayList<DwFeature>();

		if (feature == null) {
			return children;
		}
		
		children = DwFeatureUtil.getChildFeaturesOfFeature(feature, date);

		return children;
	}

	@Override
	public DwFeature getParent(DwFeature node) {
		return DwFeatureUtil.getParentFeatureOfFeature(node, date);
	}

}
