package de.darwinspl.exporter.svg;

import java.awt.Color;
import java.awt.Font;

public class DwSVGProfile {
	
	public boolean topDownOrientation = true;
	
	public int globalMargin = 4;
	public float lineWidth = 4.0f;

	public Color foregroundColor = new Color(0, 0, 0);
	public Color backgroundColor = new Color(255, 255, 255);

	public Font font = new Font("Linux Biolinum", Font.PLAIN, 48);

	public int featureNodeTextMarginX = 20;
	public int featureNodeTextMarginY = 10;
	public int featureNodeMinimumWidth = 150;
	
	public int roundBoxesRadius = 50;
	
	public int gapBetweenLevels = 80;
	public int gap = 30;

	public int featureVariationTypeCircleRadius = 12;
	public int groupVariationTypeArcRadius = 50;

//	public boolean convertTextToPaths = true;
	public boolean saveAsPDF = false;

}
