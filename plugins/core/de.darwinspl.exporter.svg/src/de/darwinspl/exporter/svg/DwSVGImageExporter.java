package de.darwinspl.exporter.svg;

import java.awt.BasicStroke;
import java.awt.Point;
import java.awt.font.GlyphVector;
import java.awt.geom.Rectangle2D;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.abego.treelayout.Configuration;
import org.abego.treelayout.Configuration.AlignmentInLevel;
import org.abego.treelayout.Configuration.Location;
import org.abego.treelayout.NodeExtentProvider;
import org.abego.treelayout.TreeForTreeLayout;
import org.abego.treelayout.TreeLayout;
import org.abego.treelayout.util.DefaultConfiguration;
import org.apache.batik.transcoder.Transcoder;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.fop.svg.PDFTranscoder;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.draw2d.geometry.Rectangle;
import org.jfree.graphics2d.svg.SVGGraphics2D;
import org.jfree.graphics2d.svg.SVGUtils;

import de.christophseidl.util.eclipse.ResourceUtil;
import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureTypeEnum;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalEditor;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalViewer;
import de.darwinspl.feature.graphical.base.figures.DwFeatureFigure;
import de.darwinspl.feature.graphical.base.util.DwGeometryUtil;
import de.darwinspl.feature.graphical.base.util.export.DwAbstractImageExporter;
import de.darwinspl.feature.util.custom.DwFeatureUtil;
import de.darwinspl.temporal.util.DwDateResolverUtil;
import de.darwinspl.temporal.util.DwEvolutionUtil;
//import sun.font.StandardGlyphVector;

public class DwSVGImageExporter extends DwAbstractImageExporter {
	
	private DwTemporalFeatureModel tfm;
	private Date date;
	
	private DwSVGProfile profile;
	
	private TreeLayout<DwFeature> treeLayout;
	
	private Map<DwFeature, Rectangle2D.Double> allFeatureBounds;
	
	

	public DwSVGImageExporter() {
		super();
	}


	@Override
	public void exportImage(DwGraphicalViewer viewer) throws IOException {
		DwGraphicalEditor editor = viewer.getGraphicalEditor();
		tfm = editor.getFeatureModel();
		date = editor.getCurrentSelectedDate();

		IFile inputFile = createImageFileFromEditorInput(viewer, "svg");
		if (inputFile == null) {
			throw new InvalidParameterException("File must be specified");
		}
		
		DwSVGExportDialog dialog = new DwSVGExportDialog(editor.getSite().getShell(), inputFile);
		if(dialog.open() != 0)
			return;
		profile = dialog.getActiveProfile();
		IResource outputResource = dialog.getOutputResource();
		
		if(outputResource instanceof IFolder) {
			IFolder outputFolder = (IFolder) outputResource;
			
			List<Date> dates = DwEvolutionUtil.collectDates(editor.getFeatureModel());
			if(!dates.contains(DwDateResolverUtil.INITIAL_STATE_DATE)) {
				dates.add(0, DwDateResolverUtil.INITIAL_STATE_DATE);
			}
			
			for(Date currentDate : dates) {
				this.date = currentDate;
				String index = String.format ("%03d", dates.indexOf(date));
				String fileName = index + "-" + DwDateResolverUtil.deresolveDate(date, new SimpleDateFormat("yyyy-MM-dd")) + ".svg";
				IFile outputFile = outputFolder.getFile(fileName);
				exportFile(outputFile);
			}
		}
		else if(outputResource instanceof IFile) {
			exportFile((IFile) outputResource);
		}
		
		ResourceUtil.refreshResourceRecursively(inputFile.getParent());
	}
	

	private void exportFile(IFile imageFile) throws IOException {
		if(!imageFile.exists()) {
			try {
				imageFile.create(new ByteArrayInputStream("".getBytes()), true, null);
			} catch (CoreException e) {
				throw new IOException("Creating new image file \"" + imageFile.getName() + "\" did not work: " + e.getMessage());
			}
		}
		
		TreeForTreeLayout<DwFeature> tree = new DwFeatureTreeForTreeLayoutSVG(DwFeatureUtil.getRootFeature(tfm, date), date);
		NodeExtentProvider<DwFeature> extents = new DwFeatureNodeExtentProviderSVG(profile, date);
		Location orientationMode = profile.topDownOrientation ? Configuration.Location.Top : Configuration.Location.Left;
		DefaultConfiguration<DwFeature> configuration = new DefaultConfiguration<DwFeature>(profile.gapBetweenLevels, profile.gap, orientationMode, AlignmentInLevel.TowardsRoot);
		treeLayout = new TreeLayout<DwFeature>(tree, extents, configuration);

		Point graphicsSize = calculateGraphicsSize();
		SVGGraphics2D g = new SVGGraphics2D(graphicsSize.x, graphicsSize.y);
		g.setFont(profile.font);
		g.setStroke(new BasicStroke(profile.lineWidth));
		
		allFeatureBounds = treeLayout.getNodeBounds();
		
		List<Object> modelChildren = getModelChildren();

		for(Object modelChild : modelChildren)
			if(modelChild instanceof DwGroup)
				drawGroup(g, (DwGroup) modelChild);
		
		for(Object modelChild : modelChildren)
			if(modelChild instanceof DwFeature)
				drawFeature(g, (DwFeature) modelChild);
		
		File svgFile = imageFile.getRawLocation().makeAbsolute().toFile();
		SVGUtils.writeToSVG(svgFile, g.getSVGElement());
		
		if(profile.saveAsPDF) {
			Transcoder transcoder = new PDFTranscoder();
	        TranscoderInput transcoderInput = new TranscoderInput(new FileInputStream(svgFile));
	        String pdfPath = svgFile.getAbsolutePath();
	        pdfPath = pdfPath.substring(0, pdfPath.length()-3) + "pdf";
	        TranscoderOutput transcoderOutput = new TranscoderOutput(new FileOutputStream(new File(pdfPath)));
	        try {
				transcoder.transcode(transcoderInput, transcoderOutput);
			} catch (TranscoderException e) {
				e.printStackTrace();
			}
	        finally {
	        	transcoderInput.getInputStream().close();
	        	transcoderOutput.getOutputStream().close();
	        }
		}
	}


	private Point calculateGraphicsSize() {
		Rectangle2D nodeBounds = treeLayout.getBounds();
		Rectangle modelBounds = DwGeometryUtil.rectangle2DToDraw2DRectangle(nodeBounds);
		return new Point(modelBounds.width + 2*profile.globalMargin, modelBounds.height + 2*profile.globalMargin);
	}
	


	private void drawGroup(SVGGraphics2D g, DwGroup group) {
		DwFeature parentFeature = DwFeatureUtil.getParentFeatureOfGroup(group, date);
		
		Rectangle parentBounds = calculateBounds(parentFeature);
		int startX;
		int startY;
		if(profile.topDownOrientation) {
			startX = parentBounds.x + (parentBounds.width / 2);
			startY = parentBounds.y + parentBounds.height + (int) (profile.lineWidth/2);
		}
		else {
			startX = parentBounds.x + parentBounds.width + (int) (profile.lineWidth/2);
			startY = parentBounds.y + (parentBounds.height / 2);
		}
		Point startPoint = new Point(startX, startY);

		List<DwFeature> features = DwFeatureUtil.getChildFeaturesOfGroup(group, date);
		
		if(groupTypeVisible(group)) {
			int circleWidth = profile.groupVariationTypeArcRadius*2;
			int circleHeight = profile.groupVariationTypeArcRadius*2;
			
			int circleX = startX-circleWidth/2;
			int circleY = startY-circleWidth/2;
			
			//Here is the problem: Trigonometric functions assume a Cartesian coordinate system with y coordinate extending towards the top.
			//However, drawing canvas has a Cartesian coordinate system where y extends towards the bottom.
			//Hence, coordinates have to be mirrored along the y-axis before being used as input to the trigonometric functions.
			
			Point mirroredOriginPoint = new Point(startPoint);
			mirroredOriginPoint.y = -mirroredOriginPoint.y;
			
			Rectangle leftestChildBounds = calculateBounds(features.get(0));
			Point mirroredLeftLineEndPoint;
			if(profile.topDownOrientation)
				mirroredLeftLineEndPoint = new Point(leftestChildBounds.x + (leftestChildBounds.width / 2), leftestChildBounds.y);
			else
				mirroredLeftLineEndPoint = new Point(leftestChildBounds.x, leftestChildBounds.y + (leftestChildBounds.height / 2));
			mirroredLeftLineEndPoint.y = -mirroredLeftLineEndPoint.y;

			Rectangle rightestChildBounds = calculateBounds(features.get(features.size()-1));
			Point mirroredRightLineEndPoint;
			if(profile.topDownOrientation)
				mirroredRightLineEndPoint = new Point(rightestChildBounds.x + (rightestChildBounds.width / 2), rightestChildBounds.y);
			else
				mirroredRightLineEndPoint = new Point(rightestChildBounds.x, rightestChildBounds.y + (rightestChildBounds.height / 2));
			mirroredRightLineEndPoint.y = -mirroredRightLineEndPoint.y;
			
			double rawAngle1 = calculateRotationAngleForLineWithEndpoints(mirroredOriginPoint, mirroredLeftLineEndPoint);
			double rawAngle2 = calculateRotationAngleForLineWithEndpoints(mirroredOriginPoint, mirroredRightLineEndPoint);

			double degreeAngle1 = Math.toDegrees(rawAngle1);
			double degreeAngle2 = Math.toDegrees(rawAngle2);
			
			int angle1 = (int) Math.ceil(degreeAngle1);
			int angle2 = (int) Math.floor(degreeAngle2);
			
			if(group.isOr(date))
				g.setColor(profile.foregroundColor);
			else
				g.setColor(profile.backgroundColor);
			
			// Fill the group type arc
			int leftDeltaX = mirroredLeftLineEndPoint.x - startX;
			int leftDeltaY = -mirroredLeftLineEndPoint.y - startY;
			double leftHypo = Math.sqrt(Math.pow(leftDeltaX, 2) + Math.pow(leftDeltaY, 2));
			double leftRatio = (profile.groupVariationTypeArcRadius + profile.lineWidth/2) / leftHypo;
			leftDeltaX = (int) Math.floor(leftDeltaX*leftRatio);
			leftDeltaY = (int) Math.ceil(leftRatio*leftDeltaY);
			
			int rightDeltaX = mirroredRightLineEndPoint.x - startX;
			int rightDeltaY = -mirroredRightLineEndPoint.y - startY;
			double rightHypo = Math.sqrt(Math.pow(rightDeltaX, 2) + Math.pow(rightDeltaY, 2));
			double rightRatio = (profile.groupVariationTypeArcRadius + profile.lineWidth/2) / rightHypo;
			rightDeltaX = (int) Math.ceil(rightRatio*rightDeltaX);
			rightDeltaY = (int) Math.ceil(rightRatio*rightDeltaY);
			
			g.fillPolygon(new int[] {startX, startX+leftDeltaX, startX+rightDeltaX}, new int[] {startY, startY+leftDeltaY, startY+rightDeltaY}, 3);
			g.fillArc(circleX, circleY, circleWidth, circleHeight, angle1, angle2 - angle1);

			g.setColor(profile.foregroundColor);
			g.drawArc(circleX, circleY, circleWidth, circleHeight, angle1, angle2 - angle1);
		}
		
		for (DwFeature feature : features) {
			Rectangle childBounds = calculateBounds(feature);

			int endX;
			int endY;
			if(profile.topDownOrientation) {
				endX = childBounds.x + (childBounds.width / 2);
				endY = childBounds.y;
			}
			else {
				endX = childBounds.x;
				endY = childBounds.y + (childBounds.height / 2);
			}
			
			if (DwFeatureFigure.variationTypeCircleVisible(feature, date)) {
				if(profile.topDownOrientation)
					endY -= profile.featureVariationTypeCircleRadius;
				else
					endX -= profile.featureVariationTypeCircleRadius;
			}

			g.setColor(profile.foregroundColor);
			g.drawLine(startX, startY, endX, endY);
		}
	}
	
	private double calculateRotationAngleForLineWithEndpoints(Point startPoint, Point endPoint) {
		double deltaX = endPoint.x - startPoint.x;
		double deltaY = endPoint.y - startPoint.y;
		
		return Math.atan2(deltaY, deltaX);
	}
	
	private boolean groupTypeVisible(DwGroup group) {
		if (group.isAlternative(date) || group.isOr(date)) {
			List<DwFeature> features = DwFeatureUtil.getChildFeaturesOfGroup(group, date);
			
			return features.size() >= 2;
		}
		
		return false;
	}
	
	

	private void drawFeature(SVGGraphics2D g, DwFeature feature) {
		Rectangle featureBounds = calculateBounds(feature);
		
		if (DwFeatureFigure.variationTypeCircleVisible(feature, date)) {
			int variationCircleDiameter = profile.featureVariationTypeCircleRadius*2;

			int variationCircleX;
			int variationCircleY;
			
			if(profile.topDownOrientation) {
				variationCircleX = featureBounds.x + featureBounds.width/2 - variationCircleDiameter/2;
				variationCircleY = featureBounds.y - variationCircleDiameter;
			}
			else {
				variationCircleX = featureBounds.x - variationCircleDiameter;
				variationCircleY = featureBounds.y + featureBounds.height/2 - variationCircleDiameter/2;
			}
			
			DwFeatureTypeEnum type = DwFeatureUtil.getType(feature, date).getType();
			if(type.equals(DwFeatureTypeEnum.OPTIONAL))
				g.setColor(profile.backgroundColor);
			else
				g.setColor(profile.foregroundColor);
			g.fillOval(variationCircleX, variationCircleY, variationCircleDiameter, variationCircleDiameter);
			
			g.setColor(profile.foregroundColor);
			g.drawOval(variationCircleX, variationCircleY, variationCircleDiameter, variationCircleDiameter);
		}
		
		g.setColor(profile.backgroundColor);
		g.fillRoundRect(featureBounds.x, featureBounds.y, featureBounds.width, featureBounds.height, profile.roundBoxesRadius, profile.roundBoxesRadius);
		g.setColor(profile.foregroundColor);
		g.drawRoundRect(featureBounds.x, featureBounds.y, featureBounds.width, featureBounds.height, profile.roundBoxesRadius, profile.roundBoxesRadius);
		
		String name = DwFeatureUtil.getName(feature, date).getName();
		Rectangle2D nameBounds = g.getFontMetrics().getStringBounds(name, g);
		
		int namePositionX = featureBounds.x;
		namePositionX += featureBounds.width/2;
		namePositionX -= nameBounds.getWidth()/2;
		
		int namePositionY = featureBounds.y;
		namePositionY += featureBounds.height/2;
		namePositionY += 1.5d * (nameBounds.getHeight() + nameBounds.getY());
		
//		if(!profile.convertTextToPaths) {
			g.drawString(name, namePositionX, namePositionY);
//		} else {
//			GlyphVector glyph = new StandardGlyphVector(profile.font, name, g.getFontRenderContext());
//			g.drawGlyphVector(glyph, namePositionX, namePositionY);
//		}
	}

	private Rectangle calculateBounds(DwFeature feature) {
		Rectangle2D.Double nodeBounds = allFeatureBounds.get(feature);
		return new Rectangle((int) nodeBounds.x + profile.globalMargin, (int) nodeBounds.y + profile.globalMargin, (int) nodeBounds.width, (int) nodeBounds.height);
	}
	
	

	private List<Object> getModelChildren() {
		List<Object> modelChildren = new ArrayList<Object>();
		traverseModel(modelChildren, tfm);
		return modelChildren;
	}
	
	private boolean traverseModel(List<Object> modelChildren, DwTemporalFeatureModel featureModel) {
		DwFeature rootFeature = DwFeatureUtil.getRootFeature(featureModel, date);
		if (rootFeature == null) {
			return false;
		}

		modelChildren.add(rootFeature);

		List<DwGroup> groups = DwFeatureUtil.getChildGroupsOfFeature(rootFeature, date);
		
		if (!groups.isEmpty()) {
			collectGroups(modelChildren, rootFeature);
			collectFeatures(modelChildren, groups);
		}

		return true;
	}
	
	private void collectFeatures(List<Object> modelChildren, List<DwGroup> groups) {
		for (DwGroup group : groups) {
			for (DwFeature feature : DwFeatureUtil.getChildFeaturesOfGroup(group, date)) {
				modelChildren.add(feature);
				collectFeatures(modelChildren, DwFeatureUtil.getChildGroupsOfFeature(feature, date));
			}
		}
	}
	
	private void collectGroups(List<Object> modelChildren, DwFeature rootFeature) {
		for (DwGroup group : DwFeatureUtil.getChildGroupsOfFeature(rootFeature, date)) {
			modelChildren.add(group);

			for (DwFeature currentFeature : DwFeatureUtil.getChildFeaturesOfGroup(group, date)) {
				collectGroups(modelChildren, currentFeature);
			}
		}
	}

}




















