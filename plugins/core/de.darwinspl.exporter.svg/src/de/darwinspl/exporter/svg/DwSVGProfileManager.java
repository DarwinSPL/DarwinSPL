package de.darwinspl.exporter.svg;

import java.awt.Color;
import java.awt.Font;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;

public class DwSVGProfileManager {
	
	private static final String ROOT_NODE_NAME = "de.darwinspl.svg.profiles";
	private static final String DEFAULT_PROFILE_NAME = "Default Top Down";
	
	private Map<String, DwSVGProfile> allProfiles = new HashMap<>();
	
	
	
	public DwSVGProfileManager() {
	}
	
	
	
	public String getDefaultProfileName() {
		return DEFAULT_PROFILE_NAME;
	}

	public String getProfileName(DwSVGProfile activeProfile) {
		for(String profileName : allProfiles.keySet()) {
			if(activeProfile.equals(allProfiles.get(profileName)))
				return profileName;
		}
		return null;
	}
	


	public DwSVGProfile getDefaultProfile() {
		return getProfiles().get(DEFAULT_PROFILE_NAME);
	}

	public DwSVGProfile getProfile(String profileName) {
		return allProfiles.get(profileName);
	}
	
	public Map<String, DwSVGProfile> getProfiles() {
		IEclipsePreferences pref = InstanceScope.INSTANCE.getNode(ROOT_NODE_NAME);
		
		String[] nodeChildren;
		try {
			nodeChildren = pref.childrenNames();
		} catch (BackingStoreException e) {
			e.printStackTrace();
			return getDefaultProfiles();
		}		
		
		if(nodeChildren.length == 0) {
			storeDefaultProfiles();
			return getProfiles();
		}
		
		DwSVGProfile defaultProfile = new DwSVGProfile();
		
		for(String childName : nodeChildren) {
			if(allProfiles.containsKey(childName))
				continue;
			
			Preferences childPref = pref.node(childName);
			
			DwSVGProfile newProfile = new DwSVGProfile();

			newProfile.foregroundColor = Color.decode(childPref.get("foregroundColor", "#000000"));
			newProfile.backgroundColor = Color.decode(childPref.get("backgroundColor", "#FFFFFF"));
			
//			newProfile.convertTextToPaths = childPref.getBoolean("convertTextToPath", defaultProfile.convertTextToPaths);
			newProfile.featureNodeMinimumWidth = childPref.getInt("featureNodeMinimumWidth", defaultProfile.featureNodeMinimumWidth);
			newProfile.featureNodeTextMarginX = childPref.getInt("featureNodeTextMarginX", defaultProfile.featureNodeTextMarginX);
			newProfile.featureNodeTextMarginY = childPref.getInt("featureNodeTextMarginY", defaultProfile.featureNodeTextMarginY);
			newProfile.featureVariationTypeCircleRadius = childPref.getInt("featureVariationTypeCircleRadius", defaultProfile.featureVariationTypeCircleRadius);
			String fontName = childPref.get("fontName", null);
			int fontStyle = childPref.getInt("fontStyle", -1);
			int fontSize = childPref.getInt("fontSize", -1);
			if(fontName == null || fontStyle == -1 || fontSize == -1)
				newProfile.font = defaultProfile.font;
			else
				newProfile.font = new Font(fontName, fontStyle, fontSize);
			newProfile.gap = childPref.getInt("gap", defaultProfile.gap);
			newProfile.gapBetweenLevels = childPref.getInt("gapBetweenLevels", defaultProfile.gapBetweenLevels);
			newProfile.globalMargin = childPref.getInt("globalMargin", defaultProfile.globalMargin);
			newProfile.groupVariationTypeArcRadius = childPref.getInt("groupVariationTypeArcRadius", defaultProfile.groupVariationTypeArcRadius);
			newProfile.lineWidth = childPref.getFloat("lineWidth", defaultProfile.lineWidth);
			newProfile.roundBoxesRadius = childPref.getInt("roundBoxesRadius", defaultProfile.roundBoxesRadius);
			newProfile.saveAsPDF = childPref.getBoolean("saveAsPDF", defaultProfile.saveAsPDF);
			newProfile.topDownOrientation = childPref.getBoolean("topDownOrientation", defaultProfile.topDownOrientation);
			
			allProfiles.put(childName, newProfile);
		}
		
		return allProfiles;
	}
	
	public void storeProfile(String profileName, DwSVGProfile profile) {
		allProfiles.put(profileName, profile);
		
		IEclipsePreferences rootPref = InstanceScope.INSTANCE.getNode(ROOT_NODE_NAME);
		Preferences pref = rootPref.node(profileName);

		pref.put("topDownOrientation", String.valueOf(profile.topDownOrientation));
		
		pref.put("globalMargin", String.valueOf(profile.globalMargin));
		pref.put("lineWidth", String.valueOf(profile.globalMargin));

		String hexForeground = String.format("#%02X%02X%02X", profile.foregroundColor.getRed(), profile.foregroundColor.getGreen(), profile.foregroundColor.getBlue());  
		pref.put("foregroundColor", hexForeground);
		String hexBackground = String.format("#%02X%02X%02X", profile.backgroundColor.getRed(), profile.backgroundColor.getGreen(), profile.backgroundColor.getBlue());  
		pref.put("backgroundColor", hexBackground);
		
		pref.put("fontName", profile.font.getName());
		pref.put("fontStyle", String.valueOf(profile.font.getStyle()));
		pref.put("fontSize", String.valueOf(profile.font.getSize()));
		
		pref.put("featureNodeTextMarginX", String.valueOf(profile.featureNodeTextMarginX));
		pref.put("featureNodeTextMarginY", String.valueOf(profile.featureNodeTextMarginY));
		pref.put("featureNodeMinimumWidth", String.valueOf(profile.featureNodeMinimumWidth));
		
		pref.put("roundBoxesRadius", String.valueOf(profile.roundBoxesRadius));
		
		pref.put("gapBetweenLevels", String.valueOf(profile.gapBetweenLevels));
		pref.put("gap", String.valueOf(profile.gap));
		
		pref.put("featureVariationTypeCircleRadius", String.valueOf(profile.featureVariationTypeCircleRadius));
		pref.put("groupVariationTypeArcRadius", String.valueOf(profile.groupVariationTypeArcRadius));
		
//		pref.put("convertTextToPaths", String.valueOf(profile.convertTextToPaths));
		pref.put("saveAsPDF", String.valueOf(profile.saveAsPDF));
		
		try {
			pref.flush();
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}
	}

	private void storeDefaultProfiles() {
		Map<String, DwSVGProfile> defaultProfiles = getDefaultProfiles();

		for(String profileName : defaultProfiles.keySet()) {
			DwSVGProfile profile = defaultProfiles.get(profileName);
			storeProfile(profileName, profile);
		}
	}
	
	private Map<String, DwSVGProfile> getDefaultProfiles() {
		Map<String, DwSVGProfile> defaultProfiles = new HashMap<>();

		DwSVGProfile defaultTopDownProfile = new DwSVGProfile();
		defaultProfiles.put(DEFAULT_PROFILE_NAME, defaultTopDownProfile);
		
		DwSVGProfile defaultLeftRightProfile = new DwSVGProfile();
		defaultLeftRightProfile.topDownOrientation = false;
		defaultProfiles.put("Default Left Right", defaultLeftRightProfile);
		
		return defaultProfiles;
	}

}
