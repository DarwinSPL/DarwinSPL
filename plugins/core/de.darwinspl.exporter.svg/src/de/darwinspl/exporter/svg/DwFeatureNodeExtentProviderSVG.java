package de.darwinspl.exporter.svg;

import java.awt.geom.Rectangle2D;
import java.util.Date;

import org.abego.treelayout.NodeExtentProvider;
import org.jfree.graphics2d.svg.SVGGraphics2D;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.util.custom.DwFeatureUtil;

public class DwFeatureNodeExtentProviderSVG implements NodeExtentProvider<DwFeature> {
	
	private static SVGGraphics2D graphics;
	
	private Date date;
	private DwSVGProfile design;
	
	public DwFeatureNodeExtentProviderSVG(DwSVGProfile design, Date date) {
		this.date = date;
		this.design = design;
		
		graphics = new SVGGraphics2D(500, 500);
		graphics.setFont(design.font);
	}
	
	@Override
	public double getWidth(DwFeature feature) {
		String name = DwFeatureUtil.getName(feature, date).getName();
		int nameWidth = (int) Math.ceil(graphics.getFontMetrics().getStringBounds(name, graphics).getWidth()) + design.featureNodeTextMarginX*2;
		
		if(nameWidth < design.featureNodeMinimumWidth)
			return design.featureNodeMinimumWidth;
		return nameWidth;
	}

	@Override
	public double getHeight(DwFeature feature) {
		String name = DwFeatureUtil.getName(feature, date).getName();
		
		Rectangle2D fontBounds = graphics.getFontMetrics().getStringBounds(name, graphics);
		int height = (int) Math.ceil(fontBounds.getHeight());
		int y = (int) Math.ceil(fontBounds.getY());

//		int nameHeight = height + (height+y) + design.featureNodeTextMarginY*2;
		int nameHeight = -y + (height+y) + design.featureNodeTextMarginY*2;
		
		return nameHeight;
	}

}
