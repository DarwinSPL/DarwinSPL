package de.darwinspl.importer.evolution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;

import de.darwinspl.constraint.DwConstraintFactory;
import de.darwinspl.constraint.DwConstraintModel;
import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureType;
import de.darwinspl.feature.DwFeatureTypeEnum;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupType;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.evolution.operation.factory.DwSimpleEvolutionOperationFactory;
import de.darwinspl.feature.evolution.operation.interpreter.DwEvolutionOperationInterpreter;
import de.darwinspl.feature.operation.DwEvolutionOperation;
import de.darwinspl.feature.operation.DwFeatureCreateOperation;
import de.darwinspl.feature.operation.DwFeatureDeleteOperation;
import de.darwinspl.feature.operation.DwFeatureMoveOperation;
import de.darwinspl.feature.operation.DwFeatureRenameOperation;
import de.darwinspl.feature.operation.DwFeatureTypeChangeOperation;
import de.darwinspl.feature.operation.DwGroupTypeChangeOperation;
import de.darwinspl.feature.util.custom.DwSimpleFeatureFactoryCustom;
import de.darwinspl.feature.util.custom.DwFeatureUtil;
import de.darwinspl.importer.FeatureModelConstraintsTuple;
import de.darwinspl.temporal.util.DwEvolutionUtil;

public class DwFeatureModelEvolutionImporter3 {
	
	private DwSimpleEvolutionOperationFactory operationFactory = new DwSimpleEvolutionOperationFactory();
	
	// Collected operations
	private List<DwFeatureTypeChangeOperation> featureTypeChangeOperations;
	private List<DwGroupTypeChangeOperation> groupTypeChangeOperations;
	
	private List<DwFeatureMoveOperation> moveOperations;
	private List<DwFeatureDeleteOperation> deleteOperations;
	


	/**
	 * Merges multiple feature model evolution snapshots to one Temporal Feature
	 * Model
	 * 
	 * @param models
	 *            Each feature model has to be associated to a date at which the
	 *            changes have occured.
	 * @param monitor 
	 * @return The whole feature model history merged into one TFM.
	 * @throws Exception
	 */
	public FeatureModelConstraintsTuple importFeatureModelEvolutionSnapshots(Map<Date, FeatureModelConstraintsTuple> models, IProgressMonitor monitor) {
		DwFeature rootFeature = DwSimpleFeatureFactoryCustom.createNewFeature("RootFeature", DwFeatureTypeEnum.MANDATORY);
		DwTemporalFeatureModel mergedFeatureModel = DwSimpleFeatureFactoryCustom.createNewFeatureModel(rootFeature);
		
		DwConstraintModel mergedConstraintModel = DwConstraintFactory.eINSTANCE.createDwConstraintModel();

		List<Date> sortedDateList = new ArrayList<Date>(models.keySet());
		sortedDateList.remove(null);
		Collections.sort(sortedDateList);

		monitor.beginTask("Merging snapshots to evolution-aware temporal feature model...", models.size());
		monitor.subTask("Importing base model...");
		if (models.get(null) != null) {
			mergeModels(models.get(null), mergedFeatureModel, mergedConstraintModel, null);
		}
		
		monitor.worked(1);
		if(monitor.isCanceled())
			return null;

		for (Date date : sortedDateList) {
			monitor.subTask("Importing model at date " + date);
			mergeModels(models.get(date), mergedFeatureModel, mergedConstraintModel, date);
			
			monitor.worked(1);
			if(monitor.isCanceled())
				return null;
		}

		FeatureModelConstraintsTuple mergedModels = new FeatureModelConstraintsTuple(mergedFeatureModel, mergedConstraintModel);

		return mergedModels;
	}
	
	
	
	private void mergeModels(FeatureModelConstraintsTuple snapshotModels, DwTemporalFeatureModel evolutionTFM, DwConstraintModel mergedConstraintModel, Date date) {
		// Get root features of both models
		DwTemporalFeatureModel snapshotTFM = snapshotModels.getFeatureModel();
		DwFeature snapshotTFMRootFeature = DwFeatureUtil.getRootFeature(snapshotTFM, date);
		DwFeature evolutionTFMRootFeature = DwFeatureUtil.getRootFeature(evolutionTFM, date);
		
		// First, rename root feature if necessary
		renameRootFeature(snapshotTFMRootFeature, evolutionTFMRootFeature, date);
		
		featureTypeChangeOperations = new ArrayList<>();
		groupTypeChangeOperations = new ArrayList<>();
		moveOperations = new ArrayList<>();
		deleteOperations = new ArrayList<>();
		
		// Add additional structure while collecting all additional operations that are to be applied
		addFeaturesAndCollectAdditionalOperations(snapshotTFMRootFeature, evolutionTFMRootFeature, date);
		
		for(DwFeatureMoveOperation moveOperation : moveOperations) {
			moveOperation = operationFactory.createDwFeatureMoveOperation(moveOperation.getFeature().getFeatureModel(), moveOperation.getFeature(), moveOperation.getAddOperation().getParent(), date, true);
			executeOperation(moveOperation);
		}
		
		for(DwFeatureDeleteOperation deleteOperation : deleteOperations) {
			deleteOperation = operationFactory.createDwFeatureDeleteOperation(deleteOperation.getFeature().getFeatureModel(), deleteOperation.getFeature(), date, true);
			executeOperation(deleteOperation);
		}
	}
	
	
	
	private void renameRootFeature(DwFeature snapshotTFMRootFeature, DwFeature evolutionTFMRootFeature, Date date) {
		String snapshotTFMRootFeatureName = DwFeatureUtil.getName(snapshotTFMRootFeature, date).getName();
		String evolutionTFMRootFeatureName = DwFeatureUtil.getName(evolutionTFMRootFeature, date).getName();

		if(!snapshotTFMRootFeatureName.equals(evolutionTFMRootFeatureName)) {
			DwFeatureRenameOperation renameOp = operationFactory.createDwFeatureRenameOperation(evolutionTFMRootFeature.getFeatureModel(), evolutionTFMRootFeature, snapshotTFMRootFeatureName, date, true);
			DwEvolutionOperationInterpreter.execute(renameOp);
		}
	}
	
	
	
	private void addFeaturesAndCollectAdditionalOperations(DwFeature snapshotFeature, DwFeature evolutionFeature, Date date) {
		DwFeatureTypeChangeOperation featureTypeChangeOperation = createFeatureTypeChangeOperation(snapshotFeature, evolutionFeature, date);
		featureTypeChangeOperations.add(featureTypeChangeOperation);
		
		DwGroupTypeChangeOperation childGroupTypeChangeOperation = createChildGroupTypeChangeOperation(snapshotFeature, evolutionFeature, date);
		groupTypeChangeOperations.add(childGroupTypeChangeOperation);
		
		// iterate over features to be merged into the model
		List<DwFeature> snapshotChildFeatures = DwFeatureUtil.getChildFeaturesOfFeature(snapshotFeature, date);
		for(DwFeature snapshotChildFeature : snapshotChildFeatures) {
			String snapshotChildFeatureName = DwFeatureUtil.getName(snapshotChildFeature, date).getName();
			DwFeature evolutionChildFeature = searchForFeature(evolutionFeature, snapshotChildFeatureName, date);
			
			if(evolutionChildFeature == null) {
				// evolutionChildFeature did not yet exist underneath evolutionFeature --> move to or add underneath evolutionFeature
				evolutionChildFeature = searchForFeature(evolutionFeature.getFeatureModel(), snapshotChildFeatureName, date);
				
				if(evolutionChildFeature != null) {
					if(DwEvolutionUtil.isValid(evolutionChildFeature, date)) {
						// evolutionChildFeature exists somewhere else in the prior version --> move it underneath evolutionFeature
						moveOperations.add(operationFactory.createDwFeatureMoveOperation(evolutionChildFeature.getFeatureModel(), evolutionChildFeature, evolutionFeature, date, true));
					}
					else {
						continue;
					}
				}
				else {
					// evolutionChildFeature did not yet exist anywhere in the prior FM version --> add it
					DwFeatureCreateOperation createOperation = operationFactory.createDwFeatureCreateOperation(evolutionFeature.getFeatureModel(), evolutionFeature, false, date, true);
					DwEvolutionOperationInterpreter.execute(createOperation);
					
					evolutionChildFeature = createOperation.getFeature();
					
					DwFeatureRenameOperation renameOperation = operationFactory.createDwFeatureRenameOperation(evolutionChildFeature.getFeatureModel(), evolutionChildFeature, snapshotChildFeatureName, date, true);
					DwEvolutionOperationInterpreter.execute(renameOperation);
				}
			}
			
			addFeaturesAndCollectAdditionalOperations(snapshotChildFeature, evolutionChildFeature, date);
		}
		
		// iterate over features of prior version to see which features need to be deleted
		List<DwFeature> evolutionChildFeatures = DwFeatureUtil.getChildFeaturesOfFeature(evolutionFeature, date);
		for(DwFeature evolutionChildFeature : evolutionChildFeatures) {
			String evolutionChildFeatureName = DwFeatureUtil.getName(evolutionChildFeature, date).getName();
			DwFeature snapshotChildFeature = searchForFeature(snapshotFeature, evolutionChildFeatureName, date);
			
			if(snapshotChildFeature == null) {
				// evolutionChildFeature does not exist underneath evolutionFeature anymore --> delete evolutionChildFeature
				deleteOperations.add(operationFactory.createDwFeatureDeleteOperation(evolutionChildFeature.getFeatureModel(), evolutionChildFeature, date, true));
			}
		}
		
	}
	
	

	private DwFeatureTypeChangeOperation createFeatureTypeChangeOperation(DwFeature snapshotFeature, DwFeature evolutionFeature, Date date) {
		DwFeatureType snapshotType = DwEvolutionUtil.getValidTemporalElement(snapshotFeature.getTypes(), date);
		DwFeatureType evolutionType = DwEvolutionUtil.getValidTemporalElement(evolutionFeature.getTypes(), date);
		
		if (!snapshotType.getType().equals(evolutionType.getType())) {
			return operationFactory.createDwFeatureTypeChangeOperation(evolutionFeature.getFeatureModel(), evolutionFeature, snapshotType.getType(), date, true);
		}
		return null;
	}
	
	private DwGroupTypeChangeOperation createChildGroupTypeChangeOperation(DwFeature snapshotFeature, DwFeature evolutionFeature, Date date) {
		List<DwGroup> snapshotGroups = DwFeatureUtil.getChildGroupsOfFeature(snapshotFeature, date);
		if(snapshotGroups.size() > 1)
			throw new RuntimeException("More than one child group in input model...");
		
		List<DwGroup> evolutionGroups = DwFeatureUtil.getChildGroupsOfFeature(evolutionFeature, date);
		if(evolutionGroups.size() > 1)
			throw new RuntimeException("More than one child group in evolution model...");
		
		if(snapshotGroups.size() == 0 || evolutionGroups.size() == 0)
			return null;

		DwGroupType snapshotType = DwEvolutionUtil.getValidTemporalElement(snapshotGroups.get(0).getTypes(), date);
		DwGroupType evolutionType = DwEvolutionUtil.getValidTemporalElement(evolutionGroups.get(0).getTypes(), date);

		if (!snapshotType.getType().equals(evolutionType.getType())) {
			return operationFactory.createDwGroupTypeChangeOperation(evolutionFeature.getFeatureModel(), evolutionGroups.get(0), snapshotType.getType(), date, true);
		}
		return null;
	}
	
	/**
	 * Returns the <strong>first</strong> found feature with matching name
	 * @param featureModel temporal feature model to be searched in
	 * @param featureName name of the feature to be searched for
	 * @param date date to be searched for
	 * @return the <strong>first</strong> found feature with matching name
	 */
	private DwFeature searchForFeature(DwTemporalFeatureModel featureModel, String featureName, Date date) {
		for(DwFeature feature : featureModel.getFeatures()) {
			if(DwFeatureUtil.getName(feature, date).getName().equals(featureName)) {
				return feature;
			}
		}
		return null;
	}
	
	private DwFeature searchForFeature(DwFeature parentFeature, String subFeatureName, Date date) {
		for(DwFeature potentialSubFeature : DwFeatureUtil.getChildFeaturesOfFeature(parentFeature, date)) {
			if(DwFeatureUtil.getName(potentialSubFeature, date).getName().equals(subFeatureName)) {
				return potentialSubFeature;
			}
		}
		return null;
	}
	
	

	private void executeOperation(DwEvolutionOperation operation) {
		if(operation != null)
			DwEvolutionOperationInterpreter.execute(operation);
	}
	
}










