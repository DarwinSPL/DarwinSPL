package de.darwinspl.importer.evolution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.darwinspl.constraint.DwConstraint;
import de.darwinspl.constraint.DwConstraintFactory;
import de.darwinspl.constraint.DwConstraintModel;
import de.darwinspl.datatypes.DwEnumLiteral;
import de.darwinspl.expression.DwBinaryExpression;
import de.darwinspl.expression.DwExpression;
import de.darwinspl.expression.DwFeatureReferenceExpression;
import de.darwinspl.expression.DwUnaryExpression;
import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureType;
import de.darwinspl.feature.DwFeatureTypeEnum;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupComposition;
import de.darwinspl.feature.DwGroupType;
import de.darwinspl.feature.DwGroupTypeEnum;
import de.darwinspl.feature.DwName;
import de.darwinspl.feature.DwNamedElement;
import de.darwinspl.feature.DwParentFeatureChildGroupContainer;
import de.darwinspl.feature.DwRootFeatureContainer;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.evolution.operation.factory.DwSimpleEvolutionOperationFactory;
import de.darwinspl.feature.evolution.operation.interpreter.DwEvolutionOperationInterpreter;
import de.darwinspl.feature.operation.DwFeatureCreateOperation;
import de.darwinspl.feature.operation.DwFeatureDeleteOperation;
import de.darwinspl.feature.operation.DwFeatureMoveOperation;
import de.darwinspl.feature.operation.DwFeatureRenameOperation;
import de.darwinspl.feature.operation.DwFeatureTypeChangeOperation;
import de.darwinspl.feature.operation.DwGroupCreateOperation;
import de.darwinspl.feature.operation.DwGroupTypeChangeOperation;
import de.darwinspl.feature.util.custom.DwSimpleFeatureFactoryCustom;
import de.darwinspl.feature.util.custom.DwFeatureUtil;
import de.darwinspl.importer.FeatureModelConstraintsTuple;
import de.darwinspl.temporal.util.DwEvolutionUtil;

public class DwFeatureModelEvolutionImporter2 {
	
	private DwSimpleEvolutionOperationFactory operationFactory = new DwSimpleEvolutionOperationFactory();
	


	/**
	 * Merges multiple feature model evolution snapshots to one Temporal Feature
	 * Model
	 * 
	 * @param models
	 *            Each feature model has to be associated to a date at which the
	 *            changes have occured.
	 * @param monitor 
	 * @return The whole feature model history merged into one TFM.
	 * @throws Exception
	 */
	public FeatureModelConstraintsTuple importFeatureModelEvolutionSnapshots(Map<Date, FeatureModelConstraintsTuple> models, IProgressMonitor monitor)
			throws Exception {
		
		DwFeature rootFeature = DwSimpleFeatureFactoryCustom.createNewFeature("RootFeature", DwFeatureTypeEnum.MANDATORY);
		DwTemporalFeatureModel mergedFeatureModel = DwSimpleFeatureFactoryCustom.createNewFeatureModel(rootFeature);
		
		DwConstraintModel mergedConstraintModel = DwConstraintFactory.eINSTANCE.createDwConstraintModel();

		List<Date> sortedDateList = new ArrayList<Date>(models.keySet());
		sortedDateList.remove(null);
		Collections.sort(sortedDateList);

		monitor.beginTask("Merging snapshots to evolution-aware temporal feature model...", models.size());
		monitor.subTask("Importing base model...");
		if (models.get(null) != null) {
			mergeModels(models.get(null), mergedFeatureModel, mergedConstraintModel, null);
		}
		
		monitor.worked(1);
		if(monitor.isCanceled())
			return null;

		for (Date date : sortedDateList) {
			monitor.subTask("Importing model at date " + date);
			mergeModels(models.get(date), mergedFeatureModel, mergedConstraintModel, date);
			
			monitor.worked(1);
			if(monitor.isCanceled())
				return null;
		}

		FeatureModelConstraintsTuple mergedModels = new FeatureModelConstraintsTuple(mergedFeatureModel, mergedConstraintModel);

		return mergedModels;
	}
	
	protected void mergeModels(FeatureModelConstraintsTuple modelsToBeMerged, DwTemporalFeatureModel mergedFeatureModel,
			DwConstraintModel mergedConstraintModel, Date date) throws Exception {

		List<DwFeature> featuresToInvalidate = new ArrayList<DwFeature>(DwEvolutionUtil.getValidTemporalElements(mergedFeatureModel.getFeatures(), date));

		// Step 1: Check that each feature / attribute exists. If not create it and set
		// valid since. If a feature / attribute existed before, but not anymore, set
		// valid until.
		DwTemporalFeatureModel featureModelToBeMerged = modelsToBeMerged.getFeatureModel();

		List<DwFeature> featuresToBeAddedToMergedModel = new ArrayList<DwFeature>();

		// Key is the feature/group of the input model and value is the feature/group of the merged model.
		Map<DwFeature, DwFeature> featureMap = matchFeatures(featureModelToBeMerged, mergedFeatureModel, featuresToInvalidate, featuresToBeAddedToMergedModel, date);
		Map<DwGroup, DwGroup> groupMap = matchGroups(featureModelToBeMerged, mergedFeatureModel, featureMap, date);
		
		
		// TODO clarify why multiple times matchFeatures/matchGroups is necessary. Make it incremental.
		renameRootFeature(featureModelToBeMerged, mergedFeatureModel, date);
		
		updateFeatureTypesToOptional(mergedFeatureModel, featureMap, date);
		updateGroupTypesToAnd(groupMap, date);
		
		// TODO why is here no update necessary?
//		featureMap = matchFeatures(featureModelToBeMerged, mergedFeatureModel, featuresToInvalidate, featuresToBeAddedToMergedModel, date);
//		groupMap = matchGroups(featureModelToBeMerged, mergedFeatureModel, featureMap, date);
		
		addNewFeatures(featuresToBeAddedToMergedModel, mergedFeatureModel, featureMap, date);
		
		featureMap = matchFeatures(featureModelToBeMerged, mergedFeatureModel, featuresToInvalidate, featuresToBeAddedToMergedModel, date);
		groupMap = matchGroups(featureModelToBeMerged, mergedFeatureModel, featureMap, date);
		
		// do this for newly added features and groups. TODO Optimize?
		updateFeatureTypesToOptional(mergedFeatureModel, featureMap, date);
		updateGroupTypesToAnd(groupMap, date);
		
		// TODO why is this necessary here again? Found bugs, this fixed it. Better solution should exist. Idea: to analyze this: compare mapping before and after
		featureMap = matchFeatures(featureModelToBeMerged, mergedFeatureModel, featuresToInvalidate, featuresToBeAddedToMergedModel, date);
		groupMap = matchGroups(featureModelToBeMerged, mergedFeatureModel, featureMap, date);

		moveFeaturesIfTheyHaveBeenMovedInInputModel(mergedFeatureModel, featureMap, groupMap, date);

		// TODO why is this necessary here again? Found bugs, this fixed it. Better solution should exist. Idea: to analyze this: compare mapping before and after
		featureMap = matchFeatures(featureModelToBeMerged, mergedFeatureModel, featuresToInvalidate, featuresToBeAddedToMergedModel, date);
		groupMap = matchGroups(featureModelToBeMerged, mergedFeatureModel, featureMap, date);
		
		updateFeatureTypesToMandatory(mergedFeatureModel, featureMap, date);
		updateGroupTypesToOrAlternative(groupMap, date);
		
		
		List<DwFeature> featuresToDelete = new ArrayList<>(featuresToInvalidate);
		// Invalidate features which do not exist anymore. Update group composition.
		for (DwFeature featureToInvalidate : featuresToInvalidate) {
			List<DwFeature> subFeaturesofFeatureToInvalidate = DwFeatureUtil.getFeaturesOfSubtree(featureToInvalidate, date, -1);
			featuresToDelete.removeAll(subFeaturesofFeatureToInvalidate);
		}
		
		for (DwFeature featureToDelete : featuresToDelete) {
			// The feature was not already deleted by a sub-tree delete
			DwFeatureDeleteOperation deleteOperation = operationFactory.createDwFeatureDeleteOperation(featureToDelete.getFeatureModel(), featureToDelete, date, true);
			DwEvolutionOperationInterpreter.execute(deleteOperation);
		}

		// Merge constraint models
		DwConstraintModel constraintModelToBeMerged = modelsToBeMerged.getConstraintModel();
		if (constraintModelToBeMerged != null) {
			mergedConstraintModel.setFeatureModel(mergedFeatureModel);
			mergeConstraintModel(constraintModelToBeMerged, mergedConstraintModel, featureMap, date);
		}
		
	}
	
	
	
	/**
	 * 
	 * @param featureModelToBeMerged
	 * @param mergedFeatureModel
	 * @param featuresToInvalidate List of all features from merged model which not have been matched. Will indicate which features won't have a matching partner and thus be invalidated.
	 * @param addedFeatureAttributes List of feature attributes which have been matched during this method
	 * @param featuresToBeAddedToMergedModel List of all features from input model which not have been matched and, thus, have to be added to the merged model
	 * @param date
	 * @return key: feature from model to be merged, value: feature from merged model
	 * @throws Exception
	 */
	protected Map<DwFeature, DwFeature> matchFeatures(DwTemporalFeatureModel featureModelToBeMerged, DwTemporalFeatureModel mergedFeatureModel,
			List<DwFeature> featuresToInvalidate, List<DwFeature> featuresToBeAddedToMergedModel, Date date) throws Exception {
		Map<DwFeature, DwFeature> featureMap = new HashMap<DwFeature, DwFeature>();

		List<DwFeature> potentialFeatureMatchingPartners = new ArrayList<DwFeature>();
		potentialFeatureMatchingPartners.addAll(DwEvolutionUtil.getValidTemporalElements(mergedFeatureModel.getFeatures(), date));
		
		for (DwFeature featureToBeMerged : featureModelToBeMerged.getFeatures()) {
			DwFeature equivalentFeature;
			
			if(DwFeatureUtil.isRootFeature(featureModelToBeMerged, featureToBeMerged, date)) {
				equivalentFeature = DwFeatureUtil.getRootFeature(mergedFeatureModel, date);
			} else {
				equivalentFeature = checkIfElementAlreadyExists(featureToBeMerged, potentialFeatureMatchingPartners, date);
			}

			if (equivalentFeature == null) {
				featuresToBeAddedToMergedModel.add(featureToBeMerged);
			} else {
				potentialFeatureMatchingPartners.remove(equivalentFeature);
				featuresToInvalidate.remove(equivalentFeature);
				featureMap.put(featureToBeMerged, equivalentFeature);
			}
		}
		
		return featureMap;
	}

	/**
	 * 
	 * @param elementToBeChecked
	 * @param elementsOfMergedModel
	 * @return null if no equivalent element exists. Otherwise the equivalentElement
	 */
	protected <S extends EObject> S checkIfElementAlreadyExists(S elementToBeChecked, List<S> elementsOfMergedModel, Date date) {
		for (S featureOfMergedModel : elementsOfMergedModel) {
			if (checkForEquality(elementToBeChecked, featureOfMergedModel, date)) {
				return featureOfMergedModel;
			}
		}

		return null;
	}

	protected boolean checkForEquality(EObject o1, EObject o2, Date date) {
		if (o1 instanceof DwNamedElement && o2 instanceof DwNamedElement) {
			DwNamedElement namedElement1 = (DwNamedElement) o1;
			DwNamedElement namedElement2 = (DwNamedElement) o2;

			DwName name1 = DwEvolutionUtil.getValidTemporalElement(namedElement1.getNames(), date);
			DwName name2 = DwEvolutionUtil.getValidTemporalElement(namedElement2.getNames(), date);

			if (name1.getName().equals(name2.getName())) {
				return true;
			} else {
				return false;
			}
		} else if (o1 instanceof DwEnumLiteral && o2 instanceof DwEnumLiteral) {
			DwEnumLiteral enumLiteral1 = (DwEnumLiteral) o1;
			DwEnumLiteral enumLiteral2 = (DwEnumLiteral) o2;

			if (enumLiteral1.getName().equals(enumLiteral2.getName())) {
				return true;
			} else {
				return false;
			}
		}

		return false;
	}
	
	
	
	/**
	 * 
	 * @param featureModelToBeMerged
	 * @param mergedFeatureModel
	 * @param featureMap key: feature from model to be merged, value: feature from merged model
	 * @param date
	 * @return key: group from model to be merged, value: group from merged model
	 */
	protected Map<DwGroup, DwGroup> matchGroups(DwTemporalFeatureModel featureModelToBeMerged, DwTemporalFeatureModel mergedFeatureModel, Map<DwFeature, DwFeature> featureMap, Date date) {
		Map<DwGroup, DwGroup> groupMap = new HashMap<DwGroup, DwGroup>();
		
		List<DwGroup> potentialGroupMatchingPartners = new ArrayList<DwGroup>();
		potentialGroupMatchingPartners.addAll(DwEvolutionUtil.getValidTemporalElements(mergedFeatureModel.getGroups(), date));
		
		DwGroup matchedGroupMatchingPartners = null;
		
		// ---- group matching based on parent. Assumes only one group per parent!
		// first link all And groups with the same parent.
		for (DwGroup groupToBeMerged : featureModelToBeMerged.getGroups()) {
			if(matchedGroupMatchingPartners != null) {
				potentialGroupMatchingPartners.remove(matchedGroupMatchingPartners);				
				matchedGroupMatchingPartners = null;
			}
			
			DwFeature parentFeatureOfGroupToBeMerged = DwEvolutionUtil.getValidTemporalElement(groupToBeMerged.getChildOf(), date).getParent();
			DwFeature equivalentToParentOfGroupToBeMerged = featureMap.get(parentFeatureOfGroupToBeMerged);
			
			if(equivalentToParentOfGroupToBeMerged != null) {
				List<DwParentFeatureChildGroupContainer> validFeatureChildren = DwEvolutionUtil.getValidTemporalElements(equivalentToParentOfGroupToBeMerged.getParentOf(), date);
				
				if(validFeatureChildren != null && validFeatureChildren.size() > 0) {
					if(validFeatureChildren.size() > 1) {
						System.err.println("Cannot perform group match as multiple child groups under feature "+DwEvolutionUtil.getValidTemporalElement(equivalentToParentOfGroupToBeMerged.getNames(), date).getName()+" at date "+date);
						continue;
					}
					
					DwGroup potentialMatchingPartner = validFeatureChildren.get(0).getChildGroup();
					if(potentialGroupMatchingPartners.contains(potentialMatchingPartner)) {
						groupMap.put(groupToBeMerged, potentialMatchingPartner);
						matchedGroupMatchingPartners = potentialMatchingPartner;
					}
					else {
						System.err.println("Cannot perform group match as input model seems to have multiple groups under feature "+ DwEvolutionUtil.getValidTemporalElement(parentFeatureOfGroupToBeMerged.getNames(), date).getName()+" at date "+date);
						continue;
					}
				}
			}
		}
		
		return groupMap;
	}
	
	

	protected void moveFeaturesIfTheyHaveBeenMovedInInputModel(DwTemporalFeatureModel mergedFeatureModel, Map<DwFeature, DwFeature> featureMap, Map<DwGroup, DwGroup> groupMap, Date date) {
		for (Entry<DwFeature, DwFeature> featureEntrySet : featureMap.entrySet()) {
			DwFeature mergedFeature = featureEntrySet.getValue();
			DwFeature parentOfMergedFeature = DwFeatureUtil.getParentFeatureOfFeature(mergedFeature, date);
			
			DwFeature inputFeature = featureEntrySet.getKey();
			DwFeature parentOfInputFeature = DwFeatureUtil.getParentFeatureOfFeature(inputFeature, date);

			if (parentOfInputFeature == null) {
				// seems to be new root.
				continue;
			}

			DwFeature equivalentToParentOfInputFeature = featureMap.get(parentOfInputFeature);

			if (equivalentToParentOfInputFeature == null) {
//				System.out.println("WTF");
//				DwFeatureDetachOperation detachOperation = operationFactory.createDwFeatureDetachOperation(mergedFeature, date, true);
//				DwEvolutionOperationInterpreter.executeWithoutAnalysis(detachOperation);
				continue;
			} else if (equivalentToParentOfInputFeature == parentOfMergedFeature) {
				// TODO may be wrong if the group changed for the same parent
				continue;
			} else {
				// parents are different.
				DwFeatureMoveOperation moveOperation = operationFactory.createDwFeatureMoveOperation(mergedFeature.getFeatureModel(), mergedFeature, equivalentToParentOfInputFeature, date, true);
				DwEvolutionOperationInterpreter.execute(moveOperation);
			}
		}
	}
	
	

	protected void updateFeatureTypesToOptional(DwTemporalFeatureModel mergedFeatureModel, Map<DwFeature, DwFeature> featureMap, Date date) {
		for (Entry<DwFeature, DwFeature> entry : featureMap.entrySet()) {
			DwFeature mergedFeature = entry.getValue();
			DwFeature inputFeature = entry.getKey();
			
			// key feature from input model, value feature from merged model
			DwFeatureType mergedType = DwEvolutionUtil.getValidTemporalElement(mergedFeature.getTypes(), date);
			DwFeatureType inputType = DwEvolutionUtil.getValidTemporalElement(inputFeature.getTypes(), date);
			
			if(inputType.getType().equals(DwFeatureTypeEnum.OPTIONAL)) {
				if (!inputType.getType().equals(mergedType.getType())) {
					DwEvolutionOperationInterpreter.execute(operationFactory.createDwFeatureTypeChangeOperation(mergedFeatureModel, mergedFeature, inputType.getType(), date, true));
				}
			}
		}
	}
	
	protected void updateFeatureTypesToMandatory(DwTemporalFeatureModel mergedFeatureModel, Map<DwFeature, DwFeature> featureMap, Date date) {
		for (Entry<DwFeature, DwFeature> entry : featureMap.entrySet()) {
			DwFeature mergedFeature = entry.getValue();
			DwFeature inputFeature = entry.getKey();
			
			// key feature from input model, value feature from merged model
			DwFeatureType mergedType = DwEvolutionUtil.getValidTemporalElement(mergedFeature.getTypes(), date);
			DwFeatureType inputType = DwEvolutionUtil.getValidTemporalElement(inputFeature.getTypes(), date);

			if(inputType.getType().equals(DwFeatureTypeEnum.MANDATORY)) {
				if (!inputType.getType().equals(mergedType.getType())) {
					DwEvolutionOperationInterpreter.execute(operationFactory.createDwFeatureTypeChangeOperation(mergedFeatureModel, mergedFeature, inputType.getType(), date, true));
				}
			}
		}
	}
	


	protected void updateGroupTypesToAnd(Map<DwGroup, DwGroup> groupMap, Date date) {
		for (Entry<DwGroup, DwGroup> entry : groupMap.entrySet()) {
			DwGroup mergedGroup = entry.getValue();
			DwGroup inputGroup = entry.getKey();
			
			DwGroupType groupTypeOfMergedGroup = DwEvolutionUtil.getValidTemporalElement(mergedGroup.getTypes(), date);
			DwGroupType groupTypeOfGroupToBeMerged = DwEvolutionUtil.getValidTemporalElement(inputGroup.getTypes(), date);

			if(groupTypeOfGroupToBeMerged.getType().equals(DwGroupTypeEnum.AND)) {
				if (!groupTypeOfGroupToBeMerged.getType().equals(groupTypeOfMergedGroup.getType())) {
					DwEvolutionOperationInterpreter.execute(operationFactory.createDwGroupTypeChangeOperation(mergedGroup.getFeatureModel(), mergedGroup, groupTypeOfGroupToBeMerged.getType(), date, true));
				}
			}
		}
	}

	protected void updateGroupTypesToOrAlternative(Map<DwGroup, DwGroup> groupMap, Date date) {
		for (Entry<DwGroup, DwGroup> entry : groupMap.entrySet()) {
			DwGroup mergedGroup = entry.getValue();
			DwGroup inputGroup = entry.getKey();
			
			DwGroupType groupTypeOfMergedGroup = DwEvolutionUtil.getValidTemporalElement(mergedGroup.getTypes(), date);
			DwGroupType groupTypeOfGroupToBeMerged = DwEvolutionUtil.getValidTemporalElement(inputGroup.getTypes(), date);

			if(!groupTypeOfGroupToBeMerged.getType().equals(DwGroupTypeEnum.AND)) {
				if (!groupTypeOfGroupToBeMerged.getType().equals(groupTypeOfMergedGroup.getType())) {
					DwEvolutionOperationInterpreter.execute(operationFactory.createDwGroupTypeChangeOperation(mergedGroup.getFeatureModel(), mergedGroup, groupTypeOfGroupToBeMerged.getType(), date, true));
				}
			}
		}
	}
	
	
	
	private void renameRootFeature(DwTemporalFeatureModel inputFeatureModel, DwTemporalFeatureModel mergedFeatureModel, Date date) {
		DwRootFeatureContainer inputFeatureModelRootFeatureContainer = DwEvolutionUtil.getValidTemporalElement(inputFeatureModel.getRootFeatures(), date);
		DwFeature inputFeatureModelRootFeature = inputFeatureModelRootFeatureContainer.getFeature();
		String inputFeatureModelRootFeatureName = DwFeatureUtil.getName(inputFeatureModelRootFeature, date).getName();
		
		DwRootFeatureContainer mergedFeatureModelRootFeatureContainer = DwEvolutionUtil.getValidTemporalElement(mergedFeatureModel.getRootFeatures(), date);
		DwFeature mergedFeatureModelRootFeature = mergedFeatureModelRootFeatureContainer.getFeature();
		String mergedFeatureModelRootFeatureName = DwFeatureUtil.getName(mergedFeatureModelRootFeature, date).getName();

		if(!inputFeatureModelRootFeatureName.equals(mergedFeatureModelRootFeatureName)) {
			DwFeatureRenameOperation renameOp = operationFactory.createDwFeatureRenameOperation(mergedFeatureModelRootFeature.getFeatureModel(), mergedFeatureModelRootFeature, inputFeatureModelRootFeatureName, date, true);
			DwEvolutionOperationInterpreter.execute(renameOp);
		}
	}
	
	
	
	protected void addNewFeatures(List<DwFeature> snapshotFeatures, DwTemporalFeatureModel mergedFeatureModel, Map<DwFeature, DwFeature> featureMap, Date date) throws Exception {
		List<DwFeature> featuresAddedToMergedModel = new ArrayList<>();
		
		while(!snapshotFeatures.isEmpty()) {
			int featuresAddedBeforeThisLoopRun = featuresAddedToMergedModel.size();
			
			for (DwFeature snapshotFeature : snapshotFeatures) {
				DwFeature parentInMergedModel;
				
				// Handle relation to parent group!
				// Either this is the root feature or we find the appropriate parent group to add the feature to
				
				DwGroupComposition snapshotGroupComposition = DwEvolutionUtil.getValidTemporalElement(snapshotFeature.getGroupMembership(), date);
				if (snapshotGroupComposition != null) {
					DwGroup snapshotParentGroup = snapshotGroupComposition.getCompositionOf();
					DwParentFeatureChildGroupContainer parentOfSnapshotParentGroup = DwEvolutionUtil.getValidTemporalElement(snapshotParentGroup.getChildOf(), date);
					DwFeature parentFeatureInSnapshotModel = parentOfSnapshotParentGroup.getParent();
	
					if (!featureMap.containsKey(parentFeatureInSnapshotModel)) {
						// parent feature is not yet contained in the merged model
						// --> continue with this loop run and revisit the feature later.
						continue;
					}
	
					parentInMergedModel = featureMap.get(parentFeatureInSnapshotModel);
					if (parentInMergedModel == null) {
						throw new Exception("Parent of feature "
								+ DwEvolutionUtil.getValidTemporalElement(snapshotFeature.getNames(), date).getName()
								+ " is neither newly added to the merged model nor is it already existent in it. Aborting whole merging process.");
					}
				} 
				else {
					// Seems to be a root feature, how is that possible !?

					throw new Exception("A new root feature "
							+ DwEvolutionUtil.getValidTemporalElement(snapshotFeature.getNames(), date).getName()
							+ " is tried to be added to the model, that does not make sense! Aborting whole merging process.");
				}
				
				// ???
				
				DwFeatureCreateOperation createOp = operationFactory.createDwFeatureCreateOperation(mergedFeatureModel, parentInMergedModel, false, date, true);
				DwEvolutionOperationInterpreter.execute(createOp);
				
				DwFeature newFeatureInMergedModel = createOp.getFeature();
				
				String newFeatureName = DwEvolutionUtil.getValidTemporalElement(snapshotFeature.getNames(), date).getName();
				DwFeatureRenameOperation renameOp = operationFactory.createDwFeatureRenameOperation(newFeatureInMergedModel.getFeatureModel(), newFeatureInMergedModel, newFeatureName, date, true);
				DwEvolutionOperationInterpreter.execute(renameOp);
				
				DwFeatureTypeEnum newFeatureNameType = DwEvolutionUtil.getValidTemporalElement(snapshotFeature.getTypes(), date).getType();
				if(newFeatureNameType != DwSimpleFeatureFactoryCustom.DEFAULT_FEATURE_TYPE) {
					DwFeatureTypeChangeOperation typeChangeOp = operationFactory.createDwFeatureTypeChangeOperation(mergedFeatureModel, newFeatureInMergedModel, newFeatureNameType, date, true);
					DwEvolutionOperationInterpreter.execute(typeChangeOp);
				}
				
				List<DwParentFeatureChildGroupContainer> snapshotFeatureChildren = DwEvolutionUtil.getValidTemporalElements(snapshotFeature.getParentOf(), date);
				if(snapshotFeatureChildren.size() > 1)
					System.out.println("Weird: more than one valid feature child!?");
				
				for (DwParentFeatureChildGroupContainer snapshotFeatureChild : snapshotFeatureChildren) {
					DwGroup snapshotChildGroup = snapshotFeatureChild.getChildGroup();
					DwGroupTypeEnum snapshotChildGroupType = DwEvolutionUtil.getValidTemporalElement(snapshotChildGroup.getTypes(), date).getType();
	
					DwGroupCreateOperation childGroupCreateOp = operationFactory.createDwGroupCreateOperation(mergedFeatureModel, newFeatureInMergedModel, date, true);
					DwEvolutionOperationInterpreter.execute(childGroupCreateOp);
					
					DwGroup newChildGroup = childGroupCreateOp.getGroup();
					
					if(snapshotChildGroupType != DwSimpleFeatureFactoryCustom.DEFAULT_GROUP_TYPE) {
						DwGroupTypeChangeOperation childGroupTypeChangeOp = operationFactory.createDwGroupTypeChangeOperation(mergedFeatureModel, newChildGroup, snapshotChildGroupType, date, true);
						DwEvolutionOperationInterpreter.execute(childGroupTypeChangeOp);
					}
				}
				
				// update reference
				featureMap.put(snapshotFeature, newFeatureInMergedModel);
				
				featuresAddedToMergedModel.add(snapshotFeature);
			}
			
			if(snapshotFeatures.size() == featuresAddedBeforeThisLoopRun) {
				throw new Exception("Adding features does never end: " + snapshotFeatures.size() + 
						"features remain without in merged model parent and can thus not be added.");
			}
			
			snapshotFeatures.removeAll(featuresAddedToMergedModel);
		}
	}
	
	
	
	protected DwGroup getBestMatchingGroup(DwFeature parent, Date date) {
		// ---- Assuming that each feature has only one child group
		List<DwParentFeatureChildGroupContainer> featureChildrenOfParentInMergedModel = DwEvolutionUtil.getValidTemporalElements(parent.getParentOf(), date);
		if(featureChildrenOfParentInMergedModel == null || featureChildrenOfParentInMergedModel.size() == 0) {
			return null;
		}
		
		if(featureChildrenOfParentInMergedModel.size() > 1) {
			System.err.println("");
			return null;
		}
		
		return featureChildrenOfParentInMergedModel.get(0).getChildGroup();
	}
	
	
	
	protected void mergeConstraintModel(DwConstraintModel constraintModelToBeMerged,
			DwConstraintModel mergedConstraintModel, Map<DwFeature, DwFeature> featureMap, Date date) {
		// For each constraint in model to be merged:
		// Create a map, in which constraints from the model to be merged are mapped to
		// equal constraints from the merged model

		// Key is the constraint from model to be merged and value is constraint from
		// merged model
		List<DwConstraint> constraintsToBeMergedWithoutMatchingPartner = new ArrayList<DwConstraint>(
				constraintModelToBeMerged.getConstraints());
		List<DwConstraint> remainingMatchingPartners = new ArrayList<DwConstraint>(
				DwEvolutionUtil.getValidTemporalElements(mergedConstraintModel.getConstraints(), date));
		
		// Finding matching constraints
		for (DwConstraint constraintToBeMerged : constraintModelToBeMerged.getConstraints()) {
			DwConstraint equalConstraint = findEqualConstraint(constraintToBeMerged, remainingMatchingPartners,
					featureMap, date);
			
			if (equalConstraint != null) {
				remainingMatchingPartners.remove(equalConstraint);
				constraintsToBeMergedWithoutMatchingPartner.remove(constraintToBeMerged);
//				System.err.println(zdt.toString()+": Matched a constraint.");
				continue;
			}
		}

		// For each constraint of the model to be merged that does not have a mapping
		// partner: add the constraint with valid since
		
		// Adding new constraints
		for (DwConstraint constraintToBeMergedWithoutMatchingPartner : constraintsToBeMergedWithoutMatchingPartner) {
			mergedConstraintModel.getConstraints()
					.add(createConstraintInMergedModel(constraintToBeMergedWithoutMatchingPartner, featureMap, date));
		}

		// For each constraint of the merged model that does not have a mapping partner:
		// set the valid until of the constraint
		
		// Invalidate constraints
		for (DwConstraint unmatchedConstraint : remainingMatchingPartners) {
			unmatchedConstraint.getValidity().setTo(date);
		}
	}
	
	

	protected DwConstraint findEqualConstraint(DwConstraint constraint, List<DwConstraint> potentialMatchingPartners,
			Map<DwFeature, DwFeature> featureMap, Date date) {
		for (DwConstraint potentialMatchingPartner : potentialMatchingPartners) {
			if (areExpressionsEqual(constraint.getRootExpression(), potentialMatchingPartner.getRootExpression(),
					featureMap, date)) {
				return potentialMatchingPartner;
			}
		}

		return null;
	}

	protected boolean areExpressionsEqual(DwExpression expressionOfConstraintToBeMerged,
			DwExpression expressionOfMergedConstraint, Map<DwFeature, DwFeature> featureMap, Date date) {
		if (expressionOfConstraintToBeMerged.getClass().toString()
				.equals(expressionOfMergedConstraint.getClass().toString())) {
			if (expressionOfConstraintToBeMerged instanceof DwBinaryExpression) {
				DwBinaryExpression bin1 = (DwBinaryExpression) expressionOfConstraintToBeMerged;
				DwBinaryExpression bin2 = (DwBinaryExpression) expressionOfMergedConstraint;

				boolean operandsMatch = false;

				operandsMatch = areExpressionsEqual(bin1.getOperand1(), bin2.getOperand1(), featureMap, date)
						&& areExpressionsEqual(bin1.getOperand2(), bin2.getOperand2(), featureMap, date);

				return operandsMatch;
			} else if (expressionOfConstraintToBeMerged instanceof DwUnaryExpression) {
				DwUnaryExpression unary1 = (DwUnaryExpression) expressionOfConstraintToBeMerged;
				DwUnaryExpression unary2 = (DwUnaryExpression) expressionOfMergedConstraint;

				return areExpressionsEqual(unary1.getOperand(), unary2.getOperand(), featureMap, date);
			} else if (expressionOfConstraintToBeMerged instanceof DwFeatureReferenceExpression) {
				DwFeatureReferenceExpression featureRef1 = (DwFeatureReferenceExpression) expressionOfConstraintToBeMerged;
				DwFeatureReferenceExpression featureRef2 = (DwFeatureReferenceExpression) expressionOfMergedConstraint;	
				
				DwFeature equivalentFeatureFromMergedFeatureModel = featureMap.get(featureRef1.getFeature());
				boolean featuresMatch = equivalentFeatureFromMergedFeatureModel==featureRef2.getFeature();
				return featuresMatch;
			} else {
				System.err.println("Currently unsupported expressions have been compared in de.darwinspl.importer.evolution.DwFeatureModelEvolutionImporter.areExpressionsEqual(DwExpression, DwExpression, Map<DwFeature, DwFeature>, Date) " + expressionOfConstraintToBeMerged.getClass());
				return false;
			}
		} else {
			return false;
		}
	}

	protected DwConstraint createConstraintInMergedModel(DwConstraint constraintToBeMerged,
			Map<DwFeature, DwFeature> featureMap, Date date) {
		DwConstraint constraint = EcoreUtil.copy(constraintToBeMerged);
		DwEvolutionUtil.setTemporalValidity(constraint, date, null);

		TreeIterator<EObject> iterator = constraint.eAllContents();
		while (iterator.hasNext()) {
			EObject eObject = iterator.next();
			if (eObject instanceof DwFeatureReferenceExpression) {
				DwFeatureReferenceExpression featureReference = (DwFeatureReferenceExpression) eObject;
				// TODO ??? no versions are supported
//				featureReference.setVersionRestriction(null);

				DwFeature equivalentFeatureInMergedModel = featureMap.get(featureReference.getFeature());
				if(equivalentFeatureInMergedModel != null) {
					featureReference.setFeature(equivalentFeatureInMergedModel);					
				}
			}
		}

		return constraint;
	}
	
}
