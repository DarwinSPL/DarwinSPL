package de.darwinspl.importer.evolution.check;

public class ModelCheckException extends Exception {

	private static final long serialVersionUID = 3400015289626522336L;

	public ModelCheckException(String string) {
		super(string);
	}

}
