package de.darwinspl.importer.evolution.check;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureTypeEnum;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupComposition;
import de.darwinspl.feature.DwGroupTypeEnum;
import de.darwinspl.feature.DwNamedElement;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.util.custom.DwFeatureUtil;
import de.darwinspl.importer.FeatureModelConstraintsTuple;
import de.darwinspl.temporal.DwTemporalElement;
import de.darwinspl.temporal.util.DwEvolutionUtil;

public class DwModelSanityCheck {
	
	private List<String> problems;

	public void performCheck(DwTemporalFeatureModel mergedTFM, Map<Date, FeatureModelConstraintsTuple> snapshotModels, IProgressMonitor monitor) throws ModelCheckException {
		List<Date> sortedDateList = new ArrayList<Date>(snapshotModels.keySet());
		sortedDateList.remove(null);
		Collections.sort(sortedDateList);
		
		monitor.beginTask("Checking correctness of imported TFM...", snapshotModels.size());
		problems = new ArrayList<>();
		
		for (Date date : sortedDateList) {
			monitor.subTask("Checking correctness of imported model for date "+date);
			
			compareTFMs(mergedTFM, snapshotModels.get(date).getFeatureModel(), date);
			
			monitor.worked(1);
			if(monitor.isCanceled())
				return;
		}
		
		if(problems.size() > 0)
			throw new ModelCheckException(this.toString());
	}
	
	@Override
	public String toString() {
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append("Number of problems: ");
		strBuilder.append(problems.size());
		strBuilder.append(System.lineSeparator());
		
		for(String problem : problems) {
			strBuilder.append(problem);
			strBuilder.append(System.lineSeparator());
		}
		return strBuilder.toString();
	}

	private void compareTFMs(DwTemporalFeatureModel mergedTFM, DwTemporalFeatureModel snapshotTFM, Date date) {
		List<DwFeature> allMergedFeatures = DwFeatureUtil.getFeatures(mergedTFM, date);
		List<DwFeature> allSnapshotFeatures = DwFeatureUtil.getFeatures(mergedTFM, date);
		if(allMergedFeatures.size() != allSnapshotFeatures.size())
			problems.add("compareTFMs - Different amount of TFM features for date " + date);

		List<DwGroup> allMergedGroups = DwFeatureUtil.getGroups(mergedTFM, date);
		List<DwGroup> allSnapshotGroups = DwFeatureUtil.getGroups(mergedTFM, date);
		if(allMergedGroups.size() != allSnapshotGroups.size())
			problems.add("compareTFMs - Different amount of TFM groups for date " + date);

		DwFeature mergedRootFeature = DwFeatureUtil.getRootFeature(mergedTFM, date);
		DwFeature snapshotRootFeature = DwFeatureUtil.getRootFeature(snapshotTFM, date);
		
		compareFeatures(mergedRootFeature, snapshotRootFeature, date);
	}

	private void compareFeatures(DwFeature mergedFeature, DwFeature snapshotFeature, Date date) {
		String mergedName = DwFeatureUtil.getName(mergedFeature, date).getName();
		String snapshotName = DwFeatureUtil.getName(snapshotFeature, date).getName();
		if(!mergedName.equals(snapshotName))
			problems.add("compareFeatures - Different names of TFM feature " + printElementFromTFM(mergedFeature, date) + " for date " + date);
		
		DwFeatureTypeEnum mergedType = DwFeatureUtil.getType(mergedFeature, date).getType();
		DwFeatureTypeEnum snapshotType = DwFeatureUtil.getType(snapshotFeature, date).getType();
		if(!mergedType.equals(snapshotType))
			problems.add("compareFeatures - Different types of TFM feature " + printElementFromTFM(mergedFeature, date) + " for date " + date);
		
		List<DwGroup> mergedChildGroups = DwFeatureUtil.getChildGroupsOfFeature(mergedFeature, date);
		List<DwGroup> snapshotChildGroups = DwFeatureUtil.getChildGroupsOfFeature(snapshotFeature, date);
		if(mergedChildGroups.size() > 1) {
			problems.add("compareFeatures - More than one child group of TFM feature " + printElementFromTFM(mergedFeature, date) + " for date " + date);
			return;
		}
		
		if(mergedChildGroups.size() != snapshotChildGroups.size()) {
			problems.add("compareFeatures - Unmatching number of child groups of TFM feature " + printElementFromTFM(mergedFeature, date) + " for date " + date);
			return;
		}
		
		for(int i=0; i<mergedChildGroups.size(); i++) {
			compareGroups(mergedChildGroups.get(i), snapshotChildGroups.get(i), date);
		}
	}

	private void compareGroups(DwGroup mergedGroup, DwGroup snapshotGroup, Date date) {
		DwGroupTypeEnum mergedType = DwFeatureUtil.getType(mergedGroup, date).getType();
		DwGroupTypeEnum snapshotType = DwFeatureUtil.getType(snapshotGroup, date).getType();
		if(!mergedType.equals(snapshotType))
			problems.add("compareGroups - Different types of TFM group " + printElementFromTFM(mergedGroup, date) + " for date " + date);

		DwGroupComposition mergedGroupComposition = DwEvolutionUtil.getValidTemporalElement(mergedGroup.getParentOf(), date);
		DwGroupComposition snapshotGroupComposition = DwEvolutionUtil.getValidTemporalElement(snapshotGroup.getParentOf(), date);

		if(mergedGroupComposition == null || mergedGroupComposition.getChildFeatures().size() == 0) {
			problems.add("compareGroups - Empty TFM group " + printElementFromTFM(mergedGroup, date) + " for date " + date);
			return;
		}
		if(snapshotGroupComposition == null || snapshotGroupComposition.getChildFeatures().size() == 0) {
			problems.add("compareGroups - Empty group in snapshot-counterpart for TFM group " + printElementFromTFM(mergedGroup, date) + " for date " + date);
			return;
		}
		
		if(mergedGroupComposition.getChildFeatures().size() != snapshotGroupComposition.getChildFeatures().size()) {
			problems.add("compareGroups - Different amount of child feature in TFM group " + printElementFromTFM(mergedGroup, date) + " for date " + date);
			return;
		}
		
		for (DwFeature mergedChildFeature : mergedGroupComposition.getChildFeatures()) {
			String mergedChildFeatureName = DwFeatureUtil.getName(mergedChildFeature, date).getName();
			
			DwFeature snapshotChildFeature = findFeatureWithName(snapshotGroupComposition, mergedChildFeatureName, date);
			if(snapshotChildFeature == null) {
				problems.add("compareGroups - Could not find a snapshot-counterpart for TFM feature " + printElementFromTFM(mergedChildFeature, date) + " in its parent group for date " + date);
				continue;
			}
			
			compareFeatures(mergedChildFeature, snapshotChildFeature, date);
		}
	}

	private DwFeature findFeatureWithName(DwGroupComposition groupCompositionToSearchIn, String featureName, Date date) {
		for(DwFeature childFeature : groupCompositionToSearchIn.getChildFeatures()) {
			String childFeatureName = DwFeatureUtil.getName(childFeature, date).getName();
			if(childFeatureName.equals(featureName))
				return childFeature;
		}
		return null;
	}
	
	private String printElementFromTFM(DwTemporalElement element, Date date) {
		if(element instanceof DwNamedElement) {
			return DwEvolutionUtil.getValidTemporalElement(((DwNamedElement) element).getNames(), date).getName();
		}
		if(element instanceof DwGroup) {
			DwFeature parentFeature = DwFeatureUtil.getParentFeatureOfGroup((DwGroup) element, date);
			return "[child group of feature " + printElementFromTFM(parentFeature, date) + "]";
		}
		
		return element.getId();
	}

}




