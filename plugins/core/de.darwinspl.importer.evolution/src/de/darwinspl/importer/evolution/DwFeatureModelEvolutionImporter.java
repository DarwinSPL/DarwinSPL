package de.darwinspl.importer.evolution;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.darwinspl.constraint.DwConstraint;
import de.darwinspl.constraint.DwConstraintFactory;
import de.darwinspl.constraint.DwConstraintModel;
import de.darwinspl.datatypes.DwEnum;
import de.darwinspl.datatypes.DwEnumLiteral;
import de.darwinspl.expression.DwBinaryExpression;
import de.darwinspl.expression.DwExpression;
import de.darwinspl.expression.DwFeatureReferenceExpression;
import de.darwinspl.expression.DwUnaryExpression;
import de.darwinspl.feature.DwBooleanAttribute;
import de.darwinspl.feature.DwEnumAttribute;
import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureAttribute;
import de.darwinspl.feature.DwFeatureFactory;
import de.darwinspl.feature.DwFeatureType;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupComposition;
import de.darwinspl.feature.DwGroupType;
import de.darwinspl.feature.DwGroupTypeEnum;
import de.darwinspl.feature.DwName;
import de.darwinspl.feature.DwNamedElement;
import de.darwinspl.feature.DwNumberAttribute;
import de.darwinspl.feature.DwParentFeatureChildGroupContainer;
import de.darwinspl.feature.DwRootFeatureContainer;
import de.darwinspl.feature.DwStringAttribute;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.evolution.operation.factory.DwSimpleEvolutionOperationFactory;
import de.darwinspl.feature.evolution.operation.interpreter.DwEvolutionOperationInterpreter;
import de.darwinspl.feature.operation.DwFeatureDeleteOperation;
import de.darwinspl.feature.operation.DwFeatureDetachOperation;
import de.darwinspl.feature.operation.DwGroupCreateOperation;
import de.darwinspl.feature.util.custom.DwSimpleFeatureFactoryCustom;
import de.darwinspl.feature.util.custom.DwFeatureUtil;
import de.darwinspl.importer.FeatureModelConstraintsTuple;
import de.darwinspl.temporal.util.DwDateResolverUtil;
import de.darwinspl.temporal.util.DwEvolutionUtil;

public class DwFeatureModelEvolutionImporter {
	
	private DwSimpleEvolutionOperationFactory operationFactory = new DwSimpleEvolutionOperationFactory();
	


	/**
	 * Merges multiple feature model evolution snapshots to one Temporal Feature
	 * Model
	 * 
	 * @param models
	 *            Each feature model has to be associated to a date at which the
	 *            changes have occured.
	 * @param monitor 
	 * @return The whole feature model history merged into one TFM.
	 * @throws Exception
	 */
	public FeatureModelConstraintsTuple importFeatureModelEvolutionSnapshots(Map<Date, FeatureModelConstraintsTuple> models, IProgressMonitor monitor)
			throws Exception {
//		Map<Date, FeatureModelConstraintsTuple> darwinSPLModels = models;

//		for (Entry<FeatureModelConstraintsTuple, Date> entry : models.entrySet()) {
//			darwinSPLModels.put(entry.getValue(), entry.getKey());
//			
//			DwConstraintModel constraintModel = entry.getKey().getConstraintModel();
//			if(constraintModel != null) {
//				EcoreUtil.resolveAll(entry.getKey().getConstraintModel());				
//			}
//		}

		FeatureModelConstraintsTuple mergedModels = mergeFeatureModels(models, monitor);
		
		// TODO remove -> debug code.
//		List<Date> dates = DwEvolutionUtil.collectDates(mergedModels.getFeatureModel());
//		for(DwFeature feature: mergedModels.getFeatureModel().getFeatures()) {
//			if(feature.getGroupMembership().size() > 1) {
//				for(Date date: dates) {
//					if(DwEvolutionUtil.getValidTemporalElements(feature.getGroupMembership(), date).size() > 1) {
//						System.out.println("Culprit!");
//					}
//				}
//			}
//		}
		
		return mergedModels;
	}

	/**
	 * No evolution shall exist in the input models.
	 * 
	 * @param models
	 * @param monitor 
	 * @return
	 * @throws Exception
	 */
	protected FeatureModelConstraintsTuple mergeFeatureModels(Map<Date, FeatureModelConstraintsTuple> models, IProgressMonitor monitor)
			throws Exception {
		DwTemporalFeatureModel mergedFeatureModel = DwFeatureFactory.eINSTANCE.createDwTemporalFeatureModel();
		DwConstraintModel mergedConstraintModel = DwConstraintFactory.eINSTANCE.createDwConstraintModel();

		List<Date> sortedDateList = new ArrayList<Date>(models.keySet());
		sortedDateList.remove(null);
		Collections.sort(sortedDateList);

		ZonedDateTime zdt = ZonedDateTime.now();
		monitor.beginTask("Merging snapshots to evolution-aware temporal feature model...", models.size()+1);
		monitor.subTask("Importing base model...");
		
		long start = System.currentTimeMillis();
		if (models.get(null) != null) {
			mergeModels(models.get(null), mergedFeatureModel, mergedConstraintModel, null);
		}
		long end = System.currentTimeMillis();
		
		System.out.println("Importing base model took "+(end-start)+" milliseconds.");
		monitor.worked(1);
		if(monitor.isCanceled())
			return null;

		for (Date date : sortedDateList) {
			monitor.subTask("Importing model at date "+date);
			
			zdt = ZonedDateTime.now();
			start = System.currentTimeMillis();
			mergeModels(models.get(date), mergedFeatureModel, mergedConstraintModel, date);
			end = System.currentTimeMillis();
			
			System.out.println("Importing model at date "+date+ " took "+(end-start)+" milliseconds");
			monitor.worked(1);
			if(monitor.isCanceled())
				return null;
		}

		FeatureModelConstraintsTuple mergedModels = new FeatureModelConstraintsTuple(mergedFeatureModel, mergedConstraintModel);

		return mergedModels;
	}
	
	protected void mergeModels(FeatureModelConstraintsTuple modelsToBeMerged, DwTemporalFeatureModel mergedFeatureModel,
			DwConstraintModel mergedConstraintModel, Date date) throws Exception {


		List<DwFeature> featuresToInvalidate = new ArrayList<DwFeature>(DwEvolutionUtil.getValidTemporalElements(mergedFeatureModel.getFeatures(), date));

		// Step 1: Check that each feature / attribute exists. If not create it and set
		// valid since. If a feature / attribute existed before, but not anymore, set
		// valid until.
		DwTemporalFeatureModel featureModelToBeMerged = modelsToBeMerged.getFeatureModel();

		List<DwFeature> featuresToBeAddedToMergedModel = new ArrayList<DwFeature>();

		List<DwFeatureAttribute> addedFeatureAttributes = new ArrayList<DwFeatureAttribute>();

		ZonedDateTime zdt = ZonedDateTime.now();
		System.out.println(zdt.toString()+": Searching for matching features");

		// Key is the feature of the input model and value is the feature of the merged
		// model.
		Map<DwFeature, DwFeature> featureMap = matchFeatures(featureModelToBeMerged, mergedFeatureModel, featuresToInvalidate, addedFeatureAttributes, featuresToBeAddedToMergedModel, date);

		zdt = ZonedDateTime.now();
		System.out.println(zdt.toString()+": Searching for matching groups");
		
		Map<DwGroup, DwGroup> groupMap = matchGroups(featureModelToBeMerged, mergedFeatureModel, featureMap, date);

		consistencyCheck(mergedFeatureModel);
		
		zdt = ZonedDateTime.now();
		System.out.println(zdt.toString()+": Checking whether features have to be moved");
		moveFeaturesIfTheyHaveBeenMovedInInputModel(mergedFeatureModel, featureMap, groupMap, date);
		
		consistencyCheck(mergedFeatureModel);

		zdt = ZonedDateTime.now();
		System.out.println(zdt.toString()+": Checking whether feature types need to be updated");
		updateFeatureTypes(mergedFeatureModel, featureMap, date);
		
		consistencyCheck(mergedFeatureModel);

		zdt = ZonedDateTime.now();
		System.out.println(zdt.toString()+": Checking whether group types need to be updated");
		updateGroupTypes(groupMap, date);
		
		consistencyCheck(mergedFeatureModel);

		zdt = ZonedDateTime.now();
		System.out.println(zdt.toString()+": Adding new features to merged model");
		addNewFeatures(featuresToBeAddedToMergedModel, mergedFeatureModel, featureMap, date);
		
		consistencyCheck(mergedFeatureModel);

		zdt = ZonedDateTime.now();
		System.out.println(zdt.toString()+": Adding enums of feature attributes");
		// handle enums. Only considered enums which are used by feature attributes.
		Set<DwEnum> enums = new HashSet<DwEnum>();

		for (DwFeatureAttribute addedFeatureAttribute : addedFeatureAttributes) {
			if (addedFeatureAttribute instanceof DwEnumAttribute) {
				DwEnumAttribute enumAttribute = (DwEnumAttribute) addedFeatureAttribute;
				enums.add(enumAttribute.getEnum());
			}
		}

		mergedFeatureModel.getEnums().addAll(enums);

		consistencyCheck(mergedFeatureModel);
		
		zdt = ZonedDateTime.now();
		System.out.println(zdt.toString()+": Invalidating features");
		// Invalidate features which do not exist anymore. Update group composition.
		for (DwFeature featureToInvalidate : featuresToInvalidate) {
			DwFeatureDeleteOperation deleteOperation = operationFactory.createDwFeatureDeleteOperation(featureToInvalidate.getFeatureModel(), featureToInvalidate, date, true);
			DwEvolutionOperationInterpreter.executeWithoutAnalysis(deleteOperation);

			DwEvolutionUtil.getValidTemporalElement(featureToInvalidate.getNames(), date).getValidity().setTo(date);
			DwEvolutionUtil.getValidTemporalElement(featureToInvalidate.getTypes(), date).getValidity().setTo(date);
			
			for(DwFeatureAttribute attribute: DwEvolutionUtil.getValidTemporalElements(featureToInvalidate.getAttributes(), date)) {
				DwEvolutionUtil.getValidIntervalForDate(attribute, date).setTo(date);
			}
		}

		consistencyCheck(mergedFeatureModel);
		
		// Merge constraint models
		DwConstraintModel constraintModelToBeMerged = modelsToBeMerged.getConstraintModel();
		if (constraintModelToBeMerged != null) {
			zdt = ZonedDateTime.now();
			System.out.println(zdt.toString()+": Merging constraint model");
			mergedConstraintModel.setFeatureModel(mergedFeatureModel);
			mergeConstraintModel(constraintModelToBeMerged, mergedConstraintModel, featureMap, date);
		}
		
		consistencyCheck(mergedFeatureModel);

	}
	
	


	/**
	 * 
	 * @param featureModelToBeMerged
	 * @param mergedFeatureModel
	 * @param featuresToInvalidate List of all features from merged model which not have been matched. Will indicate which features won't have a matching partner and thus be invalidated.
	 * @param addedFeatureAttributes List of feature attributes which have been matched during this method
	 * @param featuresToBeAddedToMergedModel List of all features from input model which not have been matched and, thus, have to be added to the merged model
	 * @param date
	 * @return key: feature from model to be merged, value: feature from merged model
	 * @throws Exception
	 */
	protected Map<DwFeature, DwFeature> matchFeatures(DwTemporalFeatureModel featureModelToBeMerged, DwTemporalFeatureModel mergedFeatureModel, List<DwFeature> featuresToInvalidate, List<DwFeatureAttribute> addedFeatureAttributes, List<DwFeature> featuresToBeAddedToMergedModel, Date date) throws Exception {
		Map<DwFeature, DwFeature> featureMap = new HashMap<DwFeature, DwFeature>();

		List<DwFeature> potentialFeatureMatchingPartners = new ArrayList<DwFeature>();
		potentialFeatureMatchingPartners.addAll(DwEvolutionUtil.getValidTemporalElements(mergedFeatureModel.getFeatures(), date));
		
		for (DwFeature featureToBeMerged : featureModelToBeMerged.getFeatures()) {
			DwFeature equivalentFeature = checkIfElementAlreadyExists(featureToBeMerged, potentialFeatureMatchingPartners, date);
			boolean featureAlreadyExists = (equivalentFeature != null);

			if (!featureAlreadyExists) {
				featuresToBeAddedToMergedModel.add(featureToBeMerged);
			} else {
				potentialFeatureMatchingPartners.remove(equivalentFeature);
				featuresToInvalidate.remove(equivalentFeature);
				featureMap.put(featureToBeMerged, equivalentFeature);
				addedFeatureAttributes.addAll(mergeFeatureAttributes(featureToBeMerged, equivalentFeature, date));
			}
		}
		
		return featureMap;
	}
	
	/**
	 * 
	 * @param featureModelToBeMerged
	 * @param mergedFeatureModel
	 * @param featureMap key: feature from model to be merged, value: feature from merged model
	 * @param date
	 * @return key: group from model to be merged, value: group from merged model
	 */
	protected Map<DwGroup, DwGroup> matchGroups(DwTemporalFeatureModel featureModelToBeMerged, DwTemporalFeatureModel mergedFeatureModel, Map<DwFeature, DwFeature> featureMap, Date date) {
		Map<DwGroup, DwGroup> groupMap = new HashMap<DwGroup, DwGroup>();
		
		
		List<DwGroup> potentialGroupMatchingPartners = new ArrayList<DwGroup>();
		potentialGroupMatchingPartners.addAll(DwEvolutionUtil.getValidTemporalElements(mergedFeatureModel.getGroups(), date));
		
		DwGroup matchedGroupMatchingPartners = null;
		
		// ---- group matching based on parent. Assumes only one group per parent!
		// first link all And groups with the same parent.
		for (DwGroup groupToBeMerged : featureModelToBeMerged.getGroups()) {
			if(matchedGroupMatchingPartners != null) {
				potentialGroupMatchingPartners.remove(matchedGroupMatchingPartners);				
				matchedGroupMatchingPartners = null;
			}
			
			DwFeature parentFeatureOfGroupToBeMerged = DwEvolutionUtil.getValidTemporalElement(groupToBeMerged.getChildOf(), date).getParent();
			DwFeature equivalentToParentOfGroupToBeMerged = featureMap.get(parentFeatureOfGroupToBeMerged);
			
			if(equivalentToParentOfGroupToBeMerged != null) {
				List<DwParentFeatureChildGroupContainer> validFeatureChildren = DwEvolutionUtil.getValidTemporalElements(equivalentToParentOfGroupToBeMerged.getParentOf(), date);
				
				if(validFeatureChildren != null && validFeatureChildren.size() > 0) {
					if(validFeatureChildren.size() > 1) {
						System.err.println("Cannot perform group match as multiple child groups under feature "+DwEvolutionUtil.getValidTemporalElement(equivalentToParentOfGroupToBeMerged.getNames(), date).getName()+" at date "+date);
						continue;
					}
					
					DwGroup potentialMatchingPartner = validFeatureChildren.get(0).getChildGroup();
					if(potentialGroupMatchingPartners.contains(potentialMatchingPartner)) {
						groupMap.put(groupToBeMerged, potentialMatchingPartner);
						matchedGroupMatchingPartners = potentialMatchingPartner;
					}
					else {
						System.err.println("Cannot perform group match as input model seems to have multiple groups under feature "+ DwEvolutionUtil.getValidTemporalElement(parentFeatureOfGroupToBeMerged.getNames(), date).getName()+" at date "+date);
						continue;
					}
				}
			}
		}
		
		// ---- not working more sophisticated group matching
//		List<DwGroup> potentialGroupMatchingPartners = new ArrayList<DwGroup>();
//		potentialGroupMatchingPartners.addAll(DwEvolutionUtil.getValidTemporalElements(mergedFeatureModel.getGroups(), date));
//		
//		DwGroup matchedGroupMatchingPartners = null;
//		
//		// first link all And groups with the same parent.
//		linkAndGroupsLoop:
//		for (DwGroup groupToBeMerged : featureModelToBeMerged.getGroups()) {
//			if(!DwFeatureEvolutionUtil.getType(groupToBeMerged, date).getType().equals(DwGroupTypeEnum.AND)) {
//				// no and group -> skip
//				continue;
//			}
//			if(matchedGroupMatchingPartners != null) {
//				potentialGroupMatchingPartners.remove(matchedGroupMatchingPartners);				
//				matchedGroupMatchingPartners = null;
//			}
//			
//			DwFeature parentFeatureOfGroupToBeMerged = DwFeatureEvolutionUtil.getParentOfGroup(groupToBeMerged, date);
//			DwFeature equivalentToParentOfGroupToBeMerged = featureMap.get(parentFeatureOfGroupToBeMerged);
//			
//			if(equivalentToParentOfGroupToBeMerged != null) {
//				for(DwGroup groupFromMergedModel: potentialGroupMatchingPartners) {
//					if(!DwFeatureEvolutionUtil.getType(groupFromMergedModel, date).getType().equals(DwGroupTypeEnum.AND)) {
//						// no and group -> skip
//						continue;
//					}
//					
//					DwFeature parentFeatureOfPotentialMatchingPartner = DwFeatureEvolutionUtil.getParentOfGroup(groupFromMergedModel, date);
//					if(parentFeatureOfPotentialMatchingPartner == equivalentToParentOfGroupToBeMerged) {
//						groupMap.put(groupToBeMerged, groupFromMergedModel);
//						matchedGroupMatchingPartners = groupFromMergedModel;
//						continue linkAndGroupsLoop;
//					}
//				}
//			}
//		}
//		
//		matchedGroupMatchingPartners = null;
//		
//		groupsToBeMergedLoop:
//		for (DwGroup groupToBeMerged : featureModelToBeMerged.getGroups()) {
//			// Only compare the group to be merged to groups which are valid at that point in time.
//			if(matchedGroupMatchingPartners != null) {
//				potentialGroupMatchingPartners.remove(matchedGroupMatchingPartners);				
//				matchedGroupMatchingPartners = null;
//			}
//			
//			for(DwGroup groupFromMergedModel: potentialGroupMatchingPartners) {
//				int amountOfSimilarFeatures = 0;
//
//				List<DwFeature> featuresOfMergedGroup = DwFeatureEvolutionUtil.getFeaturesOfGroup(groupFromMergedModel,
//						date);
//
//				for (DwFeature featureOfGroupToBeMerged : DwFeatureEvolutionUtil.getFeaturesOfGroup(groupToBeMerged,
//						date)) {
//					if (featuresOfMergedGroup.contains(featureMap.get(featureOfGroupToBeMerged))) {
//						amountOfSimilarFeatures++;
//					}
//				}
//
//				double sameFeatureRatio = (double) amountOfSimilarFeatures / (double) featuresOfMergedGroup.size();
//				if (sameFeatureRatio >= 0.75) {
//					groupMap.put(groupToBeMerged, groupFromMergedModel);
//					matchedGroupMatchingPartners = groupFromMergedModel;
//					continue groupsToBeMergedLoop;
//				}
//			}
//		}
		
		return groupMap;
	}
	
	
	
	/**
	 * Creates a new group if necessary.
	 * 
	 * @param featureToBeAdded
	 * @param groupCompositionOfFeatureToBeAdded
	 * @param parentInMergedModel
	 * @param groupTypeEnum
	 * @param featureMap
	 * @param mergedFeatureModel
	 * @param date
	 */
	protected void addFeatureToBestMatchingGroup(DwFeature featureToBeAdded,
			DwGroupComposition groupCompositionOfFeatureToBeAdded, DwFeature parentInMergedModel,
			DwGroupTypeEnum groupTypeEnum, Map<DwFeature, DwFeature> featureMap, DwTemporalFeatureModel mergedFeatureModel,
			Date date) {
		
		DwGroup matchingGroup = getBestMatchingGroup(featureToBeAdded, groupCompositionOfFeatureToBeAdded, parentInMergedModel, groupTypeEnum, featureMap, date);

		addFeatureToGroup(featureToBeAdded, groupCompositionOfFeatureToBeAdded, matchingGroup, parentInMergedModel, groupTypeEnum, mergedFeatureModel, date);
	}
	
	protected DwGroup getBestMatchingGroup(DwFeature featureToBeAdded,
			DwGroupComposition groupCompositionOfFeatureToBeAdded, DwFeature parentInMergedModel,
			DwGroupTypeEnum groupTypeEnum, Map<DwFeature, DwFeature> featureMap, Date date) {
		
		// ---- Assuming that each feature has only one child group
		List<DwParentFeatureChildGroupContainer> featureChildrenOfParentInMergedModel = DwEvolutionUtil.getValidTemporalElements(parentInMergedModel.getParentOf(), date);
		if(featureChildrenOfParentInMergedModel == null || featureChildrenOfParentInMergedModel.size() == 0) {
			return null;
		}
		
		if(featureChildrenOfParentInMergedModel.size() > 1) {
			System.err.println("");
			return null;
		}
		
		return featureChildrenOfParentInMergedModel.get(0).getChildGroup();
		
		// ---- TODO More sophisticated but not yet working. Needs thinking
//		DwGroup matchingGroup = null;
//		int maximumAmountOfMatchingSiblings = 0;
//
//		for (DwFeatureChild featureChild : DwEvolutionUtil.getValidTemporalElements(parentInMergedModel.getParentOf(),
//				date)) {
//			
//			DwGroup childGroupOfParentInMergedModel = featureChild.getChildGroup();
//			if (DwEvolutionUtil.getValidTemporalElement(childGroupOfParentInMergedModel.getTypes(), date).getType().equals(groupTypeEnum)) {
//
//				if(DwEvolutionUtil.getValidTemporalElement(childGroupOfParentInMergedModel.getTypes(), date).getType().equals(DwGroupTypeEnum.AND)) {
//					// For and groups it does not matter in which group they are. just return it.
//					return childGroupOfParentInMergedModel;
//				}
//				
//				int amountOfMatchingSiblings = 0;
//
//				for (DwFeature featureInGroup : groupCompositionOfFeatureToBeAdded.getFeatures()) {
//					if (featureInGroup == featureToBeAdded) {
//						continue;
//					}
//
//					if (featureMap.get(featureInGroup) != null) {
//						amountOfMatchingSiblings++;
//					}
//				}
//
//				if (amountOfMatchingSiblings > maximumAmountOfMatchingSiblings) {
//					amountOfMatchingSiblings = maximumAmountOfMatchingSiblings;
//					matchingGroup = childGroupOfParentInMergedModel;
//				}
//			}
//		}
//		
//		return matchingGroup;
	}
	
	protected void addFeatureToGroup(DwFeature featureToBeAdded, DwGroupComposition oldGroupComposition, DwGroup targetGroup, DwFeature newParentFeature, DwGroupTypeEnum groupTypeEnum, DwTemporalFeatureModel mergedFeatureModel, Date date) {
		if (targetGroup != null) {
//			// Add feature to best matching alternative group
//			DwGroupComposition newGroupComposition;
//			
//			DwGroupComposition oldGroupCompositionOfTargetGroup = DwEvolutionUtil.getValidTemporalElement(targetGroup.getParentOf(), date);
//			
//			// Prevents the creation of a new group composition for each new feature that is added.
//			if(oldGroupCompositionOfTargetGroup.getValidity().getFrom() != null && oldGroupCompositionOfTargetGroup.getValidity().getFrom().equals(date)) {
//				newGroupComposition = oldGroupCompositionOfTargetGroup;
//			}
//			else {
//				oldGroupCompositionOfTargetGroup.getValidity().setTo(date);
//				
//				newGroupComposition = DwFactoryCustom.createNewGroupComposition(targetGroup, date);
//				newGroupComposition.getChildFeatures().addAll(oldGroupCompositionOfTargetGroup.getChildFeatures());				
//			}
//			
//			newGroupComposition.getChildFeatures().add(featureToBeAdded);
			
			// TODO: Prevent the creation of a new group composition for each new feature that is added.
			DwEvolutionOperationInterpreter.execute(operationFactory.createDwFeatureAddOperation(mergedFeatureModel, featureToBeAdded, targetGroup, false, date, true));
		} else {
//			// create a new group
//			DwGroup newGroup = DwFactoryCustom.createNewGroup(groupTypeEnum, date);
//			mergedFeatureModel.getGroups().add(newGroup);
//
//			DwParentFeatureChildGroupContainer newFeatureChild = DwFactoryCustom.createNewParentFeatureChildGroupContainer(newParentFeature, date);
//			newFeatureChild.setChildGroup(newGroup);
//
//			DwGroupComposition newGroupComposition = DwFactoryCustom.createNewGroupComposition(newGroup, date);
//			newGroupComposition.getChildFeatures().add(featureToBeAdded);
			
			DwGroupCreateOperation newGroupCreateOp = operationFactory.createDwGroupCreateOperation(mergedFeatureModel, newParentFeature, date, true);
			DwEvolutionOperationInterpreter.execute(newGroupCreateOp);
			
			targetGroup = newGroupCreateOp.getGroup();
			DwEvolutionOperationInterpreter.execute(operationFactory.createDwFeatureAddOperation(mergedFeatureModel, featureToBeAdded, targetGroup, false, date, true));
		}

		oldGroupComposition.getChildFeatures().remove(featureToBeAdded);
	}
	
	

	/**
	 * 
	 * @param elementToBeChecked
	 * @param elementsOfMergedModel
	 * @return null if no equivalent element exists. Otherwise the equivalentElement
	 */
	protected <S extends EObject> S checkIfElementAlreadyExists(S elementToBeChecked, List<S> elementsOfMergedModel,
			Date date) {
		for (S featureOfMergedModel : elementsOfMergedModel) {

			if (checkForEquality(elementToBeChecked, featureOfMergedModel, date)) {
				return featureOfMergedModel;
			}
		}

		return null;
	}

	protected boolean checkForEquality(EObject o1, EObject o2, Date date) {
		if (o1 instanceof DwNamedElement && o2 instanceof DwNamedElement) {
			DwNamedElement namedElement1 = (DwNamedElement) o1;
			DwNamedElement namedElement2 = (DwNamedElement) o2;

			DwName name1 = DwEvolutionUtil.getValidTemporalElement(namedElement1.getNames(), date);
			DwName name2 = DwEvolutionUtil.getValidTemporalElement(namedElement2.getNames(), date);

			if (name1.getName().equals(name2.getName())) {
				return true;
			} else {
				return false;
			}
		} else if (o1 instanceof DwEnumLiteral && o2 instanceof DwEnumLiteral) {
			DwEnumLiteral enumLiteral1 = (DwEnumLiteral) o1;
			DwEnumLiteral enumLiteral2 = (DwEnumLiteral) o2;

			if (enumLiteral1.getName().equals(enumLiteral2.getName())) {
				return true;
			} else {
				return false;
			}
		}

		return false;
	}
	
	

	protected List<DwFeatureAttribute> mergeFeatureAttributes(DwFeature featureToBeMerged,
			DwFeature equivalentFeatureOfMergedModel, Date date) throws Exception {
		// Step 2: check whether attributes are matching.
		List<DwFeatureAttribute> attributesToBeAddedToEquivalentFeature = new ArrayList<DwFeatureAttribute>();

		for (DwFeatureAttribute attributeOfFeatureToBeMerged : featureToBeMerged.getAttributes()) {

			DwFeatureAttribute equivalentAttribute = checkIfElementAlreadyExists(attributeOfFeatureToBeMerged,
					DwEvolutionUtil.getValidTemporalElements(equivalentFeatureOfMergedModel.getAttributes(), date),
					date);
			boolean attributeAlreadyExists = (equivalentAttribute != null);

			if (!attributeAlreadyExists) {
				attributesToBeAddedToEquivalentFeature.add(attributeOfFeatureToBeMerged);
			} else {
				// Step 1.a: Check whether attribute domains are the same. If not, extend them
				// to have the union of their domains
				if (equivalentAttribute instanceof DwNumberAttribute
						&& attributeOfFeatureToBeMerged instanceof DwNumberAttribute) {
					DwNumberAttribute equivalentNumberAttribute = (DwNumberAttribute) equivalentAttribute;
					DwNumberAttribute numberAttributeOfFeatureToBeMerged = (DwNumberAttribute) attributeOfFeatureToBeMerged;

					double minimum = Math.min(equivalentNumberAttribute.getMin(),
							numberAttributeOfFeatureToBeMerged.getMin());
					double maximum = Math.max(equivalentNumberAttribute.getMax(),
							numberAttributeOfFeatureToBeMerged.getMax());

					equivalentNumberAttribute.setMin(minimum);
					equivalentNumberAttribute.setMax(maximum);
				} else if (equivalentAttribute instanceof DwBooleanAttribute
						&& attributeOfFeatureToBeMerged instanceof DwBooleanAttribute) {
					// nothing to do.
				} else if (equivalentAttribute instanceof DwEnumAttribute
						&& attributeOfFeatureToBeMerged instanceof DwEnumAttribute) {
					DwEnumAttribute equivalentEnumAttribute = (DwEnumAttribute) equivalentAttribute;
					DwEnumAttribute enumAttributeOfFeatureToBeMerged = (DwEnumAttribute) attributeOfFeatureToBeMerged;
					mergeEnumAttributes(equivalentEnumAttribute, enumAttributeOfFeatureToBeMerged, date);
				} else if (equivalentAttribute instanceof DwStringAttribute
						&& attributeOfFeatureToBeMerged instanceof DwStringAttribute) {
					// nothing to do.
				} else {
					throw new Exception("Attributes "
							+ DwEvolutionUtil.getValidTemporalElement(equivalentAttribute.getNames(), date).getName()
							+ " have the same name but have different types. Cannot merge.");
				}
			}

		}

		for (DwFeatureAttribute attributeToBeAdded : attributesToBeAddedToEquivalentFeature) {
			DwEvolutionUtil.setCurrentTemporalValidity(attributeToBeAdded, date, date, null);
			equivalentFeatureOfMergedModel.getAttributes().add(attributeToBeAdded);
		}

		return attributesToBeAddedToEquivalentFeature;
	}

	protected void mergeEnumAttributes(DwEnumAttribute equivalentEnumAttribute,
			DwEnumAttribute enumAttributeOfFeatureToBeMerged, Date date) throws Exception {
		if (equivalentEnumAttribute.getEnum() == null || enumAttributeOfFeatureToBeMerged.getEnum() == null) {
			throw new Exception("Enum type of attribute "
					+ DwEvolutionUtil.getValidTemporalElement(equivalentEnumAttribute.getNames(), date).getName()
					+ " of either the merged or the input model is null. Aborting Merge!");
		}

		if (equivalentEnumAttribute.getEnum().getName()
				.equals(enumAttributeOfFeatureToBeMerged.getEnum().getName())) {

			List<DwEnumLiteral> literalsToBeAdded = new ArrayList<DwEnumLiteral>();

			for (DwEnumLiteral enumLiteralOfFeatureAttributeToBeMerged : enumAttributeOfFeatureToBeMerged.getEnum()
					.getLiterals()) {

				DwEnumLiteral equivalentEnumLiteral = checkIfElementAlreadyExists(
						enumLiteralOfFeatureAttributeToBeMerged, DwEvolutionUtil.getValidTemporalElements(
								equivalentEnumAttribute.getEnum().getLiterals(), date),
						date);
				boolean equivalentLiteralExists = (equivalentEnumLiteral != null);

				if (!equivalentLiteralExists) {
					literalsToBeAdded.add(enumLiteralOfFeatureAttributeToBeMerged);
				}
			}

			List<Integer> alreadyUsedLiteralValues = new ArrayList<Integer>();

			if (!literalsToBeAdded.isEmpty()) {
				for (DwEnumLiteral literalOfEquivalentAttribute : equivalentEnumAttribute.getEnum().getLiterals()) {
					alreadyUsedLiteralValues.add(literalOfEquivalentAttribute.getValue());
				}
			}

			for (DwEnumLiteral literalToBeAdded : literalsToBeAdded) {
				int value = 0;
				while (alreadyUsedLiteralValues.contains(value)) {
					value++;
				}
				alreadyUsedLiteralValues.add(value);

				literalToBeAdded.setValue(value);
				equivalentEnumAttribute.getEnum().getLiterals().add(literalToBeAdded);
			}
		} else {
			throw new Exception("Attribute names of "
					+ DwEvolutionUtil.getValidTemporalElement(equivalentEnumAttribute.getNames(), date).getName()
					+ " are the same but enum types do not match. Aborting merge!");
		}

	}
	
	

	protected void updateFeatureTypes(DwTemporalFeatureModel mergedFeatureModel, Map<DwFeature, DwFeature> featureMap, Date date) {
		for (Entry<DwFeature, DwFeature> entry : featureMap.entrySet()) {
			DwFeature mergedFeature = entry.getValue();
			DwFeature inputFeature = entry.getKey();
			
			// key feature from input mode, value feature from merged model
			DwFeatureType inputType = DwEvolutionUtil.getValidTemporalElement(inputFeature.getTypes(), date);
			DwFeatureType mergedType = DwEvolutionUtil.getValidTemporalElement(mergedFeature.getTypes(), date);

			if (!inputType.getType().equals(mergedType.getType())) {
//				mergedType.getValidity().setTo(date);
//
//				DwFeatureType newFeatureType = DwFactoryCustom.createNewFeatureType(inputType.getType(), date);
//				mergedFeature.getTypes().add(newFeatureType);
				
				DwEvolutionOperationInterpreter.execute(operationFactory.createDwFeatureTypeChangeOperation(mergedFeatureModel, mergedFeature, inputType.getType(), date, true));
			}
		}
	}

	protected void updateGroupTypes(Map<DwGroup, DwGroup> groupMap, Date date) {
		for (Entry<DwGroup, DwGroup> entry : groupMap.entrySet()) {
			DwGroup mergedGroup = entry.getValue();
			DwGroup inputGroup = entry.getKey();
			
			DwGroupType groupTypeOfGroupToBeMerged = DwEvolutionUtil.getValidTemporalElement(inputGroup.getTypes(), date);
			DwGroupType groupTypeOfMergedGroup = DwEvolutionUtil.getValidTemporalElement(mergedGroup.getTypes(), date);

			if (!groupTypeOfGroupToBeMerged.getType().equals(groupTypeOfMergedGroup.getType())) {
//				groupTypeOfMergedGroup.getValidity().setTo(date);
//
//				DwGroupType newGroupType = DwFactoryCustom.createNewGroupType(groupTypeOfGroupToBeMerged.getType(), date);
//				mergedGroup.getTypes().add(newGroupType);
				
				DwEvolutionOperationInterpreter.execute(operationFactory.createDwGroupTypeChangeOperation(mergedGroup.getFeatureModel(), mergedGroup, groupTypeOfGroupToBeMerged.getType(), date, true));
			}
		}
	}
	
	

	protected void addNewFeatures(List<DwFeature> featuresToBeAddedToMergedModel, DwTemporalFeatureModel mergedFeatureModel,
			Map<DwFeature, DwFeature> featureMap, Date date) throws Exception {
		
		for (DwFeature featureToBeAdded : featuresToBeAddedToMergedModel) {
			
			// set validity of feature, its name and type to date
			DwTemporalFeatureModel oldFeatureModel = featureToBeAdded.getFeatureModel();
			mergedFeatureModel.getFeatures().add(featureToBeAdded);
			DwEvolutionUtil.setCurrentTemporalValidity(featureToBeAdded, date, date, DwDateResolverUtil.ETERNITY_DATE);

			// clear everything from evolution.
			DwName validName = DwEvolutionUtil.getValidTemporalElement(featureToBeAdded.getNames(), date);
			DwEvolutionUtil.setTemporalValidity(validName, date, DwDateResolverUtil.ETERNITY_DATE);
			featureToBeAdded.getNames().clear();
			featureToBeAdded.getNames().add(validName);

			DwFeatureType validType = DwEvolutionUtil.getValidTemporalElement(featureToBeAdded.getTypes(), date);
			DwEvolutionUtil.setTemporalValidity(validType, date, DwDateResolverUtil.ETERNITY_DATE);
			featureToBeAdded.getTypes().clear();
			featureToBeAdded.getTypes().add(validType);
			
			
			
//			DwFeatureCreateOperation createOp = operationFactory.createDwFeatureCreateOperation(mergedFeatureModel, parent, false, date, true);
//			DwEvolutionOperationInterpreter.execute(createOp);
//			
//			DwFeature newFeature = createOp.getFeature();
//			String newFeatureName = DwEvolutionUtil.getValidTemporalElement(featureToBeAdded.getNames(), date).getName();
//			DwFeatureTypeEnum newFeatureNameType = DwEvolutionUtil.getValidTemporalElement(featureToBeAdded.getTypes(), date).getType();
//			
//			DwFeatureRenameOperation renameOp = operationFactory.createDwFeatureRenameOperation(newFeature, newFeatureName, date, true);
//			DwEvolutionOperationInterpreter.execute(renameOp);
//			
//			DwFeatureTypeChangeOperation typeChangeOp = operationFactory.createDwFeatureTypeChangeOperation(newFeature, newFeatureNameType, date, true);
//			DwEvolutionOperationInterpreter.execute(typeChangeOp);
			
			
			
			List<DwParentFeatureChildGroupContainer> validFeatureChildren = DwEvolutionUtil.getValidTemporalElements(featureToBeAdded.getParentOf(), date);
			
			featureToBeAdded.getParentOf().clear();
			
			for (DwParentFeatureChildGroupContainer featureChild : validFeatureChildren) {
				featureToBeAdded.getParentOf().add(featureChild);
				DwEvolutionUtil.setTemporalValidity(featureChild, date, DwDateResolverUtil.ETERNITY_DATE);

				DwGroup group = featureChild.getChildGroup();
				mergedFeatureModel.getGroups().add(group);
				DwEvolutionUtil.setCurrentTemporalValidity(group, date, date, DwDateResolverUtil.ETERNITY_DATE);

				DwGroupType validGroupType = DwEvolutionUtil.getValidTemporalElement(group.getTypes(), date);
				DwEvolutionUtil.setTemporalValidity(validGroupType, date, DwDateResolverUtil.ETERNITY_DATE);
				group.getTypes().clear();
				group.getTypes().add(validGroupType);

				DwGroupComposition validGroupComposition = DwEvolutionUtil.getValidTemporalElement(group.getParentOf(), date);
				DwEvolutionUtil.setTemporalValidity(validGroupComposition, date, DwDateResolverUtil.ETERNITY_DATE);
				group.getParentOf().clear();
				group.getParentOf().add(validGroupComposition);
				
				List<DwFeature> featuresOfGroupComposition = new ArrayList<DwFeature>();
				featuresOfGroupComposition.addAll(validGroupComposition.getChildFeatures());
				
				for(DwFeature featureOfGroupComposition: featuresOfGroupComposition) {
					DwFeature equivalentFeature = featureMap.get(featureOfGroupComposition);
					if(equivalentFeature != null) {
						validGroupComposition.getChildFeatures().remove(featureOfGroupComposition);
						validGroupComposition.getChildFeatures().add(equivalentFeature);
					}
				}
				
			}

			// Put feature in correct group

			DwGroupComposition groupComposition = DwEvolutionUtil
					.getValidTemporalElement(featureToBeAdded.getGroupMembership(), date);

			if (groupComposition != null) {
				DwGroup group = groupComposition.getCompositionOf();
				DwParentFeatureChildGroupContainer featureChildOfGroup = DwEvolutionUtil.getValidTemporalElement(group.getChildOf(), date);
				DwFeature parentFeatureInInputModel = featureChildOfGroup.getParent();

				if (featuresToBeAddedToMergedModel.contains(parentFeatureInInputModel)) {
					// parent feature is also added to the new model. We are done.
					continue;
				}

				DwFeature parentInMergedModel = featureMap.get(parentFeatureInInputModel);
				if (parentInMergedModel != null) {
					DwGroupType groupType = DwEvolutionUtil.getValidTemporalElement(group.getTypes(), date);

					DwGroupTypeEnum groupTypeEnum = groupType.getType();
					addFeatureToBestMatchingGroup(featureToBeAdded, groupComposition, parentInMergedModel,
							groupTypeEnum, featureMap, mergedFeatureModel, date);
				} else {
					throw new Exception("We have a problem. The parent of the feature "
							+ DwEvolutionUtil.getValidTemporalElement(featureToBeAdded.getNames(), date).getName()
							+ " is neither newly added to the merged model nor is it already existent in it. Aborting whole merging process");
				}
				
				
			} 
			else {
				DwRootFeatureContainer oldFeatureModelRootFeatureContainer = DwEvolutionUtil.getValidTemporalElement(oldFeatureModel.getRootFeatures(), date);

				if (oldFeatureModelRootFeatureContainer == null || oldFeatureModelRootFeatureContainer.getFeature() != featureToBeAdded) {
					throw new Exception("Feature "
							+ DwEvolutionUtil.getValidTemporalElement(featureToBeAdded.getNames(), date).getName()
							+ " did not have a group but is no root feature. Aborted merge.");
				}

				DwRootFeatureContainer oldRootFeatureContainer = DwEvolutionUtil.getValidTemporalElement(mergedFeatureModel.getRootFeatures(), date);
				if (oldRootFeatureContainer != null) {
					oldRootFeatureContainer.getValidity().setTo(date);
				}

				DwRootFeatureContainer newRootFeatureContainer = DwSimpleFeatureFactoryCustom.createNewRootFeatureContainer(featureToBeAdded, date);
				mergedFeatureModel.getRootFeatures().add(newRootFeatureContainer);
			}
		}
	}

	protected void moveFeaturesIfTheyHaveBeenMovedInInputModel(DwTemporalFeatureModel mergedFeatureModel,
			Map<DwFeature, DwFeature> featureMap, Map<DwGroup, DwGroup> groupMap, Date date) {
		for (Entry<DwFeature, DwFeature> featureEntrySet : featureMap.entrySet()) {
			
			DwFeature parentOfMergedFeature = DwFeatureUtil.getParentFeatureOfFeature(featureEntrySet.getValue(), date);
			
			DwFeature parentFeatureFromInputModel = DwFeatureUtil.getParentFeatureOfFeature(featureEntrySet.getKey(), date);

			if (parentFeatureFromInputModel == null) {
				// seems to be new root.
				continue;
			}

			DwFeature equivalentToParentFromInputModel = featureMap.get(parentFeatureFromInputModel);

			if (equivalentToParentFromInputModel == null) {
				// Parent will be added to the merged model.
				// Has to be removed from its current group.
				// When the new parent will be added to the model, this feature will be added to its group.
//				DwFeatureUtil.removeFeatureFromGroup(featureEntrySet.getValue(), date);
				DwFeatureDetachOperation detachOperation = operationFactory.createDwFeatureDetachOperation(featureEntrySet.getValue().getFeatureModel(), featureEntrySet.getValue(), date, true);
				DwEvolutionOperationInterpreter.executeWithoutAnalysis(detachOperation);
			} else if (equivalentToParentFromInputModel == parentOfMergedFeature) {
				continue;
				// TODO may be wrong if the group changed for the same parent
//				DwFeature inputFeature = featureEntrySet.getKey();
//				DwFeature equivalentFeature = featureEntrySet.getValue();
//				
//				DwGroup groupOfInputFeature = DwEvolutionUtil.getValidTemporalElement(inputFeature.getGroupMembership(), date).getCompositionOf();
//				DwGroupType groupTypeOfInputGroup = DwEvolutionUtil.getValidTemporalElement(groupOfInputFeature.getTypes(), date);
//				DwGroupComposition groupCompositionOfInputFeature = DwEvolutionUtil.getValidTemporalElement(inputFeature.getGroupMembership(), date);
//				
//				DwGroup groupOfEquivalentFeature = DwEvolutionUtil.getValidTemporalElement(equivalentFeature.getGroupMembership(), date).getCompositionOf();
//				
//				DwGroup equivalentGroup = groupMap.get(groupOfInputFeature);
//				
//				if(equivalentGroup != null && equivalentGroup == groupOfEquivalentFeature) {
//					// groups also stayed the same. done
//					continue;
//				}
//				
//				System.out.println("New Feature Move method");
//				// Group didn't stay the same.
//				DwGroup bestMatchingGroupOfMergedModel = getBestMatchingGroup(inputFeature, groupCompositionOfInputFeature, equivalentToParentFromInputModel, groupTypeOfInputGroup.getType(), featureMap, date);
//				if(bestMatchingGroupOfMergedModel != null) {
//					if(bestMatchingGroupOfMergedModel == groupOfEquivalentFeature) {
//						// done. best matching group is still the same as before.
//						continue;
//					}
//				}
//				addFeatureToGroup(inputFeature, groupCompositionOfInputFeature, bestMatchingGroupOfMergedModel, equivalentToParentFromInputModel, groupTypeOfInputGroup.getType(), mergedFeatureModel, date);
			} else {
				// parents are different.
				DwGroupComposition groupCompositionOfFeatureFromInputModel =
						DwEvolutionUtil.getValidTemporalElement(featureEntrySet.getKey().getGroupMembership(), date);
				DwGroupTypeEnum groupTypeOfGroupOfFeatureFromInputModel =
						DwEvolutionUtil.getValidTemporalElement(groupCompositionOfFeatureFromInputModel.getCompositionOf().getTypes(), date).getType();

				addFeatureToBestMatchingGroup(featureEntrySet.getValue(), groupCompositionOfFeatureFromInputModel,
						equivalentToParentFromInputModel, groupTypeOfGroupOfFeatureFromInputModel, featureMap,
						mergedFeatureModel, date);

//				DwFeatureEvolutionUtil.removeFeatureFromGroup(featureEntrySet.getValue(), date);
				
				DwGroup matchingGroup = getBestMatchingGroup(featureEntrySet.getValue(), groupCompositionOfFeatureFromInputModel, equivalentToParentFromInputModel, groupTypeOfGroupOfFeatureFromInputModel, featureMap, date);
				
				DwFeatureDetachOperation detachOperation = operationFactory.createDwFeatureDetachOperation(featureEntrySet.getValue().getFeatureModel(), featureEntrySet.getValue(), date, true);
				DwEvolutionOperationInterpreter.executeWithoutAnalysis(detachOperation);
			}
		}
	}
	protected void mergeConstraintModel(DwConstraintModel constraintModelToBeMerged,
			DwConstraintModel mergedConstraintModel, Map<DwFeature, DwFeature> featureMap, Date date) {
		// For each constraint in model to be merged:
		// Create a map, in which constraints from the model to be merged are mapped to
		// equal constraints from the merged model

		// Key is the constraint from model to be merged and value is constraint from
		// merged model
		List<DwConstraint> constraintsToBeMergedWithoutMatchingPartner = new ArrayList<DwConstraint>(
				constraintModelToBeMerged.getConstraints());
		List<DwConstraint> remainingMatchingPartners = new ArrayList<DwConstraint>(
				DwEvolutionUtil.getValidTemporalElements(mergedConstraintModel.getConstraints(), date));
		
		ZonedDateTime zdt = ZonedDateTime.now();
		System.out.println(zdt.toString()+": Finding matching constraints");
		for (DwConstraint constraintToBeMerged : constraintModelToBeMerged.getConstraints()) {
			DwConstraint equalConstraint = findEqualConstraint(constraintToBeMerged, remainingMatchingPartners,
					featureMap, date);
			
			if (equalConstraint != null) {
				remainingMatchingPartners.remove(equalConstraint);
				constraintsToBeMergedWithoutMatchingPartner.remove(constraintToBeMerged);
				zdt = ZonedDateTime.now();
//				System.err.println(zdt.toString()+": Matched a constraint.");
				continue;
			}
		}

		// For each constraint of the model to be merged that does not have a mapping
		// partner: add the constraint with valid since
		
		zdt = ZonedDateTime.now();
		System.out.println(zdt.toString()+": Adding new constraints");
		for (DwConstraint constraintToBeMergedWithoutMatchingPartner : constraintsToBeMergedWithoutMatchingPartner) {
			mergedConstraintModel.getConstraints()
					.add(createConstraintInMergedModel(constraintToBeMergedWithoutMatchingPartner, featureMap, date));
		}

		// For each constraint of the merged model that does not have a mapping partner:
		// set the valid until of the constraint
		
		zdt = ZonedDateTime.now();
		System.out.println(zdt.toString()+": Invalidate constraints");
		for (DwConstraint unmatchedConstraint : remainingMatchingPartners) {
			unmatchedConstraint.getValidity().setTo(date);
		}
	}

	protected DwConstraint findEqualConstraint(DwConstraint constraint, List<DwConstraint> potentialMatchingPartners,
			Map<DwFeature, DwFeature> featureMap, Date date) {
		for (DwConstraint potentialMatchingPartner : potentialMatchingPartners) {
			if (areExpressionsEqual(constraint.getRootExpression(), potentialMatchingPartner.getRootExpression(),
					featureMap, date)) {
				return potentialMatchingPartner;
			}
		}

		return null;
	}

	protected boolean areExpressionsEqual(DwExpression expressionOfConstraintToBeMerged,
			DwExpression expressionOfMergedConstraint, Map<DwFeature, DwFeature> featureMap, Date date) {
		if (expressionOfConstraintToBeMerged.getClass().toString()
				.equals(expressionOfMergedConstraint.getClass().toString())) {
			if (expressionOfConstraintToBeMerged instanceof DwBinaryExpression) {
				DwBinaryExpression bin1 = (DwBinaryExpression) expressionOfConstraintToBeMerged;
				DwBinaryExpression bin2 = (DwBinaryExpression) expressionOfMergedConstraint;

				boolean operandsMatch = false;

				operandsMatch = areExpressionsEqual(bin1.getOperand1(), bin2.getOperand1(), featureMap, date)
						&& areExpressionsEqual(bin1.getOperand2(), bin2.getOperand2(), featureMap, date);

				return operandsMatch;
			} else if (expressionOfConstraintToBeMerged instanceof DwUnaryExpression) {
				DwUnaryExpression unary1 = (DwUnaryExpression) expressionOfConstraintToBeMerged;
				DwUnaryExpression unary2 = (DwUnaryExpression) expressionOfMergedConstraint;

				return areExpressionsEqual(unary1.getOperand(), unary2.getOperand(), featureMap, date);
			} else if (expressionOfConstraintToBeMerged instanceof DwFeatureReferenceExpression) {
				DwFeatureReferenceExpression featureRef1 = (DwFeatureReferenceExpression) expressionOfConstraintToBeMerged;
				DwFeatureReferenceExpression featureRef2 = (DwFeatureReferenceExpression) expressionOfMergedConstraint;	
				
				DwFeature equivalentFeatureFromMergedFeatureModel = featureMap.get(featureRef1.getFeature());
				boolean featuresMatch = equivalentFeatureFromMergedFeatureModel==featureRef2.getFeature();
				return featuresMatch;
			} else {
				System.err.println("Currently unsupported expressions have been compared in de.darwinspl.importer.evolution.DwFeatureModelEvolutionImporter.areExpressionsEqual(DwExpression, DwExpression, Map<DwFeature, DwFeature>, Date) " + expressionOfConstraintToBeMerged.getClass());
				return false;
			}
		} else {
			return false;
		}
	}

	protected DwConstraint createConstraintInMergedModel(DwConstraint constraintToBeMerged,
			Map<DwFeature, DwFeature> featureMap, Date date) {
		DwConstraint constraint = EcoreUtil.copy(constraintToBeMerged);
		DwEvolutionUtil.setTemporalValidity(constraint, date, null);

		TreeIterator<EObject> iterator = constraint.eAllContents();
		while (iterator.hasNext()) {
			EObject eObject = iterator.next();
			if (eObject instanceof DwFeatureReferenceExpression) {
				DwFeatureReferenceExpression featureReference = (DwFeatureReferenceExpression) eObject;
				// TODO ??? no versions are supported
//				featureReference.setVersionRestriction(null);

				DwFeature equivalentFeatureInMergedModel = featureMap.get(featureReference.getFeature());
				if(equivalentFeatureInMergedModel != null) {
					featureReference.setFeature(equivalentFeatureInMergedModel);					
				}
			}
		}

		return constraint;
	}

	/**
	 * Only use this method if you have the feeling that something isn't imported correctly. 
	 * @param mergedFeatureModel
	 */
	private void consistencyCheck(DwTemporalFeatureModel mergedFeatureModel) {
		
//		List<Date> dates = DwEvolutionUtil.collectDates(mergedFeatureModel);
//		if(dates.size() != 0) {
//			Date firstDate = dates.get(0);
//			Calendar cal = new GregorianCalendar();
//			cal.setTime(firstDate);
//			cal.add(Calendar.DAY_OF_MONTH, -1);
//			Date beforeFirstDate = cal.getTime();
//			dates.add(beforeFirstDate);
//			
//			if(dates.size() > 1) {
//				Date lastDate = dates.get(dates.size()-1);
//				cal.setTime(lastDate);
//				cal.add(Calendar.DAY_OF_MONTH, 1);
//				Date afterLastDate = cal.getTime();
//				dates.add(afterLastDate);
//			}
//			
//			Collections.sort(dates);
//		}
//		
//		for(Date date: dates) {
//			for(DwFeature feature: DwEvolutionUtil.getValidTemporalElements(mergedFeatureModel.getFeatures(), date)) {
//				if(DwEvolutionUtil.isValid(feature, date)) {
//					List<DwGroupComposition> groupMemberships = DwEvolutionUtil.getValidTemporalElements(feature.getGroupMembership(), date) ;
//					
//					if(groupMemberships == null || groupMemberships.size() == 0) {
//						System.err.println("Feature "+DwEvolutionUtil.getValidTemporalElement(feature.getNames(), date).getName()+ " has no group membership at date "+date);
//					}
//					else if(groupMemberships.size() > 1) {
//						System.err.println("Feature "+DwEvolutionUtil.getValidTemporalElement(feature.getNames(), date).getName()+ " has multiple group membership at date "+date);
//					}
//				}
//				List<DwFeatureChild> validFeatureChildren = DwEvolutionUtil.getValidTemporalElements(feature.getParentOf(), date);
//				if(validFeatureChildren != null && validFeatureChildren.size()>1) {
//					System.err.println("Feature "+DwEvolutionUtil.getValidTemporalElement(feature.getNames(), date).getName()+" has more than one child group at date "+date);
//				}
//			}
//			
//			for(DwGroup group: mergedFeatureModel.getGroups()) {
//				if(DwEvolutionUtil.isValid(group, date)) {
//					List<DwFeatureChild> featureChildrenParent = DwEvolutionUtil.getValidTemporalElements(group.getChildOf(), date);
//					List<DwGroupComposition> groupCompositions = DwEvolutionUtil.getValidTemporalElements(group.getParentOf(), date);
//					
//					if(featureChildrenParent == null || featureChildrenParent.size() == 0) {
//						System.err.println("Group has no group parent feature at date "+date);
//					}
//					else if(featureChildrenParent.size() > 1) {
//						System.err.println("Group has multiple group parent feature at date "+date);
//					}
//					
//					if(groupCompositions == null || groupCompositions.size() == 0) {
//						System.err.println("Group has no composition at date "+date);
//					}
//					else if(groupCompositions.size() > 1) {
//						System.err.println("Group has multiple compositions at date "+date);
//					}
//				}
//			}
//		}
//		
//		
//		for(DwGroup group: mergedFeatureModel.getGroups()) {
//			for(DwGroupComposition groupComposition: group.getParentOf()) {
//				if(!mergedFeatureModel.getFeatures().containsAll(groupComposition.getFeatures())) {
//					System.err.println("Non consistent!");
//					return;
//				}
//			}
//		}
	}
	
}
