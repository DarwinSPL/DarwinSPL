package de.darwinspl.common.ecore.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import de.christophseidl.util.ecore.EcoreIOUtil;
import de.christophseidl.util.ecore.EcoreResolverUtil;
import de.darwinspl.common.eclipse.util.DwResourceUtil;

public class DwEcoreUtil {

	
	public static EObject loadAccompanyingModelInSameProject(EObject elementInOriginalResource, String... extensions) {
		if(elementInOriginalResource == null || extensions == null || extensions.length < 1) {
			return null;
		}
		
		IFile sourceFile = EcoreResolverUtil.resolveRelativeFileFromEObject(elementInOriginalResource);
		
		IFile modelFile = DwResourceUtil.findFileWithExtensionInSameProject(sourceFile, extensions);
		
		if(modelFile != null) {
			return EcoreIOUtil.loadModel(modelFile, elementInOriginalResource.eResource().getResourceSet());
		}
		
		return null;
	}
	
	
	
	private static Map<Resource, EObject> cache = new HashMap<>();
	
	public static EObject resolveFeatureModelReference(String identifier, EObject container) {
		Resource containerResource = container.eResource();
		if(cache.get(containerResource) != null)
			return cache.get(containerResource);
		
		IFile containingIFile = EcoreIOUtil.getFile(containerResource);
		
		IResource referencedFile = containingIFile.getParent().findMember(identifier.substring(1, identifier.length()-1));
		if(referencedFile == null || !referencedFile.exists()) {
			throw new RuntimeException("Referenced feature model could not be found: No such file under the given path");
		}
		
		ResourceSet resSet = container.eResource().getResourceSet();
		
		String uri = "platform:/resource/" + containingIFile.getProject().getName() + "/" + referencedFile.getProjectRelativePath();
	    Resource resource = resSet.getResource(URI.createURI(uri), true);
	    
	    try {
			resource.load(Collections.EMPTY_MAP);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Referenced feature model could not be found: Loading of EMF model failed");
		}
	    
	    EObject result = resource.getContents().get(0);
	    cache.put(containerResource, result);
	    return result;
	}
	


	public static String deresolveFeatureModelReference(EObject referencedElement, EObject container) {
		String error = "<referenced_model.tfm>";
		
		Resource referencedResource = referencedElement.eResource();
		Resource containerResource = container.eResource();

		if(referencedResource == null || containerResource == null) {
			return error;
		}
		
		List<String> referencedResourceSegments = new ArrayList<>(referencedResource.getURI().segmentsList());
		List<String> containerResourceSegments = new ArrayList<>(containerResource.getURI().segmentsList());
		
		int commonSegments;
		for(commonSegments=0; commonSegments<referencedResourceSegments.size(); commonSegments++) {
			if(commonSegments == containerResourceSegments.size())
				break;
			if(!referencedResourceSegments.get(commonSegments).equals(containerResourceSegments.get(commonSegments)))
				break;
		}
		
		for(int i=0; i<commonSegments; i++) {
			referencedResourceSegments.remove(0);
			containerResourceSegments.remove(0);
		}
		
		// TODO references across different folders need to be revisited, not even considered here!
		if(containerResourceSegments.size() == 1 && referencedResourceSegments.size() == 1) {
			return "<" + referencedResourceSegments.get(0) + ">";
		}
		
		return error;
	}
		
}

























