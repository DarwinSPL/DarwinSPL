/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.analysis;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

import de.darwinspl.expression.util.ExpressionResolver;
import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.util.custom.DwFeatureResolverUtil;
import de.darwinspl.temporal.DwSingleTemporalIntervalElement;

public class DwFeatureReferenceExpressionFeatureReferenceResolver implements
		de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceResolver<de.darwinspl.expression.DwFeatureReferenceExpression, de.darwinspl.feature.DwFeature> {

	public void resolve(String identifier, de.darwinspl.expression.DwFeatureReferenceExpression container,
			EReference reference, int position, boolean resolveFuzzy,
			final de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceResolveResult<de.darwinspl.feature.DwFeature> result) {
		DwFeature feature = ExpressionResolver.resolveFeature(identifier, container);

		if (feature != null) {
			result.addMapping(identifier, feature);
		}
	}

	public String deResolve(de.darwinspl.feature.DwFeature element,
			de.darwinspl.expression.DwFeatureReferenceExpression container, EReference reference) {
		EObject eContainer = container.eContainer();
		while(! (eContainer instanceof DwSingleTemporalIntervalElement)) {
			eContainer = eContainer.eContainer();
			if(eContainer == null) {
				break;
			}
		}
		
		if(eContainer == null) {
			return "Could not find containing constraint for timestamp";
		}
		
		DwSingleTemporalIntervalElement constraint = (DwSingleTemporalIntervalElement) eContainer;
		
		return DwFeatureResolverUtil.deresolveFeature(element, constraint.getFrom());
	}

	public void setOptions(Map<?, ?> options) {
		// save options in a field or leave method empty if this resolver does not
		// depend
		// on any option
	}

}
