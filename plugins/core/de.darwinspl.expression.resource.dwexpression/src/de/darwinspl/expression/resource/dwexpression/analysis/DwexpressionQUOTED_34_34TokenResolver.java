/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.analysis;

import java.util.Map;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

public class DwexpressionQUOTED_34_34TokenResolver implements de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolver {
	
	private de.darwinspl.expression.resource.dwexpression.analysis.DwexpressionDefaultTokenResolver defaultTokenResolver = new de.darwinspl.expression.resource.dwexpression.analysis.DwexpressionDefaultTokenResolver(true);
	
	public String deResolve(Object value, EStructuralFeature feature, EObject container) {
		// By default token de-resolving is delegated to the DefaultTokenResolver.
		String result = defaultTokenResolver.deResolve(value, feature, container, "\"", "\"", null);
		return result;
	}
	
	public void resolve(String lexem, EStructuralFeature feature, de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolveResult result) {
		// By default token resolving is delegated to the DefaultTokenResolver.
		defaultTokenResolver.resolve(lexem, feature, result, "\"", "\"", null);
	}
	
	public void setOptions(Map<?,?> options) {
		defaultTokenResolver.setOptions(options);
	}
	
}
