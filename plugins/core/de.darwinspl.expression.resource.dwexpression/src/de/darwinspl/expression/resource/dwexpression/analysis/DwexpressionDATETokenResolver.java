/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.analysis;

import java.util.Date;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import de.darwinspl.temporal.util.DwDateResolverUtil;

public class DwexpressionDATETokenResolver implements de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolver {
	
	public String deResolve(Object value, EStructuralFeature feature, EObject container) {
		if(value instanceof Date) {
			Date date = (Date) value;
			return DwDateResolverUtil.deresolveDateAlternative(date);
		}
		return null;
	}
	
	public void resolve(String lexem, EStructuralFeature feature, de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolveResult result) {
		result.setResolvedToken(DwDateResolverUtil.resolveDateAlternative(lexem));
	}
	
	public void setOptions(Map<?,?> options) {
	}
	
}
