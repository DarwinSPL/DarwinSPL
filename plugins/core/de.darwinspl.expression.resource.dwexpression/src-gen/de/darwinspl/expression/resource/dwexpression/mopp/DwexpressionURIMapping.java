/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.mopp;

import org.eclipse.emf.common.util.URI;

/**
 * <p>
 * A basic implementation of the
 * de.darwinspl.expression.resource.dwexpression.IDwexpressionURIMapping interface
 * that can map identifiers to URIs.
 * </p>
 * 
 * @param <ReferenceType> unused type parameter which is needed to implement
 * de.darwinspl.expression.resource.dwexpression.IDwexpressionURIMapping.
 */
public class DwexpressionURIMapping<ReferenceType> implements de.darwinspl.expression.resource.dwexpression.IDwexpressionURIMapping<ReferenceType> {
	
	private URI uri;
	private String identifier;
	private String warning;
	
	public DwexpressionURIMapping(String identifier, URI newIdentifier, String warning) {
		super();
		this.uri = newIdentifier;
		this.identifier = identifier;
		this.warning = warning;
	}
	
	public URI getTargetIdentifier() {
		return uri;
	}
	
	public String getIdentifier() {
		return identifier;
	}
	
	public String getWarning() {
		return warning;
	}
	
}
