/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression;


/**
 * Implementors of this interface can provide a post-processor for text resources.
 */
public interface IDwexpressionResourcePostProcessorProvider {
	
	/**
	 * Returns the processor that shall be called after text resource are successfully
	 * parsed.
	 */
	public de.darwinspl.expression.resource.dwexpression.IDwexpressionResourcePostProcessor getResourcePostProcessor();
	
}
