/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.mopp;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

/**
 * <p>
 * A factory for ContextDependentURIFragments. Given a feasible reference
 * resolver, this factory returns a matching fragment that used the resolver to
 * resolver proxy objects.
 * </p>
 * 
 * @param <ContainerType> the type of the class containing the reference to be
 * resolved
 * @param <ReferenceType> the type of the reference to be resolved
 */
public class DwexpressionContextDependentURIFragmentFactory<ContainerType extends EObject, ReferenceType extends EObject>  implements de.darwinspl.expression.resource.dwexpression.IDwexpressionContextDependentURIFragmentFactory<ContainerType, ReferenceType> {
	
	private final de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceResolver<ContainerType, ReferenceType> resolver;
	
	public DwexpressionContextDependentURIFragmentFactory(de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceResolver<ContainerType, ReferenceType> resolver) {
		this.resolver = resolver;
	}
	
	public de.darwinspl.expression.resource.dwexpression.IDwexpressionContextDependentURIFragment<?> create(String identifier, ContainerType container, EReference reference, int positionInReference, EObject proxy) {
		
		return new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionContextDependentURIFragment<ContainerType, ReferenceType>(identifier, container, reference, positionInReference, proxy) {
			public de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceResolver<ContainerType, ReferenceType> getResolver() {
				return resolver;
			}
		};
	}
}
