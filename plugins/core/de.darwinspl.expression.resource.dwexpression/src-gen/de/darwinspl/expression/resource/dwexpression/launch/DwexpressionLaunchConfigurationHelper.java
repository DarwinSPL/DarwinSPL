/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.launch;


/**
 * A class that provides common methods that are required by launch configuration
 * delegates.
 */
public class DwexpressionLaunchConfigurationHelper {
	// The generator for this class is currently disabled by option
	// 'disableLaunchSupport' in the .cs file.
}
