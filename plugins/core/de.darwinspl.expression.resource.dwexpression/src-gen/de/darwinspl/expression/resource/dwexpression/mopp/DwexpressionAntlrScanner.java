/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.mopp;

import org.antlr.runtime3_4_0.ANTLRStringStream;
import org.antlr.runtime3_4_0.Lexer;
import org.antlr.runtime3_4_0.Token;

public class DwexpressionAntlrScanner implements de.darwinspl.expression.resource.dwexpression.IDwexpressionTextScanner {
	
	private Lexer antlrLexer;
	
	public DwexpressionAntlrScanner(Lexer antlrLexer) {
		this.antlrLexer = antlrLexer;
	}
	
	public de.darwinspl.expression.resource.dwexpression.IDwexpressionTextToken getNextToken() {
		if (antlrLexer.getCharStream() == null) {
			return null;
		}
		final Token current = antlrLexer.nextToken();
		if (current == null || current.getType() < 0) {
			return null;
		}
		de.darwinspl.expression.resource.dwexpression.IDwexpressionTextToken result = new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionANTLRTextToken(current);
		return result;
	}
	
	public void setText(String text) {
		antlrLexer.setCharStream(new ANTLRStringStream(text));
	}
	
}
