/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.mopp;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;

public class DwexpressionResourceFactory implements Resource.Factory {
	
	public DwexpressionResourceFactory() {
		super();
	}
	
	public Resource createResource(URI uri) {
		return new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionResource(uri);
	}
	
}
