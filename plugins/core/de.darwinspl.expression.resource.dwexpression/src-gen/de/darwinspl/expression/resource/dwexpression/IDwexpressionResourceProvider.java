/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression;


/**
 * Implementors of this interface provide an EMF resource.
 */
public interface IDwexpressionResourceProvider {
	
	/**
	 * Returns the resource.
	 */
	public de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource getResource();
	
}
