/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.grammar;


public class DwexpressionWhiteSpace extends de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionFormattingElement {
	
	private final int amount;
	
	public DwexpressionWhiteSpace(int amount, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality cardinality) {
		super(cardinality);
		this.amount = amount;
	}
	
	public int getAmount() {
		return amount;
	}
	
	public String toString() {
		return "#" + getAmount();
	}
	
}
