/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.mopp;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used when computing code completion proposals hold groups of
 * expected elements which belong to the same follow set.
 */
public class DwexpressionFollowSetGroupList {
	
	private int lastFollowSetID = -1;
	private List<de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionFollowSetGroup> followSetGroups = new ArrayList<de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionFollowSetGroup>();
	
	public DwexpressionFollowSetGroupList(List<de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal> expectedTerminals) {
		for (de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal expectedTerminal : expectedTerminals) {
			addExpectedTerminal(expectedTerminal);
		}
	}
	
	private void addExpectedTerminal(de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal expectedTerminal) {
		de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionFollowSetGroup group;
		
		int followSetID = expectedTerminal.getFollowSetID();
		if (followSetID == lastFollowSetID) {
			if (followSetGroups.isEmpty()) {
				group = addNewGroup();
			} else {
				group = followSetGroups.get(followSetGroups.size() - 1);
			}
		} else {
			group = addNewGroup();
			lastFollowSetID = followSetID;
		}
		
		group.add(expectedTerminal);
	}
	
	public List<de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionFollowSetGroup> getFollowSetGroups() {
		return followSetGroups;
	}
	
	private de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionFollowSetGroup addNewGroup() {
		de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionFollowSetGroup group = new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionFollowSetGroup();
		followSetGroups.add(group);
		return group;
	}
	
}
