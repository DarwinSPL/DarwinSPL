/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression;


public enum DwexpressionEProblemSeverity {
	WARNING, ERROR;
}
