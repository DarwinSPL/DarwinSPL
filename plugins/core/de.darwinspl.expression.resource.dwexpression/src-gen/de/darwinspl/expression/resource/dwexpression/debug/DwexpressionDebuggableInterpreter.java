/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.debug;


/**
 * A DebuggableInterpreter is a facade for interpreters that adds debug support.
 */
public class DwexpressionDebuggableInterpreter {
	// The generator for this class is currently disabled by option
	// 'disableDebugSupport' in the .cs file.
}
