grammar Dwexpression;

options {
	superClass = DwexpressionANTLRParserBase;
	backtrack = true;
	memoize = true;
}

@lexer::header {
	package de.darwinspl.expression.resource.dwexpression.mopp;
	
	import java.util.ArrayList;
import java.util.List;
import org.antlr.runtime3_4_0.ANTLRStringStream;
import org.antlr.runtime3_4_0.RecognitionException;
}

@lexer::members {
	public List<RecognitionException> lexerExceptions  = new ArrayList<RecognitionException>();
	public List<Integer> lexerExceptionPositions = new ArrayList<Integer>();
	
	public void reportError(RecognitionException e) {
		lexerExceptions.add(e);
		lexerExceptionPositions.add(((ANTLRStringStream) input).index());
	}
}
@header{
	package de.darwinspl.expression.resource.dwexpression.mopp;
	
	import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.antlr.runtime3_4_0.ANTLRInputStream;
import org.antlr.runtime3_4_0.BitSet;
import org.antlr.runtime3_4_0.CommonToken;
import org.antlr.runtime3_4_0.CommonTokenStream;
import org.antlr.runtime3_4_0.IntStream;
import org.antlr.runtime3_4_0.Lexer;
import org.antlr.runtime3_4_0.RecognitionException;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
}

@members{
	private de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolverFactory tokenResolverFactory = new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTokenResolverFactory();
	
	/**
	 * the index of the last token that was handled by collectHiddenTokens()
	 */
	private int lastPosition;
	
	/**
	 * A flag that indicates whether the parser should remember all expected elements.
	 * This flag is set to true when using the parse for code completion. Otherwise it
	 * is set to false.
	 */
	private boolean rememberExpectedElements = false;
	
	private Object parseToIndexTypeObject;
	private int lastTokenIndex = 0;
	
	/**
	 * A list of expected elements the were collected while parsing the input stream.
	 * This list is only filled if <code>rememberExpectedElements</code> is set to
	 * true.
	 */
	private List<de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal> expectedElements = new ArrayList<de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal>();
	
	private int mismatchedTokenRecoveryTries = 0;
	/**
	 * A helper list to allow a lexer to pass errors to its parser
	 */
	protected List<RecognitionException> lexerExceptions = Collections.synchronizedList(new ArrayList<RecognitionException>());
	
	/**
	 * Another helper list to allow a lexer to pass positions of errors to its parser
	 */
	protected List<Integer> lexerExceptionPositions = Collections.synchronizedList(new ArrayList<Integer>());
	
	/**
	 * A stack for incomplete objects. This stack is used filled when the parser is
	 * used for code completion. Whenever the parser starts to read an object it is
	 * pushed on the stack. Once the element was parser completely it is popped from
	 * the stack.
	 */
	List<EObject> incompleteObjects = new ArrayList<EObject>();
	
	private int stopIncludingHiddenTokens;
	private int stopExcludingHiddenTokens;
	private int tokenIndexOfLastCompleteElement;
	
	private int expectedElementsIndexOfLastCompleteElement;
	
	/**
	 * The offset indicating the cursor position when the parser is used for code
	 * completion by calling parseToExpectedElements().
	 */
	private int cursorOffset;
	
	/**
	 * The offset of the first hidden token of the last expected element. This offset
	 * is used to discard expected elements, which are not needed for code completion.
	 */
	private int lastStartIncludingHidden;
	
	private de.darwinspl.expression.resource.dwexpression.IDwexpressionLocationMap locationMap;
	
	private de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionSyntaxErrorMessageConverter syntaxErrorMessageConverter = new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionSyntaxErrorMessageConverter(tokenNames);
	
	@Override
	public void reportError(RecognitionException re) {
		addErrorToResource(syntaxErrorMessageConverter.translateParseError(re));
	}
	
	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
		postParseCommands.add(new de.darwinspl.expression.resource.dwexpression.IDwexpressionCommand<de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource>() {
			public boolean execute(de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource resource) {
				if (resource == null) {
					// the resource can be null if the parser is used for code completion
					return true;
				}
				resource.addProblem(new de.darwinspl.expression.resource.dwexpression.IDwexpressionProblem() {
					public de.darwinspl.expression.resource.dwexpression.DwexpressionEProblemSeverity getSeverity() {
						return de.darwinspl.expression.resource.dwexpression.DwexpressionEProblemSeverity.ERROR;
					}
					public de.darwinspl.expression.resource.dwexpression.DwexpressionEProblemType getType() {
						return de.darwinspl.expression.resource.dwexpression.DwexpressionEProblemType.SYNTAX_ERROR;
					}
					public String getMessage() {
						return errorMessage;
					}
					public Collection<de.darwinspl.expression.resource.dwexpression.IDwexpressionQuickFix> getQuickFixes() {
						return null;
					}
				}, column, line, startIndex, stopIndex);
				return true;
			}
		});
	}
	
	protected void addErrorToResource(de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionLocalizedMessage message) {
		if (message == null) {
			return;
		}
		addErrorToResource(message.getMessage(), message.getColumn(), message.getLine(), message.getCharStart(), message.getCharEnd());
	}
	
	public void addExpectedElement(EClass eClass, int expectationStartIndex, int expectationEndIndex) {
		for (int expectationIndex = expectationStartIndex; expectationIndex <= expectationEndIndex; expectationIndex++) {
			addExpectedElement(eClass, de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectationConstants.EXPECTATIONS[expectationIndex]);
		}
	}
	
	public void addExpectedElement(EClass eClass, int expectationIndex) {
		addExpectedElement(eClass, de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectationConstants.EXPECTATIONS[expectationIndex]);
	}
	
	public void addExpectedElement(EClass eClass, int[] ids) {
		if (!this.rememberExpectedElements) {
			return;
		}
		int terminalID = ids[0];
		int followSetID = ids[1];
		de.darwinspl.expression.resource.dwexpression.IDwexpressionExpectedElement terminal = de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionFollowSetProvider.TERMINALS[terminalID];
		de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionContainedFeature[] containmentFeatures = new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionContainedFeature[ids.length - 2];
		for (int i = 2; i < ids.length; i++) {
			containmentFeatures[i - 2] = de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionFollowSetProvider.LINKS[ids[i]];
		}
		de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainmentTrace containmentTrace = new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainmentTrace(eClass, containmentFeatures);
		EObject container = getLastIncompleteElement();
		de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal expectedElement = new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal(container, terminal, followSetID, containmentTrace);
		setPosition(expectedElement, input.index());
		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
		lastStartIncludingHidden = startIncludingHiddenTokens;
		this.expectedElements.add(expectedElement);
	}
	
	protected void collectHiddenTokens(EObject element) {
	}
	
	protected void copyLocalizationInfos(final EObject source, final EObject target) {
		if (disableLocationMap) {
			return;
		}
		final de.darwinspl.expression.resource.dwexpression.IDwexpressionLocationMap locationMap = this.locationMap;
		if (locationMap == null) {
			// the locationMap can be null if the parser is used for code completion
			return;
		}
		postParseCommands.add(new de.darwinspl.expression.resource.dwexpression.IDwexpressionCommand<de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource>() {
			public boolean execute(de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource resource) {
				locationMap.setCharStart(target, locationMap.getCharStart(source));
				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
				locationMap.setColumn(target, locationMap.getColumn(source));
				locationMap.setLine(target, locationMap.getLine(source));
				return true;
			}
		});
	}
	
	protected void copyLocalizationInfos(final CommonToken source, final EObject target) {
		if (disableLocationMap) {
			return;
		}
		final de.darwinspl.expression.resource.dwexpression.IDwexpressionLocationMap locationMap = this.locationMap;
		if (locationMap == null) {
			// the locationMap can be null if the parser is used for code completion
			return;
		}
		postParseCommands.add(new de.darwinspl.expression.resource.dwexpression.IDwexpressionCommand<de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource>() {
			public boolean execute(de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource resource) {
				if (source == null) {
					return true;
				}
				locationMap.setCharStart(target, source.getStartIndex());
				locationMap.setCharEnd(target, source.getStopIndex());
				locationMap.setColumn(target, source.getCharPositionInLine());
				locationMap.setLine(target, source.getLine());
				return true;
			}
		});
	}
	
	/**
	 * Sets the end character index and the last line for the given object in the
	 * location map.
	 */
	protected void setLocalizationEnd(Collection<de.darwinspl.expression.resource.dwexpression.IDwexpressionCommand<de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource>> postParseCommands , final EObject object, final int endChar, final int endLine) {
		if (disableLocationMap) {
			return;
		}
		final de.darwinspl.expression.resource.dwexpression.IDwexpressionLocationMap locationMap = this.locationMap;
		if (locationMap == null) {
			// the locationMap can be null if the parser is used for code completion
			return;
		}
		postParseCommands.add(new de.darwinspl.expression.resource.dwexpression.IDwexpressionCommand<de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource>() {
			public boolean execute(de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource resource) {
				locationMap.setCharEnd(object, endChar);
				locationMap.setLine(object, endLine);
				return true;
			}
		});
	}
	
	public de.darwinspl.expression.resource.dwexpression.IDwexpressionTextParser createInstance(InputStream actualInputStream, String encoding) {
		try {
			if (encoding == null) {
				return new DwexpressionParser(new CommonTokenStream(new DwexpressionLexer(new ANTLRInputStream(actualInputStream))));
			} else {
				return new DwexpressionParser(new CommonTokenStream(new DwexpressionLexer(new ANTLRInputStream(actualInputStream, encoding))));
			}
		} catch (IOException e) {
			new de.darwinspl.expression.resource.dwexpression.util.DwexpressionRuntimeUtil().logError("Error while creating parser.", e);
			return null;
		}
	}
	
	/**
	 * This default constructor is only used to call createInstance() on it.
	 */
	public DwexpressionParser() {
		super(null);
	}
	
	protected EObject doParse() throws RecognitionException {
		this.lastPosition = 0;
		// required because the lexer class can not be subclassed
		((DwexpressionLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
		((DwexpressionLexer) getTokenStream().getTokenSource()).lexerExceptionPositions = lexerExceptionPositions;
		Object typeObject = getTypeObject();
		if (typeObject == null) {
			return start();
		} else if (typeObject instanceof EClass) {
		}
		throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionUnexpectedContentTypeException(typeObject);
	}
	
	public int getMismatchedTokenRecoveryTries() {
		return mismatchedTokenRecoveryTries;
	}
	
	public Object getMissingSymbol(IntStream arg0, RecognitionException arg1, int arg2, BitSet arg3) {
		mismatchedTokenRecoveryTries++;
		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
	}
	
	public Object getParseToIndexTypeObject() {
		return parseToIndexTypeObject;
	}
	
	protected Object getTypeObject() {
		Object typeObject = getParseToIndexTypeObject();
		if (typeObject != null) {
			return typeObject;
		}
		Map<?,?> options = getOptions();
		if (options != null) {
			typeObject = options.get(de.darwinspl.expression.resource.dwexpression.IDwexpressionOptions.RESOURCE_CONTENT_TYPE);
		}
		return typeObject;
	}
	
	/**
	 * Implementation that calls {@link #doParse()} and handles the thrown
	 * RecognitionExceptions.
	 */
	public de.darwinspl.expression.resource.dwexpression.IDwexpressionParseResult parse() {
		// Reset parser state
		terminateParsing = false;
		postParseCommands = new ArrayList<de.darwinspl.expression.resource.dwexpression.IDwexpressionCommand<de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource>>();
		de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionParseResult parseResult = new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionParseResult();
		if (disableLocationMap) {
			locationMap = new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionDevNullLocationMap();
		} else {
			locationMap = new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionLocationMap();
		}
		// Run parser
		try {
			EObject result =  doParse();
			if (lexerExceptions.isEmpty()) {
				parseResult.setRoot(result);
				parseResult.setLocationMap(locationMap);
			}
		} catch (RecognitionException re) {
			addErrorToResource(syntaxErrorMessageConverter.translateParseError(re));
		} catch (IllegalArgumentException iae) {
			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
				// can be caused if a null is set on EMF models where not allowed. this will just
				// happen if other errors occurred before
			} else {
				iae.printStackTrace();
			}
		}
		for (RecognitionException re : lexerExceptions) {
			addErrorToResource(syntaxErrorMessageConverter.translateLexicalError(re, lexerExceptions, lexerExceptionPositions));
		}
		parseResult.getPostParseCommands().addAll(postParseCommands);
		return parseResult;
	}
	
	public List<de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal> parseToExpectedElements(EClass type, de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource dummyResource, int cursorOffset) {
		this.rememberExpectedElements = true;
		this.parseToIndexTypeObject = type;
		this.cursorOffset = cursorOffset;
		this.lastStartIncludingHidden = -1;
		final CommonTokenStream tokenStream = (CommonTokenStream) getTokenStream();
		de.darwinspl.expression.resource.dwexpression.IDwexpressionParseResult result = parse();
		for (EObject incompleteObject : incompleteObjects) {
			Lexer lexer = (Lexer) tokenStream.getTokenSource();
			int endChar = lexer.getCharIndex();
			int endLine = lexer.getLine();
			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
		}
		if (result != null) {
			EObject root = result.getRoot();
			if (root != null) {
				dummyResource.getContentsInternal().add(root);
			}
			for (de.darwinspl.expression.resource.dwexpression.IDwexpressionCommand<de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource> command : result.getPostParseCommands()) {
				command.execute(dummyResource);
			}
		}
		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
		Set<de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal> currentFollowSet = new LinkedHashSet<de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal>();
		List<de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal> newFollowSet = new ArrayList<de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal>();
		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
			de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal expectedElementI = expectedElements.get(i);
			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
				currentFollowSet.add(expectedElementI);
			} else {
				break;
			}
		}
		int followSetID = 13;
		int i;
		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
			CommonToken nextToken = (CommonToken) tokenStream.get(i);
			if (nextToken.getType() < 0) {
				break;
			}
			if (nextToken.getChannel() == 99) {
				// hidden tokens do not reduce the follow set
			} else {
				// now that we have found the next visible token the position for that expected
				// terminals can be set
				for (de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal nextFollow : newFollowSet) {
					lastTokenIndex = 0;
					setPosition(nextFollow, i);
				}
				newFollowSet.clear();
				// normal tokens do reduce the follow set - only elements that match the token are
				// kept
				for (de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal nextFollow : currentFollowSet) {
					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
						// keep this one - it matches
						Collection<de.darwinspl.expression.resource.dwexpression.util.DwexpressionPair<de.darwinspl.expression.resource.dwexpression.IDwexpressionExpectedElement, de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionContainedFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
						for (de.darwinspl.expression.resource.dwexpression.util.DwexpressionPair<de.darwinspl.expression.resource.dwexpression.IDwexpressionExpectedElement, de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionContainedFeature[]> newFollowerPair : newFollowers) {
							de.darwinspl.expression.resource.dwexpression.IDwexpressionExpectedElement newFollower = newFollowerPair.getLeft();
							EObject container = getLastIncompleteElement();
							de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainmentTrace containmentTrace = new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainmentTrace(null, newFollowerPair.getRight());
							de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal newFollowTerminal = new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal(container, newFollower, followSetID, containmentTrace);
							newFollowSet.add(newFollowTerminal);
							expectedElements.add(newFollowTerminal);
						}
					}
				}
				currentFollowSet.clear();
				currentFollowSet.addAll(newFollowSet);
			}
			followSetID++;
		}
		// after the last token in the stream we must set the position for the elements
		// that were added during the last iteration of the loop
		for (de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal nextFollow : newFollowSet) {
			lastTokenIndex = 0;
			setPosition(nextFollow, i);
		}
		return this.expectedElements;
	}
	
	public void setPosition(de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal expectedElement, int tokenIndex) {
		int currentIndex = Math.max(0, tokenIndex);
		for (int index = lastTokenIndex; index < currentIndex; index++) {
			if (index >= input.size()) {
				break;
			}
			CommonToken tokenAtIndex = (CommonToken) input.get(index);
			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			if (tokenAtIndex.getChannel() != 99 && !anonymousTokens.contains(tokenAtIndex)) {
				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			}
		}
		lastTokenIndex = Math.max(0, currentIndex);
		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
	}
	
	public Object recoverFromMismatchedToken(IntStream input, int ttype, BitSet follow) throws RecognitionException {
		if (!rememberExpectedElements) {
			return super.recoverFromMismatchedToken(input, ttype, follow);
		} else {
			return null;
		}
	}
	
	private void startIncompleteElement(Object object) {
		if (object instanceof EObject) {
			this.incompleteObjects.add((EObject) object);
		}
	}
	
	private void completedElement(Object object, boolean isContainment) {
		if (isContainment && !this.incompleteObjects.isEmpty()) {
			this.incompleteObjects.remove(object);
		}
		if (object instanceof EObject) {
			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
		}
	}
	
	private EObject getLastIncompleteElement() {
		if (incompleteObjects.isEmpty()) {
			return null;
		}
		return incompleteObjects.get(incompleteObjects.size() - 1);
	}
	
}

start returns [ EObject element = null]
:
	{
		// follow set for start rule(s)
		addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwEquivalenceExpression(), 0, 324);
		addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwImpliesExpression(), 325, 649);
		addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwOrExpression(), 650, 974);
		addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwAndExpression(), 975, 1299);
		addExpectedElement(null, 1300, 1304);
		expectedElementsIndexOfLastCompleteElement = 1304;
	}
	(
		c0 = parse_de_darwinspl_expression_DwExpression{ element = c0; }
	)
	EOF	{
		retrieveLayoutInformation(element, null, null, false);
	}
	
;

parseop_DwExpression_level_0 returns [de.darwinspl.expression.DwExpression element = null]
@init{
}
:
	leftArg = parseop_DwExpression_level_1	((
		()
		{ element = null; }
		a0 = '<->' {
			if (element == null) {
				element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwEquivalenceExpression();
				startIncompleteElement(element);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_0_0_0_1, null, true);
			copyLocalizationInfos((CommonToken)a0, element);
		}
		{
			// expected elements (follow set)
			addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwEquivalenceExpression(), 1305, 1629);
		}
		
		rightArg = parseop_DwExpression_level_1		{
			if (terminateParsing) {
				throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwEquivalenceExpression();
				startIncompleteElement(element);
			}
			if (leftArg != null) {
				if (leftArg != null) {
					Object value = leftArg;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_EQUIVALENCE_EXPRESSION__OPERAND1), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_0_0_0_0, leftArg, true);
				copyLocalizationInfos(leftArg, element);
			}
		}
		{
			if (terminateParsing) {
				throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwEquivalenceExpression();
				startIncompleteElement(element);
			}
			if (rightArg != null) {
				if (rightArg != null) {
					Object value = rightArg;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_EQUIVALENCE_EXPRESSION__OPERAND2), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_0_0_0_2, rightArg, true);
				copyLocalizationInfos(rightArg, element);
			}
		}
		{ leftArg = element; /* this may become an argument in the next iteration */ }
	)+ | /* epsilon */ { element = leftArg; }
	
)
;

parseop_DwExpression_level_1 returns [de.darwinspl.expression.DwExpression element = null]
@init{
}
:
leftArg = parseop_DwExpression_level_2((
	()
	{ element = null; }
	a0 = '->' {
		if (element == null) {
			element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwImpliesExpression();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_1_0_0_1, null, true);
		copyLocalizationInfos((CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwImpliesExpression(), 1630, 1954);
	}
	
	rightArg = parseop_DwExpression_level_2	{
		if (terminateParsing) {
			throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTerminateParsingException();
		}
		if (element == null) {
			element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwImpliesExpression();
			startIncompleteElement(element);
		}
		if (leftArg != null) {
			if (leftArg != null) {
				Object value = leftArg;
				element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_IMPLIES_EXPRESSION__OPERAND1), value);
				completedElement(value, true);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_1_0_0_0, leftArg, true);
			copyLocalizationInfos(leftArg, element);
		}
	}
	{
		if (terminateParsing) {
			throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTerminateParsingException();
		}
		if (element == null) {
			element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwImpliesExpression();
			startIncompleteElement(element);
		}
		if (rightArg != null) {
			if (rightArg != null) {
				Object value = rightArg;
				element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_IMPLIES_EXPRESSION__OPERAND2), value);
				completedElement(value, true);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_1_0_0_2, rightArg, true);
			copyLocalizationInfos(rightArg, element);
		}
	}
	{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_DwExpression_level_2 returns [de.darwinspl.expression.DwExpression element = null]
@init{
}
:
leftArg = parseop_DwExpression_level_3((
()
{ element = null; }
a0 = '||' {
	if (element == null) {
		element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwOrExpression();
		startIncompleteElement(element);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_2_0_0_1, null, true);
	copyLocalizationInfos((CommonToken)a0, element);
}
{
	// expected elements (follow set)
	addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwOrExpression(), 1955, 2279);
}

rightArg = parseop_DwExpression_level_3{
	if (terminateParsing) {
		throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTerminateParsingException();
	}
	if (element == null) {
		element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwOrExpression();
		startIncompleteElement(element);
	}
	if (leftArg != null) {
		if (leftArg != null) {
			Object value = leftArg;
			element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_OR_EXPRESSION__OPERAND1), value);
			completedElement(value, true);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_2_0_0_0, leftArg, true);
		copyLocalizationInfos(leftArg, element);
	}
}
{
	if (terminateParsing) {
		throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTerminateParsingException();
	}
	if (element == null) {
		element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwOrExpression();
		startIncompleteElement(element);
	}
	if (rightArg != null) {
		if (rightArg != null) {
			Object value = rightArg;
			element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_OR_EXPRESSION__OPERAND2), value);
			completedElement(value, true);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_2_0_0_2, rightArg, true);
		copyLocalizationInfos(rightArg, element);
	}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_DwExpression_level_3 returns [de.darwinspl.expression.DwExpression element = null]
@init{
}
:
leftArg = parseop_DwExpression_level_14((
()
{ element = null; }
a0 = '&&' {
if (element == null) {
	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwAndExpression();
	startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_3_0_0_1, null, true);
copyLocalizationInfos((CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwAndExpression(), 2280, 2604);
}

rightArg = parseop_DwExpression_level_14{
if (terminateParsing) {
	throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTerminateParsingException();
}
if (element == null) {
	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwAndExpression();
	startIncompleteElement(element);
}
if (leftArg != null) {
	if (leftArg != null) {
		Object value = leftArg;
		element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_AND_EXPRESSION__OPERAND1), value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_3_0_0_0, leftArg, true);
	copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
	throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTerminateParsingException();
}
if (element == null) {
	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwAndExpression();
	startIncompleteElement(element);
}
if (rightArg != null) {
	if (rightArg != null) {
		Object value = rightArg;
		element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_AND_EXPRESSION__OPERAND2), value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_3_0_0_2, rightArg, true);
	copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_DwExpression_level_14 returns [de.darwinspl.expression.DwExpression element = null]
@init{
}
:
a0 = '!' {
if (element == null) {
element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwNotExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_4_0_0_0, null, true);
copyLocalizationInfos((CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwNotExpression(), 2605, 2929);
}

arg = parseop_DwExpression_level_15{
if (terminateParsing) {
throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTerminateParsingException();
}
if (element == null) {
element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwNotExpression();
startIncompleteElement(element);
}
if (arg != null) {
if (arg != null) {
Object value = arg;
element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_NOT_EXPRESSION__OPERAND), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_4_0_0_1, arg, true);
copyLocalizationInfos(arg, element);
}
}
|

arg = parseop_DwExpression_level_15{ element = arg; }
;

parseop_DwExpression_level_15 returns [de.darwinspl.expression.DwExpression element = null]
@init{
}
:
c0 = parse_de_darwinspl_expression_DwNestedExpression{ element = c0; /* this is a subclass or primitive expression choice */ }
|c1 = parse_de_darwinspl_expression_DwFeatureReferenceExpression{ element = c1; /* this is a subclass or primitive expression choice */ }
|c2 = parse_de_darwinspl_expression_DwBooleanValueExpression{ element = c2; /* this is a subclass or primitive expression choice */ }
;

parse_de_darwinspl_expression_DwNestedExpression returns [de.darwinspl.expression.DwNestedExpression element = null]
@init{
}
:
a0 = '(' {
if (element == null) {
element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwNestedExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_5_0_0_0, null, true);
copyLocalizationInfos((CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwNestedExpression(), 2930, 3254);
}

(
a1_0 = parse_de_darwinspl_expression_DwExpression{
if (terminateParsing) {
throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTerminateParsingException();
}
if (element == null) {
element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwNestedExpression();
startIncompleteElement(element);
}
if (a1_0 != null) {
if (a1_0 != null) {
	Object value = a1_0;
	element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_NESTED_EXPRESSION__OPERAND), value);
	completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_5_0_0_1, a1_0, true);
copyLocalizationInfos(a1_0, element);
}
}
)
{
// expected elements (follow set)
addExpectedElement(null, 3255);
}

a2 = ')' {
if (element == null) {
element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwNestedExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_5_0_0_2, null, true);
copyLocalizationInfos((CommonToken)a2, element);
}
{
// expected elements (follow set)
// We've found the last token for this rule. The constructed EObject is now
// complete.
completedElement(element, true);
addExpectedElement(null, 3256, 3260);
}

;

parse_de_darwinspl_expression_DwFeatureReferenceExpression returns [de.darwinspl.expression.DwFeatureReferenceExpression element = null]
@init{
}
:
(
(
a0 = QUOTED_34_34
{
if (terminateParsing) {
	throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTerminateParsingException();
}
if (element == null) {
	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwFeatureReferenceExpression();
	startIncompleteElement(element);
}
if (a0 != null) {
	de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
	tokenResolver.setOptions(getOptions());
	de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolveResult result = getFreshTokenResolveResult();
	tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), result);
	Object resolvedObject = result.getResolvedToken();
	if (resolvedObject == null) {
		addErrorToResource(result.getErrorMessage(), ((CommonToken) a0).getLine(), ((CommonToken) a0).getCharPositionInLine(), ((CommonToken) a0).getStartIndex(), ((CommonToken) a0).getStopIndex());
	}
	String resolved = (String) resolvedObject;
	de.darwinspl.feature.DwFeature proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwFeature();
	collectHiddenTokens(element);
	registerContextDependentProxy(new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionContextDependentURIFragmentFactory<de.darwinspl.expression.DwFeatureReferenceExpression, de.darwinspl.feature.DwFeature>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getDwFeatureReferenceExpressionFeatureReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), resolved, proxy);
	if (proxy != null) {
		Object value = proxy;
		element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), value);
		completedElement(value, false);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_6_0_0_0_0_0_0, proxy, true);
	copyLocalizationInfos((CommonToken) a0, element);
	copyLocalizationInfos((CommonToken) a0, proxy);
}
}
)
{
// expected elements (follow set)
addExpectedElement(null, 3261, 3265);
}


|(
a1 = IDENTIFIER_TOKEN
{
if (terminateParsing) {
	throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTerminateParsingException();
}
if (element == null) {
	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwFeatureReferenceExpression();
	startIncompleteElement(element);
}
if (a1 != null) {
	de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER_TOKEN");
	tokenResolver.setOptions(getOptions());
	de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolveResult result = getFreshTokenResolveResult();
	tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), result);
	Object resolvedObject = result.getResolvedToken();
	if (resolvedObject == null) {
		addErrorToResource(result.getErrorMessage(), ((CommonToken) a1).getLine(), ((CommonToken) a1).getCharPositionInLine(), ((CommonToken) a1).getStartIndex(), ((CommonToken) a1).getStopIndex());
	}
	String resolved = (String) resolvedObject;
	de.darwinspl.feature.DwFeature proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwFeature();
	collectHiddenTokens(element);
	registerContextDependentProxy(new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionContextDependentURIFragmentFactory<de.darwinspl.expression.DwFeatureReferenceExpression, de.darwinspl.feature.DwFeature>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getDwFeatureReferenceExpressionFeatureReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), resolved, proxy);
	if (proxy != null) {
		Object value = proxy;
		element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), value);
		completedElement(value, false);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_6_0_0_0_0_1_0, proxy, true);
	copyLocalizationInfos((CommonToken) a1, element);
	copyLocalizationInfos((CommonToken) a1, proxy);
}
}
)
{
// expected elements (follow set)
// We've found the last token for this rule. The constructed EObject is now
// complete.
completedElement(element, true);
addExpectedElement(null, 3266, 3270);
}

)
{
// expected elements (follow set)
// We've found the last token for this rule. The constructed EObject is now
// complete.
completedElement(element, true);
addExpectedElement(null, 3271, 3275);
}

;

parse_de_darwinspl_expression_DwBooleanValueExpression returns [de.darwinspl.expression.DwBooleanValueExpression element = null]
@init{
}
:
(
(
a0 = 'true' {
if (element == null) {
	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwBooleanValueExpression();
	startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_7_0_0_0, true, true);
copyLocalizationInfos((CommonToken)a0, element);
// set value of boolean attribute
Object value = true;
element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_BOOLEAN_VALUE_EXPRESSION__VALUE), value);
completedElement(value, false);
}
|a1 = 'false' {
if (element == null) {
	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwBooleanValueExpression();
	startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_7_0_0_0, false, true);
copyLocalizationInfos((CommonToken)a1, element);
// set value of boolean attribute
Object value = false;
element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_BOOLEAN_VALUE_EXPRESSION__VALUE), value);
completedElement(value, false);
}
)
)
{
// expected elements (follow set)
// We've found the last token for this rule. The constructed EObject is now
// complete.
completedElement(element, true);
addExpectedElement(null, 3276, 3280);
}

;

parse_de_darwinspl_expression_DwExpression returns [de.darwinspl.expression.DwExpression element = null]
:
c = parseop_DwExpression_level_0{ element = c; /* this rule is an expression root */ }

;

DATE:
((('0'..'9')+ '/'('0'..'9')+ '/'('0'..'9')+ ( 'T'('0'..'9')+ ':'('0'..'9')+ (':' ('0'..'9')+ )?)?))
{ _channel = 99; }
;
INTEGER_LITERAL:
(('0'..'9')+ )
{ _channel = 99; }
;
IDENTIFIER_TOKEN:
(('A'..'Z'|'a'..'z'|'_')('A'..'Z'|'a'..'z'|'_'|'0'..'9')*)
;
SL_COMMENT:
('//'(~('\n'|'\r'|'\uffff'))*)
{ _channel = 99; }
;
ML_COMMENT:
('/*'.*'*/')
{ _channel = 99; }
;
LINEBREAK:
(('\r\n'|'\r'|'\n'))
{ _channel = 99; }
;
WHITESPACE:
((' '|'\t'|'\f'))
{ _channel = 99; }
;
QUOTED_34_34:
(('"')(~('"'))*('"'))
;

