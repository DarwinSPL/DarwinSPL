/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.mopp;

import org.eclipse.emf.ecore.EClass;

public class DwexpressionSyntaxCoverageInformationProvider {
	
	public EClass[] getClassesWithSyntax() {
		return new EClass[] {
			de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwEquivalenceExpression(),
			de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwImpliesExpression(),
			de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwOrExpression(),
			de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwAndExpression(),
			de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwNotExpression(),
			de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwNestedExpression(),
			de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwFeatureReferenceExpression(),
			de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwBooleanValueExpression(),
		};
	}
	
	public EClass[] getStartSymbols() {
		return new EClass[] {
			de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwExpression(),
		};
	}
	
}
