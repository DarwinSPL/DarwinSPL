/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.mopp;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * The DwexpressionTokenResolverFactory class provides access to all generated
 * token resolvers. By giving the name of a defined token, the corresponding
 * resolve can be obtained. Despite the fact that this class is called
 * TokenResolverFactory is does NOT create new token resolvers whenever a client
 * calls methods to obtain a resolver. Rather, this class maintains a map of all
 * resolvers and creates each resolver at most once.
 */
public class DwexpressionTokenResolverFactory implements de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolverFactory {
	
	private Map<String, de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolver> tokenName2TokenResolver;
	private Map<String, de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolver> featureName2CollectInTokenResolver;
	private static de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolver defaultResolver = new de.darwinspl.expression.resource.dwexpression.analysis.DwexpressionDefaultTokenResolver();
	
	public DwexpressionTokenResolverFactory() {
		tokenName2TokenResolver = new LinkedHashMap<String, de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolver>();
		featureName2CollectInTokenResolver = new LinkedHashMap<String, de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolver>();
		registerTokenResolver("IDENTIFIER_TOKEN", new de.darwinspl.expression.resource.dwexpression.analysis.DwexpressionIDENTIFIER_TOKENTokenResolver());
		registerTokenResolver("QUOTED_34_34", new de.darwinspl.expression.resource.dwexpression.analysis.DwexpressionQUOTED_34_34TokenResolver());
	}
	
	public de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolver createTokenResolver(String tokenName) {
		return internalCreateResolver(tokenName2TokenResolver, tokenName);
	}
	
	public de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolver createCollectInTokenResolver(String featureName) {
		return internalCreateResolver(featureName2CollectInTokenResolver, featureName);
	}
	
	protected boolean registerTokenResolver(String tokenName, de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolver resolver){
		return internalRegisterTokenResolver(tokenName2TokenResolver, tokenName, resolver);
	}
	
	protected boolean registerCollectInTokenResolver(String featureName, de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolver resolver){
		return internalRegisterTokenResolver(featureName2CollectInTokenResolver, featureName, resolver);
	}
	
	protected de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolver deRegisterTokenResolver(String tokenName){
		return tokenName2TokenResolver.remove(tokenName);
	}
	
	private de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolver internalCreateResolver(Map<String, de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolver> resolverMap, String key) {
		if (resolverMap.containsKey(key)){
			return resolverMap.get(key);
		} else {
			return defaultResolver;
		}
	}
	
	private boolean internalRegisterTokenResolver(Map<String, de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolver> resolverMap, String key, de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolver resolver) {
		if (!resolverMap.containsKey(key)) {
			resolverMap.put(key,resolver);
			return true;
		}
		return false;
	}
	
}
