/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression;


/**
 * A simple interface for commands that can be executed and that return
 * information about the success of their execution.
 */
public interface IDwexpressionCommand<ContextType> {
	
	public boolean execute(ContextType context);
}
