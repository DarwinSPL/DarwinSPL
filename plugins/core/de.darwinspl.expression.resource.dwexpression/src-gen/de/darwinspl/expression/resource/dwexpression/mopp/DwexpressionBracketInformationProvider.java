/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.mopp;

import java.util.ArrayList;
import java.util.Collection;

/**
 * This class provides information about how brackets must be handled in the
 * editor (e.g., whether they must be closed automatically).
 */
public class DwexpressionBracketInformationProvider {
	
	public Collection<de.darwinspl.expression.resource.dwexpression.IDwexpressionBracketPair> getBracketPairs() {
		Collection<de.darwinspl.expression.resource.dwexpression.IDwexpressionBracketPair> result = new ArrayList<de.darwinspl.expression.resource.dwexpression.IDwexpressionBracketPair>();
		result.add(new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionBracketPair("(", ")", true, false));
		result.add(new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionBracketPair("\"", "\"", false, false));
		return result;
	}
	
}
