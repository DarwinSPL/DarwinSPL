/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.mopp;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;

public class DwexpressionProblem implements de.darwinspl.expression.resource.dwexpression.IDwexpressionProblem {
	
	private String message;
	private de.darwinspl.expression.resource.dwexpression.DwexpressionEProblemType type;
	private de.darwinspl.expression.resource.dwexpression.DwexpressionEProblemSeverity severity;
	private Collection<de.darwinspl.expression.resource.dwexpression.IDwexpressionQuickFix> quickFixes;
	
	public DwexpressionProblem(String message, de.darwinspl.expression.resource.dwexpression.DwexpressionEProblemType type, de.darwinspl.expression.resource.dwexpression.DwexpressionEProblemSeverity severity) {
		this(message, type, severity, Collections.<de.darwinspl.expression.resource.dwexpression.IDwexpressionQuickFix>emptySet());
	}
	
	public DwexpressionProblem(String message, de.darwinspl.expression.resource.dwexpression.DwexpressionEProblemType type, de.darwinspl.expression.resource.dwexpression.DwexpressionEProblemSeverity severity, de.darwinspl.expression.resource.dwexpression.IDwexpressionQuickFix quickFix) {
		this(message, type, severity, Collections.singleton(quickFix));
	}
	
	public DwexpressionProblem(String message, de.darwinspl.expression.resource.dwexpression.DwexpressionEProblemType type, de.darwinspl.expression.resource.dwexpression.DwexpressionEProblemSeverity severity, Collection<de.darwinspl.expression.resource.dwexpression.IDwexpressionQuickFix> quickFixes) {
		super();
		this.message = message;
		this.type = type;
		this.severity = severity;
		this.quickFixes = new LinkedHashSet<de.darwinspl.expression.resource.dwexpression.IDwexpressionQuickFix>();
		this.quickFixes.addAll(quickFixes);
	}
	
	public de.darwinspl.expression.resource.dwexpression.DwexpressionEProblemType getType() {
		return type;
	}
	
	public de.darwinspl.expression.resource.dwexpression.DwexpressionEProblemSeverity getSeverity() {
		return severity;
	}
	
	public String getMessage() {
		return message;
	}
	
	public Collection<de.darwinspl.expression.resource.dwexpression.IDwexpressionQuickFix> getQuickFixes() {
		return quickFixes;
	}
	
}
