// $ANTLR 3.4

	package de.darwinspl.expression.resource.dwexpression.mopp;
	
	import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.antlr.runtime3_4_0.ANTLRInputStream;
import org.antlr.runtime3_4_0.BitSet;
import org.antlr.runtime3_4_0.CommonToken;
import org.antlr.runtime3_4_0.CommonTokenStream;
import org.antlr.runtime3_4_0.IntStream;
import org.antlr.runtime3_4_0.Lexer;
import org.antlr.runtime3_4_0.RecognitionException;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;


import org.antlr.runtime3_4_0.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class DwexpressionParser extends DwexpressionANTLRParserBase {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "DATE", "IDENTIFIER_TOKEN", "INTEGER_LITERAL", "LINEBREAK", "ML_COMMENT", "QUOTED_34_34", "SL_COMMENT", "WHITESPACE", "'!'", "'&&'", "'('", "')'", "'->'", "'<->'", "'false'", "'true'", "'||'"
    };

    public static final int EOF=-1;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__19=19;
    public static final int T__20=20;
    public static final int DATE=4;
    public static final int IDENTIFIER_TOKEN=5;
    public static final int INTEGER_LITERAL=6;
    public static final int LINEBREAK=7;
    public static final int ML_COMMENT=8;
    public static final int QUOTED_34_34=9;
    public static final int SL_COMMENT=10;
    public static final int WHITESPACE=11;

    // delegates
    public DwexpressionANTLRParserBase[] getDelegates() {
        return new DwexpressionANTLRParserBase[] {};
    }

    // delegators


    public DwexpressionParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public DwexpressionParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
        this.state.initializeRuleMemo(24 + 1);
         

    }

    public String[] getTokenNames() { return DwexpressionParser.tokenNames; }
    public String getGrammarFileName() { return "Dwexpression.g"; }


    	private de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolverFactory tokenResolverFactory = new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTokenResolverFactory();
    	
    	/**
    	 * the index of the last token that was handled by collectHiddenTokens()
    	 */
    	private int lastPosition;
    	
    	/**
    	 * A flag that indicates whether the parser should remember all expected elements.
    	 * This flag is set to true when using the parse for code completion. Otherwise it
    	 * is set to false.
    	 */
    	private boolean rememberExpectedElements = false;
    	
    	private Object parseToIndexTypeObject;
    	private int lastTokenIndex = 0;
    	
    	/**
    	 * A list of expected elements the were collected while parsing the input stream.
    	 * This list is only filled if <code>rememberExpectedElements</code> is set to
    	 * true.
    	 */
    	private List<de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal> expectedElements = new ArrayList<de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal>();
    	
    	private int mismatchedTokenRecoveryTries = 0;
    	/**
    	 * A helper list to allow a lexer to pass errors to its parser
    	 */
    	protected List<RecognitionException> lexerExceptions = Collections.synchronizedList(new ArrayList<RecognitionException>());
    	
    	/**
    	 * Another helper list to allow a lexer to pass positions of errors to its parser
    	 */
    	protected List<Integer> lexerExceptionPositions = Collections.synchronizedList(new ArrayList<Integer>());
    	
    	/**
    	 * A stack for incomplete objects. This stack is used filled when the parser is
    	 * used for code completion. Whenever the parser starts to read an object it is
    	 * pushed on the stack. Once the element was parser completely it is popped from
    	 * the stack.
    	 */
    	List<EObject> incompleteObjects = new ArrayList<EObject>();
    	
    	private int stopIncludingHiddenTokens;
    	private int stopExcludingHiddenTokens;
    	private int tokenIndexOfLastCompleteElement;
    	
    	private int expectedElementsIndexOfLastCompleteElement;
    	
    	/**
    	 * The offset indicating the cursor position when the parser is used for code
    	 * completion by calling parseToExpectedElements().
    	 */
    	private int cursorOffset;
    	
    	/**
    	 * The offset of the first hidden token of the last expected element. This offset
    	 * is used to discard expected elements, which are not needed for code completion.
    	 */
    	private int lastStartIncludingHidden;
    	
    	private de.darwinspl.expression.resource.dwexpression.IDwexpressionLocationMap locationMap;
    	
    	private de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionSyntaxErrorMessageConverter syntaxErrorMessageConverter = new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionSyntaxErrorMessageConverter(tokenNames);
    	
    	@Override
    	public void reportError(RecognitionException re) {
    		addErrorToResource(syntaxErrorMessageConverter.translateParseError(re));
    	}
    	
    	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
    		postParseCommands.add(new de.darwinspl.expression.resource.dwexpression.IDwexpressionCommand<de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource>() {
    			public boolean execute(de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource resource) {
    				if (resource == null) {
    					// the resource can be null if the parser is used for code completion
    					return true;
    				}
    				resource.addProblem(new de.darwinspl.expression.resource.dwexpression.IDwexpressionProblem() {
    					public de.darwinspl.expression.resource.dwexpression.DwexpressionEProblemSeverity getSeverity() {
    						return de.darwinspl.expression.resource.dwexpression.DwexpressionEProblemSeverity.ERROR;
    					}
    					public de.darwinspl.expression.resource.dwexpression.DwexpressionEProblemType getType() {
    						return de.darwinspl.expression.resource.dwexpression.DwexpressionEProblemType.SYNTAX_ERROR;
    					}
    					public String getMessage() {
    						return errorMessage;
    					}
    					public Collection<de.darwinspl.expression.resource.dwexpression.IDwexpressionQuickFix> getQuickFixes() {
    						return null;
    					}
    				}, column, line, startIndex, stopIndex);
    				return true;
    			}
    		});
    	}
    	
    	protected void addErrorToResource(de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionLocalizedMessage message) {
    		if (message == null) {
    			return;
    		}
    		addErrorToResource(message.getMessage(), message.getColumn(), message.getLine(), message.getCharStart(), message.getCharEnd());
    	}
    	
    	public void addExpectedElement(EClass eClass, int expectationStartIndex, int expectationEndIndex) {
    		for (int expectationIndex = expectationStartIndex; expectationIndex <= expectationEndIndex; expectationIndex++) {
    			addExpectedElement(eClass, de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectationConstants.EXPECTATIONS[expectationIndex]);
    		}
    	}
    	
    	public void addExpectedElement(EClass eClass, int expectationIndex) {
    		addExpectedElement(eClass, de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectationConstants.EXPECTATIONS[expectationIndex]);
    	}
    	
    	public void addExpectedElement(EClass eClass, int[] ids) {
    		if (!this.rememberExpectedElements) {
    			return;
    		}
    		int terminalID = ids[0];
    		int followSetID = ids[1];
    		de.darwinspl.expression.resource.dwexpression.IDwexpressionExpectedElement terminal = de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionFollowSetProvider.TERMINALS[terminalID];
    		de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionContainedFeature[] containmentFeatures = new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionContainedFeature[ids.length - 2];
    		for (int i = 2; i < ids.length; i++) {
    			containmentFeatures[i - 2] = de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionFollowSetProvider.LINKS[ids[i]];
    		}
    		de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainmentTrace containmentTrace = new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainmentTrace(eClass, containmentFeatures);
    		EObject container = getLastIncompleteElement();
    		de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal expectedElement = new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal(container, terminal, followSetID, containmentTrace);
    		setPosition(expectedElement, input.index());
    		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
    		lastStartIncludingHidden = startIncludingHiddenTokens;
    		this.expectedElements.add(expectedElement);
    	}
    	
    	protected void collectHiddenTokens(EObject element) {
    	}
    	
    	protected void copyLocalizationInfos(final EObject source, final EObject target) {
    		if (disableLocationMap) {
    			return;
    		}
    		final de.darwinspl.expression.resource.dwexpression.IDwexpressionLocationMap locationMap = this.locationMap;
    		if (locationMap == null) {
    			// the locationMap can be null if the parser is used for code completion
    			return;
    		}
    		postParseCommands.add(new de.darwinspl.expression.resource.dwexpression.IDwexpressionCommand<de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource>() {
    			public boolean execute(de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource resource) {
    				locationMap.setCharStart(target, locationMap.getCharStart(source));
    				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
    				locationMap.setColumn(target, locationMap.getColumn(source));
    				locationMap.setLine(target, locationMap.getLine(source));
    				return true;
    			}
    		});
    	}
    	
    	protected void copyLocalizationInfos(final CommonToken source, final EObject target) {
    		if (disableLocationMap) {
    			return;
    		}
    		final de.darwinspl.expression.resource.dwexpression.IDwexpressionLocationMap locationMap = this.locationMap;
    		if (locationMap == null) {
    			// the locationMap can be null if the parser is used for code completion
    			return;
    		}
    		postParseCommands.add(new de.darwinspl.expression.resource.dwexpression.IDwexpressionCommand<de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource>() {
    			public boolean execute(de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource resource) {
    				if (source == null) {
    					return true;
    				}
    				locationMap.setCharStart(target, source.getStartIndex());
    				locationMap.setCharEnd(target, source.getStopIndex());
    				locationMap.setColumn(target, source.getCharPositionInLine());
    				locationMap.setLine(target, source.getLine());
    				return true;
    			}
    		});
    	}
    	
    	/**
    	 * Sets the end character index and the last line for the given object in the
    	 * location map.
    	 */
    	protected void setLocalizationEnd(Collection<de.darwinspl.expression.resource.dwexpression.IDwexpressionCommand<de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource>> postParseCommands , final EObject object, final int endChar, final int endLine) {
    		if (disableLocationMap) {
    			return;
    		}
    		final de.darwinspl.expression.resource.dwexpression.IDwexpressionLocationMap locationMap = this.locationMap;
    		if (locationMap == null) {
    			// the locationMap can be null if the parser is used for code completion
    			return;
    		}
    		postParseCommands.add(new de.darwinspl.expression.resource.dwexpression.IDwexpressionCommand<de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource>() {
    			public boolean execute(de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource resource) {
    				locationMap.setCharEnd(object, endChar);
    				locationMap.setLine(object, endLine);
    				return true;
    			}
    		});
    	}
    	
    	public de.darwinspl.expression.resource.dwexpression.IDwexpressionTextParser createInstance(InputStream actualInputStream, String encoding) {
    		try {
    			if (encoding == null) {
    				return new DwexpressionParser(new CommonTokenStream(new DwexpressionLexer(new ANTLRInputStream(actualInputStream))));
    			} else {
    				return new DwexpressionParser(new CommonTokenStream(new DwexpressionLexer(new ANTLRInputStream(actualInputStream, encoding))));
    			}
    		} catch (IOException e) {
    			new de.darwinspl.expression.resource.dwexpression.util.DwexpressionRuntimeUtil().logError("Error while creating parser.", e);
    			return null;
    		}
    	}
    	
    	/**
    	 * This default constructor is only used to call createInstance() on it.
    	 */
    	public DwexpressionParser() {
    		super(null);
    	}
    	
    	protected EObject doParse() throws RecognitionException {
    		this.lastPosition = 0;
    		// required because the lexer class can not be subclassed
    		((DwexpressionLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
    		((DwexpressionLexer) getTokenStream().getTokenSource()).lexerExceptionPositions = lexerExceptionPositions;
    		Object typeObject = getTypeObject();
    		if (typeObject == null) {
    			return start();
    		} else if (typeObject instanceof EClass) {
    		}
    		throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionUnexpectedContentTypeException(typeObject);
    	}
    	
    	public int getMismatchedTokenRecoveryTries() {
    		return mismatchedTokenRecoveryTries;
    	}
    	
    	public Object getMissingSymbol(IntStream arg0, RecognitionException arg1, int arg2, BitSet arg3) {
    		mismatchedTokenRecoveryTries++;
    		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
    	}
    	
    	public Object getParseToIndexTypeObject() {
    		return parseToIndexTypeObject;
    	}
    	
    	protected Object getTypeObject() {
    		Object typeObject = getParseToIndexTypeObject();
    		if (typeObject != null) {
    			return typeObject;
    		}
    		Map<?,?> options = getOptions();
    		if (options != null) {
    			typeObject = options.get(de.darwinspl.expression.resource.dwexpression.IDwexpressionOptions.RESOURCE_CONTENT_TYPE);
    		}
    		return typeObject;
    	}
    	
    	/**
    	 * Implementation that calls {@link #doParse()} and handles the thrown
    	 * RecognitionExceptions.
    	 */
    	public de.darwinspl.expression.resource.dwexpression.IDwexpressionParseResult parse() {
    		// Reset parser state
    		terminateParsing = false;
    		postParseCommands = new ArrayList<de.darwinspl.expression.resource.dwexpression.IDwexpressionCommand<de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource>>();
    		de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionParseResult parseResult = new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionParseResult();
    		if (disableLocationMap) {
    			locationMap = new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionDevNullLocationMap();
    		} else {
    			locationMap = new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionLocationMap();
    		}
    		// Run parser
    		try {
    			EObject result =  doParse();
    			if (lexerExceptions.isEmpty()) {
    				parseResult.setRoot(result);
    				parseResult.setLocationMap(locationMap);
    			}
    		} catch (RecognitionException re) {
    			addErrorToResource(syntaxErrorMessageConverter.translateParseError(re));
    		} catch (IllegalArgumentException iae) {
    			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
    				// can be caused if a null is set on EMF models where not allowed. this will just
    				// happen if other errors occurred before
    			} else {
    				iae.printStackTrace();
    			}
    		}
    		for (RecognitionException re : lexerExceptions) {
    			addErrorToResource(syntaxErrorMessageConverter.translateLexicalError(re, lexerExceptions, lexerExceptionPositions));
    		}
    		parseResult.getPostParseCommands().addAll(postParseCommands);
    		return parseResult;
    	}
    	
    	public List<de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal> parseToExpectedElements(EClass type, de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource dummyResource, int cursorOffset) {
    		this.rememberExpectedElements = true;
    		this.parseToIndexTypeObject = type;
    		this.cursorOffset = cursorOffset;
    		this.lastStartIncludingHidden = -1;
    		final CommonTokenStream tokenStream = (CommonTokenStream) getTokenStream();
    		de.darwinspl.expression.resource.dwexpression.IDwexpressionParseResult result = parse();
    		for (EObject incompleteObject : incompleteObjects) {
    			Lexer lexer = (Lexer) tokenStream.getTokenSource();
    			int endChar = lexer.getCharIndex();
    			int endLine = lexer.getLine();
    			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
    		}
    		if (result != null) {
    			EObject root = result.getRoot();
    			if (root != null) {
    				dummyResource.getContentsInternal().add(root);
    			}
    			for (de.darwinspl.expression.resource.dwexpression.IDwexpressionCommand<de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource> command : result.getPostParseCommands()) {
    				command.execute(dummyResource);
    			}
    		}
    		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
    		Set<de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal> currentFollowSet = new LinkedHashSet<de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal>();
    		List<de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal> newFollowSet = new ArrayList<de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal>();
    		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
    			de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal expectedElementI = expectedElements.get(i);
    			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
    				currentFollowSet.add(expectedElementI);
    			} else {
    				break;
    			}
    		}
    		int followSetID = 13;
    		int i;
    		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
    			CommonToken nextToken = (CommonToken) tokenStream.get(i);
    			if (nextToken.getType() < 0) {
    				break;
    			}
    			if (nextToken.getChannel() == 99) {
    				// hidden tokens do not reduce the follow set
    			} else {
    				// now that we have found the next visible token the position for that expected
    				// terminals can be set
    				for (de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal nextFollow : newFollowSet) {
    					lastTokenIndex = 0;
    					setPosition(nextFollow, i);
    				}
    				newFollowSet.clear();
    				// normal tokens do reduce the follow set - only elements that match the token are
    				// kept
    				for (de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal nextFollow : currentFollowSet) {
    					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
    						// keep this one - it matches
    						Collection<de.darwinspl.expression.resource.dwexpression.util.DwexpressionPair<de.darwinspl.expression.resource.dwexpression.IDwexpressionExpectedElement, de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionContainedFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
    						for (de.darwinspl.expression.resource.dwexpression.util.DwexpressionPair<de.darwinspl.expression.resource.dwexpression.IDwexpressionExpectedElement, de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionContainedFeature[]> newFollowerPair : newFollowers) {
    							de.darwinspl.expression.resource.dwexpression.IDwexpressionExpectedElement newFollower = newFollowerPair.getLeft();
    							EObject container = getLastIncompleteElement();
    							de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainmentTrace containmentTrace = new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainmentTrace(null, newFollowerPair.getRight());
    							de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal newFollowTerminal = new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal(container, newFollower, followSetID, containmentTrace);
    							newFollowSet.add(newFollowTerminal);
    							expectedElements.add(newFollowTerminal);
    						}
    					}
    				}
    				currentFollowSet.clear();
    				currentFollowSet.addAll(newFollowSet);
    			}
    			followSetID++;
    		}
    		// after the last token in the stream we must set the position for the elements
    		// that were added during the last iteration of the loop
    		for (de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal nextFollow : newFollowSet) {
    			lastTokenIndex = 0;
    			setPosition(nextFollow, i);
    		}
    		return this.expectedElements;
    	}
    	
    	public void setPosition(de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionExpectedTerminal expectedElement, int tokenIndex) {
    		int currentIndex = Math.max(0, tokenIndex);
    		for (int index = lastTokenIndex; index < currentIndex; index++) {
    			if (index >= input.size()) {
    				break;
    			}
    			CommonToken tokenAtIndex = (CommonToken) input.get(index);
    			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			if (tokenAtIndex.getChannel() != 99 && !anonymousTokens.contains(tokenAtIndex)) {
    				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			}
    		}
    		lastTokenIndex = Math.max(0, currentIndex);
    		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
    	}
    	
    	public Object recoverFromMismatchedToken(IntStream input, int ttype, BitSet follow) throws RecognitionException {
    		if (!rememberExpectedElements) {
    			return super.recoverFromMismatchedToken(input, ttype, follow);
    		} else {
    			return null;
    		}
    	}
    	
    	private void startIncompleteElement(Object object) {
    		if (object instanceof EObject) {
    			this.incompleteObjects.add((EObject) object);
    		}
    	}
    	
    	private void completedElement(Object object, boolean isContainment) {
    		if (isContainment && !this.incompleteObjects.isEmpty()) {
    			this.incompleteObjects.remove(object);
    		}
    		if (object instanceof EObject) {
    			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
    			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
    		}
    	}
    	
    	private EObject getLastIncompleteElement() {
    		if (incompleteObjects.isEmpty()) {
    			return null;
    		}
    		return incompleteObjects.get(incompleteObjects.size() - 1);
    	}
    	



    // $ANTLR start "start"
    // Dwexpression.g:478:1: start returns [ EObject element = null] : (c0= parse_de_darwinspl_expression_DwExpression ) EOF ;
    public final EObject start() throws RecognitionException {
        EObject element =  null;

        int start_StartIndex = input.index();

        de.darwinspl.expression.DwExpression c0 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 1) ) { return element; }

            // Dwexpression.g:479:2: ( (c0= parse_de_darwinspl_expression_DwExpression ) EOF )
            // Dwexpression.g:480:2: (c0= parse_de_darwinspl_expression_DwExpression ) EOF
            {
            if ( state.backtracking==0 ) {
            		// follow set for start rule(s)
            		addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwEquivalenceExpression(), 0, 324);
            		addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwImpliesExpression(), 325, 649);
            		addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwOrExpression(), 650, 974);
            		addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwAndExpression(), 975, 1299);
            		addExpectedElement(null, 1300, 1304);
            		expectedElementsIndexOfLastCompleteElement = 1304;
            	}

            // Dwexpression.g:489:2: (c0= parse_de_darwinspl_expression_DwExpression )
            // Dwexpression.g:490:3: c0= parse_de_darwinspl_expression_DwExpression
            {
            pushFollow(FOLLOW_parse_de_darwinspl_expression_DwExpression_in_start82);
            c0=parse_de_darwinspl_expression_DwExpression();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) { element = c0; }

            }


            match(input,EOF,FOLLOW_EOF_in_start89); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		retrieveLayoutInformation(element, null, null, false);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 1, start_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "start"



    // $ANTLR start "parseop_DwExpression_level_0"
    // Dwexpression.g:498:1: parseop_DwExpression_level_0 returns [de.darwinspl.expression.DwExpression element = null] : leftArg= parseop_DwExpression_level_1 ( ( () a0= '<->' rightArg= parseop_DwExpression_level_1 )+ |) ;
    public final de.darwinspl.expression.DwExpression parseop_DwExpression_level_0() throws RecognitionException {
        de.darwinspl.expression.DwExpression element =  null;

        int parseop_DwExpression_level_0_StartIndex = input.index();

        Token a0=null;
        de.darwinspl.expression.DwExpression leftArg =null;

        de.darwinspl.expression.DwExpression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 2) ) { return element; }

            // Dwexpression.g:501:2: (leftArg= parseop_DwExpression_level_1 ( ( () a0= '<->' rightArg= parseop_DwExpression_level_1 )+ |) )
            // Dwexpression.g:502:2: leftArg= parseop_DwExpression_level_1 ( ( () a0= '<->' rightArg= parseop_DwExpression_level_1 )+ |)
            {
            pushFollow(FOLLOW_parseop_DwExpression_level_1_in_parseop_DwExpression_level_0115);
            leftArg=parseop_DwExpression_level_1();

            state._fsp--;
            if (state.failed) return element;

            // Dwexpression.g:502:41: ( ( () a0= '<->' rightArg= parseop_DwExpression_level_1 )+ |)
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==17) ) {
                alt2=1;
            }
            else if ( (LA2_0==EOF||LA2_0==15) ) {
                alt2=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }
            switch (alt2) {
                case 1 :
                    // Dwexpression.g:502:42: ( () a0= '<->' rightArg= parseop_DwExpression_level_1 )+
                    {
                    // Dwexpression.g:502:42: ( () a0= '<->' rightArg= parseop_DwExpression_level_1 )+
                    int cnt1=0;
                    loop1:
                    do {
                        int alt1=2;
                        int LA1_0 = input.LA(1);

                        if ( (LA1_0==17) ) {
                            alt1=1;
                        }


                        switch (alt1) {
                    	case 1 :
                    	    // Dwexpression.g:503:3: () a0= '<->' rightArg= parseop_DwExpression_level_1
                    	    {
                    	    // Dwexpression.g:503:3: ()
                    	    // Dwexpression.g:503:4: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,17,FOLLOW_17_in_parseop_DwExpression_level_0135); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    			if (element == null) {
                    	    				element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwEquivalenceExpression();
                    	    				startIncompleteElement(element);
                    	    			}
                    	    			collectHiddenTokens(element);
                    	    			retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_0_0_0_1, null, true);
                    	    			copyLocalizationInfos((CommonToken)a0, element);
                    	    		}

                    	    if ( state.backtracking==0 ) {
                    	    			// expected elements (follow set)
                    	    			addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwEquivalenceExpression(), 1305, 1629);
                    	    		}

                    	    pushFollow(FOLLOW_parseop_DwExpression_level_1_in_parseop_DwExpression_level_0152);
                    	    rightArg=parseop_DwExpression_level_1();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    			if (terminateParsing) {
                    	    				throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTerminateParsingException();
                    	    			}
                    	    			if (element == null) {
                    	    				element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwEquivalenceExpression();
                    	    				startIncompleteElement(element);
                    	    			}
                    	    			if (leftArg != null) {
                    	    				if (leftArg != null) {
                    	    					Object value = leftArg;
                    	    					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_EQUIVALENCE_EXPRESSION__OPERAND1), value);
                    	    					completedElement(value, true);
                    	    				}
                    	    				collectHiddenTokens(element);
                    	    				retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_0_0_0_0, leftArg, true);
                    	    				copyLocalizationInfos(leftArg, element);
                    	    			}
                    	    		}

                    	    if ( state.backtracking==0 ) {
                    	    			if (terminateParsing) {
                    	    				throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTerminateParsingException();
                    	    			}
                    	    			if (element == null) {
                    	    				element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwEquivalenceExpression();
                    	    				startIncompleteElement(element);
                    	    			}
                    	    			if (rightArg != null) {
                    	    				if (rightArg != null) {
                    	    					Object value = rightArg;
                    	    					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_EQUIVALENCE_EXPRESSION__OPERAND2), value);
                    	    					completedElement(value, true);
                    	    				}
                    	    				collectHiddenTokens(element);
                    	    				retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_0_0_0_2, rightArg, true);
                    	    				copyLocalizationInfos(rightArg, element);
                    	    			}
                    	    		}

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt1 >= 1 ) break loop1;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(1, input);
                                throw eee;
                        }
                        cnt1++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Dwexpression.g:558:21: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 2, parseop_DwExpression_level_0_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_DwExpression_level_0"



    // $ANTLR start "parseop_DwExpression_level_1"
    // Dwexpression.g:563:1: parseop_DwExpression_level_1 returns [de.darwinspl.expression.DwExpression element = null] : leftArg= parseop_DwExpression_level_2 ( ( () a0= '->' rightArg= parseop_DwExpression_level_2 )+ |) ;
    public final de.darwinspl.expression.DwExpression parseop_DwExpression_level_1() throws RecognitionException {
        de.darwinspl.expression.DwExpression element =  null;

        int parseop_DwExpression_level_1_StartIndex = input.index();

        Token a0=null;
        de.darwinspl.expression.DwExpression leftArg =null;

        de.darwinspl.expression.DwExpression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 3) ) { return element; }

            // Dwexpression.g:566:9: (leftArg= parseop_DwExpression_level_2 ( ( () a0= '->' rightArg= parseop_DwExpression_level_2 )+ |) )
            // Dwexpression.g:567:9: leftArg= parseop_DwExpression_level_2 ( ( () a0= '->' rightArg= parseop_DwExpression_level_2 )+ |)
            {
            pushFollow(FOLLOW_parseop_DwExpression_level_2_in_parseop_DwExpression_level_1198);
            leftArg=parseop_DwExpression_level_2();

            state._fsp--;
            if (state.failed) return element;

            // Dwexpression.g:567:39: ( ( () a0= '->' rightArg= parseop_DwExpression_level_2 )+ |)
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==16) ) {
                alt4=1;
            }
            else if ( (LA4_0==EOF||LA4_0==15||LA4_0==17) ) {
                alt4=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;

            }
            switch (alt4) {
                case 1 :
                    // Dwexpression.g:567:40: ( () a0= '->' rightArg= parseop_DwExpression_level_2 )+
                    {
                    // Dwexpression.g:567:40: ( () a0= '->' rightArg= parseop_DwExpression_level_2 )+
                    int cnt3=0;
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==16) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // Dwexpression.g:568:2: () a0= '->' rightArg= parseop_DwExpression_level_2
                    	    {
                    	    // Dwexpression.g:568:2: ()
                    	    // Dwexpression.g:568:3: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,16,FOLLOW_16_in_parseop_DwExpression_level_1214); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    		if (element == null) {
                    	    			element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwImpliesExpression();
                    	    			startIncompleteElement(element);
                    	    		}
                    	    		collectHiddenTokens(element);
                    	    		retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_1_0_0_1, null, true);
                    	    		copyLocalizationInfos((CommonToken)a0, element);
                    	    	}

                    	    if ( state.backtracking==0 ) {
                    	    		// expected elements (follow set)
                    	    		addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwImpliesExpression(), 1630, 1954);
                    	    	}

                    	    pushFollow(FOLLOW_parseop_DwExpression_level_2_in_parseop_DwExpression_level_1228);
                    	    rightArg=parseop_DwExpression_level_2();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    		if (terminateParsing) {
                    	    			throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTerminateParsingException();
                    	    		}
                    	    		if (element == null) {
                    	    			element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwImpliesExpression();
                    	    			startIncompleteElement(element);
                    	    		}
                    	    		if (leftArg != null) {
                    	    			if (leftArg != null) {
                    	    				Object value = leftArg;
                    	    				element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_IMPLIES_EXPRESSION__OPERAND1), value);
                    	    				completedElement(value, true);
                    	    			}
                    	    			collectHiddenTokens(element);
                    	    			retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_1_0_0_0, leftArg, true);
                    	    			copyLocalizationInfos(leftArg, element);
                    	    		}
                    	    	}

                    	    if ( state.backtracking==0 ) {
                    	    		if (terminateParsing) {
                    	    			throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTerminateParsingException();
                    	    		}
                    	    		if (element == null) {
                    	    			element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwImpliesExpression();
                    	    			startIncompleteElement(element);
                    	    		}
                    	    		if (rightArg != null) {
                    	    			if (rightArg != null) {
                    	    				Object value = rightArg;
                    	    				element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_IMPLIES_EXPRESSION__OPERAND2), value);
                    	    				completedElement(value, true);
                    	    			}
                    	    			collectHiddenTokens(element);
                    	    			retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_1_0_0_2, rightArg, true);
                    	    			copyLocalizationInfos(rightArg, element);
                    	    		}
                    	    	}

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt3 >= 1 ) break loop3;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(3, input);
                                throw eee;
                        }
                        cnt3++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Dwexpression.g:623:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 3, parseop_DwExpression_level_1_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_DwExpression_level_1"



    // $ANTLR start "parseop_DwExpression_level_2"
    // Dwexpression.g:628:1: parseop_DwExpression_level_2 returns [de.darwinspl.expression.DwExpression element = null] : leftArg= parseop_DwExpression_level_3 ( ( () a0= '||' rightArg= parseop_DwExpression_level_3 )+ |) ;
    public final de.darwinspl.expression.DwExpression parseop_DwExpression_level_2() throws RecognitionException {
        de.darwinspl.expression.DwExpression element =  null;

        int parseop_DwExpression_level_2_StartIndex = input.index();

        Token a0=null;
        de.darwinspl.expression.DwExpression leftArg =null;

        de.darwinspl.expression.DwExpression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 4) ) { return element; }

            // Dwexpression.g:631:9: (leftArg= parseop_DwExpression_level_3 ( ( () a0= '||' rightArg= parseop_DwExpression_level_3 )+ |) )
            // Dwexpression.g:632:9: leftArg= parseop_DwExpression_level_3 ( ( () a0= '||' rightArg= parseop_DwExpression_level_3 )+ |)
            {
            pushFollow(FOLLOW_parseop_DwExpression_level_3_in_parseop_DwExpression_level_2269);
            leftArg=parseop_DwExpression_level_3();

            state._fsp--;
            if (state.failed) return element;

            // Dwexpression.g:632:39: ( ( () a0= '||' rightArg= parseop_DwExpression_level_3 )+ |)
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==20) ) {
                alt6=1;
            }
            else if ( (LA6_0==EOF||(LA6_0 >= 15 && LA6_0 <= 17)) ) {
                alt6=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;

            }
            switch (alt6) {
                case 1 :
                    // Dwexpression.g:632:40: ( () a0= '||' rightArg= parseop_DwExpression_level_3 )+
                    {
                    // Dwexpression.g:632:40: ( () a0= '||' rightArg= parseop_DwExpression_level_3 )+
                    int cnt5=0;
                    loop5:
                    do {
                        int alt5=2;
                        int LA5_0 = input.LA(1);

                        if ( (LA5_0==20) ) {
                            alt5=1;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // Dwexpression.g:633:0: () a0= '||' rightArg= parseop_DwExpression_level_3
                    	    {
                    	    // Dwexpression.g:633:2: ()
                    	    // Dwexpression.g:633:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,20,FOLLOW_20_in_parseop_DwExpression_level_2282); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    	if (element == null) {
                    	    		element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwOrExpression();
                    	    		startIncompleteElement(element);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_2_0_0_1, null, true);
                    	    	copyLocalizationInfos((CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    	// expected elements (follow set)
                    	    	addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwOrExpression(), 1955, 2279);
                    	    }

                    	    pushFollow(FOLLOW_parseop_DwExpression_level_3_in_parseop_DwExpression_level_2293);
                    	    rightArg=parseop_DwExpression_level_3();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    	if (terminateParsing) {
                    	    		throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTerminateParsingException();
                    	    	}
                    	    	if (element == null) {
                    	    		element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwOrExpression();
                    	    		startIncompleteElement(element);
                    	    	}
                    	    	if (leftArg != null) {
                    	    		if (leftArg != null) {
                    	    			Object value = leftArg;
                    	    			element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_OR_EXPRESSION__OPERAND1), value);
                    	    			completedElement(value, true);
                    	    		}
                    	    		collectHiddenTokens(element);
                    	    		retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_2_0_0_0, leftArg, true);
                    	    		copyLocalizationInfos(leftArg, element);
                    	    	}
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    	if (terminateParsing) {
                    	    		throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTerminateParsingException();
                    	    	}
                    	    	if (element == null) {
                    	    		element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwOrExpression();
                    	    		startIncompleteElement(element);
                    	    	}
                    	    	if (rightArg != null) {
                    	    		if (rightArg != null) {
                    	    			Object value = rightArg;
                    	    			element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_OR_EXPRESSION__OPERAND2), value);
                    	    			completedElement(value, true);
                    	    		}
                    	    		collectHiddenTokens(element);
                    	    		retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_2_0_0_2, rightArg, true);
                    	    		copyLocalizationInfos(rightArg, element);
                    	    	}
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt5 >= 1 ) break loop5;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(5, input);
                                throw eee;
                        }
                        cnt5++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Dwexpression.g:688:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 4, parseop_DwExpression_level_2_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_DwExpression_level_2"



    // $ANTLR start "parseop_DwExpression_level_3"
    // Dwexpression.g:693:1: parseop_DwExpression_level_3 returns [de.darwinspl.expression.DwExpression element = null] : leftArg= parseop_DwExpression_level_14 ( ( () a0= '&&' rightArg= parseop_DwExpression_level_14 )+ |) ;
    public final de.darwinspl.expression.DwExpression parseop_DwExpression_level_3() throws RecognitionException {
        de.darwinspl.expression.DwExpression element =  null;

        int parseop_DwExpression_level_3_StartIndex = input.index();

        Token a0=null;
        de.darwinspl.expression.DwExpression leftArg =null;

        de.darwinspl.expression.DwExpression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 5) ) { return element; }

            // Dwexpression.g:696:9: (leftArg= parseop_DwExpression_level_14 ( ( () a0= '&&' rightArg= parseop_DwExpression_level_14 )+ |) )
            // Dwexpression.g:697:9: leftArg= parseop_DwExpression_level_14 ( ( () a0= '&&' rightArg= parseop_DwExpression_level_14 )+ |)
            {
            pushFollow(FOLLOW_parseop_DwExpression_level_14_in_parseop_DwExpression_level_3331);
            leftArg=parseop_DwExpression_level_14();

            state._fsp--;
            if (state.failed) return element;

            // Dwexpression.g:697:40: ( ( () a0= '&&' rightArg= parseop_DwExpression_level_14 )+ |)
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==13) ) {
                alt8=1;
            }
            else if ( (LA8_0==EOF||(LA8_0 >= 15 && LA8_0 <= 17)||LA8_0==20) ) {
                alt8=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;

            }
            switch (alt8) {
                case 1 :
                    // Dwexpression.g:697:41: ( () a0= '&&' rightArg= parseop_DwExpression_level_14 )+
                    {
                    // Dwexpression.g:697:41: ( () a0= '&&' rightArg= parseop_DwExpression_level_14 )+
                    int cnt7=0;
                    loop7:
                    do {
                        int alt7=2;
                        int LA7_0 = input.LA(1);

                        if ( (LA7_0==13) ) {
                            alt7=1;
                        }


                        switch (alt7) {
                    	case 1 :
                    	    // Dwexpression.g:698:0: () a0= '&&' rightArg= parseop_DwExpression_level_14
                    	    {
                    	    // Dwexpression.g:698:2: ()
                    	    // Dwexpression.g:698:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,13,FOLLOW_13_in_parseop_DwExpression_level_3344); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwAndExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_3_0_0_1, null, true);
                    	    copyLocalizationInfos((CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwAndExpression(), 2280, 2604);
                    	    }

                    	    pushFollow(FOLLOW_parseop_DwExpression_level_14_in_parseop_DwExpression_level_3355);
                    	    rightArg=parseop_DwExpression_level_14();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    	throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwAndExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    	if (leftArg != null) {
                    	    		Object value = leftArg;
                    	    		element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_AND_EXPRESSION__OPERAND1), value);
                    	    		completedElement(value, true);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_3_0_0_0, leftArg, true);
                    	    	copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    	throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwAndExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    	if (rightArg != null) {
                    	    		Object value = rightArg;
                    	    		element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_AND_EXPRESSION__OPERAND2), value);
                    	    		completedElement(value, true);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_3_0_0_2, rightArg, true);
                    	    	copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt7 >= 1 ) break loop7;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(7, input);
                                throw eee;
                        }
                        cnt7++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Dwexpression.g:753:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 5, parseop_DwExpression_level_3_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_DwExpression_level_3"



    // $ANTLR start "parseop_DwExpression_level_14"
    // Dwexpression.g:758:1: parseop_DwExpression_level_14 returns [de.darwinspl.expression.DwExpression element = null] : (a0= '!' arg= parseop_DwExpression_level_15 |arg= parseop_DwExpression_level_15 );
    public final de.darwinspl.expression.DwExpression parseop_DwExpression_level_14() throws RecognitionException {
        de.darwinspl.expression.DwExpression element =  null;

        int parseop_DwExpression_level_14_StartIndex = input.index();

        Token a0=null;
        de.darwinspl.expression.DwExpression arg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 6) ) { return element; }

            // Dwexpression.g:761:0: (a0= '!' arg= parseop_DwExpression_level_15 |arg= parseop_DwExpression_level_15 )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==12) ) {
                alt9=1;
            }
            else if ( (LA9_0==IDENTIFIER_TOKEN||LA9_0==QUOTED_34_34||LA9_0==14||(LA9_0 >= 18 && LA9_0 <= 19)) ) {
                alt9=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;

            }
            switch (alt9) {
                case 1 :
                    // Dwexpression.g:762:0: a0= '!' arg= parseop_DwExpression_level_15
                    {
                    a0=(Token)match(input,12,FOLLOW_12_in_parseop_DwExpression_level_14393); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwNotExpression();
                    startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_4_0_0_0, null, true);
                    copyLocalizationInfos((CommonToken)a0, element);
                    }

                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwNotExpression(), 2605, 2929);
                    }

                    pushFollow(FOLLOW_parseop_DwExpression_level_15_in_parseop_DwExpression_level_14404);
                    arg=parseop_DwExpression_level_15();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTerminateParsingException();
                    }
                    if (element == null) {
                    element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwNotExpression();
                    startIncompleteElement(element);
                    }
                    if (arg != null) {
                    if (arg != null) {
                    Object value = arg;
                    element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_NOT_EXPRESSION__OPERAND), value);
                    completedElement(value, true);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_4_0_0_1, arg, true);
                    copyLocalizationInfos(arg, element);
                    }
                    }

                    }
                    break;
                case 2 :
                    // Dwexpression.g:797:5: arg= parseop_DwExpression_level_15
                    {
                    pushFollow(FOLLOW_parseop_DwExpression_level_15_in_parseop_DwExpression_level_14414);
                    arg=parseop_DwExpression_level_15();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = arg; }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 6, parseop_DwExpression_level_14_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_DwExpression_level_14"



    // $ANTLR start "parseop_DwExpression_level_15"
    // Dwexpression.g:800:1: parseop_DwExpression_level_15 returns [de.darwinspl.expression.DwExpression element = null] : (c0= parse_de_darwinspl_expression_DwNestedExpression |c1= parse_de_darwinspl_expression_DwFeatureReferenceExpression |c2= parse_de_darwinspl_expression_DwBooleanValueExpression );
    public final de.darwinspl.expression.DwExpression parseop_DwExpression_level_15() throws RecognitionException {
        de.darwinspl.expression.DwExpression element =  null;

        int parseop_DwExpression_level_15_StartIndex = input.index();

        de.darwinspl.expression.DwNestedExpression c0 =null;

        de.darwinspl.expression.DwFeatureReferenceExpression c1 =null;

        de.darwinspl.expression.DwBooleanValueExpression c2 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 7) ) { return element; }

            // Dwexpression.g:803:0: (c0= parse_de_darwinspl_expression_DwNestedExpression |c1= parse_de_darwinspl_expression_DwFeatureReferenceExpression |c2= parse_de_darwinspl_expression_DwBooleanValueExpression )
            int alt10=3;
            switch ( input.LA(1) ) {
            case 14:
                {
                alt10=1;
                }
                break;
            case IDENTIFIER_TOKEN:
            case QUOTED_34_34:
                {
                alt10=2;
                }
                break;
            case 18:
            case 19:
                {
                alt10=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;

            }

            switch (alt10) {
                case 1 :
                    // Dwexpression.g:804:0: c0= parse_de_darwinspl_expression_DwNestedExpression
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_expression_DwNestedExpression_in_parseop_DwExpression_level_15436);
                    c0=parse_de_darwinspl_expression_DwNestedExpression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Dwexpression.g:805:2: c1= parse_de_darwinspl_expression_DwFeatureReferenceExpression
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_expression_DwFeatureReferenceExpression_in_parseop_DwExpression_level_15444);
                    c1=parse_de_darwinspl_expression_DwFeatureReferenceExpression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 3 :
                    // Dwexpression.g:806:2: c2= parse_de_darwinspl_expression_DwBooleanValueExpression
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_expression_DwBooleanValueExpression_in_parseop_DwExpression_level_15452);
                    c2=parse_de_darwinspl_expression_DwBooleanValueExpression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c2; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 7, parseop_DwExpression_level_15_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_DwExpression_level_15"



    // $ANTLR start "parse_de_darwinspl_expression_DwNestedExpression"
    // Dwexpression.g:809:1: parse_de_darwinspl_expression_DwNestedExpression returns [de.darwinspl.expression.DwNestedExpression element = null] : a0= '(' (a1_0= parse_de_darwinspl_expression_DwExpression ) a2= ')' ;
    public final de.darwinspl.expression.DwNestedExpression parse_de_darwinspl_expression_DwNestedExpression() throws RecognitionException {
        de.darwinspl.expression.DwNestedExpression element =  null;

        int parse_de_darwinspl_expression_DwNestedExpression_StartIndex = input.index();

        Token a0=null;
        Token a2=null;
        de.darwinspl.expression.DwExpression a1_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 8) ) { return element; }

            // Dwexpression.g:812:4: (a0= '(' (a1_0= parse_de_darwinspl_expression_DwExpression ) a2= ')' )
            // Dwexpression.g:813:4: a0= '(' (a1_0= parse_de_darwinspl_expression_DwExpression ) a2= ')'
            {
            a0=(Token)match(input,14,FOLLOW_14_in_parse_de_darwinspl_expression_DwNestedExpression474); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (element == null) {
            element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwNestedExpression();
            startIncompleteElement(element);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_5_0_0_0, null, true);
            copyLocalizationInfos((CommonToken)a0, element);
            }

            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwNestedExpression(), 2930, 3254);
            }

            // Dwexpression.g:827:6: (a1_0= parse_de_darwinspl_expression_DwExpression )
            // Dwexpression.g:828:6: a1_0= parse_de_darwinspl_expression_DwExpression
            {
            pushFollow(FOLLOW_parse_de_darwinspl_expression_DwExpression_in_parse_de_darwinspl_expression_DwNestedExpression487);
            a1_0=parse_de_darwinspl_expression_DwExpression();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (terminateParsing) {
            throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTerminateParsingException();
            }
            if (element == null) {
            element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwNestedExpression();
            startIncompleteElement(element);
            }
            if (a1_0 != null) {
            if (a1_0 != null) {
            	Object value = a1_0;
            	element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_NESTED_EXPRESSION__OPERAND), value);
            	completedElement(value, true);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_5_0_0_1, a1_0, true);
            copyLocalizationInfos(a1_0, element);
            }
            }

            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(null, 3255);
            }

            a2=(Token)match(input,15,FOLLOW_15_in_parse_de_darwinspl_expression_DwNestedExpression499); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (element == null) {
            element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwNestedExpression();
            startIncompleteElement(element);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_5_0_0_2, null, true);
            copyLocalizationInfos((CommonToken)a2, element);
            }

            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            // We've found the last token for this rule. The constructed EObject is now
            // complete.
            completedElement(element, true);
            addExpectedElement(null, 3256, 3260);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 8, parse_de_darwinspl_expression_DwNestedExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_expression_DwNestedExpression"



    // $ANTLR start "parse_de_darwinspl_expression_DwFeatureReferenceExpression"
    // Dwexpression.g:872:1: parse_de_darwinspl_expression_DwFeatureReferenceExpression returns [de.darwinspl.expression.DwFeatureReferenceExpression element = null] : ( (a0= QUOTED_34_34 ) | (a1= IDENTIFIER_TOKEN ) ) ;
    public final de.darwinspl.expression.DwFeatureReferenceExpression parse_de_darwinspl_expression_DwFeatureReferenceExpression() throws RecognitionException {
        de.darwinspl.expression.DwFeatureReferenceExpression element =  null;

        int parse_de_darwinspl_expression_DwFeatureReferenceExpression_StartIndex = input.index();

        Token a0=null;
        Token a1=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 9) ) { return element; }

            // Dwexpression.g:875:0: ( ( (a0= QUOTED_34_34 ) | (a1= IDENTIFIER_TOKEN ) ) )
            // Dwexpression.g:876:0: ( (a0= QUOTED_34_34 ) | (a1= IDENTIFIER_TOKEN ) )
            {
            // Dwexpression.g:876:0: ( (a0= QUOTED_34_34 ) | (a1= IDENTIFIER_TOKEN ) )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==QUOTED_34_34) ) {
                alt11=1;
            }
            else if ( (LA11_0==IDENTIFIER_TOKEN) ) {
                alt11=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;

            }
            switch (alt11) {
                case 1 :
                    // Dwexpression.g:877:0: (a0= QUOTED_34_34 )
                    {
                    // Dwexpression.g:877:4: (a0= QUOTED_34_34 )
                    // Dwexpression.g:878:4: a0= QUOTED_34_34
                    {
                    a0=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_de_darwinspl_expression_DwFeatureReferenceExpression529); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    	throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTerminateParsingException();
                    }
                    if (element == null) {
                    	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwFeatureReferenceExpression();
                    	startIncompleteElement(element);
                    }
                    if (a0 != null) {
                    	de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
                    	tokenResolver.setOptions(getOptions());
                    	de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolveResult result = getFreshTokenResolveResult();
                    	tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), result);
                    	Object resolvedObject = result.getResolvedToken();
                    	if (resolvedObject == null) {
                    		addErrorToResource(result.getErrorMessage(), ((CommonToken) a0).getLine(), ((CommonToken) a0).getCharPositionInLine(), ((CommonToken) a0).getStartIndex(), ((CommonToken) a0).getStopIndex());
                    	}
                    	String resolved = (String) resolvedObject;
                    	de.darwinspl.feature.DwFeature proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwFeature();
                    	collectHiddenTokens(element);
                    	registerContextDependentProxy(new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionContextDependentURIFragmentFactory<de.darwinspl.expression.DwFeatureReferenceExpression, de.darwinspl.feature.DwFeature>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getDwFeatureReferenceExpressionFeatureReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), resolved, proxy);
                    	if (proxy != null) {
                    		Object value = proxy;
                    		element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), value);
                    		completedElement(value, false);
                    	}
                    	collectHiddenTokens(element);
                    	retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_6_0_0_0_0_0_0, proxy, true);
                    	copyLocalizationInfos((CommonToken) a0, element);
                    	copyLocalizationInfos((CommonToken) a0, proxy);
                    }
                    }

                    }


                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(null, 3261, 3265);
                    }

                    }
                    break;
                case 2 :
                    // Dwexpression.g:918:2: (a1= IDENTIFIER_TOKEN )
                    {
                    // Dwexpression.g:918:2: (a1= IDENTIFIER_TOKEN )
                    // Dwexpression.g:919:4: a1= IDENTIFIER_TOKEN
                    {
                    a1=(Token)match(input,IDENTIFIER_TOKEN,FOLLOW_IDENTIFIER_TOKEN_in_parse_de_darwinspl_expression_DwFeatureReferenceExpression546); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    	throw new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTerminateParsingException();
                    }
                    if (element == null) {
                    	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwFeatureReferenceExpression();
                    	startIncompleteElement(element);
                    }
                    if (a1 != null) {
                    	de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER_TOKEN");
                    	tokenResolver.setOptions(getOptions());
                    	de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolveResult result = getFreshTokenResolveResult();
                    	tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), result);
                    	Object resolvedObject = result.getResolvedToken();
                    	if (resolvedObject == null) {
                    		addErrorToResource(result.getErrorMessage(), ((CommonToken) a1).getLine(), ((CommonToken) a1).getCharPositionInLine(), ((CommonToken) a1).getStartIndex(), ((CommonToken) a1).getStopIndex());
                    	}
                    	String resolved = (String) resolvedObject;
                    	de.darwinspl.feature.DwFeature proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwFeature();
                    	collectHiddenTokens(element);
                    	registerContextDependentProxy(new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionContextDependentURIFragmentFactory<de.darwinspl.expression.DwFeatureReferenceExpression, de.darwinspl.feature.DwFeature>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getDwFeatureReferenceExpressionFeatureReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), resolved, proxy);
                    	if (proxy != null) {
                    		Object value = proxy;
                    		element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), value);
                    		completedElement(value, false);
                    	}
                    	collectHiddenTokens(element);
                    	retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_6_0_0_0_0_1_0, proxy, true);
                    	copyLocalizationInfos((CommonToken) a1, element);
                    	copyLocalizationInfos((CommonToken) a1, proxy);
                    }
                    }

                    }


                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    // We've found the last token for this rule. The constructed EObject is now
                    // complete.
                    completedElement(element, true);
                    addExpectedElement(null, 3266, 3270);
                    }

                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            // We've found the last token for this rule. The constructed EObject is now
            // complete.
            completedElement(element, true);
            addExpectedElement(null, 3271, 3275);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 9, parse_de_darwinspl_expression_DwFeatureReferenceExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_expression_DwFeatureReferenceExpression"



    // $ANTLR start "parse_de_darwinspl_expression_DwBooleanValueExpression"
    // Dwexpression.g:972:1: parse_de_darwinspl_expression_DwBooleanValueExpression returns [de.darwinspl.expression.DwBooleanValueExpression element = null] : ( (a0= 'true' |a1= 'false' ) ) ;
    public final de.darwinspl.expression.DwBooleanValueExpression parse_de_darwinspl_expression_DwBooleanValueExpression() throws RecognitionException {
        de.darwinspl.expression.DwBooleanValueExpression element =  null;

        int parse_de_darwinspl_expression_DwBooleanValueExpression_StartIndex = input.index();

        Token a0=null;
        Token a1=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 10) ) { return element; }

            // Dwexpression.g:975:0: ( ( (a0= 'true' |a1= 'false' ) ) )
            // Dwexpression.g:976:0: ( (a0= 'true' |a1= 'false' ) )
            {
            // Dwexpression.g:976:0: ( (a0= 'true' |a1= 'false' ) )
            // Dwexpression.g:977:0: (a0= 'true' |a1= 'false' )
            {
            // Dwexpression.g:977:0: (a0= 'true' |a1= 'false' )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==19) ) {
                alt12=1;
            }
            else if ( (LA12_0==18) ) {
                alt12=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;

            }
            switch (alt12) {
                case 1 :
                    // Dwexpression.g:978:0: a0= 'true'
                    {
                    a0=(Token)match(input,19,FOLLOW_19_in_parse_de_darwinspl_expression_DwBooleanValueExpression583); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwBooleanValueExpression();
                    	startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_7_0_0_0, true, true);
                    copyLocalizationInfos((CommonToken)a0, element);
                    // set value of boolean attribute
                    Object value = true;
                    element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_BOOLEAN_VALUE_EXPRESSION__VALUE), value);
                    completedElement(value, false);
                    }

                    }
                    break;
                case 2 :
                    // Dwexpression.g:991:2: a1= 'false'
                    {
                    a1=(Token)match(input,18,FOLLOW_18_in_parse_de_darwinspl_expression_DwBooleanValueExpression592); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwBooleanValueExpression();
                    	startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.DWEXPRESSION_7_0_0_0, false, true);
                    copyLocalizationInfos((CommonToken)a1, element);
                    // set value of boolean attribute
                    Object value = false;
                    element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_BOOLEAN_VALUE_EXPRESSION__VALUE), value);
                    completedElement(value, false);
                    }

                    }
                    break;

            }


            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            // We've found the last token for this rule. The constructed EObject is now
            // complete.
            completedElement(element, true);
            addExpectedElement(null, 3276, 3280);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 10, parse_de_darwinspl_expression_DwBooleanValueExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_expression_DwBooleanValueExpression"



    // $ANTLR start "parse_de_darwinspl_expression_DwExpression"
    // Dwexpression.g:1016:1: parse_de_darwinspl_expression_DwExpression returns [de.darwinspl.expression.DwExpression element = null] : c= parseop_DwExpression_level_0 ;
    public final de.darwinspl.expression.DwExpression parse_de_darwinspl_expression_DwExpression() throws RecognitionException {
        de.darwinspl.expression.DwExpression element =  null;

        int parse_de_darwinspl_expression_DwExpression_StartIndex = input.index();

        de.darwinspl.expression.DwExpression c =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 11) ) { return element; }

            // Dwexpression.g:1017:3: (c= parseop_DwExpression_level_0 )
            // Dwexpression.g:1018:3: c= parseop_DwExpression_level_0
            {
            pushFollow(FOLLOW_parseop_DwExpression_level_0_in_parse_de_darwinspl_expression_DwExpression618);
            c=parseop_DwExpression_level_0();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) { element = c; /* this rule is an expression root */ }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 11, parse_de_darwinspl_expression_DwExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_expression_DwExpression"

    // Delegated rules


 

    public static final BitSet FOLLOW_parse_de_darwinspl_expression_DwExpression_in_start82 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_start89 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_DwExpression_level_1_in_parseop_DwExpression_level_0115 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_17_in_parseop_DwExpression_level_0135 = new BitSet(new long[]{0x00000000000C5220L});
    public static final BitSet FOLLOW_parseop_DwExpression_level_1_in_parseop_DwExpression_level_0152 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_parseop_DwExpression_level_2_in_parseop_DwExpression_level_1198 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_16_in_parseop_DwExpression_level_1214 = new BitSet(new long[]{0x00000000000C5220L});
    public static final BitSet FOLLOW_parseop_DwExpression_level_2_in_parseop_DwExpression_level_1228 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_parseop_DwExpression_level_3_in_parseop_DwExpression_level_2269 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_20_in_parseop_DwExpression_level_2282 = new BitSet(new long[]{0x00000000000C5220L});
    public static final BitSet FOLLOW_parseop_DwExpression_level_3_in_parseop_DwExpression_level_2293 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_parseop_DwExpression_level_14_in_parseop_DwExpression_level_3331 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_13_in_parseop_DwExpression_level_3344 = new BitSet(new long[]{0x00000000000C5220L});
    public static final BitSet FOLLOW_parseop_DwExpression_level_14_in_parseop_DwExpression_level_3355 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_12_in_parseop_DwExpression_level_14393 = new BitSet(new long[]{0x00000000000C4220L});
    public static final BitSet FOLLOW_parseop_DwExpression_level_15_in_parseop_DwExpression_level_14404 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_DwExpression_level_15_in_parseop_DwExpression_level_14414 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_expression_DwNestedExpression_in_parseop_DwExpression_level_15436 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_expression_DwFeatureReferenceExpression_in_parseop_DwExpression_level_15444 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_expression_DwBooleanValueExpression_in_parseop_DwExpression_level_15452 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_parse_de_darwinspl_expression_DwNestedExpression474 = new BitSet(new long[]{0x00000000000C5220L});
    public static final BitSet FOLLOW_parse_de_darwinspl_expression_DwExpression_in_parse_de_darwinspl_expression_DwNestedExpression487 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_parse_de_darwinspl_expression_DwNestedExpression499 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_de_darwinspl_expression_DwFeatureReferenceExpression529 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTIFIER_TOKEN_in_parse_de_darwinspl_expression_DwFeatureReferenceExpression546 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_parse_de_darwinspl_expression_DwBooleanValueExpression583 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_parse_de_darwinspl_expression_DwBooleanValueExpression592 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_DwExpression_level_0_in_parse_de_darwinspl_expression_DwExpression618 = new BitSet(new long[]{0x0000000000000002L});

}