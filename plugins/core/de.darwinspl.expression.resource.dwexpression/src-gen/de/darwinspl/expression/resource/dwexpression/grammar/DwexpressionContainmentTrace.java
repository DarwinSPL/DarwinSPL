/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.grammar;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * A DwexpressionContainmentTrace represents a specific path to a structural
 * feature by navigating over a set of a structural feature from a start class.
 * DwexpressionContainmentTraces are used during code completion to reconstruct
 * containment trees that are not created by the parser, for example, if the first
 * character of the contained object has not been typed yet.
 */
public class DwexpressionContainmentTrace {
	
	/**
	 * The class where the trace starts.
	 */
	private EClass startClass;
	
	/**
	 * The path of contained features.
	 */
	private de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionContainedFeature[] path;
	
	public DwexpressionContainmentTrace(EClass startClass, de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionContainedFeature[] path) {
		super();
		// Verify arguments
		if (startClass != null) {
			if (path.length > 0) {
				EStructuralFeature feature = path[path.length - 1].getFeature();
				if (!startClass.getEAllStructuralFeatures().contains(feature)) {
					throw new RuntimeException("Metaclass " + startClass.getName() + " must contain feature " + feature.getName());
				}
			}
		}
		this.startClass = startClass;
		this.path = path;
	}
	
	public EClass getStartClass() {
		return startClass;
	}
	
	public de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionContainedFeature[] getPath() {
		return path;
	}
	
	public String toString() {
		return (startClass == null ? "null" : startClass.getName()) + "." + de.darwinspl.expression.resource.dwexpression.util.DwexpressionStringUtil.explode(path, "->");
	}
	
	public boolean contains(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule rule) {
		if (path == null) {
			return false;
		}
		
		EClass ruleMetaclass = rule.getMetaclass();
		for (de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionContainedFeature pathElement : path) {
			EClass containerClass = pathElement.getContainerClass();
			if (containerClass == ruleMetaclass) {
				return true;
			}
		}
		
		return startClass == ruleMetaclass;
	}
	
}
