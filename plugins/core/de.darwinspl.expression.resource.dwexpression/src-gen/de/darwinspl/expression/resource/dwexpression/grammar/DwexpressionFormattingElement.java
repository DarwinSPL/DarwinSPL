/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.grammar;


public abstract class DwexpressionFormattingElement extends de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSyntaxElement {
	
	public DwexpressionFormattingElement(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality cardinality) {
		super(cardinality, null);
	}
	
}
