/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.grammar;


public class DwexpressionChoice extends de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSyntaxElement {
	
	public DwexpressionChoice(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality cardinality, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSyntaxElement... choices) {
		super(cardinality, choices);
	}
	
	public String toString() {
		return de.darwinspl.expression.resource.dwexpression.util.DwexpressionStringUtil.explode(getChildren(), "|");
	}
	
}
