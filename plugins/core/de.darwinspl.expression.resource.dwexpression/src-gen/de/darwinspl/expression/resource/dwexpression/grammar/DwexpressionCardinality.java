/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.grammar;


public enum DwexpressionCardinality {
	
	ONE, PLUS, QUESTIONMARK, STAR;
	
}
