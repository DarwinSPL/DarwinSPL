/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.mopp;

import java.util.ArrayList;
import java.util.Collection;
import org.eclipse.emf.ecore.EObject;

public class DwexpressionParseResult implements de.darwinspl.expression.resource.dwexpression.IDwexpressionParseResult {
	
	private EObject root;
	
	private de.darwinspl.expression.resource.dwexpression.IDwexpressionLocationMap locationMap;
	
	private Collection<de.darwinspl.expression.resource.dwexpression.IDwexpressionCommand<de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource>> commands = new ArrayList<de.darwinspl.expression.resource.dwexpression.IDwexpressionCommand<de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource>>();
	
	public DwexpressionParseResult() {
		super();
	}
	
	public EObject getRoot() {
		return root;
	}
	
	public de.darwinspl.expression.resource.dwexpression.IDwexpressionLocationMap getLocationMap() {
		return locationMap;
	}
	
	public void setRoot(EObject root) {
		this.root = root;
	}
	
	public void setLocationMap(de.darwinspl.expression.resource.dwexpression.IDwexpressionLocationMap locationMap) {
		this.locationMap = locationMap;
	}
	
	public Collection<de.darwinspl.expression.resource.dwexpression.IDwexpressionCommand<de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource>> getPostParseCommands() {
		return commands;
	}
	
}
