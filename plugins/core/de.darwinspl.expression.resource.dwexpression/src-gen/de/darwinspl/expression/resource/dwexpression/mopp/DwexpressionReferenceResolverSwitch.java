/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.mopp;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;

public class DwexpressionReferenceResolverSwitch implements de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceResolverSwitch {
	
	/**
	 * This map stores a copy of the options the were set for loading the resource.
	 */
	private Map<Object, Object> options;
	
	protected de.darwinspl.expression.resource.dwexpression.analysis.DwFeatureReferenceExpressionFeatureReferenceResolver dwFeatureReferenceExpressionFeatureReferenceResolver = new de.darwinspl.expression.resource.dwexpression.analysis.DwFeatureReferenceExpressionFeatureReferenceResolver();
	
	public de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceResolver<de.darwinspl.expression.DwFeatureReferenceExpression, de.darwinspl.feature.DwFeature> getDwFeatureReferenceExpressionFeatureReferenceResolver() {
		return getResolverChain(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwFeatureReferenceExpression_Feature(), dwFeatureReferenceExpressionFeatureReferenceResolver);
	}
	
	public void setOptions(Map<?, ?> options) {
		if (options != null) {
			this.options = new LinkedHashMap<Object, Object>();
			this.options.putAll(options);
		}
		dwFeatureReferenceExpressionFeatureReferenceResolver.setOptions(options);
	}
	
	public void resolveFuzzy(String identifier, EObject container, EReference reference, int position, de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceResolveResult<EObject> result) {
		if (container == null) {
			return;
		}
		if (de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwFeatureReferenceExpression().isInstance(container)) {
			DwexpressionFuzzyResolveResult<de.darwinspl.feature.DwFeature> frr = new DwexpressionFuzzyResolveResult<de.darwinspl.feature.DwFeature>(result);
			String referenceName = reference.getName();
			EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof EReference && referenceName != null && referenceName.equals("feature")) {
				dwFeatureReferenceExpressionFeatureReferenceResolver.resolve(identifier, (de.darwinspl.expression.DwFeatureReferenceExpression) container, (EReference) feature, position, true, frr);
			}
		}
	}
	
	public de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceResolver<? extends EObject, ? extends EObject> getResolver(EStructuralFeature reference) {
		if (reference == de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwFeatureReferenceExpression_Feature()) {
			return getResolverChain(reference, dwFeatureReferenceExpressionFeatureReferenceResolver);
		}
		return null;
	}
	
	@SuppressWarnings({"rawtypes", "unchecked"})
	public <ContainerType extends EObject, ReferenceType extends EObject> de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceResolver<ContainerType, ReferenceType> getResolverChain(EStructuralFeature reference, de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceResolver<ContainerType, ReferenceType> originalResolver) {
		if (options == null) {
			return originalResolver;
		}
		Object value = options.get(de.darwinspl.expression.resource.dwexpression.IDwexpressionOptions.ADDITIONAL_REFERENCE_RESOLVERS);
		if (value == null) {
			return originalResolver;
		}
		if (!(value instanceof Map)) {
			// send this to the error log
			new de.darwinspl.expression.resource.dwexpression.util.DwexpressionRuntimeUtil().logWarning("Found value with invalid type for option " + de.darwinspl.expression.resource.dwexpression.IDwexpressionOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + Map.class.getName() + ", but was " + value.getClass().getName() + ")", null);
			return originalResolver;
		}
		Map<?,?> resolverMap = (Map<?,?>) value;
		Object resolverValue = resolverMap.get(reference);
		if (resolverValue == null) {
			return originalResolver;
		}
		if (resolverValue instanceof de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceResolver) {
			de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceResolver replacingResolver = (de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceResolver) resolverValue;
			if (replacingResolver instanceof de.darwinspl.expression.resource.dwexpression.IDwexpressionDelegatingReferenceResolver) {
				// pass original resolver to the replacing one
				((de.darwinspl.expression.resource.dwexpression.IDwexpressionDelegatingReferenceResolver) replacingResolver).setDelegate(originalResolver);
			}
			return replacingResolver;
		} else if (resolverValue instanceof Collection) {
			Collection replacingResolvers = (Collection) resolverValue;
			de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceResolver replacingResolver = originalResolver;
			for (Object next : replacingResolvers) {
				if (next instanceof de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceCache) {
					de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceResolver nextResolver = (de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceResolver) next;
					if (nextResolver instanceof de.darwinspl.expression.resource.dwexpression.IDwexpressionDelegatingReferenceResolver) {
						// pass original resolver to the replacing one
						((de.darwinspl.expression.resource.dwexpression.IDwexpressionDelegatingReferenceResolver) nextResolver).setDelegate(replacingResolver);
					}
					replacingResolver = nextResolver;
				} else {
					// The collection contains a non-resolver. Send a warning to the error log.
					new de.darwinspl.expression.resource.dwexpression.util.DwexpressionRuntimeUtil().logWarning("Found value with invalid type in value map for option " + de.darwinspl.expression.resource.dwexpression.IDwexpressionOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + de.darwinspl.expression.resource.dwexpression.IDwexpressionDelegatingReferenceResolver.class.getName() + ", but was " + next.getClass().getName() + ")", null);
				}
			}
			return replacingResolver;
		} else {
			// The value for the option ADDITIONAL_REFERENCE_RESOLVERS has an unknown type.
			new de.darwinspl.expression.resource.dwexpression.util.DwexpressionRuntimeUtil().logWarning("Found value with invalid type in value map for option " + de.darwinspl.expression.resource.dwexpression.IDwexpressionOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + de.darwinspl.expression.resource.dwexpression.IDwexpressionDelegatingReferenceResolver.class.getName() + ", but was " + resolverValue.getClass().getName() + ")", null);
			return originalResolver;
		}
	}
	
}
