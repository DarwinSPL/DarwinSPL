/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression;

import java.util.Collection;
import java.util.Set;
import org.eclipse.emf.ecore.EClass;

/**
 * An element that is expected at a given position in a resource stream.
 */
public interface IDwexpressionExpectedElement {
	
	/**
	 * Returns the names of all tokens that are expected at the given position.
	 */
	public Set<String> getTokenNames();
	
	/**
	 * Returns the metaclass of the rule that contains the expected element.
	 */
	public EClass getRuleMetaclass();
	
	/**
	 * Returns the syntax element that is expected.
	 */
	public de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSyntaxElement getSyntaxElement();
	
	/**
	 * Adds an element that is a valid follower for this element.
	 */
	public void addFollower(de.darwinspl.expression.resource.dwexpression.IDwexpressionExpectedElement follower, de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionContainedFeature[] path);
	
	/**
	 * Returns all valid followers for this element. Each follower is represented by a
	 * pair of an expected elements and the containment trace that leads from the
	 * current element to the follower.
	 */
	public Collection<de.darwinspl.expression.resource.dwexpression.util.DwexpressionPair<de.darwinspl.expression.resource.dwexpression.IDwexpressionExpectedElement, de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionContainedFeature[]>> getFollowers();
	
}
