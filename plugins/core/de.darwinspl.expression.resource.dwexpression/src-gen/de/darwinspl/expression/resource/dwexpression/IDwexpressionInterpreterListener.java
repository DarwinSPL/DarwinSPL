/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression;

import org.eclipse.emf.ecore.EObject;

public interface IDwexpressionInterpreterListener {
	
	public void handleInterpreteObject(EObject element);
}
