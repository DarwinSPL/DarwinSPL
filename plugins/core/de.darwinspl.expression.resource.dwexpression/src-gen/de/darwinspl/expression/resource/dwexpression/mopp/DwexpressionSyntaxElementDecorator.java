/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.mopp;

import java.util.ArrayList;
import java.util.List;

public class DwexpressionSyntaxElementDecorator {
	
	/**
	 * the syntax element to be decorated
	 */
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSyntaxElement decoratedElement;
	
	/**
	 * an array of child decorators (one decorator per child of the decorated syntax
	 * element
	 */
	private DwexpressionSyntaxElementDecorator[] childDecorators;
	
	/**
	 * a list of the indices that must be printed
	 */
	private List<Integer> indicesToPrint = new ArrayList<Integer>();
	
	public DwexpressionSyntaxElementDecorator(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSyntaxElement decoratedElement, DwexpressionSyntaxElementDecorator[] childDecorators) {
		super();
		this.decoratedElement = decoratedElement;
		this.childDecorators = childDecorators;
	}
	
	public void addIndexToPrint(Integer index) {
		indicesToPrint.add(index);
	}
	
	public de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSyntaxElement getDecoratedElement() {
		return decoratedElement;
	}
	
	public DwexpressionSyntaxElementDecorator[] getChildDecorators() {
		return childDecorators;
	}
	
	public Integer getNextIndexToPrint() {
		if (indicesToPrint.size() == 0) {
			return null;
		}
		return indicesToPrint.remove(0);
	}
	
	public String toString() {
		return "" + getDecoratedElement();
	}
	
}
