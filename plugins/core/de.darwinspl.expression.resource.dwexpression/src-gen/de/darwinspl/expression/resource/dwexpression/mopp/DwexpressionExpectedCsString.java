/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.mopp;

import java.util.Collections;
import java.util.Set;

/**
 * A representation for a range in a document where a keyword (i.e., a static
 * string) is expected.
 */
public class DwexpressionExpectedCsString extends de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionAbstractExpectedElement {
	
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionKeyword keyword;
	
	public DwexpressionExpectedCsString(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionKeyword keyword) {
		super(keyword.getMetaclass());
		this.keyword = keyword;
	}
	
	public String getValue() {
		return keyword.getValue();
	}
	
	/**
	 * Returns the expected keyword.
	 */
	public de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSyntaxElement getSyntaxElement() {
		return keyword;
	}
	
	public Set<String> getTokenNames() {
		return Collections.singleton("'" + getValue() + "'");
	}
	
	public String toString() {
		return "CsString \"" + getValue() + "\"";
	}
	
	public boolean equals(Object o) {
		if (o instanceof DwexpressionExpectedCsString) {
			return getValue().equals(((DwexpressionExpectedCsString) o).getValue());
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return getValue().hashCode();
	}
	
}
