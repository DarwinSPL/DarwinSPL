/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.mopp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import org.eclipse.emf.common.util.URI;

/**
 * <p>
 * A basic implementation of the
 * de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceResolveResul
 * t interface that collects mappings in a list.
 * </p>
 * 
 * @param <ReferenceType> the type of the references that can be contained in this
 * result
 */
public class DwexpressionReferenceResolveResult<ReferenceType> implements de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceResolveResult<ReferenceType> {
	
	private Collection<de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceMapping<ReferenceType>> mappings;
	private String errorMessage;
	private boolean resolveFuzzy;
	private Set<de.darwinspl.expression.resource.dwexpression.IDwexpressionQuickFix> quickFixes;
	
	public DwexpressionReferenceResolveResult(boolean resolveFuzzy) {
		super();
		this.resolveFuzzy = resolveFuzzy;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public Collection<de.darwinspl.expression.resource.dwexpression.IDwexpressionQuickFix> getQuickFixes() {
		if (quickFixes == null) {
			quickFixes = new LinkedHashSet<de.darwinspl.expression.resource.dwexpression.IDwexpressionQuickFix>();
		}
		return Collections.unmodifiableSet(quickFixes);
	}
	
	public void addQuickFix(de.darwinspl.expression.resource.dwexpression.IDwexpressionQuickFix quickFix) {
		if (quickFixes == null) {
			quickFixes = new LinkedHashSet<de.darwinspl.expression.resource.dwexpression.IDwexpressionQuickFix>();
		}
		quickFixes.add(quickFix);
	}
	
	public Collection<de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceMapping<ReferenceType>> getMappings() {
		return mappings;
	}
	
	public boolean wasResolved() {
		return mappings != null;
	}
	
	public boolean wasResolvedMultiple() {
		return mappings != null && mappings.size() > 1;
	}
	
	public boolean wasResolvedUniquely() {
		return mappings != null && mappings.size() == 1;
	}
	
	public void setErrorMessage(String message) {
		errorMessage = message;
	}
	
	public void addMapping(String identifier, ReferenceType target) {
		if (!resolveFuzzy && target == null) {
			throw new IllegalArgumentException("Mapping references to null is only allowed for fuzzy resolution.");
		}
		addMapping(identifier, target, null);
	}
	
	public void addMapping(String identifier, ReferenceType target, String warning) {
		if (mappings == null) {
			mappings = new ArrayList<de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceMapping<ReferenceType>>(1);
		}
		mappings.add(new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionElementMapping<ReferenceType>(identifier, target, warning));
		errorMessage = null;
	}
	
	public void addMapping(String identifier, URI uri) {
		addMapping(identifier, uri, null);
	}
	
	public void addMapping(String identifier, URI uri, String warning) {
		if (mappings == null) {
			mappings = new ArrayList<de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceMapping<ReferenceType>>(1);
		}
		mappings.add(new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionURIMapping<ReferenceType>(identifier, uri, warning));
	}
}
