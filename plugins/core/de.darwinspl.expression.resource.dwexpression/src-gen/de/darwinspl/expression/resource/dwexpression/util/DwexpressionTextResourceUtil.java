/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.util;

import java.io.File;
import java.util.Map;
import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;

/**
 * Class DwexpressionTextResourceUtil can be used to perform common tasks on text
 * resources, such as loading and saving resources, as well as, checking them for
 * errors. This class is deprecated and has been replaced by
 * de.darwinspl.expression.resource.dwexpression.util.DwexpressionResourceUtil.
 */
public class DwexpressionTextResourceUtil {
	
	/**
	 * Use
	 * de.darwinspl.expression.resource.dwexpression.util.DwexpressionResourceUtil.getR
	 * esource() instead.
	 */
	@Deprecated
	public static de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionResource getResource(IFile file) {
		return new de.darwinspl.expression.resource.dwexpression.util.DwexpressionEclipseProxy().getResource(file);
	}
	
	/**
	 * Use
	 * de.darwinspl.expression.resource.dwexpression.util.DwexpressionResourceUtil.getR
	 * esource() instead.
	 */
	@Deprecated
	public static de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionResource getResource(File file, Map<?,?> options) {
		return de.darwinspl.expression.resource.dwexpression.util.DwexpressionResourceUtil.getResource(file, options);
	}
	
	/**
	 * Use
	 * de.darwinspl.expression.resource.dwexpression.util.DwexpressionResourceUtil.getR
	 * esource() instead.
	 */
	@Deprecated
	public static de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionResource getResource(URI uri) {
		return de.darwinspl.expression.resource.dwexpression.util.DwexpressionResourceUtil.getResource(uri);
	}
	
	/**
	 * Use
	 * de.darwinspl.expression.resource.dwexpression.util.DwexpressionResourceUtil.getR
	 * esource() instead.
	 */
	@Deprecated
	public static de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionResource getResource(URI uri, Map<?,?> options) {
		return de.darwinspl.expression.resource.dwexpression.util.DwexpressionResourceUtil.getResource(uri, options);
	}
	
}
