/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.grammar;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * A class to represent boolean terminals in a grammar.
 */
public class DwexpressionBooleanTerminal extends de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionTerminal {
	
	private String trueLiteral;
	private String falseLiteral;
	
	public DwexpressionBooleanTerminal(EStructuralFeature attribute, String trueLiteral, String falseLiteral, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality cardinality, int mandatoryOccurrencesAfter) {
		super(attribute, cardinality, mandatoryOccurrencesAfter);
		assert attribute instanceof EAttribute;
		this.trueLiteral = trueLiteral;
		this.falseLiteral = falseLiteral;
	}
	
	public String getTrueLiteral() {
		return trueLiteral;
	}
	
	public String getFalseLiteral() {
		return falseLiteral;
	}
	
	public EAttribute getAttribute() {
		return (EAttribute) getFeature();
	}
	
}
