/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.grammar;


public class DwexpressionCompound extends de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSyntaxElement {
	
	public DwexpressionCompound(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice choice, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality cardinality) {
		super(cardinality, new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSyntaxElement[] {choice});
	}
	
	public String toString() {
		return "(" + getChildren()[0] + ")";
	}
	
}
