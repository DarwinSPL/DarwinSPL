/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.grammar;

import org.eclipse.emf.ecore.EClass;

/**
 * A class to represent a rules in the grammar.
 */
public class DwexpressionRule extends de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSyntaxElement {
	
	private final EClass metaclass;
	
	public DwexpressionRule(EClass metaclass, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice choice, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality cardinality) {
		super(cardinality, new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSyntaxElement[] {choice});
		this.metaclass = metaclass;
	}
	
	public EClass getMetaclass() {
		return metaclass;
	}
	
	public de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice getDefinition() {
		return (de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice) getChildren()[0];
	}
	
	@Deprecated
	public String toString() {
		return metaclass == null ? "null" : metaclass.getName() + " ::= " + getDefinition().toString();
	}
	
}

