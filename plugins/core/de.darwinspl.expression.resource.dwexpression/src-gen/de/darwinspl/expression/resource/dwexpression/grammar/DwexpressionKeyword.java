/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.grammar;


/**
 * A class to represent a keyword in the grammar.
 */
public class DwexpressionKeyword extends de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSyntaxElement {
	
	private final String value;
	
	public DwexpressionKeyword(String value, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality cardinality) {
		super(cardinality, null);
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	public String toString() {
		return "\"" + value + "\"";
	}
	
}
