/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.launch;


/**
 * A class that handles launch configurations.
 */
public class DwexpressionLaunchConfigurationDelegate {
	// The generator for this class is currently disabled by option
	// 'disableLaunchSupport' in the .cs file.
}
