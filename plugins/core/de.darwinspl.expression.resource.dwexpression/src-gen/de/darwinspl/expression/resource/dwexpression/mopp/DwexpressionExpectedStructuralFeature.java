/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.mopp;

import java.util.Collections;
import java.util.Set;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * A representation for a range in a document where a structural feature (e.g., a
 * reference) is expected.
 */
public class DwexpressionExpectedStructuralFeature extends de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionAbstractExpectedElement {
	
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionPlaceholder placeholder;
	
	public DwexpressionExpectedStructuralFeature(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionPlaceholder placeholder) {
		super(placeholder.getMetaclass());
		this.placeholder = placeholder;
	}
	
	public EStructuralFeature getFeature() {
		return placeholder.getFeature();
	}
	
	/**
	 * Returns the expected placeholder.
	 */
	public de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSyntaxElement getSyntaxElement() {
		return placeholder;
	}
	
	public String getTokenName() {
		return placeholder.getTokenName();
	}
	
	public Set<String> getTokenNames() {
		return Collections.singleton(getTokenName());
	}
	
	public String toString() {
		return "EFeature " + getFeature().getEContainingClass().getName() + "." + getFeature().getName();
	}
	
	public boolean equals(Object o) {
		if (o instanceof DwexpressionExpectedStructuralFeature) {
			return getFeature().equals(((DwexpressionExpectedStructuralFeature) o).getFeature());
		}
		return false;
	}
	@Override
	public int hashCode() {
		return getFeature().hashCode();
	}
	
}
