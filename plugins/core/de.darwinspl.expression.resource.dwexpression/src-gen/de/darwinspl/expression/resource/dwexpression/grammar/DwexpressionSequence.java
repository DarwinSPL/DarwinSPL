/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.grammar;


public class DwexpressionSequence extends de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSyntaxElement {
	
	public DwexpressionSequence(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality cardinality, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSyntaxElement... elements) {
		super(cardinality, elements);
	}
	
	public String toString() {
		return de.darwinspl.expression.resource.dwexpression.util.DwexpressionStringUtil.explode(getChildren(), " ");
	}
	
}
