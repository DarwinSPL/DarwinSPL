/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.mopp;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.resource.Resource.Factory;

public class DwexpressionMetaInformation implements de.darwinspl.expression.resource.dwexpression.IDwexpressionMetaInformation {
	
	public String getSyntaxName() {
		return "dwexpression";
	}
	
	public String getURI() {
		return "http://www.darwinspl.de/expression/2.0";
	}
	
	public de.darwinspl.expression.resource.dwexpression.IDwexpressionTextScanner createLexer() {
		return new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionAntlrScanner(new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionLexer());
	}
	
	public de.darwinspl.expression.resource.dwexpression.IDwexpressionTextParser createParser(InputStream inputStream, String encoding) {
		return new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionParser().createInstance(inputStream, encoding);
	}
	
	public de.darwinspl.expression.resource.dwexpression.IDwexpressionTextPrinter createPrinter(OutputStream outputStream, de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource resource) {
		return new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionPrinter2(outputStream, resource);
	}
	
	public EClass[] getClassesWithSyntax() {
		return new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionSyntaxCoverageInformationProvider().getClassesWithSyntax();
	}
	
	public EClass[] getStartSymbols() {
		return new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionSyntaxCoverageInformationProvider().getStartSymbols();
	}
	
	public de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceResolverSwitch getReferenceResolverSwitch() {
		return new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionReferenceResolverSwitch();
	}
	
	public de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolverFactory getTokenResolverFactory() {
		return new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTokenResolverFactory();
	}
	
	public String getPathToCSDefinition() {
		return "de.darwinspl.expression/model/Expression.cs";
	}
	
	public String[] getTokenNames() {
		return de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionParser.tokenNames;
	}
	
	public de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenStyle getDefaultTokenStyle(String tokenName) {
		return new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTokenStyleInformationProvider().getDefaultTokenStyle(tokenName);
	}
	
	public Collection<de.darwinspl.expression.resource.dwexpression.IDwexpressionBracketPair> getBracketPairs() {
		return new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionBracketInformationProvider().getBracketPairs();
	}
	
	public EClass[] getFoldableClasses() {
		return new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionFoldingInformationProvider().getFoldableClasses();
	}
	
	public Factory createResourceFactory() {
		return new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionResourceFactory();
	}
	
	public de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionNewFileContentProvider getNewFileContentProvider() {
		return new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionNewFileContentProvider();
	}
	
	public void registerResourceFactory() {
		// if no resource factory registered, register delegator
		if (Factory.Registry.INSTANCE.getExtensionToFactoryMap().get(getSyntaxName()) == null) {
			Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(getSyntaxName(), new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionResourceFactoryDelegator());
		}
	}
	
	/**
	 * Returns the key of the option that can be used to register a preprocessor that
	 * is used as a pipe when loading resources. This key is language-specific. To
	 * register one preprocessor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getInputStreamPreprocessorProviderOptionKey() {
		return getSyntaxName() + "_" + "INPUT_STREAM_PREPROCESSOR_PROVIDER";
	}
	
	/**
	 * Returns the key of the option that can be used to register a post-processors
	 * that are invoked after loading resources. This key is language-specific. To
	 * register one post-processor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getResourcePostProcessorProviderOptionKey() {
		return getSyntaxName() + "_" + "RESOURCE_POSTPROCESSOR_PROVIDER";
	}
	
	public String getLaunchConfigurationType() {
		return "de.darwinspl.expression.resource.dwexpression.ui.launchConfigurationType";
	}
	
	public de.darwinspl.expression.resource.dwexpression.IDwexpressionNameProvider createNameProvider() {
		return new de.darwinspl.expression.resource.dwexpression.analysis.DwexpressionDefaultNameProvider();
	}
	
	public String[] getSyntaxHighlightableTokenNames() {
		de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionAntlrTokenHelper tokenHelper = new de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionAntlrTokenHelper();
		List<String> highlightableTokens = new ArrayList<String>();
		String[] parserTokenNames = getTokenNames();
		for (int i = 0; i < parserTokenNames.length; i++) {
			// If ANTLR is used we need to normalize the token names
			if (!tokenHelper.canBeUsedForSyntaxHighlighting(i)) {
				continue;
			}
			String tokenName = tokenHelper.getTokenName(parserTokenNames, i);
			if (tokenName == null) {
				continue;
			}
			highlightableTokens.add(tokenName);
		}
		highlightableTokens.add(de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionTokenStyleInformationProvider.TASK_ITEM_TOKEN_NAME);
		return highlightableTokens.toArray(new String[highlightableTokens.size()]);
	}
	
}
