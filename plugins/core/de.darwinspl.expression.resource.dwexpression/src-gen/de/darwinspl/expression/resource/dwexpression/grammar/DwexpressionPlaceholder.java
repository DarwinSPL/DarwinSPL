/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.grammar;

import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * A class to represent placeholders in a grammar.
 */
public class DwexpressionPlaceholder extends de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionTerminal {
	
	private final String tokenName;
	
	public DwexpressionPlaceholder(EStructuralFeature feature, String tokenName, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality cardinality, int mandatoryOccurencesAfter) {
		super(feature, cardinality, mandatoryOccurencesAfter);
		this.tokenName = tokenName;
	}
	
	public String getTokenName() {
		return tokenName;
	}
	
}
