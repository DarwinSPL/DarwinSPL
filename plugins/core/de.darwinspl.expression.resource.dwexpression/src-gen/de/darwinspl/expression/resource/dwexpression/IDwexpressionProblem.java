/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression;

import java.util.Collection;

public interface IDwexpressionProblem {
	public String getMessage();
	public de.darwinspl.expression.resource.dwexpression.DwexpressionEProblemSeverity getSeverity();
	public de.darwinspl.expression.resource.dwexpression.DwexpressionEProblemType getType();
	public Collection<de.darwinspl.expression.resource.dwexpression.IDwexpressionQuickFix> getQuickFixes();
}
