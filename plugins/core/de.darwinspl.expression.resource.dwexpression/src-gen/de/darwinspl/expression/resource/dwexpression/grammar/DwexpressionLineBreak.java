/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.grammar;


public class DwexpressionLineBreak extends de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionFormattingElement {
	
	private final int tabs;
	
	public DwexpressionLineBreak(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality cardinality, int tabs) {
		super(cardinality);
		this.tabs = tabs;
	}
	
	public int getTabs() {
		return tabs;
	}
	
	public String toString() {
		return "!" + getTabs();
	}
	
}
