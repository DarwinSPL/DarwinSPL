/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.grammar;

import java.lang.reflect.Field;
import java.util.LinkedHashSet;
import java.util.Set;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcoreFactory;

public class DwexpressionGrammarInformationProvider {
	
	public final static EStructuralFeature ANONYMOUS_FEATURE = EcoreFactory.eINSTANCE.createEAttribute();
	static {
		ANONYMOUS_FEATURE.setName("_");
	}
	
	public final static DwexpressionGrammarInformationProvider INSTANCE = new DwexpressionGrammarInformationProvider();
	
	private Set<String> keywords;
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment DWEXPRESSION_0_0_0_0 = INSTANCE.getDWEXPRESSION_0_0_0_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment getDWEXPRESSION_0_0_0_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwEquivalenceExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_EQUIVALENCE_EXPRESSION__OPERAND1), de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, new EClass[] {de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwExpression(), }, 0);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionKeyword DWEXPRESSION_0_0_0_1 = INSTANCE.getDWEXPRESSION_0_0_0_1();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionKeyword getDWEXPRESSION_0_0_0_1() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionKeyword("<->", de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment DWEXPRESSION_0_0_0_2 = INSTANCE.getDWEXPRESSION_0_0_0_2();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment getDWEXPRESSION_0_0_0_2() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwEquivalenceExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_EQUIVALENCE_EXPRESSION__OPERAND2), de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, new EClass[] {de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwExpression(), }, 0);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence DWEXPRESSION_0_0_0 = INSTANCE.getDWEXPRESSION_0_0_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence getDWEXPRESSION_0_0_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, DWEXPRESSION_0_0_0_0, DWEXPRESSION_0_0_0_1, DWEXPRESSION_0_0_0_2);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice DWEXPRESSION_0_0 = INSTANCE.getDWEXPRESSION_0_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice getDWEXPRESSION_0_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, DWEXPRESSION_0_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * DwEquivalenceExpression
	 */
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule DWEXPRESSION_0 = INSTANCE.getDWEXPRESSION_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule getDWEXPRESSION_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwEquivalenceExpression(), DWEXPRESSION_0_0, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment DWEXPRESSION_1_0_0_0 = INSTANCE.getDWEXPRESSION_1_0_0_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment getDWEXPRESSION_1_0_0_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwImpliesExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_IMPLIES_EXPRESSION__OPERAND1), de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, new EClass[] {de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwExpression(), }, 0);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionKeyword DWEXPRESSION_1_0_0_1 = INSTANCE.getDWEXPRESSION_1_0_0_1();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionKeyword getDWEXPRESSION_1_0_0_1() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionKeyword("->", de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment DWEXPRESSION_1_0_0_2 = INSTANCE.getDWEXPRESSION_1_0_0_2();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment getDWEXPRESSION_1_0_0_2() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwImpliesExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_IMPLIES_EXPRESSION__OPERAND2), de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, new EClass[] {de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwExpression(), }, 0);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence DWEXPRESSION_1_0_0 = INSTANCE.getDWEXPRESSION_1_0_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence getDWEXPRESSION_1_0_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, DWEXPRESSION_1_0_0_0, DWEXPRESSION_1_0_0_1, DWEXPRESSION_1_0_0_2);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice DWEXPRESSION_1_0 = INSTANCE.getDWEXPRESSION_1_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice getDWEXPRESSION_1_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, DWEXPRESSION_1_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * DwImpliesExpression
	 */
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule DWEXPRESSION_1 = INSTANCE.getDWEXPRESSION_1();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule getDWEXPRESSION_1() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwImpliesExpression(), DWEXPRESSION_1_0, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment DWEXPRESSION_2_0_0_0 = INSTANCE.getDWEXPRESSION_2_0_0_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment getDWEXPRESSION_2_0_0_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwOrExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_OR_EXPRESSION__OPERAND1), de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, new EClass[] {de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwExpression(), }, 0);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionKeyword DWEXPRESSION_2_0_0_1 = INSTANCE.getDWEXPRESSION_2_0_0_1();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionKeyword getDWEXPRESSION_2_0_0_1() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionKeyword("||", de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment DWEXPRESSION_2_0_0_2 = INSTANCE.getDWEXPRESSION_2_0_0_2();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment getDWEXPRESSION_2_0_0_2() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwOrExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_OR_EXPRESSION__OPERAND2), de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, new EClass[] {de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwExpression(), }, 0);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence DWEXPRESSION_2_0_0 = INSTANCE.getDWEXPRESSION_2_0_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence getDWEXPRESSION_2_0_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, DWEXPRESSION_2_0_0_0, DWEXPRESSION_2_0_0_1, DWEXPRESSION_2_0_0_2);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice DWEXPRESSION_2_0 = INSTANCE.getDWEXPRESSION_2_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice getDWEXPRESSION_2_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, DWEXPRESSION_2_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * DwOrExpression
	 */
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule DWEXPRESSION_2 = INSTANCE.getDWEXPRESSION_2();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule getDWEXPRESSION_2() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwOrExpression(), DWEXPRESSION_2_0, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment DWEXPRESSION_3_0_0_0 = INSTANCE.getDWEXPRESSION_3_0_0_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment getDWEXPRESSION_3_0_0_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwAndExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_AND_EXPRESSION__OPERAND1), de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, new EClass[] {de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwExpression(), }, 0);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionKeyword DWEXPRESSION_3_0_0_1 = INSTANCE.getDWEXPRESSION_3_0_0_1();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionKeyword getDWEXPRESSION_3_0_0_1() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionKeyword("&&", de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment DWEXPRESSION_3_0_0_2 = INSTANCE.getDWEXPRESSION_3_0_0_2();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment getDWEXPRESSION_3_0_0_2() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwAndExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_AND_EXPRESSION__OPERAND2), de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, new EClass[] {de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwExpression(), }, 0);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence DWEXPRESSION_3_0_0 = INSTANCE.getDWEXPRESSION_3_0_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence getDWEXPRESSION_3_0_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, DWEXPRESSION_3_0_0_0, DWEXPRESSION_3_0_0_1, DWEXPRESSION_3_0_0_2);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice DWEXPRESSION_3_0 = INSTANCE.getDWEXPRESSION_3_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice getDWEXPRESSION_3_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, DWEXPRESSION_3_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * DwAndExpression
	 */
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule DWEXPRESSION_3 = INSTANCE.getDWEXPRESSION_3();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule getDWEXPRESSION_3() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwAndExpression(), DWEXPRESSION_3_0, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionKeyword DWEXPRESSION_4_0_0_0 = INSTANCE.getDWEXPRESSION_4_0_0_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionKeyword getDWEXPRESSION_4_0_0_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionKeyword("!", de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment DWEXPRESSION_4_0_0_1 = INSTANCE.getDWEXPRESSION_4_0_0_1();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment getDWEXPRESSION_4_0_0_1() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwNotExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_NOT_EXPRESSION__OPERAND), de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, new EClass[] {de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwExpression(), }, 0);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence DWEXPRESSION_4_0_0 = INSTANCE.getDWEXPRESSION_4_0_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence getDWEXPRESSION_4_0_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, DWEXPRESSION_4_0_0_0, DWEXPRESSION_4_0_0_1);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice DWEXPRESSION_4_0 = INSTANCE.getDWEXPRESSION_4_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice getDWEXPRESSION_4_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, DWEXPRESSION_4_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * DwNotExpression
	 */
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule DWEXPRESSION_4 = INSTANCE.getDWEXPRESSION_4();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule getDWEXPRESSION_4() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwNotExpression(), DWEXPRESSION_4_0, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionKeyword DWEXPRESSION_5_0_0_0 = INSTANCE.getDWEXPRESSION_5_0_0_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionKeyword getDWEXPRESSION_5_0_0_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionKeyword("(", de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment DWEXPRESSION_5_0_0_1 = INSTANCE.getDWEXPRESSION_5_0_0_1();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment getDWEXPRESSION_5_0_0_1() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionContainment(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwNestedExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_NESTED_EXPRESSION__OPERAND), de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, new EClass[] {de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwExpression(), }, 0);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionKeyword DWEXPRESSION_5_0_0_2 = INSTANCE.getDWEXPRESSION_5_0_0_2();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionKeyword getDWEXPRESSION_5_0_0_2() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionKeyword(")", de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence DWEXPRESSION_5_0_0 = INSTANCE.getDWEXPRESSION_5_0_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence getDWEXPRESSION_5_0_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, DWEXPRESSION_5_0_0_0, DWEXPRESSION_5_0_0_1, DWEXPRESSION_5_0_0_2);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice DWEXPRESSION_5_0 = INSTANCE.getDWEXPRESSION_5_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice getDWEXPRESSION_5_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, DWEXPRESSION_5_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * DwNestedExpression
	 */
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule DWEXPRESSION_5 = INSTANCE.getDWEXPRESSION_5();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule getDWEXPRESSION_5() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwNestedExpression(), DWEXPRESSION_5_0, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionPlaceholder DWEXPRESSION_6_0_0_0_0_0_0 = INSTANCE.getDWEXPRESSION_6_0_0_0_0_0_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionPlaceholder getDWEXPRESSION_6_0_0_0_0_0_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionPlaceholder(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwFeatureReferenceExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), "QUOTED_34_34", de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence DWEXPRESSION_6_0_0_0_0_0 = INSTANCE.getDWEXPRESSION_6_0_0_0_0_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence getDWEXPRESSION_6_0_0_0_0_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, DWEXPRESSION_6_0_0_0_0_0_0);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionPlaceholder DWEXPRESSION_6_0_0_0_0_1_0 = INSTANCE.getDWEXPRESSION_6_0_0_0_0_1_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionPlaceholder getDWEXPRESSION_6_0_0_0_0_1_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionPlaceholder(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwFeatureReferenceExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), "IDENTIFIER_TOKEN", de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence DWEXPRESSION_6_0_0_0_0_1 = INSTANCE.getDWEXPRESSION_6_0_0_0_0_1();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence getDWEXPRESSION_6_0_0_0_0_1() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, DWEXPRESSION_6_0_0_0_0_1_0);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice DWEXPRESSION_6_0_0_0_0 = INSTANCE.getDWEXPRESSION_6_0_0_0_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice getDWEXPRESSION_6_0_0_0_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, DWEXPRESSION_6_0_0_0_0_0, DWEXPRESSION_6_0_0_0_0_1);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCompound DWEXPRESSION_6_0_0_0 = INSTANCE.getDWEXPRESSION_6_0_0_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCompound getDWEXPRESSION_6_0_0_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCompound(DWEXPRESSION_6_0_0_0_0, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence DWEXPRESSION_6_0_0 = INSTANCE.getDWEXPRESSION_6_0_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence getDWEXPRESSION_6_0_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, DWEXPRESSION_6_0_0_0);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice DWEXPRESSION_6_0 = INSTANCE.getDWEXPRESSION_6_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice getDWEXPRESSION_6_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, DWEXPRESSION_6_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * DwFeatureReferenceExpression
	 */
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule DWEXPRESSION_6 = INSTANCE.getDWEXPRESSION_6();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule getDWEXPRESSION_6() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwFeatureReferenceExpression(), DWEXPRESSION_6_0, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionBooleanTerminal DWEXPRESSION_7_0_0_0 = INSTANCE.getDWEXPRESSION_7_0_0_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionBooleanTerminal getDWEXPRESSION_7_0_0_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionBooleanTerminal(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwBooleanValueExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_BOOLEAN_VALUE_EXPRESSION__VALUE), "true", "false", de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence DWEXPRESSION_7_0_0 = INSTANCE.getDWEXPRESSION_7_0_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence getDWEXPRESSION_7_0_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSequence(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, DWEXPRESSION_7_0_0_0);
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice DWEXPRESSION_7_0 = INSTANCE.getDWEXPRESSION_7_0();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice getDWEXPRESSION_7_0() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionChoice(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE, DWEXPRESSION_7_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * DwBooleanValueExpression
	 */
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule DWEXPRESSION_7 = INSTANCE.getDWEXPRESSION_7();
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule getDWEXPRESSION_7() {
		return new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwBooleanValueExpression(), DWEXPRESSION_7_0, de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality.ONE);
	}
	
	
	public static String getSyntaxElementID(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSyntaxElement syntaxElement) {
		if (syntaxElement == null) {
			// null indicates EOF
			return "<EOF>";
		}
		for (Field field : de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.class.getFields()) {
			Object fieldValue;
			try {
				fieldValue = field.get(null);
				if (fieldValue == syntaxElement) {
					String id = field.getName();
					return id;
				}
			} catch (Exception e) { }
		}
		return null;
	}
	
	public static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSyntaxElement getSyntaxElementByID(String syntaxElementID) {
		try {
			return (de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSyntaxElement) de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionGrammarInformationProvider.class.getField(syntaxElementID).get(null);
		} catch (Exception e) {
			return null;
		}
	}
	
	public final static de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule[] RULES = new de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule[] {
		DWEXPRESSION_0,
		DWEXPRESSION_1,
		DWEXPRESSION_2,
		DWEXPRESSION_3,
		DWEXPRESSION_4,
		DWEXPRESSION_5,
		DWEXPRESSION_6,
		DWEXPRESSION_7,
	};
	
	/**
	 * Returns all keywords of the grammar. This includes all literals for boolean and
	 * enumeration terminals.
	 */
	public Set<String> getKeywords() {
		if (this.keywords == null) {
			this.keywords = new LinkedHashSet<String>();
			for (de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule rule : RULES) {
				findKeywords(rule, this.keywords);
			}
		}
		return keywords;
	}
	
	/**
	 * Finds all keywords in the given element and its children and adds them to the
	 * set. This includes all literals for boolean and enumeration terminals.
	 */
	private void findKeywords(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSyntaxElement element, Set<String> keywords) {
		if (element instanceof de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionKeyword) {
			keywords.add(((de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionKeyword) element).getValue());
		} else if (element instanceof de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionBooleanTerminal) {
			keywords.add(((de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionBooleanTerminal) element).getTrueLiteral());
			keywords.add(((de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionBooleanTerminal) element).getFalseLiteral());
		} else if (element instanceof de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionEnumerationTerminal) {
			de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionEnumerationTerminal terminal = (de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionEnumerationTerminal) element;
			for (String key : terminal.getLiteralMapping().keySet()) {
				keywords.add(key);
			}
		}
		for (de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSyntaxElement child : element.getChildren()) {
			findKeywords(child, this.keywords);
		}
	}
	
}
