/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.grammar;

import org.eclipse.emf.ecore.EClass;

/**
 * The abstract super class for all elements of a grammar. This class provides
 * methods to traverse the grammar rules.
 */
public abstract class DwexpressionSyntaxElement {
	
	private DwexpressionSyntaxElement[] children;
	private DwexpressionSyntaxElement parent;
	private de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality cardinality;
	
	public DwexpressionSyntaxElement(de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality cardinality, DwexpressionSyntaxElement[] children) {
		this.cardinality = cardinality;
		this.children = children;
		if (this.children != null) {
			for (DwexpressionSyntaxElement child : this.children) {
				child.setParent(this);
			}
		}
	}
	
	/**
	 * Sets the parent of this syntax element. This method must be invoked at most
	 * once.
	 */
	public void setParent(DwexpressionSyntaxElement parent) {
		assert this.parent == null;
		this.parent = parent;
	}
	
	/**
	 * Returns the parent of this syntax element. This parent is determined by the
	 * containment hierarchy in the CS model.
	 */
	public DwexpressionSyntaxElement getParent() {
		return parent;
	}
	
	/**
	 * Returns the rule of this syntax element. The rule is determined by the
	 * containment hierarchy in the CS model.
	 */
	public de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule getRule() {
		if (this instanceof de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule) {
			return (de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionRule) this;
		}
		return parent.getRule();
	}
	
	public DwexpressionSyntaxElement[] getChildren() {
		if (children == null) {
			return new DwexpressionSyntaxElement[0];
		}
		return children;
	}
	
	public EClass getMetaclass() {
		return parent.getMetaclass();
	}
	
	public de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionCardinality getCardinality() {
		return cardinality;
	}
	
	public boolean hasContainment(EClass metaclass) {
		de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSyntaxElement[] children = getChildren();
		for (de.darwinspl.expression.resource.dwexpression.grammar.DwexpressionSyntaxElement child : children) {
			if (child.hasContainment(metaclass)) {
				return true;
			}
		}
		return false;
	}
	
}
