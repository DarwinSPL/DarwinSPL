/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.mopp;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import org.eclipse.emf.ecore.EClass;

/**
 * Abstract super class for all expected elements. Provides methods to add
 * followers.
 */
public abstract class DwexpressionAbstractExpectedElement implements de.darwinspl.expression.resource.dwexpression.IDwexpressionExpectedElement {
	
	private EClass ruleMetaclass;
	
	private Set<de.darwinspl.expression.resource.dwexpression.util.DwexpressionPair<de.darwinspl.expression.resource.dwexpression.IDwexpressionExpectedElement, de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionContainedFeature[]>> followers = new LinkedHashSet<de.darwinspl.expression.resource.dwexpression.util.DwexpressionPair<de.darwinspl.expression.resource.dwexpression.IDwexpressionExpectedElement, de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionContainedFeature[]>>();
	
	public DwexpressionAbstractExpectedElement(EClass ruleMetaclass) {
		super();
		this.ruleMetaclass = ruleMetaclass;
	}
	
	public EClass getRuleMetaclass() {
		return ruleMetaclass;
	}
	
	public void addFollower(de.darwinspl.expression.resource.dwexpression.IDwexpressionExpectedElement follower, de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionContainedFeature[] path) {
		followers.add(new de.darwinspl.expression.resource.dwexpression.util.DwexpressionPair<de.darwinspl.expression.resource.dwexpression.IDwexpressionExpectedElement, de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionContainedFeature[]>(follower, path));
	}
	
	public Collection<de.darwinspl.expression.resource.dwexpression.util.DwexpressionPair<de.darwinspl.expression.resource.dwexpression.IDwexpressionExpectedElement, de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionContainedFeature[]>> getFollowers() {
		return followers;
	}
	
}
