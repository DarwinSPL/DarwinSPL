package de.darwinspl.common.eclipse.ui;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class DwDialogUtil {
	
	private static Shell shell;
	private static Set<String> onceDisplayedMessages = new HashSet<>();
	
	
	
	public static int openErrorDialog(String title, String message) {
		return openDialog(title, message, MessageDialog.ERROR, new String[] { "Okay" });
	}
	
	public static int openErrorDialogWithCopyButton(String title, String message) {
		int result = openDialog(title, message, MessageDialog.ERROR, new String[] { "Close", "Close & Copy to Clipboard" });
		if(result == 1) {
			Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			StringSelection data = new StringSelection(message);
			clipboard.setContents(data, data);
		}
		return result;
	}
	
	public static int openWarningDialog(String title, String message) {
		return openDialog(title, message, MessageDialog.WARNING, new String[] { "Okay" });
	}
	
	public static int openInfoDialog(String title, String message) {
		return openDialog(title, message, MessageDialog.INFORMATION, new String[] { "Okay" });
	}
	
	public static int openInfoDialogWithCopyButton(String title, String message) {
		int result = openDialog(title, message, MessageDialog.INFORMATION, new String[] { "Close", "Close & Copy to Clipboard" });
		if(result == 1) {
			Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			StringSelection data = new StringSelection(message);
			clipboard.setContents(data, data);
		}
		return result;
	}
	
	public static int openConfirmDialog(String title, String message) {
		return openDialog(title, message, MessageDialog.CONFIRM, new String[] { "Okay" });
	}
	
	public static int openDialog(String title, String message, int type, String ... buttonLabels) {
		if(shell == null) {
			shell = Display.getDefault().getActiveShell();
		}
		
		MessageDialog dialog = new MessageDialog(shell, title, null, message, type, buttonLabels, 0);
		return dialog.open();
	}
	
	public static int openDwDialogWithTextfield(String title, String message, String textFieldContent, int type, String ... buttonLabels) {
		if(shell == null) {
			shell = Display.getDefault().getActiveShell();
		}
		
		DwMessageDialogWithTextfield dialog = new DwMessageDialogWithTextfield(shell, title, null, message, type, buttonLabels, 0);
		dialog.setTextFieldInput(textFieldContent);
		return dialog.open();
	}
	
	public static int openDialogOnce(String dialogID, String title, String message, int type, String[] buttonLabels) {
		if(!onceDisplayedMessages.contains(dialogID)) {
			onceDisplayedMessages.add(dialogID);
			return openDialog(title, message, type, buttonLabels);
		}
		
		return -2;
	}
}
