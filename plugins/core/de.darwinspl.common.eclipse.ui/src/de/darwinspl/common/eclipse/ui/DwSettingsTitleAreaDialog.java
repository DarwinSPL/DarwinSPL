package de.darwinspl.common.eclipse.ui;

import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import de.christophseidl.util.eclipse.ui.JFaceUtil;
import de.darwinspl.common.eclipse.util.DwSettingsPreferencesManager;

public class DwSettingsTitleAreaDialog extends TitleAreaDialog {

    private Combo autoFmecCombo;
    private Combo maudeDebugCombo;
    private Text maudeAddressText;

    private boolean autoFMEC;
    private boolean maudeDebug;
    private String maudeAddress;
    
    
    
    public DwSettingsTitleAreaDialog(Shell parentShell) {
        super(parentShell);
    }
    
    
    
    @Override
    public void create() {
        super.create();
        
        ImageDescriptor imageDescriptor = JFaceUtil.getImageDescriptorFromClassBundle("icons/SettingsTitle.png", getClass());
        Image headerImage = new Image(Display.getDefault(), imageDescriptor.getImageData(100));
        
        setTitleImage(headerImage);
        getTitleImageLabel().getLayoutData();
        
        setTitle("DarwinSPL Settings");
        setMessage("Settings Page for DarwinSPL.", IMessageProvider.INFORMATION);
        this.getShell().setSize(600, 400);
    }

    @Override
    protected Control createDialogArea(Composite parent) {
        Composite area = (Composite) super.createDialogArea(parent);
        Composite container = new Composite(area, SWT.NONE);
        container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        GridLayout layout = new GridLayout(2, false);
        layout.horizontalSpacing = 50;
        layout.verticalSpacing = 20;
        layout.marginWidth = 20;
        layout.marginHeight = 20;
        container.setLayout(layout);

        createMaudeAddress(container);
        createAutoFMEC(container);
//		new Label(container, SWT.HORIZONTAL | SWT.SEPARATOR).setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false, 2, 1));
        createMaudeDebug(container);

        return area;
    }

    private void createAutoFMEC(Composite container) {
        Label label = new Label(container, SWT.NONE);
        label.setText("Automatically verify all associated FMEC modules\nupon editing a TFM (this option can cause a\nlarge increase in computation time)");

        GridData layoutData = new GridData();
        layoutData.grabExcessHorizontalSpace = true;
        layoutData.horizontalAlignment = GridData.FILL;

        autoFmecCombo = new Combo(container, SWT.BORDER | SWT.READ_ONLY);
        autoFmecCombo.setLayoutData(layoutData);
        autoFmecCombo.add("enabled");
        autoFmecCombo.add("disabled");
        
        if(DwSettingsPreferencesManager.isAutoFMEC())
        	autoFmecCombo.select(0);
        else
        	autoFmecCombo.select(1);
    }

    private void createMaudeDebug(Composite container) {
        Label label = new Label(container, SWT.NONE);
        label.setText("Create debug files for the verification of FM\nevolution plans (containing the message\nthat is sent to the verification service)");

        GridData layoutData = new GridData();
        layoutData.grabExcessHorizontalSpace = true;
        layoutData.horizontalAlignment = GridData.FILL;

        maudeDebugCombo = new Combo(container, SWT.BORDER | SWT.READ_ONLY);
        maudeDebugCombo.setLayoutData(layoutData);
        maudeDebugCombo.add("enabled");
        maudeDebugCombo.add("disabled");
        
        if(DwSettingsPreferencesManager.isMaudeDebug())
        	maudeDebugCombo.select(0);
        else
        	maudeDebugCombo.select(1);
    }

    private void createMaudeAddress(Composite container) {
        Label label = new Label(container, SWT.NONE);
        label.setText("Address of FM evolution plan verification service");

        GridData layoutData = new GridData();
        layoutData.grabExcessHorizontalSpace = true;
        layoutData.horizontalAlignment = GridData.FILL;

        maudeAddressText = new Text(container, SWT.BORDER);
        maudeAddressText.setLayoutData(layoutData);
        maudeAddressText.setMessage(DwSettingsPreferencesManager.MAUDE_ADDRESS_DEFAULT);
        maudeAddressText.setText(DwSettingsPreferencesManager.getMaudeAddress());
        
        maudeAddressText.addModifyListener(new ModifyListener(){
            // decorator for UI warning
            ControlDecoration decorator;

            /*
             * In this anonymous constructor we will initialize what needs to be initialized only once, namely the decorator.
             */
            {
                decorator = new ControlDecoration(maudeAddressText, SWT.CENTER);
                decorator.setDescriptionText("Not a valid URL with protocol and valid port, e.g., 'http://localhost:8080'");
                Image image = FieldDecorationRegistry.getDefault().getFieldDecoration(FieldDecorationRegistry.DEC_ERROR).getImage();
                decorator.setImage(image);
                decorator.hide();
            }

            @Override
            public void modifyText(ModifyEvent e) {
            	boolean urlOkay = false;
            	try {
					URL bla = new URL(maudeAddressText.getText());
					if(bla.getHost() != null && bla.getPort() != -1 && bla.getProtocol() != null)
						urlOkay = true;
				} catch (MalformedURLException e1) {
				}
            	
            	if(!urlOkay) {
                    decorator.show();
                    getButton(IDialogConstants.OK_ID).setEnabled(false);
            	}
            	else {
                    decorator.hide();
                    getButton(IDialogConstants.OK_ID).setEnabled(true);
            	}
            }
        });
    }



    @Override
    protected boolean isResizable() {
        return false;
    }

    // save content of the Text fields because they get disposed
    // as soon as the Dialog closes
    private void saveInput() {
    	autoFMEC = autoFmecCombo.getSelectionIndex() == 0;
    	maudeDebug = maudeDebugCombo.getSelectionIndex() == 0;
    	
    	if(maudeAddressText.getText() == null || maudeAddressText.getText().equals(""))
    		maudeAddress = DwSettingsPreferencesManager.MAUDE_ADDRESS_DEFAULT;
    	else
    		maudeAddress = maudeAddressText.getText();
    }

    @Override
    protected void okPressed() {
        saveInput();
        super.okPressed();
    }

    public boolean isAutoFMEC() {
        return autoFMEC;
    }

    public boolean isMaudeDebug() {
        return maudeDebug;
    }

	public String getMaudeAddress() {
		return maudeAddress;
	}
}