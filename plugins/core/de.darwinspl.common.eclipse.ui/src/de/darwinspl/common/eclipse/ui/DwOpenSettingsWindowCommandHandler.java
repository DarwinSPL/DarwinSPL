package de.darwinspl.common.eclipse.ui;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import de.darwinspl.common.eclipse.util.DwSettingsPreferencesManager;


public class DwOpenSettingsWindowCommandHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		Shell shell = window.getShell();
		
		DwSettingsTitleAreaDialog dialog = new DwSettingsTitleAreaDialog(shell);
		int result = dialog.open();

		if(result == 0) {
			DwSettingsPreferencesManager.setAutoFMEC(dialog.isAutoFMEC());
			DwSettingsPreferencesManager.setMaudeDebug(dialog.isMaudeDebug());
			DwSettingsPreferencesManager.setMaudeAddress(dialog.getMaudeAddress());
		}
		
		return null;
	}

}
