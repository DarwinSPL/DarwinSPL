package de.darwinspl.common.eclipse.ui;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.FontDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class DwMessageDialogWithTextfield extends MessageDialog {

	protected String textFieldContent;
	private Text textField;

	public DwMessageDialogWithTextfield(Shell parentShell, String dialogTitle, Image dialogTitleImage,
			String dialogMessage, int dialogImageType, String[] dialogButtonLabels, int defaultIndex) {
		
		super(parentShell, dialogTitle, dialogTitleImage, dialogMessage, dialogImageType, dialogButtonLabels, defaultIndex);
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		
		Label textFieldLabel = new Label(container, SWT.NONE);
		textFieldLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		FontDescriptor boldDescriptor = FontDescriptor.createFrom(textFieldLabel.getFont()).setStyle(SWT.BOLD);
		Font boldFont = boldDescriptor.createFont(textFieldLabel.getDisplay());
		textFieldLabel.setFont( boldFont );
		textFieldLabel.setText("Further Information:");
		
		ScrolledComposite scrolledComposite = new ScrolledComposite(container, SWT.H_SCROLL | SWT.V_SCROLL);
		scrolledComposite.setLayoutData(new GridData(SWT.CENTER, SWT.TOP, true, true, 2, 1));
		scrolledComposite.setLayout(new GridLayout());
		scrolledComposite.setExpandHorizontal(true);
		
		textField = new Text(scrolledComposite, SWT.WRAP);
		textField.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		textField.setText(this.textFieldContent);
		textField.setEditable(false);
		textField.pack();

		scrolledComposite.setContent(textField);

		Shell parentShell = (Shell) parent;
		parentShell.setSize(550, 500);
		
	    Display display = parentShell.getDisplay();
	    
	    Monitor primary = display.getPrimaryMonitor();
	    Rectangle bounds = primary.getBounds();
	    Rectangle rect = parentShell.getBounds();
	    
	    int x = bounds.x + (bounds.width - rect.width) / 2;
	    int y = bounds.y + (bounds.height - rect.height) / 2;
	    
	    parentShell.setLocation(x, y);
		
		return container;
	}

	public void setTextFieldInput(String textFieldContent) {
		this.textFieldContent = textFieldContent;
	}

}
