package de.darwinspl.temporal.maude.analysis;

public interface DwEvolutionCheckerResultHook {
	
	public boolean isResultValid(DwEvolutionCheckingResult result);
	
}
