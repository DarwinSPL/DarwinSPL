package de.darwinspl.temporal.maude.translation;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.evolution.operation.resource.DwEvolutionOperationUtil;
import de.darwinspl.feature.operation.DwEvolutionOperation;
import de.darwinspl.feature.operation.DwEvolutionOperationModel;
import de.darwinspl.feature.operation.DwFeatureAddOperation;
import de.darwinspl.feature.operation.DwFeatureCreateOperation;
import de.darwinspl.feature.operation.DwFeatureDeleteOperation;
import de.darwinspl.feature.operation.DwFeatureDetachOperation;
import de.darwinspl.feature.operation.DwFeatureMoveOperation;
import de.darwinspl.feature.operation.DwFeatureRenameOperation;
import de.darwinspl.feature.operation.DwFeatureTypeChangeOperation;
import de.darwinspl.feature.operation.DwGroupAddOperation;
import de.darwinspl.feature.operation.DwGroupCreateOperation;
import de.darwinspl.feature.operation.DwGroupDeleteOperation;
import de.darwinspl.feature.operation.DwGroupDetachOperation;
import de.darwinspl.feature.operation.DwGroupMoveOperation;
import de.darwinspl.feature.operation.DwGroupTypeChangeOperation;
import de.darwinspl.feature.util.custom.DwFeatureUtil;
import de.darwinspl.temporal.DwTemporalElement;

public class DwOperationsToMaudeTranslator {
	
	private Map<String, String> initialFeatureNameMap;
	
	

	public String translateOperations(DwTemporalFeatureModel tfm, DwEvolutionOperationModel operationsModel, List<Date> allDates) {
		initialFeatureNameMap = new HashMap<>();
		
		// Create a map of dates to lists of operations.
		Map<Date, List<DwEvolutionOperation>> dateToSortedOperationMap = DwEvolutionOperationUtil.getSortedOperationListsForEachDate(operationsModel, allDates);

//		System.out.println("Translating " + operationsModel.getEvolutionOperations().size() + " operations...");
		
		StringBuilder result = new StringBuilder();
		result.append(System.lineSeparator() + "eq plan = ");
		List<String> planSteps = new ArrayList<>();
		
		for(int i=0; i<allDates.size(); i++) {
			String planStep = "at " + (i+1)*2 + " do" + System.lineSeparator();
			
			List<DwEvolutionOperation> orderedOperationsForCurrentDate = dateToSortedOperationMap.get(allDates.get(i));
			for(DwEvolutionOperation operation : orderedOperationsForCurrentDate) {
				String operationTranslation = translate(operation);
				if(operationTranslation != null)
					planStep += operationTranslation + System.lineSeparator();
			}
			
			planStep += ";";
			planSteps.add(planStep);
		}
		
		result.append(String.join(System.lineSeparator() + ";;" + System.lineSeparator() + System.lineSeparator(), planSteps));
		result.append(" .");
		
//		System.out.println("Translation done.");
		return result.toString();
	}
	
	

	private String translate(DwEvolutionOperation operation) {
		if(operation == null)
			throw new UnsupportedOperationException("Given operation was null");
		
		// Feature Operations
		
		if (operation instanceof DwFeatureCreateOperation)
			return doTranslate((DwFeatureCreateOperation) operation);

		else if (operation instanceof DwFeatureAddOperation)
			return null;

		else if (operation instanceof DwFeatureDeleteOperation)
			return doTranslate((DwFeatureDeleteOperation) operation);

		else if (operation instanceof DwFeatureDetachOperation)
			return null;

		else if (operation instanceof DwFeatureMoveOperation)
			return doTranslate((DwFeatureMoveOperation) operation);

		else if (operation instanceof DwFeatureRenameOperation)
			return doTranslate((DwFeatureRenameOperation) operation);

		else if (operation instanceof DwFeatureTypeChangeOperation)
			return doTranslate((DwFeatureTypeChangeOperation) operation);
		
		// Group Operations
		
		else if (operation instanceof DwGroupCreateOperation)
			return doTranslate((DwGroupCreateOperation) operation);

		else if (operation instanceof DwGroupAddOperation)
			return null;

		else if (operation instanceof DwGroupDeleteOperation)
			return doTranslate((DwGroupDeleteOperation) operation);

		else if (operation instanceof DwGroupDetachOperation)
			return null;

		else if (operation instanceof DwGroupTypeChangeOperation)
			return doTranslate((DwGroupTypeChangeOperation) operation);

		else if (operation instanceof DwGroupMoveOperation)
			return doTranslate((DwGroupMoveOperation) operation);
		
		// Operation was not recognized
		throw new UnsupportedOperationException("No translation for operation type " + operation.getClass().getSimpleName());
	}
	
	
	
	/*
	    eq plan = at 1 do
        addGroup(infotainment, "Group1", XOR)                              --- T1
        addFeature(auto, "Android Auto", "Group1", optional)               --- T1
        addFeature(carPlay, "Apple Car Play", "Group1", optional)     ; ;; --- T1

        at 2 do
        addFeature(assistance, "Comfort Systems", "car1", optional)        --- T2
        addGroup(assistance, "comfort1", AND)                              --- T2
        addFeature(pilot, "Parking Pilot", "comfort1", optional)      ; ;; --- T2

        at 5 do
        changeGroupType("Group1", OR)                                      --- T5
        addGroup(pilot, "pilot1", AND)                                     --- T5
        addFeature(distance, "Distance Sensors", "pilot1", mandatory) ; ;; --- T5

        at 6 do
        addFeature(sensors, "Sensors", "car1", optional)                   --- T6
        addGroup(sensors, "sensors1", AND)                                 --- T6 (implicit)
        moveFeature(distance, "sensors1")                                  --- T6
        addFeature(brake, "Emergency Brake", "comfort1", optional)         --- T6
        renameFeature(assistance, "Assistance Systems")               ; ;; --- T6

        at 7 do
        removeFeature(distance)                                            --- T7 |
        addFeature(frontSensors, "Front Sensors", "sensors1", optional)    --- T7 | Split Sensors feature
        addFeature(rearSensors, "Rear Sensors", "sensors1", optional)      --- T7 |
        removeFeature(bluetooth)                                      ;    --- T7
        .
	 */
	
	

	/**
	 * Adds the feature with id newFid, name new-
	 * Name, feature type fType to the group target-
	 * Group
	 * @param operation will be translated to Maude
	 * @return translated Maude expression
	 */
	private String doTranslate(DwFeatureCreateOperation operation) {
		String additionalOperations = "";
		
		DwFeatureAddOperation featureAddOperation = operation.getFeatureAddOperation();
		DwGroupCreateOperation groupCreateOperation = featureAddOperation.getGroupCreateOperation();
		if(groupCreateOperation != null)
			additionalOperations = translate(groupCreateOperation) + System.lineSeparator();
		
		Date date = operation.getOperationDate();
		DwFeature feature = operation.getFeature();
		
		String newFid = getIdString(feature);
		String newName = "\"" + DwFeatureUtil.getName(feature, date).getName() + "\"";
//		String fType = DwFeatureUtil.getType(feature, date).getType().toString();
		String fType = "OPTIONAL";
		String targetGroup = getIdString(featureAddOperation.getGroupToAddTo());
		
		initialFeatureNameMap.put(newFid, newName);

		// addFeature(newFid, newName, targetGroup, fType)
		return additionalOperations + "addFeature(" + newFid + ", " + newName + ", " + targetGroup + ", " + fType + ")";
	}
	
	/**
	 * Removes the feature with ID featureID from the
	 * feature table
	 * @param operation will be translated to Maude
	 * @return translated Maude expression
	 */
	private String doTranslate(DwFeatureDeleteOperation operation) {
		String additionalPreOperations = "";
		for(DwGroupDeleteOperation consequentOperation : operation.getConsequentGroupOperations()) {
			additionalPreOperations += doTranslate(consequentOperation) + System.lineSeparator();
		}

		String additionalPostOperations = "";
		DwGroupDeleteOperation consequentOperation = operation.getDetachOperation().getConsequentGroupOperation();
		if(consequentOperation != null) {
			additionalPostOperations += System.lineSeparator() + doTranslate(consequentOperation);
		}

		String featureID = getIdString(operation.getFeature());
		
		// removeFeature(featureID)
		return additionalPreOperations + "removeFeature(" + featureID + ")" + additionalPostOperations;
	}
	
	/**
	 * Moves the feature with ID featureID to the group
	 * with ID newGroupID
	 * @param operation will be translated to Maude
	 * @return translated Maude expression
	 */
	private String doTranslate(DwFeatureMoveOperation operation) {
		DwFeatureAddOperation featureAddOperation = operation.getAddOperation();
		
		String additionalPreOperations = "";
		DwFeatureTypeChangeOperation consequentFeatureTypeChangeOperation = featureAddOperation.getConsequentFeatureTypeChangeOperation();
		if(consequentFeatureTypeChangeOperation != null)
			additionalPreOperations = translate(consequentFeatureTypeChangeOperation) + System.lineSeparator();
		
		DwGroupCreateOperation groupCreateOperation = featureAddOperation.getGroupCreateOperation();
		if(groupCreateOperation != null)
			additionalPreOperations += translate(groupCreateOperation) + System.lineSeparator();

		String additionalPostOperations = "";
		DwFeatureDetachOperation featureDetachOperation = operation.getDetachOperation();
		DwGroupDeleteOperation consequentGroupDeleteOperation = featureDetachOperation.getConsequentGroupOperation();
		if(consequentGroupDeleteOperation != null)
			additionalPostOperations = System.lineSeparator() + translate(consequentGroupDeleteOperation);
		
		DwFeature feature = operation.getFeature();
		
		String featureID = getIdString(feature);
		String newGroupID = getIdString(operation.getAddOperation().getGroupToAddTo());
		
		// moveFeature(featureID, newGroupID)
		return additionalPreOperations + "moveFeature(" + featureID + ", " + newGroupID + ")" + additionalPostOperations;
	}
	
	/**
	 * Changes name held in the feature with ID
	 * featureID to newName
	 * @param operation will be translated to Maude
	 * @return translated Maude expression
	 */
	private String doTranslate(DwFeatureRenameOperation operation) {
		DwFeature feature = operation.getFeature();
		
		String featureID = getIdString(feature);
		String newName = putInQuotes(operation.getNewNameString());
		
		String initialFeatureName = initialFeatureNameMap.get(featureID);
		if(newName.equals(initialFeatureName))
			return null;
		
		// renameFeature(featureID, newName)
		return "renameFeature(" + featureID + ", " + newName + ")";
	}



	/**
	 * Changes the feature variation type of the feature
	 * with ID featureID to newFeatureType
	 * @param operation will be translated to Maude
	 * @return translated Maude expression
	 */
	private String doTranslate(DwFeatureTypeChangeOperation operation) {
		DwFeature feature = operation.getFeature();
		
		String featureID = getIdString(feature);
		String newFeatureType = operation.getNewFeatureTypeEnum().toString();
		
		// changeFeatureType(featureID, newFeatureType)
		return "changeFeatureType(" + featureID + ", " + newFeatureType + ")";
	}
	

	
	/**
	 * Adds group (groupID, groupType, ;) to the
	 * list of groups in the feature with ID parentFeatureID.
	 * @param operation will be translated to Maude
	 * @return translated Maude expression
	 */
	private String doTranslate(DwGroupCreateOperation operation) {
//		Date date = operation.getOperationDate();
		DwGroup group = operation.getGroup();
		
//		String parentFeatureID = getIdString(DwFeatureUtil.getParentFeatureOfGroup(group, date));
		String parentFeatureID = getIdString(operation.getGroupAddOperation().getParentFeature());
		String groupID = getIdString(group);
//		String groupType = DwFeatureUtil.getType(group, date).getType().toString();
		String groupType = "AND";
		
		// addGroup(parentFeatureID, groupID, groupType)
		return "addGroup(" + parentFeatureID + ", " + groupID + ", " + groupType + ")";
	}
	
	/**
	 * Removes the group with ID groupID from the
	 * feature table
	 * @param operation will be translated to Maude
	 * @return translated Maude expression
	 */
	private String doTranslate(DwGroupDeleteOperation operation) {
		String additionalOperations = "";
		
//		List<DwFeature> childFeatures = DwFeatureUtil.getFeaturesOfGroup(operation.getGroup(), operation.getOperationDate());
//		for(DwFeature childFeature : childFeatures) {
//			DwFeatureDeleteOperation tempDeleteOperation = operationsFactory.createDwFeatureDeleteOperation(childFeature, operation.getOperationDate(), !operation.isIntermediateOperation());
//			additionalOperations += translate(tempDeleteOperation) + System.lineSeparator();
//		}
		
		for(DwFeatureDeleteOperation consequentOperation : operation.getConsequentFeatureOperations()) {
			additionalOperations += doTranslate(consequentOperation) + System.lineSeparator();
		}
		
		String groupID = getIdString(operation.getGroup());
		
		// removeGroup(groupID)
		return additionalOperations + "removeGroup(" + groupID + ")";
	}
	
	/**
	 * Changes the group variation type of the group
	 * with ID groupID to groupType
	 * @param operation will be translated to Maude
	 * @return translated Maude expression
	 */
	private String doTranslate(DwGroupTypeChangeOperation operation) {
		String additionalOperations = "";
		for(DwEvolutionOperation consequentOperation : operation.getConsequentOperations())
			additionalOperations += translate(consequentOperation) + System.lineSeparator();
		
		String groupID = getIdString(operation.getGroup());
		String groupType = operation.getNewGroupTypeEnum().toString();
		
		// changeGroupType(groupID, groupType)
		return additionalOperations + "changeGroupType(" + groupID + ", " + groupType + ")";
	}
	
	/**
	 * Moves the group with ID groupID to the list of
	 * groups in the feature with ID newParentFid
	 * @param operation will be translated to Maude
	 * @return translated Maude expression
	 */
	private String doTranslate(DwGroupMoveOperation operation) {
		String groupID = getIdString(operation.getGroup());
		String newParentFid = getIdString(operation.getGroupAddOperation().getParentFeature());
		
		// moveGroup(groupID, newParentFid)
		return "moveGroup(" + groupID + ", " + newParentFid + ")";
	}
	

	
	private String getIdString(DwTemporalElement element) {
		return "\"" + element.getId() + "\"";
	}
	
	private String putInQuotes(String nameString) {
//		return "\"" + nameString + "@" + (new Object()).hashCode() + "\"";
		return "\"" + nameString + "\"";
	}
	
	private String putInQuotesPlusHashCode(String nameString) {
		return "\"" + nameString + "@" + (new Object()).hashCode() + "\"";
	}

}









