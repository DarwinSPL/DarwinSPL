package de.darwinspl.temporal.maude.translation;

import java.util.Date;
import java.util.List;

import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.evolution.operation.resource.DwEvolutionOperationModelResourceUtil;
import de.darwinspl.feature.evolution.operation.resource.DwEvolutionOperationUtil;
import de.darwinspl.feature.operation.DwEvolutionOperationModel;
import de.darwinspl.fmec.FeatureModelEvolutionConstraintModel;
import de.darwinspl.temporal.util.DwDateResolverUtil;

public class DwToMaudeTranslator {
	
	public static final String MAUDE_INPUT_NAME = "DWINPUT";
	
	private FeatureModelEvolutionConstraintModel forceFmecModel;


	public void forceFmecModuleCheck(FeatureModelEvolutionConstraintModel fmecModel) {
		forceFmecModel = fmecModel;
	}
	
	
	public String translate(DwTemporalFeatureModel tfm, boolean translateFMECs) {
		if(tfm.getRootFeatures().size() != 1) {
			// Problem! We assume there is only one root feature...
			throw new RuntimeException("Problem! We assume there is exactly one root feature");
		}
		
		String result =
				"load featuremodel-checker.maude" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"mod " + MAUDE_INPUT_NAME + " is including FEATUREMODEL-CHECKER . protecting STRING + NAT ." + System.lineSeparator() + 
				"  subsorts String < FeatureID GroupID Name ." + System.lineSeparator() + 
				"  subsort Nat < TimePoint ." + System.lineSeparator() + 
				"  op model : -> FeatureModel ." + System.lineSeparator() + 
				"  op plan : -> Plans ." + System.lineSeparator() + 
				"  op log : -> Log ." + System.lineSeparator() + 
				"  op init : -> TimeConfiguration ." + System.lineSeparator() + 
				"  eq init = currentTime: 0 C: model # done # log plan: plan ." + System.lineSeparator();

		DwEvolutionOperationModel operationsModel = DwEvolutionOperationModelResourceUtil.loadAndCacheOperationsModel(tfm);
		if(operationsModel == null)
			return null;
		
		List<Date> allDates = DwEvolutionOperationUtil.collectDates(operationsModel);
		if(allDates.size() == 0)
			return null;
		
		DwTemporalFeatureModelToMaudeTranslator tfmTranslator = new DwTemporalFeatureModelToMaudeTranslator(translateFMECs);
		result += tfmTranslator.translateFeatureModel(tfm, DwDateResolverUtil.INITIAL_STATE_DATE);
		
		DwOperationsToMaudeTranslator operationsTranslator = new DwOperationsToMaudeTranslator();
		String translatedOperations = operationsTranslator.translateOperations(tfm, operationsModel, allDates);
		if(translatedOperations == null)
			return null;
		result += translatedOperations;
		
		result += System.lineSeparator() + System.lineSeparator() + "endm" + System.lineSeparator();
		
		// (FMEC) model checking part begins
		if(translateFMECs) {
			DwFMECToMaudeTranslator constraintsTranslator = new DwFMECToMaudeTranslator(allDates);
			String translatedConstraints;
			
			if(forceFmecModel == null) {
				translatedConstraints = constraintsTranslator.translateConstraints(tfm);
			}
			else {
				translatedConstraints = constraintsTranslator.translateFMECModel(forceFmecModel);
				forceFmecModel = null;
			}
			
			if(translatedConstraints != null)
				result += System.lineSeparator() + System.lineSeparator() + translatedConstraints;
			else
				result += System.lineSeparator() + "rew init .";
		}
		else {
			result += System.lineSeparator() + "rew init .";
		}
		
		result += System.lineSeparator() + "quit" + System.lineSeparator();
		
		return result;
	}

}


