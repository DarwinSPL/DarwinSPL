package de.darwinspl.temporal.maude.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import de.darwinspl.common.eclipse.util.DwSettingsPreferencesManager;
import de.darwinspl.temporal.util.DwDateResolverUtil;

public class WebClient {
	// For how long is the server supposed to wait for an answer from the Maude process?
	// (in milliseconds)
	private static final long MAUDE_DEFAULT_TIMEOUT = 20000L;
	private static final String USER_AGENT = "DarwinSPL/WebClient";
	
	

	public String sendRequestAndWaitForReply(String msg) throws IOException {
		URL obj = new URL(DwSettingsPreferencesManager.getMaudeAddress());
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Maude-Timout", String.valueOf(MAUDE_DEFAULT_TIMEOUT));

		// Write Request Body for POST Method
		con.setDoOutput(true);
		OutputStream os = con.getOutputStream();
		os.write(msg.getBytes());
		os.flush();
		os.close();

		con.setConnectTimeout((int) 2000);
		con.setReadTimeout((int) MAUDE_DEFAULT_TIMEOUT);

		long startTime = System.currentTimeMillis();
		int responseCode = con.getResponseCode();
		long timeDiff = System.currentTimeMillis() - startTime;
		
		if (responseCode == HttpURLConnection.HTTP_OK) {
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			
			StringBuffer response = new StringBuffer();
			response.append("## Response Code: " + responseCode + System.lineSeparator());
			response.append("## Online calculation time: " + DwDateResolverUtil.getTimeAsString(timeDiff, true) + System.lineSeparator());

			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine + System.lineSeparator());
			}
			in.close();
			
			response.append(System.lineSeparator() + System.lineSeparator());
			return response.toString();
		} else {
			throw new IOException(responseCode + System.lineSeparator() + "## Calling the web service failed...");
		}
	}

}
