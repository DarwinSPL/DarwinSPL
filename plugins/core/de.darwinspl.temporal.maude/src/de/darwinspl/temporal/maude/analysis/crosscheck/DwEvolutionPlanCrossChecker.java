package de.darwinspl.temporal.maude.analysis.crosscheck;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureType;
import de.darwinspl.feature.DwFeatureTypeEnum;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupComposition;
import de.darwinspl.feature.DwGroupType;
import de.darwinspl.feature.DwGroupTypeEnum;
import de.darwinspl.feature.DwName;
import de.darwinspl.feature.DwParentFeatureChildGroupContainer;
import de.darwinspl.feature.DwRootFeatureContainer;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.evolution.operation.factory.DwSimpleEvolutionOperationFactory;
import de.darwinspl.feature.evolution.operation.interpreter.DwEvolutionOperationInterpreter;
import de.darwinspl.feature.evolution.operation.resource.DwEvolutionOperationModelResourceUtil;
import de.darwinspl.feature.evolution.operation.resource.DwEvolutionOperationUtil;
import de.darwinspl.feature.operation.DwEvolutionOperation;
import de.darwinspl.feature.operation.DwEvolutionOperationModel;
import de.darwinspl.feature.operation.DwFeatureAddOperation;
import de.darwinspl.feature.operation.DwFeatureCreateOperation;
import de.darwinspl.feature.operation.DwFeatureDeleteOperation;
import de.darwinspl.feature.operation.DwFeatureMoveOperation;
import de.darwinspl.feature.operation.DwFeatureOperation;
import de.darwinspl.feature.operation.DwFeatureRenameOperation;
import de.darwinspl.feature.operation.DwFeatureTypeChangeOperation;
import de.darwinspl.feature.operation.DwGroupAddOperation;
import de.darwinspl.feature.operation.DwGroupDeleteOperation;
import de.darwinspl.feature.operation.DwGroupMoveOperation;
import de.darwinspl.feature.operation.DwGroupOperation;
import de.darwinspl.feature.operation.DwGroupTypeChangeOperation;
import de.darwinspl.feature.util.custom.DwFeatureUtil;
import de.darwinspl.temporal.DwTemporalElement;
import de.darwinspl.temporal.util.DwDateResolverUtil;
import de.darwinspl.temporal.util.DwEvolutionUtil;

public class DwEvolutionPlanCrossChecker {

	private DwTemporalFeatureModel copiedTFM;
	private DwSimpleEvolutionOperationFactory operationFactory;
	
	private List<DwTemporalElement> deletedElements;
	private Set<String> currentlyPickedNames;
	
	

	public DwEvolutionPlanCrossChecker() {
		operationFactory = new DwSimpleEvolutionOperationFactory();
	}
	
	
	
	public String run(DwEvolutionOperation operationToCheck) {
		String result;
		String info;
		
		long startTime = System.currentTimeMillis();
		
		try {
			if(operationToCheck != null)
				doRun(operationToCheck);
			result = "SUCCESS" + System.lineSeparator();
			info = "";
		} catch (DwCrossCheckingException e) {
			result = "FAILED!" + System.lineSeparator();
			info =  System.lineSeparator() + e.getMessage();
		}
		
		long timeDiff = System.currentTimeMillis() - startTime;
		String timeString = "Calculation time: " + timeDiff + "ms";
		
		return result + timeString + info;
	}
	
	
	
	private void doRun(DwEvolutionOperation operationToCheck) throws DwCrossCheckingException {
		if(true)
			return;
		
		DwTemporalFeatureModel originalTFM = operationToCheck.getFeatureModel();
		DwEvolutionOperationModel operationModel = DwEvolutionOperationModelResourceUtil.loadAndCacheOperationsModel(originalTFM);
		
		Map<Date, List<DwEvolutionOperation>> dateToOperationListMap = DwEvolutionOperationUtil.getSortedOperationListsForEachDate(operationModel);
//		dateToOperationListMap.get(operationToCheck.getOperationDate()).add(operationToCheck);
		
		copiedTFM = EcoreUtil.copy(originalTFM);
		DwEvolutionOperation equivalentOperation = getEquivalentOperationOnCopiedTFM(operationToCheck);
		DwEvolutionOperationInterpreter.executeWithoutAnalysis(equivalentOperation);
		
		currentlyPickedNames = new HashSet<>();
		for(DwName initialName : DwFeatureUtil.getAllFeatureNames(copiedTFM, DwDateResolverUtil.INITIAL_STATE_DATE)) {
			currentlyPickedNames.add(initialName.getName());
		}

//		List<DwTemporalElement> mustNotBeDeleted = getEffectlessOperationParadoxCriticalElements(operationToCheck);
		List<DwTemporalElement> mustNotBeDeleted = null;
		deletedElements = new LinkedList<>();
		
		for(Date date : DwEvolutionOperationUtil.collectDates(operationModel)) {
			List<DwEvolutionOperation> operations = dateToOperationListMap.get(date);
			for(DwEvolutionOperation operation : operations) {
				checkOperation(operation, mustNotBeDeleted, date);
			}
		}
		
//		tempTFM = EcoreUtil.copy(originalTFM);
//		DwEvolutionOperation equivalentOperation = getEquivalentOperationOnCopiedTFM(operationToCheck);
//		DwEvolutionOperationInterpreter.executeWithoutAnalysis(equivalentOperation);
		
		for(Date date : DwEvolutionUtil.collectDates(copiedTFM)) {
//			checkDoubleFeatureNames(date);
			
			List<DwRootFeatureContainer> validRootFeatureContainers = DwEvolutionUtil.getValidTemporalElements(copiedTFM.getRootFeatures(), date);
			
			// Rule 1: the feature model has exactly one root feature.
			{
				if(validRootFeatureContainers.size() != 1)
					throw new DwCrossCheckingException("Violation of Wellformedness Rule 1: the feature model has exactly one root feature.");
			}
			
			DwFeature rootFeature = validRootFeatureContainers.get(0).getFeature();
			
			// Rule 2: the root feature must be mandatory.
			{
				if(!DwFeatureUtil.getType(rootFeature, date).getType().equals(DwFeatureTypeEnum.MANDATORY))
					throw new DwCrossCheckingException("Violation of Wellformedness Rule 2: the root feature must be mandatory.");
			}
			
			// Rule 5: each feature, except for the root feature, must be part of exactly one group.
			{
				List<DwGroupComposition> validParentRelation = DwEvolutionUtil.getValidTemporalElements(rootFeature.getGroupMembership(), date);
				if(validParentRelation.size() != 0)
					throw new DwCrossCheckingException("Violation of Wellformedness Rule 5: each feature, except for the root feature, must be part of exactly one group (feature: root feature).");
			}

			for(DwGroup childGroup : DwFeatureUtil.getChildGroupsOfFeature(rootFeature, date)) {
				checkGroup(childGroup, date);
			}
		}
	}
	
	
	
	private List<DwTemporalElement> getEffectlessOperationParadoxCriticalElements(DwEvolutionOperation operation) {
		DwTemporalElement leafElement;
		
		if(operation instanceof DwFeatureCreateOperation) {
			leafElement = ((DwFeatureCreateOperation) operation).getFeatureAddOperation().getGroupToAddTo();
		}
		else if(operation instanceof DwFeatureMoveOperation) {
			leafElement = ((DwFeatureMoveOperation) operation).getAddOperation().getGroupToAddTo();
		}
		else if(operation instanceof DwGroupMoveOperation) {
			leafElement = ((DwGroupMoveOperation) operation).getGroupAddOperation().getParentFeature();
		}
		else {
			return null;
		}
		
		List<DwTemporalElement> mustNotBeDeleted = new LinkedList<>();
		getFeaturesAndGroupsOfBranchUpToRoot(mustNotBeDeleted, leafElement, operation.getOperationDate());
		return mustNotBeDeleted;
	}
	
	

	private void getFeaturesAndGroupsOfBranchUpToRoot(List<DwTemporalElement> mustNotBeDeleted, DwTemporalElement element, Date date) {
		if(element == null)
			return;
		
		mustNotBeDeleted.add(element);
		
		if(element instanceof DwFeature) {
			getFeaturesAndGroupsOfBranchUpToRoot(mustNotBeDeleted, DwFeatureUtil.getParentGroupOfFeature((DwFeature) element, date), date);
		}
		else if(element instanceof DwGroup) {
			getFeaturesAndGroupsOfBranchUpToRoot(mustNotBeDeleted, DwFeatureUtil.getParentFeatureOfGroup((DwGroup) element, date), date);
		}
	}



	private void checkOperation(DwEvolutionOperation operation, List<DwTemporalElement> mustNotBeDeleted, Date date) throws DwCrossCheckingException {
		if(operation instanceof DwFeatureOperation) {
			if(deletedElements.contains(((DwFeatureOperation) operation).getFeature())) {
				throw new DwCrossCheckingException("Violation of Wellformedness Rule 5/6: Void Element Paradox !!! A deleted feature was tried to be changed via a " + operation.getClass().getSimpleName() + System.lineSeparator() + operation.toString());
			}
		}
		if(operation instanceof DwGroupOperation) {
			if(deletedElements.contains(((DwGroupOperation) operation).getGroup())) {
				throw new DwCrossCheckingException("Violation of Wellformedness Rule 5/6: Void Element Paradox !!! A deleted group was tried to be changed via a " + operation.getClass().getSimpleName() + System.lineSeparator() + operation.toString());
			}
		}
		if(operation instanceof DwFeatureAddOperation) {
			if(deletedElements.contains(((DwFeatureAddOperation) operation).getGroupToAddTo())) {
				throw new DwCrossCheckingException("Violation of Wellformedness Rule 5/6: Void Element Paradox !!! A feature was tried to be added to a deleted group via a " + operation.getClass().getSimpleName() + System.lineSeparator() + operation.toString());
			}
		}
		if(operation instanceof DwGroupAddOperation) {
			if(deletedElements.contains(((DwGroupAddOperation) operation).getParentFeature())) {
				throw new DwCrossCheckingException("Violation of Wellformedness Rule 5/6: Void Element Paradox !!! A group was tried to be added to a deleted feature via a " + operation.getClass().getSimpleName() + System.lineSeparator() + operation.toString());
			}
		}
		
		if(operation instanceof DwFeatureRenameOperation) {
			String newName = ((DwFeatureRenameOperation) operation).getNewNameString();
			String oldName = ((DwFeatureRenameOperation) operation).getOldNameString();
			
			if(!currentlyPickedNames.remove(oldName)) {
				// old name was not contained in name history...
			}
			
			if(!currentlyPickedNames.add(newName)) {
				throw new DwCrossCheckingException("Violation of Wellformedness Rule 3: each feature has a unique name - does not apply to feature " + newName);
			}
		}
		else if(operation instanceof DwFeatureMoveOperation || operation instanceof DwGroupMoveOperation) {
			DwTemporalElement elementToMove;
			DwTemporalElement elementToAddTo;
			if(operation instanceof DwFeatureMoveOperation) {
				elementToMove = ((DwFeatureMoveOperation) operation).getFeature();
				elementToAddTo = ((DwFeatureMoveOperation) operation).getAddOperation().getGroupToAddTo();
			}
			else if(operation instanceof DwGroupMoveOperation) {
				elementToMove = ((DwGroupMoveOperation) operation).getGroup();
				elementToAddTo = ((DwGroupMoveOperation) operation).getGroupAddOperation().getParentFeature();
			}
			else throw new RuntimeException();

			Set<DwTemporalElement> superElements = new HashSet<>();
			DwFeatureUtil.collectFeaturesAndGroupsOfSupertree(superElements, elementToAddTo, date);
			if(superElements.contains(elementToMove)) {
				throw new DwCrossCheckingException("Violation of Wellformedness Rule 5/6: A feature was tried to be moved to its parent element.");
			}
		}
		
		for(EObject childOperation : operation.eContents()) {
			if(childOperation instanceof DwEvolutionOperation) {
				checkOperation((DwEvolutionOperation) childOperation, mustNotBeDeleted, date);
			}
		}
		
		if(operation instanceof DwFeatureDeleteOperation) {
			DwFeatureDeleteOperation deleteOperation = (DwFeatureDeleteOperation) operation;
			deletedElements.add(deleteOperation.getFeature());
			
			DwFeature featureWithAppliedOperation = findEquivalentObject(copiedTFM, deleteOperation.getFeature());
			if(featureWithAppliedOperation == null) {
				// We are currently investigating whether a delete operaiton is safe.
				// The delete operation seems to have removed a feature that was renamed
				
			}
			else {
				String currentName = DwFeatureUtil.getName(featureWithAppliedOperation, date).getName();
				
				if(!currentlyPickedNames.remove(currentName)) {
					// name was not contained in name history...
					System.out.println("This name should have been in the list of picked names: " + currentName);
				}
			}
			
			if(mustNotBeDeleted != null) {
				if(mustNotBeDeleted.contains(deleteOperation.getFeature())) {
//					throw new DwCrossCheckingException("Effectless Operation Paradox !!!");
				}
			}
		}
		else if(operation instanceof DwGroupDeleteOperation) {
			DwGroupDeleteOperation deleteOperation = (DwGroupDeleteOperation) operation;
			deletedElements.add(deleteOperation.getGroup());
			
			if(mustNotBeDeleted != null) {
				if(mustNotBeDeleted.contains(deleteOperation.getGroup())) {
//					throw new DwCrossCheckingException("Effectless Operation Paradox !!!");
				}
			}
		}
	}



//	private void checkIfOperationDeletesElement(DwTemporalElement actuallyDeletedElement, DwTemporalElement mustNotBeDeleted, Date date) throws CrossCheckingException {
//		if(mustNotBeDeleted == null)
//			return;
//		
//		if(actuallyDeletedElement.equals(mustNotBeDeleted))
//			throw new CrossCheckingException("Effectless Operation Paradox !!!");
//		
//		if(actuallyDeletedElement instanceof DwFeature) {
//			for(DwGroup childGroup : DwFeatureUtil.getChildGroupsOfFeature((DwFeature) actuallyDeletedElement, date)) {
//				checkIfOperationDeletesElement(childGroup, )
//			}
//		}
//		else if(actuallyDeletedElement instanceof DwGroup) {
//			for(DwFeature childFeature: DwFeatureUtil.getChildFeaturesOfGroup((DwGroup) actuallyDeletedElement, date)) {
//				
//			}
//		}
//	}



	private DwEvolutionOperation getEquivalentOperationOnCopiedTFM(DwEvolutionOperation operation) {
		if(operation instanceof DwFeatureCreateOperation) {
			Date date = operation.getOperationDate();
			boolean lastDateSelected = false;
			
			EObject parent = findEquivalentObject(copiedTFM, ((DwFeatureCreateOperation) operation).getFeatureAddOperation().getParent());
			boolean forceCreateParentGroup = false;
			
			return operationFactory.createDwFeatureCreateOperation(copiedTFM, parent, forceCreateParentGroup, date, lastDateSelected);
		}
		if(operation instanceof DwFeatureDeleteOperation) {
			Date date = operation.getOperationDate();
			boolean lastDateSelected = false;
			
			DwFeature feature = findEquivalentObject(copiedTFM, ((DwFeatureDeleteOperation) operation).getFeature());
			
			return operationFactory.createDwFeatureDeleteOperation(feature.getFeatureModel(), feature, date, lastDateSelected);
		}
		if(operation instanceof DwFeatureMoveOperation) {
			Date date = operation.getOperationDate();
			boolean lastDateSelected = false;
			
			DwFeature feature = findEquivalentObject(copiedTFM, ((DwFeatureMoveOperation) operation).getFeature());
			EObject newParent = findEquivalentObject(copiedTFM, ((DwFeatureMoveOperation) operation).getAddOperation().getParent());
			
			return operationFactory.createDwFeatureMoveOperation(feature.getFeatureModel(), feature, newParent, date, lastDateSelected);
		}
		if(operation instanceof DwFeatureRenameOperation) {
			Date date = operation.getOperationDate();
			boolean lastDateSelected = false;

			DwFeature feature = findEquivalentObject(copiedTFM, ((DwFeatureRenameOperation) operation).getFeature());
			String newName = ((DwFeatureRenameOperation) operation).getNewNameString();
			
			return operationFactory.createDwFeatureRenameOperation(feature.getFeatureModel(), feature, newName, date, lastDateSelected);
		}
		if(operation instanceof DwFeatureTypeChangeOperation) {
			Date date = operation.getOperationDate();
			boolean lastDateSelected = false;
			
			DwFeature feature = findEquivalentObject(copiedTFM, ((DwFeatureTypeChangeOperation) operation).getFeature());
			DwFeatureTypeEnum newType = ((DwFeatureTypeChangeOperation) operation).getNewFeatureTypeEnum();
			
			return operationFactory.createDwFeatureTypeChangeOperation(copiedTFM, feature, newType, date, lastDateSelected);
		}
		
		if(operation instanceof DwGroupDeleteOperation) {
			Date date = operation.getOperationDate();
			boolean lastDateSelected = false;
			
			DwGroup group = findEquivalentObject(copiedTFM, ((DwGroupDeleteOperation) operation).getGroup());
			
			return operationFactory.createDwGroupDeleteOperation(group.getFeatureModel(), group, date, lastDateSelected);
		}
		if(operation instanceof DwGroupMoveOperation) {
			Date date = operation.getOperationDate();
			boolean lastDateSelected = false;
			
			DwGroup group = findEquivalentObject(copiedTFM, ((DwGroupMoveOperation) operation).getGroup());
			DwFeature newParentFeature = findEquivalentObject(copiedTFM, ((DwGroupMoveOperation) operation).getGroupAddOperation().getParentFeature());
			
			return operationFactory.createDwGroupMoveOperation(group.getFeatureModel(), group, newParentFeature, date, lastDateSelected);
		}
		if(operation instanceof DwGroupTypeChangeOperation) {
			Date date = operation.getOperationDate();
			boolean lastDateSelected = false;
			
			DwGroup group = findEquivalentObject(copiedTFM, ((DwGroupTypeChangeOperation) operation).getGroup());
			DwGroupTypeEnum newType = ((DwGroupTypeChangeOperation) operation).getNewGroupTypeEnum();
			
			return operationFactory.createDwGroupTypeChangeOperation(copiedTFM, group, newType, date, lastDateSelected);
		}
		
		throw new RuntimeException("Could not match the given operation ...");
	}
	
	
	
//	private void checkDoubleFeatureNames(Date date) throws DwCrossCheckingException {
//		// Rule 3: each feature has a unique name
//
//		Set<String> uniqueNames = new HashSet<>();
//		Set<String> doubleNames = new HashSet<>();
//		
//		for(DwName featureName : DwFeatureUtil.getAllFeatureNames(copiedTFM, date)) {
//			String nameString = featureName.getName();
//			if(uniqueNames.contains(nameString)) {
//				doubleNames.add("\"" + nameString + "\"");
//			}
//			else {
//				uniqueNames.add(nameString);
//			}
//		}
//		
//		if(!doubleNames.isEmpty()) {
//			throw new DwCrossCheckingException("Violation of Wellformedness Rule 3: each feature has a unique name - does not apply to features " + String.join(", ", doubleNames));
//		}
//	}
	
	private void checkGroup(DwGroup group, Date date) throws DwCrossCheckingException {
		// Rule 4: features are organized in groups that have variation types as well.
		{
			List<DwGroupType> validTypes = DwEvolutionUtil.getValidTemporalElements(group.getTypes(), date);
			if(validTypes.size() != 1)
				throw new DwCrossCheckingException("Violation of Wellformedness Rule 4: features are organized in groups that have variation types as well (group: " + group.toString() + ").");
		}
		
		// Rule 6: each group must have exactly one parent feature.
		{
			List<DwParentFeatureChildGroupContainer> validParentRelation = DwEvolutionUtil.getValidTemporalElements(group.getChildOf(), date);
			if(validParentRelation.size() != 1)
				throw new DwCrossCheckingException("Violation of Wellformedness Rule 6: each group must have exactly one parent feature (group: " + group.toString() + ").");
		}
		
		List<DwFeature> childFeatures = DwFeatureUtil.getChildFeaturesOfGroup(group, date);
		
		// Rule 7: groups with types ALTERNATIVE or OR must not contain MANDATORY features.
		{
			DwGroupTypeEnum validGroupType = DwFeatureUtil.getType(group, date).getType();
			if(validGroupType.equals(DwGroupTypeEnum.ALTERNATIVE) || validGroupType.equals(DwGroupTypeEnum.OR)) {
				for(DwFeature childFeature : childFeatures) {
					if(DwFeatureUtil.getType(childFeature, date).getType().equals(DwFeatureTypeEnum.MANDATORY))
						throw new DwCrossCheckingException("Violation of Wellformedness Rule 7: groups with types ALTERNATIVE or OR must not contain MANDATORY features (group: " + group.toString() + ").");
				}
			}
		}

//		 Rule 8: groups with types ALTERNATIVE or OR must contain at least two child features.
		{
			DwGroupTypeEnum validGroupType = DwFeatureUtil.getType(group, date).getType();
			if(validGroupType.equals(DwGroupTypeEnum.ALTERNATIVE) || validGroupType.equals(DwGroupTypeEnum.OR)) {
				if(childFeatures.size() < 2)
					throw new DwCrossCheckingException("Violation of Wellformedness Rule 8: groups with types ALTERNATIVE or OR must contain at least two child features (group: " + group.toString() + ").");
			}
		}

		for(DwFeature childFeature : childFeatures) {
			checkFeature(childFeature, date);
		}
	}
	
	private void checkFeature(DwFeature feature, Date date) throws DwCrossCheckingException {
		// Rule 3: each feature has exactly one variation type.
		{
			List<DwFeatureType> validTypes = DwEvolutionUtil.getValidTemporalElements(feature.getTypes(), date);
			if(validTypes.size() != 1)
				throw new DwCrossCheckingException("Violation of Wellformedness Rule 3: each feature has exactly one variation type. (feature: " + feature.toString() + ").");
		}

		// Rule 5: each feature, except for the root feature, must be part of exactly one group.
		{
			List<DwGroupComposition> validParentRelation = DwEvolutionUtil.getValidTemporalElements(feature.getGroupMembership(), date);
			if(validParentRelation.size() != 1)
				throw new DwCrossCheckingException("Violation of Wellformedness Rule 5: each feature, except for the root feature, must be part of exactly one group (feature: " + feature.toString() + ").");
		}

		for(DwGroup childGroup : DwFeatureUtil.getChildGroupsOfFeature(feature, date)) {
			checkGroup(childGroup, date);
		}
	}
	
	
	
	/**
	 * A simple matching via ID
	 * @param modelToSearchIn the model that shall be search in.
	 * @param objectToSearchQuivalentFor an element that is the equivalent of an element within the given model.
	 * @return equivalent object to given element.
	 */
	@SuppressWarnings("unchecked")
	public <T> T findEquivalentObject(DwTemporalFeatureModel modelToSearchIn, T objectToSearchQuivalentFor) {
		if(objectToSearchQuivalentFor instanceof DwTemporalElement) {
			for(DwFeature potentialMatch : modelToSearchIn.getFeatures()) {
				if(potentialMatch.getId().equals(((DwTemporalElement) objectToSearchQuivalentFor).getId())) {
					return (T) potentialMatch;
				}
			}
			for(DwGroup potentialMatch : modelToSearchIn.getGroups()) {
				if(potentialMatch.getId().equals(((DwTemporalElement) objectToSearchQuivalentFor).getId())) {
					return (T) potentialMatch;
				}
			}
			
			return null;
		}
		
		throw new RuntimeException("Unhandled type: findEquivalentObject(...) does not support a search for elements of type " + objectToSearchQuivalentFor.getClass().getSimpleName());
	}
	
}









