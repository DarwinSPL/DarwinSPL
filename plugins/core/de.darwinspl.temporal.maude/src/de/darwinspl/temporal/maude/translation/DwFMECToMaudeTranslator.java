package de.darwinspl.temporal.maude.translation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.fmec.FeatureModelEvolutionConstraintModel;
import de.darwinspl.fmec.constraints.BinaryConstraint;
import de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock;
import de.darwinspl.fmec.constraints.FeatureModelEvolutionConstraint;
import de.darwinspl.fmec.constraints.NestedConstraint;
import de.darwinspl.fmec.constraints.TimeIntervalConstraint;
import de.darwinspl.fmec.constraints.TimePointConstraint;
import de.darwinspl.fmec.constraints.UnaryConstraint;
import de.darwinspl.fmec.keywords.FeatureModelEvolutionConstraintKeyword;
import de.darwinspl.fmec.keywords.buildingblocks.KeywordStarts;
import de.darwinspl.fmec.keywords.buildingblocks.KeywordStops;
import de.darwinspl.fmec.keywords.buildingblocks.KeywordValid;
import de.darwinspl.fmec.keywords.logical.KeywordAnd;
import de.darwinspl.fmec.keywords.logical.KeywordImplies;
import de.darwinspl.fmec.keywords.logical.KeywordNot;
import de.darwinspl.fmec.keywords.logical.KeywordOr;
import de.darwinspl.fmec.keywords.timepoint.KeywordAfter;
import de.darwinspl.fmec.keywords.timepoint.KeywordAt;
import de.darwinspl.fmec.keywords.timepoint.KeywordBefore;
import de.darwinspl.fmec.keywords.timepoint.KeywordUntil;
import de.darwinspl.fmec.keywords.timepoint.KeywordWhen;
import de.darwinspl.fmec.property.EvolutionaryProperty;
import de.darwinspl.fmec.property.FeatureExistenceProperty;
import de.darwinspl.fmec.property.FeatureParentProperty;
import de.darwinspl.fmec.property.FeatureTypeProperty;
import de.darwinspl.fmec.property.GroupExistenceProperty;
import de.darwinspl.fmec.property.GroupParentProperty;
import de.darwinspl.fmec.property.GroupTypeProperty;
import de.darwinspl.fmec.property.Property;
import de.darwinspl.fmec.timepoint.EvolutionaryEvent;
import de.darwinspl.fmec.timepoint.ExplicitTimePoint;
import de.darwinspl.fmec.timepoint.TimeOffset;
import de.darwinspl.fmec.timepoint.TimePoint;
import de.darwinspl.fmec.util.DwTemporalConstraintsUtil;

public class DwFMECToMaudeTranslator {
	
	private List<Date> allDates;
	
	public DwFMECToMaudeTranslator(List<Date> allDates) {
		this.allDates = allDates;
	}



	public String translateConstraints(DwTemporalFeatureModel tfm) {
		List<FeatureModelEvolutionConstraintModel> constraintsModels;
		try {
			constraintsModels = DwTemporalConstraintsUtil.getFMECModelsForTFM(tfm);
		} catch (Exception e1) {
			e1.printStackTrace();
			return null;
		}

		if(constraintsModels == null)
			return null;
		
		List<String> translatedConstraintsModels = new ArrayList<>();
		for(FeatureModelEvolutionConstraintModel constraintsModel : constraintsModels) {
			String translatedConstraints = translateFMECModel(constraintsModel);
			
			if(translatedConstraints != null)
				translatedConstraintsModels.add(translatedConstraints);
		}
		
		if(translatedConstraintsModels.size() == 0)
			return null;
		
		return String.join(System.lineSeparator(), translatedConstraintsModels);
	}
	
	
	
	public String translateFMECModel(FeatureModelEvolutionConstraintModel constraintsModel) {
		List<String> translatedConstraints = new ArrayList<>();
		for(FeatureModelEvolutionConstraint constraint : constraintsModel.getConstraints()) {
			translatedConstraints.add("red check(init, " + translateFMEC(constraint) + ") .");
		}
		
		if(translatedConstraints.size() > 0)
			return (String.join(System.lineSeparator(), translatedConstraints));
		
		return null;
	}
	


	private String translateFMEC(FeatureModelEvolutionConstraint element) {
		if(element instanceof BinaryConstraint)
			return translateBinaryConstraint((BinaryConstraint) element);
		
		if(element instanceof UnaryConstraint)
			return translateUnaryConstraint((UnaryConstraint) element);
		
		if(element instanceof NestedConstraint)
			return translateNestedConstraint((NestedConstraint) element);
		
		if(element instanceof TimePointConstraint)
			return translateTimePointConstraint((TimePointConstraint) element);
		
		if(element instanceof TimeIntervalConstraint)
			return translateTimeIntervalConstraint((TimeIntervalConstraint) element);
		
		throw new RuntimeException("Unrecognized kind of Temporal Constraint!");
	}
	
	private String translateBinaryConstraint(BinaryConstraint element) {
		String keyword = translateKeyword(element.getKeyword());
		
		String leftResult = translateFMEC(element.getLeftChild());
		String rightResult = translateFMEC(element.getRightChild());
		
		return "(" + leftResult + ") " + keyword + " (" + rightResult + ")";
	}
	
	private String translateUnaryConstraint(UnaryConstraint element) {
		String keyword = translateKeyword(element.getKeyword());

		String childResult = translateFMEC(element.getChild());
		
		return keyword + " (" + childResult + ")";
	}
	
	private String translateNestedConstraint(NestedConstraint element) {
		String childResult = translateFMEC(element.getNestedConstraint());
		return "(" + childResult + ")";
	}
	
	
	
	private String translateTimePointConstraint(TimePointConstraint element) {
		String keyword = translateKeyword(element.getKeyword());
		String preArgument = translateAbstractBuildingBlock(element.getPreArgument());
		String postArgument = translateAbstractBuildingBlock(element.getPostArgument());
		
		String constraint = preArgument + " " + keyword + " " + postArgument;
		
//		if(element.getKeyword() instanceof KeywordBefore) {
//			// check additional condition: /\ ( timepoint2 -> event.property )
//			// FMEC: <event> at <t>
//			// TODO distinguish between starting and ending events !!
//			Entity entity = element.getPreArgument().getEntity();
//			String eventPropertyElementID;
//			if(entity instanceof FeatureReference)
//				eventPropertyElementID = ((FeatureReference) entity).getReferencedFeature().get(0).getId();
//			else if(entity instanceof GroupReference)
//				eventPropertyElementID = ((GroupReference) entity).getReferencedGroup().getId();
//			else
//				throw new RuntimeException("Unrecognized kind of Entity!");
//			
//			String keyword;
//			if(element.getTemporalPoint2().getTimePoint() instanceof ImplicitTemporalPoint)
//				keyword = "when";
//			else if(element.getTemporalPoint2().getTimePoint() instanceof FixedTemporalPoint)
//				keyword = "at";
//			else
//				throw new RuntimeException("Unrecognized kind of TimePoint!");
//			
//			constraint = "("+ constraint +") /\\ (feature \""+ eventPropertyElementID +"\" exists "+ keyword +" "+ translateTemporalPoint(element.getTemporalPoint2()) +")";
//		}
		
		return constraint;
	}

	// ::= temporalPoint relation interval;
	private String translateTimeIntervalConstraint(TimeIntervalConstraint element) {
		String translatedPreArgument = translateAbstractBuildingBlock(element.getPreArgument());
		
		String keyword;
		if(element.getPreArgument() instanceof EvolutionaryProperty)
			keyword = "while between";
		else if(element.getPreArgument() instanceof TimePoint)
			keyword = "between";
		else
			throw new RuntimeException("Unexpected kind of Abstract FMEC Building Block...");

		String translatedStartPoint = translateTimePoint(element.getStartPoint());
		String translatedEndPoint = translateTimePoint(element.getEndPoint());
		
		return translatedPreArgument + " " + keyword + " " + translatedStartPoint + " and " + translatedEndPoint;
	}
	
	
	
	private String translateAbstractBuildingBlock(EvolutionaryAbstractBuidingBlock element) {
		if(element instanceof EvolutionaryProperty)
			return translateEvolutionaryProperty((EvolutionaryProperty) element);
		if(element instanceof TimePoint)
			return translateTimePoint((TimePoint) element);
		
		throw new RuntimeException("Unrecognized kind of Abstract FMEC Building Block...");
	}

	private String translateEvolutionaryProperty(EvolutionaryProperty element) {
		Property entity = element.getProperty();
		if(entity instanceof FeatureExistenceProperty) {
			String featureID = ((FeatureExistenceProperty) entity).getReferencedFeature().getId();
			return "feature \"" + featureID + "\" exists";
		}
		if(entity instanceof FeatureTypeProperty) {
			String featureID = ((FeatureTypeProperty) entity).getReferencedFeature().getId();
			String type = ((FeatureTypeProperty) entity).getFeatureType().toString();
			return "feature \"" + featureID + "\" has type " + type;
		}
		if(entity instanceof FeatureParentProperty) {
			String featureID = ((FeatureParentProperty) entity).getReferencedFeature().getId();
			String groupID = ((FeatureParentProperty) entity).getReferencedGroup().getId();
			return "feature \"" + featureID + "\" has parent group \"" + groupID + "\"";
		}
		
		if(entity instanceof GroupExistenceProperty) {
			String featureID = ((GroupExistenceProperty) entity).getReferencedGroup().getId();
			return "feature \"" + featureID + "\" exists";
		}
		if(entity instanceof GroupTypeProperty) {
			String groupID = ((GroupTypeProperty) entity).getReferencedGroup().getId();
			String type = ((GroupTypeProperty) entity).getGroupType().toString();
			return "group \"" + groupID + "\" has type " + type;
		}
		if(entity instanceof GroupParentProperty) {
			String featureID = ((GroupParentProperty) entity).getReferencedFeature().getId();
			String groupID = ((GroupParentProperty) entity).getReferencedGroup().getId();
			return "group \"" + groupID + "\" has parent \"" + featureID + "\"";
		}
		
		throw new RuntimeException("Unrecognized Entity...");
	}
	
	private String translateTimePoint(TimePoint element) {
		String translatedTimePoint;
		if(element.getTimePoint() instanceof ExplicitTimePoint)
			translatedTimePoint = translateExplicitTimePoint((ExplicitTimePoint) element.getTimePoint());
		else if(element.getTimePoint() instanceof EvolutionaryEvent)
			translatedTimePoint = translateEvolutionaryEvent((EvolutionaryEvent) element.getTimePoint());
		else
			throw new RuntimeException("Unrecognized kind of time point...");
		
		TimeOffset offset = element.getOffset();
		// TODO translate offset !!
		
		return translatedTimePoint;
	}

	private String translateExplicitTimePoint(ExplicitTimePoint element) {
		Date searchedDate = element.getDate();
		
		int index = allDates.indexOf(searchedDate);
		if(index != -1) {
			index = (index+1)*2;
		}
		else {
			for(int i=0; i<allDates.size(); i++) {
				if(searchedDate.compareTo(allDates.get(i)) < 0) {
					index = (i+1)*2-1;
					break;
				}
			}
		}
		if(index == -1) {
			index = (allDates.size()+1)*2;
		}
		
		return String.valueOf(index);
	}
	
	private String translateEvolutionaryEvent(EvolutionaryEvent element) {
		String featureID = element.getReferencedFeature().getId();
		String keyword = translateKeyword(element.getKeyword());
		return "feature \"" + featureID + "\" exists " + keyword;
	}
	
	
	
	private String translateKeyword(FeatureModelEvolutionConstraintKeyword keyword) {
		if(keyword instanceof KeywordValid)
			return "";
			
		if(keyword instanceof KeywordStarts)
			return "starts";
		
		if(keyword instanceof KeywordStops)
			return "stops";
		
		if(keyword instanceof KeywordAnd)
			return "/\\";
			
		if(keyword instanceof KeywordOr)
			return "\\/";
		
		if(keyword instanceof KeywordImplies)
			return "->";
		
		if(keyword instanceof KeywordNot)
			return "~";
		
		if(keyword instanceof KeywordBefore)
			return "before";
		
		if(keyword instanceof KeywordUntil)
			return "until";
			
		if(keyword instanceof KeywordAfter)
			return "after";

		if(keyword instanceof KeywordWhen)
			return "when";
		
		if(keyword instanceof KeywordAt)
			return "at";
		
		throw new RuntimeException("Unrecognized Constraint Keyword...");
	}

}
