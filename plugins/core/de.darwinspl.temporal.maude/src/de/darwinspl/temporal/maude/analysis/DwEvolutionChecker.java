package de.darwinspl.temporal.maude.analysis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.resource.Resource;

import de.christophseidl.util.ecore.EcoreIOUtil;
import de.darwinspl.common.eclipse.util.DwResourceUtil;
import de.darwinspl.common.eclipse.util.DwSettingsPreferencesManager;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.evolution.operation.interpreter.extensions.DwEvolutionOperationIntepreterExtension;
import de.darwinspl.feature.evolution.operation.resource.DwEvolutionOperationModelResourceUtil;
import de.darwinspl.feature.operation.DwEvolutionOperation;
import de.darwinspl.fmec.FeatureModelEvolutionConstraintModel;
import de.darwinspl.temporal.maude.analysis.crosscheck.DwEvolutionPlanCrossChecker;
import de.darwinspl.temporal.maude.translation.DwToMaudeTranslator;
import de.darwinspl.temporal.maude.web.WebClient;

public class DwEvolutionChecker implements DwEvolutionOperationIntepreterExtension {

	private static final DwToMaudeTranslator translator = new DwToMaudeTranslator();
	private static DwEvolutionChecker instance;
	
	public static DwEvolutionChecker getInstance() {
		if(instance == null)
			instance = new DwEvolutionChecker();
		return instance;
	}
	
	

	private List<DwEvolutionCheckerResultHook> observers = new ArrayList<>();

	public List<DwEvolutionCheckerResultHook> getObservers() {
		return Collections.unmodifiableList(observers);
	}

	public boolean addObserver(DwEvolutionCheckerResultHook newObserver) {
		return observers.add(newObserver);
	}

	public boolean addObservers(List<DwEvolutionCheckerResultHook> newObservers) {
		return observers.addAll(newObservers);
	}
	
	public boolean removeObserver(DwEvolutionCheckerResultHook oldObserver) {
		return observers.remove(oldObserver);
	}
	
	public void clearObservers() {
		observers.clear();
	}
	
	protected boolean isResultValid(DwEvolutionCheckingResult result) {
		for(DwEvolutionCheckerResultHook observer : this.observers)
			if(!observer.isResultValid(result))
				return false;
		
		return true;
	}
	
	
	
	private DwEvolutionPlanCrossChecker crossChecker;
	
	public DwEvolutionChecker() {
		crossChecker = new DwEvolutionPlanCrossChecker();
	}
	
	

	@Override
	public void init() {
		getInstance();
	}

	@Override
	public boolean run(DwEvolutionOperation evoOp, String additionalInfo) {
		return instance.doCheckOperation(evoOp, additionalInfo);
	}
	
	

	private boolean doCheckOperation(DwEvolutionOperation evoOp, String additionalInfo) {
		boolean alwaysSearchAndTranslateFMECs = DwSettingsPreferencesManager.isAutoFMEC();
		
		if(alwaysSearchAndTranslateFMECs || evoOp.isIntermediateOperation()) {
			String maudeInput = translator.translate(evoOp.getFeatureModel(), alwaysSearchAndTranslateFMECs);
			
			if(DwSettingsPreferencesManager.isMaudeDebug())
				writeLoggedMaudeFormalism(evoOp.getFeatureModel().eResource(), maudeInput);
			
//			String maudeResult = readLoggedMaudeFormalism(evoOp.getFeatureModel().eResource());
//			if(maudeResult == null) {
//				maudeResult = translator.translate(evoOp.getFeatureModel(), evoOp.getOperationDate());
//				writeLoggedMaudeFormalism(evoOp.getFeatureModel().eResource(), maudeResult);
//			}
			
//			printProminent(maudeResult);

			String crossCheckingResult = crossChecker.run(evoOp);
//			System.out.println(crossCheckingResult);

			if(maudeInput != null) {
				WebClient wsClient = new WebClient();
				DwEvolutionCheckingResult result;
				try {
					String wsClientReply = wsClient.sendRequestAndWaitForReply(maudeInput);
					result = new DwEvolutionCheckingResult(wsClientReply, crossCheckingResult, additionalInfo);
//					printProminent(wsClientReply);
				} catch (IOException e) {
					result = new DwEvolutionCheckingResult(null, crossCheckingResult, additionalInfo);
				}
				
				// Let user confirm editing the TFM in an intermediate evolution stage.
				if(this.isResultValid(result)) {
					return true;
				} else {
					DwEvolutionOperationModelResourceUtil.removeOperationFromOperationsModel(evoOp);
					return false;
				}
			}
		}
		
		return true;
	}
	
	public String checkFmecModule(DwTemporalFeatureModel tfm, FeatureModelEvolutionConstraintModel fmecModel) {
		translator.forceFmecModuleCheck(fmecModel);
		String maudeInput = translator.translate(tfm, true);
		
		if(DwSettingsPreferencesManager.isMaudeDebug())
			writeLoggedMaudeFormalism(tfm.eResource(), maudeInput);
		
		if(maudeInput != null) {
			WebClient wsClient = new WebClient();
			try {
				return wsClient.sendRequestAndWaitForReply(maudeInput);
			} catch (IOException e) {
				return e.toString();
			}
		}
		
		return "Could not translate evolution plan";
	}
	
	
	
	private String readLoggedMaudeFormalism(Resource resource) {
		IFile opModelFile = EcoreIOUtil.getFile(resource);
		IContainer parent = opModelFile.getParent();

		IFile logFile = parent.getFile(new Path("last_query.maude"));
		if(logFile.exists()) {
			StringBuilder textBuilder = new StringBuilder();
		    try (Reader reader = new BufferedReader(new InputStreamReader(logFile.getContents(), Charset.forName(StandardCharsets.UTF_8.name())))) {
		        int c = 0;
		        while ((c = reader.read()) != -1) {
		            textBuilder.append((char) c);
		        }
		        return textBuilder.toString();
		    } catch (IOException e) {
				e.printStackTrace();
			} catch (CoreException e) {
				e.printStackTrace();
			}
		}
		
		return null;
	}
	
	private void writeLoggedMaudeFormalism(Resource resource, String maudeResult) {
		IFile opModelFile = EcoreIOUtil.getFile(resource);
		if(opModelFile == null)
			return;
		IContainer parent = opModelFile.getParent();
		
		IFile logFile = parent.getFile(new Path("last_query.maude"));
		DwResourceUtil.writeToFile(logFile, maudeResult);
	}

	private void printProminent(String s) {
		System.out.println("___________________________________________________________________________________________________");
		System.out.println(s);
		System.out.println("___________________________________________________________________________________________________");
	}
	
}












