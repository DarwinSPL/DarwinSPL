package de.darwinspl.temporal.maude.analysis.crosscheck;

public class DwCrossCheckingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6438618174732787153L;

	public DwCrossCheckingException(String message) {
		super(message);
	}

}
