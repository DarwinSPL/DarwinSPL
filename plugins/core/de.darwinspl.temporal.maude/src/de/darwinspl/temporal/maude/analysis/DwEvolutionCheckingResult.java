package de.darwinspl.temporal.maude.analysis;

public class DwEvolutionCheckingResult {
	
	private static final String UNSAFE_OPERATION_PREFIX = "ERROR: ";
//	private static final String UNSAFE_OPERATION_EXPLANATION_PREFIX = "PLAN ERROR: ";

	private String rawMaudeAnswer;
	private String[] rawMaudeAnswerLines;
	
	private boolean successMaude = false;

	private String title;
	private String message;
	
//	private String[] rawCrossCheckingResultLines;
	
	private int maudeRuntimeOffline;
	private int maudeRuntimeOnline;
	private int crossCheckingRuntime;
	
	private String transientEffectExplanation;
	
	
	
	public DwEvolutionCheckingResult(String rawMaudeResult, String crossCheckingResult, String additionalInfo) {
		this.rawMaudeAnswer = rawMaudeResult;
		if(rawMaudeResult != null)
			this.rawMaudeAnswerLines = rawMaudeResult.split(System.lineSeparator());
		
//		if(crossCheckingResult != null)
//			this.rawCrossCheckingResultLines = crossCheckingResult.split(System.lineSeparator());
		
		transientEffectExplanation = additionalInfo;
		
		interprete();
	}
	


	protected void interprete() {
		interpreteMaude();
		
		if(this.isSuccess()) {
			if(transientEffectExplanation != null) {
				this.successMaude = false;
				this.title = "Transient Effect Evolution Paradox!";
				this.message = "Transient Effect Evolution Paradox detected: " + transientEffectExplanation;
			}
		}
	}



	protected void interpreteMaude() {
		if(rawMaudeAnswer == null) {
			setNoConnectionFail();
			return;
		}
		
		{
			maudeRuntimeOnline = timeStringToInt(rawMaudeAnswerLines[1]);
			maudeRuntimeOffline = timeStringToInt(rawMaudeAnswerLines[2]);
		}
		
		String errorExplanation = "Error Line not found ...";
		for(int i=3; i<rawMaudeAnswerLines.length; i++) {
			if(rawMaudeAnswerLines[i].contains(":")) {
				errorExplanation = rawMaudeAnswerLines[i];
				break;
			}
		}
		
		String state = rawMaudeAnswerLines[3].trim();
		if(state.equals("TIMEOUT") || state.equals("LAUNCH ERROR")) {
			setServerSideFail(errorExplanation);
			return;
		}
		
		if(state.startsWith(UNSAFE_OPERATION_PREFIX)) {
			setUnsafeOperationFail(errorExplanation);
			return;
		}
		
		if(state.equals("PLAN CORRECT") && rawMaudeAnswerLines.length == 4) {
			setSuccess();
			return;
		}
		
		setFMECs();
		
//		setCannotInterprete();
	}



	public static int timeStringToInt(String timeString) {
		String str = timeString.split(":")[1].trim();
		str = str.split(" ", 2)[0].trim();
		str = str.replace("ms", "");
		try {
			return Integer.valueOf(str);
		} catch(NumberFormatException e) {
			System.err.println("\"" + timeString + "\" could not be parsed");
			return -1;
		}
	}



	private void setUnsafeOperationFail(String message) {
		this.successMaude = false;
		this.title = "Unsafe Intermediate Evolution Operation!";
		
		this.message = "The Maude model checker found a paradox in the evolution plan.";
//		this.explanation = "Inconsistent operation in time point: ";
//		
//		try {
//			String timepointStr = message.substring(UNSAFE_OPERATION_EXPLANATION_PREFIX.length());
//			timepointStr = timepointStr.split(" ", 2)[0];
//			this.timepointIndex = Integer.valueOf(timepointStr);
//			this.explanation += this.timepointIndex;
//		}
//		catch (Exception e) {
//			e.printStackTrace();
//			this.explanation += "<could not be parsed>";
//		}
//		
//		this.explanation += System.lineSeparator() + rawMaudeAnswerLines[3].substring(UNSAFE_OPERATION_PREFIX.length());
	}

	private boolean isConstraintCheckFail(int relevantIndex) {
//		for(int i=relevantIndex; i<rawAnswerLines.length; i++) {
//			String line = rawAnswerLines[i];
//			if(line.startsWith(constraintCheckSuccessResultLine)) {
//				String result = line.substring(constraintCheckSuccessResultLine.length());
//				return ! Boolean.parseBoolean(result);
//			}
//			if(line.startsWith(constraintCheckFailedResultLine)) {
//				return true;
//			}
//		}
//		
//		System.out.println("Maude constraint check result was not parsable!");
//		// To be safe, return that the check failed.
		return true;
	}

	private void setViolatedConstraintsFail(int relevantIndex) {
//		this.success = false;
//		this.title = "Violated Temporal Business Concerns!";
//		this.message = "The Maude model checker found violations to a specified temporal business concern:";
//		
//		int endIndex = 0;
//		for(int i=relevantIndex; i<rawAnswerLines.length; i++) {
//			String line = rawAnswerLines[i];
//			if(line.startsWith(constraintCheckSuccessResultLine) || line.startsWith(constraintCheckFailedResultLine)) {
//				endIndex = i;
//				break;
//			}
//		}
//
//		this.explanation = "";
//		for(int i=relevantIndex; i<endIndex; i++) {
//			this.explanation += " " + rawAnswerLines[i].trim();
//		}
//		
//		if(explanation.length() > 3) {
//			this.explanation = explanation.substring(constraintCheckStartLine.length(), explanation.length()-3).trim();
//		} else {
//			this.explanation = "Answer not parsable. Please refer to the raw answer.";
//		}
	}

	private void setServerSideFail(String message) {
		this.successMaude = false;
		this.title = "Evolution Plan Checking Service Failure";
		this.message = message;
	}
	
	private void setNoConnectionFail() {
		this.successMaude = false;
		this.title = "Maude Service not Reachable";
		this.message = "DarwinSPL was not able to establish a connection to the Maude model checking service.\nPlease check your DarwinSPL settings.\n\nProceeding with the operation could introduce inconsistencies to your model.";
	}

	protected void setSuccess() {
		this.successMaude = true;
		this.title = "Safe Intermediate Evolution Operation";
		this.message = "The Maude model checker did not find any inconsistencies in the model history/plan.";
	}

	private void setCannotInterprete() {
		this.successMaude = false;
		this.title = "Answer cannot be interpreted";
		this.message = "Answer cannot be interpreted" + System.lineSeparator() + "This does not mean your evolution plan is invalid. Please check the evolution plan checking service result beneath manually.";
	}
	
	private void setFMECs() {
		this.successMaude = true;
		this.title = "One or more FMECs were violated.";
		this.message = "You can find violated FMECs by right-clicking each associated FMEC module and triggering the check again manually.";
		this.rawMaudeAnswer = getBetterLookingFormat(rawMaudeAnswer);
		
		for(String line : rawMaudeAnswerLines) {
			if(line.startsWith("##")) {
				continue;
			}
			if(line.equals("PLAN CORRECT")) {
				continue;
			}

			if(line.equals("FAILURE")) {
				successMaude = false;
				return;
			}
		}
	}
	
	public static String getBetterLookingFormat(String input) {
		String[] lineSplit = input.split(System.lineSeparator());
		String result = "";
		
		int fmecNumber = 1;
		for(String line : lineSplit) {
			if(line.startsWith("##")) {
				result += line + System.lineSeparator();
				continue;
			}
			if(line.equals("PLAN CORRECT")) {
				continue;
			}

			result += "FMEC #"+fmecNumber+": ";
			if(line.equals("SUCCESS"))
				result += "holds";
			else if(line.equals("FAILURE"))
				result += "VIOLATED";
			else
				result += line;
			
			result += System.lineSeparator();
			
			fmecNumber++;
		}

		return result;
	}
	
	

//	private void setOnlyCrossCheckingFail() {
//		this.title = "Cross Checking Failed";
//		this.message = "While Maude did not discover any problems with the evolution plan, additional checks failed:";
//	}
	
	
	
	public String getRawAnswer() {
		return rawMaudeAnswer;
	}

	public boolean isSuccess() {
		return successMaude;
	}

	public String getTitle() {
		return title;
	}

	public String getMessage() {
		return message;
	}

//	public String getExplanation() {
//		return explanation;
//	}

	public int getMaudeRuntimeOffline() {
		return maudeRuntimeOffline;
	}

	public int getMaudeRuntimeOnline() {
		return maudeRuntimeOnline;
	}

	public int getCrossCheckingRuntime() {
		return crossCheckingRuntime;
	}



	public boolean isMaudeTimeout() {
		return this.rawMaudeAnswer == null;
	}
	
	
	
	@Override
	public String toString() {
		String str = "Maude Evolution Plan Checking result: ";
		
		if(this.successMaude) {
			str += "SUCCESS";
		}
		else {
			str += "FAIL!";
			str += System.lineSeparator();
			str += formatAttribute("Title", this.title);
			str += System.lineSeparator();
//			str += formatAttribute("Explanation", this.explanation);
//			str += System.lineSeparator();
			str += formatAttribute("Message", this.message);
			str += System.lineSeparator();
			str += formatAttribute("Raw Answer", System.lineSeparator()+this.rawMaudeAnswer);
		}
		
		return str;
	}
	
	private String formatAttribute(String name, String attr) {
		if(attr != null && !attr.equals(""))
			return name + ": " + attr;
		return "";
	}
}




