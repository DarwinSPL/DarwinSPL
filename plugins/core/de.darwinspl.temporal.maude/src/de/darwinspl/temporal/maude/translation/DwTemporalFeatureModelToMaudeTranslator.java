package de.darwinspl.temporal.maude.translation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupComposition;
import de.darwinspl.feature.DwParentFeatureChildGroupContainer;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.util.custom.DwFeatureUtil;
import de.darwinspl.temporal.util.DwEvolutionUtil;

public class DwTemporalFeatureModelToMaudeTranslator {
	
//	public String translateInitialFeatureModel(DwTemporalFeatureModel featureModel) {
//		Date date = DwDateResolverUtil.INITIAL_STATE_DATE;
//		
//		DwFeature rootFeature = DwFeatureUtil.getRootFeature(featureModel, date);
//		if(rootFeature == null) {
//			// The initial state did not contain a root feature
//			rootFeature = featureModel.getRootFeatures().get(0).getFeature();
//		}
//		String rootFeatureId = rootFeature.getId();
//		
//		String result = "eq model = FM(\"" + rootFeatureId + "\" ," + System.lineSeparator();
//		result += translateRootFeatureInInitialState(rootFeature) + ") ." + System.lineSeparator();
//		
//		result += "eq log = feature \"" + rootFeatureId + "\" exists starts .";
//		
//		return result;
//	}
//	
//	public String translateRootFeatureInInitialState(DwFeature feature) {
//		String featureID = feature.getId();
//		String featureName = feature.getNames().get(0).getName();
//		
//		String parentFeatureId = "noParent";
//		String translatedChildGroups = "noG";
//		String featureType = feature.getTypes().get(0).getType().toString();
//		
//		return "[\"" + featureID + "\" -> f(\"" + featureName + "\", " + parentFeatureId + ", " + translatedChildGroups + ", " + featureType + ")]";
//	}
	
	private StringBuilder logBuilder;
	private boolean translateFMECs;
	
	
	
	public DwTemporalFeatureModelToMaudeTranslator(boolean translateFMECs) {
		this.translateFMECs = translateFMECs;
	}
	
	

	public String translateFeatureModel(DwTemporalFeatureModel featureModel, Date date) {
		logBuilder = new StringBuilder();
		
		String rootFeatureId = DwFeatureUtil.getRootFeature(featureModel, date).getId();
		
		String result = "eq model = FM(\"" + rootFeatureId + "\" ," + System.lineSeparator();
		logBuilder.append("eq log = feature \"" + rootFeatureId + "\" exists starts " + System.lineSeparator());
		
		List<String> featureEntries = new ArrayList<>();
		for(DwFeature currentlyValidFeature : DwEvolutionUtil.getValidTemporalElements(featureModel.getFeatures(), date)) {
			// ALL features are inspected, without respect to feature model hierarchy
			featureEntries.add(translateFeature(currentlyValidFeature, date)); 
		}
		result += String.join(" + " + System.lineSeparator(), featureEntries) + ") ." + System.lineSeparator();
		
		logBuilder.append(" ." + System.lineSeparator());
		
		if(this.translateFMECs)
			return result + logBuilder.toString();
		else
			return result;
	}
	
	public String translateFeature(DwFeature feature, Date date) {
		String featureID = feature.getId();
		String featureName = DwEvolutionUtil.getValidTemporalElement(feature.getNames(), date).getName();
		
		String parentFeatureId;
		String parentGroupId;
		DwFeature rootFeature = feature.getFeatureModel().getRootFeatures().get(0).getFeature();
		if(feature.equals(rootFeature)) {
			// Translate the root feature...!
			parentFeatureId = "noParent";
			
			parentGroupId = "noParent";
		} else {
			DwGroup parentGroup = DwEvolutionUtil.getValidTemporalElement(feature.getGroupMembership(), date).getCompositionOf();
			parentFeatureId = "\"" + DwEvolutionUtil.getValidTemporalElement(parentGroup.getChildOf(), date).getParent().getId() + "\"";
			
			parentGroupId = "\"" + DwFeatureUtil.getParentGroupOfFeature(feature, date).getId() + "\"";
		}
		String translatedChildGroups = translateGroups(feature, date);
		String featureType = DwEvolutionUtil.getValidTemporalElement(feature.getTypes(), date).getType().toString();
		
//		, feature NewFid exists starts 
//		, feature NewFid has name NewName starts 
//		, feature NewFid has type FType starts 
//		, feature NewFid has parent ParentFid starts 
//		, feature NewFid has parent group TargetGroup starts 
		logBuilder.append(" ," + System.lineSeparator() + "feature \"" + featureID + "\" exists starts");
		logBuilder.append(" ," + System.lineSeparator() + "feature \"" + featureID + "\" has name \"" + featureName + "\" starts");
		logBuilder.append(" ," + System.lineSeparator() + "feature \"" + featureID + "\" has type " + featureType + " starts");
		if(!feature.equals(rootFeature)) {
			logBuilder.append(" ," + System.lineSeparator() + "feature \"" + featureID + "\" has parent " + parentFeatureId + " starts");
			logBuilder.append(" ," + System.lineSeparator() + "feature \"" + featureID + "\" has parent group " + parentGroupId + " starts");
		}
		
		return "[\"" + featureID + "\" -> f(\"" + featureName + "\", " + parentFeatureId + ", " + translatedChildGroups + ", " + featureType + ")]";
	}
	
	public String translateGroups(DwFeature parentFeature, Date date) {
		List<String> translatedGroups = new ArrayList<>();
		for(DwParentFeatureChildGroupContainer featureChildRelation : DwEvolutionUtil.getValidTemporalElements(parentFeature.getParentOf(), date)) {
			DwGroup group = featureChildRelation.getChildGroup();
			String type = DwEvolutionUtil.getValidTemporalElement(group.getTypes(), date).getType().toString();
			List<String> featureIDs = new ArrayList<>();
			
			for(DwGroupComposition groupChildRelation : DwEvolutionUtil.getValidTemporalElements(group.getParentOf(), date)) {
				for(DwFeature childFeature : groupChildRelation.getChildFeatures()) {
					featureIDs.add("\"" + childFeature.getId() + "\"");
				}
			}
			String featureIDsString = featureIDs.size() == 0 ? "noF" : String.join(" :: ", featureIDs);
			
//			, group Gid exists starts 
//			, group Gid has type GType starts 
//			, group Gid has parent Fid starts 
			logBuilder.append(" ," + System.lineSeparator() + "group \"" + group.getId() + "\" exists starts");
			logBuilder.append(" ," + System.lineSeparator() + "group \"" + group.getId() + "\" has type " + type + " starts");
			logBuilder.append(" ," + System.lineSeparator() + "group \"" + group.getId() + "\" has parent \"" + parentFeature.getId() + "\" starts");
			
			translatedGroups.add("g(\"" + group.getId() + "\", " + type + ", " + featureIDsString + ")");
		}

		return translatedGroups.size() == 0 ? "noG" : String.join(" :: ", translatedGroups);
	}
	
}









