/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.ui;


/**
 * The preference page to set the occurrence highlighting is not used anymore.
 * This empty class is only generated to override old existing preference page
 * code to avoid compile errors.
 */
public class DwexpressionOccurrencePreferencePage {
}
