/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.ui;

import java.util.List;

/**
 * A class which can be overridden to customize code completion proposals.
 */
public class DwexpressionProposalPostProcessor {
	
	public List<de.darwinspl.expression.resource.dwexpression.ui.DwexpressionCompletionProposal> process(List<de.darwinspl.expression.resource.dwexpression.ui.DwexpressionCompletionProposal> proposals) {
		// the default implementation does returns the proposals as they are
		return proposals;
	}
	
}
