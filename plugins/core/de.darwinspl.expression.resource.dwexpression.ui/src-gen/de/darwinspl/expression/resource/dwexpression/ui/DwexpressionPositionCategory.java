/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.ui;


/**
 * An enumeration of all position categories.
 */
public enum DwexpressionPositionCategory {
	BRACKET, DEFINITION, PROXY;
}
