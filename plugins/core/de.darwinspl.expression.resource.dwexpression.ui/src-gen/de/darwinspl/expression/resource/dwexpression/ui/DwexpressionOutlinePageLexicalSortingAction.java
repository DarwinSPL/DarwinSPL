/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.ui;

import org.eclipse.jface.action.IAction;

public class DwexpressionOutlinePageLexicalSortingAction extends de.darwinspl.expression.resource.dwexpression.ui.AbstractDwexpressionOutlinePageAction {
	
	public DwexpressionOutlinePageLexicalSortingAction(de.darwinspl.expression.resource.dwexpression.ui.DwexpressionOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Sort alphabetically", IAction.AS_CHECK_BOX);
		initialize("icons/sort_lexically_icon.gif");
	}
	
	public void runInternal(boolean on) {
		getTreeViewerComparator().setSortLexically(on);
		getTreeViewer().refresh();
	}
	
}
