/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.ui;

import org.eclipse.core.resources.IResource;

public class DwexpressionUIMetaInformation extends de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionMetaInformation {
	
	public de.darwinspl.expression.resource.dwexpression.IDwexpressionHoverTextProvider getHoverTextProvider() {
		return new de.darwinspl.expression.resource.dwexpression.ui.DwexpressionHoverTextProvider();
	}
	
	public de.darwinspl.expression.resource.dwexpression.ui.DwexpressionImageProvider getImageProvider() {
		return de.darwinspl.expression.resource.dwexpression.ui.DwexpressionImageProvider.INSTANCE;
	}
	
	public de.darwinspl.expression.resource.dwexpression.ui.DwexpressionColorManager createColorManager() {
		return new de.darwinspl.expression.resource.dwexpression.ui.DwexpressionColorManager();
	}
	
	/**
	 * @deprecated this method is only provided to preserve API compatibility. Use
	 * createTokenScanner(de.darwinspl.expression.resource.dwexpression.IDwexpressionTe
	 * xtResource,
	 * de.darwinspl.expression.resource.dwexpression.ui.DwexpressionColorManager)
	 * instead.
	 */
	public de.darwinspl.expression.resource.dwexpression.ui.DwexpressionTokenScanner createTokenScanner(de.darwinspl.expression.resource.dwexpression.ui.DwexpressionColorManager colorManager) {
		return (de.darwinspl.expression.resource.dwexpression.ui.DwexpressionTokenScanner) createTokenScanner(null, colorManager);
	}
	
	public de.darwinspl.expression.resource.dwexpression.ui.IDwexpressionTokenScanner createTokenScanner(de.darwinspl.expression.resource.dwexpression.IDwexpressionTextResource resource, de.darwinspl.expression.resource.dwexpression.ui.DwexpressionColorManager colorManager) {
		return new de.darwinspl.expression.resource.dwexpression.ui.DwexpressionTokenScanner(resource, colorManager);
	}
	
	public de.darwinspl.expression.resource.dwexpression.ui.DwexpressionCodeCompletionHelper createCodeCompletionHelper() {
		return new de.darwinspl.expression.resource.dwexpression.ui.DwexpressionCodeCompletionHelper();
	}
	
	@SuppressWarnings("rawtypes")
	public Object createResourceAdapter(Object adaptableObject, Class adapterType, IResource resource) {
		return new de.darwinspl.expression.resource.dwexpression.ui.debug.DwexpressionLineBreakpointAdapter();
	}
	
}
