/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.ui;

import org.eclipse.jface.text.rules.ITokenScanner;

public interface IDwexpressionTokenScanner extends ITokenScanner {
	
	public String getTokenText();
	
}
