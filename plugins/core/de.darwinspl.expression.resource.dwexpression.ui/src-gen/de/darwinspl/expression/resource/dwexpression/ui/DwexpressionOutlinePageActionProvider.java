/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.ui;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.jface.action.IAction;

public class DwexpressionOutlinePageActionProvider {
	
	public List<IAction> getActions(de.darwinspl.expression.resource.dwexpression.ui.DwexpressionOutlinePageTreeViewer treeViewer) {
		// To add custom actions to the outline view, set the
		// 'overrideOutlinePageActionProvider' option to <code>false</code> and modify
		// this method.
		List<IAction> defaultActions = new ArrayList<IAction>();
		defaultActions.add(new de.darwinspl.expression.resource.dwexpression.ui.DwexpressionOutlinePageLinkWithEditorAction(treeViewer));
		defaultActions.add(new de.darwinspl.expression.resource.dwexpression.ui.DwexpressionOutlinePageCollapseAllAction(treeViewer));
		defaultActions.add(new de.darwinspl.expression.resource.dwexpression.ui.DwexpressionOutlinePageExpandAllAction(treeViewer));
		defaultActions.add(new de.darwinspl.expression.resource.dwexpression.ui.DwexpressionOutlinePageAutoExpandAction(treeViewer));
		defaultActions.add(new de.darwinspl.expression.resource.dwexpression.ui.DwexpressionOutlinePageLexicalSortingAction(treeViewer));
		defaultActions.add(new de.darwinspl.expression.resource.dwexpression.ui.DwexpressionOutlinePageTypeSortingAction(treeViewer));
		return defaultActions;
	}
	
}
