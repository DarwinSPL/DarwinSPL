/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.ui;


/**
 * This class is only generated for backwards compatiblity. The original contents
 * of this class have been moved to class
 * de.darwinspl.expression.resource.dwexpression.mopp.DwexpressionAntlrTokenHelper.
 */
public class DwexpressionAntlrTokenHelper {
	// This class is intentionally left empty.
}
