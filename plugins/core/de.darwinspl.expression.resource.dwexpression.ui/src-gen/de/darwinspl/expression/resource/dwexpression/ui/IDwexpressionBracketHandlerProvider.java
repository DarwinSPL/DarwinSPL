/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.ui;


/**
 * A provider for BracketHandler objects.
 */
public interface IDwexpressionBracketHandlerProvider {
	
	/**
	 * Returns the bracket handler.
	 */
	public de.darwinspl.expression.resource.dwexpression.ui.IDwexpressionBracketHandler getBracketHandler();
	
}
