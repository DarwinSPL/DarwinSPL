/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.ui;

import org.eclipse.jface.action.IAction;

public class DwexpressionOutlinePageExpandAllAction extends de.darwinspl.expression.resource.dwexpression.ui.AbstractDwexpressionOutlinePageAction {
	
	public DwexpressionOutlinePageExpandAllAction(de.darwinspl.expression.resource.dwexpression.ui.DwexpressionOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Expand all", IAction.AS_PUSH_BUTTON);
		initialize("icons/expand_all_icon.gif");
	}
	
	public void runInternal(boolean on) {
		if (on) {
			getTreeViewer().expandAll();
		}
	}
	
	public boolean keepState() {
		return false;
	}
	
}
