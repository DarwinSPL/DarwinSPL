/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.ui;

import org.eclipse.jface.action.IAction;

public class DwexpressionOutlinePageTypeSortingAction extends de.darwinspl.expression.resource.dwexpression.ui.AbstractDwexpressionOutlinePageAction {
	
	public DwexpressionOutlinePageTypeSortingAction(de.darwinspl.expression.resource.dwexpression.ui.DwexpressionOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Group types", IAction.AS_CHECK_BOX);
		initialize("icons/group_types_icon.gif");
	}
	
	public void runInternal(boolean on) {
		getTreeViewerComparator().setGroupTypes(on);
		getTreeViewer().refresh();
	}
	
}
