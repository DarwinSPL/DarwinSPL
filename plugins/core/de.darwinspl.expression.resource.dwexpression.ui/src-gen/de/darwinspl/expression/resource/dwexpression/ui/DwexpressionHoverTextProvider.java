/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.expression.resource.dwexpression.ui;

import org.eclipse.emf.ecore.EObject;

public class DwexpressionHoverTextProvider implements de.darwinspl.expression.resource.dwexpression.IDwexpressionHoverTextProvider {
	
	private de.darwinspl.expression.resource.dwexpression.ui.DwexpressionDefaultHoverTextProvider defaultProvider = new de.darwinspl.expression.resource.dwexpression.ui.DwexpressionDefaultHoverTextProvider();
	
	public String getHoverText(EObject container, EObject referencedObject) {
		// Set option overrideHoverTextProvider to false and customize this method to
		// obtain custom hover texts.
		return defaultProvider.getHoverText(referencedObject);
	}
	
	public String getHoverText(EObject object) {
		// Set option overrideHoverTextProvider to false and customize this method to
		// obtain custom hover texts.
		return defaultProvider.getHoverText(object);
	}
	
}
