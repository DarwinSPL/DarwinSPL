package de.darwinspl.feature.evolution.operation.custom;

import de.darwinspl.feature.evolution.operation.custom.operations.DwFeatureAddOperationCustom;
import de.darwinspl.feature.evolution.operation.custom.operations.DwFeatureCreateOperationCustom;
import de.darwinspl.feature.evolution.operation.custom.operations.DwFeatureDeleteOperationCustom;
import de.darwinspl.feature.evolution.operation.custom.operations.DwFeatureDetachOperationCustom;
import de.darwinspl.feature.evolution.operation.custom.operations.DwFeatureMoveOperationCustom;
import de.darwinspl.feature.evolution.operation.custom.operations.DwFeatureRenameOperationCustom;
import de.darwinspl.feature.evolution.operation.custom.operations.DwFeatureTypeChangeOperationCustom;
import de.darwinspl.feature.evolution.operation.custom.operations.DwGroupAddOperationCustom;
import de.darwinspl.feature.evolution.operation.custom.operations.DwGroupCreateOperationCustom;
import de.darwinspl.feature.evolution.operation.custom.operations.DwGroupDeleteOperationCustom;
import de.darwinspl.feature.evolution.operation.custom.operations.DwGroupDetachOperationCustom;
import de.darwinspl.feature.evolution.operation.custom.operations.DwGroupMoveOperationCustom;
import de.darwinspl.feature.evolution.operation.custom.operations.DwGroupTypeChangeOperationCustom;
import de.darwinspl.feature.operation.DwFeatureAddOperation;
import de.darwinspl.feature.operation.DwFeatureCreateOperation;
import de.darwinspl.feature.operation.DwFeatureDeleteOperation;
import de.darwinspl.feature.operation.DwFeatureDetachOperation;
import de.darwinspl.feature.operation.DwFeatureMoveOperation;
import de.darwinspl.feature.operation.DwFeatureRenameOperation;
import de.darwinspl.feature.operation.DwFeatureTypeChangeOperation;
import de.darwinspl.feature.operation.DwGroupAddOperation;
import de.darwinspl.feature.operation.DwGroupCreateOperation;
import de.darwinspl.feature.operation.DwGroupDeleteOperation;
import de.darwinspl.feature.operation.DwGroupDetachOperation;
import de.darwinspl.feature.operation.DwGroupMoveOperation;
import de.darwinspl.feature.operation.DwGroupTypeChangeOperation;
import de.darwinspl.feature.operation.impl.DwOperationFactoryImpl;

public class DwOperationFactoryCustom extends DwOperationFactoryImpl {
	
	@Override
	public DwFeatureAddOperation createDwFeatureAddOperation() {
		return new DwFeatureAddOperationCustom();
	}
	
	@Override
	public DwFeatureCreateOperation createDwFeatureCreateOperation() {
		return new DwFeatureCreateOperationCustom();
	}
	
	@Override
	public DwFeatureDeleteOperation createDwFeatureDeleteOperation() {
		return new DwFeatureDeleteOperationCustom();
	}

	@Override
	public DwFeatureDetachOperation createDwFeatureDetachOperation() {
		return new DwFeatureDetachOperationCustom();
	}

	@Override
	public DwFeatureMoveOperation createDwFeatureMoveOperation() {
		return new DwFeatureMoveOperationCustom();
	}

	@Override
	public DwFeatureRenameOperation createDwFeatureRenameOperation() {
		return new DwFeatureRenameOperationCustom();
	}

	@Override
	public DwFeatureTypeChangeOperation createDwFeatureTypeChangeOperation() {
		return new DwFeatureTypeChangeOperationCustom();
	}

	@Override
	public DwGroupCreateOperation createDwGroupCreateOperation() {
		return new DwGroupCreateOperationCustom();
	}

	@Override
	public DwGroupAddOperation createDwGroupAddOperation() {
		return new DwGroupAddOperationCustom();
	}

	@Override
	public DwGroupDeleteOperation createDwGroupDeleteOperation() {
		return new DwGroupDeleteOperationCustom();
	}

	@Override
	public DwGroupDetachOperation createDwGroupDetachOperation() {
		return new DwGroupDetachOperationCustom();
	}

	@Override
	public DwGroupMoveOperation createDwGroupMoveOperation() {
		return new DwGroupMoveOperationCustom();
	}

	@Override
	public DwGroupTypeChangeOperation createDwGroupTypeChangeOperation() {
		return new DwGroupTypeChangeOperationCustom();
	}
	
}
