package de.darwinspl.feature.evolution.operation.interpreter;

import de.darwinspl.feature.DwFeatureAttribute;
import de.darwinspl.feature.operation.DwEvolutionOperation;
import de.darwinspl.feature.operation.DwFeatureAttributeCreateOperation;
import de.darwinspl.feature.operation.DwFeatureAttributeDeleteOperation;
import de.darwinspl.feature.operation.DwFeatureAttributeRenameOperation;

/**
 * This class contains the actual operation to execute and undo evolution operations on {@link DwFeatureAttribute}s.
 * The {@link #execute(DwEvolutionOperation)} and {@link #undo(DwEvolutionOperation)} methods delegate to their respective handler methods depending on the type of evolution Operation.
 * This class should not be used directly to call execute operations. Please use the {@link DwEvolutionOperationInterpreter} class' execute and undo method instead.
 *
 */
public class DwAttributeOperationInterpreter {

//	private static final DwOperationFactory operationFactory = DwOperationFactory.eINSTANCE;
//	private static final DwFeatureFactory featureFactory = DwFeatureFactory.eINSTANCE;

	public static void execute(DwEvolutionOperation evoOp) {
		if (evoOp instanceof DwFeatureAttributeCreateOperation)
			handleExecute((DwFeatureAttributeCreateOperation) evoOp);

		else if (evoOp instanceof DwFeatureAttributeDeleteOperation)
			handleExecute((DwFeatureAttributeDeleteOperation) evoOp);

		else if (evoOp instanceof DwFeatureAttributeRenameOperation)
			handleExecute((DwFeatureAttributeRenameOperation) evoOp);

	}

	public static void undo(DwEvolutionOperation evoOp) {
		if (evoOp instanceof DwFeatureAttributeCreateOperation)
			handleUndo((DwFeatureAttributeCreateOperation) evoOp);

		else if (evoOp instanceof DwFeatureAttributeDeleteOperation)
			handleUndo((DwFeatureAttributeDeleteOperation) evoOp);

		else if (evoOp instanceof DwFeatureAttributeRenameOperation)
			handleUndo((DwFeatureAttributeRenameOperation) evoOp);
	}

	private static void handleExecute(DwFeatureAttributeCreateOperation evoOp) {
		// TODO Auto-generated method stub

	}

	private static void handleUndo(DwFeatureAttributeCreateOperation evoOp) {
		// TODO Auto-generated method stub
	
	}

	private static void handleExecute(DwFeatureAttributeDeleteOperation evoOp) {
		// TODO Auto-generated method stub

	}

	private static void handleUndo(DwFeatureAttributeDeleteOperation evoOp) {
		// TODO Auto-generated method stub
	
	}

	private static void handleExecute(DwFeatureAttributeRenameOperation evoOp) {
		// TODO Auto-generated method stub

	}

	private static void handleUndo(DwFeatureAttributeRenameOperation evoOp) {
		// TODO Auto-generated method stub

	}
}
