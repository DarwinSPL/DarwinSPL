package de.darwinspl.feature.evolution.operation.interpreter;

import java.util.Date;

import de.darwinspl.common.eclipse.util.DwLogger;
import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupType;
import de.darwinspl.feature.DwParentFeatureChildGroupContainer;
import de.darwinspl.feature.operation.DwEvolutionOperation;
import de.darwinspl.feature.operation.DwFeatureDeleteOperation;
import de.darwinspl.feature.operation.DwFeatureTypeChangeOperation;
import de.darwinspl.feature.operation.DwGroupAddOperation;
import de.darwinspl.feature.operation.DwGroupCreateOperation;
import de.darwinspl.feature.operation.DwGroupDeleteOperation;
import de.darwinspl.feature.operation.DwGroupDetachOperation;
import de.darwinspl.feature.operation.DwGroupMoveOperation;
import de.darwinspl.feature.operation.DwGroupTypeChangeOperation;
import de.darwinspl.feature.util.custom.DwSimpleFeatureFactoryCustom;
import de.darwinspl.feature.util.custom.DwFeatureUtil;
import de.darwinspl.temporal.DwTemporalInterval;
import de.darwinspl.temporal.util.DwEvolutionUtil;

/**
 * This class contains the actual operation to execute and undo evolution operations on {@link DwGroup}s.
 * The {@link #execute(DwEvolutionOperation)} and {@link #undo(DwEvolutionOperation)} methods delegate to their respective handler methods depending on the type of evolution Operation.
 * This class should not be used directly to call execute operations. Please use the {@link DwEvolutionOperationInterpreter} class' execute and undo method instead.
 *
 */
public class DwGroupOperationInterpreter {

	public static void execute(DwEvolutionOperation evoOp) {
		if (evoOp instanceof DwGroupCreateOperation)
			handleExecute((DwGroupCreateOperation) evoOp);

		else if (evoOp instanceof DwGroupAddOperation)
			handleExecute((DwGroupAddOperation) evoOp);

		else if (evoOp instanceof DwGroupDeleteOperation)
			handleExecute((DwGroupDeleteOperation) evoOp);

		else if (evoOp instanceof DwGroupDetachOperation)
			handleExecute((DwGroupDetachOperation) evoOp);

		else if (evoOp instanceof DwGroupTypeChangeOperation)
			handleExecute((DwGroupTypeChangeOperation) evoOp);

		else if (evoOp instanceof DwGroupMoveOperation)
			handleExecute((DwGroupMoveOperation) evoOp);
	}

	public static void undo(DwEvolutionOperation evoOp) {
		if (evoOp instanceof DwGroupCreateOperation)
			handleUndo((DwGroupCreateOperation) evoOp);

		else if (evoOp instanceof DwGroupAddOperation)
			handleUndo((DwGroupAddOperation) evoOp);

		else if (evoOp instanceof DwGroupDeleteOperation)
			handleUndo((DwGroupDeleteOperation) evoOp);

		else if (evoOp instanceof DwGroupDetachOperation)
			handleUndo((DwGroupDetachOperation) evoOp);

		else if (evoOp instanceof DwGroupTypeChangeOperation)
			handleUndo((DwGroupTypeChangeOperation) evoOp);

		else if (evoOp instanceof DwGroupMoveOperation)
			handleUndo((DwGroupMoveOperation) evoOp);
	}
	
	/*
	 * 		CREATE
	 */

	private static void handleExecute(DwGroupCreateOperation evoOp) {
		evoOp.getFeatureModel().getGroups().add(evoOp.getGroup());
		DwEvolutionOperationInterpreter.executeWithoutAnalysis(evoOp.getGroupAddOperation());
	}

	private static void handleUndo(DwGroupCreateOperation evoOp) {
		DwEvolutionOperationInterpreter.undo(evoOp.getGroupAddOperation());
		evoOp.getFeatureModel().getGroups().remove(evoOp.getGroup());
	}
	
	/*
	 * 		ADD
	 */

	private static void handleExecute(DwGroupAddOperation evoOp) {
		DwGroup group = evoOp.getGroup();
		DwFeature parentFeature = evoOp.getParentFeature();
		Date date = evoOp.getOperationDate();
		
		DwParentFeatureChildGroupContainer container = DwSimpleFeatureFactoryCustom.createNewParentFeatureChildGroupContainer(parentFeature, date);
		container.setChildGroup(group);
		DwEvolutionUtil.setTemporalValidity(container, date, DwEvolutionUtil.getValidIntervalForDate(group, date).getTo());
		
		evoOp.setNewContainer(container);
	}

	private static void handleUndo(DwGroupAddOperation evoOp) {
		DwParentFeatureChildGroupContainer container = evoOp.getNewContainer();
		container.setChildGroup(null);
		container.setParent(null);
	}
	
	/*
	 * 		DELETE
	 */

	private static void handleExecute(DwGroupDeleteOperation evoOp) {
		for(DwFeatureDeleteOperation consequentOperation : evoOp.getConsequentFeatureOperations())
			DwEvolutionOperationInterpreter.executeWithoutAnalysis(consequentOperation);
		
		DwEvolutionOperationInterpreter.executeWithoutAnalysis(evoOp.getDetachOperation());
		
		Date date = evoOp.getOperationDate();
		DwGroup group = evoOp.getGroup();
		
		DwTemporalInterval validGroupInterval = DwEvolutionUtil.getValidIntervalForDate(group, date);
		if(validGroupInterval == null) {
			DwLogger.logWarning("Deletion of a Group failed: No valid temporal interval available. This was probably caused by an unvalid operation.");
			return;
		}
		
		evoOp.setOldGroupValidUntil(validGroupInterval.getTo());

		DwTemporalInterval groupValidity = DwEvolutionUtil.getValidIntervalForDate(group, date);
		groupValidity.setTo(date);

		if (!DwEvolutionUtil.isEverValid(group)) {
			// TODO this will cause references to uncontained groups in the operation model
//			evoOp.getFeatureModel().getGroups().remove(group);
		}
	}

	private static void handleUndo(DwGroupDeleteOperation evoOp) {
		Date date = evoOp.getOperationDate();
		DwGroup group = evoOp.getGroup();
		DwTemporalInterval groupInterval = DwEvolutionUtil.getIntervalForEndDate(group, date);
		if(groupInterval != null)
			groupInterval.setTo(evoOp.getOldGroupValidUntil());

		if (group.getFeatureModel() == null) {
			evoOp.getFeatureModel().getGroups().add(group);
		}
		
		DwEvolutionOperationInterpreter.undo(evoOp.getDetachOperation());
		
		for(int i=evoOp.getConsequentFeatureOperations().size()-1; i >= 0; i--)
			DwEvolutionOperationInterpreter.undo(evoOp.getConsequentFeatureOperations().get(i));
	}
	
	/*
	 * 		DETACH
	 */

	private static void handleExecute(DwGroupDetachOperation evoOp) {
		DwGroup group = evoOp.getGroup();
		Date date = evoOp.getOperationDate();

		DwParentFeatureChildGroupContainer oldContainer = DwEvolutionUtil.getValidTemporalElement(group.getChildOf(), date);

		if (oldContainer != null) {
			evoOp.setOldContainer(oldContainer);
			oldContainer.getValidity().setTo(date);

			if (!DwEvolutionUtil.isEverValid(oldContainer)) {
				evoOp.setOldParent(oldContainer.getParent());
				oldContainer.setChildGroup(null);
				oldContainer.setParent(null);
			}
		}
	}

	private static void handleUndo(DwGroupDetachOperation evoOp) {
		DwGroup group = evoOp.getGroup();
		DwParentFeatureChildGroupContainer oldContainer = evoOp.getOldContainer();
		Date date = evoOp.getOperationDate();

		if (oldContainer != null) {
			oldContainer.getValidity().setTo(DwEvolutionUtil.getValidIntervalForDate(group, date).getTo());

			if (oldContainer.getChildGroup() == null) {
				oldContainer.setChildGroup(group);
				oldContainer.setParent(evoOp.getOldParent());
			}
		}
	}
	
	/*
	 * 		TYPE CHANGE
	 */

	private static void handleExecute(DwGroupTypeChangeOperation evoOp) {
		DwGroup group = evoOp.getGroup();
		Date date = evoOp.getOperationDate();
		
		for(DwFeatureTypeChangeOperation consequentOperation : evoOp.getConsequentOperations()) {
			DwEvolutionOperationInterpreter.executeWithoutAnalysis(consequentOperation);
		}

		DwGroupType oldGroupType = DwFeatureUtil.getType(group, date);

		DwGroupType newGroupType = DwSimpleFeatureFactoryCustom.createNewGroupType(evoOp.getNewGroupTypeEnum());
		DwEvolutionUtil.setTemporalValidity(newGroupType, date, DwEvolutionUtil.getValidIntervalForDate(oldGroupType, date).getTo());
		evoOp.setNewGroupType(newGroupType);
		
		newGroupType.setGoup(group);
		
		if (oldGroupType != null) {
			evoOp.setOldGroupType(oldGroupType);

			oldGroupType.getValidity().setTo(date);

			if ((oldGroupType.getFrom() == null && oldGroupType.getTo() == null) || !DwEvolutionUtil.isEverValid(oldGroupType)) {
				oldGroupType.setGoup(null);
			}
		}
	}

	private static void handleUndo(DwGroupTypeChangeOperation evoOp) {
		Date date = evoOp.getOperationDate();
		DwGroup group = evoOp.getGroup();
		DwGroupType newGroupType = evoOp.getNewGroupType();
		
//		newGroupType.setGoup(null);
		group.getTypes().remove(newGroupType);

		DwGroupType oldGroupType = evoOp.getOldGroupType();

		if (oldGroupType != null) {
			oldGroupType.getValidity().setTo(DwEvolutionUtil.getValidIntervalForDate(group, date).getTo());

			if (oldGroupType.getGoup() == null) {
				oldGroupType.setGoup(group);
			}
		}
		
		for(DwFeatureTypeChangeOperation consequentOperation : evoOp.getConsequentOperations()) {
			DwEvolutionOperationInterpreter.undo(consequentOperation);
		}
	}
	
	/*
	 * 		MOVE
	 */

	private static void handleExecute(DwGroupMoveOperation evoOp) {
		handleExecute(evoOp.getGroupDetachOperation());
		handleExecute(evoOp.getGroupAddOperation());
	}

	private static void handleUndo(DwGroupMoveOperation evoOp) {
		handleUndo(evoOp.getGroupAddOperation());
		handleUndo(evoOp.getGroupDetachOperation());
	}
}
