package de.darwinspl.feature.evolution.operation.interpreter;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.util.EcoreUtil;

import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.operation.DwEvolutionOperation;

public class DwUndoHandler {
	
	
	
	private static Map<DwEvolutionOperation, DwTemporalFeatureModel> oldVersions = new HashMap<>();
	
	
	
	public static void store(DwEvolutionOperation op) {
		DwTemporalFeatureModel tfm = op.getFeatureModel();
		oldVersions.put(op, EcoreUtil.copy(tfm));
	}
	
	
	
	// TODO
	// This is a poor man's implementation that serves as temporary fix
	// for sophisticated and performant undo functionality!
	public static void undo(DwEvolutionOperation op) {
		DwTemporalFeatureModel tfm = op.getFeatureModel();
		DwTemporalFeatureModel oldTfm = oldVersions.get(op);

		EcoreUtil.removeAll(tfm.getEnums());
		EcoreUtil.removeAll(tfm.getFeatures());
		EcoreUtil.removeAll(tfm.getGroups());
		EcoreUtil.removeAll(tfm.getRootFeatures());

		tfm.getEnums().addAll(oldTfm.getEnums());
		tfm.getFeatures().addAll(oldTfm.getFeatures());
		tfm.getGroups().addAll(oldTfm.getGroups());
		tfm.getRootFeatures().addAll(oldTfm.getRootFeatures());
	}

}
