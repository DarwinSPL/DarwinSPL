package de.darwinspl.feature.evolution.operation.custom.operations;

import de.darwinspl.feature.evolution.operation.custom.DwOperationStringUtil;
import de.darwinspl.feature.operation.impl.DwFeatureRenameOperationImpl;

public class DwFeatureRenameOperationCustom extends DwFeatureRenameOperationImpl {
	
	@Override
	public String toString() {
		String dateAndContributorInfo = DwOperationStringUtil.getContributorAndDateInfo(this);
		
		StringBuilder infoBuilder = new StringBuilder();
		infoBuilder.append("Feature Rename Operation".toUpperCase());
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(dateAndContributorInfo);
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(DwOperationStringUtil.printInfo("Feature to Rename", DwOperationStringUtil.getInfo(this.getFeature())));
		infoBuilder.append(System.lineSeparator());

		infoBuilder.append(DwOperationStringUtil.printInfo("Old Name", (this.getOldNameString())));
		infoBuilder.append(DwOperationStringUtil.printInfo("New Name", (this.getNewNameString())));
		
		return infoBuilder.toString();
	}

}
