package de.darwinspl.feature.evolution.operation.custom.operations;

import de.darwinspl.feature.evolution.operation.custom.DwOperationStringUtil;
import de.darwinspl.feature.operation.impl.DwGroupAddOperationImpl;

public class DwGroupAddOperationCustom extends DwGroupAddOperationImpl {
	
	@Override
	public String toString() {
		String dateAndContributorInfo = DwOperationStringUtil.getContributorAndDateInfo(this);
		
		StringBuilder infoBuilder = new StringBuilder();
		infoBuilder.append("Group Add Operation".toUpperCase());
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(dateAndContributorInfo);
		infoBuilder.append(System.lineSeparator());

		infoBuilder.append(DwOperationStringUtil.printInfo("Group to Add", DwOperationStringUtil.getInfo(this.getGroup())));
		infoBuilder.append(System.lineSeparator());

		infoBuilder.append(DwOperationStringUtil.printInfo("New Parent Feature", DwOperationStringUtil.getInfo(this.getParentFeature())));

		return infoBuilder.toString();
	}

}
