package de.darwinspl.feature.evolution.operation.interpreter.extensions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;

import de.darwinspl.feature.operation.DwEvolutionOperation;

public class DwEvolutionOperationInterpreterExtensionRunner {
	
	private static class ExtensionInfo {
		public String name;
		public String requires;
		
		public ExtensionInfo requiredInfo;
		public DwEvolutionOperationIntepreterExtension executable;
	}
	
	
	
	private static final String EXTENSION_POINT_ID = "de.darwinspl.feature.evolution.operation.preexecution_extension";
	private List<ExtensionInfo> preExecutionExtensionInfo;
	
	
	
	private void init() throws CoreException {
		List<ExtensionInfo> infos = new ArrayList<>();
		IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor(EXTENSION_POINT_ID);
		
        for (IConfigurationElement e : config) {
        	ExtensionInfo newInfo = new ExtensionInfo();
        	
        	newInfo.name = e.getAttribute("name");
            if(newInfo.name == null)
            	newInfo.name = "<Unspecified>";
            newInfo.requires = e.getAttribute("requires");
            
            final Object o = e.createExecutableExtension("implementation");
            if (o instanceof DwEvolutionOperationIntepreterExtension) {
            	newInfo.executable = (DwEvolutionOperationIntepreterExtension) o;
            	newInfo.executable.init();
            	
            	infos.add(newInfo);
            }
        }
        
        // Resolve references within extensions
        resolveReferences(infos);
        
        // Detect circles
		if(containsCircles(infos))
			throw new RuntimeException("Detected a circular dependency in evolution operation interpreter extensions.");
        
        // Set sorted extensions
		preExecutionExtensionInfo = sortInfos(infos);
	}

	private void resolveReferences(List<ExtensionInfo> infos) {
		for(ExtensionInfo outerInfo : infos) {
        	if(outerInfo.requires != null) {
        		for(ExtensionInfo innerInfo : infos) {
        			if(outerInfo.equals(innerInfo)) {
        				continue;
        			}
        			if(outerInfo.requires.equals(innerInfo.name)) {
        				outerInfo.requiredInfo = innerInfo;
        				continue;
        			}
        		}
        		
        		if(outerInfo.requiredInfo == null)
        			throw new RuntimeException("Could not resolve the requirement \"" + outerInfo.requires + "\" of client \"" + outerInfo.name + "\".");
        	}
        }
	}
	
	private boolean containsCircles(List<ExtensionInfo> infos) {
		for(ExtensionInfo outerInfo : infos) {
        	ExtensionInfo innerInfo = outerInfo.requiredInfo;
        	while(innerInfo != null) {
        		if(innerInfo.equals(outerInfo))
        			return true;
        		innerInfo = innerInfo.requiredInfo;
        	}
        }
		
		return false;
	}
	
	private List<ExtensionInfo> sortInfos(List<ExtensionInfo> unsortedInfos) {
		List<ExtensionInfo> sortedInfos = new ArrayList<>();
		
		while(sortedInfos.size() != unsortedInfos.size()) {
			for(ExtensionInfo info : unsortedInfos) {
				if(info.requiredInfo == null)
					sortedInfos.add(info);
				else if(sortedInfos.contains(info.requiredInfo))
					sortedInfos.add(info);
			}
		}
		
		return sortedInfos;
	}
	
	
	
	public boolean runPreExecutionExtensions(DwEvolutionOperation evoOp, String additionalInfo) {
		if(preExecutionExtensionInfo == null) {
			try {
				init();
			} catch(RuntimeException e) {
				preExecutionExtensionInfo = new ArrayList<>();
				throw e;
			} catch (CoreException e) {
				preExecutionExtensionInfo = new ArrayList<>();
				e.printStackTrace();
			}
		}
		
		boolean success = true;
		for(ExtensionInfo triple : preExecutionExtensionInfo) {
			if(!triple.executable.run(evoOp, additionalInfo))
				success = false;
		}
		
		return success;
	}

	
}
