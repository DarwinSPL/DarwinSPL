package de.darwinspl.feature.evolution.operation.custom;

import org.eclipse.emf.ecore.EObject;

import de.darwinspl.feature.contributor.DwContributor;
import de.darwinspl.feature.operation.DwEvolutionOperation;
import de.darwinspl.temporal.util.DwDateResolverUtil;

public class DwOperationStringUtil {
	
	public static String getContributorAndDateInfo(DwEvolutionOperation operation) {
		String dateInfo = DwDateResolverUtil.deresolveDate(operation.getOperationDate());
		String contributorInfo = DwOperationStringUtil.getInfo(operation.getContributor());
		
		return indent(dateInfo + System.lineSeparator() + contributorInfo, "  ");
	}

	public static String getInfo(DwContributor contributor) {
		return contributor.toString();
//		return "Contributor: " + contributor.getName() + " (" + contributor.getId() + ")";
	}

	public static String getInfo(EObject object) {
		return object.toString();
//		if(object instanceof DwFeature)
//			return getInfo((DwFeature) object);
//		
//		throw new UnsupportedOperationException("Unkown kind of parent...");
	}
	
	
	
	public static String printContainedOperation(String name, DwEvolutionOperation operation) {
		if(operation == null) {
			return printInfo(name, null);
		}
		else {
			return printInfo(name, operation.toString());
		}
	}
	
	public static String printInfo(String name, String info) {
		String result = "  " + name + ":" + System.lineSeparator();
		if(info == null) {
			return result + indent("<unset>", "    ");
		}
		else {
			return result + indent(info, "    ");
		}
	}
	
	private static String indent(String str, String indention) {
		StringBuilder output = new StringBuilder();
		for(String line : str.split(System.lineSeparator())) {
			output.append(indention);
			output.append(line);
			output.append(System.lineSeparator());
		}
		return output.toString();
	}

}
