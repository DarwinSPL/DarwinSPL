package de.darwinspl.feature.evolution.operation.custom.operations;

import de.darwinspl.feature.evolution.operation.custom.DwOperationStringUtil;
import de.darwinspl.feature.operation.impl.DwFeatureMoveOperationImpl;

public class DwFeatureMoveOperationCustom extends DwFeatureMoveOperationImpl {
	
	@Override
	public String toString() {
		String dateAndContributorInfo = DwOperationStringUtil.getContributorAndDateInfo(this);
		
		StringBuilder infoBuilder = new StringBuilder();
		infoBuilder.append("Feature Move Operation".toUpperCase());
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(dateAndContributorInfo);
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(DwOperationStringUtil.printInfo("Feature to Move", DwOperationStringUtil.getInfo(this.getFeature())));
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(DwOperationStringUtil.printContainedOperation("Triggered Feature Detach Operation", this.getDetachOperation()));
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(DwOperationStringUtil.printContainedOperation("Triggered Feature Add Operation", this.getAddOperation()));
		
		return infoBuilder.toString();
	}

}
