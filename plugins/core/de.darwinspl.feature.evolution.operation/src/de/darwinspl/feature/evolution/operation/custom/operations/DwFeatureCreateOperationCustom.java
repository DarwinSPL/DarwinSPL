package de.darwinspl.feature.evolution.operation.custom.operations;

import de.darwinspl.feature.evolution.operation.custom.DwOperationStringUtil;
import de.darwinspl.feature.operation.impl.DwFeatureCreateOperationImpl;

public class DwFeatureCreateOperationCustom extends DwFeatureCreateOperationImpl {
	
	@Override
	public String toString() {
		String dateAndContributorInfo = DwOperationStringUtil.getContributorAndDateInfo(this);
		
		StringBuilder infoBuilder = new StringBuilder();
		infoBuilder.append("Feature Create Operation".toUpperCase());
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(dateAndContributorInfo);
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(DwOperationStringUtil.printInfo("Created Feature", DwOperationStringUtil.getInfo(this.getFeature())));
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(DwOperationStringUtil.printContainedOperation("Triggered Feature Add Operation", this.getFeatureAddOperation()));
		
		return infoBuilder.toString();
	}

}
