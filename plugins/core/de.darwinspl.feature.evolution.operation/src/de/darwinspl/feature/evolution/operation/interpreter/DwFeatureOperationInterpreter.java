package de.darwinspl.feature.evolution.operation.interpreter;

import java.util.Date;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.darwinspl.common.eclipse.util.DwLogger;
import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureType;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupComposition;
import de.darwinspl.feature.DwName;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.evolution.operation.interpreter.extensions.DwEvolutionOperationExecutionException;
import de.darwinspl.feature.operation.DwEvolutionOperation;
import de.darwinspl.feature.operation.DwFeatureAddOperation;
import de.darwinspl.feature.operation.DwFeatureCreateOperation;
import de.darwinspl.feature.operation.DwFeatureDeleteOperation;
import de.darwinspl.feature.operation.DwFeatureDetachOperation;
import de.darwinspl.feature.operation.DwFeatureMoveOperation;
import de.darwinspl.feature.operation.DwFeatureRenameOperation;
import de.darwinspl.feature.operation.DwFeatureTypeChangeOperation;
import de.darwinspl.feature.operation.DwGroupDeleteOperation;
import de.darwinspl.feature.util.custom.DwSimpleFeatureFactoryCustom;
import de.darwinspl.feature.util.custom.DwFeatureUtil;
import de.darwinspl.temporal.DwTemporalInterval;
import de.darwinspl.temporal.util.DwDateResolverUtil;
import de.darwinspl.temporal.util.DwEvolutionUtil;

/**
 * This class contains the actual operation to execute and undo evolution operations on {@link DwFeature}s.
 * The {@link #execute(DwEvolutionOperation)} and {@link #undo(DwEvolutionOperation)} methods delegate to their respective handler methods depending on the type of evolution Operation.
 * This class should not be used directly to call execute operations. Please use the {@link DwEvolutionOperationInterpreter} class' execute and undo method instead.
 *
 */
public class DwFeatureOperationInterpreter {

	public static void execute(DwEvolutionOperation evoOp) {
		if (evoOp instanceof DwFeatureCreateOperation)
			handleExecute((DwFeatureCreateOperation) evoOp);

		else if (evoOp instanceof DwFeatureAddOperation)
			handleExecute((DwFeatureAddOperation) evoOp);

		else if (evoOp instanceof DwFeatureDeleteOperation)
			handleExecute((DwFeatureDeleteOperation) evoOp);

		else if (evoOp instanceof DwFeatureDetachOperation)
			handleExecute((DwFeatureDetachOperation) evoOp);

		else if (evoOp instanceof DwFeatureMoveOperation)
			handleExecute((DwFeatureMoveOperation) evoOp);

		else if (evoOp instanceof DwFeatureRenameOperation)
			handleExecute((DwFeatureRenameOperation) evoOp);

		else if (evoOp instanceof DwFeatureTypeChangeOperation)
			handleExecute((DwFeatureTypeChangeOperation) evoOp);
	}

	public static void undo(DwEvolutionOperation evoOp) {
		if (evoOp instanceof DwFeatureCreateOperation)
			handleUndo((DwFeatureCreateOperation) evoOp);

		else if (evoOp instanceof DwFeatureAddOperation)
			handleUndo((DwFeatureAddOperation) evoOp);

		else if (evoOp instanceof DwFeatureDeleteOperation)
			handleUndo((DwFeatureDeleteOperation) evoOp);

		else if (evoOp instanceof DwFeatureDetachOperation)
			handleUndo((DwFeatureDetachOperation) evoOp);

		else if (evoOp instanceof DwFeatureMoveOperation)
			handleUndo((DwFeatureMoveOperation) evoOp);

		else if (evoOp instanceof DwFeatureRenameOperation)
			handleUndo((DwFeatureRenameOperation) evoOp);

		else if (evoOp instanceof DwFeatureTypeChangeOperation)
			handleUndo((DwFeatureTypeChangeOperation) evoOp);
	}
	
	/*
	 * 		CREATE
	 */

	private static void handleExecute(DwFeatureCreateOperation evoOp) {
		evoOp.getFeatureModel().getFeatures().add(evoOp.getFeature());
		
		if(evoOp.getFeatureAddOperation() != null)
			// This can happen when a root feature is created (for instance, within the importer)
			DwEvolutionOperationInterpreter.executeWithoutAnalysis(evoOp.getFeatureAddOperation());
	}

	private static void handleUndo(DwFeatureCreateOperation evoOp) {
		if(evoOp.getFeatureAddOperation() != null)
			// This can happen when a root feature is created (for instance, within the importer)
			DwEvolutionOperationInterpreter.undo(evoOp.getFeatureAddOperation());
		
		evoOp.getFeatureModel().getFeatures().remove(evoOp.getFeature());
	}
	
	/*
	 * 		ADD
	 */

	private static void handleExecute(DwFeatureAddOperation evoOp) {
		DwFeature featureToAdd = evoOp.getFeature();
		Date date = evoOp.getOperationDate();

		EObject newParentObject = evoOp.getParent();
		DwGroup newParentGroupToAddTo = evoOp.getGroupToAddTo();

		if (evoOp.getConsequentFeatureTypeChangeOperation() != null) {
			DwEvolutionOperationInterpreter.executeWithoutAnalysis(evoOp.getConsequentFeatureTypeChangeOperation());
		}

		if (evoOp.getGroupCreateOperation() != null) {
			DwEvolutionOperationInterpreter.executeWithoutAnalysis(evoOp.getGroupCreateOperation());

			DwTemporalInterval parentFeatureValidity = DwEvolutionUtil.getValidIntervalForDate((DwFeature) newParentObject, date);
			if(parentFeatureValidity != null) {
				DwEvolutionUtil.getValidIntervalForDate(newParentGroupToAddTo, date).setTo(parentFeatureValidity.getTo());
			}
			else {
				throw new RuntimeException("Execution of DwFeatureAddOperation - Cannot determine a validity of the parent object");
			}

			evoOp.getGroupTypeChangeOperation().setGroup(newParentGroupToAddTo);
			DwEvolutionOperationInterpreter.executeWithoutAnalysis(evoOp.getGroupTypeChangeOperation());
		}
		
		DwGroupComposition oldChildGroupComposition = DwFeatureUtil.getGroupComposition(newParentGroupToAddTo, date);
		DwGroupComposition newChildGroupComposition = DwSimpleFeatureFactoryCustom.createNewGroupComposition(newParentGroupToAddTo, date);
		if(oldChildGroupComposition != null)
			DwEvolutionUtil.setTemporalValidity(newChildGroupComposition, date, DwEvolutionUtil.getValidIntervalForDate(oldChildGroupComposition, date).getTo());
		
		evoOp.setNewGroupComposition(newChildGroupComposition);

		if (oldChildGroupComposition != null) {
			evoOp.setOldGroupComposition(oldChildGroupComposition);

			oldChildGroupComposition.getValidity().setTo(date);
			newChildGroupComposition.getChildFeatures().addAll(oldChildGroupComposition.getChildFeatures());

			if ((oldChildGroupComposition.getFrom() == null && oldChildGroupComposition.getTo() == null) || !DwEvolutionUtil.isEverValid(oldChildGroupComposition)) {
				oldChildGroupComposition.setCompositionOf(null);
				oldChildGroupComposition.getChildFeatures().clear();
			}
		}

		newChildGroupComposition.getChildFeatures().add(featureToAdd);
		
		if(evoOp.isIntermediateOperation()) {
			Date updateBorder = null;
			
			if(evoOp.eContainer() instanceof DwFeatureMoveOperation) {
				DwFeatureMoveOperation parentOperation = (DwFeatureMoveOperation) evoOp.eContainer();
				DwGroupComposition sourceParentRelation = parentOperation.getDetachOperation().getNewGroupMembership();
				if(sourceParentRelation != null)
					updateBorder = sourceParentRelation.getTo();
				if(DwDateResolverUtil.ETERNITY_DATE.equals(updateBorder))
					updateBorder = null;
			}
			
			List<DwGroupComposition> subsequentParentRelations = DwEvolutionUtil.getElementsThatStartAtOrAfterDate(newParentGroupToAddTo.getParentOf(), date);
			for(DwGroupComposition successorGroupMembership : subsequentParentRelations) {
				if(updateBorder != null) {
					if(successorGroupMembership.getFrom().compareTo(updateBorder) <= 0) {
						if(successorGroupMembership.getTo().compareTo(updateBorder) > 0) {
							DwGroupComposition additionalSuccessorGroupMembership = EcoreUtil.copy(successorGroupMembership);
							additionalSuccessorGroupMembership.getChildFeatures().addAll(successorGroupMembership.getChildFeatures());
							additionalSuccessorGroupMembership.getChildFeatures().remove(featureToAdd);
							newParentGroupToAddTo.getParentOf().add(additionalSuccessorGroupMembership);
							
							additionalSuccessorGroupMembership.getValidity().setFrom(updateBorder);
							successorGroupMembership.getValidity().setTo(updateBorder);
						}
						successorGroupMembership.getChildFeatures().add(featureToAdd);
						continue;
					}
				}
				else {
					successorGroupMembership.getChildFeatures().add(featureToAdd);
				}
			}
		}
	}

	private static void handleUndo(DwFeatureAddOperation evoOp) {
		Date date = evoOp.getOperationDate();
		DwGroup groupToAddTo = evoOp.getGroupToAddTo();
		DwFeature feature = evoOp.getFeature();

		DwGroupComposition oldGroupComposition = evoOp.getOldGroupComposition();
		DwGroupComposition newGroupComposition = evoOp.getNewGroupComposition();

		if (evoOp.getGroupCreateOperation() != null) {
			DwEvolutionOperationInterpreter.undo(evoOp.getGroupTypeChangeOperation());
			DwEvolutionOperationInterpreter.undo(evoOp.getGroupCreateOperation());
		} else {
			if (oldGroupComposition != null) {
				oldGroupComposition.getValidity().setTo(DwEvolutionUtil.getValidIntervalForDate(groupToAddTo, date).getTo());

				if (oldGroupComposition.getCompositionOf() == null) {
					oldGroupComposition.setCompositionOf(groupToAddTo);
					oldGroupComposition.getChildFeatures().addAll(newGroupComposition.getChildFeatures());
					oldGroupComposition.getChildFeatures().remove(feature);
				}
			}

			newGroupComposition.setCompositionOf(null);
		}

		newGroupComposition.getChildFeatures().clear();
		if(evoOp.isIntermediateOperation()) {
			// TODO
			// TODO
			// TODO
			// TODO
			// TODO
			// TODO
			// TODO
			// TODO
			// TODO
			// TODO
			// TODO
			// TODO
			// TODO
			// TODO
			// TODO
			// TODO
			// TODO
			// TODO
			// TODO
		}
		
		if (evoOp.getConsequentFeatureTypeChangeOperation() != null) {
			DwEvolutionOperationInterpreter.undo(evoOp.getConsequentFeatureTypeChangeOperation());
		}
	}
	
	/*
	 * 		DELETE
	 */

	private static void handleExecute(DwFeatureDeleteOperation evoOp) throws UnsupportedOperationException {
		for(DwGroupDeleteOperation consequentOperation : evoOp.getConsequentGroupOperations())
			DwEvolutionOperationInterpreter.executeWithoutAnalysis((DwGroupDeleteOperation) consequentOperation);
		
		DwEvolutionOperationInterpreter.executeWithoutAnalysis(evoOp.getDetachOperation());
		
		Date date = evoOp.getOperationDate();
		DwFeature feature = evoOp.getFeature();
		
		DwTemporalInterval validFeatureInterval = DwEvolutionUtil.getValidIntervalForDate(feature, date);
		if(validFeatureInterval == null) {
			DwLogger.logWarning("Could not find a validity interval for feature \"" + feature.getId() + "\" at " + date.toString() + ".");
			return;
//			throw new RuntimeException("Could not find a validity interval for feature \"" + feature.getId() + "\" at " + date.toString() + ".");
		}
		
		Date oldFeatureValidUntil = validFeatureInterval.getTo();
		validFeatureInterval.setTo(date);
		
		evoOp.setOldFeatureValidUntil(oldFeatureValidUntil);
		
		if (!DwEvolutionUtil.isEverValid(feature)) {
			// TODO this will cause references to uncontained features in the operation model
//			evoOp.getFeatureModel().getFeatures().remove(feature);
		}
	}

	private static void handleUndo(DwFeatureDeleteOperation evoOp) {
		Date date = evoOp.getOperationDate();
		DwFeature feature = evoOp.getFeature();
		DwTemporalFeatureModel featureModel = evoOp.getFeatureModel();

		DwTemporalInterval featureInterval = DwEvolutionUtil.getIntervalForEndDate(feature, date);
		if(featureInterval != null)
			featureInterval.setTo(evoOp.getOldFeatureValidUntil());

		if (feature.getFeatureModel() == null) {
			featureModel.getFeatures().add(feature);
		}

		DwEvolutionOperationInterpreter.undo(evoOp.getDetachOperation());
		
		for(int i=evoOp.getConsequentGroupOperations().size()-1; i >= 0; i--)
			DwEvolutionOperationInterpreter.undo(evoOp.getConsequentGroupOperations().get(i));
	}
	
	/*
	 * 		DETACH
	 */

	private static void handleExecute(DwFeatureDetachOperation evoOp) {
		Date date = evoOp.getOperationDate();
		DwFeature feature = evoOp.getFeature();

		DwGroupComposition oldGroupMembership = DwFeatureUtil.getGroupMembership(feature, date);
		
		if(evoOp.isIntermediateOperation()) {
			// Detach feature from all subsequent relations to parent group
			EList<DwGroupComposition> siblingRelationsOfParentGroup = DwFeatureUtil.getParentGroupOfFeature(feature, date).getParentOf();
			for(DwGroupComposition successorGroupMembership : DwEvolutionUtil.getElementsThatStartAtOrAfterDate(siblingRelationsOfParentGroup, date)) {
				if(oldGroupMembership.getTo().compareTo(successorGroupMembership.getFrom()) <= 0)
					continue;
				successorGroupMembership.getChildFeatures().remove(feature);
				// TODO: Delete group membership if it doesn't contain feature anymore?
			}
			
//			DwGroupComposition successorGroupMembership = DwEvolutionUtil.getTemporalElementForStartDate(feature.getGroupMembership(), oldGroupMembership.getTo());
//			DwEvolutionUtil.setTemporalValidity(successorGroupMembership, date, successorGroupMembership.getTo());
		}

		if (oldGroupMembership != null) {
			evoOp.setOldGroupMembership(oldGroupMembership);
			evoOp.setOldValidUntil(oldGroupMembership.getValidity().getTo());
			
			if(evoOp.getConsequentGroupOperation() == null) {
				DwGroup parentGroup = oldGroupMembership.getCompositionOf();
				DwGroupComposition newGroupMembership = DwSimpleFeatureFactoryCustom.createNewGroupComposition(parentGroup, date);
				DwEvolutionUtil.setTemporalValidity(newGroupMembership, date, evoOp.getOldValidUntil());
				newGroupMembership.getChildFeatures().addAll(oldGroupMembership.getChildFeatures());
				newGroupMembership.getChildFeatures().remove(feature);
				evoOp.setNewGroupMembership(newGroupMembership);
			}
			else {
				// The group became empty due to the detachment
				DwEvolutionOperationInterpreter.executeWithoutAnalysis(evoOp.getConsequentGroupOperation());
			}
			
			oldGroupMembership.getValidity().setTo(date);
			if (!DwEvolutionUtil.isEverValid(oldGroupMembership)) {
				oldGroupMembership.setCompositionOf(null);
				oldGroupMembership.getChildFeatures().clear();
			}
		}
	}

	private static void handleUndo(DwFeatureDetachOperation evoOp) {
		Date date = evoOp.getOperationDate();
		DwFeature feature = evoOp.getFeature();
		DwGroupComposition oldGroupMembership = evoOp.getOldGroupMembership();
		
		if(evoOp.isIntermediateOperation()) {
			// TODO
			// TODO
			// TODO
			// TODO
			// TODO
			// TODO
			// TODO
		}

		if (oldGroupMembership != null) {
			if(evoOp.getConsequentGroupOperation() != null) {
				DwEvolutionOperationInterpreter.undo(evoOp.getConsequentGroupOperation());
			}

			oldGroupMembership.getValidity().setTo(evoOp.getOldValidUntil());
			
			DwGroupComposition newGroupMembership = evoOp.getNewGroupMembership();
			if(newGroupMembership != null) {
				DwGroup newParentGroup = newGroupMembership.getCompositionOf();

				if (oldGroupMembership.getCompositionOf() == null) {
					oldGroupMembership.setCompositionOf(newParentGroup);
					oldGroupMembership.getChildFeatures().addAll(newGroupMembership.getChildFeatures());
					oldGroupMembership.getChildFeatures().add(feature);
				}

				newGroupMembership.getChildFeatures().clear();
				newGroupMembership.setCompositionOf(null);
			}
		}
	}
	
	/*
	 * 		MOVE
	 */

	private static void handleExecute(DwFeatureMoveOperation evoOp) {
		DwFeature currentParent = DwFeatureUtil.getParentFeatureOfFeature(evoOp.getFeature(), evoOp.getOperationDate());
		EObject futureParent = evoOp.getAddOperation().getParent();
		if(currentParent == futureParent)
			throw new DwEvolutionOperationExecutionException("Moving a feature to its current parent is not sensible.");
		
		handleExecute(evoOp.getDetachOperation());
		handleExecute(evoOp.getAddOperation());
	}

	private static void handleUndo(DwFeatureMoveOperation evoOp) {
		handleUndo(evoOp.getAddOperation());
		handleUndo(evoOp.getDetachOperation());
	}
	
	/*
	 * 		RENAME
	 */

	private static void handleExecute(DwFeatureRenameOperation evoOp) {
		DwFeature feature = evoOp.getFeature();
		Date date = evoOp.getOperationDate();

		DwName oldName = DwFeatureUtil.getName(feature, date);
		
		DwName newName = DwSimpleFeatureFactoryCustom.createNewName(evoOp.getNewNameString());
		evoOp.setNewName(newName);

		Date newNameValidUntil = null;
		DwTemporalInterval featureValidInterval = DwEvolutionUtil.getValidIntervalForDate(oldName, date);
		if(featureValidInterval != null)
			newNameValidUntil = featureValidInterval.getTo();
		
		DwEvolutionUtil.setTemporalValidity(newName, date, newNameValidUntil);
	
		boolean newNameReplacedOldName = false;
//		feature.getNames().add(newName);
		
		if (oldName != null) {
			evoOp.setOldName(oldName);
			if(oldName.getValidity() == null)
				DwEvolutionUtil.setTemporalValidity(oldName, null, date);
			else
				oldName.getValidity().setTo(date);

			if ((oldName.getFrom() == null && oldName.getTo() == null) || !DwEvolutionUtil.isEverValid(oldName)) {
				// This trick is required as GEF will automatically paint upon model change. Otherwise two elements may be valid at the same time.
				int oldNameIndex = feature.getNames().indexOf(oldName);
				feature.getNames().set(oldNameIndex, newName);
				newNameReplacedOldName = true;
			}
		}
		
		if (!newNameReplacedOldName) {
			feature.getNames().add(newName);
		}
	}

	private static void handleUndo(DwFeatureRenameOperation evoOp) {
		DwFeature feature = evoOp.getFeature();
		Date date = evoOp.getOperationDate();

		DwName oldName = evoOp.getOldName();

		if (oldName != null) {
			if (!feature.getNames().contains(oldName)) {
				feature.getNames().add(oldName);
			}

			oldName.getValidity().setTo(DwEvolutionUtil.getValidIntervalForDate(feature, date).getTo());
		}
		
		feature.getNames().remove(evoOp.getNewName());
	}
	
	/*
	 * 		TYPE CHANGE
	 */

	private static void handleExecute(DwFeatureTypeChangeOperation evoOp) {
		DwFeature feature = evoOp.getFeature();
		Date date = evoOp.getOperationDate();
		
		DwFeatureType oldFeatureType = DwFeatureUtil.getType(feature, date);
		
		DwFeatureType newFeatureType = DwSimpleFeatureFactoryCustom.createNewFeatureType(evoOp.getNewFeatureTypeEnum());
		evoOp.setNewFeatureType(newFeatureType);
		DwEvolutionUtil.setTemporalValidity(newFeatureType, date, DwEvolutionUtil.getValidIntervalForDate(oldFeatureType, date).getTo());

		boolean newTypeAdded = false;

		if (oldFeatureType != null) {
			evoOp.setOldFeatureType(oldFeatureType);
			oldFeatureType.getValidity().setTo(date);

			if ((oldFeatureType.getValidity().getFrom() == null && oldFeatureType.getValidity().getTo() == null) || !DwEvolutionUtil.isEverValid(oldFeatureType)) {
				// This trick is required as GEF would refresh if the set of types changed.
				// If oldType would be removed before adding newType, no valid type exists at that date.
				// But if newType was added before removing oldType, two valid types exist at date.
				int oldTypeIndex = feature.getTypes().indexOf(oldFeatureType);
				feature.getTypes().set(oldTypeIndex, newFeatureType);
				newTypeAdded = true;
			}
		}
		
		if (!newTypeAdded) {
			feature.getTypes().add(newFeatureType);
		}
	}

	private static void handleUndo(DwFeatureTypeChangeOperation evoOp) {
		DwFeature feature = evoOp.getFeature();
		DwFeatureType newFeatureType = evoOp.getNewFeatureType();
		
		feature.getTypes().remove(newFeatureType);

		DwFeatureType oldFeatureType = evoOp.getOldFeatureType();
		if (oldFeatureType != null) {
			oldFeatureType.getValidity().setTo(DwEvolutionUtil.getValidIntervalForDate(feature, evoOp.getOperationDate()).getTo());

			if (oldFeatureType.getFeature() == null) {
				feature.getTypes().add(oldFeatureType);
			}
		}
	}
}














