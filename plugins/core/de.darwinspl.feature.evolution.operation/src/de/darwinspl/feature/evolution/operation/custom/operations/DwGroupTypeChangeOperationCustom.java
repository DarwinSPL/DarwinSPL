package de.darwinspl.feature.evolution.operation.custom.operations;

import de.darwinspl.feature.evolution.operation.custom.DwOperationStringUtil;
import de.darwinspl.feature.operation.impl.DwGroupTypeChangeOperationImpl;

public class DwGroupTypeChangeOperationCustom extends DwGroupTypeChangeOperationImpl {
	
	@Override
	public String toString() {
		String dateAndContributorInfo = DwOperationStringUtil.getContributorAndDateInfo(this);
		
		StringBuilder infoBuilder = new StringBuilder();
		infoBuilder.append("Group Type Change Operation".toUpperCase());
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(dateAndContributorInfo);
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(DwOperationStringUtil.printInfo("Group to Change Type", DwOperationStringUtil.getInfo(this.getGroup())));
		infoBuilder.append(System.lineSeparator());

		infoBuilder.append(DwOperationStringUtil.printInfo("New Type", this.getNewGroupTypeEnum().toString()));
		
		return infoBuilder.toString();
	}

}
