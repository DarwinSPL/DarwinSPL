package de.darwinspl.feature.evolution.operation.custom.operations;

import de.darwinspl.feature.evolution.operation.custom.DwOperationStringUtil;
import de.darwinspl.feature.operation.impl.DwFeatureDetachOperationImpl;

public class DwFeatureDetachOperationCustom extends DwFeatureDetachOperationImpl {
	
	@Override
	public String toString() {
		String dateAndContributorInfo = DwOperationStringUtil.getContributorAndDateInfo(this);
		
		StringBuilder infoBuilder = new StringBuilder();
		infoBuilder.append("Feature Detach Operation".toUpperCase());
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(dateAndContributorInfo);
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(DwOperationStringUtil.printInfo("Feature to Detach", DwOperationStringUtil.getInfo(this.getFeature())));
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(DwOperationStringUtil.printContainedOperation("Triggered Group Delete Operations", this.getConsequentGroupOperation()));
		
		return infoBuilder.toString();
	}

}
