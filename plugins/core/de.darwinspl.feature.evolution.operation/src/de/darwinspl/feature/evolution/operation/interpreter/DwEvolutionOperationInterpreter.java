package de.darwinspl.feature.evolution.operation.interpreter;

import de.darwinspl.feature.evolution.operation.interpreter.extensions.DwEvolutionOperationExecutionException;
import de.darwinspl.feature.evolution.operation.interpreter.extensions.DwEvolutionOperationInterpreterExtensionRunner;
import de.darwinspl.feature.evolution.operation.resource.DwEvolutionOperationModelResourceUtil;
import de.darwinspl.feature.operation.DwEvolutionOperation;
import de.darwinspl.feature.operation.DwFeatureAttributeOperation;
import de.darwinspl.feature.operation.DwFeatureOperation;
import de.darwinspl.feature.operation.DwFeatureVersionOperation;
import de.darwinspl.feature.operation.DwGroupOperation;

public class DwEvolutionOperationInterpreter {
	
	private static DwEvolutionOperationInterpreterExtensionRunner extensionRunner = new DwEvolutionOperationInterpreterExtensionRunner();


	/**
	 * Executes the given evolution operation <b>while</b>
	 * - storing the operation in corresponding operations model
	 * - calling additional analysis means
	 * @param evoOp evolution operation
	 */
	public static void execute(DwEvolutionOperation evoOp) {
		String transientEffectExplanation = DwEvolutionOperationModelResourceUtil.addOperationToOperationsModel(evoOp);
		
		// Pre-execution means
		if(!extensionRunner.runPreExecutionExtensions(evoOp, transientEffectExplanation))
			throw new DwEvolutionOperationExecutionException();
		
		executeWithoutAnalysis(evoOp);
	}
	
	/**
	 * Executes the given evolution operation <b>without</b> calling additional analysis and operation tracking means
	 * @param evoOp evolution operation
	 */
	public static void executeWithoutAnalysis(DwEvolutionOperation evoOp) {
		DwUndoHandler.store(evoOp);
		
		if (evoOp instanceof DwFeatureAttributeOperation)
			DwFeatureOperationInterpreter.execute(evoOp);
		
		else if (evoOp instanceof DwGroupOperation)
			DwGroupOperationInterpreter.execute(evoOp);
		
		else if (evoOp instanceof DwFeatureVersionOperation)
			DwVersionOperationInterpreter.execute(evoOp);
		
		// Do this last due to evoOp inheritance
		else if (evoOp instanceof DwFeatureOperation)
			DwFeatureOperationInterpreter.execute(evoOp);
		
		else
			throw new UnsupportedOperationException("Following operation was tried to be executed, but not recognized: " + (evoOp == null ? "no Operation given (null)" : evoOp.toString()));
	}
	
	
	
	/**
	 * Reverts a previously executed evolution operation
	 * @param evoOp previously executed evolution operation
	 */
	public static void undo(DwEvolutionOperation evoOp) {
		DwUndoHandler.undo(evoOp);
		
//		if(evoOp.isIntermediateOperation()) {
//			throw new RuntimeException("Intermediate Evolution Operations can currently not be reverted.");
//		}
		
//		if (evoOp instanceof DwFeatureAttributeOperation)
//			DwFeatureOperationInterpreter.undo(evoOp);
//
//		else if (evoOp instanceof DwGroupOperation)
//			DwGroupOperationInterpreter.undo(evoOp);
//
//		else if (evoOp instanceof DwFeatureVersionOperation)
//			DwVersionOperationInterpreter.undo(evoOp);
//
//		// Do this last due to evoOp inheritance
//		else if (evoOp instanceof DwFeatureOperation)
//			DwFeatureOperationInterpreter.undo(evoOp);
//		
//		else
//			throw new UnsupportedOperationException("Following operation was tried to be undone, but not recognized: " + (evoOp == null ? "no Operation given (null)" : evoOp.toString()));

		DwEvolutionOperationModelResourceUtil.undoOperation(evoOp);
	}

}
