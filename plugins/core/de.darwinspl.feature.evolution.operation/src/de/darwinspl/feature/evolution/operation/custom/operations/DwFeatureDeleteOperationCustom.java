package de.darwinspl.feature.evolution.operation.custom.operations;

import de.darwinspl.feature.evolution.operation.custom.DwOperationStringUtil;
import de.darwinspl.feature.operation.DwGroupDeleteOperation;
import de.darwinspl.feature.operation.impl.DwFeatureDeleteOperationImpl;

public class DwFeatureDeleteOperationCustom extends DwFeatureDeleteOperationImpl {
	
	@Override
	public String toString() {
		String dateAndContributorInfo = DwOperationStringUtil.getContributorAndDateInfo(this);
		
		StringBuilder infoBuilder = new StringBuilder();
		infoBuilder.append("Feature Delete Operation".toUpperCase());
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(dateAndContributorInfo);
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(DwOperationStringUtil.printInfo("Feature to Delete", DwOperationStringUtil.getInfo(this.getFeature())));
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(DwOperationStringUtil.printContainedOperation("Triggered Feature Detach Operation", this.getDetachOperation()));
		infoBuilder.append(System.lineSeparator());
		
		for(DwGroupDeleteOperation operation: this.getConsequentGroupOperations()) {
			infoBuilder.append(DwOperationStringUtil.printContainedOperation("Triggered Group Delete Operation", operation));
		}
		
		return infoBuilder.toString();
	}

}
