package de.darwinspl.feature.evolution.operation.custom.operations;

import de.darwinspl.feature.evolution.operation.custom.DwOperationStringUtil;
import de.darwinspl.feature.operation.impl.DwFeatureAddOperationImpl;

public class DwFeatureAddOperationCustom extends DwFeatureAddOperationImpl {
	
	@Override
	public String toString() {
		String dateAndContributorInfo = DwOperationStringUtil.getContributorAndDateInfo(this);
		
		StringBuilder infoBuilder = new StringBuilder();
		infoBuilder.append("Feature Add Operation".toUpperCase());
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(dateAndContributorInfo);
		infoBuilder.append(System.lineSeparator());

		infoBuilder.append(DwOperationStringUtil.printInfo("Feature to add", DwOperationStringUtil.getInfo(this.getFeature())));
		infoBuilder.append(System.lineSeparator());

		infoBuilder.append(DwOperationStringUtil.printInfo("Selected Parent Element", DwOperationStringUtil.getInfo(this.getParent())));
		infoBuilder.append(System.lineSeparator());

		infoBuilder.append(DwOperationStringUtil.printInfo("Determined Group to add to", DwOperationStringUtil.getInfo(this.getGroupToAddTo())));
		infoBuilder.append(System.lineSeparator());

		infoBuilder.append(DwOperationStringUtil.printContainedOperation("Triggered Group Create Operation", this.getGroupCreateOperation()));
		infoBuilder.append(System.lineSeparator());

		infoBuilder.append(DwOperationStringUtil.printContainedOperation("Triggered Group Type Change Operation", this.getGroupTypeChangeOperation()));
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(DwOperationStringUtil.printContainedOperation("Triggered Child Feature Type Change Operation", this.getConsequentFeatureTypeChangeOperation()));
		
		return infoBuilder.toString();
	}

}
