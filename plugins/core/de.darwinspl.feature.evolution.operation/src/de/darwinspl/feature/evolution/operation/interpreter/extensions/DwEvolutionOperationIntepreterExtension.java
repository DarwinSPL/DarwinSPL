package de.darwinspl.feature.evolution.operation.interpreter.extensions;

import de.darwinspl.feature.operation.DwEvolutionOperation;

public interface DwEvolutionOperationIntepreterExtension {
	
	/**
	 * Used to initialize the pre execution analysis
	 */
	public void init();
	
	/**
	 * @param evoOp An evolution operation that shall be analysed
	 * @return true on success, false otherwise
	 */
	public boolean run(DwEvolutionOperation evoOp, String additionalInfo);

}
