package de.darwinspl.feature.evolution.operation.custom.operations;

import de.darwinspl.feature.evolution.operation.custom.DwOperationStringUtil;
import de.darwinspl.feature.operation.DwFeatureDeleteOperation;
import de.darwinspl.feature.operation.impl.DwGroupDeleteOperationImpl;

public class DwGroupDeleteOperationCustom extends DwGroupDeleteOperationImpl {
	
	@Override
	public String toString() {
		String dateAndContributorInfo = DwOperationStringUtil.getContributorAndDateInfo(this);
		
		StringBuilder infoBuilder = new StringBuilder();
		infoBuilder.append("Group Delete Operation".toUpperCase());
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(dateAndContributorInfo);
		infoBuilder.append(System.lineSeparator());

		infoBuilder.append(DwOperationStringUtil.printInfo("Group to Delete", DwOperationStringUtil.getInfo(this.getGroup())));
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(DwOperationStringUtil.printContainedOperation("Triggered Group Detach Operation", this.getDetachOperation()));
		infoBuilder.append(System.lineSeparator());
		
		for(DwFeatureDeleteOperation operation : this.getConsequentFeatureOperations()) {
			infoBuilder.append(DwOperationStringUtil.printContainedOperation("Triggered Feature Delete Operation", operation));
		}
		
		return infoBuilder.toString();
	}

}
