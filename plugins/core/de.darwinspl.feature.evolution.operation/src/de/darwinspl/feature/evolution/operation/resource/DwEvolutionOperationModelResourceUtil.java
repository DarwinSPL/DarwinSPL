package de.darwinspl.feature.evolution.operation.resource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourceAttributes;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EContentsEList;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.christophseidl.util.ecore.EcoreIOUtil;
import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupComposition;
import de.darwinspl.feature.DwParentFeatureChildGroupContainer;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.contributor.DwContributor;
import de.darwinspl.feature.evolution.operation.factory.DwSimpleEvolutionOperationFactory;
import de.darwinspl.feature.impl.custom.DwFeatureModelResource;
import de.darwinspl.feature.impl.custom.DwFeatureModelResourceListener;
import de.darwinspl.feature.operation.DwEvolutionOperation;
import de.darwinspl.feature.operation.DwEvolutionOperationModel;
import de.darwinspl.feature.operation.DwFeatureCreateOperation;
import de.darwinspl.feature.operation.DwFeatureDeleteOperation;
import de.darwinspl.feature.operation.DwFeatureMoveOperation;
import de.darwinspl.feature.operation.DwGroupDeleteOperation;
import de.darwinspl.feature.operation.DwGroupMoveOperation;
import de.darwinspl.feature.operation.DwGroupOperation;
import de.darwinspl.feature.operation.DwOperationFactory;
import de.darwinspl.feature.util.custom.DwFeatureUtil;
import de.darwinspl.temporal.DwMultiTemporalIntervalElement;
import de.darwinspl.temporal.DwTemporalElement;
import de.darwinspl.temporal.DwTemporalInterval;
import de.darwinspl.temporal.util.DwDateResolverUtil;
import de.darwinspl.temporal.util.DwEvolutionUtil;

public class DwEvolutionOperationModelResourceUtil implements DwFeatureModelResourceListener {
	
	private static DwSimpleEvolutionOperationFactory factory = new DwSimpleEvolutionOperationFactory();
	
	private static final String FEATURE_MODEL_FILE_EXTENSION = DwFeatureUtil.getFeatureModelFileExtensionForXmi();
	private static final String OPERATION_MODEL_FILE_EXTENSION = FEATURE_MODEL_FILE_EXTENSION + "_evoop";

	private static Map<String, DwEvolutionOperationModel> tfmIDToOperationModelCacheMap = new HashMap<>();
	
	private static Map<DwEvolutionOperation, Set<DwEvolutionOperation>> insertedOperationsMap = new HashMap<>();

	private static DwEvolutionOperationModel getOperationsModelFromCache(DwTemporalFeatureModel tfm) {
		return tfmIDToOperationModelCacheMap.get(tfm.getId());
	}
	
	

	@Override
	public void resourceSaved(DwFeatureModelResource res) {
		EObject root = res.getContents().get(0);
		if(root instanceof DwTemporalFeatureModel) {
			DwTemporalFeatureModel tfm = (DwTemporalFeatureModel) root;
			URI newOpResURI = calculateOpResUriFromTfm(tfm);
			
			DwEvolutionOperationModel opModel = getOperationsModelFromCache(tfm);
			if(opModel == null) {
				createNewOpModelWithResource(tfm, calculateOpResUriFromTfm(tfm));
				opModel = loadAndCacheOperationsModel(tfm);
			}
			Set<DwEvolutionOperation> operationsToBeDeleted = new HashSet<>();
			checkModelBeforeSaving(opModel, operationsToBeDeleted);
			opModel.getEvolutionOperations().removeAll(operationsToBeDeleted);
			
			Resource opRes = opModel.eResource();
			opRes.setURI(newOpResURI);
			
			try {
				opRes.save(Collections.EMPTY_MAP);
				
				IFile opResFile = EcoreIOUtil.getFile(opRes);
				ResourceAttributes attributes = new ResourceAttributes();
				attributes.setHidden(false);
				attributes.setExecutable(false);
				opResFile.setResourceAttributes(attributes);
//				opResFile.setDerived(true, null);
			} catch (IOException | CoreException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void resourceChangesDiscarded(DwFeatureModelResource res) {
		System.out.println("Discarding changes to operation model cache map.");
		DwTemporalFeatureModel tfm = (DwTemporalFeatureModel) res.getContents().get(0);
		removeOperationModelFromCache(tfm);
	}
	
	public static void removeOperationModelFromCache(DwTemporalFeatureModel tfm) {
		DwEvolutionOperationModel model = tfmIDToOperationModelCacheMap.get(tfm.getId());
		if(model == null)
			return;
		
		int removeCount = 0;
		for(DwEvolutionOperation operation : insertedOperationsMap.keySet()) {
			EObject container = operation.eContainer();
			if(container instanceof DwEvolutionOperationModel) {
				if(((DwEvolutionOperationModel) container).equals(model)) {
					insertedOperationsMap.remove(operation);
					removeCount++;
				}
			}
		}
		if(removeCount > 0)
			System.out.println("Removed " + removeCount + " entries from list of inserted operations.");
		
		tfmIDToOperationModelCacheMap.remove(tfm.getId());
	}
	
	
	
	/**
	 * This method does a sanity check of an operation model (or part) before saving it.
	 * Its purpose is to unset references to elements that are not part of a containment hierarchy.
	 * @param element the operation model or a part that is supposed to be cleared from references to non-contained elements
	 * @param elementsToDelete a set of elements that should not be part of the evolution operation model (TODO not yet implemented)
	 */
	private static void checkModelBeforeSaving(EObject element, Set<DwEvolutionOperation> elementsToDelete) {
		for (EContentsEList.FeatureIterator<EObject> featureIterator = (EContentsEList.FeatureIterator<EObject>) element
				.eCrossReferences().iterator(); featureIterator.hasNext();) {
			// Always get these two object together!
			EObject referencedObject = (EObject) featureIterator.next();
			EReference reference = (EReference) featureIterator.feature();
			
			if (referencedObject.eContainer() != null)
				continue;
			if(referencedObject instanceof DwTemporalFeatureModel)
				continue;
			if(referencedObject instanceof DwContributor)
				continue;
			
			System.out.println("Unsetting reference to an element with broken containment in: " + element.getClass().getSimpleName());
			System.out.println("\treference \"" + reference.getName() + "\" of type " + referencedObject.getClass().getSimpleName());
			
			element.eUnset(reference);
		}
		
		for(EObject containmentChild : element.eContents())
			checkModelBeforeSaving(containmentChild, elementsToDelete);
	}
	
	
	
	public static void resourceDeleted(IFile deletedTfmFile) {
		int indexOfFileExtensionDot = deletedTfmFile.getName().length() - deletedTfmFile.getFileExtension().length();
		String opModelResourceName = deletedTfmFile.getName().substring(0, indexOfFileExtensionDot) + OPERATION_MODEL_FILE_EXTENSION;
		IPath opModelResourcePath = deletedTfmFile.getFullPath().removeLastSegments(1).append(opModelResourceName);
		
//		IResource opModelResource = deletedTfmFile.getParent().findMember(opModelResourceName);
//		if(opModelResource != null && opModelResource.exists() && opModelResource instanceof IFile) {
//			WorkspaceJob job = new WorkspaceJob(opModelResourceName) {
//				@Override
//				public IStatus runInWorkspace(IProgressMonitor monitor) throws CoreException {
//					opModelResource.delete(true, monitor);
//					return Status.OK_STATUS;
//				}
//			};
//			job.schedule();
//		}
		
		List<String> toDelete = new ArrayList<>();
		
		for(String tfmId : tfmIDToOperationModelCacheMap.keySet()) {
			DwEvolutionOperationModel opModel = tfmIDToOperationModelCacheMap.get(tfmId);
			IFile cachedOpModelFile = EcoreIOUtil.getFile(opModel);
			if(cachedOpModelFile != null) {
				if(cachedOpModelFile.getFullPath().equals(opModelResourcePath)) {
					System.out.println("Deleting cached operation model");
					toDelete.add(tfmId);
				}
			}
		}
		
		for(String tfmId : toDelete) {
			System.out.println("Clearing cache map entry for: " + tfmId);
			tfmIDToOperationModelCacheMap.remove(tfmId);
		}
	}
	
	
	/**
	 * Adds the given TFM edit operation to the associated operation model.
	 * In case of a transient effect paradox, an explanation is returned.
	 * @param evoOp - TFM edit operation to add
	 * @return explanation for transient effect paradoxes or null if none exist
	 */
	public static String addOperationToOperationsModel(DwEvolutionOperation evoOp) {
		String transientEffectMessage = null;
		
		if(evoOp.getOperationDate().equals(DwDateResolverUtil.INITIAL_STATE_DATE))
			return null;
		
		DwEvolutionOperation evoOpCopy = EcoreUtil.copy(evoOp);
		DwEvolutionOperationModel operationModel = loadAndCacheOperationsModel(evoOp.getFeatureModel());
		
		operationModel.getEvolutionOperations().add(evoOpCopy);
		if(evoOpCopy.isIntermediateOperation()) {
			transientEffectMessage = ensureOperationModelSanity(operationModel, evoOp);
			if(transientEffectMessage.equals(""))
				transientEffectMessage = null;
		}
		
		DwContributor newContributor = evoOpCopy.getContributor();
		if(newContributor == null)
			return null;
		
		// Only add the new contributor if he doesn't exist in the list of contributors so far
		EList<DwContributor> contributors = operationModel.getContributors();
		for(DwContributor contributor : contributors) {
			if(newContributor.getId().equals(contributor.getId()))
				return transientEffectMessage;
		}
		
		contributors.add(newContributor);
		return transientEffectMessage;
	}
	
	/**
	 * Some intermediate operations require alterations to subsequent operations!
	 * @param operationModel
	 * @param operation
	 */
	private static String ensureOperationModelSanity(DwEvolutionOperationModel operationModel, DwEvolutionOperation operation) {
		EObject elementToAddTo;
		EObject elementToDelete;
		List<String> transientEffectReasons = new ArrayList<>();
		
		if(operation instanceof DwFeatureCreateOperation) {
			elementToAddTo = ((DwFeatureCreateOperation) operation).getFeatureAddOperation().getParent();
			elementToDelete = ((DwFeatureCreateOperation) operation).getFeatureAddOperation().getGroupToAddTo();
			if(elementToDelete.equals(elementToAddTo)) {
				elementToDelete = ((DwFeatureCreateOperation) operation).getFeatureAddOperation().getFeature();
			}
		}
		else if(operation instanceof DwFeatureMoveOperation) {
			elementToAddTo = ((DwFeatureMoveOperation) operation).getAddOperation().getParent();
			elementToDelete = ((DwFeatureMoveOperation) operation).getAddOperation().getGroupToAddTo();
			if(elementToDelete.equals(elementToAddTo)) {
				elementToDelete = ((DwFeatureMoveOperation) operation).getAddOperation().getFeature();
			}
		}
		else if(operation instanceof DwGroupMoveOperation) {
			elementToAddTo = ((DwGroupMoveOperation) operation).getGroupAddOperation().getParentFeature();
			elementToDelete = ((DwGroupMoveOperation) operation).getGroupAddOperation().getGroup();
		}
		else {
			return "";
		}
		
		List<DwEvolutionOperation> operationsToUpdate = new LinkedList<>();
		for(DwEvolutionOperation otherOperation : operationModel.getEvolutionOperations()) {
			DwEvolutionOperationUtil.findNestedDeleteOperationsThatDeleteAnElement(otherOperation, elementToAddTo, operationsToUpdate);
		}
		
		if(!operationsToUpdate.isEmpty()) {
			insertedOperationsMap.put(operation, new HashSet<DwEvolutionOperation>());
		}
		
		for(DwEvolutionOperation operationToUpdate : operationsToUpdate) {
			Date additionalOperationDate = operationToUpdate.getOperationDate();
			DwEvolutionOperation additionalOperation;
			
			if(operationToUpdate instanceof DwFeatureDeleteOperation) {
				additionalOperation = factory.createDwGroupDeleteOperation(operationToUpdate.getFeatureModel(), (DwGroup) elementToDelete, additionalOperationDate, false);

				if(operation instanceof DwFeatureCreateOperation) {
					DwFeature createdFeature = ((DwFeatureCreateOperation) operation).getFeature();
					DwFeatureDeleteOperation additionalOperation2 = factory.createDwFeatureDeleteOperation(operationToUpdate.getFeatureModel(), createdFeature, additionalOperationDate, false);
					((DwGroupDeleteOperation) additionalOperation).getConsequentFeatureOperations().add(additionalOperation2);
				}
				else if(operation instanceof DwFeatureMoveOperation) {
					DwFeature movedFeature = ((DwFeatureMoveOperation) operation).getFeature();
					DwFeatureDeleteOperation additionalOperation2 = factory.createDwFeatureDeleteOperation(operationToUpdate.getFeatureModel(), movedFeature, additionalOperationDate, false);
					((DwGroupDeleteOperation) additionalOperation).getConsequentFeatureOperations().add(additionalOperation2);
				}
				else if(operation instanceof DwGroupMoveOperation) {
					DwGroup movedGroup = ((DwGroupMoveOperation) operation).getGroup();
					for(DwFeature childFeatureOfChildGroup : DwFeatureUtil.getChildFeaturesOfGroup(movedGroup, additionalOperationDate)) {
						DwFeatureDeleteOperation additionalOperation2 = factory.createDwFeatureDeleteOperation(operationToUpdate.getFeatureModel(), childFeatureOfChildGroup, additionalOperationDate, false);
						((DwGroupDeleteOperation) additionalOperation).getConsequentFeatureOperations().add(additionalOperation2);
					}
				}
				
				((DwFeatureDeleteOperation) operationToUpdate).getConsequentGroupOperations().add((DwGroupDeleteOperation) additionalOperation);
				insertedOperationsMap.get(operation).add(additionalOperation);
				transientEffectReasons.add("A feature delete operation scheduled for "+operationToUpdate.getOperationDate()+" will undo the change you are about to perform.");
				
//				boolean additionalOperationReached = false;
//				Map<Date, List<DwEvolutionOperation>> sortedOperationLists = DwEvolutionOperationUtil.getSortedOperationListsForEachDate(operationModel);
//				for(DwEvolutionOperation otherOperationAtAdditionalOperationDate : sortedOperationLists.get(additionalOperationDate)) {
//					if(!additionalOperationReached && otherOperationAtAdditionalOperationDate.equals(operationToUpdate)) {
//						additionalOperationReached = true;
//					}
//					else if(additionalOperationReached) {
//						List<DwEvolutionOperation> nestedOperationsThatDeleteAnInterestingElement = new LinkedList<>();
//						
//						for(DwTemporalElement potentiallyDeletedElement : elementsThatCouldHaveBeenDeletedInOtherOperations) {
//							DwEvolutionOperationUtil.findNestedDeleteOperationsThatDeleteAnElement(otherOperationAtAdditionalOperationDate, potentiallyDeletedElement, nestedOperationsThatDeleteAnInterestingElement);
//							
//							for(DwEvolutionOperation nestedOperationThatDeletesAnInterestingElement : nestedOperationsThatDeleteAnInterestingElement) {
//								// Iterate over all operations that delete elements which should also have been deleted
//								if(nestedOperationThatDeletesAnInterestingElement instanceof DwFeatureDeleteOperation) {
//									DwFeature featureThatShouldBeDeleted = ((DwFeatureDeleteOperation) nestedOperationThatDeletesAnInterestingElement).getFeature();
//									if(!elementsThatWereDeletedByAdditionalOperations.contains(featureThatShouldBeDeleted)) {
//										DwGroupComposition parentRelation = DwEvolutionUtil.getTemporalElementForEndDate(featureThatShouldBeDeleted.getGroupMembership(), additionalOperationDate);
//										
//										if(parentRelation != null) {
//											DwGroup parentGroup = (DwGroup) parentRelation.eContainer();
//											for(DwEvolutionOperation operationThatNeedsToBeExtended : DwEvolutionOperationUtil.findNestedDeleteOperationsThatDeleteAnElement(additionalOperation, parentGroup)) {
//												if(operationThatNeedsToBeExtended instanceof DwGroupDeleteOperation) {
//													DwFeatureDeleteOperation additionalOperation3 = factory.createDwFeatureDeleteOperation(operationToUpdate.getFeatureModel(), featureThatShouldBeDeleted, additionalOperationDate, false);
//													((DwGroupDeleteOperation) operationThatNeedsToBeExtended).getConsequentFeatureOperations().add(0, additionalOperation3);
//												}
//											}
//										}
//									}
//								}
//								else if(nestedOperationThatDeletesAnInterestingElement instanceof DwGroupDeleteOperation) {
//									DwGroup groupThatShouldBeDeleted = ((DwGroupDeleteOperation) nestedOperationThatDeletesAnInterestingElement).getGroup();
//									if(!elementsThatWereDeletedByAdditionalOperations.contains(groupThatShouldBeDeleted)) {
//										DwParentFeatureChildGroupContainer parentRelation = DwEvolutionUtil.getTemporalElementForEndDate(groupThatShouldBeDeleted.getChildOf(), additionalOperationDate);
//										
//										if(parentRelation != null) {
//											DwFeature parentFeature = (DwFeature) parentRelation.eContainer();
//											for(DwEvolutionOperation operationThatNeedsToBeExtended : DwEvolutionOperationUtil.findNestedDeleteOperationsThatDeleteAnElement(additionalOperation, parentFeature)) {
//												if(operationThatNeedsToBeExtended instanceof DwGroupDeleteOperation) {
//													DwGroupDeleteOperation additionalOperation3 = factory.createDwGroupDeleteOperation(operationToUpdate.getFeatureModel(), groupThatShouldBeDeleted, additionalOperationDate, false);
//													((DwFeatureDeleteOperation) operationThatNeedsToBeExtended).getConsequentGroupOperations().add(0, additionalOperation3);
//												}
//											}
//										}
//									}
//								}
//							}
//							nestedOperationsThatDeleteAnInterestingElement.clear();
//						}
//						
////						if(otherOperationAtAdditionalOperationDate instanceof DwFeatureDeleteOperation) {
////							if(elementsThatCouldHaveBeenDeletedInOtherOperations.contains(((DwFeatureDeleteOperation) otherOperationAtAdditionalOperationDate).getFeature())) {
////								System.out.println("WHAT NOW");
////							}
////						}
////						if(otherOperationAtAdditionalOperationDate instanceof DwGroupDeleteOperation) {
////							if(elementsThatCouldHaveBeenDeletedInOtherOperations.contains(((DwGroupDeleteOperation) otherOperationAtAdditionalOperationDate).getGroup())) {
////								System.out.println("WHAT NOW");
////							}
////						}
//					}
//				}
			}
			else if(operationToUpdate instanceof DwGroupDeleteOperation) {
				additionalOperation = factory.createDwFeatureDeleteOperation(operationToUpdate.getFeatureModel(), (DwFeature) elementToDelete, operationToUpdate.getOperationDate(), false);
				((DwGroupDeleteOperation) operationToUpdate).getConsequentFeatureOperations().add((DwFeatureDeleteOperation) additionalOperation);
				insertedOperationsMap.get(operation).add(additionalOperation);
				transientEffectReasons.add("A group delete operation scheduled for "+operationToUpdate.getOperationDate()+" will undo the change you are about to perform.");
			}
			else {
				continue;
			}
			
			Set<DwTemporalElement> elementsThatWereDeletedByAdditionalOperations = new HashSet<DwTemporalElement>();
			DwEvolutionOperationUtil.collectAllDeletedElements(additionalOperation, elementsThatWereDeletedByAdditionalOperations);
			
			Set<DwTemporalElement> elementsThatCouldHaveBeenDeletedInOtherOperations = new HashSet<DwTemporalElement>();
			if(additionalOperation instanceof DwGroupDeleteOperation) {
				DwFeatureUtil.collectAllChildFeaturesAndGroupsAcrossTime(elementsThatCouldHaveBeenDeletedInOtherOperations, ((DwGroupOperation) additionalOperation).getGroup());
				for(DwFeatureDeleteOperation consequentOperation : ((DwGroupDeleteOperation) additionalOperation).getConsequentFeatureOperations()) {
					DwFeatureUtil.collectAllChildFeaturesAndGroupsAcrossTime(elementsThatCouldHaveBeenDeletedInOtherOperations, consequentOperation.getFeature());
				}
			}
			else if(additionalOperation instanceof DwFeatureDeleteOperation) {
				DwFeatureUtil.collectAllChildFeaturesAndGroupsAcrossTime(elementsThatCouldHaveBeenDeletedInOtherOperations, ((DwFeatureDeleteOperation) additionalOperation).getFeature());
				for(DwGroupDeleteOperation consequentOperation : ((DwFeatureDeleteOperation) additionalOperation).getConsequentGroupOperations()) {
					DwFeatureUtil.collectAllChildFeaturesAndGroupsAcrossTime(elementsThatCouldHaveBeenDeletedInOtherOperations, consequentOperation.getGroup());
				}
			}
			
			Set<DwEvolutionOperation> missingOperations = new HashSet<>();
			for(DwTemporalElement potentiallyUndeletedElement : elementsThatCouldHaveBeenDeletedInOtherOperations) {
				if(elementsThatWereDeletedByAdditionalOperations.contains(potentiallyUndeletedElement))
					continue;
				
				for(DwTemporalInterval interval : ((DwMultiTemporalIntervalElement) potentiallyUndeletedElement).getValidities()) {
					if(interval.getTo().equals(additionalOperationDate)) {
						// potentiallyUndeletedElement was deleted in this time step but we did not
						if(potentiallyUndeletedElement instanceof DwFeature) {
							missingOperations.add(factory.createDwFeatureDeleteOperation(operationToUpdate.getFeatureModel(), (DwFeature) potentiallyUndeletedElement, additionalOperationDate, false));
						}
						else if(potentiallyUndeletedElement instanceof DwGroup) {
							missingOperations.add(factory.createDwGroupDeleteOperation(operationToUpdate.getFeatureModel(), (DwGroup) potentiallyUndeletedElement, additionalOperationDate, false));
						}
					}
				}
			}
			
			while(!missingOperations.isEmpty()) {
				Set<DwEvolutionOperation> resolvedOperations = new HashSet<>();
				for(DwEvolutionOperation missingOperation : missingOperations) {
					EObject parentElement;
					if(missingOperation instanceof DwFeatureDeleteOperation) {
						DwFeature deletedFeature = ((DwFeatureDeleteOperation) missingOperation).getFeature();
						DwGroupComposition parentRelation = DwEvolutionUtil.getTemporalElementForEndDate(deletedFeature.getGroupMembership(), additionalOperationDate);
						parentElement = parentRelation.eContainer();
					}
					else if(missingOperation instanceof DwGroupDeleteOperation) {
						DwGroup deletedGroup = ((DwGroupDeleteOperation) missingOperation).getGroup();
						DwParentFeatureChildGroupContainer parentRelation = DwEvolutionUtil.getTemporalElementForEndDate(deletedGroup.getChildOf(), additionalOperationDate);
						parentElement = parentRelation.eContainer();
					}
					else throw new RuntimeException();
					
					List<DwEvolutionOperation> parentOperations = DwEvolutionOperationUtil.findNestedDeleteOperationsThatDeleteAnElement(additionalOperation, parentElement);
					if(parentOperations.size() != 1)
						continue;
					
					if(parentOperations.get(0) instanceof DwFeatureDeleteOperation) {
						DwFeatureDeleteOperation parentOperation = (DwFeatureDeleteOperation) parentOperations.get(0);
						parentOperation.getConsequentGroupOperations().add((DwGroupDeleteOperation) missingOperation);
						resolvedOperations.add(missingOperation);
					}
					else if(parentOperations.get(0) instanceof DwGroupDeleteOperation) {
						DwGroupDeleteOperation parentOperation = (DwGroupDeleteOperation) parentOperations.get(0);
						parentOperation.getConsequentFeatureOperations().add((DwFeatureDeleteOperation) missingOperation);
						resolvedOperations.add(missingOperation);
					}
				}
				
				if(resolvedOperations.isEmpty()) {
//						throw new RuntimeException("Inifinite loop: Cant find a place to plug additional operations into");
					System.out.println("Inifinite loop: Cant find a place to plug additional operations into");
					break;
				}
				
				missingOperations.removeAll(resolvedOperations);
			}
		}
		
		return String.join(System.lineSeparator(), transientEffectReasons);
	}



	/**
	 * Removes an operation along with all operations that it additionally entailed (this can occur in case of an intermediate operation)
	 * @param evoOp the operation to remove and clear from the evolution
	 */
	public static void undoOperation(DwEvolutionOperation evoOp) {
		Set<DwEvolutionOperation> insertedOperations = insertedOperationsMap.get(evoOp);
		if(insertedOperations != null) {
			for(DwEvolutionOperation insertedOperation : insertedOperations) {
				EcoreUtil.remove(insertedOperation);
			}
			insertedOperations.remove(evoOp);
		}
		
		removeOperationFromOperationsModel(evoOp);
	}



	public static void removeOperationFromOperationsModel(DwEvolutionOperation evoOp) {
		DwEvolutionOperationModel operationModel = loadAndCacheOperationsModel(evoOp.getFeatureModel());
		// needed for Test
		if(operationModel == null)
			return;
		
		for(DwEvolutionOperation op : operationModel.getEvolutionOperations()) {
			if(op.getId().equals(evoOp.getId())) {
				operationModel.getEvolutionOperations().remove(op);
				return;
			}
		}
	}
	
	
	
	public static DwEvolutionOperationModel loadAndCacheOperationsModel(DwTemporalFeatureModel tfm) {
		DwEvolutionOperationModel cachedOperationModel = getOperationsModelFromCache(tfm);
		if(cachedOperationModel != null)
			return cachedOperationModel;
		
		URI opResURI = calculateOpResUriFromTfm(tfm);
		DwEvolutionOperationModel opResModel;
		
		try {
			System.out.println("Loading Ecore Model From File... \"" + opResURI + "\"");
			long startTime = System.currentTimeMillis();
			opResModel = EcoreIOUtil.loadModel(opResURI);
			System.out.println("Finished loading \"" + opResURI + "\": " + (System.currentTimeMillis()-startTime) + "ms");
		} catch(Exception e) {
			System.out.println(e.getMessage());
			// Loading failed. Create new resource
			opResModel = createNewOpModelWithResource(tfm, opResURI);
		}
		
		System.out.println("Creating new cached operation model");
		tfmIDToOperationModelCacheMap.put(tfm.getId(), opResModel);
		return opResModel;
	}

	private static URI calculateOpResUriFromTfm(DwTemporalFeatureModel tfm) {
		Resource tfmRes = tfm.eResource();
		URI tfmResURI;
		if(tfmRes == null) {
			tfmResURI = URI.createURI("temp:/" + tfm.hashCode() + "." + FEATURE_MODEL_FILE_EXTENSION);
		}
		else {
			tfmResURI = tfmRes.getURI();
		}
		tfmResURI = tfmResURI.trimSegments(1).appendSegment(tfmResURI.lastSegment().replace(tfmResURI.fileExtension(), OPERATION_MODEL_FILE_EXTENSION));
		
		if(!tfmResURI.toString().startsWith("platform:/resource/")) {
			return URI.createURI("platform:/resource" + tfmResURI.toString());
		}
		
		return tfmResURI;
	}



	private static DwEvolutionOperationModel createNewOpModelWithResource(DwTemporalFeatureModel tfm, URI opResURI) {
		DwEvolutionOperationModel opResModel = DwOperationFactory.eINSTANCE.createDwEvolutionOperationModel();
		
		Resource opRes = (new ResourceSetImpl()).createResource(opResURI);
		if(opRes != null) {
			opRes.getContents().add(opResModel);
		}
		else {
			System.out.println("Model resource could not be created: " + opResURI);
			return null;
		}
		
		return opResModel;
	}
	
}
