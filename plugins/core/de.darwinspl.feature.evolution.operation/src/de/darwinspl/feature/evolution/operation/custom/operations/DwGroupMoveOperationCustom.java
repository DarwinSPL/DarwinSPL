package de.darwinspl.feature.evolution.operation.custom.operations;

import de.darwinspl.feature.evolution.operation.custom.DwOperationStringUtil;
import de.darwinspl.feature.operation.impl.DwGroupMoveOperationImpl;

public class DwGroupMoveOperationCustom extends DwGroupMoveOperationImpl {
	
	@Override
	public String toString() {
		String dateAndContributorInfo = DwOperationStringUtil.getContributorAndDateInfo(this);
		
		StringBuilder infoBuilder = new StringBuilder();
		infoBuilder.append("Group Move Operation".toUpperCase());
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(dateAndContributorInfo);
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(DwOperationStringUtil.printInfo("Group to Move", DwOperationStringUtil.getInfo(this.getGroup())));
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(DwOperationStringUtil.printContainedOperation("Triggered Group Detach Operation", this.getGroupDetachOperation()));
		infoBuilder.append(System.lineSeparator());
		
		infoBuilder.append(DwOperationStringUtil.printContainedOperation("Triggered Group Add Operation", this.getGroupAddOperation()));
		
		return infoBuilder.toString();
	}

}
