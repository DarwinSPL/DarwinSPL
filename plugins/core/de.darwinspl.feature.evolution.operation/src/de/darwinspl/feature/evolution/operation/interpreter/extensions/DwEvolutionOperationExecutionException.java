package de.darwinspl.feature.evolution.operation.interpreter.extensions;

import org.eclipse.emf.common.command.AbortExecutionException;

public class DwEvolutionOperationExecutionException extends AbortExecutionException {

	private static final long serialVersionUID = 159978008483784044L;

	public DwEvolutionOperationExecutionException() {
		super();
	}
	
	public DwEvolutionOperationExecutionException(String message) {
		super(message);
	}

}
