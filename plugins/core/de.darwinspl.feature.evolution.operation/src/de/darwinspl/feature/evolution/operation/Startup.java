package de.darwinspl.feature.evolution.operation;

import java.util.ArrayList;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;

import de.darwinspl.feature.evolution.operation.resource.DwEvolutionOperationModelResourceUtil;
import de.darwinspl.feature.util.custom.DwFeatureUtil;

public class Startup implements org.eclipse.ui.IStartup {
	
	@Override
	public void earlyStartup() {
		IResourceChangeListener listener = new IResourceChangeListener() {
			public void resourceChanged(IResourceChangeEvent event) {
				if (event.getType() != IResourceChangeEvent.POST_CHANGE)
					return;
				
				IResourceDelta rootDelta = event.getDelta();
				IResourceDelta docDelta = rootDelta.findMember(new Path("/"));
				if (docDelta == null)
					return;
				
				final ArrayList<IFile> deletedResources = new ArrayList<>();
				
				IResourceDeltaVisitor visitor = new IResourceDeltaVisitor() {
					public boolean visit(IResourceDelta delta) {
						// only interested in removed resources (not added or changed)
						if (delta.getKind() != IResourceDelta.REMOVED)
							return true;
						IResource resource = delta.getResource();
						
						// only interested in files with the "tfm" extension
						if(resource instanceof IFile) {
							String tfmFileExtension = DwFeatureUtil.getFeatureModelFileExtensionForXmi();
							if (resource.getType() == IResource.FILE && tfmFileExtension.equalsIgnoreCase(resource.getFileExtension())) {
								deletedResources.add((IFile) resource);
							}
						}
						return true;
					}
				};
				
				try {
					docDelta.accept(visitor);
				} catch (CoreException e) {
					System.out.println(e);
				}
				
				// nothing more to do if there were no deleted TFMs
				if (deletedResources.size() == 0)
					return;
				
				for(IFile deletedResource : deletedResources)
					DwEvolutionOperationModelResourceUtil.resourceDeleted(deletedResource);
			}
		};

		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		workspace.addResourceChangeListener(listener);
	}

}
