package de.darwinspl.feature.evolution.operation.resource;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import de.darwinspl.feature.operation.DwEvolutionOperation;
import de.darwinspl.feature.operation.DwEvolutionOperationModel;
import de.darwinspl.feature.operation.DwFeatureAddOperation;
import de.darwinspl.feature.operation.DwFeatureCreateOperation;
import de.darwinspl.feature.operation.DwFeatureDeleteOperation;
import de.darwinspl.feature.operation.DwFeatureMoveOperation;
import de.darwinspl.feature.operation.DwFeatureRenameOperation;
import de.darwinspl.feature.operation.DwFeatureTypeChangeOperation;
import de.darwinspl.feature.operation.DwGroupCreateOperation;
import de.darwinspl.feature.operation.DwGroupDeleteOperation;
import de.darwinspl.feature.operation.DwGroupMoveOperation;
import de.darwinspl.feature.operation.DwGroupTypeChangeOperation;
import de.darwinspl.temporal.DwTemporalElement;

public class DwEvolutionOperationUtil {
	
	public static Map<Date, List<DwEvolutionOperation>> getSortedOperationListsForEachDate(DwEvolutionOperationModel operationsModel) {
		List<Date> allDates = collectDates(operationsModel);
		return getSortedOperationListsForEachDate(operationsModel, allDates);
	}
	
	public static Map<Date, List<DwEvolutionOperation>> getSortedOperationListsForEachDate(DwEvolutionOperationModel operationsModel, List<Date> dates) {
		Map<Date, List<DwEvolutionOperation>> result = new HashMap<>();
		
		for(Date date : dates)
			result.put(date, new LinkedList<>());
		
		// Iterate through the operation model.
		// The list of stored operations is sorted in terms of when users performed operations.
		// That means that iterating over all operations and appending them to the corresponding
		// operation list will order all operations in time.
		EList<DwEvolutionOperation> allOperations = operationsModel.getEvolutionOperations();
		for(DwEvolutionOperation operation : allOperations) {
			Date operationDate = operation.getOperationDate();
			if(!result.keySet().contains(operationDate))
				continue;
				
			result.get(operation.getOperationDate()).add(operation);
		}
		
		return result;
	}
	
	public static List<Date> collectDates(DwEvolutionOperationModel model) {
		Set<Date> resultSet = new HashSet<>();
		for(DwEvolutionOperation operation : model.getEvolutionOperations())
			resultSet.add(operation.getOperationDate());

		List<Date> resultList = new ArrayList<Date>(resultSet);
		Collections.sort(resultList);
		return resultList;
	}

	public static List<DwEvolutionOperation> findNestedDeleteOperationsThatDeleteAnElement(DwEvolutionOperation operation, EObject elementOfInterest) {
		List<DwEvolutionOperation> result = new ArrayList<>();
		findNestedDeleteOperationsThatDeleteAnElement(operation, elementOfInterest, result);
		return result;
	}

	public static void findNestedDeleteOperationsThatDeleteAnElement(DwEvolutionOperation operation, EObject elementOfInterest, List<DwEvolutionOperation> foundOperations) {
		if(operation instanceof DwFeatureDeleteOperation) {
			DwFeatureDeleteOperation deleteOperation = (DwFeatureDeleteOperation) operation;
			if(deleteOperation.getFeature().equals(elementOfInterest)) {
				// extend this operation
				foundOperations.add(deleteOperation);
			}
		}
		else if(operation instanceof DwGroupDeleteOperation) {
			DwGroupDeleteOperation deleteOperation = (DwGroupDeleteOperation) operation;
			if(deleteOperation.getGroup().equals(elementOfInterest)) {
				// extend this operation
				foundOperations.add(deleteOperation);
			}
		}
		
		for(EObject containedOperation : operation.eContents()) {
			if(containedOperation instanceof DwEvolutionOperation) {
				findNestedDeleteOperationsThatDeleteAnElement((DwEvolutionOperation) containedOperation, elementOfInterest, foundOperations);
			}
		}
	}
	
	

	public static void collectAllDeletedElements(DwEvolutionOperation operation, Set<DwTemporalElement> foundElements) {
		if(operation instanceof DwFeatureDeleteOperation) {
			DwFeatureDeleteOperation deleteOperation = (DwFeatureDeleteOperation) operation;
			foundElements.add(deleteOperation.getFeature());
		}
		else if(operation instanceof DwGroupDeleteOperation) {
			DwGroupDeleteOperation deleteOperation = (DwGroupDeleteOperation) operation;
			foundElements.add(deleteOperation.getGroup());
		}
		
		for(EObject containedOperation : operation.eContents()) {
			if(containedOperation instanceof DwEvolutionOperation) {
				collectAllDeletedElements((DwEvolutionOperation) containedOperation, foundElements);
			}
		}
	}
	
	
	
	public static Set<DwEvolutionOperation> collectNestedOperations(DwEvolutionOperationModel model) {
		Set<DwEvolutionOperation> operations = new HashSet<>();
		collectNestedOperations(model, operations);
		return operations;
	}
	
	public static void collectNestedOperations(DwEvolutionOperationModel model, Set<DwEvolutionOperation> nestedOperations) {
		for(DwEvolutionOperation operation : model.getEvolutionOperations()) {
			collectNestedOperations(operation, nestedOperations);
		}
	}
	
	public static void collectNestedOperations(DwEvolutionOperation operation, Set<DwEvolutionOperation> nestedOperations) {
		if(operation instanceof DwFeatureCreateOperation
				|| operation instanceof DwFeatureMoveOperation
				|| operation instanceof DwFeatureDeleteOperation
				|| operation instanceof DwFeatureRenameOperation
				|| operation instanceof DwFeatureTypeChangeOperation
				
				|| operation instanceof DwGroupCreateOperation
				|| operation instanceof DwGroupMoveOperation
				|| operation instanceof DwGroupTypeChangeOperation) {
			nestedOperations.add(operation);
		}
		else {
			// Ignore this kind of operation
		}
		
		for(EObject content : operation.eContents()) {
			if(content instanceof DwEvolutionOperation) {
				collectNestedOperations((DwEvolutionOperation) content, nestedOperations);
			}
		}
	}
	
	
	
	public static Map<String, Integer> getOperationCounts(Collection<DwEvolutionOperation> operations) {
		Map<String, Integer> countMap = new HashMap<>();
		countMap.put("f_create", 0);
		countMap.put("f_move", 0);
		countMap.put("f_delete", 0);
		countMap.put("f_rename", 0);
		countMap.put("f_type", 0);

		countMap.put("g_create", 0);
		countMap.put("g_move", 0);
		countMap.put("g_type", 0);

		for(DwEvolutionOperation operation : operations) {
			if(operation instanceof DwFeatureCreateOperation)
				countMap.put("f_create", countMap.get("f_create")+1);
			else if(operation instanceof DwFeatureMoveOperation)
				countMap.put("f_move", countMap.get("f_move")+1);
			else if(operation instanceof DwFeatureDeleteOperation)
				countMap.put("f_delete", countMap.get("f_delete")+1);
			else if(operation instanceof DwFeatureRenameOperation)
				countMap.put("f_rename", countMap.get("f_rename")+1);
			else if(operation instanceof DwFeatureTypeChangeOperation)
				countMap.put("f_type", countMap.get("f_type")+1);
			
			else if(operation instanceof DwGroupCreateOperation)
				countMap.put("g_create", countMap.get("g_create")+1);
			else if(operation instanceof DwGroupMoveOperation)
				countMap.put("g_move", countMap.get("g_move")+1);
			else if(operation instanceof DwGroupTypeChangeOperation)
				countMap.put("g_type", countMap.get("g_type")+1);
			
//			else
//				throw new RuntimeException("Unknown edit operation");
		}
		
		return countMap;
	}
	

}








