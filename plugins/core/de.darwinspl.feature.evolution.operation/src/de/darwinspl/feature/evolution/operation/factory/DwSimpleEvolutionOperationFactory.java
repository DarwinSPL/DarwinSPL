package de.darwinspl.feature.evolution.operation.factory;

import java.util.Date;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureTypeEnum;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupType;
import de.darwinspl.feature.DwGroupTypeEnum;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.contributor.ContributorUtils;
import de.darwinspl.feature.contributor.DwContributor;
import de.darwinspl.feature.contributor.DwContributorFactory;
import de.darwinspl.feature.contributor.preferences.DwContributorPreferencesManager;
import de.darwinspl.feature.operation.DwEvolutionOperation;
import de.darwinspl.feature.operation.DwFeatureAddOperation;
import de.darwinspl.feature.operation.DwFeatureAttributeCreateOperation;
import de.darwinspl.feature.operation.DwFeatureAttributeDeleteOperation;
import de.darwinspl.feature.operation.DwFeatureAttributeRenameOperation;
import de.darwinspl.feature.operation.DwFeatureCreateOperation;
import de.darwinspl.feature.operation.DwFeatureDeleteOperation;
import de.darwinspl.feature.operation.DwFeatureDetachOperation;
import de.darwinspl.feature.operation.DwFeatureMoveOperation;
import de.darwinspl.feature.operation.DwFeatureRenameOperation;
import de.darwinspl.feature.operation.DwFeatureTypeChangeOperation;
import de.darwinspl.feature.operation.DwFeatureVersionCreateOperation;
import de.darwinspl.feature.operation.DwFeatureVersionDeleteOperation;
import de.darwinspl.feature.operation.DwFeatureVersionRenameOperation;
import de.darwinspl.feature.operation.DwGroupAddOperation;
import de.darwinspl.feature.operation.DwGroupCreateOperation;
import de.darwinspl.feature.operation.DwGroupDeleteOperation;
import de.darwinspl.feature.operation.DwGroupDetachOperation;
import de.darwinspl.feature.operation.DwGroupMoveOperation;
import de.darwinspl.feature.operation.DwGroupTypeChangeOperation;
import de.darwinspl.feature.operation.DwOperationFactory;
import de.darwinspl.feature.util.custom.DwFeatureUtil;
import de.darwinspl.feature.util.custom.DwSimpleFeatureFactoryCustom;

public class DwSimpleEvolutionOperationFactory {
	
	private DwOperationFactory factory;
	
	public DwSimpleEvolutionOperationFactory () {
		factory = DwOperationFactory.eINSTANCE;
	}
	
	
	
	
	
	public DwFeatureCreateOperation createDwFeatureCreateOperation(DwTemporalFeatureModel featureModel, EObject parent, boolean forceCreateParentGroup, Date date, boolean lastDateSelected) {
		DwFeatureCreateOperation operation = factory.createDwFeatureCreateOperation();
		operation.setOperationDate(date);
		operation.setIntermediateOperation(!lastDateSelected);
		operation.setFeatureModel(featureModel);
		
		DwFeature newFeature = DwSimpleFeatureFactoryCustom.createNewFeatureWithUnconflictingName(featureModel, date);
		operation.setFeature(newFeature);
		
		if(parent != null) {
			DwFeatureAddOperation addOperation = createDwFeatureAddOperation(featureModel, newFeature, parent, forceCreateParentGroup, date, lastDateSelected);
			operation.setFeatureAddOperation(addOperation);
		}
		
		setGenericInformation(operation);
		return operation;
	}
	
	
	public DwFeatureTypeChangeOperation createDwFeatureTypeChangeOperation(DwTemporalFeatureModel featureModel, DwFeature feature, DwFeatureTypeEnum newType, Date date, boolean lastDateSelected) {
		DwFeatureTypeChangeOperation operation = factory.createDwFeatureTypeChangeOperation();
		operation.setOperationDate(date);
		operation.setIntermediateOperation(!lastDateSelected);
		
		operation.setFeature(feature);
		operation.setFeatureModel(featureModel);
		operation.setNewFeatureTypeEnum(newType);
		
		setGenericInformation(operation);
		return operation;
	}

	
	public DwFeatureAddOperation createDwFeatureAddOperation(DwTemporalFeatureModel featureModel, DwFeature feature, EObject parent, boolean forceCreateParentGroup, Date date, boolean lastDateSelected) {
		DwFeatureAddOperation operation = factory.createDwFeatureAddOperation();
		operation.setOperationDate(date);
		operation.setIntermediateOperation(!lastDateSelected);
		
		operation.setFeature(feature);
		operation.setFeatureModel(featureModel);
		operation.setParent(parent);
		operation.setForceCreateParentGroup(forceCreateParentGroup);
		
		// Setup for the contained operations:
		{
			if (parent instanceof DwFeature) {
				DwFeature parentFeature = (DwFeature) parent;
				List<DwGroup> childGroups = DwFeatureUtil.getChildGroupsOfFeature(parentFeature, date);

				if (!forceCreateParentGroup) {
					for (DwGroup childGroup : childGroups) {
//						DwGroupType groupType = DwFeatureUtil.getType(childGroup, date);
//						if (groupType.getType().equals(DwGroupTypeEnum.AND)) {
							operation.setGroupToAddTo(childGroup);
//						}
					}
				}
				
				if (operation.getGroupToAddTo() == null) {
					DwGroupCreateOperation groupCreateOperation = createDwGroupCreateOperation(featureModel, parentFeature, date, lastDateSelected);
					operation.setGroupCreateOperation(groupCreateOperation);
					operation.setGroupToAddTo(groupCreateOperation.getGroup());

					DwGroupTypeChangeOperation groupTypeChangeOperation = createDwGroupTypeChangeOperation(featureModel, operation.getGroupToAddTo(), DwGroupTypeEnum.AND, date, lastDateSelected);
					operation.setGroupTypeChangeOperation(groupTypeChangeOperation);
				}
			} else if (parent instanceof DwGroup) {
				operation.setGroupToAddTo((DwGroup) parent);
			}
		}
		
		DwGroup groupToAddTo = operation.getGroupToAddTo();
		if(groupToAddTo == null)
			throw new RuntimeException("Group to add to must be known !!!");
		
		DwGroupType groupType = DwFeatureUtil.getType(groupToAddTo, date);
		if(!groupType.getType().equals(DwGroupTypeEnum.AND)) {
			// Change the feature type to optional since it was added to an OR / ALTERNATIVE group
			operation.setConsequentFeatureTypeChangeOperation(createDwFeatureTypeChangeOperation(featureModel, feature, DwFeatureTypeEnum.OPTIONAL, date, lastDateSelected));
		}
		
		setGenericInformation(operation);
		return operation;
	}

	
	public DwFeatureDeleteOperation createDwFeatureDeleteOperation(DwTemporalFeatureModel tfm, DwFeature feature, Date date, boolean lastDateSelected) {
		DwFeatureDeleteOperation operation = factory.createDwFeatureDeleteOperation();
		operation.setOperationDate(date);
		operation.setIntermediateOperation(!lastDateSelected);
		
		operation.setFeature(feature);
		operation.setFeatureModel(tfm);
		
		DwFeatureDetachOperation detachOperation = createDwFeatureDetachOperation(tfm, feature, date, lastDateSelected);
		operation.setDetachOperation(detachOperation);
		
		setGenericInformation(operation);
		
//		DwTemporalInterval intervalCopy = EcoreUtil.copy(DwEvolutionUtil.getValidIntervalForDate(feature, date));
//		intervalCopy.setTo(date);
//		if (!DwEvolutionUtil.isEverValid(intervalCopy)) {
			for(DwGroup subGroup : DwFeatureUtil.getChildGroupsOfFeature(feature, date)) {
				DwGroupDeleteOperation consequentDeleteEvoOp = createDwGroupDeleteOperation(tfm, subGroup, date, lastDateSelected);
				operation.getConsequentGroupOperations().add(consequentDeleteEvoOp);
			}
//		}
		
		return operation;
	}

	
	public DwFeatureDetachOperation createDwFeatureDetachOperation(DwTemporalFeatureModel tfm, DwFeature feature, Date date, boolean lastDateSelected) {
		DwFeatureDetachOperation operation = factory.createDwFeatureDetachOperation();
		operation.setOperationDate(date);
		operation.setIntermediateOperation(!lastDateSelected);
		
		operation.setFeature(feature);
		operation.setFeatureModel(tfm);
		
		DwGroup parentGroup = DwFeatureUtil.getParentGroupOfFeature(feature, date);
		if(DwFeatureUtil.getChildFeaturesOfGroup(parentGroup, date).size() == 1) {
			// The parent group will be empty after this operation!
			DwGroupDeleteOperation groupDeleteOperation = createDwGroupDeleteOperationWithoutRecursion(tfm, parentGroup, date, lastDateSelected);
			operation.setConsequentGroupOperation(groupDeleteOperation);
		}
		
		setGenericInformation(operation);
		return operation;
	}

	
	public DwFeatureMoveOperation createDwFeatureMoveOperation(DwTemporalFeatureModel tfm, DwFeature feature, EObject newParent, Date date, boolean lastDateSelected) {
		DwFeatureMoveOperation operation = factory.createDwFeatureMoveOperation();
		operation.setOperationDate(date);
		operation.setIntermediateOperation(!lastDateSelected);
		operation.setFeatureModel(tfm);
		
		operation.setFeature(feature);
		operation.setDetachOperation(createDwFeatureDetachOperation(tfm, feature, date, lastDateSelected));
		operation.setAddOperation(createDwFeatureAddOperation(tfm, feature, newParent, false, date, lastDateSelected));
		
		setGenericInformation(operation);
		return operation;
	}

	
	public DwFeatureRenameOperation createDwFeatureRenameOperation(DwTemporalFeatureModel tfm, DwFeature feature, String newName, Date date, boolean lastDateSelected) {
		DwFeatureRenameOperation operation = factory.createDwFeatureRenameOperation();
		operation.setOperationDate(date);
		operation.setIntermediateOperation(!lastDateSelected);
		
		operation.setFeature(feature);
		operation.setFeatureModel(tfm);
		operation.setOldNameString(DwFeatureUtil.getName(feature, date).getName());
		operation.setNewNameString(newName);
		
		setGenericInformation(operation);
		return operation;
	}

	
	public DwFeatureVersionCreateOperation createDwFeatureVersionCreateOperation() {
		throw new UnsupportedOperationException("Version operations are currently not supported.");
	}

	
	public DwFeatureVersionDeleteOperation createDwFeatureVersionDeleteOperation() {
		throw new UnsupportedOperationException("Version operations are currently not supported.");
	}

	
	public DwFeatureVersionRenameOperation createDwFeatureVersionRenameOperation() {
		throw new UnsupportedOperationException("Version operations are currently not supported.");
	}

	
	public DwFeatureAttributeCreateOperation createDwFeatureAttributeCreateOperation() {
		throw new UnsupportedOperationException("Attribute operations are currently not supported.");
	}

	
	public DwFeatureAttributeDeleteOperation createDwFeatureAttributeDeleteOperation() {
		throw new UnsupportedOperationException("Attribute operations are currently not supported.");
	}

	
	public DwFeatureAttributeRenameOperation createDwFeatureAttributeRenameOperation() {
		throw new UnsupportedOperationException("Attribute operations are currently not supported.");
	}

	
	public DwGroupTypeChangeOperation createDwGroupTypeChangeOperation(DwTemporalFeatureModel featureModel, DwGroup group, DwGroupTypeEnum newType, Date date, boolean lastDateSelected) {
		DwGroupTypeChangeOperation operation = factory.createDwGroupTypeChangeOperation();
		operation.setIntermediateOperation(!lastDateSelected);
		
		operation.setGroup(group);
		operation.setFeatureModel(featureModel);
		operation.setOperationDate(date);
		operation.setNewGroupTypeEnum(newType);
		
		// If the group type is set to "OR", change all feature children types to "OPTIONAL"
		if(newType == DwGroupTypeEnum.OR) {
			for(DwFeature childFeature : DwFeatureUtil.getChildFeaturesOfGroup(group, date)) {
				if(DwFeatureUtil.getType(childFeature, date).getType() == DwFeatureTypeEnum.MANDATORY) {
					operation.getConsequentOperations().add(createDwFeatureTypeChangeOperation(featureModel, childFeature, DwFeatureTypeEnum.OPTIONAL, date, lastDateSelected));
				}
			}
		}
		
		setGenericInformation(operation);
		return operation;
	}

	
	public DwGroupCreateOperation createDwGroupCreateOperation(DwTemporalFeatureModel featureModel, DwFeature parentFeature, Date date, boolean lastDateSelected) {
		DwGroupCreateOperation operation = factory.createDwGroupCreateOperation();
		operation.setIntermediateOperation(!lastDateSelected);
		
		operation.setFeatureModel(featureModel);
		operation.setOperationDate(date);
		
		DwGroup newGroup = DwSimpleFeatureFactoryCustom.createNewGroup(date);
		operation.setGroup(newGroup);
		
		DwGroupAddOperation groupAddOperation = createDwGroupAddOperation(featureModel, newGroup, parentFeature, date, lastDateSelected);
		operation.setGroupAddOperation(groupAddOperation);
		
		setGenericInformation(operation);
		return operation;
	}

	
	public DwGroupAddOperation createDwGroupAddOperation(DwTemporalFeatureModel featureModel, DwGroup group, DwFeature newParentFeature, Date date, boolean lastDateSelected) {
		DwGroupAddOperation operation = factory.createDwGroupAddOperation();
		operation.setIntermediateOperation(!lastDateSelected);
		
		operation.setGroup(group);
		operation.setFeatureModel(featureModel);
		operation.setOperationDate(date);
		operation.setParentFeature(newParentFeature);
		
		setGenericInformation(operation);
		return operation;
	}
	
	
	private DwGroupDeleteOperation createDwGroupDeleteOperationWithoutRecursion(DwTemporalFeatureModel tfm, DwGroup group, Date date, boolean lastDateSelected) {
		DwGroupDeleteOperation operation = factory.createDwGroupDeleteOperation();
		operation.setIntermediateOperation(!lastDateSelected);
		
		operation.setGroup(group);
		operation.setFeatureModel(tfm);
		operation.setOperationDate(date);
		
		DwGroupDetachOperation detachOperation = createDwGroupDetachOperation(tfm, group, date, lastDateSelected);
		operation.setDetachOperation(detachOperation);
		
		setGenericInformation(operation);
		return operation;
	}
	
	public DwGroupDeleteOperation createDwGroupDeleteOperation(DwTemporalFeatureModel tfm, DwGroup group, Date date, boolean lastDateSelected) {
		DwGroupDeleteOperation operation = createDwGroupDeleteOperationWithoutRecursion(tfm, group, date, lastDateSelected);

//		DwTemporalInterval intervalCopy = EcoreUtil.copy(DwEvolutionUtil.getValidIntervalForDate(group, date));
//		intervalCopy.setTo(date);
//		if(!DwEvolutionUtil.isEverValid(intervalCopy)) {
			for(DwFeature subFeature : DwFeatureUtil.getChildFeaturesOfGroup(group, date)) {
				DwFeatureDeleteOperation consequentDeleteEvoOp = createDwFeatureDeleteOperation(tfm, subFeature, date, lastDateSelected);
				operation.getConsequentFeatureOperations().add(consequentDeleteEvoOp);
				
				consequentDeleteEvoOp.getDetachOperation().setConsequentGroupOperation(null);
			}
//		}
		
		return operation;
	}

	
	public DwGroupDetachOperation createDwGroupDetachOperation(DwTemporalFeatureModel tfm, DwGroup group, Date date, boolean lastDateSelected) {
		DwGroupDetachOperation operation = factory.createDwGroupDetachOperation();
		operation.setIntermediateOperation(!lastDateSelected);
		
		operation.setGroup(group);
		operation.setFeatureModel(tfm);
		operation.setOperationDate(date);
		
		setGenericInformation(operation);
		return operation;
	}

	
	public DwGroupMoveOperation createDwGroupMoveOperation(DwTemporalFeatureModel tfm, DwGroup group, DwFeature newParentFeature, Date date, boolean lastDateSelected) {
		DwGroupMoveOperation operation = factory.createDwGroupMoveOperation();
		operation.setIntermediateOperation(!lastDateSelected);
		
		operation.setGroup(group);
		operation.setFeatureModel(tfm);
		
		operation.setGroupDetachOperation(createDwGroupDetachOperation(tfm, group, date, lastDateSelected));
		operation.setGroupAddOperation(createDwGroupAddOperation(tfm, group, newParentFeature, date, lastDateSelected));
		operation.setOperationDate(date);
		
		setGenericInformation(operation);
		return operation;
	}
	
	
	

	private void setGenericInformation(DwEvolutionOperation operation) {
		operation.createId();
		
		// Set the contributor
		{
			String contributorName = DwContributorPreferencesManager.getContributorName();
			String contributorId = ContributorUtils.getEncodedStringUTF8(DwContributorPreferencesManager.getContributorId());
			
			DwContributor contributor = DwContributorFactory.eINSTANCE.createDwContributor();
			contributor.setName(contributorName);
			contributor.setId(contributorId);
			
			operation.setContributor(contributor);
		}
		
		if(operation.getFeatureModel() == null)
			throw new NullPointerException("Feature model not set");
	}

}
