package de.darwinspl.feature.evolution.operation.interpreter;

import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.operation.DwEvolutionOperation;
import de.darwinspl.feature.operation.DwFeatureVersionCreateOperation;
import de.darwinspl.feature.operation.DwFeatureVersionDeleteOperation;
import de.darwinspl.feature.operation.DwFeatureVersionRenameOperation;

/**
 * This class contains the actual operation to execute and undo evolution operations on {@link DwFeatureVersion}s.
 * The {@link #execute(DwEvolutionOperation)} and {@link #undo(DwEvolutionOperation)} methods delegate to their respective handler methods depending on the type of evolution Operation.
 * This class should not be used directly to call execute operations. Please use the {@link DwEvolutionOperationInterpreter} class' execute and undo method instead.
 *
 */
public class DwVersionOperationInterpreter {

//	private static final DwOperationFactory operationFactory = DwOperationFactory.eINSTANCE;
//	private static final DwFeatureFactory featureFactory = DwFeatureFactory.eINSTANCE;

	public static void execute(DwEvolutionOperation evoOp) {
		if (evoOp instanceof DwFeatureVersionCreateOperation)
			handleExecute((DwFeatureVersionCreateOperation) evoOp);

		else if (evoOp instanceof DwFeatureVersionDeleteOperation)
			handleExecute((DwFeatureVersionDeleteOperation) evoOp);

		else if (evoOp instanceof DwFeatureVersionRenameOperation)
			handleExecute((DwFeatureVersionRenameOperation) evoOp);
	}

	public static void undo(DwEvolutionOperation evoOp) {
		if (evoOp instanceof DwFeatureVersionCreateOperation)
			handleUndo((DwFeatureVersionCreateOperation) evoOp);

		else if (evoOp instanceof DwFeatureVersionDeleteOperation)
			handleUndo((DwFeatureVersionDeleteOperation) evoOp);

		else if (evoOp instanceof DwFeatureVersionRenameOperation)
			handleUndo((DwFeatureVersionRenameOperation) evoOp);

	}

	private static void handleExecute(DwFeatureVersionCreateOperation evoOp) {
		// TODO Auto-generated method stub

	}

	private static void handleUndo(DwFeatureVersionCreateOperation evoOp) {
		// TODO Auto-generated method stub
	
	}

	private static void handleExecute(DwFeatureVersionDeleteOperation evoOp) {
		// TODO Auto-generated method stub

	}

	private static void handleUndo(DwFeatureVersionDeleteOperation evoOp) {
		// TODO Auto-generated method stub
	
	}

	private static void handleExecute(DwFeatureVersionRenameOperation evoOp) {
		// TODO Auto-generated method stub

	}

	private static void handleUndo(DwFeatureVersionRenameOperation evoOp) {
		// TODO Auto-generated method stub

	}
}
