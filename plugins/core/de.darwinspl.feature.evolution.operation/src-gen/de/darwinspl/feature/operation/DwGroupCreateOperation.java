/**
 */
package de.darwinspl.feature.operation;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Group Create Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.DwGroupCreateOperation#getGroupAddOperation <em>Group Add Operation</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwGroupCreateOperation()
 * @model
 * @generated
 */
public interface DwGroupCreateOperation extends DwGroupOperation {

	/**
	 * Returns the value of the '<em><b>Group Add Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group Add Operation</em>' containment reference.
	 * @see #setGroupAddOperation(DwGroupAddOperation)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwGroupCreateOperation_GroupAddOperation()
	 * @model containment="true" required="true"
	 * @generated
	 */
	DwGroupAddOperation getGroupAddOperation();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwGroupCreateOperation#getGroupAddOperation <em>Group Add Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group Add Operation</em>' containment reference.
	 * @see #getGroupAddOperation()
	 * @generated
	 */
	void setGroupAddOperation(DwGroupAddOperation value);
} // DwGroupCreateOperation
