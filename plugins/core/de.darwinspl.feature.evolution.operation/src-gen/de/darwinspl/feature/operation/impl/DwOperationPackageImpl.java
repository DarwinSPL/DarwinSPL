/**
 */
package de.darwinspl.feature.operation.impl;

import de.darwinspl.datatypes.DwDatatypesPackage;

import de.darwinspl.feature.DwFeaturePackage;

import de.darwinspl.feature.contributor.DwContributorPackage;

import de.darwinspl.feature.operation.DwEvolutionOperation;
import de.darwinspl.feature.operation.DwEvolutionOperationModel;
import de.darwinspl.feature.operation.DwFeatureAddOperation;
import de.darwinspl.feature.operation.DwFeatureAttributeCreateOperation;
import de.darwinspl.feature.operation.DwFeatureAttributeDeleteOperation;
import de.darwinspl.feature.operation.DwFeatureAttributeOperation;
import de.darwinspl.feature.operation.DwFeatureAttributeRenameOperation;
import de.darwinspl.feature.operation.DwFeatureCreateOperation;
import de.darwinspl.feature.operation.DwFeatureDeleteOperation;
import de.darwinspl.feature.operation.DwFeatureDetachOperation;
import de.darwinspl.feature.operation.DwFeatureMoveOperation;
import de.darwinspl.feature.operation.DwFeatureOperation;
import de.darwinspl.feature.operation.DwFeatureRenameOperation;
import de.darwinspl.feature.operation.DwFeatureTypeChangeOperation;
import de.darwinspl.feature.operation.DwFeatureVersionCreateOperation;
import de.darwinspl.feature.operation.DwFeatureVersionDeleteOperation;
import de.darwinspl.feature.operation.DwFeatureVersionOperation;
import de.darwinspl.feature.operation.DwFeatureVersionRenameOperation;
import de.darwinspl.feature.operation.DwGroupAddOperation;
import de.darwinspl.feature.operation.DwGroupCreateOperation;
import de.darwinspl.feature.operation.DwGroupDeleteOperation;
import de.darwinspl.feature.operation.DwGroupDetachOperation;
import de.darwinspl.feature.operation.DwGroupMoveOperation;
import de.darwinspl.feature.operation.DwGroupOperation;
import de.darwinspl.feature.operation.DwGroupTypeChangeOperation;
import de.darwinspl.feature.operation.DwOperationFactory;
import de.darwinspl.feature.operation.DwOperationPackage;

import de.darwinspl.temporal.DwTemporalModelPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DwOperationPackageImpl extends EPackageImpl implements DwOperationPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwEvolutionOperationModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwEvolutionOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureCreateOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureAddOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureDeleteOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureDetachOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureMoveOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureRenameOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureTypeChangeOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureAttributeOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureAttributeCreateOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureAttributeDeleteOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureAttributeRenameOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureVersionOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureVersionCreateOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureVersionDeleteOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureVersionRenameOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwGroupOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwGroupCreateOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwGroupAddOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwGroupDeleteOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwGroupDetachOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwGroupMoveOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwGroupTypeChangeOperationEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.darwinspl.feature.operation.DwOperationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private DwOperationPackageImpl() {
		super(eNS_URI, DwOperationFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link DwOperationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static DwOperationPackage init() {
		if (isInited) return (DwOperationPackage)EPackage.Registry.INSTANCE.getEPackage(DwOperationPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredDwOperationPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		DwOperationPackageImpl theDwOperationPackage = registeredDwOperationPackage instanceof DwOperationPackageImpl ? (DwOperationPackageImpl)registeredDwOperationPackage : new DwOperationPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		DwContributorPackage.eINSTANCE.eClass();
		DwDatatypesPackage.eINSTANCE.eClass();
		DwFeaturePackage.eINSTANCE.eClass();
		DwTemporalModelPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theDwOperationPackage.createPackageContents();

		// Initialize created meta-data
		theDwOperationPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theDwOperationPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(DwOperationPackage.eNS_URI, theDwOperationPackage);
		return theDwOperationPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwEvolutionOperationModel() {
		return dwEvolutionOperationModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwEvolutionOperationModel_EvolutionOperations() {
		return (EReference)dwEvolutionOperationModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwEvolutionOperationModel_Contributors() {
		return (EReference)dwEvolutionOperationModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwEvolutionOperation() {
		return dwEvolutionOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwEvolutionOperation_OperationDate() {
		return (EAttribute)dwEvolutionOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwEvolutionOperation_CommandStackIndex() {
		return (EAttribute)dwEvolutionOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwEvolutionOperation_FeatureModel() {
		return (EReference)dwEvolutionOperationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwEvolutionOperation_Contributor() {
		return (EReference)dwEvolutionOperationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwEvolutionOperation_IntermediateOperation() {
		return (EAttribute)dwEvolutionOperationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureOperation() {
		return dwFeatureOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureOperation_Feature() {
		return (EReference)dwFeatureOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureCreateOperation() {
		return dwFeatureCreateOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureCreateOperation_FeatureAddOperation() {
		return (EReference)dwFeatureCreateOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureAddOperation() {
		return dwFeatureAddOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureAddOperation_Parent() {
		return (EReference)dwFeatureAddOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwFeatureAddOperation_ForceCreateParentGroup() {
		return (EAttribute)dwFeatureAddOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureAddOperation_OldGroupComposition() {
		return (EReference)dwFeatureAddOperationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureAddOperation_NewGroupComposition() {
		return (EReference)dwFeatureAddOperationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureAddOperation_GroupCreateOperation() {
		return (EReference)dwFeatureAddOperationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureAddOperation_GroupTypeChangeOperation() {
		return (EReference)dwFeatureAddOperationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureAddOperation_GroupToAddTo() {
		return (EReference)dwFeatureAddOperationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureAddOperation_ConsequentFeatureTypeChangeOperation() {
		return (EReference)dwFeatureAddOperationEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureDeleteOperation() {
		return dwFeatureDeleteOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwFeatureDeleteOperation_OldFeatureValidUntil() {
		return (EAttribute)dwFeatureDeleteOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureDeleteOperation_ConsequentGroupOperations() {
		return (EReference)dwFeatureDeleteOperationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureDeleteOperation_DetachOperation() {
		return (EReference)dwFeatureDeleteOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureDetachOperation() {
		return dwFeatureDetachOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureDetachOperation_OldGroupMembership() {
		return (EReference)dwFeatureDetachOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureDetachOperation_NewGroupMembership() {
		return (EReference)dwFeatureDetachOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwFeatureDetachOperation_OldValidUntil() {
		return (EAttribute)dwFeatureDetachOperationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureDetachOperation_ConsequentGroupOperation() {
		return (EReference)dwFeatureDetachOperationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureMoveOperation() {
		return dwFeatureMoveOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureMoveOperation_DetachOperation() {
		return (EReference)dwFeatureMoveOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureMoveOperation_AddOperation() {
		return (EReference)dwFeatureMoveOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureRenameOperation() {
		return dwFeatureRenameOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureRenameOperation_NewName() {
		return (EReference)dwFeatureRenameOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwFeatureRenameOperation_NewNameString() {
		return (EAttribute)dwFeatureRenameOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureRenameOperation_OldName() {
		return (EReference)dwFeatureRenameOperationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwFeatureRenameOperation_OldNameString() {
		return (EAttribute)dwFeatureRenameOperationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureTypeChangeOperation() {
		return dwFeatureTypeChangeOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureTypeChangeOperation_NewFeatureType() {
		return (EReference)dwFeatureTypeChangeOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwFeatureTypeChangeOperation_NewFeatureTypeEnum() {
		return (EAttribute)dwFeatureTypeChangeOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureTypeChangeOperation_OldFeatureType() {
		return (EReference)dwFeatureTypeChangeOperationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureAttributeOperation() {
		return dwFeatureAttributeOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureAttributeOperation_Attribute() {
		return (EReference)dwFeatureAttributeOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureAttributeCreateOperation() {
		return dwFeatureAttributeCreateOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureAttributeDeleteOperation() {
		return dwFeatureAttributeDeleteOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureAttributeRenameOperation() {
		return dwFeatureAttributeRenameOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureAttributeRenameOperation_OldName() {
		return (EReference)dwFeatureAttributeRenameOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureAttributeRenameOperation_NewName() {
		return (EReference)dwFeatureAttributeRenameOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureVersionOperation() {
		return dwFeatureVersionOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureVersionOperation_Version() {
		return (EReference)dwFeatureVersionOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureVersionCreateOperation() {
		return dwFeatureVersionCreateOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureVersionCreateOperation_Parent() {
		return (EReference)dwFeatureVersionCreateOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwFeatureVersionCreateOperation_UseSameBranch() {
		return (EAttribute)dwFeatureVersionCreateOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwFeatureVersionCreateOperation_WireAndAddAfter() {
		return (EAttribute)dwFeatureVersionCreateOperationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureVersionDeleteOperation() {
		return dwFeatureVersionDeleteOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureVersionDeleteOperation_Parent() {
		return (EReference)dwFeatureVersionDeleteOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwFeatureVersionDeleteOperation_OldValidUntil() {
		return (EAttribute)dwFeatureVersionDeleteOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureVersionRenameOperation() {
		return dwFeatureVersionRenameOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwFeatureVersionRenameOperation_OldNumber() {
		return (EAttribute)dwFeatureVersionRenameOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwFeatureVersionRenameOperation_NewNumber() {
		return (EAttribute)dwFeatureVersionRenameOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwGroupOperation() {
		return dwGroupOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwGroupOperation_Group() {
		return (EReference)dwGroupOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwGroupCreateOperation() {
		return dwGroupCreateOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwGroupCreateOperation_GroupAddOperation() {
		return (EReference)dwGroupCreateOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwGroupAddOperation() {
		return dwGroupAddOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwGroupAddOperation_ParentFeature() {
		return (EReference)dwGroupAddOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwGroupAddOperation_NewContainer() {
		return (EReference)dwGroupAddOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwGroupDeleteOperation() {
		return dwGroupDeleteOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwGroupDeleteOperation_OldGroupValidUntil() {
		return (EAttribute)dwGroupDeleteOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwGroupDeleteOperation_ConsequentFeatureOperations() {
		return (EReference)dwGroupDeleteOperationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwGroupDeleteOperation_DetachOperation() {
		return (EReference)dwGroupDeleteOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwGroupDetachOperation() {
		return dwGroupDetachOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwGroupDetachOperation_OldContainer() {
		return (EReference)dwGroupDetachOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwGroupDetachOperation_OldParent() {
		return (EReference)dwGroupDetachOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwGroupMoveOperation() {
		return dwGroupMoveOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwGroupMoveOperation_GroupDetachOperation() {
		return (EReference)dwGroupMoveOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwGroupMoveOperation_GroupAddOperation() {
		return (EReference)dwGroupMoveOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwGroupTypeChangeOperation() {
		return dwGroupTypeChangeOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwGroupTypeChangeOperation_ConsequentOperations() {
		return (EReference)dwGroupTypeChangeOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwGroupTypeChangeOperation_NewGroupType() {
		return (EReference)dwGroupTypeChangeOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwGroupTypeChangeOperation_NewGroupTypeEnum() {
		return (EAttribute)dwGroupTypeChangeOperationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwGroupTypeChangeOperation_OldGroupType() {
		return (EReference)dwGroupTypeChangeOperationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwOperationFactory getDwOperationFactory() {
		return (DwOperationFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		dwEvolutionOperationModelEClass = createEClass(DW_EVOLUTION_OPERATION_MODEL);
		createEReference(dwEvolutionOperationModelEClass, DW_EVOLUTION_OPERATION_MODEL__EVOLUTION_OPERATIONS);
		createEReference(dwEvolutionOperationModelEClass, DW_EVOLUTION_OPERATION_MODEL__CONTRIBUTORS);

		dwEvolutionOperationEClass = createEClass(DW_EVOLUTION_OPERATION);
		createEAttribute(dwEvolutionOperationEClass, DW_EVOLUTION_OPERATION__OPERATION_DATE);
		createEAttribute(dwEvolutionOperationEClass, DW_EVOLUTION_OPERATION__COMMAND_STACK_INDEX);
		createEReference(dwEvolutionOperationEClass, DW_EVOLUTION_OPERATION__FEATURE_MODEL);
		createEReference(dwEvolutionOperationEClass, DW_EVOLUTION_OPERATION__CONTRIBUTOR);
		createEAttribute(dwEvolutionOperationEClass, DW_EVOLUTION_OPERATION__INTERMEDIATE_OPERATION);

		dwFeatureOperationEClass = createEClass(DW_FEATURE_OPERATION);
		createEReference(dwFeatureOperationEClass, DW_FEATURE_OPERATION__FEATURE);

		dwFeatureCreateOperationEClass = createEClass(DW_FEATURE_CREATE_OPERATION);
		createEReference(dwFeatureCreateOperationEClass, DW_FEATURE_CREATE_OPERATION__FEATURE_ADD_OPERATION);

		dwFeatureAddOperationEClass = createEClass(DW_FEATURE_ADD_OPERATION);
		createEReference(dwFeatureAddOperationEClass, DW_FEATURE_ADD_OPERATION__PARENT);
		createEAttribute(dwFeatureAddOperationEClass, DW_FEATURE_ADD_OPERATION__FORCE_CREATE_PARENT_GROUP);
		createEReference(dwFeatureAddOperationEClass, DW_FEATURE_ADD_OPERATION__OLD_GROUP_COMPOSITION);
		createEReference(dwFeatureAddOperationEClass, DW_FEATURE_ADD_OPERATION__NEW_GROUP_COMPOSITION);
		createEReference(dwFeatureAddOperationEClass, DW_FEATURE_ADD_OPERATION__GROUP_CREATE_OPERATION);
		createEReference(dwFeatureAddOperationEClass, DW_FEATURE_ADD_OPERATION__GROUP_TYPE_CHANGE_OPERATION);
		createEReference(dwFeatureAddOperationEClass, DW_FEATURE_ADD_OPERATION__GROUP_TO_ADD_TO);
		createEReference(dwFeatureAddOperationEClass, DW_FEATURE_ADD_OPERATION__CONSEQUENT_FEATURE_TYPE_CHANGE_OPERATION);

		dwFeatureDeleteOperationEClass = createEClass(DW_FEATURE_DELETE_OPERATION);
		createEAttribute(dwFeatureDeleteOperationEClass, DW_FEATURE_DELETE_OPERATION__OLD_FEATURE_VALID_UNTIL);
		createEReference(dwFeatureDeleteOperationEClass, DW_FEATURE_DELETE_OPERATION__DETACH_OPERATION);
		createEReference(dwFeatureDeleteOperationEClass, DW_FEATURE_DELETE_OPERATION__CONSEQUENT_GROUP_OPERATIONS);

		dwFeatureDetachOperationEClass = createEClass(DW_FEATURE_DETACH_OPERATION);
		createEReference(dwFeatureDetachOperationEClass, DW_FEATURE_DETACH_OPERATION__OLD_GROUP_MEMBERSHIP);
		createEReference(dwFeatureDetachOperationEClass, DW_FEATURE_DETACH_OPERATION__NEW_GROUP_MEMBERSHIP);
		createEAttribute(dwFeatureDetachOperationEClass, DW_FEATURE_DETACH_OPERATION__OLD_VALID_UNTIL);
		createEReference(dwFeatureDetachOperationEClass, DW_FEATURE_DETACH_OPERATION__CONSEQUENT_GROUP_OPERATION);

		dwFeatureMoveOperationEClass = createEClass(DW_FEATURE_MOVE_OPERATION);
		createEReference(dwFeatureMoveOperationEClass, DW_FEATURE_MOVE_OPERATION__DETACH_OPERATION);
		createEReference(dwFeatureMoveOperationEClass, DW_FEATURE_MOVE_OPERATION__ADD_OPERATION);

		dwFeatureRenameOperationEClass = createEClass(DW_FEATURE_RENAME_OPERATION);
		createEReference(dwFeatureRenameOperationEClass, DW_FEATURE_RENAME_OPERATION__NEW_NAME);
		createEAttribute(dwFeatureRenameOperationEClass, DW_FEATURE_RENAME_OPERATION__NEW_NAME_STRING);
		createEReference(dwFeatureRenameOperationEClass, DW_FEATURE_RENAME_OPERATION__OLD_NAME);
		createEAttribute(dwFeatureRenameOperationEClass, DW_FEATURE_RENAME_OPERATION__OLD_NAME_STRING);

		dwFeatureTypeChangeOperationEClass = createEClass(DW_FEATURE_TYPE_CHANGE_OPERATION);
		createEReference(dwFeatureTypeChangeOperationEClass, DW_FEATURE_TYPE_CHANGE_OPERATION__NEW_FEATURE_TYPE);
		createEAttribute(dwFeatureTypeChangeOperationEClass, DW_FEATURE_TYPE_CHANGE_OPERATION__NEW_FEATURE_TYPE_ENUM);
		createEReference(dwFeatureTypeChangeOperationEClass, DW_FEATURE_TYPE_CHANGE_OPERATION__OLD_FEATURE_TYPE);

		dwFeatureAttributeOperationEClass = createEClass(DW_FEATURE_ATTRIBUTE_OPERATION);
		createEReference(dwFeatureAttributeOperationEClass, DW_FEATURE_ATTRIBUTE_OPERATION__ATTRIBUTE);

		dwFeatureAttributeCreateOperationEClass = createEClass(DW_FEATURE_ATTRIBUTE_CREATE_OPERATION);

		dwFeatureAttributeDeleteOperationEClass = createEClass(DW_FEATURE_ATTRIBUTE_DELETE_OPERATION);

		dwFeatureAttributeRenameOperationEClass = createEClass(DW_FEATURE_ATTRIBUTE_RENAME_OPERATION);
		createEReference(dwFeatureAttributeRenameOperationEClass, DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__OLD_NAME);
		createEReference(dwFeatureAttributeRenameOperationEClass, DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__NEW_NAME);

		dwFeatureVersionOperationEClass = createEClass(DW_FEATURE_VERSION_OPERATION);
		createEReference(dwFeatureVersionOperationEClass, DW_FEATURE_VERSION_OPERATION__VERSION);

		dwFeatureVersionCreateOperationEClass = createEClass(DW_FEATURE_VERSION_CREATE_OPERATION);
		createEReference(dwFeatureVersionCreateOperationEClass, DW_FEATURE_VERSION_CREATE_OPERATION__PARENT);
		createEAttribute(dwFeatureVersionCreateOperationEClass, DW_FEATURE_VERSION_CREATE_OPERATION__USE_SAME_BRANCH);
		createEAttribute(dwFeatureVersionCreateOperationEClass, DW_FEATURE_VERSION_CREATE_OPERATION__WIRE_AND_ADD_AFTER);

		dwFeatureVersionDeleteOperationEClass = createEClass(DW_FEATURE_VERSION_DELETE_OPERATION);
		createEReference(dwFeatureVersionDeleteOperationEClass, DW_FEATURE_VERSION_DELETE_OPERATION__PARENT);
		createEAttribute(dwFeatureVersionDeleteOperationEClass, DW_FEATURE_VERSION_DELETE_OPERATION__OLD_VALID_UNTIL);

		dwFeatureVersionRenameOperationEClass = createEClass(DW_FEATURE_VERSION_RENAME_OPERATION);
		createEAttribute(dwFeatureVersionRenameOperationEClass, DW_FEATURE_VERSION_RENAME_OPERATION__OLD_NUMBER);
		createEAttribute(dwFeatureVersionRenameOperationEClass, DW_FEATURE_VERSION_RENAME_OPERATION__NEW_NUMBER);

		dwGroupOperationEClass = createEClass(DW_GROUP_OPERATION);
		createEReference(dwGroupOperationEClass, DW_GROUP_OPERATION__GROUP);

		dwGroupCreateOperationEClass = createEClass(DW_GROUP_CREATE_OPERATION);
		createEReference(dwGroupCreateOperationEClass, DW_GROUP_CREATE_OPERATION__GROUP_ADD_OPERATION);

		dwGroupAddOperationEClass = createEClass(DW_GROUP_ADD_OPERATION);
		createEReference(dwGroupAddOperationEClass, DW_GROUP_ADD_OPERATION__PARENT_FEATURE);
		createEReference(dwGroupAddOperationEClass, DW_GROUP_ADD_OPERATION__NEW_CONTAINER);

		dwGroupDeleteOperationEClass = createEClass(DW_GROUP_DELETE_OPERATION);
		createEAttribute(dwGroupDeleteOperationEClass, DW_GROUP_DELETE_OPERATION__OLD_GROUP_VALID_UNTIL);
		createEReference(dwGroupDeleteOperationEClass, DW_GROUP_DELETE_OPERATION__DETACH_OPERATION);
		createEReference(dwGroupDeleteOperationEClass, DW_GROUP_DELETE_OPERATION__CONSEQUENT_FEATURE_OPERATIONS);

		dwGroupDetachOperationEClass = createEClass(DW_GROUP_DETACH_OPERATION);
		createEReference(dwGroupDetachOperationEClass, DW_GROUP_DETACH_OPERATION__OLD_CONTAINER);
		createEReference(dwGroupDetachOperationEClass, DW_GROUP_DETACH_OPERATION__OLD_PARENT);

		dwGroupMoveOperationEClass = createEClass(DW_GROUP_MOVE_OPERATION);
		createEReference(dwGroupMoveOperationEClass, DW_GROUP_MOVE_OPERATION__GROUP_DETACH_OPERATION);
		createEReference(dwGroupMoveOperationEClass, DW_GROUP_MOVE_OPERATION__GROUP_ADD_OPERATION);

		dwGroupTypeChangeOperationEClass = createEClass(DW_GROUP_TYPE_CHANGE_OPERATION);
		createEReference(dwGroupTypeChangeOperationEClass, DW_GROUP_TYPE_CHANGE_OPERATION__CONSEQUENT_OPERATIONS);
		createEReference(dwGroupTypeChangeOperationEClass, DW_GROUP_TYPE_CHANGE_OPERATION__NEW_GROUP_TYPE);
		createEAttribute(dwGroupTypeChangeOperationEClass, DW_GROUP_TYPE_CHANGE_OPERATION__NEW_GROUP_TYPE_ENUM);
		createEReference(dwGroupTypeChangeOperationEClass, DW_GROUP_TYPE_CHANGE_OPERATION__OLD_GROUP_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		DwContributorPackage theDwContributorPackage = (DwContributorPackage)EPackage.Registry.INSTANCE.getEPackage(DwContributorPackage.eNS_URI);
		DwTemporalModelPackage theDwTemporalModelPackage = (DwTemporalModelPackage)EPackage.Registry.INSTANCE.getEPackage(DwTemporalModelPackage.eNS_URI);
		DwFeaturePackage theDwFeaturePackage = (DwFeaturePackage)EPackage.Registry.INSTANCE.getEPackage(DwFeaturePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		dwEvolutionOperationEClass.getESuperTypes().add(theDwTemporalModelPackage.getDwElementWithId());
		dwFeatureOperationEClass.getESuperTypes().add(this.getDwEvolutionOperation());
		dwFeatureCreateOperationEClass.getESuperTypes().add(this.getDwFeatureOperation());
		dwFeatureAddOperationEClass.getESuperTypes().add(this.getDwFeatureOperation());
		dwFeatureDeleteOperationEClass.getESuperTypes().add(this.getDwFeatureOperation());
		dwFeatureDetachOperationEClass.getESuperTypes().add(this.getDwFeatureOperation());
		dwFeatureMoveOperationEClass.getESuperTypes().add(this.getDwFeatureOperation());
		dwFeatureRenameOperationEClass.getESuperTypes().add(this.getDwFeatureOperation());
		dwFeatureTypeChangeOperationEClass.getESuperTypes().add(this.getDwFeatureOperation());
		dwFeatureAttributeOperationEClass.getESuperTypes().add(this.getDwFeatureOperation());
		dwFeatureAttributeCreateOperationEClass.getESuperTypes().add(this.getDwFeatureAttributeOperation());
		dwFeatureAttributeDeleteOperationEClass.getESuperTypes().add(this.getDwFeatureAttributeOperation());
		dwFeatureAttributeRenameOperationEClass.getESuperTypes().add(this.getDwFeatureAttributeOperation());
		dwFeatureVersionOperationEClass.getESuperTypes().add(this.getDwFeatureOperation());
		dwFeatureVersionCreateOperationEClass.getESuperTypes().add(this.getDwFeatureVersionOperation());
		dwFeatureVersionDeleteOperationEClass.getESuperTypes().add(this.getDwFeatureVersionOperation());
		dwFeatureVersionRenameOperationEClass.getESuperTypes().add(this.getDwFeatureVersionOperation());
		dwGroupOperationEClass.getESuperTypes().add(this.getDwEvolutionOperation());
		dwGroupCreateOperationEClass.getESuperTypes().add(this.getDwGroupOperation());
		dwGroupAddOperationEClass.getESuperTypes().add(this.getDwGroupOperation());
		dwGroupDeleteOperationEClass.getESuperTypes().add(this.getDwGroupOperation());
		dwGroupDetachOperationEClass.getESuperTypes().add(this.getDwGroupOperation());
		dwGroupMoveOperationEClass.getESuperTypes().add(this.getDwGroupOperation());
		dwGroupTypeChangeOperationEClass.getESuperTypes().add(this.getDwGroupOperation());

		// Initialize classes, features, and operations; add parameters
		initEClass(dwEvolutionOperationModelEClass, DwEvolutionOperationModel.class, "DwEvolutionOperationModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwEvolutionOperationModel_EvolutionOperations(), this.getDwEvolutionOperation(), null, "evolutionOperations", null, 0, -1, DwEvolutionOperationModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwEvolutionOperationModel_Contributors(), theDwContributorPackage.getDwContributor(), null, "contributors", null, 0, -1, DwEvolutionOperationModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwEvolutionOperationEClass, DwEvolutionOperation.class, "DwEvolutionOperation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDwEvolutionOperation_OperationDate(), ecorePackage.getEDate(), "operationDate", null, 1, 1, DwEvolutionOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDwEvolutionOperation_CommandStackIndex(), ecorePackage.getEInt(), "commandStackIndex", null, 1, 1, DwEvolutionOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwEvolutionOperation_FeatureModel(), theDwFeaturePackage.getDwTemporalFeatureModel(), null, "featureModel", null, 1, 1, DwEvolutionOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwEvolutionOperation_Contributor(), theDwContributorPackage.getDwContributor(), null, "contributor", null, 0, 1, DwEvolutionOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDwEvolutionOperation_IntermediateOperation(), ecorePackage.getEBoolean(), "intermediateOperation", null, 0, 1, DwEvolutionOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwFeatureOperationEClass, DwFeatureOperation.class, "DwFeatureOperation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwFeatureOperation_Feature(), theDwFeaturePackage.getDwFeature(), null, "feature", null, 1, 1, DwFeatureOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwFeatureCreateOperationEClass, DwFeatureCreateOperation.class, "DwFeatureCreateOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwFeatureCreateOperation_FeatureAddOperation(), this.getDwFeatureAddOperation(), null, "featureAddOperation", null, 0, 1, DwFeatureCreateOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwFeatureAddOperationEClass, DwFeatureAddOperation.class, "DwFeatureAddOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwFeatureAddOperation_Parent(), ecorePackage.getEObject(), null, "parent", null, 1, 1, DwFeatureAddOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDwFeatureAddOperation_ForceCreateParentGroup(), ecorePackage.getEBoolean(), "forceCreateParentGroup", null, 1, 1, DwFeatureAddOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwFeatureAddOperation_OldGroupComposition(), theDwFeaturePackage.getDwGroupComposition(), null, "oldGroupComposition", null, 0, 1, DwFeatureAddOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwFeatureAddOperation_NewGroupComposition(), theDwFeaturePackage.getDwGroupComposition(), null, "newGroupComposition", null, 0, 1, DwFeatureAddOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwFeatureAddOperation_GroupCreateOperation(), this.getDwGroupCreateOperation(), null, "groupCreateOperation", null, 0, 1, DwFeatureAddOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwFeatureAddOperation_GroupTypeChangeOperation(), this.getDwGroupTypeChangeOperation(), null, "groupTypeChangeOperation", null, 0, 1, DwFeatureAddOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwFeatureAddOperation_GroupToAddTo(), theDwFeaturePackage.getDwGroup(), null, "groupToAddTo", null, 1, 1, DwFeatureAddOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwFeatureAddOperation_ConsequentFeatureTypeChangeOperation(), this.getDwFeatureTypeChangeOperation(), null, "consequentFeatureTypeChangeOperation", null, 0, 1, DwFeatureAddOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwFeatureDeleteOperationEClass, DwFeatureDeleteOperation.class, "DwFeatureDeleteOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDwFeatureDeleteOperation_OldFeatureValidUntil(), ecorePackage.getEDate(), "oldFeatureValidUntil", null, 0, 1, DwFeatureDeleteOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwFeatureDeleteOperation_DetachOperation(), this.getDwFeatureDetachOperation(), null, "detachOperation", null, 1, 1, DwFeatureDeleteOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwFeatureDeleteOperation_ConsequentGroupOperations(), this.getDwGroupDeleteOperation(), null, "consequentGroupOperations", null, 0, -1, DwFeatureDeleteOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwFeatureDetachOperationEClass, DwFeatureDetachOperation.class, "DwFeatureDetachOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwFeatureDetachOperation_OldGroupMembership(), theDwFeaturePackage.getDwGroupComposition(), null, "oldGroupMembership", null, 0, 1, DwFeatureDetachOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwFeatureDetachOperation_NewGroupMembership(), theDwFeaturePackage.getDwGroupComposition(), null, "newGroupMembership", null, 0, 1, DwFeatureDetachOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDwFeatureDetachOperation_OldValidUntil(), ecorePackage.getEDate(), "oldValidUntil", null, 0, 1, DwFeatureDetachOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwFeatureDetachOperation_ConsequentGroupOperation(), this.getDwGroupDeleteOperation(), null, "consequentGroupOperation", null, 0, 1, DwFeatureDetachOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwFeatureMoveOperationEClass, DwFeatureMoveOperation.class, "DwFeatureMoveOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwFeatureMoveOperation_DetachOperation(), this.getDwFeatureDetachOperation(), null, "detachOperation", null, 1, 1, DwFeatureMoveOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwFeatureMoveOperation_AddOperation(), this.getDwFeatureAddOperation(), null, "addOperation", null, 1, 1, DwFeatureMoveOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwFeatureRenameOperationEClass, DwFeatureRenameOperation.class, "DwFeatureRenameOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwFeatureRenameOperation_NewName(), theDwFeaturePackage.getDwName(), null, "newName", null, 0, 1, DwFeatureRenameOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDwFeatureRenameOperation_NewNameString(), ecorePackage.getEString(), "newNameString", null, 1, 1, DwFeatureRenameOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwFeatureRenameOperation_OldName(), theDwFeaturePackage.getDwName(), null, "oldName", null, 0, 1, DwFeatureRenameOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDwFeatureRenameOperation_OldNameString(), ecorePackage.getEString(), "oldNameString", null, 1, 1, DwFeatureRenameOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwFeatureTypeChangeOperationEClass, DwFeatureTypeChangeOperation.class, "DwFeatureTypeChangeOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwFeatureTypeChangeOperation_NewFeatureType(), theDwFeaturePackage.getDwFeatureType(), null, "newFeatureType", null, 0, 1, DwFeatureTypeChangeOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDwFeatureTypeChangeOperation_NewFeatureTypeEnum(), theDwFeaturePackage.getDwFeatureTypeEnum(), "newFeatureTypeEnum", null, 1, 1, DwFeatureTypeChangeOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwFeatureTypeChangeOperation_OldFeatureType(), theDwFeaturePackage.getDwFeatureType(), null, "oldFeatureType", null, 0, 1, DwFeatureTypeChangeOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwFeatureAttributeOperationEClass, DwFeatureAttributeOperation.class, "DwFeatureAttributeOperation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwFeatureAttributeOperation_Attribute(), theDwFeaturePackage.getDwFeatureAttribute(), null, "attribute", null, 1, 1, DwFeatureAttributeOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwFeatureAttributeCreateOperationEClass, DwFeatureAttributeCreateOperation.class, "DwFeatureAttributeCreateOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dwFeatureAttributeDeleteOperationEClass, DwFeatureAttributeDeleteOperation.class, "DwFeatureAttributeDeleteOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dwFeatureAttributeRenameOperationEClass, DwFeatureAttributeRenameOperation.class, "DwFeatureAttributeRenameOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwFeatureAttributeRenameOperation_OldName(), theDwFeaturePackage.getDwName(), null, "oldName", null, 1, 1, DwFeatureAttributeRenameOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwFeatureAttributeRenameOperation_NewName(), theDwFeaturePackage.getDwName(), null, "newName", null, 1, 1, DwFeatureAttributeRenameOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwFeatureVersionOperationEClass, DwFeatureVersionOperation.class, "DwFeatureVersionOperation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwFeatureVersionOperation_Version(), theDwFeaturePackage.getDwFeatureVersion(), null, "version", null, 1, 1, DwFeatureVersionOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwFeatureVersionCreateOperationEClass, DwFeatureVersionCreateOperation.class, "DwFeatureVersionCreateOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwFeatureVersionCreateOperation_Parent(), ecorePackage.getEObject(), null, "parent", null, 1, 1, DwFeatureVersionCreateOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDwFeatureVersionCreateOperation_UseSameBranch(), ecorePackage.getEBoolean(), "useSameBranch", null, 1, 1, DwFeatureVersionCreateOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDwFeatureVersionCreateOperation_WireAndAddAfter(), ecorePackage.getEBoolean(), "wireAndAddAfter", null, 1, 1, DwFeatureVersionCreateOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwFeatureVersionDeleteOperationEClass, DwFeatureVersionDeleteOperation.class, "DwFeatureVersionDeleteOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwFeatureVersionDeleteOperation_Parent(), ecorePackage.getEObject(), null, "parent", null, 1, 1, DwFeatureVersionDeleteOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDwFeatureVersionDeleteOperation_OldValidUntil(), ecorePackage.getEDate(), "oldValidUntil", null, 1, 1, DwFeatureVersionDeleteOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwFeatureVersionRenameOperationEClass, DwFeatureVersionRenameOperation.class, "DwFeatureVersionRenameOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDwFeatureVersionRenameOperation_OldNumber(), ecorePackage.getEString(), "oldNumber", null, 1, 1, DwFeatureVersionRenameOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDwFeatureVersionRenameOperation_NewNumber(), ecorePackage.getEString(), "newNumber", null, 1, 1, DwFeatureVersionRenameOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwGroupOperationEClass, DwGroupOperation.class, "DwGroupOperation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwGroupOperation_Group(), theDwFeaturePackage.getDwGroup(), null, "group", null, 1, 1, DwGroupOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwGroupCreateOperationEClass, DwGroupCreateOperation.class, "DwGroupCreateOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwGroupCreateOperation_GroupAddOperation(), this.getDwGroupAddOperation(), null, "groupAddOperation", null, 1, 1, DwGroupCreateOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwGroupAddOperationEClass, DwGroupAddOperation.class, "DwGroupAddOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwGroupAddOperation_ParentFeature(), theDwFeaturePackage.getDwFeature(), null, "parentFeature", null, 1, 1, DwGroupAddOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwGroupAddOperation_NewContainer(), theDwFeaturePackage.getDwParentFeatureChildGroupContainer(), null, "newContainer", null, 0, 1, DwGroupAddOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwGroupDeleteOperationEClass, DwGroupDeleteOperation.class, "DwGroupDeleteOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDwGroupDeleteOperation_OldGroupValidUntil(), ecorePackage.getEDate(), "oldGroupValidUntil", null, 0, 1, DwGroupDeleteOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwGroupDeleteOperation_DetachOperation(), this.getDwGroupDetachOperation(), null, "detachOperation", null, 1, 1, DwGroupDeleteOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwGroupDeleteOperation_ConsequentFeatureOperations(), this.getDwFeatureDeleteOperation(), null, "consequentFeatureOperations", null, 0, -1, DwGroupDeleteOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwGroupDetachOperationEClass, DwGroupDetachOperation.class, "DwGroupDetachOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwGroupDetachOperation_OldContainer(), theDwFeaturePackage.getDwParentFeatureChildGroupContainer(), null, "oldContainer", null, 0, 1, DwGroupDetachOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwGroupDetachOperation_OldParent(), theDwFeaturePackage.getDwFeature(), null, "oldParent", null, 0, 1, DwGroupDetachOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwGroupMoveOperationEClass, DwGroupMoveOperation.class, "DwGroupMoveOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwGroupMoveOperation_GroupDetachOperation(), this.getDwGroupDetachOperation(), null, "groupDetachOperation", null, 1, 1, DwGroupMoveOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwGroupMoveOperation_GroupAddOperation(), this.getDwGroupAddOperation(), null, "groupAddOperation", null, 1, 1, DwGroupMoveOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwGroupTypeChangeOperationEClass, DwGroupTypeChangeOperation.class, "DwGroupTypeChangeOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwGroupTypeChangeOperation_ConsequentOperations(), this.getDwFeatureTypeChangeOperation(), null, "consequentOperations", null, 0, -1, DwGroupTypeChangeOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwGroupTypeChangeOperation_NewGroupType(), theDwFeaturePackage.getDwGroupType(), null, "newGroupType", null, 0, 1, DwGroupTypeChangeOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDwGroupTypeChangeOperation_NewGroupTypeEnum(), theDwFeaturePackage.getDwGroupTypeEnum(), "newGroupTypeEnum", null, 1, 1, DwGroupTypeChangeOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwGroupTypeChangeOperation_OldGroupType(), theDwFeaturePackage.getDwGroupType(), null, "oldGroupType", null, 0, 1, DwGroupTypeChangeOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //DwOperationPackageImpl
