/**
 */
package de.darwinspl.feature.operation.impl;

import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupComposition;

import de.darwinspl.feature.operation.DwFeatureAddOperation;
import de.darwinspl.feature.operation.DwFeatureTypeChangeOperation;
import de.darwinspl.feature.operation.DwGroupCreateOperation;
import de.darwinspl.feature.operation.DwGroupTypeChangeOperation;
import de.darwinspl.feature.operation.DwOperationPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Feature Add Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureAddOperationImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureAddOperationImpl#isForceCreateParentGroup <em>Force Create Parent Group</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureAddOperationImpl#getOldGroupComposition <em>Old Group Composition</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureAddOperationImpl#getNewGroupComposition <em>New Group Composition</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureAddOperationImpl#getGroupCreateOperation <em>Group Create Operation</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureAddOperationImpl#getGroupTypeChangeOperation <em>Group Type Change Operation</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureAddOperationImpl#getGroupToAddTo <em>Group To Add To</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureAddOperationImpl#getConsequentFeatureTypeChangeOperation <em>Consequent Feature Type Change Operation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwFeatureAddOperationImpl extends DwFeatureOperationImpl implements DwFeatureAddOperation {
	/**
	 * The cached value of the '{@link #getParent() <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParent()
	 * @generated
	 * @ordered
	 */
	protected EObject parent;

	/**
	 * The default value of the '{@link #isForceCreateParentGroup() <em>Force Create Parent Group</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isForceCreateParentGroup()
	 * @generated
	 * @ordered
	 */
	protected static final boolean FORCE_CREATE_PARENT_GROUP_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isForceCreateParentGroup() <em>Force Create Parent Group</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isForceCreateParentGroup()
	 * @generated
	 * @ordered
	 */
	protected boolean forceCreateParentGroup = FORCE_CREATE_PARENT_GROUP_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOldGroupComposition() <em>Old Group Composition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOldGroupComposition()
	 * @generated
	 * @ordered
	 */
	protected DwGroupComposition oldGroupComposition;

	/**
	 * The cached value of the '{@link #getNewGroupComposition() <em>New Group Composition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNewGroupComposition()
	 * @generated
	 * @ordered
	 */
	protected DwGroupComposition newGroupComposition;

	/**
	 * The cached value of the '{@link #getGroupCreateOperation() <em>Group Create Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroupCreateOperation()
	 * @generated
	 * @ordered
	 */
	protected DwGroupCreateOperation groupCreateOperation;

	/**
	 * The cached value of the '{@link #getGroupTypeChangeOperation() <em>Group Type Change Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroupTypeChangeOperation()
	 * @generated
	 * @ordered
	 */
	protected DwGroupTypeChangeOperation groupTypeChangeOperation;

	/**
	 * The cached value of the '{@link #getGroupToAddTo() <em>Group To Add To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroupToAddTo()
	 * @generated
	 * @ordered
	 */
	protected DwGroup groupToAddTo;

	/**
	 * The cached value of the '{@link #getConsequentFeatureTypeChangeOperation() <em>Consequent Feature Type Change Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConsequentFeatureTypeChangeOperation()
	 * @generated
	 * @ordered
	 */
	protected DwFeatureTypeChangeOperation consequentFeatureTypeChangeOperation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwFeatureAddOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwOperationPackage.Literals.DW_FEATURE_ADD_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject getParent() {
		if (parent != null && parent.eIsProxy()) {
			InternalEObject oldParent = (InternalEObject)parent;
			parent = eResolveProxy(oldParent);
			if (parent != oldParent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwOperationPackage.DW_FEATURE_ADD_OPERATION__PARENT, oldParent, parent));
			}
		}
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetParent() {
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setParent(EObject newParent) {
		EObject oldParent = parent;
		parent = newParent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_ADD_OPERATION__PARENT, oldParent, parent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isForceCreateParentGroup() {
		return forceCreateParentGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setForceCreateParentGroup(boolean newForceCreateParentGroup) {
		boolean oldForceCreateParentGroup = forceCreateParentGroup;
		forceCreateParentGroup = newForceCreateParentGroup;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_ADD_OPERATION__FORCE_CREATE_PARENT_GROUP, oldForceCreateParentGroup, forceCreateParentGroup));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroupComposition getOldGroupComposition() {
		if (oldGroupComposition != null && oldGroupComposition.eIsProxy()) {
			InternalEObject oldOldGroupComposition = (InternalEObject)oldGroupComposition;
			oldGroupComposition = (DwGroupComposition)eResolveProxy(oldOldGroupComposition);
			if (oldGroupComposition != oldOldGroupComposition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwOperationPackage.DW_FEATURE_ADD_OPERATION__OLD_GROUP_COMPOSITION, oldOldGroupComposition, oldGroupComposition));
			}
		}
		return oldGroupComposition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwGroupComposition basicGetOldGroupComposition() {
		return oldGroupComposition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOldGroupComposition(DwGroupComposition newOldGroupComposition) {
		DwGroupComposition oldOldGroupComposition = oldGroupComposition;
		oldGroupComposition = newOldGroupComposition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_ADD_OPERATION__OLD_GROUP_COMPOSITION, oldOldGroupComposition, oldGroupComposition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroupComposition getNewGroupComposition() {
		if (newGroupComposition != null && newGroupComposition.eIsProxy()) {
			InternalEObject oldNewGroupComposition = (InternalEObject)newGroupComposition;
			newGroupComposition = (DwGroupComposition)eResolveProxy(oldNewGroupComposition);
			if (newGroupComposition != oldNewGroupComposition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwOperationPackage.DW_FEATURE_ADD_OPERATION__NEW_GROUP_COMPOSITION, oldNewGroupComposition, newGroupComposition));
			}
		}
		return newGroupComposition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwGroupComposition basicGetNewGroupComposition() {
		return newGroupComposition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNewGroupComposition(DwGroupComposition newNewGroupComposition) {
		DwGroupComposition oldNewGroupComposition = newGroupComposition;
		newGroupComposition = newNewGroupComposition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_ADD_OPERATION__NEW_GROUP_COMPOSITION, oldNewGroupComposition, newGroupComposition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroupCreateOperation getGroupCreateOperation() {
		return groupCreateOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGroupCreateOperation(DwGroupCreateOperation newGroupCreateOperation, NotificationChain msgs) {
		DwGroupCreateOperation oldGroupCreateOperation = groupCreateOperation;
		groupCreateOperation = newGroupCreateOperation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_ADD_OPERATION__GROUP_CREATE_OPERATION, oldGroupCreateOperation, newGroupCreateOperation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setGroupCreateOperation(DwGroupCreateOperation newGroupCreateOperation) {
		if (newGroupCreateOperation != groupCreateOperation) {
			NotificationChain msgs = null;
			if (groupCreateOperation != null)
				msgs = ((InternalEObject)groupCreateOperation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DwOperationPackage.DW_FEATURE_ADD_OPERATION__GROUP_CREATE_OPERATION, null, msgs);
			if (newGroupCreateOperation != null)
				msgs = ((InternalEObject)newGroupCreateOperation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DwOperationPackage.DW_FEATURE_ADD_OPERATION__GROUP_CREATE_OPERATION, null, msgs);
			msgs = basicSetGroupCreateOperation(newGroupCreateOperation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_ADD_OPERATION__GROUP_CREATE_OPERATION, newGroupCreateOperation, newGroupCreateOperation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroupTypeChangeOperation getGroupTypeChangeOperation() {
		return groupTypeChangeOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGroupTypeChangeOperation(DwGroupTypeChangeOperation newGroupTypeChangeOperation, NotificationChain msgs) {
		DwGroupTypeChangeOperation oldGroupTypeChangeOperation = groupTypeChangeOperation;
		groupTypeChangeOperation = newGroupTypeChangeOperation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_ADD_OPERATION__GROUP_TYPE_CHANGE_OPERATION, oldGroupTypeChangeOperation, newGroupTypeChangeOperation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setGroupTypeChangeOperation(DwGroupTypeChangeOperation newGroupTypeChangeOperation) {
		if (newGroupTypeChangeOperation != groupTypeChangeOperation) {
			NotificationChain msgs = null;
			if (groupTypeChangeOperation != null)
				msgs = ((InternalEObject)groupTypeChangeOperation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DwOperationPackage.DW_FEATURE_ADD_OPERATION__GROUP_TYPE_CHANGE_OPERATION, null, msgs);
			if (newGroupTypeChangeOperation != null)
				msgs = ((InternalEObject)newGroupTypeChangeOperation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DwOperationPackage.DW_FEATURE_ADD_OPERATION__GROUP_TYPE_CHANGE_OPERATION, null, msgs);
			msgs = basicSetGroupTypeChangeOperation(newGroupTypeChangeOperation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_ADD_OPERATION__GROUP_TYPE_CHANGE_OPERATION, newGroupTypeChangeOperation, newGroupTypeChangeOperation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroup getGroupToAddTo() {
		if (groupToAddTo != null && groupToAddTo.eIsProxy()) {
			InternalEObject oldGroupToAddTo = (InternalEObject)groupToAddTo;
			groupToAddTo = (DwGroup)eResolveProxy(oldGroupToAddTo);
			if (groupToAddTo != oldGroupToAddTo) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwOperationPackage.DW_FEATURE_ADD_OPERATION__GROUP_TO_ADD_TO, oldGroupToAddTo, groupToAddTo));
			}
		}
		return groupToAddTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwGroup basicGetGroupToAddTo() {
		return groupToAddTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setGroupToAddTo(DwGroup newGroupToAddTo) {
		DwGroup oldGroupToAddTo = groupToAddTo;
		groupToAddTo = newGroupToAddTo;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_ADD_OPERATION__GROUP_TO_ADD_TO, oldGroupToAddTo, groupToAddTo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureTypeChangeOperation getConsequentFeatureTypeChangeOperation() {
		return consequentFeatureTypeChangeOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConsequentFeatureTypeChangeOperation(DwFeatureTypeChangeOperation newConsequentFeatureTypeChangeOperation, NotificationChain msgs) {
		DwFeatureTypeChangeOperation oldConsequentFeatureTypeChangeOperation = consequentFeatureTypeChangeOperation;
		consequentFeatureTypeChangeOperation = newConsequentFeatureTypeChangeOperation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_ADD_OPERATION__CONSEQUENT_FEATURE_TYPE_CHANGE_OPERATION, oldConsequentFeatureTypeChangeOperation, newConsequentFeatureTypeChangeOperation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setConsequentFeatureTypeChangeOperation(DwFeatureTypeChangeOperation newConsequentFeatureTypeChangeOperation) {
		if (newConsequentFeatureTypeChangeOperation != consequentFeatureTypeChangeOperation) {
			NotificationChain msgs = null;
			if (consequentFeatureTypeChangeOperation != null)
				msgs = ((InternalEObject)consequentFeatureTypeChangeOperation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DwOperationPackage.DW_FEATURE_ADD_OPERATION__CONSEQUENT_FEATURE_TYPE_CHANGE_OPERATION, null, msgs);
			if (newConsequentFeatureTypeChangeOperation != null)
				msgs = ((InternalEObject)newConsequentFeatureTypeChangeOperation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DwOperationPackage.DW_FEATURE_ADD_OPERATION__CONSEQUENT_FEATURE_TYPE_CHANGE_OPERATION, null, msgs);
			msgs = basicSetConsequentFeatureTypeChangeOperation(newConsequentFeatureTypeChangeOperation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_ADD_OPERATION__CONSEQUENT_FEATURE_TYPE_CHANGE_OPERATION, newConsequentFeatureTypeChangeOperation, newConsequentFeatureTypeChangeOperation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__GROUP_CREATE_OPERATION:
				return basicSetGroupCreateOperation(null, msgs);
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__GROUP_TYPE_CHANGE_OPERATION:
				return basicSetGroupTypeChangeOperation(null, msgs);
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__CONSEQUENT_FEATURE_TYPE_CHANGE_OPERATION:
				return basicSetConsequentFeatureTypeChangeOperation(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__PARENT:
				if (resolve) return getParent();
				return basicGetParent();
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__FORCE_CREATE_PARENT_GROUP:
				return isForceCreateParentGroup();
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__OLD_GROUP_COMPOSITION:
				if (resolve) return getOldGroupComposition();
				return basicGetOldGroupComposition();
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__NEW_GROUP_COMPOSITION:
				if (resolve) return getNewGroupComposition();
				return basicGetNewGroupComposition();
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__GROUP_CREATE_OPERATION:
				return getGroupCreateOperation();
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__GROUP_TYPE_CHANGE_OPERATION:
				return getGroupTypeChangeOperation();
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__GROUP_TO_ADD_TO:
				if (resolve) return getGroupToAddTo();
				return basicGetGroupToAddTo();
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__CONSEQUENT_FEATURE_TYPE_CHANGE_OPERATION:
				return getConsequentFeatureTypeChangeOperation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__PARENT:
				setParent((EObject)newValue);
				return;
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__FORCE_CREATE_PARENT_GROUP:
				setForceCreateParentGroup((Boolean)newValue);
				return;
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__OLD_GROUP_COMPOSITION:
				setOldGroupComposition((DwGroupComposition)newValue);
				return;
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__NEW_GROUP_COMPOSITION:
				setNewGroupComposition((DwGroupComposition)newValue);
				return;
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__GROUP_CREATE_OPERATION:
				setGroupCreateOperation((DwGroupCreateOperation)newValue);
				return;
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__GROUP_TYPE_CHANGE_OPERATION:
				setGroupTypeChangeOperation((DwGroupTypeChangeOperation)newValue);
				return;
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__GROUP_TO_ADD_TO:
				setGroupToAddTo((DwGroup)newValue);
				return;
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__CONSEQUENT_FEATURE_TYPE_CHANGE_OPERATION:
				setConsequentFeatureTypeChangeOperation((DwFeatureTypeChangeOperation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__PARENT:
				setParent((EObject)null);
				return;
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__FORCE_CREATE_PARENT_GROUP:
				setForceCreateParentGroup(FORCE_CREATE_PARENT_GROUP_EDEFAULT);
				return;
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__OLD_GROUP_COMPOSITION:
				setOldGroupComposition((DwGroupComposition)null);
				return;
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__NEW_GROUP_COMPOSITION:
				setNewGroupComposition((DwGroupComposition)null);
				return;
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__GROUP_CREATE_OPERATION:
				setGroupCreateOperation((DwGroupCreateOperation)null);
				return;
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__GROUP_TYPE_CHANGE_OPERATION:
				setGroupTypeChangeOperation((DwGroupTypeChangeOperation)null);
				return;
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__GROUP_TO_ADD_TO:
				setGroupToAddTo((DwGroup)null);
				return;
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__CONSEQUENT_FEATURE_TYPE_CHANGE_OPERATION:
				setConsequentFeatureTypeChangeOperation((DwFeatureTypeChangeOperation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__PARENT:
				return parent != null;
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__FORCE_CREATE_PARENT_GROUP:
				return forceCreateParentGroup != FORCE_CREATE_PARENT_GROUP_EDEFAULT;
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__OLD_GROUP_COMPOSITION:
				return oldGroupComposition != null;
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__NEW_GROUP_COMPOSITION:
				return newGroupComposition != null;
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__GROUP_CREATE_OPERATION:
				return groupCreateOperation != null;
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__GROUP_TYPE_CHANGE_OPERATION:
				return groupTypeChangeOperation != null;
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__GROUP_TO_ADD_TO:
				return groupToAddTo != null;
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION__CONSEQUENT_FEATURE_TYPE_CHANGE_OPERATION:
				return consequentFeatureTypeChangeOperation != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (forceCreateParentGroup: ");
		result.append(forceCreateParentGroup);
		result.append(')');
		return result.toString();
	}

} //DwFeatureAddOperationImpl
