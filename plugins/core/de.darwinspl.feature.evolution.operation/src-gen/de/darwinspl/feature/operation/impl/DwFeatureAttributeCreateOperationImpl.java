/**
 */
package de.darwinspl.feature.operation.impl;

import de.darwinspl.feature.operation.DwFeatureAttributeCreateOperation;
import de.darwinspl.feature.operation.DwOperationPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Feature Attribute Create Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DwFeatureAttributeCreateOperationImpl extends DwFeatureAttributeOperationImpl implements DwFeatureAttributeCreateOperation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwFeatureAttributeCreateOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwOperationPackage.Literals.DW_FEATURE_ATTRIBUTE_CREATE_OPERATION;
	}

} //DwFeatureAttributeCreateOperationImpl
