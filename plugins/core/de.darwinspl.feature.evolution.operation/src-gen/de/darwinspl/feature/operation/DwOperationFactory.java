/**
 */
package de.darwinspl.feature.operation;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.darwinspl.feature.operation.DwOperationPackage
 * @generated
 */
public interface DwOperationFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DwOperationFactory eINSTANCE = de.darwinspl.feature.operation.impl.DwOperationFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Dw Evolution Operation Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Evolution Operation Model</em>'.
	 * @generated
	 */
	DwEvolutionOperationModel createDwEvolutionOperationModel();

	/**
	 * Returns a new object of class '<em>Dw Feature Create Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Feature Create Operation</em>'.
	 * @generated
	 */
	DwFeatureCreateOperation createDwFeatureCreateOperation();

	/**
	 * Returns a new object of class '<em>Dw Feature Add Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Feature Add Operation</em>'.
	 * @generated
	 */
	DwFeatureAddOperation createDwFeatureAddOperation();

	/**
	 * Returns a new object of class '<em>Dw Feature Delete Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Feature Delete Operation</em>'.
	 * @generated
	 */
	DwFeatureDeleteOperation createDwFeatureDeleteOperation();

	/**
	 * Returns a new object of class '<em>Dw Feature Detach Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Feature Detach Operation</em>'.
	 * @generated
	 */
	DwFeatureDetachOperation createDwFeatureDetachOperation();

	/**
	 * Returns a new object of class '<em>Dw Feature Move Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Feature Move Operation</em>'.
	 * @generated
	 */
	DwFeatureMoveOperation createDwFeatureMoveOperation();

	/**
	 * Returns a new object of class '<em>Dw Feature Rename Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Feature Rename Operation</em>'.
	 * @generated
	 */
	DwFeatureRenameOperation createDwFeatureRenameOperation();

	/**
	 * Returns a new object of class '<em>Dw Feature Type Change Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Feature Type Change Operation</em>'.
	 * @generated
	 */
	DwFeatureTypeChangeOperation createDwFeatureTypeChangeOperation();

	/**
	 * Returns a new object of class '<em>Dw Feature Attribute Create Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Feature Attribute Create Operation</em>'.
	 * @generated
	 */
	DwFeatureAttributeCreateOperation createDwFeatureAttributeCreateOperation();

	/**
	 * Returns a new object of class '<em>Dw Feature Attribute Delete Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Feature Attribute Delete Operation</em>'.
	 * @generated
	 */
	DwFeatureAttributeDeleteOperation createDwFeatureAttributeDeleteOperation();

	/**
	 * Returns a new object of class '<em>Dw Feature Attribute Rename Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Feature Attribute Rename Operation</em>'.
	 * @generated
	 */
	DwFeatureAttributeRenameOperation createDwFeatureAttributeRenameOperation();

	/**
	 * Returns a new object of class '<em>Dw Feature Version Create Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Feature Version Create Operation</em>'.
	 * @generated
	 */
	DwFeatureVersionCreateOperation createDwFeatureVersionCreateOperation();

	/**
	 * Returns a new object of class '<em>Dw Feature Version Delete Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Feature Version Delete Operation</em>'.
	 * @generated
	 */
	DwFeatureVersionDeleteOperation createDwFeatureVersionDeleteOperation();

	/**
	 * Returns a new object of class '<em>Dw Feature Version Rename Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Feature Version Rename Operation</em>'.
	 * @generated
	 */
	DwFeatureVersionRenameOperation createDwFeatureVersionRenameOperation();

	/**
	 * Returns a new object of class '<em>Dw Group Create Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Group Create Operation</em>'.
	 * @generated
	 */
	DwGroupCreateOperation createDwGroupCreateOperation();

	/**
	 * Returns a new object of class '<em>Dw Group Add Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Group Add Operation</em>'.
	 * @generated
	 */
	DwGroupAddOperation createDwGroupAddOperation();

	/**
	 * Returns a new object of class '<em>Dw Group Delete Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Group Delete Operation</em>'.
	 * @generated
	 */
	DwGroupDeleteOperation createDwGroupDeleteOperation();

	/**
	 * Returns a new object of class '<em>Dw Group Detach Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Group Detach Operation</em>'.
	 * @generated
	 */
	DwGroupDetachOperation createDwGroupDetachOperation();

	/**
	 * Returns a new object of class '<em>Dw Group Move Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Group Move Operation</em>'.
	 * @generated
	 */
	DwGroupMoveOperation createDwGroupMoveOperation();

	/**
	 * Returns a new object of class '<em>Dw Group Type Change Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Group Type Change Operation</em>'.
	 * @generated
	 */
	DwGroupTypeChangeOperation createDwGroupTypeChangeOperation();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	DwOperationPackage getDwOperationPackage();

} //DwOperationFactory
