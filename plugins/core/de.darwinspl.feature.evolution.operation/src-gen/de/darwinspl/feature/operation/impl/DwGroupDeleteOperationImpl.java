/**
 */
package de.darwinspl.feature.operation.impl;

import de.darwinspl.feature.operation.DwFeatureDeleteOperation;
import de.darwinspl.feature.operation.DwGroupDeleteOperation;
import de.darwinspl.feature.operation.DwGroupDetachOperation;
import de.darwinspl.feature.operation.DwOperationPackage;

import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Group Delete Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwGroupDeleteOperationImpl#getOldGroupValidUntil <em>Old Group Valid Until</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwGroupDeleteOperationImpl#getDetachOperation <em>Detach Operation</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwGroupDeleteOperationImpl#getConsequentFeatureOperations <em>Consequent Feature Operations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwGroupDeleteOperationImpl extends DwGroupOperationImpl implements DwGroupDeleteOperation {
	/**
	 * The default value of the '{@link #getOldGroupValidUntil() <em>Old Group Valid Until</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOldGroupValidUntil()
	 * @generated
	 * @ordered
	 */
	protected static final Date OLD_GROUP_VALID_UNTIL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOldGroupValidUntil() <em>Old Group Valid Until</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOldGroupValidUntil()
	 * @generated
	 * @ordered
	 */
	protected Date oldGroupValidUntil = OLD_GROUP_VALID_UNTIL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDetachOperation() <em>Detach Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDetachOperation()
	 * @generated
	 * @ordered
	 */
	protected DwGroupDetachOperation detachOperation;

	/**
	 * The cached value of the '{@link #getConsequentFeatureOperations() <em>Consequent Feature Operations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConsequentFeatureOperations()
	 * @generated
	 * @ordered
	 */
	protected EList<DwFeatureDeleteOperation> consequentFeatureOperations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwGroupDeleteOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwOperationPackage.Literals.DW_GROUP_DELETE_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Date getOldGroupValidUntil() {
		return oldGroupValidUntil;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOldGroupValidUntil(Date newOldGroupValidUntil) {
		Date oldOldGroupValidUntil = oldGroupValidUntil;
		oldGroupValidUntil = newOldGroupValidUntil;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_GROUP_DELETE_OPERATION__OLD_GROUP_VALID_UNTIL, oldOldGroupValidUntil, oldGroupValidUntil));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwFeatureDeleteOperation> getConsequentFeatureOperations() {
		if (consequentFeatureOperations == null) {
			consequentFeatureOperations = new EObjectContainmentEList<DwFeatureDeleteOperation>(DwFeatureDeleteOperation.class, this, DwOperationPackage.DW_GROUP_DELETE_OPERATION__CONSEQUENT_FEATURE_OPERATIONS);
		}
		return consequentFeatureOperations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroupDetachOperation getDetachOperation() {
		return detachOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDetachOperation(DwGroupDetachOperation newDetachOperation, NotificationChain msgs) {
		DwGroupDetachOperation oldDetachOperation = detachOperation;
		detachOperation = newDetachOperation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_GROUP_DELETE_OPERATION__DETACH_OPERATION, oldDetachOperation, newDetachOperation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDetachOperation(DwGroupDetachOperation newDetachOperation) {
		if (newDetachOperation != detachOperation) {
			NotificationChain msgs = null;
			if (detachOperation != null)
				msgs = ((InternalEObject)detachOperation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DwOperationPackage.DW_GROUP_DELETE_OPERATION__DETACH_OPERATION, null, msgs);
			if (newDetachOperation != null)
				msgs = ((InternalEObject)newDetachOperation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DwOperationPackage.DW_GROUP_DELETE_OPERATION__DETACH_OPERATION, null, msgs);
			msgs = basicSetDetachOperation(newDetachOperation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_GROUP_DELETE_OPERATION__DETACH_OPERATION, newDetachOperation, newDetachOperation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwOperationPackage.DW_GROUP_DELETE_OPERATION__DETACH_OPERATION:
				return basicSetDetachOperation(null, msgs);
			case DwOperationPackage.DW_GROUP_DELETE_OPERATION__CONSEQUENT_FEATURE_OPERATIONS:
				return ((InternalEList<?>)getConsequentFeatureOperations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwOperationPackage.DW_GROUP_DELETE_OPERATION__OLD_GROUP_VALID_UNTIL:
				return getOldGroupValidUntil();
			case DwOperationPackage.DW_GROUP_DELETE_OPERATION__DETACH_OPERATION:
				return getDetachOperation();
			case DwOperationPackage.DW_GROUP_DELETE_OPERATION__CONSEQUENT_FEATURE_OPERATIONS:
				return getConsequentFeatureOperations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwOperationPackage.DW_GROUP_DELETE_OPERATION__OLD_GROUP_VALID_UNTIL:
				setOldGroupValidUntil((Date)newValue);
				return;
			case DwOperationPackage.DW_GROUP_DELETE_OPERATION__DETACH_OPERATION:
				setDetachOperation((DwGroupDetachOperation)newValue);
				return;
			case DwOperationPackage.DW_GROUP_DELETE_OPERATION__CONSEQUENT_FEATURE_OPERATIONS:
				getConsequentFeatureOperations().clear();
				getConsequentFeatureOperations().addAll((Collection<? extends DwFeatureDeleteOperation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_GROUP_DELETE_OPERATION__OLD_GROUP_VALID_UNTIL:
				setOldGroupValidUntil(OLD_GROUP_VALID_UNTIL_EDEFAULT);
				return;
			case DwOperationPackage.DW_GROUP_DELETE_OPERATION__DETACH_OPERATION:
				setDetachOperation((DwGroupDetachOperation)null);
				return;
			case DwOperationPackage.DW_GROUP_DELETE_OPERATION__CONSEQUENT_FEATURE_OPERATIONS:
				getConsequentFeatureOperations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_GROUP_DELETE_OPERATION__OLD_GROUP_VALID_UNTIL:
				return OLD_GROUP_VALID_UNTIL_EDEFAULT == null ? oldGroupValidUntil != null : !OLD_GROUP_VALID_UNTIL_EDEFAULT.equals(oldGroupValidUntil);
			case DwOperationPackage.DW_GROUP_DELETE_OPERATION__DETACH_OPERATION:
				return detachOperation != null;
			case DwOperationPackage.DW_GROUP_DELETE_OPERATION__CONSEQUENT_FEATURE_OPERATIONS:
				return consequentFeatureOperations != null && !consequentFeatureOperations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (oldGroupValidUntil: ");
		result.append(oldGroupValidUntil);
		result.append(')');
		return result.toString();
	}

} //DwGroupDeleteOperationImpl
