/**
 */
package de.darwinspl.feature.operation.impl;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwParentFeatureChildGroupContainer;

import de.darwinspl.feature.operation.DwGroupAddOperation;
import de.darwinspl.feature.operation.DwOperationPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Group Add Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwGroupAddOperationImpl#getParentFeature <em>Parent Feature</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwGroupAddOperationImpl#getNewContainer <em>New Container</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwGroupAddOperationImpl extends DwGroupOperationImpl implements DwGroupAddOperation {
	/**
	 * The cached value of the '{@link #getParentFeature() <em>Parent Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParentFeature()
	 * @generated
	 * @ordered
	 */
	protected DwFeature parentFeature;

	/**
	 * The cached value of the '{@link #getNewContainer() <em>New Container</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNewContainer()
	 * @generated
	 * @ordered
	 */
	protected DwParentFeatureChildGroupContainer newContainer;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwGroupAddOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwOperationPackage.Literals.DW_GROUP_ADD_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeature getParentFeature() {
		if (parentFeature != null && parentFeature.eIsProxy()) {
			InternalEObject oldParentFeature = (InternalEObject)parentFeature;
			parentFeature = (DwFeature)eResolveProxy(oldParentFeature);
			if (parentFeature != oldParentFeature) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwOperationPackage.DW_GROUP_ADD_OPERATION__PARENT_FEATURE, oldParentFeature, parentFeature));
			}
		}
		return parentFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwFeature basicGetParentFeature() {
		return parentFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setParentFeature(DwFeature newParentFeature) {
		DwFeature oldParentFeature = parentFeature;
		parentFeature = newParentFeature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_GROUP_ADD_OPERATION__PARENT_FEATURE, oldParentFeature, parentFeature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwParentFeatureChildGroupContainer getNewContainer() {
		if (newContainer != null && newContainer.eIsProxy()) {
			InternalEObject oldNewContainer = (InternalEObject)newContainer;
			newContainer = (DwParentFeatureChildGroupContainer)eResolveProxy(oldNewContainer);
			if (newContainer != oldNewContainer) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwOperationPackage.DW_GROUP_ADD_OPERATION__NEW_CONTAINER, oldNewContainer, newContainer));
			}
		}
		return newContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwParentFeatureChildGroupContainer basicGetNewContainer() {
		return newContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNewContainer(DwParentFeatureChildGroupContainer newNewContainer) {
		DwParentFeatureChildGroupContainer oldNewContainer = newContainer;
		newContainer = newNewContainer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_GROUP_ADD_OPERATION__NEW_CONTAINER, oldNewContainer, newContainer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwOperationPackage.DW_GROUP_ADD_OPERATION__PARENT_FEATURE:
				if (resolve) return getParentFeature();
				return basicGetParentFeature();
			case DwOperationPackage.DW_GROUP_ADD_OPERATION__NEW_CONTAINER:
				if (resolve) return getNewContainer();
				return basicGetNewContainer();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwOperationPackage.DW_GROUP_ADD_OPERATION__PARENT_FEATURE:
				setParentFeature((DwFeature)newValue);
				return;
			case DwOperationPackage.DW_GROUP_ADD_OPERATION__NEW_CONTAINER:
				setNewContainer((DwParentFeatureChildGroupContainer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_GROUP_ADD_OPERATION__PARENT_FEATURE:
				setParentFeature((DwFeature)null);
				return;
			case DwOperationPackage.DW_GROUP_ADD_OPERATION__NEW_CONTAINER:
				setNewContainer((DwParentFeatureChildGroupContainer)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_GROUP_ADD_OPERATION__PARENT_FEATURE:
				return parentFeature != null;
			case DwOperationPackage.DW_GROUP_ADD_OPERATION__NEW_CONTAINER:
				return newContainer != null;
		}
		return super.eIsSet(featureID);
	}

} //DwGroupAddOperationImpl
