/**
 */
package de.darwinspl.feature.operation;

import java.util.Date;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Feature Delete Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureDeleteOperation#getOldFeatureValidUntil <em>Old Feature Valid Until</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureDeleteOperation#getDetachOperation <em>Detach Operation</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureDeleteOperation#getConsequentGroupOperations <em>Consequent Group Operations</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureDeleteOperation()
 * @model
 * @generated
 */
public interface DwFeatureDeleteOperation extends DwFeatureOperation {
	/**
	 * Returns the value of the '<em><b>Old Feature Valid Until</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Old Feature Valid Until</em>' attribute.
	 * @see #setOldFeatureValidUntil(Date)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureDeleteOperation_OldFeatureValidUntil()
	 * @model
	 * @generated
	 */
	Date getOldFeatureValidUntil();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureDeleteOperation#getOldFeatureValidUntil <em>Old Feature Valid Until</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Old Feature Valid Until</em>' attribute.
	 * @see #getOldFeatureValidUntil()
	 * @generated
	 */
	void setOldFeatureValidUntil(Date value);

	/**
	 * Returns the value of the '<em><b>Consequent Group Operations</b></em>' containment reference list.
	 * The list contents are of type {@link de.darwinspl.feature.operation.DwGroupDeleteOperation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Consequent Group Operations</em>' containment reference list.
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureDeleteOperation_ConsequentGroupOperations()
	 * @model containment="true"
	 * @generated
	 */
	EList<DwGroupDeleteOperation> getConsequentGroupOperations();

	/**
	 * Returns the value of the '<em><b>Detach Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Detach Operation</em>' containment reference.
	 * @see #setDetachOperation(DwFeatureDetachOperation)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureDeleteOperation_DetachOperation()
	 * @model containment="true" required="true"
	 * @generated
	 */
	DwFeatureDetachOperation getDetachOperation();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureDeleteOperation#getDetachOperation <em>Detach Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Detach Operation</em>' containment reference.
	 * @see #getDetachOperation()
	 * @generated
	 */
	void setDetachOperation(DwFeatureDetachOperation value);

} // DwFeatureDeleteOperation
