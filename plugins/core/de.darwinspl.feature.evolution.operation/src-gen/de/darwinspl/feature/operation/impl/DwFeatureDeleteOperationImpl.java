/**
 */
package de.darwinspl.feature.operation.impl;

import de.darwinspl.feature.operation.DwFeatureDeleteOperation;
import de.darwinspl.feature.operation.DwFeatureDetachOperation;
import de.darwinspl.feature.operation.DwGroupDeleteOperation;
import de.darwinspl.feature.operation.DwOperationPackage;

import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Feature Delete Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureDeleteOperationImpl#getOldFeatureValidUntil <em>Old Feature Valid Until</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureDeleteOperationImpl#getDetachOperation <em>Detach Operation</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureDeleteOperationImpl#getConsequentGroupOperations <em>Consequent Group Operations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwFeatureDeleteOperationImpl extends DwFeatureOperationImpl implements DwFeatureDeleteOperation {
	/**
	 * The default value of the '{@link #getOldFeatureValidUntil() <em>Old Feature Valid Until</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOldFeatureValidUntil()
	 * @generated
	 * @ordered
	 */
	protected static final Date OLD_FEATURE_VALID_UNTIL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOldFeatureValidUntil() <em>Old Feature Valid Until</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOldFeatureValidUntil()
	 * @generated
	 * @ordered
	 */
	protected Date oldFeatureValidUntil = OLD_FEATURE_VALID_UNTIL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDetachOperation() <em>Detach Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDetachOperation()
	 * @generated
	 * @ordered
	 */
	protected DwFeatureDetachOperation detachOperation;

	/**
	 * The cached value of the '{@link #getConsequentGroupOperations() <em>Consequent Group Operations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConsequentGroupOperations()
	 * @generated
	 * @ordered
	 */
	protected EList<DwGroupDeleteOperation> consequentGroupOperations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwFeatureDeleteOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwOperationPackage.Literals.DW_FEATURE_DELETE_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Date getOldFeatureValidUntil() {
		return oldFeatureValidUntil;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOldFeatureValidUntil(Date newOldFeatureValidUntil) {
		Date oldOldFeatureValidUntil = oldFeatureValidUntil;
		oldFeatureValidUntil = newOldFeatureValidUntil;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_DELETE_OPERATION__OLD_FEATURE_VALID_UNTIL, oldOldFeatureValidUntil, oldFeatureValidUntil));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwGroupDeleteOperation> getConsequentGroupOperations() {
		if (consequentGroupOperations == null) {
			consequentGroupOperations = new EObjectContainmentEList<DwGroupDeleteOperation>(DwGroupDeleteOperation.class, this, DwOperationPackage.DW_FEATURE_DELETE_OPERATION__CONSEQUENT_GROUP_OPERATIONS);
		}
		return consequentGroupOperations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureDetachOperation getDetachOperation() {
		return detachOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDetachOperation(DwFeatureDetachOperation newDetachOperation, NotificationChain msgs) {
		DwFeatureDetachOperation oldDetachOperation = detachOperation;
		detachOperation = newDetachOperation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_DELETE_OPERATION__DETACH_OPERATION, oldDetachOperation, newDetachOperation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDetachOperation(DwFeatureDetachOperation newDetachOperation) {
		if (newDetachOperation != detachOperation) {
			NotificationChain msgs = null;
			if (detachOperation != null)
				msgs = ((InternalEObject)detachOperation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DwOperationPackage.DW_FEATURE_DELETE_OPERATION__DETACH_OPERATION, null, msgs);
			if (newDetachOperation != null)
				msgs = ((InternalEObject)newDetachOperation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DwOperationPackage.DW_FEATURE_DELETE_OPERATION__DETACH_OPERATION, null, msgs);
			msgs = basicSetDetachOperation(newDetachOperation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_DELETE_OPERATION__DETACH_OPERATION, newDetachOperation, newDetachOperation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_DELETE_OPERATION__DETACH_OPERATION:
				return basicSetDetachOperation(null, msgs);
			case DwOperationPackage.DW_FEATURE_DELETE_OPERATION__CONSEQUENT_GROUP_OPERATIONS:
				return ((InternalEList<?>)getConsequentGroupOperations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_DELETE_OPERATION__OLD_FEATURE_VALID_UNTIL:
				return getOldFeatureValidUntil();
			case DwOperationPackage.DW_FEATURE_DELETE_OPERATION__DETACH_OPERATION:
				return getDetachOperation();
			case DwOperationPackage.DW_FEATURE_DELETE_OPERATION__CONSEQUENT_GROUP_OPERATIONS:
				return getConsequentGroupOperations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_DELETE_OPERATION__OLD_FEATURE_VALID_UNTIL:
				setOldFeatureValidUntil((Date)newValue);
				return;
			case DwOperationPackage.DW_FEATURE_DELETE_OPERATION__DETACH_OPERATION:
				setDetachOperation((DwFeatureDetachOperation)newValue);
				return;
			case DwOperationPackage.DW_FEATURE_DELETE_OPERATION__CONSEQUENT_GROUP_OPERATIONS:
				getConsequentGroupOperations().clear();
				getConsequentGroupOperations().addAll((Collection<? extends DwGroupDeleteOperation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_DELETE_OPERATION__OLD_FEATURE_VALID_UNTIL:
				setOldFeatureValidUntil(OLD_FEATURE_VALID_UNTIL_EDEFAULT);
				return;
			case DwOperationPackage.DW_FEATURE_DELETE_OPERATION__DETACH_OPERATION:
				setDetachOperation((DwFeatureDetachOperation)null);
				return;
			case DwOperationPackage.DW_FEATURE_DELETE_OPERATION__CONSEQUENT_GROUP_OPERATIONS:
				getConsequentGroupOperations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_DELETE_OPERATION__OLD_FEATURE_VALID_UNTIL:
				return OLD_FEATURE_VALID_UNTIL_EDEFAULT == null ? oldFeatureValidUntil != null : !OLD_FEATURE_VALID_UNTIL_EDEFAULT.equals(oldFeatureValidUntil);
			case DwOperationPackage.DW_FEATURE_DELETE_OPERATION__DETACH_OPERATION:
				return detachOperation != null;
			case DwOperationPackage.DW_FEATURE_DELETE_OPERATION__CONSEQUENT_GROUP_OPERATIONS:
				return consequentGroupOperations != null && !consequentGroupOperations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (oldFeatureValidUntil: ");
		result.append(oldFeatureValidUntil);
		result.append(')');
		return result.toString();
	}

} //DwFeatureDeleteOperationImpl
