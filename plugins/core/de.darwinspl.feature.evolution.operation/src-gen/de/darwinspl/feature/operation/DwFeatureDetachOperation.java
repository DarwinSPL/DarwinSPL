/**
 */
package de.darwinspl.feature.operation;

import de.darwinspl.feature.DwGroupComposition;
import java.util.Date;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Feature Detach Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureDetachOperation#getOldGroupMembership <em>Old Group Membership</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureDetachOperation#getNewGroupMembership <em>New Group Membership</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureDetachOperation#getOldValidUntil <em>Old Valid Until</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureDetachOperation#getConsequentGroupOperation <em>Consequent Group Operation</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureDetachOperation()
 * @model
 * @generated
 */
public interface DwFeatureDetachOperation extends DwFeatureOperation {
	/**
	 * Returns the value of the '<em><b>Old Group Membership</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Old Group Membership</em>' reference.
	 * @see #setOldGroupMembership(DwGroupComposition)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureDetachOperation_OldGroupMembership()
	 * @model
	 * @generated
	 */
	DwGroupComposition getOldGroupMembership();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureDetachOperation#getOldGroupMembership <em>Old Group Membership</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Old Group Membership</em>' reference.
	 * @see #getOldGroupMembership()
	 * @generated
	 */
	void setOldGroupMembership(DwGroupComposition value);

	/**
	 * Returns the value of the '<em><b>New Group Membership</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>New Group Membership</em>' reference.
	 * @see #setNewGroupMembership(DwGroupComposition)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureDetachOperation_NewGroupMembership()
	 * @model
	 * @generated
	 */
	DwGroupComposition getNewGroupMembership();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureDetachOperation#getNewGroupMembership <em>New Group Membership</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>New Group Membership</em>' reference.
	 * @see #getNewGroupMembership()
	 * @generated
	 */
	void setNewGroupMembership(DwGroupComposition value);

	/**
	 * Returns the value of the '<em><b>Old Valid Until</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Old Valid Until</em>' attribute.
	 * @see #setOldValidUntil(Date)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureDetachOperation_OldValidUntil()
	 * @model
	 * @generated
	 */
	Date getOldValidUntil();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureDetachOperation#getOldValidUntil <em>Old Valid Until</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Old Valid Until</em>' attribute.
	 * @see #getOldValidUntil()
	 * @generated
	 */
	void setOldValidUntil(Date value);

	/**
	 * Returns the value of the '<em><b>Consequent Group Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Consequent Group Operation</em>' containment reference.
	 * @see #setConsequentGroupOperation(DwGroupDeleteOperation)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureDetachOperation_ConsequentGroupOperation()
	 * @model containment="true"
	 * @generated
	 */
	DwGroupDeleteOperation getConsequentGroupOperation();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureDetachOperation#getConsequentGroupOperation <em>Consequent Group Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Consequent Group Operation</em>' containment reference.
	 * @see #getConsequentGroupOperation()
	 * @generated
	 */
	void setConsequentGroupOperation(DwGroupDeleteOperation value);

} // DwFeatureDetachOperation
