/**
 */
package de.darwinspl.feature.operation;

import de.darwinspl.feature.DwGroup;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Group Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.DwGroupOperation#getGroup <em>Group</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwGroupOperation()
 * @model abstract="true"
 * @generated
 */
public interface DwGroupOperation extends DwEvolutionOperation {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' reference.
	 * @see #setGroup(DwGroup)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwGroupOperation_Group()
	 * @model required="true"
	 * @generated
	 */
	DwGroup getGroup();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwGroupOperation#getGroup <em>Group</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group</em>' reference.
	 * @see #getGroup()
	 * @generated
	 */
	void setGroup(DwGroup value);

} // DwGroupOperation
