/**
 */
package de.darwinspl.feature.operation.impl;

import de.darwinspl.feature.operation.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DwOperationFactoryImpl extends EFactoryImpl implements DwOperationFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DwOperationFactory init() {
		try {
			DwOperationFactory theDwOperationFactory = (DwOperationFactory)EPackage.Registry.INSTANCE.getEFactory(DwOperationPackage.eNS_URI);
			if (theDwOperationFactory != null) {
				return theDwOperationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DwOperationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwOperationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case DwOperationPackage.DW_EVOLUTION_OPERATION_MODEL: return createDwEvolutionOperationModel();
			case DwOperationPackage.DW_FEATURE_CREATE_OPERATION: return createDwFeatureCreateOperation();
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION: return createDwFeatureAddOperation();
			case DwOperationPackage.DW_FEATURE_DELETE_OPERATION: return createDwFeatureDeleteOperation();
			case DwOperationPackage.DW_FEATURE_DETACH_OPERATION: return createDwFeatureDetachOperation();
			case DwOperationPackage.DW_FEATURE_MOVE_OPERATION: return createDwFeatureMoveOperation();
			case DwOperationPackage.DW_FEATURE_RENAME_OPERATION: return createDwFeatureRenameOperation();
			case DwOperationPackage.DW_FEATURE_TYPE_CHANGE_OPERATION: return createDwFeatureTypeChangeOperation();
			case DwOperationPackage.DW_FEATURE_ATTRIBUTE_CREATE_OPERATION: return createDwFeatureAttributeCreateOperation();
			case DwOperationPackage.DW_FEATURE_ATTRIBUTE_DELETE_OPERATION: return createDwFeatureAttributeDeleteOperation();
			case DwOperationPackage.DW_FEATURE_ATTRIBUTE_RENAME_OPERATION: return createDwFeatureAttributeRenameOperation();
			case DwOperationPackage.DW_FEATURE_VERSION_CREATE_OPERATION: return createDwFeatureVersionCreateOperation();
			case DwOperationPackage.DW_FEATURE_VERSION_DELETE_OPERATION: return createDwFeatureVersionDeleteOperation();
			case DwOperationPackage.DW_FEATURE_VERSION_RENAME_OPERATION: return createDwFeatureVersionRenameOperation();
			case DwOperationPackage.DW_GROUP_CREATE_OPERATION: return createDwGroupCreateOperation();
			case DwOperationPackage.DW_GROUP_ADD_OPERATION: return createDwGroupAddOperation();
			case DwOperationPackage.DW_GROUP_DELETE_OPERATION: return createDwGroupDeleteOperation();
			case DwOperationPackage.DW_GROUP_DETACH_OPERATION: return createDwGroupDetachOperation();
			case DwOperationPackage.DW_GROUP_MOVE_OPERATION: return createDwGroupMoveOperation();
			case DwOperationPackage.DW_GROUP_TYPE_CHANGE_OPERATION: return createDwGroupTypeChangeOperation();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwEvolutionOperationModel createDwEvolutionOperationModel() {
		DwEvolutionOperationModelImpl dwEvolutionOperationModel = new DwEvolutionOperationModelImpl();
		return dwEvolutionOperationModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureCreateOperation createDwFeatureCreateOperation() {
		DwFeatureCreateOperationImpl dwFeatureCreateOperation = new DwFeatureCreateOperationImpl();
		return dwFeatureCreateOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureAddOperation createDwFeatureAddOperation() {
		DwFeatureAddOperationImpl dwFeatureAddOperation = new DwFeatureAddOperationImpl();
		return dwFeatureAddOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureDeleteOperation createDwFeatureDeleteOperation() {
		DwFeatureDeleteOperationImpl dwFeatureDeleteOperation = new DwFeatureDeleteOperationImpl();
		return dwFeatureDeleteOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureDetachOperation createDwFeatureDetachOperation() {
		DwFeatureDetachOperationImpl dwFeatureDetachOperation = new DwFeatureDetachOperationImpl();
		return dwFeatureDetachOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureMoveOperation createDwFeatureMoveOperation() {
		DwFeatureMoveOperationImpl dwFeatureMoveOperation = new DwFeatureMoveOperationImpl();
		return dwFeatureMoveOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureRenameOperation createDwFeatureRenameOperation() {
		DwFeatureRenameOperationImpl dwFeatureRenameOperation = new DwFeatureRenameOperationImpl();
		return dwFeatureRenameOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureTypeChangeOperation createDwFeatureTypeChangeOperation() {
		DwFeatureTypeChangeOperationImpl dwFeatureTypeChangeOperation = new DwFeatureTypeChangeOperationImpl();
		return dwFeatureTypeChangeOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureAttributeCreateOperation createDwFeatureAttributeCreateOperation() {
		DwFeatureAttributeCreateOperationImpl dwFeatureAttributeCreateOperation = new DwFeatureAttributeCreateOperationImpl();
		return dwFeatureAttributeCreateOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureAttributeDeleteOperation createDwFeatureAttributeDeleteOperation() {
		DwFeatureAttributeDeleteOperationImpl dwFeatureAttributeDeleteOperation = new DwFeatureAttributeDeleteOperationImpl();
		return dwFeatureAttributeDeleteOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureAttributeRenameOperation createDwFeatureAttributeRenameOperation() {
		DwFeatureAttributeRenameOperationImpl dwFeatureAttributeRenameOperation = new DwFeatureAttributeRenameOperationImpl();
		return dwFeatureAttributeRenameOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureVersionCreateOperation createDwFeatureVersionCreateOperation() {
		DwFeatureVersionCreateOperationImpl dwFeatureVersionCreateOperation = new DwFeatureVersionCreateOperationImpl();
		return dwFeatureVersionCreateOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureVersionDeleteOperation createDwFeatureVersionDeleteOperation() {
		DwFeatureVersionDeleteOperationImpl dwFeatureVersionDeleteOperation = new DwFeatureVersionDeleteOperationImpl();
		return dwFeatureVersionDeleteOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureVersionRenameOperation createDwFeatureVersionRenameOperation() {
		DwFeatureVersionRenameOperationImpl dwFeatureVersionRenameOperation = new DwFeatureVersionRenameOperationImpl();
		return dwFeatureVersionRenameOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroupCreateOperation createDwGroupCreateOperation() {
		DwGroupCreateOperationImpl dwGroupCreateOperation = new DwGroupCreateOperationImpl();
		return dwGroupCreateOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroupAddOperation createDwGroupAddOperation() {
		DwGroupAddOperationImpl dwGroupAddOperation = new DwGroupAddOperationImpl();
		return dwGroupAddOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroupDeleteOperation createDwGroupDeleteOperation() {
		DwGroupDeleteOperationImpl dwGroupDeleteOperation = new DwGroupDeleteOperationImpl();
		return dwGroupDeleteOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroupDetachOperation createDwGroupDetachOperation() {
		DwGroupDetachOperationImpl dwGroupDetachOperation = new DwGroupDetachOperationImpl();
		return dwGroupDetachOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroupMoveOperation createDwGroupMoveOperation() {
		DwGroupMoveOperationImpl dwGroupMoveOperation = new DwGroupMoveOperationImpl();
		return dwGroupMoveOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroupTypeChangeOperation createDwGroupTypeChangeOperation() {
		DwGroupTypeChangeOperationImpl dwGroupTypeChangeOperation = new DwGroupTypeChangeOperationImpl();
		return dwGroupTypeChangeOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwOperationPackage getDwOperationPackage() {
		return (DwOperationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DwOperationPackage getPackage() {
		return DwOperationPackage.eINSTANCE;
	}

} //DwOperationFactoryImpl
