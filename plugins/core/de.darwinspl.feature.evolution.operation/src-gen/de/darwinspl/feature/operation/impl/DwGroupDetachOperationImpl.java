/**
 */
package de.darwinspl.feature.operation.impl;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwParentFeatureChildGroupContainer;

import de.darwinspl.feature.operation.DwGroupDetachOperation;
import de.darwinspl.feature.operation.DwOperationPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Group Detach Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwGroupDetachOperationImpl#getOldContainer <em>Old Container</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwGroupDetachOperationImpl#getOldParent <em>Old Parent</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwGroupDetachOperationImpl extends DwGroupOperationImpl implements DwGroupDetachOperation {
	/**
	 * The cached value of the '{@link #getOldContainer() <em>Old Container</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOldContainer()
	 * @generated
	 * @ordered
	 */
	protected DwParentFeatureChildGroupContainer oldContainer;

	/**
	 * The cached value of the '{@link #getOldParent() <em>Old Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOldParent()
	 * @generated
	 * @ordered
	 */
	protected DwFeature oldParent;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwGroupDetachOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwOperationPackage.Literals.DW_GROUP_DETACH_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwParentFeatureChildGroupContainer getOldContainer() {
		if (oldContainer != null && oldContainer.eIsProxy()) {
			InternalEObject oldOldContainer = (InternalEObject)oldContainer;
			oldContainer = (DwParentFeatureChildGroupContainer)eResolveProxy(oldOldContainer);
			if (oldContainer != oldOldContainer) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwOperationPackage.DW_GROUP_DETACH_OPERATION__OLD_CONTAINER, oldOldContainer, oldContainer));
			}
		}
		return oldContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwParentFeatureChildGroupContainer basicGetOldContainer() {
		return oldContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOldContainer(DwParentFeatureChildGroupContainer newOldContainer) {
		DwParentFeatureChildGroupContainer oldOldContainer = oldContainer;
		oldContainer = newOldContainer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_GROUP_DETACH_OPERATION__OLD_CONTAINER, oldOldContainer, oldContainer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeature getOldParent() {
		if (oldParent != null && oldParent.eIsProxy()) {
			InternalEObject oldOldParent = (InternalEObject)oldParent;
			oldParent = (DwFeature)eResolveProxy(oldOldParent);
			if (oldParent != oldOldParent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwOperationPackage.DW_GROUP_DETACH_OPERATION__OLD_PARENT, oldOldParent, oldParent));
			}
		}
		return oldParent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwFeature basicGetOldParent() {
		return oldParent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOldParent(DwFeature newOldParent) {
		DwFeature oldOldParent = oldParent;
		oldParent = newOldParent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_GROUP_DETACH_OPERATION__OLD_PARENT, oldOldParent, oldParent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwOperationPackage.DW_GROUP_DETACH_OPERATION__OLD_CONTAINER:
				if (resolve) return getOldContainer();
				return basicGetOldContainer();
			case DwOperationPackage.DW_GROUP_DETACH_OPERATION__OLD_PARENT:
				if (resolve) return getOldParent();
				return basicGetOldParent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwOperationPackage.DW_GROUP_DETACH_OPERATION__OLD_CONTAINER:
				setOldContainer((DwParentFeatureChildGroupContainer)newValue);
				return;
			case DwOperationPackage.DW_GROUP_DETACH_OPERATION__OLD_PARENT:
				setOldParent((DwFeature)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_GROUP_DETACH_OPERATION__OLD_CONTAINER:
				setOldContainer((DwParentFeatureChildGroupContainer)null);
				return;
			case DwOperationPackage.DW_GROUP_DETACH_OPERATION__OLD_PARENT:
				setOldParent((DwFeature)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_GROUP_DETACH_OPERATION__OLD_CONTAINER:
				return oldContainer != null;
			case DwOperationPackage.DW_GROUP_DETACH_OPERATION__OLD_PARENT:
				return oldParent != null;
		}
		return super.eIsSet(featureID);
	}

} //DwGroupDetachOperationImpl
