/**
 */
package de.darwinspl.feature.operation;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Feature Version Create Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureVersionCreateOperation#getParent <em>Parent</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureVersionCreateOperation#isUseSameBranch <em>Use Same Branch</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureVersionCreateOperation#isWireAndAddAfter <em>Wire And Add After</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureVersionCreateOperation()
 * @model
 * @generated
 */
public interface DwFeatureVersionCreateOperation extends DwFeatureVersionOperation {
	/**
	 * Returns the value of the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' reference.
	 * @see #setParent(EObject)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureVersionCreateOperation_Parent()
	 * @model required="true"
	 * @generated
	 */
	EObject getParent();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureVersionCreateOperation#getParent <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(EObject value);

	/**
	 * Returns the value of the '<em><b>Use Same Branch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Same Branch</em>' attribute.
	 * @see #setUseSameBranch(boolean)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureVersionCreateOperation_UseSameBranch()
	 * @model required="true"
	 * @generated
	 */
	boolean isUseSameBranch();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureVersionCreateOperation#isUseSameBranch <em>Use Same Branch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use Same Branch</em>' attribute.
	 * @see #isUseSameBranch()
	 * @generated
	 */
	void setUseSameBranch(boolean value);

	/**
	 * Returns the value of the '<em><b>Wire And Add After</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wire And Add After</em>' attribute.
	 * @see #setWireAndAddAfter(boolean)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureVersionCreateOperation_WireAndAddAfter()
	 * @model required="true"
	 * @generated
	 */
	boolean isWireAndAddAfter();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureVersionCreateOperation#isWireAndAddAfter <em>Wire And Add After</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wire And Add After</em>' attribute.
	 * @see #isWireAndAddAfter()
	 * @generated
	 */
	void setWireAndAddAfter(boolean value);

} // DwFeatureVersionCreateOperation
