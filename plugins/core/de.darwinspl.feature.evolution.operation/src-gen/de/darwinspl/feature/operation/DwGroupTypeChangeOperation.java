/**
 */
package de.darwinspl.feature.operation;

import de.darwinspl.feature.DwGroupType;
import de.darwinspl.feature.DwGroupTypeEnum;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Group Type Change Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.DwGroupTypeChangeOperation#getConsequentOperations <em>Consequent Operations</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwGroupTypeChangeOperation#getNewGroupType <em>New Group Type</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwGroupTypeChangeOperation#getNewGroupTypeEnum <em>New Group Type Enum</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwGroupTypeChangeOperation#getOldGroupType <em>Old Group Type</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwGroupTypeChangeOperation()
 * @model
 * @generated
 */
public interface DwGroupTypeChangeOperation extends DwGroupOperation {
	/**
	 * Returns the value of the '<em><b>Consequent Operations</b></em>' containment reference list.
	 * The list contents are of type {@link de.darwinspl.feature.operation.DwFeatureTypeChangeOperation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Consequent Operations</em>' containment reference list.
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwGroupTypeChangeOperation_ConsequentOperations()
	 * @model containment="true"
	 * @generated
	 */
	EList<DwFeatureTypeChangeOperation> getConsequentOperations();

	/**
	 * Returns the value of the '<em><b>New Group Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>New Group Type</em>' reference.
	 * @see #setNewGroupType(DwGroupType)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwGroupTypeChangeOperation_NewGroupType()
	 * @model
	 * @generated
	 */
	DwGroupType getNewGroupType();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwGroupTypeChangeOperation#getNewGroupType <em>New Group Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>New Group Type</em>' reference.
	 * @see #getNewGroupType()
	 * @generated
	 */
	void setNewGroupType(DwGroupType value);

	/**
	 * Returns the value of the '<em><b>New Group Type Enum</b></em>' attribute.
	 * The literals are from the enumeration {@link de.darwinspl.feature.DwGroupTypeEnum}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>New Group Type Enum</em>' attribute.
	 * @see de.darwinspl.feature.DwGroupTypeEnum
	 * @see #setNewGroupTypeEnum(DwGroupTypeEnum)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwGroupTypeChangeOperation_NewGroupTypeEnum()
	 * @model required="true"
	 * @generated
	 */
	DwGroupTypeEnum getNewGroupTypeEnum();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwGroupTypeChangeOperation#getNewGroupTypeEnum <em>New Group Type Enum</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>New Group Type Enum</em>' attribute.
	 * @see de.darwinspl.feature.DwGroupTypeEnum
	 * @see #getNewGroupTypeEnum()
	 * @generated
	 */
	void setNewGroupTypeEnum(DwGroupTypeEnum value);

	/**
	 * Returns the value of the '<em><b>Old Group Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Old Group Type</em>' reference.
	 * @see #setOldGroupType(DwGroupType)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwGroupTypeChangeOperation_OldGroupType()
	 * @model
	 * @generated
	 */
	DwGroupType getOldGroupType();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwGroupTypeChangeOperation#getOldGroupType <em>Old Group Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Old Group Type</em>' reference.
	 * @see #getOldGroupType()
	 * @generated
	 */
	void setOldGroupType(DwGroupType value);

} // DwGroupTypeChangeOperation
