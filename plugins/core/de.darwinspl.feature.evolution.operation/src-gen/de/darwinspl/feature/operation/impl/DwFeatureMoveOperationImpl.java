/**
 */
package de.darwinspl.feature.operation.impl;

import de.darwinspl.feature.operation.DwFeatureAddOperation;
import de.darwinspl.feature.operation.DwFeatureDetachOperation;
import de.darwinspl.feature.operation.DwFeatureMoveOperation;
import de.darwinspl.feature.operation.DwOperationPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Feature Move Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureMoveOperationImpl#getDetachOperation <em>Detach Operation</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureMoveOperationImpl#getAddOperation <em>Add Operation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwFeatureMoveOperationImpl extends DwFeatureOperationImpl implements DwFeatureMoveOperation {
	/**
	 * The cached value of the '{@link #getDetachOperation() <em>Detach Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDetachOperation()
	 * @generated
	 * @ordered
	 */
	protected DwFeatureDetachOperation detachOperation;

	/**
	 * The cached value of the '{@link #getAddOperation() <em>Add Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAddOperation()
	 * @generated
	 * @ordered
	 */
	protected DwFeatureAddOperation addOperation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwFeatureMoveOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwOperationPackage.Literals.DW_FEATURE_MOVE_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureDetachOperation getDetachOperation() {
		return detachOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDetachOperation(DwFeatureDetachOperation newDetachOperation, NotificationChain msgs) {
		DwFeatureDetachOperation oldDetachOperation = detachOperation;
		detachOperation = newDetachOperation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_MOVE_OPERATION__DETACH_OPERATION, oldDetachOperation, newDetachOperation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDetachOperation(DwFeatureDetachOperation newDetachOperation) {
		if (newDetachOperation != detachOperation) {
			NotificationChain msgs = null;
			if (detachOperation != null)
				msgs = ((InternalEObject)detachOperation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DwOperationPackage.DW_FEATURE_MOVE_OPERATION__DETACH_OPERATION, null, msgs);
			if (newDetachOperation != null)
				msgs = ((InternalEObject)newDetachOperation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DwOperationPackage.DW_FEATURE_MOVE_OPERATION__DETACH_OPERATION, null, msgs);
			msgs = basicSetDetachOperation(newDetachOperation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_MOVE_OPERATION__DETACH_OPERATION, newDetachOperation, newDetachOperation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureAddOperation getAddOperation() {
		return addOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAddOperation(DwFeatureAddOperation newAddOperation, NotificationChain msgs) {
		DwFeatureAddOperation oldAddOperation = addOperation;
		addOperation = newAddOperation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_MOVE_OPERATION__ADD_OPERATION, oldAddOperation, newAddOperation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAddOperation(DwFeatureAddOperation newAddOperation) {
		if (newAddOperation != addOperation) {
			NotificationChain msgs = null;
			if (addOperation != null)
				msgs = ((InternalEObject)addOperation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DwOperationPackage.DW_FEATURE_MOVE_OPERATION__ADD_OPERATION, null, msgs);
			if (newAddOperation != null)
				msgs = ((InternalEObject)newAddOperation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DwOperationPackage.DW_FEATURE_MOVE_OPERATION__ADD_OPERATION, null, msgs);
			msgs = basicSetAddOperation(newAddOperation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_MOVE_OPERATION__ADD_OPERATION, newAddOperation, newAddOperation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_MOVE_OPERATION__DETACH_OPERATION:
				return basicSetDetachOperation(null, msgs);
			case DwOperationPackage.DW_FEATURE_MOVE_OPERATION__ADD_OPERATION:
				return basicSetAddOperation(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_MOVE_OPERATION__DETACH_OPERATION:
				return getDetachOperation();
			case DwOperationPackage.DW_FEATURE_MOVE_OPERATION__ADD_OPERATION:
				return getAddOperation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_MOVE_OPERATION__DETACH_OPERATION:
				setDetachOperation((DwFeatureDetachOperation)newValue);
				return;
			case DwOperationPackage.DW_FEATURE_MOVE_OPERATION__ADD_OPERATION:
				setAddOperation((DwFeatureAddOperation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_MOVE_OPERATION__DETACH_OPERATION:
				setDetachOperation((DwFeatureDetachOperation)null);
				return;
			case DwOperationPackage.DW_FEATURE_MOVE_OPERATION__ADD_OPERATION:
				setAddOperation((DwFeatureAddOperation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_MOVE_OPERATION__DETACH_OPERATION:
				return detachOperation != null;
			case DwOperationPackage.DW_FEATURE_MOVE_OPERATION__ADD_OPERATION:
				return addOperation != null;
		}
		return super.eIsSet(featureID);
	}

} //DwFeatureMoveOperationImpl
