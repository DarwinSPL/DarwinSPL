/**
 */
package de.darwinspl.feature.operation.impl;

import de.darwinspl.feature.DwName;

import de.darwinspl.feature.operation.DwFeatureAttributeRenameOperation;
import de.darwinspl.feature.operation.DwOperationPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Feature Attribute Rename Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureAttributeRenameOperationImpl#getOldName <em>Old Name</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureAttributeRenameOperationImpl#getNewName <em>New Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwFeatureAttributeRenameOperationImpl extends DwFeatureAttributeOperationImpl implements DwFeatureAttributeRenameOperation {
	/**
	 * The cached value of the '{@link #getOldName() <em>Old Name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOldName()
	 * @generated
	 * @ordered
	 */
	protected DwName oldName;

	/**
	 * The cached value of the '{@link #getNewName() <em>New Name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNewName()
	 * @generated
	 * @ordered
	 */
	protected DwName newName;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwFeatureAttributeRenameOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwOperationPackage.Literals.DW_FEATURE_ATTRIBUTE_RENAME_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwName getOldName() {
		if (oldName != null && oldName.eIsProxy()) {
			InternalEObject oldOldName = (InternalEObject)oldName;
			oldName = (DwName)eResolveProxy(oldOldName);
			if (oldName != oldOldName) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwOperationPackage.DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__OLD_NAME, oldOldName, oldName));
			}
		}
		return oldName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwName basicGetOldName() {
		return oldName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOldName(DwName newOldName) {
		DwName oldOldName = oldName;
		oldName = newOldName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__OLD_NAME, oldOldName, oldName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwName getNewName() {
		if (newName != null && newName.eIsProxy()) {
			InternalEObject oldNewName = (InternalEObject)newName;
			newName = (DwName)eResolveProxy(oldNewName);
			if (newName != oldNewName) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwOperationPackage.DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__NEW_NAME, oldNewName, newName));
			}
		}
		return newName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwName basicGetNewName() {
		return newName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNewName(DwName newNewName) {
		DwName oldNewName = newName;
		newName = newNewName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__NEW_NAME, oldNewName, newName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__OLD_NAME:
				if (resolve) return getOldName();
				return basicGetOldName();
			case DwOperationPackage.DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__NEW_NAME:
				if (resolve) return getNewName();
				return basicGetNewName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__OLD_NAME:
				setOldName((DwName)newValue);
				return;
			case DwOperationPackage.DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__NEW_NAME:
				setNewName((DwName)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__OLD_NAME:
				setOldName((DwName)null);
				return;
			case DwOperationPackage.DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__NEW_NAME:
				setNewName((DwName)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__OLD_NAME:
				return oldName != null;
			case DwOperationPackage.DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__NEW_NAME:
				return newName != null;
		}
		return super.eIsSet(featureID);
	}

} //DwFeatureAttributeRenameOperationImpl
