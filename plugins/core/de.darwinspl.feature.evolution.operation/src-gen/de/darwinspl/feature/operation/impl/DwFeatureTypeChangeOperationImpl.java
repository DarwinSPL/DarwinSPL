/**
 */
package de.darwinspl.feature.operation.impl;

import de.darwinspl.feature.DwFeatureType;
import de.darwinspl.feature.DwFeatureTypeEnum;

import de.darwinspl.feature.operation.DwFeatureTypeChangeOperation;
import de.darwinspl.feature.operation.DwOperationPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Feature Type Change Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureTypeChangeOperationImpl#getNewFeatureType <em>New Feature Type</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureTypeChangeOperationImpl#getNewFeatureTypeEnum <em>New Feature Type Enum</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureTypeChangeOperationImpl#getOldFeatureType <em>Old Feature Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwFeatureTypeChangeOperationImpl extends DwFeatureOperationImpl implements DwFeatureTypeChangeOperation {
	/**
	 * The cached value of the '{@link #getNewFeatureType() <em>New Feature Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNewFeatureType()
	 * @generated
	 * @ordered
	 */
	protected DwFeatureType newFeatureType;

	/**
	 * The default value of the '{@link #getNewFeatureTypeEnum() <em>New Feature Type Enum</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNewFeatureTypeEnum()
	 * @generated
	 * @ordered
	 */
	protected static final DwFeatureTypeEnum NEW_FEATURE_TYPE_ENUM_EDEFAULT = DwFeatureTypeEnum.OPTIONAL;

	/**
	 * The cached value of the '{@link #getNewFeatureTypeEnum() <em>New Feature Type Enum</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNewFeatureTypeEnum()
	 * @generated
	 * @ordered
	 */
	protected DwFeatureTypeEnum newFeatureTypeEnum = NEW_FEATURE_TYPE_ENUM_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOldFeatureType() <em>Old Feature Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOldFeatureType()
	 * @generated
	 * @ordered
	 */
	protected DwFeatureType oldFeatureType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwFeatureTypeChangeOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwOperationPackage.Literals.DW_FEATURE_TYPE_CHANGE_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureType getNewFeatureType() {
		if (newFeatureType != null && newFeatureType.eIsProxy()) {
			InternalEObject oldNewFeatureType = (InternalEObject)newFeatureType;
			newFeatureType = (DwFeatureType)eResolveProxy(oldNewFeatureType);
			if (newFeatureType != oldNewFeatureType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwOperationPackage.DW_FEATURE_TYPE_CHANGE_OPERATION__NEW_FEATURE_TYPE, oldNewFeatureType, newFeatureType));
			}
		}
		return newFeatureType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwFeatureType basicGetNewFeatureType() {
		return newFeatureType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNewFeatureType(DwFeatureType newNewFeatureType) {
		DwFeatureType oldNewFeatureType = newFeatureType;
		newFeatureType = newNewFeatureType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_TYPE_CHANGE_OPERATION__NEW_FEATURE_TYPE, oldNewFeatureType, newFeatureType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureTypeEnum getNewFeatureTypeEnum() {
		return newFeatureTypeEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNewFeatureTypeEnum(DwFeatureTypeEnum newNewFeatureTypeEnum) {
		DwFeatureTypeEnum oldNewFeatureTypeEnum = newFeatureTypeEnum;
		newFeatureTypeEnum = newNewFeatureTypeEnum == null ? NEW_FEATURE_TYPE_ENUM_EDEFAULT : newNewFeatureTypeEnum;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_TYPE_CHANGE_OPERATION__NEW_FEATURE_TYPE_ENUM, oldNewFeatureTypeEnum, newFeatureTypeEnum));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureType getOldFeatureType() {
		if (oldFeatureType != null && oldFeatureType.eIsProxy()) {
			InternalEObject oldOldFeatureType = (InternalEObject)oldFeatureType;
			oldFeatureType = (DwFeatureType)eResolveProxy(oldOldFeatureType);
			if (oldFeatureType != oldOldFeatureType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwOperationPackage.DW_FEATURE_TYPE_CHANGE_OPERATION__OLD_FEATURE_TYPE, oldOldFeatureType, oldFeatureType));
			}
		}
		return oldFeatureType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwFeatureType basicGetOldFeatureType() {
		return oldFeatureType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOldFeatureType(DwFeatureType newOldFeatureType) {
		DwFeatureType oldOldFeatureType = oldFeatureType;
		oldFeatureType = newOldFeatureType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_TYPE_CHANGE_OPERATION__OLD_FEATURE_TYPE, oldOldFeatureType, oldFeatureType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_TYPE_CHANGE_OPERATION__NEW_FEATURE_TYPE:
				if (resolve) return getNewFeatureType();
				return basicGetNewFeatureType();
			case DwOperationPackage.DW_FEATURE_TYPE_CHANGE_OPERATION__NEW_FEATURE_TYPE_ENUM:
				return getNewFeatureTypeEnum();
			case DwOperationPackage.DW_FEATURE_TYPE_CHANGE_OPERATION__OLD_FEATURE_TYPE:
				if (resolve) return getOldFeatureType();
				return basicGetOldFeatureType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_TYPE_CHANGE_OPERATION__NEW_FEATURE_TYPE:
				setNewFeatureType((DwFeatureType)newValue);
				return;
			case DwOperationPackage.DW_FEATURE_TYPE_CHANGE_OPERATION__NEW_FEATURE_TYPE_ENUM:
				setNewFeatureTypeEnum((DwFeatureTypeEnum)newValue);
				return;
			case DwOperationPackage.DW_FEATURE_TYPE_CHANGE_OPERATION__OLD_FEATURE_TYPE:
				setOldFeatureType((DwFeatureType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_TYPE_CHANGE_OPERATION__NEW_FEATURE_TYPE:
				setNewFeatureType((DwFeatureType)null);
				return;
			case DwOperationPackage.DW_FEATURE_TYPE_CHANGE_OPERATION__NEW_FEATURE_TYPE_ENUM:
				setNewFeatureTypeEnum(NEW_FEATURE_TYPE_ENUM_EDEFAULT);
				return;
			case DwOperationPackage.DW_FEATURE_TYPE_CHANGE_OPERATION__OLD_FEATURE_TYPE:
				setOldFeatureType((DwFeatureType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_TYPE_CHANGE_OPERATION__NEW_FEATURE_TYPE:
				return newFeatureType != null;
			case DwOperationPackage.DW_FEATURE_TYPE_CHANGE_OPERATION__NEW_FEATURE_TYPE_ENUM:
				return newFeatureTypeEnum != NEW_FEATURE_TYPE_ENUM_EDEFAULT;
			case DwOperationPackage.DW_FEATURE_TYPE_CHANGE_OPERATION__OLD_FEATURE_TYPE:
				return oldFeatureType != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (newFeatureTypeEnum: ");
		result.append(newFeatureTypeEnum);
		result.append(')');
		return result.toString();
	}

} //DwFeatureTypeChangeOperationImpl
