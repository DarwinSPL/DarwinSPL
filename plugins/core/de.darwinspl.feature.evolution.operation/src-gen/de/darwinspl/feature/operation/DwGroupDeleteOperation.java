/**
 */
package de.darwinspl.feature.operation;

import java.util.Date;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Group Delete Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.DwGroupDeleteOperation#getOldGroupValidUntil <em>Old Group Valid Until</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwGroupDeleteOperation#getDetachOperation <em>Detach Operation</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwGroupDeleteOperation#getConsequentFeatureOperations <em>Consequent Feature Operations</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwGroupDeleteOperation()
 * @model
 * @generated
 */
public interface DwGroupDeleteOperation extends DwGroupOperation {
	/**
	 * Returns the value of the '<em><b>Old Group Valid Until</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Old Group Valid Until</em>' attribute.
	 * @see #setOldGroupValidUntil(Date)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwGroupDeleteOperation_OldGroupValidUntil()
	 * @model
	 * @generated
	 */
	Date getOldGroupValidUntil();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwGroupDeleteOperation#getOldGroupValidUntil <em>Old Group Valid Until</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Old Group Valid Until</em>' attribute.
	 * @see #getOldGroupValidUntil()
	 * @generated
	 */
	void setOldGroupValidUntil(Date value);

	/**
	 * Returns the value of the '<em><b>Consequent Feature Operations</b></em>' containment reference list.
	 * The list contents are of type {@link de.darwinspl.feature.operation.DwFeatureDeleteOperation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Consequent Feature Operations</em>' containment reference list.
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwGroupDeleteOperation_ConsequentFeatureOperations()
	 * @model containment="true"
	 * @generated
	 */
	EList<DwFeatureDeleteOperation> getConsequentFeatureOperations();

	/**
	 * Returns the value of the '<em><b>Detach Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Detach Operation</em>' containment reference.
	 * @see #setDetachOperation(DwGroupDetachOperation)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwGroupDeleteOperation_DetachOperation()
	 * @model containment="true" required="true"
	 * @generated
	 */
	DwGroupDetachOperation getDetachOperation();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwGroupDeleteOperation#getDetachOperation <em>Detach Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Detach Operation</em>' containment reference.
	 * @see #getDetachOperation()
	 * @generated
	 */
	void setDetachOperation(DwGroupDetachOperation value);

} // DwGroupDeleteOperation
