/**
 */
package de.darwinspl.feature.operation.impl;

import de.darwinspl.feature.operation.DwFeatureVersionCreateOperation;
import de.darwinspl.feature.operation.DwOperationPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Feature Version Create Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureVersionCreateOperationImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureVersionCreateOperationImpl#isUseSameBranch <em>Use Same Branch</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureVersionCreateOperationImpl#isWireAndAddAfter <em>Wire And Add After</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwFeatureVersionCreateOperationImpl extends DwFeatureVersionOperationImpl implements DwFeatureVersionCreateOperation {
	/**
	 * The cached value of the '{@link #getParent() <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParent()
	 * @generated
	 * @ordered
	 */
	protected EObject parent;

	/**
	 * The default value of the '{@link #isUseSameBranch() <em>Use Same Branch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUseSameBranch()
	 * @generated
	 * @ordered
	 */
	protected static final boolean USE_SAME_BRANCH_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isUseSameBranch() <em>Use Same Branch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUseSameBranch()
	 * @generated
	 * @ordered
	 */
	protected boolean useSameBranch = USE_SAME_BRANCH_EDEFAULT;

	/**
	 * The default value of the '{@link #isWireAndAddAfter() <em>Wire And Add After</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isWireAndAddAfter()
	 * @generated
	 * @ordered
	 */
	protected static final boolean WIRE_AND_ADD_AFTER_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isWireAndAddAfter() <em>Wire And Add After</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isWireAndAddAfter()
	 * @generated
	 * @ordered
	 */
	protected boolean wireAndAddAfter = WIRE_AND_ADD_AFTER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwFeatureVersionCreateOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwOperationPackage.Literals.DW_FEATURE_VERSION_CREATE_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject getParent() {
		if (parent != null && parent.eIsProxy()) {
			InternalEObject oldParent = (InternalEObject)parent;
			parent = eResolveProxy(oldParent);
			if (parent != oldParent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwOperationPackage.DW_FEATURE_VERSION_CREATE_OPERATION__PARENT, oldParent, parent));
			}
		}
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetParent() {
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setParent(EObject newParent) {
		EObject oldParent = parent;
		parent = newParent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_VERSION_CREATE_OPERATION__PARENT, oldParent, parent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isUseSameBranch() {
		return useSameBranch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUseSameBranch(boolean newUseSameBranch) {
		boolean oldUseSameBranch = useSameBranch;
		useSameBranch = newUseSameBranch;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_VERSION_CREATE_OPERATION__USE_SAME_BRANCH, oldUseSameBranch, useSameBranch));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isWireAndAddAfter() {
		return wireAndAddAfter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setWireAndAddAfter(boolean newWireAndAddAfter) {
		boolean oldWireAndAddAfter = wireAndAddAfter;
		wireAndAddAfter = newWireAndAddAfter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_VERSION_CREATE_OPERATION__WIRE_AND_ADD_AFTER, oldWireAndAddAfter, wireAndAddAfter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_VERSION_CREATE_OPERATION__PARENT:
				if (resolve) return getParent();
				return basicGetParent();
			case DwOperationPackage.DW_FEATURE_VERSION_CREATE_OPERATION__USE_SAME_BRANCH:
				return isUseSameBranch();
			case DwOperationPackage.DW_FEATURE_VERSION_CREATE_OPERATION__WIRE_AND_ADD_AFTER:
				return isWireAndAddAfter();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_VERSION_CREATE_OPERATION__PARENT:
				setParent((EObject)newValue);
				return;
			case DwOperationPackage.DW_FEATURE_VERSION_CREATE_OPERATION__USE_SAME_BRANCH:
				setUseSameBranch((Boolean)newValue);
				return;
			case DwOperationPackage.DW_FEATURE_VERSION_CREATE_OPERATION__WIRE_AND_ADD_AFTER:
				setWireAndAddAfter((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_VERSION_CREATE_OPERATION__PARENT:
				setParent((EObject)null);
				return;
			case DwOperationPackage.DW_FEATURE_VERSION_CREATE_OPERATION__USE_SAME_BRANCH:
				setUseSameBranch(USE_SAME_BRANCH_EDEFAULT);
				return;
			case DwOperationPackage.DW_FEATURE_VERSION_CREATE_OPERATION__WIRE_AND_ADD_AFTER:
				setWireAndAddAfter(WIRE_AND_ADD_AFTER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_VERSION_CREATE_OPERATION__PARENT:
				return parent != null;
			case DwOperationPackage.DW_FEATURE_VERSION_CREATE_OPERATION__USE_SAME_BRANCH:
				return useSameBranch != USE_SAME_BRANCH_EDEFAULT;
			case DwOperationPackage.DW_FEATURE_VERSION_CREATE_OPERATION__WIRE_AND_ADD_AFTER:
				return wireAndAddAfter != WIRE_AND_ADD_AFTER_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (useSameBranch: ");
		result.append(useSameBranch);
		result.append(", wireAndAddAfter: ");
		result.append(wireAndAddAfter);
		result.append(')');
		return result.toString();
	}

} //DwFeatureVersionCreateOperationImpl
