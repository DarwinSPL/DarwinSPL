/**
 */
package de.darwinspl.feature.operation.impl;

import de.darwinspl.feature.DwGroupComposition;

import de.darwinspl.feature.operation.DwFeatureDetachOperation;
import de.darwinspl.feature.operation.DwGroupDeleteOperation;
import de.darwinspl.feature.operation.DwOperationPackage;
import java.util.Date;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Feature Detach Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureDetachOperationImpl#getOldGroupMembership <em>Old Group Membership</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureDetachOperationImpl#getNewGroupMembership <em>New Group Membership</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureDetachOperationImpl#getOldValidUntil <em>Old Valid Until</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureDetachOperationImpl#getConsequentGroupOperation <em>Consequent Group Operation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwFeatureDetachOperationImpl extends DwFeatureOperationImpl implements DwFeatureDetachOperation {
	/**
	 * The cached value of the '{@link #getOldGroupMembership() <em>Old Group Membership</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOldGroupMembership()
	 * @generated
	 * @ordered
	 */
	protected DwGroupComposition oldGroupMembership;

	/**
	 * The cached value of the '{@link #getNewGroupMembership() <em>New Group Membership</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNewGroupMembership()
	 * @generated
	 * @ordered
	 */
	protected DwGroupComposition newGroupMembership;

	/**
	 * The default value of the '{@link #getOldValidUntil() <em>Old Valid Until</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOldValidUntil()
	 * @generated
	 * @ordered
	 */
	protected static final Date OLD_VALID_UNTIL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOldValidUntil() <em>Old Valid Until</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOldValidUntil()
	 * @generated
	 * @ordered
	 */
	protected Date oldValidUntil = OLD_VALID_UNTIL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getConsequentGroupOperation() <em>Consequent Group Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConsequentGroupOperation()
	 * @generated
	 * @ordered
	 */
	protected DwGroupDeleteOperation consequentGroupOperation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwFeatureDetachOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwOperationPackage.Literals.DW_FEATURE_DETACH_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroupComposition getOldGroupMembership() {
		if (oldGroupMembership != null && oldGroupMembership.eIsProxy()) {
			InternalEObject oldOldGroupMembership = (InternalEObject)oldGroupMembership;
			oldGroupMembership = (DwGroupComposition)eResolveProxy(oldOldGroupMembership);
			if (oldGroupMembership != oldOldGroupMembership) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwOperationPackage.DW_FEATURE_DETACH_OPERATION__OLD_GROUP_MEMBERSHIP, oldOldGroupMembership, oldGroupMembership));
			}
		}
		return oldGroupMembership;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwGroupComposition basicGetOldGroupMembership() {
		return oldGroupMembership;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOldGroupMembership(DwGroupComposition newOldGroupMembership) {
		DwGroupComposition oldOldGroupMembership = oldGroupMembership;
		oldGroupMembership = newOldGroupMembership;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_DETACH_OPERATION__OLD_GROUP_MEMBERSHIP, oldOldGroupMembership, oldGroupMembership));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroupComposition getNewGroupMembership() {
		if (newGroupMembership != null && newGroupMembership.eIsProxy()) {
			InternalEObject oldNewGroupMembership = (InternalEObject)newGroupMembership;
			newGroupMembership = (DwGroupComposition)eResolveProxy(oldNewGroupMembership);
			if (newGroupMembership != oldNewGroupMembership) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwOperationPackage.DW_FEATURE_DETACH_OPERATION__NEW_GROUP_MEMBERSHIP, oldNewGroupMembership, newGroupMembership));
			}
		}
		return newGroupMembership;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwGroupComposition basicGetNewGroupMembership() {
		return newGroupMembership;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNewGroupMembership(DwGroupComposition newNewGroupMembership) {
		DwGroupComposition oldNewGroupMembership = newGroupMembership;
		newGroupMembership = newNewGroupMembership;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_DETACH_OPERATION__NEW_GROUP_MEMBERSHIP, oldNewGroupMembership, newGroupMembership));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Date getOldValidUntil() {
		return oldValidUntil;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOldValidUntil(Date newOldValidUntil) {
		Date oldOldValidUntil = oldValidUntil;
		oldValidUntil = newOldValidUntil;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_DETACH_OPERATION__OLD_VALID_UNTIL, oldOldValidUntil, oldValidUntil));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroupDeleteOperation getConsequentGroupOperation() {
		return consequentGroupOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConsequentGroupOperation(DwGroupDeleteOperation newConsequentGroupOperation, NotificationChain msgs) {
		DwGroupDeleteOperation oldConsequentGroupOperation = consequentGroupOperation;
		consequentGroupOperation = newConsequentGroupOperation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_DETACH_OPERATION__CONSEQUENT_GROUP_OPERATION, oldConsequentGroupOperation, newConsequentGroupOperation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setConsequentGroupOperation(DwGroupDeleteOperation newConsequentGroupOperation) {
		if (newConsequentGroupOperation != consequentGroupOperation) {
			NotificationChain msgs = null;
			if (consequentGroupOperation != null)
				msgs = ((InternalEObject)consequentGroupOperation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DwOperationPackage.DW_FEATURE_DETACH_OPERATION__CONSEQUENT_GROUP_OPERATION, null, msgs);
			if (newConsequentGroupOperation != null)
				msgs = ((InternalEObject)newConsequentGroupOperation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DwOperationPackage.DW_FEATURE_DETACH_OPERATION__CONSEQUENT_GROUP_OPERATION, null, msgs);
			msgs = basicSetConsequentGroupOperation(newConsequentGroupOperation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_DETACH_OPERATION__CONSEQUENT_GROUP_OPERATION, newConsequentGroupOperation, newConsequentGroupOperation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_DETACH_OPERATION__CONSEQUENT_GROUP_OPERATION:
				return basicSetConsequentGroupOperation(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_DETACH_OPERATION__OLD_GROUP_MEMBERSHIP:
				if (resolve) return getOldGroupMembership();
				return basicGetOldGroupMembership();
			case DwOperationPackage.DW_FEATURE_DETACH_OPERATION__NEW_GROUP_MEMBERSHIP:
				if (resolve) return getNewGroupMembership();
				return basicGetNewGroupMembership();
			case DwOperationPackage.DW_FEATURE_DETACH_OPERATION__OLD_VALID_UNTIL:
				return getOldValidUntil();
			case DwOperationPackage.DW_FEATURE_DETACH_OPERATION__CONSEQUENT_GROUP_OPERATION:
				return getConsequentGroupOperation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_DETACH_OPERATION__OLD_GROUP_MEMBERSHIP:
				setOldGroupMembership((DwGroupComposition)newValue);
				return;
			case DwOperationPackage.DW_FEATURE_DETACH_OPERATION__NEW_GROUP_MEMBERSHIP:
				setNewGroupMembership((DwGroupComposition)newValue);
				return;
			case DwOperationPackage.DW_FEATURE_DETACH_OPERATION__OLD_VALID_UNTIL:
				setOldValidUntil((Date)newValue);
				return;
			case DwOperationPackage.DW_FEATURE_DETACH_OPERATION__CONSEQUENT_GROUP_OPERATION:
				setConsequentGroupOperation((DwGroupDeleteOperation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_DETACH_OPERATION__OLD_GROUP_MEMBERSHIP:
				setOldGroupMembership((DwGroupComposition)null);
				return;
			case DwOperationPackage.DW_FEATURE_DETACH_OPERATION__NEW_GROUP_MEMBERSHIP:
				setNewGroupMembership((DwGroupComposition)null);
				return;
			case DwOperationPackage.DW_FEATURE_DETACH_OPERATION__OLD_VALID_UNTIL:
				setOldValidUntil(OLD_VALID_UNTIL_EDEFAULT);
				return;
			case DwOperationPackage.DW_FEATURE_DETACH_OPERATION__CONSEQUENT_GROUP_OPERATION:
				setConsequentGroupOperation((DwGroupDeleteOperation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_DETACH_OPERATION__OLD_GROUP_MEMBERSHIP:
				return oldGroupMembership != null;
			case DwOperationPackage.DW_FEATURE_DETACH_OPERATION__NEW_GROUP_MEMBERSHIP:
				return newGroupMembership != null;
			case DwOperationPackage.DW_FEATURE_DETACH_OPERATION__OLD_VALID_UNTIL:
				return OLD_VALID_UNTIL_EDEFAULT == null ? oldValidUntil != null : !OLD_VALID_UNTIL_EDEFAULT.equals(oldValidUntil);
			case DwOperationPackage.DW_FEATURE_DETACH_OPERATION__CONSEQUENT_GROUP_OPERATION:
				return consequentGroupOperation != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (oldValidUntil: ");
		result.append(oldValidUntil);
		result.append(')');
		return result.toString();
	}

} //DwFeatureDetachOperationImpl
