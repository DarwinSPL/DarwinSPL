/**
 */
package de.darwinspl.feature.operation;

import de.darwinspl.feature.DwFeatureVersion;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Feature Version Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureVersionOperation#getVersion <em>Version</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureVersionOperation()
 * @model abstract="true"
 * @generated
 */
public interface DwFeatureVersionOperation extends DwFeatureOperation {
	/**
	 * Returns the value of the '<em><b>Version</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' reference.
	 * @see #setVersion(DwFeatureVersion)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureVersionOperation_Version()
	 * @model required="true"
	 * @generated
	 */
	DwFeatureVersion getVersion();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureVersionOperation#getVersion <em>Version</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' reference.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(DwFeatureVersion value);

} // DwFeatureVersionOperation
