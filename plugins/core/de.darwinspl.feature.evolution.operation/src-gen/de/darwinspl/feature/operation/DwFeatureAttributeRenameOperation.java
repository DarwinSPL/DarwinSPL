/**
 */
package de.darwinspl.feature.operation;

import de.darwinspl.feature.DwName;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Feature Attribute Rename Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureAttributeRenameOperation#getOldName <em>Old Name</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureAttributeRenameOperation#getNewName <em>New Name</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureAttributeRenameOperation()
 * @model
 * @generated
 */
public interface DwFeatureAttributeRenameOperation extends DwFeatureAttributeOperation {
	/**
	 * Returns the value of the '<em><b>Old Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Old Name</em>' reference.
	 * @see #setOldName(DwName)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureAttributeRenameOperation_OldName()
	 * @model required="true"
	 * @generated
	 */
	DwName getOldName();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureAttributeRenameOperation#getOldName <em>Old Name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Old Name</em>' reference.
	 * @see #getOldName()
	 * @generated
	 */
	void setOldName(DwName value);

	/**
	 * Returns the value of the '<em><b>New Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>New Name</em>' reference.
	 * @see #setNewName(DwName)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureAttributeRenameOperation_NewName()
	 * @model required="true"
	 * @generated
	 */
	DwName getNewName();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureAttributeRenameOperation#getNewName <em>New Name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>New Name</em>' reference.
	 * @see #getNewName()
	 * @generated
	 */
	void setNewName(DwName value);

} // DwFeatureAttributeRenameOperation
