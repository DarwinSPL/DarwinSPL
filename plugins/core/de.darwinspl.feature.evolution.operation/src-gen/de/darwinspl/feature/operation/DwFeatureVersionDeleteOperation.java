/**
 */
package de.darwinspl.feature.operation;

import java.util.Date;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Feature Version Delete Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureVersionDeleteOperation#getParent <em>Parent</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureVersionDeleteOperation#getOldValidUntil <em>Old Valid Until</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureVersionDeleteOperation()
 * @model
 * @generated
 */
public interface DwFeatureVersionDeleteOperation extends DwFeatureVersionOperation {
	/**
	 * Returns the value of the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' reference.
	 * @see #setParent(EObject)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureVersionDeleteOperation_Parent()
	 * @model required="true"
	 * @generated
	 */
	EObject getParent();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureVersionDeleteOperation#getParent <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(EObject value);

	/**
	 * Returns the value of the '<em><b>Old Valid Until</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Old Valid Until</em>' attribute.
	 * @see #setOldValidUntil(Date)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureVersionDeleteOperation_OldValidUntil()
	 * @model required="true"
	 * @generated
	 */
	Date getOldValidUntil();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureVersionDeleteOperation#getOldValidUntil <em>Old Valid Until</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Old Valid Until</em>' attribute.
	 * @see #getOldValidUntil()
	 * @generated
	 */
	void setOldValidUntil(Date value);

} // DwFeatureVersionDeleteOperation
