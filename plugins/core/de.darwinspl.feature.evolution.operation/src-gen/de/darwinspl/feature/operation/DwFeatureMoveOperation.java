/**
 */
package de.darwinspl.feature.operation;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Feature Move Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureMoveOperation#getDetachOperation <em>Detach Operation</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureMoveOperation#getAddOperation <em>Add Operation</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureMoveOperation()
 * @model
 * @generated
 */
public interface DwFeatureMoveOperation extends DwFeatureOperation {
	/**
	 * Returns the value of the '<em><b>Detach Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Detach Operation</em>' containment reference.
	 * @see #setDetachOperation(DwFeatureDetachOperation)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureMoveOperation_DetachOperation()
	 * @model containment="true" required="true"
	 * @generated
	 */
	DwFeatureDetachOperation getDetachOperation();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureMoveOperation#getDetachOperation <em>Detach Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Detach Operation</em>' containment reference.
	 * @see #getDetachOperation()
	 * @generated
	 */
	void setDetachOperation(DwFeatureDetachOperation value);

	/**
	 * Returns the value of the '<em><b>Add Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Add Operation</em>' containment reference.
	 * @see #setAddOperation(DwFeatureAddOperation)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureMoveOperation_AddOperation()
	 * @model containment="true" required="true"
	 * @generated
	 */
	DwFeatureAddOperation getAddOperation();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureMoveOperation#getAddOperation <em>Add Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Add Operation</em>' containment reference.
	 * @see #getAddOperation()
	 * @generated
	 */
	void setAddOperation(DwFeatureAddOperation value);

} // DwFeatureMoveOperation
