/**
 */
package de.darwinspl.feature.operation.impl;

import de.darwinspl.feature.contributor.DwContributor;
import de.darwinspl.feature.operation.DwEvolutionOperation;
import de.darwinspl.feature.operation.DwEvolutionOperationModel;
import de.darwinspl.feature.operation.DwOperationPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Evolution Operation Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwEvolutionOperationModelImpl#getEvolutionOperations <em>Evolution Operations</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwEvolutionOperationModelImpl#getContributors <em>Contributors</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwEvolutionOperationModelImpl extends MinimalEObjectImpl.Container implements DwEvolutionOperationModel {
	/**
	 * The cached value of the '{@link #getEvolutionOperations() <em>Evolution Operations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvolutionOperations()
	 * @generated
	 * @ordered
	 */
	protected EList<DwEvolutionOperation> evolutionOperations;
	/**
	 * The cached value of the '{@link #getContributors() <em>Contributors</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContributors()
	 * @generated
	 * @ordered
	 */
	protected EList<DwContributor> contributors;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwEvolutionOperationModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwOperationPackage.Literals.DW_EVOLUTION_OPERATION_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwEvolutionOperation> getEvolutionOperations() {
		if (evolutionOperations == null) {
			evolutionOperations = new EObjectContainmentEList<DwEvolutionOperation>(DwEvolutionOperation.class, this, DwOperationPackage.DW_EVOLUTION_OPERATION_MODEL__EVOLUTION_OPERATIONS);
		}
		return evolutionOperations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwContributor> getContributors() {
		if (contributors == null) {
			contributors = new EObjectContainmentEList<DwContributor>(DwContributor.class, this, DwOperationPackage.DW_EVOLUTION_OPERATION_MODEL__CONTRIBUTORS);
		}
		return contributors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwOperationPackage.DW_EVOLUTION_OPERATION_MODEL__EVOLUTION_OPERATIONS:
				return ((InternalEList<?>)getEvolutionOperations()).basicRemove(otherEnd, msgs);
			case DwOperationPackage.DW_EVOLUTION_OPERATION_MODEL__CONTRIBUTORS:
				return ((InternalEList<?>)getContributors()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwOperationPackage.DW_EVOLUTION_OPERATION_MODEL__EVOLUTION_OPERATIONS:
				return getEvolutionOperations();
			case DwOperationPackage.DW_EVOLUTION_OPERATION_MODEL__CONTRIBUTORS:
				return getContributors();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwOperationPackage.DW_EVOLUTION_OPERATION_MODEL__EVOLUTION_OPERATIONS:
				getEvolutionOperations().clear();
				getEvolutionOperations().addAll((Collection<? extends DwEvolutionOperation>)newValue);
				return;
			case DwOperationPackage.DW_EVOLUTION_OPERATION_MODEL__CONTRIBUTORS:
				getContributors().clear();
				getContributors().addAll((Collection<? extends DwContributor>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_EVOLUTION_OPERATION_MODEL__EVOLUTION_OPERATIONS:
				getEvolutionOperations().clear();
				return;
			case DwOperationPackage.DW_EVOLUTION_OPERATION_MODEL__CONTRIBUTORS:
				getContributors().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_EVOLUTION_OPERATION_MODEL__EVOLUTION_OPERATIONS:
				return evolutionOperations != null && !evolutionOperations.isEmpty();
			case DwOperationPackage.DW_EVOLUTION_OPERATION_MODEL__CONTRIBUTORS:
				return contributors != null && !contributors.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DwEvolutionOperationModelImpl
