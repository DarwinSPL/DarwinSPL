/**
 */
package de.darwinspl.feature.operation;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwParentFeatureChildGroupContainer;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Group Detach Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.DwGroupDetachOperation#getOldContainer <em>Old Container</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwGroupDetachOperation#getOldParent <em>Old Parent</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwGroupDetachOperation()
 * @model
 * @generated
 */
public interface DwGroupDetachOperation extends DwGroupOperation {
	/**
	 * Returns the value of the '<em><b>Old Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Old Container</em>' reference.
	 * @see #setOldContainer(DwParentFeatureChildGroupContainer)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwGroupDetachOperation_OldContainer()
	 * @model
	 * @generated
	 */
	DwParentFeatureChildGroupContainer getOldContainer();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwGroupDetachOperation#getOldContainer <em>Old Container</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Old Container</em>' reference.
	 * @see #getOldContainer()
	 * @generated
	 */
	void setOldContainer(DwParentFeatureChildGroupContainer value);

	/**
	 * Returns the value of the '<em><b>Old Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Old Parent</em>' reference.
	 * @see #setOldParent(DwFeature)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwGroupDetachOperation_OldParent()
	 * @model
	 * @generated
	 */
	DwFeature getOldParent();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwGroupDetachOperation#getOldParent <em>Old Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Old Parent</em>' reference.
	 * @see #getOldParent()
	 * @generated
	 */
	void setOldParent(DwFeature value);

} // DwGroupDetachOperation
