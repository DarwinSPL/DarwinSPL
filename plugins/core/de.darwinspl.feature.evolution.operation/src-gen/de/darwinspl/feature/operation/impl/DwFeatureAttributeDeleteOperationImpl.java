/**
 */
package de.darwinspl.feature.operation.impl;

import de.darwinspl.feature.operation.DwFeatureAttributeDeleteOperation;
import de.darwinspl.feature.operation.DwOperationPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Feature Attribute Delete Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DwFeatureAttributeDeleteOperationImpl extends DwFeatureAttributeOperationImpl implements DwFeatureAttributeDeleteOperation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwFeatureAttributeDeleteOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwOperationPackage.Literals.DW_FEATURE_ATTRIBUTE_DELETE_OPERATION;
	}

} //DwFeatureAttributeDeleteOperationImpl
