/**
 */
package de.darwinspl.feature.operation.util;

import de.darwinspl.feature.operation.*;

import de.darwinspl.temporal.DwElementWithId;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see de.darwinspl.feature.operation.DwOperationPackage
 * @generated
 */
public class DwOperationAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DwOperationPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwOperationAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = DwOperationPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwOperationSwitch<Adapter> modelSwitch =
		new DwOperationSwitch<Adapter>() {
			@Override
			public Adapter caseDwEvolutionOperationModel(DwEvolutionOperationModel object) {
				return createDwEvolutionOperationModelAdapter();
			}
			@Override
			public Adapter caseDwEvolutionOperation(DwEvolutionOperation object) {
				return createDwEvolutionOperationAdapter();
			}
			@Override
			public Adapter caseDwFeatureOperation(DwFeatureOperation object) {
				return createDwFeatureOperationAdapter();
			}
			@Override
			public Adapter caseDwFeatureCreateOperation(DwFeatureCreateOperation object) {
				return createDwFeatureCreateOperationAdapter();
			}
			@Override
			public Adapter caseDwFeatureAddOperation(DwFeatureAddOperation object) {
				return createDwFeatureAddOperationAdapter();
			}
			@Override
			public Adapter caseDwFeatureDeleteOperation(DwFeatureDeleteOperation object) {
				return createDwFeatureDeleteOperationAdapter();
			}
			@Override
			public Adapter caseDwFeatureDetachOperation(DwFeatureDetachOperation object) {
				return createDwFeatureDetachOperationAdapter();
			}
			@Override
			public Adapter caseDwFeatureMoveOperation(DwFeatureMoveOperation object) {
				return createDwFeatureMoveOperationAdapter();
			}
			@Override
			public Adapter caseDwFeatureRenameOperation(DwFeatureRenameOperation object) {
				return createDwFeatureRenameOperationAdapter();
			}
			@Override
			public Adapter caseDwFeatureTypeChangeOperation(DwFeatureTypeChangeOperation object) {
				return createDwFeatureTypeChangeOperationAdapter();
			}
			@Override
			public Adapter caseDwFeatureAttributeOperation(DwFeatureAttributeOperation object) {
				return createDwFeatureAttributeOperationAdapter();
			}
			@Override
			public Adapter caseDwFeatureAttributeCreateOperation(DwFeatureAttributeCreateOperation object) {
				return createDwFeatureAttributeCreateOperationAdapter();
			}
			@Override
			public Adapter caseDwFeatureAttributeDeleteOperation(DwFeatureAttributeDeleteOperation object) {
				return createDwFeatureAttributeDeleteOperationAdapter();
			}
			@Override
			public Adapter caseDwFeatureAttributeRenameOperation(DwFeatureAttributeRenameOperation object) {
				return createDwFeatureAttributeRenameOperationAdapter();
			}
			@Override
			public Adapter caseDwFeatureVersionOperation(DwFeatureVersionOperation object) {
				return createDwFeatureVersionOperationAdapter();
			}
			@Override
			public Adapter caseDwFeatureVersionCreateOperation(DwFeatureVersionCreateOperation object) {
				return createDwFeatureVersionCreateOperationAdapter();
			}
			@Override
			public Adapter caseDwFeatureVersionDeleteOperation(DwFeatureVersionDeleteOperation object) {
				return createDwFeatureVersionDeleteOperationAdapter();
			}
			@Override
			public Adapter caseDwFeatureVersionRenameOperation(DwFeatureVersionRenameOperation object) {
				return createDwFeatureVersionRenameOperationAdapter();
			}
			@Override
			public Adapter caseDwGroupOperation(DwGroupOperation object) {
				return createDwGroupOperationAdapter();
			}
			@Override
			public Adapter caseDwGroupCreateOperation(DwGroupCreateOperation object) {
				return createDwGroupCreateOperationAdapter();
			}
			@Override
			public Adapter caseDwGroupAddOperation(DwGroupAddOperation object) {
				return createDwGroupAddOperationAdapter();
			}
			@Override
			public Adapter caseDwGroupDeleteOperation(DwGroupDeleteOperation object) {
				return createDwGroupDeleteOperationAdapter();
			}
			@Override
			public Adapter caseDwGroupDetachOperation(DwGroupDetachOperation object) {
				return createDwGroupDetachOperationAdapter();
			}
			@Override
			public Adapter caseDwGroupMoveOperation(DwGroupMoveOperation object) {
				return createDwGroupMoveOperationAdapter();
			}
			@Override
			public Adapter caseDwGroupTypeChangeOperation(DwGroupTypeChangeOperation object) {
				return createDwGroupTypeChangeOperationAdapter();
			}
			@Override
			public Adapter caseDwElementWithId(DwElementWithId object) {
				return createDwElementWithIdAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.operation.DwEvolutionOperationModel <em>Dw Evolution Operation Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.operation.DwEvolutionOperationModel
	 * @generated
	 */
	public Adapter createDwEvolutionOperationModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.operation.DwEvolutionOperation <em>Dw Evolution Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.operation.DwEvolutionOperation
	 * @generated
	 */
	public Adapter createDwEvolutionOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.operation.DwFeatureOperation <em>Dw Feature Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.operation.DwFeatureOperation
	 * @generated
	 */
	public Adapter createDwFeatureOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.operation.DwFeatureCreateOperation <em>Dw Feature Create Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.operation.DwFeatureCreateOperation
	 * @generated
	 */
	public Adapter createDwFeatureCreateOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.operation.DwFeatureAddOperation <em>Dw Feature Add Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.operation.DwFeatureAddOperation
	 * @generated
	 */
	public Adapter createDwFeatureAddOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.operation.DwFeatureDeleteOperation <em>Dw Feature Delete Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.operation.DwFeatureDeleteOperation
	 * @generated
	 */
	public Adapter createDwFeatureDeleteOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.operation.DwFeatureDetachOperation <em>Dw Feature Detach Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.operation.DwFeatureDetachOperation
	 * @generated
	 */
	public Adapter createDwFeatureDetachOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.operation.DwFeatureMoveOperation <em>Dw Feature Move Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.operation.DwFeatureMoveOperation
	 * @generated
	 */
	public Adapter createDwFeatureMoveOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.operation.DwFeatureRenameOperation <em>Dw Feature Rename Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.operation.DwFeatureRenameOperation
	 * @generated
	 */
	public Adapter createDwFeatureRenameOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.operation.DwFeatureTypeChangeOperation <em>Dw Feature Type Change Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.operation.DwFeatureTypeChangeOperation
	 * @generated
	 */
	public Adapter createDwFeatureTypeChangeOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.operation.DwFeatureAttributeOperation <em>Dw Feature Attribute Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.operation.DwFeatureAttributeOperation
	 * @generated
	 */
	public Adapter createDwFeatureAttributeOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.operation.DwFeatureAttributeCreateOperation <em>Dw Feature Attribute Create Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.operation.DwFeatureAttributeCreateOperation
	 * @generated
	 */
	public Adapter createDwFeatureAttributeCreateOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.operation.DwFeatureAttributeDeleteOperation <em>Dw Feature Attribute Delete Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.operation.DwFeatureAttributeDeleteOperation
	 * @generated
	 */
	public Adapter createDwFeatureAttributeDeleteOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.operation.DwFeatureAttributeRenameOperation <em>Dw Feature Attribute Rename Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.operation.DwFeatureAttributeRenameOperation
	 * @generated
	 */
	public Adapter createDwFeatureAttributeRenameOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.operation.DwFeatureVersionOperation <em>Dw Feature Version Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.operation.DwFeatureVersionOperation
	 * @generated
	 */
	public Adapter createDwFeatureVersionOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.operation.DwFeatureVersionCreateOperation <em>Dw Feature Version Create Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.operation.DwFeatureVersionCreateOperation
	 * @generated
	 */
	public Adapter createDwFeatureVersionCreateOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.operation.DwFeatureVersionDeleteOperation <em>Dw Feature Version Delete Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.operation.DwFeatureVersionDeleteOperation
	 * @generated
	 */
	public Adapter createDwFeatureVersionDeleteOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.operation.DwFeatureVersionRenameOperation <em>Dw Feature Version Rename Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.operation.DwFeatureVersionRenameOperation
	 * @generated
	 */
	public Adapter createDwFeatureVersionRenameOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.operation.DwGroupOperation <em>Dw Group Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.operation.DwGroupOperation
	 * @generated
	 */
	public Adapter createDwGroupOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.operation.DwGroupCreateOperation <em>Dw Group Create Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.operation.DwGroupCreateOperation
	 * @generated
	 */
	public Adapter createDwGroupCreateOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.operation.DwGroupAddOperation <em>Dw Group Add Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.operation.DwGroupAddOperation
	 * @generated
	 */
	public Adapter createDwGroupAddOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.operation.DwGroupDeleteOperation <em>Dw Group Delete Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.operation.DwGroupDeleteOperation
	 * @generated
	 */
	public Adapter createDwGroupDeleteOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.operation.DwGroupDetachOperation <em>Dw Group Detach Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.operation.DwGroupDetachOperation
	 * @generated
	 */
	public Adapter createDwGroupDetachOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.operation.DwGroupMoveOperation <em>Dw Group Move Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.operation.DwGroupMoveOperation
	 * @generated
	 */
	public Adapter createDwGroupMoveOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.operation.DwGroupTypeChangeOperation <em>Dw Group Type Change Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.operation.DwGroupTypeChangeOperation
	 * @generated
	 */
	public Adapter createDwGroupTypeChangeOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.temporal.DwElementWithId <em>Dw Element With Id</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.temporal.DwElementWithId
	 * @generated
	 */
	public Adapter createDwElementWithIdAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //DwOperationAdapterFactory
