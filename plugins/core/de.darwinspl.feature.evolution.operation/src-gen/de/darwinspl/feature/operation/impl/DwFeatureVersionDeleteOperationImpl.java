/**
 */
package de.darwinspl.feature.operation.impl;

import de.darwinspl.feature.operation.DwFeatureVersionDeleteOperation;
import de.darwinspl.feature.operation.DwOperationPackage;

import java.util.Date;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Feature Version Delete Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureVersionDeleteOperationImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureVersionDeleteOperationImpl#getOldValidUntil <em>Old Valid Until</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwFeatureVersionDeleteOperationImpl extends DwFeatureVersionOperationImpl implements DwFeatureVersionDeleteOperation {
	/**
	 * The cached value of the '{@link #getParent() <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParent()
	 * @generated
	 * @ordered
	 */
	protected EObject parent;

	/**
	 * The default value of the '{@link #getOldValidUntil() <em>Old Valid Until</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOldValidUntil()
	 * @generated
	 * @ordered
	 */
	protected static final Date OLD_VALID_UNTIL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOldValidUntil() <em>Old Valid Until</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOldValidUntil()
	 * @generated
	 * @ordered
	 */
	protected Date oldValidUntil = OLD_VALID_UNTIL_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwFeatureVersionDeleteOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwOperationPackage.Literals.DW_FEATURE_VERSION_DELETE_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject getParent() {
		if (parent != null && parent.eIsProxy()) {
			InternalEObject oldParent = (InternalEObject)parent;
			parent = eResolveProxy(oldParent);
			if (parent != oldParent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwOperationPackage.DW_FEATURE_VERSION_DELETE_OPERATION__PARENT, oldParent, parent));
			}
		}
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetParent() {
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setParent(EObject newParent) {
		EObject oldParent = parent;
		parent = newParent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_VERSION_DELETE_OPERATION__PARENT, oldParent, parent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Date getOldValidUntil() {
		return oldValidUntil;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOldValidUntil(Date newOldValidUntil) {
		Date oldOldValidUntil = oldValidUntil;
		oldValidUntil = newOldValidUntil;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_VERSION_DELETE_OPERATION__OLD_VALID_UNTIL, oldOldValidUntil, oldValidUntil));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_VERSION_DELETE_OPERATION__PARENT:
				if (resolve) return getParent();
				return basicGetParent();
			case DwOperationPackage.DW_FEATURE_VERSION_DELETE_OPERATION__OLD_VALID_UNTIL:
				return getOldValidUntil();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_VERSION_DELETE_OPERATION__PARENT:
				setParent((EObject)newValue);
				return;
			case DwOperationPackage.DW_FEATURE_VERSION_DELETE_OPERATION__OLD_VALID_UNTIL:
				setOldValidUntil((Date)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_VERSION_DELETE_OPERATION__PARENT:
				setParent((EObject)null);
				return;
			case DwOperationPackage.DW_FEATURE_VERSION_DELETE_OPERATION__OLD_VALID_UNTIL:
				setOldValidUntil(OLD_VALID_UNTIL_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_VERSION_DELETE_OPERATION__PARENT:
				return parent != null;
			case DwOperationPackage.DW_FEATURE_VERSION_DELETE_OPERATION__OLD_VALID_UNTIL:
				return OLD_VALID_UNTIL_EDEFAULT == null ? oldValidUntil != null : !OLD_VALID_UNTIL_EDEFAULT.equals(oldValidUntil);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (oldValidUntil: ");
		result.append(oldValidUntil);
		result.append(')');
		return result.toString();
	}

} //DwFeatureVersionDeleteOperationImpl
