/**
 */
package de.darwinspl.feature.operation.impl;

import de.darwinspl.feature.operation.DwFeatureAddOperation;
import de.darwinspl.feature.operation.DwFeatureCreateOperation;
import de.darwinspl.feature.operation.DwOperationPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Feature Create Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureCreateOperationImpl#getFeatureAddOperation <em>Feature Add Operation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwFeatureCreateOperationImpl extends DwFeatureOperationImpl implements DwFeatureCreateOperation {
	/**
	 * The cached value of the '{@link #getFeatureAddOperation() <em>Feature Add Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatureAddOperation()
	 * @generated
	 * @ordered
	 */
	protected DwFeatureAddOperation featureAddOperation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwFeatureCreateOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwOperationPackage.Literals.DW_FEATURE_CREATE_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureAddOperation getFeatureAddOperation() {
		return featureAddOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFeatureAddOperation(DwFeatureAddOperation newFeatureAddOperation, NotificationChain msgs) {
		DwFeatureAddOperation oldFeatureAddOperation = featureAddOperation;
		featureAddOperation = newFeatureAddOperation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_CREATE_OPERATION__FEATURE_ADD_OPERATION, oldFeatureAddOperation, newFeatureAddOperation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFeatureAddOperation(DwFeatureAddOperation newFeatureAddOperation) {
		if (newFeatureAddOperation != featureAddOperation) {
			NotificationChain msgs = null;
			if (featureAddOperation != null)
				msgs = ((InternalEObject)featureAddOperation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DwOperationPackage.DW_FEATURE_CREATE_OPERATION__FEATURE_ADD_OPERATION, null, msgs);
			if (newFeatureAddOperation != null)
				msgs = ((InternalEObject)newFeatureAddOperation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DwOperationPackage.DW_FEATURE_CREATE_OPERATION__FEATURE_ADD_OPERATION, null, msgs);
			msgs = basicSetFeatureAddOperation(newFeatureAddOperation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_CREATE_OPERATION__FEATURE_ADD_OPERATION, newFeatureAddOperation, newFeatureAddOperation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_CREATE_OPERATION__FEATURE_ADD_OPERATION:
				return basicSetFeatureAddOperation(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_CREATE_OPERATION__FEATURE_ADD_OPERATION:
				return getFeatureAddOperation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_CREATE_OPERATION__FEATURE_ADD_OPERATION:
				setFeatureAddOperation((DwFeatureAddOperation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_CREATE_OPERATION__FEATURE_ADD_OPERATION:
				setFeatureAddOperation((DwFeatureAddOperation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_CREATE_OPERATION__FEATURE_ADD_OPERATION:
				return featureAddOperation != null;
		}
		return super.eIsSet(featureID);
	}

} //DwFeatureCreateOperationImpl
