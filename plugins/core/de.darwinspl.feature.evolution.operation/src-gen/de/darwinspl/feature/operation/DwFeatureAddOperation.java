/**
 */
package de.darwinspl.feature.operation;

import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupComposition;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Feature Add Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureAddOperation#getParent <em>Parent</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureAddOperation#isForceCreateParentGroup <em>Force Create Parent Group</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureAddOperation#getOldGroupComposition <em>Old Group Composition</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureAddOperation#getNewGroupComposition <em>New Group Composition</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureAddOperation#getGroupCreateOperation <em>Group Create Operation</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureAddOperation#getGroupTypeChangeOperation <em>Group Type Change Operation</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureAddOperation#getGroupToAddTo <em>Group To Add To</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureAddOperation#getConsequentFeatureTypeChangeOperation <em>Consequent Feature Type Change Operation</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureAddOperation()
 * @model
 * @generated
 */
public interface DwFeatureAddOperation extends DwFeatureOperation {
	/**
	 * Returns the value of the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' reference.
	 * @see #setParent(EObject)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureAddOperation_Parent()
	 * @model required="true"
	 * @generated
	 */
	EObject getParent();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureAddOperation#getParent <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(EObject value);

	/**
	 * Returns the value of the '<em><b>Force Create Parent Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Force Create Parent Group</em>' attribute.
	 * @see #setForceCreateParentGroup(boolean)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureAddOperation_ForceCreateParentGroup()
	 * @model required="true"
	 * @generated
	 */
	boolean isForceCreateParentGroup();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureAddOperation#isForceCreateParentGroup <em>Force Create Parent Group</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Force Create Parent Group</em>' attribute.
	 * @see #isForceCreateParentGroup()
	 * @generated
	 */
	void setForceCreateParentGroup(boolean value);

	/**
	 * Returns the value of the '<em><b>Old Group Composition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Old Group Composition</em>' reference.
	 * @see #setOldGroupComposition(DwGroupComposition)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureAddOperation_OldGroupComposition()
	 * @model
	 * @generated
	 */
	DwGroupComposition getOldGroupComposition();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureAddOperation#getOldGroupComposition <em>Old Group Composition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Old Group Composition</em>' reference.
	 * @see #getOldGroupComposition()
	 * @generated
	 */
	void setOldGroupComposition(DwGroupComposition value);

	/**
	 * Returns the value of the '<em><b>New Group Composition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>New Group Composition</em>' reference.
	 * @see #setNewGroupComposition(DwGroupComposition)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureAddOperation_NewGroupComposition()
	 * @model
	 * @generated
	 */
	DwGroupComposition getNewGroupComposition();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureAddOperation#getNewGroupComposition <em>New Group Composition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>New Group Composition</em>' reference.
	 * @see #getNewGroupComposition()
	 * @generated
	 */
	void setNewGroupComposition(DwGroupComposition value);

	/**
	 * Returns the value of the '<em><b>Group Create Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group Create Operation</em>' containment reference.
	 * @see #setGroupCreateOperation(DwGroupCreateOperation)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureAddOperation_GroupCreateOperation()
	 * @model containment="true"
	 * @generated
	 */
	DwGroupCreateOperation getGroupCreateOperation();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureAddOperation#getGroupCreateOperation <em>Group Create Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group Create Operation</em>' containment reference.
	 * @see #getGroupCreateOperation()
	 * @generated
	 */
	void setGroupCreateOperation(DwGroupCreateOperation value);

	/**
	 * Returns the value of the '<em><b>Group Type Change Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group Type Change Operation</em>' containment reference.
	 * @see #setGroupTypeChangeOperation(DwGroupTypeChangeOperation)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureAddOperation_GroupTypeChangeOperation()
	 * @model containment="true"
	 * @generated
	 */
	DwGroupTypeChangeOperation getGroupTypeChangeOperation();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureAddOperation#getGroupTypeChangeOperation <em>Group Type Change Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group Type Change Operation</em>' containment reference.
	 * @see #getGroupTypeChangeOperation()
	 * @generated
	 */
	void setGroupTypeChangeOperation(DwGroupTypeChangeOperation value);

	/**
	 * Returns the value of the '<em><b>Group To Add To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group To Add To</em>' reference.
	 * @see #setGroupToAddTo(DwGroup)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureAddOperation_GroupToAddTo()
	 * @model required="true"
	 * @generated
	 */
	DwGroup getGroupToAddTo();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureAddOperation#getGroupToAddTo <em>Group To Add To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group To Add To</em>' reference.
	 * @see #getGroupToAddTo()
	 * @generated
	 */
	void setGroupToAddTo(DwGroup value);

	/**
	 * Returns the value of the '<em><b>Consequent Feature Type Change Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Consequent Feature Type Change Operation</em>' containment reference.
	 * @see #setConsequentFeatureTypeChangeOperation(DwFeatureTypeChangeOperation)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureAddOperation_ConsequentFeatureTypeChangeOperation()
	 * @model containment="true"
	 * @generated
	 */
	DwFeatureTypeChangeOperation getConsequentFeatureTypeChangeOperation();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureAddOperation#getConsequentFeatureTypeChangeOperation <em>Consequent Feature Type Change Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Consequent Feature Type Change Operation</em>' containment reference.
	 * @see #getConsequentFeatureTypeChangeOperation()
	 * @generated
	 */
	void setConsequentFeatureTypeChangeOperation(DwFeatureTypeChangeOperation value);

} // DwFeatureAddOperation
