/**
 */
package de.darwinspl.feature.operation;

import de.darwinspl.feature.DwFeatureAttribute;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Feature Attribute Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureAttributeOperation#getAttribute <em>Attribute</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureAttributeOperation()
 * @model abstract="true"
 * @generated
 */
public interface DwFeatureAttributeOperation extends DwFeatureOperation {
	/**
	 * Returns the value of the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute</em>' reference.
	 * @see #setAttribute(DwFeatureAttribute)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureAttributeOperation_Attribute()
	 * @model required="true"
	 * @generated
	 */
	DwFeatureAttribute getAttribute();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureAttributeOperation#getAttribute <em>Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute</em>' reference.
	 * @see #getAttribute()
	 * @generated
	 */
	void setAttribute(DwFeatureAttribute value);

} // DwFeatureAttributeOperation
