/**
 */
package de.darwinspl.feature.operation.impl;

import de.darwinspl.feature.operation.DwFeatureVersionRenameOperation;
import de.darwinspl.feature.operation.DwOperationPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Feature Version Rename Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureVersionRenameOperationImpl#getOldNumber <em>Old Number</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureVersionRenameOperationImpl#getNewNumber <em>New Number</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwFeatureVersionRenameOperationImpl extends DwFeatureVersionOperationImpl implements DwFeatureVersionRenameOperation {
	/**
	 * The default value of the '{@link #getOldNumber() <em>Old Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOldNumber()
	 * @generated
	 * @ordered
	 */
	protected static final String OLD_NUMBER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOldNumber() <em>Old Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOldNumber()
	 * @generated
	 * @ordered
	 */
	protected String oldNumber = OLD_NUMBER_EDEFAULT;

	/**
	 * The default value of the '{@link #getNewNumber() <em>New Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNewNumber()
	 * @generated
	 * @ordered
	 */
	protected static final String NEW_NUMBER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNewNumber() <em>New Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNewNumber()
	 * @generated
	 * @ordered
	 */
	protected String newNumber = NEW_NUMBER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwFeatureVersionRenameOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwOperationPackage.Literals.DW_FEATURE_VERSION_RENAME_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOldNumber() {
		return oldNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOldNumber(String newOldNumber) {
		String oldOldNumber = oldNumber;
		oldNumber = newOldNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_VERSION_RENAME_OPERATION__OLD_NUMBER, oldOldNumber, oldNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getNewNumber() {
		return newNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNewNumber(String newNewNumber) {
		String oldNewNumber = newNumber;
		newNumber = newNewNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_VERSION_RENAME_OPERATION__NEW_NUMBER, oldNewNumber, newNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_VERSION_RENAME_OPERATION__OLD_NUMBER:
				return getOldNumber();
			case DwOperationPackage.DW_FEATURE_VERSION_RENAME_OPERATION__NEW_NUMBER:
				return getNewNumber();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_VERSION_RENAME_OPERATION__OLD_NUMBER:
				setOldNumber((String)newValue);
				return;
			case DwOperationPackage.DW_FEATURE_VERSION_RENAME_OPERATION__NEW_NUMBER:
				setNewNumber((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_VERSION_RENAME_OPERATION__OLD_NUMBER:
				setOldNumber(OLD_NUMBER_EDEFAULT);
				return;
			case DwOperationPackage.DW_FEATURE_VERSION_RENAME_OPERATION__NEW_NUMBER:
				setNewNumber(NEW_NUMBER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_VERSION_RENAME_OPERATION__OLD_NUMBER:
				return OLD_NUMBER_EDEFAULT == null ? oldNumber != null : !OLD_NUMBER_EDEFAULT.equals(oldNumber);
			case DwOperationPackage.DW_FEATURE_VERSION_RENAME_OPERATION__NEW_NUMBER:
				return NEW_NUMBER_EDEFAULT == null ? newNumber != null : !NEW_NUMBER_EDEFAULT.equals(newNumber);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (oldNumber: ");
		result.append(oldNumber);
		result.append(", newNumber: ");
		result.append(newNumber);
		result.append(')');
		return result.toString();
	}

} //DwFeatureVersionRenameOperationImpl
