/**
 */
package de.darwinspl.feature.operation;

import de.darwinspl.feature.DwName;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Feature Rename Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureRenameOperation#getNewName <em>New Name</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureRenameOperation#getNewNameString <em>New Name String</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureRenameOperation#getOldName <em>Old Name</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureRenameOperation#getOldNameString <em>Old Name String</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureRenameOperation()
 * @model
 * @generated
 */
public interface DwFeatureRenameOperation extends DwFeatureOperation {
	/**
	 * Returns the value of the '<em><b>New Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>New Name</em>' reference.
	 * @see #setNewName(DwName)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureRenameOperation_NewName()
	 * @model
	 * @generated
	 */
	DwName getNewName();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureRenameOperation#getNewName <em>New Name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>New Name</em>' reference.
	 * @see #getNewName()
	 * @generated
	 */
	void setNewName(DwName value);

	/**
	 * Returns the value of the '<em><b>New Name String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>New Name String</em>' attribute.
	 * @see #setNewNameString(String)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureRenameOperation_NewNameString()
	 * @model required="true"
	 * @generated
	 */
	String getNewNameString();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureRenameOperation#getNewNameString <em>New Name String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>New Name String</em>' attribute.
	 * @see #getNewNameString()
	 * @generated
	 */
	void setNewNameString(String value);

	/**
	 * Returns the value of the '<em><b>Old Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Old Name</em>' reference.
	 * @see #setOldName(DwName)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureRenameOperation_OldName()
	 * @model
	 * @generated
	 */
	DwName getOldName();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureRenameOperation#getOldName <em>Old Name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Old Name</em>' reference.
	 * @see #getOldName()
	 * @generated
	 */
	void setOldName(DwName value);

	/**
	 * Returns the value of the '<em><b>Old Name String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Old Name String</em>' attribute.
	 * @see #setOldNameString(String)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureRenameOperation_OldNameString()
	 * @model required="true"
	 * @generated
	 */
	String getOldNameString();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureRenameOperation#getOldNameString <em>Old Name String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Old Name String</em>' attribute.
	 * @see #getOldNameString()
	 * @generated
	 */
	void setOldNameString(String value);

} // DwFeatureRenameOperation
