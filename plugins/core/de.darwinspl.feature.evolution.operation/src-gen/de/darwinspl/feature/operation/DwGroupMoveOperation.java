/**
 */
package de.darwinspl.feature.operation;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Group Move Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.DwGroupMoveOperation#getGroupDetachOperation <em>Group Detach Operation</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwGroupMoveOperation#getGroupAddOperation <em>Group Add Operation</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwGroupMoveOperation()
 * @model
 * @generated
 */
public interface DwGroupMoveOperation extends DwGroupOperation {
	/**
	 * Returns the value of the '<em><b>Group Detach Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group Detach Operation</em>' containment reference.
	 * @see #setGroupDetachOperation(DwGroupDetachOperation)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwGroupMoveOperation_GroupDetachOperation()
	 * @model containment="true" required="true"
	 * @generated
	 */
	DwGroupDetachOperation getGroupDetachOperation();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwGroupMoveOperation#getGroupDetachOperation <em>Group Detach Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group Detach Operation</em>' containment reference.
	 * @see #getGroupDetachOperation()
	 * @generated
	 */
	void setGroupDetachOperation(DwGroupDetachOperation value);

	/**
	 * Returns the value of the '<em><b>Group Add Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group Add Operation</em>' containment reference.
	 * @see #setGroupAddOperation(DwGroupAddOperation)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwGroupMoveOperation_GroupAddOperation()
	 * @model containment="true" required="true"
	 * @generated
	 */
	DwGroupAddOperation getGroupAddOperation();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwGroupMoveOperation#getGroupAddOperation <em>Group Add Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group Add Operation</em>' containment reference.
	 * @see #getGroupAddOperation()
	 * @generated
	 */
	void setGroupAddOperation(DwGroupAddOperation value);

} // DwGroupMoveOperation
