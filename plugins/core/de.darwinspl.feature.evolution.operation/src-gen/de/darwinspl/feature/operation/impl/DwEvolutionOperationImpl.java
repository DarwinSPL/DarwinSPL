/**
 */
package de.darwinspl.feature.operation.impl;

import de.darwinspl.feature.DwTemporalFeatureModel;

import de.darwinspl.feature.contributor.DwContributor;

import de.darwinspl.feature.operation.DwEvolutionOperation;
import de.darwinspl.feature.operation.DwOperationPackage;

import de.darwinspl.temporal.impl.DwElementWithIdImpl;
import java.util.Date;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Evolution Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwEvolutionOperationImpl#getOperationDate <em>Operation Date</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwEvolutionOperationImpl#getCommandStackIndex <em>Command Stack Index</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwEvolutionOperationImpl#getFeatureModel <em>Feature Model</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwEvolutionOperationImpl#getContributor <em>Contributor</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwEvolutionOperationImpl#isIntermediateOperation <em>Intermediate Operation</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class DwEvolutionOperationImpl extends DwElementWithIdImpl implements DwEvolutionOperation {
	/**
	 * The default value of the '{@link #getOperationDate() <em>Operation Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationDate()
	 * @generated
	 * @ordered
	 */
	protected static final Date OPERATION_DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOperationDate() <em>Operation Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationDate()
	 * @generated
	 * @ordered
	 */
	protected Date operationDate = OPERATION_DATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getCommandStackIndex() <em>Command Stack Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommandStackIndex()
	 * @generated
	 * @ordered
	 */
	protected static final int COMMAND_STACK_INDEX_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCommandStackIndex() <em>Command Stack Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommandStackIndex()
	 * @generated
	 * @ordered
	 */
	protected int commandStackIndex = COMMAND_STACK_INDEX_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFeatureModel() <em>Feature Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatureModel()
	 * @generated
	 * @ordered
	 */
	protected DwTemporalFeatureModel featureModel;

	/**
	 * The cached value of the '{@link #getContributor() <em>Contributor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContributor()
	 * @generated
	 * @ordered
	 */
	protected DwContributor contributor;

	/**
	 * The default value of the '{@link #isIntermediateOperation() <em>Intermediate Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIntermediateOperation()
	 * @generated
	 * @ordered
	 */
	protected static final boolean INTERMEDIATE_OPERATION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIntermediateOperation() <em>Intermediate Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIntermediateOperation()
	 * @generated
	 * @ordered
	 */
	protected boolean intermediateOperation = INTERMEDIATE_OPERATION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwEvolutionOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwOperationPackage.Literals.DW_EVOLUTION_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Date getOperationDate() {
		return operationDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOperationDate(Date newOperationDate) {
		Date oldOperationDate = operationDate;
		operationDate = newOperationDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_EVOLUTION_OPERATION__OPERATION_DATE, oldOperationDate, operationDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getCommandStackIndex() {
		return commandStackIndex;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCommandStackIndex(int newCommandStackIndex) {
		int oldCommandStackIndex = commandStackIndex;
		commandStackIndex = newCommandStackIndex;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_EVOLUTION_OPERATION__COMMAND_STACK_INDEX, oldCommandStackIndex, commandStackIndex));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwTemporalFeatureModel getFeatureModel() {
		if (featureModel != null && featureModel.eIsProxy()) {
			InternalEObject oldFeatureModel = (InternalEObject)featureModel;
			featureModel = (DwTemporalFeatureModel)eResolveProxy(oldFeatureModel);
			if (featureModel != oldFeatureModel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwOperationPackage.DW_EVOLUTION_OPERATION__FEATURE_MODEL, oldFeatureModel, featureModel));
			}
		}
		return featureModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwTemporalFeatureModel basicGetFeatureModel() {
		return featureModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFeatureModel(DwTemporalFeatureModel newFeatureModel) {
		DwTemporalFeatureModel oldFeatureModel = featureModel;
		featureModel = newFeatureModel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_EVOLUTION_OPERATION__FEATURE_MODEL, oldFeatureModel, featureModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwContributor getContributor() {
		if (contributor != null && contributor.eIsProxy()) {
			InternalEObject oldContributor = (InternalEObject)contributor;
			contributor = (DwContributor)eResolveProxy(oldContributor);
			if (contributor != oldContributor) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwOperationPackage.DW_EVOLUTION_OPERATION__CONTRIBUTOR, oldContributor, contributor));
			}
		}
		return contributor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwContributor basicGetContributor() {
		return contributor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setContributor(DwContributor newContributor) {
		DwContributor oldContributor = contributor;
		contributor = newContributor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_EVOLUTION_OPERATION__CONTRIBUTOR, oldContributor, contributor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isIntermediateOperation() {
		return intermediateOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIntermediateOperation(boolean newIntermediateOperation) {
		boolean oldIntermediateOperation = intermediateOperation;
		intermediateOperation = newIntermediateOperation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_EVOLUTION_OPERATION__INTERMEDIATE_OPERATION, oldIntermediateOperation, intermediateOperation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwOperationPackage.DW_EVOLUTION_OPERATION__OPERATION_DATE:
				return getOperationDate();
			case DwOperationPackage.DW_EVOLUTION_OPERATION__COMMAND_STACK_INDEX:
				return getCommandStackIndex();
			case DwOperationPackage.DW_EVOLUTION_OPERATION__FEATURE_MODEL:
				if (resolve) return getFeatureModel();
				return basicGetFeatureModel();
			case DwOperationPackage.DW_EVOLUTION_OPERATION__CONTRIBUTOR:
				if (resolve) return getContributor();
				return basicGetContributor();
			case DwOperationPackage.DW_EVOLUTION_OPERATION__INTERMEDIATE_OPERATION:
				return isIntermediateOperation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwOperationPackage.DW_EVOLUTION_OPERATION__OPERATION_DATE:
				setOperationDate((Date)newValue);
				return;
			case DwOperationPackage.DW_EVOLUTION_OPERATION__COMMAND_STACK_INDEX:
				setCommandStackIndex((Integer)newValue);
				return;
			case DwOperationPackage.DW_EVOLUTION_OPERATION__FEATURE_MODEL:
				setFeatureModel((DwTemporalFeatureModel)newValue);
				return;
			case DwOperationPackage.DW_EVOLUTION_OPERATION__CONTRIBUTOR:
				setContributor((DwContributor)newValue);
				return;
			case DwOperationPackage.DW_EVOLUTION_OPERATION__INTERMEDIATE_OPERATION:
				setIntermediateOperation((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_EVOLUTION_OPERATION__OPERATION_DATE:
				setOperationDate(OPERATION_DATE_EDEFAULT);
				return;
			case DwOperationPackage.DW_EVOLUTION_OPERATION__COMMAND_STACK_INDEX:
				setCommandStackIndex(COMMAND_STACK_INDEX_EDEFAULT);
				return;
			case DwOperationPackage.DW_EVOLUTION_OPERATION__FEATURE_MODEL:
				setFeatureModel((DwTemporalFeatureModel)null);
				return;
			case DwOperationPackage.DW_EVOLUTION_OPERATION__CONTRIBUTOR:
				setContributor((DwContributor)null);
				return;
			case DwOperationPackage.DW_EVOLUTION_OPERATION__INTERMEDIATE_OPERATION:
				setIntermediateOperation(INTERMEDIATE_OPERATION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_EVOLUTION_OPERATION__OPERATION_DATE:
				return OPERATION_DATE_EDEFAULT == null ? operationDate != null : !OPERATION_DATE_EDEFAULT.equals(operationDate);
			case DwOperationPackage.DW_EVOLUTION_OPERATION__COMMAND_STACK_INDEX:
				return commandStackIndex != COMMAND_STACK_INDEX_EDEFAULT;
			case DwOperationPackage.DW_EVOLUTION_OPERATION__FEATURE_MODEL:
				return featureModel != null;
			case DwOperationPackage.DW_EVOLUTION_OPERATION__CONTRIBUTOR:
				return contributor != null;
			case DwOperationPackage.DW_EVOLUTION_OPERATION__INTERMEDIATE_OPERATION:
				return intermediateOperation != INTERMEDIATE_OPERATION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (operationDate: ");
		result.append(operationDate);
		result.append(", commandStackIndex: ");
		result.append(commandStackIndex);
		result.append(", intermediateOperation: ");
		result.append(intermediateOperation);
		result.append(')');
		return result.toString();
	}

} //DwEvolutionOperationImpl
