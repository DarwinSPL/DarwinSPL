/**
 */
package de.darwinspl.feature.operation;

import de.darwinspl.temporal.DwTemporalModelPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.darwinspl.feature.operation.DwOperationFactory
 * @model kind="package"
 * @generated
 */
public interface DwOperationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "operation";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.darwinspl.de/feature/operation/2.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "operation";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DwOperationPackage eINSTANCE = de.darwinspl.feature.operation.impl.DwOperationPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.operation.impl.DwEvolutionOperationModelImpl <em>Dw Evolution Operation Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.operation.impl.DwEvolutionOperationModelImpl
	 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwEvolutionOperationModel()
	 * @generated
	 */
	int DW_EVOLUTION_OPERATION_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Evolution Operations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_EVOLUTION_OPERATION_MODEL__EVOLUTION_OPERATIONS = 0;

	/**
	 * The feature id for the '<em><b>Contributors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_EVOLUTION_OPERATION_MODEL__CONTRIBUTORS = 1;

	/**
	 * The number of structural features of the '<em>Dw Evolution Operation Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_EVOLUTION_OPERATION_MODEL_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Dw Evolution Operation Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_EVOLUTION_OPERATION_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.operation.impl.DwEvolutionOperationImpl <em>Dw Evolution Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.operation.impl.DwEvolutionOperationImpl
	 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwEvolutionOperation()
	 * @generated
	 */
	int DW_EVOLUTION_OPERATION = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_EVOLUTION_OPERATION__ID = DwTemporalModelPackage.DW_ELEMENT_WITH_ID__ID;

	/**
	 * The feature id for the '<em><b>Operation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_EVOLUTION_OPERATION__OPERATION_DATE = DwTemporalModelPackage.DW_ELEMENT_WITH_ID_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Command Stack Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_EVOLUTION_OPERATION__COMMAND_STACK_INDEX = DwTemporalModelPackage.DW_ELEMENT_WITH_ID_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_EVOLUTION_OPERATION__FEATURE_MODEL = DwTemporalModelPackage.DW_ELEMENT_WITH_ID_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Contributor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_EVOLUTION_OPERATION__CONTRIBUTOR = DwTemporalModelPackage.DW_ELEMENT_WITH_ID_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Intermediate Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_EVOLUTION_OPERATION__INTERMEDIATE_OPERATION = DwTemporalModelPackage.DW_ELEMENT_WITH_ID_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Dw Evolution Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_EVOLUTION_OPERATION_FEATURE_COUNT = DwTemporalModelPackage.DW_ELEMENT_WITH_ID_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_EVOLUTION_OPERATION___CREATE_ID = DwTemporalModelPackage.DW_ELEMENT_WITH_ID___CREATE_ID;

	/**
	 * The number of operations of the '<em>Dw Evolution Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_EVOLUTION_OPERATION_OPERATION_COUNT = DwTemporalModelPackage.DW_ELEMENT_WITH_ID_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.operation.impl.DwFeatureOperationImpl <em>Dw Feature Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.operation.impl.DwFeatureOperationImpl
	 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureOperation()
	 * @generated
	 */
	int DW_FEATURE_OPERATION = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_OPERATION__ID = DW_EVOLUTION_OPERATION__ID;

	/**
	 * The feature id for the '<em><b>Operation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_OPERATION__OPERATION_DATE = DW_EVOLUTION_OPERATION__OPERATION_DATE;

	/**
	 * The feature id for the '<em><b>Command Stack Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_OPERATION__COMMAND_STACK_INDEX = DW_EVOLUTION_OPERATION__COMMAND_STACK_INDEX;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_OPERATION__FEATURE_MODEL = DW_EVOLUTION_OPERATION__FEATURE_MODEL;

	/**
	 * The feature id for the '<em><b>Contributor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_OPERATION__CONTRIBUTOR = DW_EVOLUTION_OPERATION__CONTRIBUTOR;

	/**
	 * The feature id for the '<em><b>Intermediate Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_OPERATION__INTERMEDIATE_OPERATION = DW_EVOLUTION_OPERATION__INTERMEDIATE_OPERATION;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_OPERATION__FEATURE = DW_EVOLUTION_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Dw Feature Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_OPERATION_FEATURE_COUNT = DW_EVOLUTION_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_OPERATION___CREATE_ID = DW_EVOLUTION_OPERATION___CREATE_ID;

	/**
	 * The number of operations of the '<em>Dw Feature Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_OPERATION_OPERATION_COUNT = DW_EVOLUTION_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.operation.impl.DwFeatureCreateOperationImpl <em>Dw Feature Create Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.operation.impl.DwFeatureCreateOperationImpl
	 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureCreateOperation()
	 * @generated
	 */
	int DW_FEATURE_CREATE_OPERATION = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_CREATE_OPERATION__ID = DW_FEATURE_OPERATION__ID;

	/**
	 * The feature id for the '<em><b>Operation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_CREATE_OPERATION__OPERATION_DATE = DW_FEATURE_OPERATION__OPERATION_DATE;

	/**
	 * The feature id for the '<em><b>Command Stack Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_CREATE_OPERATION__COMMAND_STACK_INDEX = DW_FEATURE_OPERATION__COMMAND_STACK_INDEX;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_CREATE_OPERATION__FEATURE_MODEL = DW_FEATURE_OPERATION__FEATURE_MODEL;

	/**
	 * The feature id for the '<em><b>Contributor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_CREATE_OPERATION__CONTRIBUTOR = DW_FEATURE_OPERATION__CONTRIBUTOR;

	/**
	 * The feature id for the '<em><b>Intermediate Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_CREATE_OPERATION__INTERMEDIATE_OPERATION = DW_FEATURE_OPERATION__INTERMEDIATE_OPERATION;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_CREATE_OPERATION__FEATURE = DW_FEATURE_OPERATION__FEATURE;

	/**
	 * The feature id for the '<em><b>Feature Add Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_CREATE_OPERATION__FEATURE_ADD_OPERATION = DW_FEATURE_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Dw Feature Create Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_CREATE_OPERATION_FEATURE_COUNT = DW_FEATURE_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_CREATE_OPERATION___CREATE_ID = DW_FEATURE_OPERATION___CREATE_ID;

	/**
	 * The number of operations of the '<em>Dw Feature Create Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_CREATE_OPERATION_OPERATION_COUNT = DW_FEATURE_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.operation.impl.DwFeatureAddOperationImpl <em>Dw Feature Add Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.operation.impl.DwFeatureAddOperationImpl
	 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureAddOperation()
	 * @generated
	 */
	int DW_FEATURE_ADD_OPERATION = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ADD_OPERATION__ID = DW_FEATURE_OPERATION__ID;

	/**
	 * The feature id for the '<em><b>Operation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ADD_OPERATION__OPERATION_DATE = DW_FEATURE_OPERATION__OPERATION_DATE;

	/**
	 * The feature id for the '<em><b>Command Stack Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ADD_OPERATION__COMMAND_STACK_INDEX = DW_FEATURE_OPERATION__COMMAND_STACK_INDEX;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ADD_OPERATION__FEATURE_MODEL = DW_FEATURE_OPERATION__FEATURE_MODEL;

	/**
	 * The feature id for the '<em><b>Contributor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ADD_OPERATION__CONTRIBUTOR = DW_FEATURE_OPERATION__CONTRIBUTOR;

	/**
	 * The feature id for the '<em><b>Intermediate Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ADD_OPERATION__INTERMEDIATE_OPERATION = DW_FEATURE_OPERATION__INTERMEDIATE_OPERATION;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ADD_OPERATION__FEATURE = DW_FEATURE_OPERATION__FEATURE;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ADD_OPERATION__PARENT = DW_FEATURE_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Force Create Parent Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ADD_OPERATION__FORCE_CREATE_PARENT_GROUP = DW_FEATURE_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Old Group Composition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ADD_OPERATION__OLD_GROUP_COMPOSITION = DW_FEATURE_OPERATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>New Group Composition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ADD_OPERATION__NEW_GROUP_COMPOSITION = DW_FEATURE_OPERATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Group Create Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ADD_OPERATION__GROUP_CREATE_OPERATION = DW_FEATURE_OPERATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Group Type Change Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ADD_OPERATION__GROUP_TYPE_CHANGE_OPERATION = DW_FEATURE_OPERATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Group To Add To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ADD_OPERATION__GROUP_TO_ADD_TO = DW_FEATURE_OPERATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Consequent Feature Type Change Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ADD_OPERATION__CONSEQUENT_FEATURE_TYPE_CHANGE_OPERATION = DW_FEATURE_OPERATION_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Dw Feature Add Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ADD_OPERATION_FEATURE_COUNT = DW_FEATURE_OPERATION_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ADD_OPERATION___CREATE_ID = DW_FEATURE_OPERATION___CREATE_ID;

	/**
	 * The number of operations of the '<em>Dw Feature Add Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ADD_OPERATION_OPERATION_COUNT = DW_FEATURE_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.operation.impl.DwFeatureDeleteOperationImpl <em>Dw Feature Delete Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.operation.impl.DwFeatureDeleteOperationImpl
	 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureDeleteOperation()
	 * @generated
	 */
	int DW_FEATURE_DELETE_OPERATION = 5;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DELETE_OPERATION__ID = DW_FEATURE_OPERATION__ID;

	/**
	 * The feature id for the '<em><b>Operation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DELETE_OPERATION__OPERATION_DATE = DW_FEATURE_OPERATION__OPERATION_DATE;

	/**
	 * The feature id for the '<em><b>Command Stack Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DELETE_OPERATION__COMMAND_STACK_INDEX = DW_FEATURE_OPERATION__COMMAND_STACK_INDEX;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DELETE_OPERATION__FEATURE_MODEL = DW_FEATURE_OPERATION__FEATURE_MODEL;

	/**
	 * The feature id for the '<em><b>Contributor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DELETE_OPERATION__CONTRIBUTOR = DW_FEATURE_OPERATION__CONTRIBUTOR;

	/**
	 * The feature id for the '<em><b>Intermediate Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DELETE_OPERATION__INTERMEDIATE_OPERATION = DW_FEATURE_OPERATION__INTERMEDIATE_OPERATION;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DELETE_OPERATION__FEATURE = DW_FEATURE_OPERATION__FEATURE;

	/**
	 * The feature id for the '<em><b>Old Feature Valid Until</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DELETE_OPERATION__OLD_FEATURE_VALID_UNTIL = DW_FEATURE_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Detach Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DELETE_OPERATION__DETACH_OPERATION = DW_FEATURE_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Consequent Group Operations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DELETE_OPERATION__CONSEQUENT_GROUP_OPERATIONS = DW_FEATURE_OPERATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Dw Feature Delete Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DELETE_OPERATION_FEATURE_COUNT = DW_FEATURE_OPERATION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DELETE_OPERATION___CREATE_ID = DW_FEATURE_OPERATION___CREATE_ID;

	/**
	 * The number of operations of the '<em>Dw Feature Delete Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DELETE_OPERATION_OPERATION_COUNT = DW_FEATURE_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.operation.impl.DwFeatureDetachOperationImpl <em>Dw Feature Detach Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.operation.impl.DwFeatureDetachOperationImpl
	 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureDetachOperation()
	 * @generated
	 */
	int DW_FEATURE_DETACH_OPERATION = 6;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DETACH_OPERATION__ID = DW_FEATURE_OPERATION__ID;

	/**
	 * The feature id for the '<em><b>Operation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DETACH_OPERATION__OPERATION_DATE = DW_FEATURE_OPERATION__OPERATION_DATE;

	/**
	 * The feature id for the '<em><b>Command Stack Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DETACH_OPERATION__COMMAND_STACK_INDEX = DW_FEATURE_OPERATION__COMMAND_STACK_INDEX;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DETACH_OPERATION__FEATURE_MODEL = DW_FEATURE_OPERATION__FEATURE_MODEL;

	/**
	 * The feature id for the '<em><b>Contributor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DETACH_OPERATION__CONTRIBUTOR = DW_FEATURE_OPERATION__CONTRIBUTOR;

	/**
	 * The feature id for the '<em><b>Intermediate Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DETACH_OPERATION__INTERMEDIATE_OPERATION = DW_FEATURE_OPERATION__INTERMEDIATE_OPERATION;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DETACH_OPERATION__FEATURE = DW_FEATURE_OPERATION__FEATURE;

	/**
	 * The feature id for the '<em><b>Old Group Membership</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DETACH_OPERATION__OLD_GROUP_MEMBERSHIP = DW_FEATURE_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>New Group Membership</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DETACH_OPERATION__NEW_GROUP_MEMBERSHIP = DW_FEATURE_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Old Valid Until</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DETACH_OPERATION__OLD_VALID_UNTIL = DW_FEATURE_OPERATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Consequent Group Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DETACH_OPERATION__CONSEQUENT_GROUP_OPERATION = DW_FEATURE_OPERATION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Dw Feature Detach Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DETACH_OPERATION_FEATURE_COUNT = DW_FEATURE_OPERATION_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DETACH_OPERATION___CREATE_ID = DW_FEATURE_OPERATION___CREATE_ID;

	/**
	 * The number of operations of the '<em>Dw Feature Detach Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DETACH_OPERATION_OPERATION_COUNT = DW_FEATURE_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.operation.impl.DwFeatureMoveOperationImpl <em>Dw Feature Move Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.operation.impl.DwFeatureMoveOperationImpl
	 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureMoveOperation()
	 * @generated
	 */
	int DW_FEATURE_MOVE_OPERATION = 7;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_MOVE_OPERATION__ID = DW_FEATURE_OPERATION__ID;

	/**
	 * The feature id for the '<em><b>Operation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_MOVE_OPERATION__OPERATION_DATE = DW_FEATURE_OPERATION__OPERATION_DATE;

	/**
	 * The feature id for the '<em><b>Command Stack Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_MOVE_OPERATION__COMMAND_STACK_INDEX = DW_FEATURE_OPERATION__COMMAND_STACK_INDEX;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_MOVE_OPERATION__FEATURE_MODEL = DW_FEATURE_OPERATION__FEATURE_MODEL;

	/**
	 * The feature id for the '<em><b>Contributor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_MOVE_OPERATION__CONTRIBUTOR = DW_FEATURE_OPERATION__CONTRIBUTOR;

	/**
	 * The feature id for the '<em><b>Intermediate Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_MOVE_OPERATION__INTERMEDIATE_OPERATION = DW_FEATURE_OPERATION__INTERMEDIATE_OPERATION;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_MOVE_OPERATION__FEATURE = DW_FEATURE_OPERATION__FEATURE;

	/**
	 * The feature id for the '<em><b>Detach Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_MOVE_OPERATION__DETACH_OPERATION = DW_FEATURE_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Add Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_MOVE_OPERATION__ADD_OPERATION = DW_FEATURE_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Dw Feature Move Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_MOVE_OPERATION_FEATURE_COUNT = DW_FEATURE_OPERATION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_MOVE_OPERATION___CREATE_ID = DW_FEATURE_OPERATION___CREATE_ID;

	/**
	 * The number of operations of the '<em>Dw Feature Move Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_MOVE_OPERATION_OPERATION_COUNT = DW_FEATURE_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.operation.impl.DwFeatureRenameOperationImpl <em>Dw Feature Rename Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.operation.impl.DwFeatureRenameOperationImpl
	 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureRenameOperation()
	 * @generated
	 */
	int DW_FEATURE_RENAME_OPERATION = 8;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_RENAME_OPERATION__ID = DW_FEATURE_OPERATION__ID;

	/**
	 * The feature id for the '<em><b>Operation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_RENAME_OPERATION__OPERATION_DATE = DW_FEATURE_OPERATION__OPERATION_DATE;

	/**
	 * The feature id for the '<em><b>Command Stack Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_RENAME_OPERATION__COMMAND_STACK_INDEX = DW_FEATURE_OPERATION__COMMAND_STACK_INDEX;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_RENAME_OPERATION__FEATURE_MODEL = DW_FEATURE_OPERATION__FEATURE_MODEL;

	/**
	 * The feature id for the '<em><b>Contributor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_RENAME_OPERATION__CONTRIBUTOR = DW_FEATURE_OPERATION__CONTRIBUTOR;

	/**
	 * The feature id for the '<em><b>Intermediate Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_RENAME_OPERATION__INTERMEDIATE_OPERATION = DW_FEATURE_OPERATION__INTERMEDIATE_OPERATION;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_RENAME_OPERATION__FEATURE = DW_FEATURE_OPERATION__FEATURE;

	/**
	 * The feature id for the '<em><b>New Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_RENAME_OPERATION__NEW_NAME = DW_FEATURE_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>New Name String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_RENAME_OPERATION__NEW_NAME_STRING = DW_FEATURE_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Old Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_RENAME_OPERATION__OLD_NAME = DW_FEATURE_OPERATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Old Name String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_RENAME_OPERATION__OLD_NAME_STRING = DW_FEATURE_OPERATION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Dw Feature Rename Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_RENAME_OPERATION_FEATURE_COUNT = DW_FEATURE_OPERATION_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_RENAME_OPERATION___CREATE_ID = DW_FEATURE_OPERATION___CREATE_ID;

	/**
	 * The number of operations of the '<em>Dw Feature Rename Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_RENAME_OPERATION_OPERATION_COUNT = DW_FEATURE_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.operation.impl.DwFeatureTypeChangeOperationImpl <em>Dw Feature Type Change Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.operation.impl.DwFeatureTypeChangeOperationImpl
	 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureTypeChangeOperation()
	 * @generated
	 */
	int DW_FEATURE_TYPE_CHANGE_OPERATION = 9;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_TYPE_CHANGE_OPERATION__ID = DW_FEATURE_OPERATION__ID;

	/**
	 * The feature id for the '<em><b>Operation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_TYPE_CHANGE_OPERATION__OPERATION_DATE = DW_FEATURE_OPERATION__OPERATION_DATE;

	/**
	 * The feature id for the '<em><b>Command Stack Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_TYPE_CHANGE_OPERATION__COMMAND_STACK_INDEX = DW_FEATURE_OPERATION__COMMAND_STACK_INDEX;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_TYPE_CHANGE_OPERATION__FEATURE_MODEL = DW_FEATURE_OPERATION__FEATURE_MODEL;

	/**
	 * The feature id for the '<em><b>Contributor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_TYPE_CHANGE_OPERATION__CONTRIBUTOR = DW_FEATURE_OPERATION__CONTRIBUTOR;

	/**
	 * The feature id for the '<em><b>Intermediate Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_TYPE_CHANGE_OPERATION__INTERMEDIATE_OPERATION = DW_FEATURE_OPERATION__INTERMEDIATE_OPERATION;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_TYPE_CHANGE_OPERATION__FEATURE = DW_FEATURE_OPERATION__FEATURE;

	/**
	 * The feature id for the '<em><b>New Feature Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_TYPE_CHANGE_OPERATION__NEW_FEATURE_TYPE = DW_FEATURE_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>New Feature Type Enum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_TYPE_CHANGE_OPERATION__NEW_FEATURE_TYPE_ENUM = DW_FEATURE_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Old Feature Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_TYPE_CHANGE_OPERATION__OLD_FEATURE_TYPE = DW_FEATURE_OPERATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Dw Feature Type Change Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_TYPE_CHANGE_OPERATION_FEATURE_COUNT = DW_FEATURE_OPERATION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_TYPE_CHANGE_OPERATION___CREATE_ID = DW_FEATURE_OPERATION___CREATE_ID;

	/**
	 * The number of operations of the '<em>Dw Feature Type Change Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_TYPE_CHANGE_OPERATION_OPERATION_COUNT = DW_FEATURE_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.operation.impl.DwFeatureAttributeOperationImpl <em>Dw Feature Attribute Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.operation.impl.DwFeatureAttributeOperationImpl
	 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureAttributeOperation()
	 * @generated
	 */
	int DW_FEATURE_ATTRIBUTE_OPERATION = 10;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_OPERATION__ID = DW_FEATURE_OPERATION__ID;

	/**
	 * The feature id for the '<em><b>Operation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_OPERATION__OPERATION_DATE = DW_FEATURE_OPERATION__OPERATION_DATE;

	/**
	 * The feature id for the '<em><b>Command Stack Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_OPERATION__COMMAND_STACK_INDEX = DW_FEATURE_OPERATION__COMMAND_STACK_INDEX;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_OPERATION__FEATURE_MODEL = DW_FEATURE_OPERATION__FEATURE_MODEL;

	/**
	 * The feature id for the '<em><b>Contributor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_OPERATION__CONTRIBUTOR = DW_FEATURE_OPERATION__CONTRIBUTOR;

	/**
	 * The feature id for the '<em><b>Intermediate Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_OPERATION__INTERMEDIATE_OPERATION = DW_FEATURE_OPERATION__INTERMEDIATE_OPERATION;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_OPERATION__FEATURE = DW_FEATURE_OPERATION__FEATURE;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_OPERATION__ATTRIBUTE = DW_FEATURE_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Dw Feature Attribute Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_OPERATION_FEATURE_COUNT = DW_FEATURE_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_OPERATION___CREATE_ID = DW_FEATURE_OPERATION___CREATE_ID;

	/**
	 * The number of operations of the '<em>Dw Feature Attribute Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_OPERATION_OPERATION_COUNT = DW_FEATURE_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.operation.impl.DwFeatureAttributeCreateOperationImpl <em>Dw Feature Attribute Create Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.operation.impl.DwFeatureAttributeCreateOperationImpl
	 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureAttributeCreateOperation()
	 * @generated
	 */
	int DW_FEATURE_ATTRIBUTE_CREATE_OPERATION = 11;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_CREATE_OPERATION__ID = DW_FEATURE_ATTRIBUTE_OPERATION__ID;

	/**
	 * The feature id for the '<em><b>Operation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_CREATE_OPERATION__OPERATION_DATE = DW_FEATURE_ATTRIBUTE_OPERATION__OPERATION_DATE;

	/**
	 * The feature id for the '<em><b>Command Stack Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_CREATE_OPERATION__COMMAND_STACK_INDEX = DW_FEATURE_ATTRIBUTE_OPERATION__COMMAND_STACK_INDEX;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_CREATE_OPERATION__FEATURE_MODEL = DW_FEATURE_ATTRIBUTE_OPERATION__FEATURE_MODEL;

	/**
	 * The feature id for the '<em><b>Contributor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_CREATE_OPERATION__CONTRIBUTOR = DW_FEATURE_ATTRIBUTE_OPERATION__CONTRIBUTOR;

	/**
	 * The feature id for the '<em><b>Intermediate Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_CREATE_OPERATION__INTERMEDIATE_OPERATION = DW_FEATURE_ATTRIBUTE_OPERATION__INTERMEDIATE_OPERATION;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_CREATE_OPERATION__FEATURE = DW_FEATURE_ATTRIBUTE_OPERATION__FEATURE;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_CREATE_OPERATION__ATTRIBUTE = DW_FEATURE_ATTRIBUTE_OPERATION__ATTRIBUTE;

	/**
	 * The number of structural features of the '<em>Dw Feature Attribute Create Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_CREATE_OPERATION_FEATURE_COUNT = DW_FEATURE_ATTRIBUTE_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_CREATE_OPERATION___CREATE_ID = DW_FEATURE_ATTRIBUTE_OPERATION___CREATE_ID;

	/**
	 * The number of operations of the '<em>Dw Feature Attribute Create Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_CREATE_OPERATION_OPERATION_COUNT = DW_FEATURE_ATTRIBUTE_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.operation.impl.DwFeatureAttributeDeleteOperationImpl <em>Dw Feature Attribute Delete Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.operation.impl.DwFeatureAttributeDeleteOperationImpl
	 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureAttributeDeleteOperation()
	 * @generated
	 */
	int DW_FEATURE_ATTRIBUTE_DELETE_OPERATION = 12;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_DELETE_OPERATION__ID = DW_FEATURE_ATTRIBUTE_OPERATION__ID;

	/**
	 * The feature id for the '<em><b>Operation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_DELETE_OPERATION__OPERATION_DATE = DW_FEATURE_ATTRIBUTE_OPERATION__OPERATION_DATE;

	/**
	 * The feature id for the '<em><b>Command Stack Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_DELETE_OPERATION__COMMAND_STACK_INDEX = DW_FEATURE_ATTRIBUTE_OPERATION__COMMAND_STACK_INDEX;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_DELETE_OPERATION__FEATURE_MODEL = DW_FEATURE_ATTRIBUTE_OPERATION__FEATURE_MODEL;

	/**
	 * The feature id for the '<em><b>Contributor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_DELETE_OPERATION__CONTRIBUTOR = DW_FEATURE_ATTRIBUTE_OPERATION__CONTRIBUTOR;

	/**
	 * The feature id for the '<em><b>Intermediate Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_DELETE_OPERATION__INTERMEDIATE_OPERATION = DW_FEATURE_ATTRIBUTE_OPERATION__INTERMEDIATE_OPERATION;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_DELETE_OPERATION__FEATURE = DW_FEATURE_ATTRIBUTE_OPERATION__FEATURE;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_DELETE_OPERATION__ATTRIBUTE = DW_FEATURE_ATTRIBUTE_OPERATION__ATTRIBUTE;

	/**
	 * The number of structural features of the '<em>Dw Feature Attribute Delete Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_DELETE_OPERATION_FEATURE_COUNT = DW_FEATURE_ATTRIBUTE_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_DELETE_OPERATION___CREATE_ID = DW_FEATURE_ATTRIBUTE_OPERATION___CREATE_ID;

	/**
	 * The number of operations of the '<em>Dw Feature Attribute Delete Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_DELETE_OPERATION_OPERATION_COUNT = DW_FEATURE_ATTRIBUTE_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.operation.impl.DwFeatureAttributeRenameOperationImpl <em>Dw Feature Attribute Rename Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.operation.impl.DwFeatureAttributeRenameOperationImpl
	 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureAttributeRenameOperation()
	 * @generated
	 */
	int DW_FEATURE_ATTRIBUTE_RENAME_OPERATION = 13;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__ID = DW_FEATURE_ATTRIBUTE_OPERATION__ID;

	/**
	 * The feature id for the '<em><b>Operation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__OPERATION_DATE = DW_FEATURE_ATTRIBUTE_OPERATION__OPERATION_DATE;

	/**
	 * The feature id for the '<em><b>Command Stack Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__COMMAND_STACK_INDEX = DW_FEATURE_ATTRIBUTE_OPERATION__COMMAND_STACK_INDEX;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__FEATURE_MODEL = DW_FEATURE_ATTRIBUTE_OPERATION__FEATURE_MODEL;

	/**
	 * The feature id for the '<em><b>Contributor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__CONTRIBUTOR = DW_FEATURE_ATTRIBUTE_OPERATION__CONTRIBUTOR;

	/**
	 * The feature id for the '<em><b>Intermediate Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__INTERMEDIATE_OPERATION = DW_FEATURE_ATTRIBUTE_OPERATION__INTERMEDIATE_OPERATION;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__FEATURE = DW_FEATURE_ATTRIBUTE_OPERATION__FEATURE;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__ATTRIBUTE = DW_FEATURE_ATTRIBUTE_OPERATION__ATTRIBUTE;

	/**
	 * The feature id for the '<em><b>Old Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__OLD_NAME = DW_FEATURE_ATTRIBUTE_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>New Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__NEW_NAME = DW_FEATURE_ATTRIBUTE_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Dw Feature Attribute Rename Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_RENAME_OPERATION_FEATURE_COUNT = DW_FEATURE_ATTRIBUTE_OPERATION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_RENAME_OPERATION___CREATE_ID = DW_FEATURE_ATTRIBUTE_OPERATION___CREATE_ID;

	/**
	 * The number of operations of the '<em>Dw Feature Attribute Rename Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_RENAME_OPERATION_OPERATION_COUNT = DW_FEATURE_ATTRIBUTE_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.operation.impl.DwFeatureVersionOperationImpl <em>Dw Feature Version Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.operation.impl.DwFeatureVersionOperationImpl
	 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureVersionOperation()
	 * @generated
	 */
	int DW_FEATURE_VERSION_OPERATION = 14;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_OPERATION__ID = DW_FEATURE_OPERATION__ID;

	/**
	 * The feature id for the '<em><b>Operation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_OPERATION__OPERATION_DATE = DW_FEATURE_OPERATION__OPERATION_DATE;

	/**
	 * The feature id for the '<em><b>Command Stack Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_OPERATION__COMMAND_STACK_INDEX = DW_FEATURE_OPERATION__COMMAND_STACK_INDEX;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_OPERATION__FEATURE_MODEL = DW_FEATURE_OPERATION__FEATURE_MODEL;

	/**
	 * The feature id for the '<em><b>Contributor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_OPERATION__CONTRIBUTOR = DW_FEATURE_OPERATION__CONTRIBUTOR;

	/**
	 * The feature id for the '<em><b>Intermediate Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_OPERATION__INTERMEDIATE_OPERATION = DW_FEATURE_OPERATION__INTERMEDIATE_OPERATION;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_OPERATION__FEATURE = DW_FEATURE_OPERATION__FEATURE;

	/**
	 * The feature id for the '<em><b>Version</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_OPERATION__VERSION = DW_FEATURE_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Dw Feature Version Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_OPERATION_FEATURE_COUNT = DW_FEATURE_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_OPERATION___CREATE_ID = DW_FEATURE_OPERATION___CREATE_ID;

	/**
	 * The number of operations of the '<em>Dw Feature Version Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_OPERATION_OPERATION_COUNT = DW_FEATURE_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.operation.impl.DwFeatureVersionCreateOperationImpl <em>Dw Feature Version Create Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.operation.impl.DwFeatureVersionCreateOperationImpl
	 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureVersionCreateOperation()
	 * @generated
	 */
	int DW_FEATURE_VERSION_CREATE_OPERATION = 15;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_CREATE_OPERATION__ID = DW_FEATURE_VERSION_OPERATION__ID;

	/**
	 * The feature id for the '<em><b>Operation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_CREATE_OPERATION__OPERATION_DATE = DW_FEATURE_VERSION_OPERATION__OPERATION_DATE;

	/**
	 * The feature id for the '<em><b>Command Stack Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_CREATE_OPERATION__COMMAND_STACK_INDEX = DW_FEATURE_VERSION_OPERATION__COMMAND_STACK_INDEX;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_CREATE_OPERATION__FEATURE_MODEL = DW_FEATURE_VERSION_OPERATION__FEATURE_MODEL;

	/**
	 * The feature id for the '<em><b>Contributor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_CREATE_OPERATION__CONTRIBUTOR = DW_FEATURE_VERSION_OPERATION__CONTRIBUTOR;

	/**
	 * The feature id for the '<em><b>Intermediate Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_CREATE_OPERATION__INTERMEDIATE_OPERATION = DW_FEATURE_VERSION_OPERATION__INTERMEDIATE_OPERATION;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_CREATE_OPERATION__FEATURE = DW_FEATURE_VERSION_OPERATION__FEATURE;

	/**
	 * The feature id for the '<em><b>Version</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_CREATE_OPERATION__VERSION = DW_FEATURE_VERSION_OPERATION__VERSION;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_CREATE_OPERATION__PARENT = DW_FEATURE_VERSION_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Use Same Branch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_CREATE_OPERATION__USE_SAME_BRANCH = DW_FEATURE_VERSION_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Wire And Add After</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_CREATE_OPERATION__WIRE_AND_ADD_AFTER = DW_FEATURE_VERSION_OPERATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Dw Feature Version Create Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_CREATE_OPERATION_FEATURE_COUNT = DW_FEATURE_VERSION_OPERATION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_CREATE_OPERATION___CREATE_ID = DW_FEATURE_VERSION_OPERATION___CREATE_ID;

	/**
	 * The number of operations of the '<em>Dw Feature Version Create Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_CREATE_OPERATION_OPERATION_COUNT = DW_FEATURE_VERSION_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.operation.impl.DwFeatureVersionDeleteOperationImpl <em>Dw Feature Version Delete Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.operation.impl.DwFeatureVersionDeleteOperationImpl
	 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureVersionDeleteOperation()
	 * @generated
	 */
	int DW_FEATURE_VERSION_DELETE_OPERATION = 16;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_DELETE_OPERATION__ID = DW_FEATURE_VERSION_OPERATION__ID;

	/**
	 * The feature id for the '<em><b>Operation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_DELETE_OPERATION__OPERATION_DATE = DW_FEATURE_VERSION_OPERATION__OPERATION_DATE;

	/**
	 * The feature id for the '<em><b>Command Stack Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_DELETE_OPERATION__COMMAND_STACK_INDEX = DW_FEATURE_VERSION_OPERATION__COMMAND_STACK_INDEX;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_DELETE_OPERATION__FEATURE_MODEL = DW_FEATURE_VERSION_OPERATION__FEATURE_MODEL;

	/**
	 * The feature id for the '<em><b>Contributor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_DELETE_OPERATION__CONTRIBUTOR = DW_FEATURE_VERSION_OPERATION__CONTRIBUTOR;

	/**
	 * The feature id for the '<em><b>Intermediate Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_DELETE_OPERATION__INTERMEDIATE_OPERATION = DW_FEATURE_VERSION_OPERATION__INTERMEDIATE_OPERATION;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_DELETE_OPERATION__FEATURE = DW_FEATURE_VERSION_OPERATION__FEATURE;

	/**
	 * The feature id for the '<em><b>Version</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_DELETE_OPERATION__VERSION = DW_FEATURE_VERSION_OPERATION__VERSION;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_DELETE_OPERATION__PARENT = DW_FEATURE_VERSION_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Old Valid Until</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_DELETE_OPERATION__OLD_VALID_UNTIL = DW_FEATURE_VERSION_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Dw Feature Version Delete Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_DELETE_OPERATION_FEATURE_COUNT = DW_FEATURE_VERSION_OPERATION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_DELETE_OPERATION___CREATE_ID = DW_FEATURE_VERSION_OPERATION___CREATE_ID;

	/**
	 * The number of operations of the '<em>Dw Feature Version Delete Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_DELETE_OPERATION_OPERATION_COUNT = DW_FEATURE_VERSION_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.operation.impl.DwFeatureVersionRenameOperationImpl <em>Dw Feature Version Rename Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.operation.impl.DwFeatureVersionRenameOperationImpl
	 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureVersionRenameOperation()
	 * @generated
	 */
	int DW_FEATURE_VERSION_RENAME_OPERATION = 17;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_RENAME_OPERATION__ID = DW_FEATURE_VERSION_OPERATION__ID;

	/**
	 * The feature id for the '<em><b>Operation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_RENAME_OPERATION__OPERATION_DATE = DW_FEATURE_VERSION_OPERATION__OPERATION_DATE;

	/**
	 * The feature id for the '<em><b>Command Stack Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_RENAME_OPERATION__COMMAND_STACK_INDEX = DW_FEATURE_VERSION_OPERATION__COMMAND_STACK_INDEX;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_RENAME_OPERATION__FEATURE_MODEL = DW_FEATURE_VERSION_OPERATION__FEATURE_MODEL;

	/**
	 * The feature id for the '<em><b>Contributor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_RENAME_OPERATION__CONTRIBUTOR = DW_FEATURE_VERSION_OPERATION__CONTRIBUTOR;

	/**
	 * The feature id for the '<em><b>Intermediate Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_RENAME_OPERATION__INTERMEDIATE_OPERATION = DW_FEATURE_VERSION_OPERATION__INTERMEDIATE_OPERATION;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_RENAME_OPERATION__FEATURE = DW_FEATURE_VERSION_OPERATION__FEATURE;

	/**
	 * The feature id for the '<em><b>Version</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_RENAME_OPERATION__VERSION = DW_FEATURE_VERSION_OPERATION__VERSION;

	/**
	 * The feature id for the '<em><b>Old Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_RENAME_OPERATION__OLD_NUMBER = DW_FEATURE_VERSION_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>New Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_RENAME_OPERATION__NEW_NUMBER = DW_FEATURE_VERSION_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Dw Feature Version Rename Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_RENAME_OPERATION_FEATURE_COUNT = DW_FEATURE_VERSION_OPERATION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_RENAME_OPERATION___CREATE_ID = DW_FEATURE_VERSION_OPERATION___CREATE_ID;

	/**
	 * The number of operations of the '<em>Dw Feature Version Rename Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_RENAME_OPERATION_OPERATION_COUNT = DW_FEATURE_VERSION_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.operation.impl.DwGroupOperationImpl <em>Dw Group Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.operation.impl.DwGroupOperationImpl
	 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwGroupOperation()
	 * @generated
	 */
	int DW_GROUP_OPERATION = 18;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_OPERATION__ID = DW_EVOLUTION_OPERATION__ID;

	/**
	 * The feature id for the '<em><b>Operation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_OPERATION__OPERATION_DATE = DW_EVOLUTION_OPERATION__OPERATION_DATE;

	/**
	 * The feature id for the '<em><b>Command Stack Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_OPERATION__COMMAND_STACK_INDEX = DW_EVOLUTION_OPERATION__COMMAND_STACK_INDEX;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_OPERATION__FEATURE_MODEL = DW_EVOLUTION_OPERATION__FEATURE_MODEL;

	/**
	 * The feature id for the '<em><b>Contributor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_OPERATION__CONTRIBUTOR = DW_EVOLUTION_OPERATION__CONTRIBUTOR;

	/**
	 * The feature id for the '<em><b>Intermediate Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_OPERATION__INTERMEDIATE_OPERATION = DW_EVOLUTION_OPERATION__INTERMEDIATE_OPERATION;

	/**
	 * The feature id for the '<em><b>Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_OPERATION__GROUP = DW_EVOLUTION_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Dw Group Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_OPERATION_FEATURE_COUNT = DW_EVOLUTION_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_OPERATION___CREATE_ID = DW_EVOLUTION_OPERATION___CREATE_ID;

	/**
	 * The number of operations of the '<em>Dw Group Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_OPERATION_OPERATION_COUNT = DW_EVOLUTION_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.operation.impl.DwGroupCreateOperationImpl <em>Dw Group Create Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.operation.impl.DwGroupCreateOperationImpl
	 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwGroupCreateOperation()
	 * @generated
	 */
	int DW_GROUP_CREATE_OPERATION = 19;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_CREATE_OPERATION__ID = DW_GROUP_OPERATION__ID;

	/**
	 * The feature id for the '<em><b>Operation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_CREATE_OPERATION__OPERATION_DATE = DW_GROUP_OPERATION__OPERATION_DATE;

	/**
	 * The feature id for the '<em><b>Command Stack Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_CREATE_OPERATION__COMMAND_STACK_INDEX = DW_GROUP_OPERATION__COMMAND_STACK_INDEX;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_CREATE_OPERATION__FEATURE_MODEL = DW_GROUP_OPERATION__FEATURE_MODEL;

	/**
	 * The feature id for the '<em><b>Contributor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_CREATE_OPERATION__CONTRIBUTOR = DW_GROUP_OPERATION__CONTRIBUTOR;

	/**
	 * The feature id for the '<em><b>Intermediate Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_CREATE_OPERATION__INTERMEDIATE_OPERATION = DW_GROUP_OPERATION__INTERMEDIATE_OPERATION;

	/**
	 * The feature id for the '<em><b>Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_CREATE_OPERATION__GROUP = DW_GROUP_OPERATION__GROUP;

	/**
	 * The feature id for the '<em><b>Group Add Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_CREATE_OPERATION__GROUP_ADD_OPERATION = DW_GROUP_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Dw Group Create Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_CREATE_OPERATION_FEATURE_COUNT = DW_GROUP_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_CREATE_OPERATION___CREATE_ID = DW_GROUP_OPERATION___CREATE_ID;

	/**
	 * The number of operations of the '<em>Dw Group Create Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_CREATE_OPERATION_OPERATION_COUNT = DW_GROUP_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.operation.impl.DwGroupAddOperationImpl <em>Dw Group Add Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.operation.impl.DwGroupAddOperationImpl
	 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwGroupAddOperation()
	 * @generated
	 */
	int DW_GROUP_ADD_OPERATION = 20;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_ADD_OPERATION__ID = DW_GROUP_OPERATION__ID;

	/**
	 * The feature id for the '<em><b>Operation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_ADD_OPERATION__OPERATION_DATE = DW_GROUP_OPERATION__OPERATION_DATE;

	/**
	 * The feature id for the '<em><b>Command Stack Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_ADD_OPERATION__COMMAND_STACK_INDEX = DW_GROUP_OPERATION__COMMAND_STACK_INDEX;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_ADD_OPERATION__FEATURE_MODEL = DW_GROUP_OPERATION__FEATURE_MODEL;

	/**
	 * The feature id for the '<em><b>Contributor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_ADD_OPERATION__CONTRIBUTOR = DW_GROUP_OPERATION__CONTRIBUTOR;

	/**
	 * The feature id for the '<em><b>Intermediate Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_ADD_OPERATION__INTERMEDIATE_OPERATION = DW_GROUP_OPERATION__INTERMEDIATE_OPERATION;

	/**
	 * The feature id for the '<em><b>Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_ADD_OPERATION__GROUP = DW_GROUP_OPERATION__GROUP;

	/**
	 * The feature id for the '<em><b>Parent Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_ADD_OPERATION__PARENT_FEATURE = DW_GROUP_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>New Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_ADD_OPERATION__NEW_CONTAINER = DW_GROUP_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Dw Group Add Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_ADD_OPERATION_FEATURE_COUNT = DW_GROUP_OPERATION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_ADD_OPERATION___CREATE_ID = DW_GROUP_OPERATION___CREATE_ID;

	/**
	 * The number of operations of the '<em>Dw Group Add Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_ADD_OPERATION_OPERATION_COUNT = DW_GROUP_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.operation.impl.DwGroupDeleteOperationImpl <em>Dw Group Delete Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.operation.impl.DwGroupDeleteOperationImpl
	 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwGroupDeleteOperation()
	 * @generated
	 */
	int DW_GROUP_DELETE_OPERATION = 21;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_DELETE_OPERATION__ID = DW_GROUP_OPERATION__ID;

	/**
	 * The feature id for the '<em><b>Operation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_DELETE_OPERATION__OPERATION_DATE = DW_GROUP_OPERATION__OPERATION_DATE;

	/**
	 * The feature id for the '<em><b>Command Stack Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_DELETE_OPERATION__COMMAND_STACK_INDEX = DW_GROUP_OPERATION__COMMAND_STACK_INDEX;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_DELETE_OPERATION__FEATURE_MODEL = DW_GROUP_OPERATION__FEATURE_MODEL;

	/**
	 * The feature id for the '<em><b>Contributor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_DELETE_OPERATION__CONTRIBUTOR = DW_GROUP_OPERATION__CONTRIBUTOR;

	/**
	 * The feature id for the '<em><b>Intermediate Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_DELETE_OPERATION__INTERMEDIATE_OPERATION = DW_GROUP_OPERATION__INTERMEDIATE_OPERATION;

	/**
	 * The feature id for the '<em><b>Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_DELETE_OPERATION__GROUP = DW_GROUP_OPERATION__GROUP;

	/**
	 * The feature id for the '<em><b>Old Group Valid Until</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_DELETE_OPERATION__OLD_GROUP_VALID_UNTIL = DW_GROUP_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Detach Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_DELETE_OPERATION__DETACH_OPERATION = DW_GROUP_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Consequent Feature Operations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_DELETE_OPERATION__CONSEQUENT_FEATURE_OPERATIONS = DW_GROUP_OPERATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Dw Group Delete Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_DELETE_OPERATION_FEATURE_COUNT = DW_GROUP_OPERATION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_DELETE_OPERATION___CREATE_ID = DW_GROUP_OPERATION___CREATE_ID;

	/**
	 * The number of operations of the '<em>Dw Group Delete Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_DELETE_OPERATION_OPERATION_COUNT = DW_GROUP_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.operation.impl.DwGroupDetachOperationImpl <em>Dw Group Detach Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.operation.impl.DwGroupDetachOperationImpl
	 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwGroupDetachOperation()
	 * @generated
	 */
	int DW_GROUP_DETACH_OPERATION = 22;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_DETACH_OPERATION__ID = DW_GROUP_OPERATION__ID;

	/**
	 * The feature id for the '<em><b>Operation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_DETACH_OPERATION__OPERATION_DATE = DW_GROUP_OPERATION__OPERATION_DATE;

	/**
	 * The feature id for the '<em><b>Command Stack Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_DETACH_OPERATION__COMMAND_STACK_INDEX = DW_GROUP_OPERATION__COMMAND_STACK_INDEX;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_DETACH_OPERATION__FEATURE_MODEL = DW_GROUP_OPERATION__FEATURE_MODEL;

	/**
	 * The feature id for the '<em><b>Contributor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_DETACH_OPERATION__CONTRIBUTOR = DW_GROUP_OPERATION__CONTRIBUTOR;

	/**
	 * The feature id for the '<em><b>Intermediate Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_DETACH_OPERATION__INTERMEDIATE_OPERATION = DW_GROUP_OPERATION__INTERMEDIATE_OPERATION;

	/**
	 * The feature id for the '<em><b>Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_DETACH_OPERATION__GROUP = DW_GROUP_OPERATION__GROUP;

	/**
	 * The feature id for the '<em><b>Old Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_DETACH_OPERATION__OLD_CONTAINER = DW_GROUP_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Old Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_DETACH_OPERATION__OLD_PARENT = DW_GROUP_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Dw Group Detach Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_DETACH_OPERATION_FEATURE_COUNT = DW_GROUP_OPERATION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_DETACH_OPERATION___CREATE_ID = DW_GROUP_OPERATION___CREATE_ID;

	/**
	 * The number of operations of the '<em>Dw Group Detach Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_DETACH_OPERATION_OPERATION_COUNT = DW_GROUP_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.operation.impl.DwGroupMoveOperationImpl <em>Dw Group Move Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.operation.impl.DwGroupMoveOperationImpl
	 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwGroupMoveOperation()
	 * @generated
	 */
	int DW_GROUP_MOVE_OPERATION = 23;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_MOVE_OPERATION__ID = DW_GROUP_OPERATION__ID;

	/**
	 * The feature id for the '<em><b>Operation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_MOVE_OPERATION__OPERATION_DATE = DW_GROUP_OPERATION__OPERATION_DATE;

	/**
	 * The feature id for the '<em><b>Command Stack Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_MOVE_OPERATION__COMMAND_STACK_INDEX = DW_GROUP_OPERATION__COMMAND_STACK_INDEX;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_MOVE_OPERATION__FEATURE_MODEL = DW_GROUP_OPERATION__FEATURE_MODEL;

	/**
	 * The feature id for the '<em><b>Contributor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_MOVE_OPERATION__CONTRIBUTOR = DW_GROUP_OPERATION__CONTRIBUTOR;

	/**
	 * The feature id for the '<em><b>Intermediate Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_MOVE_OPERATION__INTERMEDIATE_OPERATION = DW_GROUP_OPERATION__INTERMEDIATE_OPERATION;

	/**
	 * The feature id for the '<em><b>Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_MOVE_OPERATION__GROUP = DW_GROUP_OPERATION__GROUP;

	/**
	 * The feature id for the '<em><b>Group Detach Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_MOVE_OPERATION__GROUP_DETACH_OPERATION = DW_GROUP_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Group Add Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_MOVE_OPERATION__GROUP_ADD_OPERATION = DW_GROUP_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Dw Group Move Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_MOVE_OPERATION_FEATURE_COUNT = DW_GROUP_OPERATION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_MOVE_OPERATION___CREATE_ID = DW_GROUP_OPERATION___CREATE_ID;

	/**
	 * The number of operations of the '<em>Dw Group Move Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_MOVE_OPERATION_OPERATION_COUNT = DW_GROUP_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.operation.impl.DwGroupTypeChangeOperationImpl <em>Dw Group Type Change Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.operation.impl.DwGroupTypeChangeOperationImpl
	 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwGroupTypeChangeOperation()
	 * @generated
	 */
	int DW_GROUP_TYPE_CHANGE_OPERATION = 24;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_TYPE_CHANGE_OPERATION__ID = DW_GROUP_OPERATION__ID;

	/**
	 * The feature id for the '<em><b>Operation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_TYPE_CHANGE_OPERATION__OPERATION_DATE = DW_GROUP_OPERATION__OPERATION_DATE;

	/**
	 * The feature id for the '<em><b>Command Stack Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_TYPE_CHANGE_OPERATION__COMMAND_STACK_INDEX = DW_GROUP_OPERATION__COMMAND_STACK_INDEX;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_TYPE_CHANGE_OPERATION__FEATURE_MODEL = DW_GROUP_OPERATION__FEATURE_MODEL;

	/**
	 * The feature id for the '<em><b>Contributor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_TYPE_CHANGE_OPERATION__CONTRIBUTOR = DW_GROUP_OPERATION__CONTRIBUTOR;

	/**
	 * The feature id for the '<em><b>Intermediate Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_TYPE_CHANGE_OPERATION__INTERMEDIATE_OPERATION = DW_GROUP_OPERATION__INTERMEDIATE_OPERATION;

	/**
	 * The feature id for the '<em><b>Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_TYPE_CHANGE_OPERATION__GROUP = DW_GROUP_OPERATION__GROUP;

	/**
	 * The feature id for the '<em><b>Consequent Operations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_TYPE_CHANGE_OPERATION__CONSEQUENT_OPERATIONS = DW_GROUP_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>New Group Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_TYPE_CHANGE_OPERATION__NEW_GROUP_TYPE = DW_GROUP_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>New Group Type Enum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_TYPE_CHANGE_OPERATION__NEW_GROUP_TYPE_ENUM = DW_GROUP_OPERATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Old Group Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_TYPE_CHANGE_OPERATION__OLD_GROUP_TYPE = DW_GROUP_OPERATION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Dw Group Type Change Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_TYPE_CHANGE_OPERATION_FEATURE_COUNT = DW_GROUP_OPERATION_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_TYPE_CHANGE_OPERATION___CREATE_ID = DW_GROUP_OPERATION___CREATE_ID;

	/**
	 * The number of operations of the '<em>Dw Group Type Change Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_TYPE_CHANGE_OPERATION_OPERATION_COUNT = DW_GROUP_OPERATION_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.operation.DwEvolutionOperationModel <em>Dw Evolution Operation Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Evolution Operation Model</em>'.
	 * @see de.darwinspl.feature.operation.DwEvolutionOperationModel
	 * @generated
	 */
	EClass getDwEvolutionOperationModel();

	/**
	 * Returns the meta object for the containment reference list '{@link de.darwinspl.feature.operation.DwEvolutionOperationModel#getEvolutionOperations <em>Evolution Operations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Evolution Operations</em>'.
	 * @see de.darwinspl.feature.operation.DwEvolutionOperationModel#getEvolutionOperations()
	 * @see #getDwEvolutionOperationModel()
	 * @generated
	 */
	EReference getDwEvolutionOperationModel_EvolutionOperations();

	/**
	 * Returns the meta object for the containment reference list '{@link de.darwinspl.feature.operation.DwEvolutionOperationModel#getContributors <em>Contributors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Contributors</em>'.
	 * @see de.darwinspl.feature.operation.DwEvolutionOperationModel#getContributors()
	 * @see #getDwEvolutionOperationModel()
	 * @generated
	 */
	EReference getDwEvolutionOperationModel_Contributors();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.operation.DwEvolutionOperation <em>Dw Evolution Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Evolution Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwEvolutionOperation
	 * @generated
	 */
	EClass getDwEvolutionOperation();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.operation.DwEvolutionOperation#getOperationDate <em>Operation Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operation Date</em>'.
	 * @see de.darwinspl.feature.operation.DwEvolutionOperation#getOperationDate()
	 * @see #getDwEvolutionOperation()
	 * @generated
	 */
	EAttribute getDwEvolutionOperation_OperationDate();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.operation.DwEvolutionOperation#getCommandStackIndex <em>Command Stack Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Command Stack Index</em>'.
	 * @see de.darwinspl.feature.operation.DwEvolutionOperation#getCommandStackIndex()
	 * @see #getDwEvolutionOperation()
	 * @generated
	 */
	EAttribute getDwEvolutionOperation_CommandStackIndex();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwEvolutionOperation#getFeatureModel <em>Feature Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Feature Model</em>'.
	 * @see de.darwinspl.feature.operation.DwEvolutionOperation#getFeatureModel()
	 * @see #getDwEvolutionOperation()
	 * @generated
	 */
	EReference getDwEvolutionOperation_FeatureModel();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwEvolutionOperation#getContributor <em>Contributor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Contributor</em>'.
	 * @see de.darwinspl.feature.operation.DwEvolutionOperation#getContributor()
	 * @see #getDwEvolutionOperation()
	 * @generated
	 */
	EReference getDwEvolutionOperation_Contributor();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.operation.DwEvolutionOperation#isIntermediateOperation <em>Intermediate Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Intermediate Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwEvolutionOperation#isIntermediateOperation()
	 * @see #getDwEvolutionOperation()
	 * @generated
	 */
	EAttribute getDwEvolutionOperation_IntermediateOperation();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.operation.DwFeatureOperation <em>Dw Feature Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Feature Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureOperation
	 * @generated
	 */
	EClass getDwFeatureOperation();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwFeatureOperation#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Feature</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureOperation#getFeature()
	 * @see #getDwFeatureOperation()
	 * @generated
	 */
	EReference getDwFeatureOperation_Feature();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.operation.DwFeatureCreateOperation <em>Dw Feature Create Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Feature Create Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureCreateOperation
	 * @generated
	 */
	EClass getDwFeatureCreateOperation();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.feature.operation.DwFeatureCreateOperation#getFeatureAddOperation <em>Feature Add Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Feature Add Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureCreateOperation#getFeatureAddOperation()
	 * @see #getDwFeatureCreateOperation()
	 * @generated
	 */
	EReference getDwFeatureCreateOperation_FeatureAddOperation();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.operation.DwFeatureAddOperation <em>Dw Feature Add Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Feature Add Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureAddOperation
	 * @generated
	 */
	EClass getDwFeatureAddOperation();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwFeatureAddOperation#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parent</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureAddOperation#getParent()
	 * @see #getDwFeatureAddOperation()
	 * @generated
	 */
	EReference getDwFeatureAddOperation_Parent();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.operation.DwFeatureAddOperation#isForceCreateParentGroup <em>Force Create Parent Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Force Create Parent Group</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureAddOperation#isForceCreateParentGroup()
	 * @see #getDwFeatureAddOperation()
	 * @generated
	 */
	EAttribute getDwFeatureAddOperation_ForceCreateParentGroup();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwFeatureAddOperation#getOldGroupComposition <em>Old Group Composition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Old Group Composition</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureAddOperation#getOldGroupComposition()
	 * @see #getDwFeatureAddOperation()
	 * @generated
	 */
	EReference getDwFeatureAddOperation_OldGroupComposition();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwFeatureAddOperation#getNewGroupComposition <em>New Group Composition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>New Group Composition</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureAddOperation#getNewGroupComposition()
	 * @see #getDwFeatureAddOperation()
	 * @generated
	 */
	EReference getDwFeatureAddOperation_NewGroupComposition();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.feature.operation.DwFeatureAddOperation#getGroupCreateOperation <em>Group Create Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Group Create Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureAddOperation#getGroupCreateOperation()
	 * @see #getDwFeatureAddOperation()
	 * @generated
	 */
	EReference getDwFeatureAddOperation_GroupCreateOperation();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.feature.operation.DwFeatureAddOperation#getGroupTypeChangeOperation <em>Group Type Change Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Group Type Change Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureAddOperation#getGroupTypeChangeOperation()
	 * @see #getDwFeatureAddOperation()
	 * @generated
	 */
	EReference getDwFeatureAddOperation_GroupTypeChangeOperation();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwFeatureAddOperation#getGroupToAddTo <em>Group To Add To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Group To Add To</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureAddOperation#getGroupToAddTo()
	 * @see #getDwFeatureAddOperation()
	 * @generated
	 */
	EReference getDwFeatureAddOperation_GroupToAddTo();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.feature.operation.DwFeatureAddOperation#getConsequentFeatureTypeChangeOperation <em>Consequent Feature Type Change Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Consequent Feature Type Change Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureAddOperation#getConsequentFeatureTypeChangeOperation()
	 * @see #getDwFeatureAddOperation()
	 * @generated
	 */
	EReference getDwFeatureAddOperation_ConsequentFeatureTypeChangeOperation();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.operation.DwFeatureDeleteOperation <em>Dw Feature Delete Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Feature Delete Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureDeleteOperation
	 * @generated
	 */
	EClass getDwFeatureDeleteOperation();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.operation.DwFeatureDeleteOperation#getOldFeatureValidUntil <em>Old Feature Valid Until</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Old Feature Valid Until</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureDeleteOperation#getOldFeatureValidUntil()
	 * @see #getDwFeatureDeleteOperation()
	 * @generated
	 */
	EAttribute getDwFeatureDeleteOperation_OldFeatureValidUntil();

	/**
	 * Returns the meta object for the containment reference list '{@link de.darwinspl.feature.operation.DwFeatureDeleteOperation#getConsequentGroupOperations <em>Consequent Group Operations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Consequent Group Operations</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureDeleteOperation#getConsequentGroupOperations()
	 * @see #getDwFeatureDeleteOperation()
	 * @generated
	 */
	EReference getDwFeatureDeleteOperation_ConsequentGroupOperations();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.feature.operation.DwFeatureDeleteOperation#getDetachOperation <em>Detach Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Detach Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureDeleteOperation#getDetachOperation()
	 * @see #getDwFeatureDeleteOperation()
	 * @generated
	 */
	EReference getDwFeatureDeleteOperation_DetachOperation();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.operation.DwFeatureDetachOperation <em>Dw Feature Detach Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Feature Detach Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureDetachOperation
	 * @generated
	 */
	EClass getDwFeatureDetachOperation();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwFeatureDetachOperation#getOldGroupMembership <em>Old Group Membership</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Old Group Membership</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureDetachOperation#getOldGroupMembership()
	 * @see #getDwFeatureDetachOperation()
	 * @generated
	 */
	EReference getDwFeatureDetachOperation_OldGroupMembership();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwFeatureDetachOperation#getNewGroupMembership <em>New Group Membership</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>New Group Membership</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureDetachOperation#getNewGroupMembership()
	 * @see #getDwFeatureDetachOperation()
	 * @generated
	 */
	EReference getDwFeatureDetachOperation_NewGroupMembership();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.operation.DwFeatureDetachOperation#getOldValidUntil <em>Old Valid Until</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Old Valid Until</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureDetachOperation#getOldValidUntil()
	 * @see #getDwFeatureDetachOperation()
	 * @generated
	 */
	EAttribute getDwFeatureDetachOperation_OldValidUntil();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.feature.operation.DwFeatureDetachOperation#getConsequentGroupOperation <em>Consequent Group Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Consequent Group Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureDetachOperation#getConsequentGroupOperation()
	 * @see #getDwFeatureDetachOperation()
	 * @generated
	 */
	EReference getDwFeatureDetachOperation_ConsequentGroupOperation();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.operation.DwFeatureMoveOperation <em>Dw Feature Move Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Feature Move Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureMoveOperation
	 * @generated
	 */
	EClass getDwFeatureMoveOperation();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.feature.operation.DwFeatureMoveOperation#getDetachOperation <em>Detach Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Detach Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureMoveOperation#getDetachOperation()
	 * @see #getDwFeatureMoveOperation()
	 * @generated
	 */
	EReference getDwFeatureMoveOperation_DetachOperation();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.feature.operation.DwFeatureMoveOperation#getAddOperation <em>Add Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Add Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureMoveOperation#getAddOperation()
	 * @see #getDwFeatureMoveOperation()
	 * @generated
	 */
	EReference getDwFeatureMoveOperation_AddOperation();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.operation.DwFeatureRenameOperation <em>Dw Feature Rename Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Feature Rename Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureRenameOperation
	 * @generated
	 */
	EClass getDwFeatureRenameOperation();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwFeatureRenameOperation#getNewName <em>New Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>New Name</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureRenameOperation#getNewName()
	 * @see #getDwFeatureRenameOperation()
	 * @generated
	 */
	EReference getDwFeatureRenameOperation_NewName();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.operation.DwFeatureRenameOperation#getNewNameString <em>New Name String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>New Name String</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureRenameOperation#getNewNameString()
	 * @see #getDwFeatureRenameOperation()
	 * @generated
	 */
	EAttribute getDwFeatureRenameOperation_NewNameString();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwFeatureRenameOperation#getOldName <em>Old Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Old Name</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureRenameOperation#getOldName()
	 * @see #getDwFeatureRenameOperation()
	 * @generated
	 */
	EReference getDwFeatureRenameOperation_OldName();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.operation.DwFeatureRenameOperation#getOldNameString <em>Old Name String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Old Name String</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureRenameOperation#getOldNameString()
	 * @see #getDwFeatureRenameOperation()
	 * @generated
	 */
	EAttribute getDwFeatureRenameOperation_OldNameString();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.operation.DwFeatureTypeChangeOperation <em>Dw Feature Type Change Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Feature Type Change Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureTypeChangeOperation
	 * @generated
	 */
	EClass getDwFeatureTypeChangeOperation();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwFeatureTypeChangeOperation#getNewFeatureType <em>New Feature Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>New Feature Type</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureTypeChangeOperation#getNewFeatureType()
	 * @see #getDwFeatureTypeChangeOperation()
	 * @generated
	 */
	EReference getDwFeatureTypeChangeOperation_NewFeatureType();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.operation.DwFeatureTypeChangeOperation#getNewFeatureTypeEnum <em>New Feature Type Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>New Feature Type Enum</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureTypeChangeOperation#getNewFeatureTypeEnum()
	 * @see #getDwFeatureTypeChangeOperation()
	 * @generated
	 */
	EAttribute getDwFeatureTypeChangeOperation_NewFeatureTypeEnum();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwFeatureTypeChangeOperation#getOldFeatureType <em>Old Feature Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Old Feature Type</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureTypeChangeOperation#getOldFeatureType()
	 * @see #getDwFeatureTypeChangeOperation()
	 * @generated
	 */
	EReference getDwFeatureTypeChangeOperation_OldFeatureType();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.operation.DwFeatureAttributeOperation <em>Dw Feature Attribute Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Feature Attribute Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureAttributeOperation
	 * @generated
	 */
	EClass getDwFeatureAttributeOperation();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwFeatureAttributeOperation#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureAttributeOperation#getAttribute()
	 * @see #getDwFeatureAttributeOperation()
	 * @generated
	 */
	EReference getDwFeatureAttributeOperation_Attribute();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.operation.DwFeatureAttributeCreateOperation <em>Dw Feature Attribute Create Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Feature Attribute Create Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureAttributeCreateOperation
	 * @generated
	 */
	EClass getDwFeatureAttributeCreateOperation();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.operation.DwFeatureAttributeDeleteOperation <em>Dw Feature Attribute Delete Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Feature Attribute Delete Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureAttributeDeleteOperation
	 * @generated
	 */
	EClass getDwFeatureAttributeDeleteOperation();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.operation.DwFeatureAttributeRenameOperation <em>Dw Feature Attribute Rename Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Feature Attribute Rename Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureAttributeRenameOperation
	 * @generated
	 */
	EClass getDwFeatureAttributeRenameOperation();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwFeatureAttributeRenameOperation#getOldName <em>Old Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Old Name</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureAttributeRenameOperation#getOldName()
	 * @see #getDwFeatureAttributeRenameOperation()
	 * @generated
	 */
	EReference getDwFeatureAttributeRenameOperation_OldName();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwFeatureAttributeRenameOperation#getNewName <em>New Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>New Name</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureAttributeRenameOperation#getNewName()
	 * @see #getDwFeatureAttributeRenameOperation()
	 * @generated
	 */
	EReference getDwFeatureAttributeRenameOperation_NewName();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.operation.DwFeatureVersionOperation <em>Dw Feature Version Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Feature Version Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureVersionOperation
	 * @generated
	 */
	EClass getDwFeatureVersionOperation();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwFeatureVersionOperation#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Version</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureVersionOperation#getVersion()
	 * @see #getDwFeatureVersionOperation()
	 * @generated
	 */
	EReference getDwFeatureVersionOperation_Version();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.operation.DwFeatureVersionCreateOperation <em>Dw Feature Version Create Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Feature Version Create Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureVersionCreateOperation
	 * @generated
	 */
	EClass getDwFeatureVersionCreateOperation();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwFeatureVersionCreateOperation#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parent</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureVersionCreateOperation#getParent()
	 * @see #getDwFeatureVersionCreateOperation()
	 * @generated
	 */
	EReference getDwFeatureVersionCreateOperation_Parent();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.operation.DwFeatureVersionCreateOperation#isUseSameBranch <em>Use Same Branch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Use Same Branch</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureVersionCreateOperation#isUseSameBranch()
	 * @see #getDwFeatureVersionCreateOperation()
	 * @generated
	 */
	EAttribute getDwFeatureVersionCreateOperation_UseSameBranch();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.operation.DwFeatureVersionCreateOperation#isWireAndAddAfter <em>Wire And Add After</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wire And Add After</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureVersionCreateOperation#isWireAndAddAfter()
	 * @see #getDwFeatureVersionCreateOperation()
	 * @generated
	 */
	EAttribute getDwFeatureVersionCreateOperation_WireAndAddAfter();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.operation.DwFeatureVersionDeleteOperation <em>Dw Feature Version Delete Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Feature Version Delete Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureVersionDeleteOperation
	 * @generated
	 */
	EClass getDwFeatureVersionDeleteOperation();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwFeatureVersionDeleteOperation#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parent</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureVersionDeleteOperation#getParent()
	 * @see #getDwFeatureVersionDeleteOperation()
	 * @generated
	 */
	EReference getDwFeatureVersionDeleteOperation_Parent();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.operation.DwFeatureVersionDeleteOperation#getOldValidUntil <em>Old Valid Until</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Old Valid Until</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureVersionDeleteOperation#getOldValidUntil()
	 * @see #getDwFeatureVersionDeleteOperation()
	 * @generated
	 */
	EAttribute getDwFeatureVersionDeleteOperation_OldValidUntil();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.operation.DwFeatureVersionRenameOperation <em>Dw Feature Version Rename Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Feature Version Rename Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureVersionRenameOperation
	 * @generated
	 */
	EClass getDwFeatureVersionRenameOperation();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.operation.DwFeatureVersionRenameOperation#getOldNumber <em>Old Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Old Number</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureVersionRenameOperation#getOldNumber()
	 * @see #getDwFeatureVersionRenameOperation()
	 * @generated
	 */
	EAttribute getDwFeatureVersionRenameOperation_OldNumber();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.operation.DwFeatureVersionRenameOperation#getNewNumber <em>New Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>New Number</em>'.
	 * @see de.darwinspl.feature.operation.DwFeatureVersionRenameOperation#getNewNumber()
	 * @see #getDwFeatureVersionRenameOperation()
	 * @generated
	 */
	EAttribute getDwFeatureVersionRenameOperation_NewNumber();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.operation.DwGroupOperation <em>Dw Group Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Group Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwGroupOperation
	 * @generated
	 */
	EClass getDwGroupOperation();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwGroupOperation#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Group</em>'.
	 * @see de.darwinspl.feature.operation.DwGroupOperation#getGroup()
	 * @see #getDwGroupOperation()
	 * @generated
	 */
	EReference getDwGroupOperation_Group();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.operation.DwGroupCreateOperation <em>Dw Group Create Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Group Create Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwGroupCreateOperation
	 * @generated
	 */
	EClass getDwGroupCreateOperation();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.feature.operation.DwGroupCreateOperation#getGroupAddOperation <em>Group Add Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Group Add Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwGroupCreateOperation#getGroupAddOperation()
	 * @see #getDwGroupCreateOperation()
	 * @generated
	 */
	EReference getDwGroupCreateOperation_GroupAddOperation();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.operation.DwGroupAddOperation <em>Dw Group Add Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Group Add Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwGroupAddOperation
	 * @generated
	 */
	EClass getDwGroupAddOperation();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwGroupAddOperation#getParentFeature <em>Parent Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parent Feature</em>'.
	 * @see de.darwinspl.feature.operation.DwGroupAddOperation#getParentFeature()
	 * @see #getDwGroupAddOperation()
	 * @generated
	 */
	EReference getDwGroupAddOperation_ParentFeature();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwGroupAddOperation#getNewContainer <em>New Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>New Container</em>'.
	 * @see de.darwinspl.feature.operation.DwGroupAddOperation#getNewContainer()
	 * @see #getDwGroupAddOperation()
	 * @generated
	 */
	EReference getDwGroupAddOperation_NewContainer();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.operation.DwGroupDeleteOperation <em>Dw Group Delete Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Group Delete Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwGroupDeleteOperation
	 * @generated
	 */
	EClass getDwGroupDeleteOperation();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.operation.DwGroupDeleteOperation#getOldGroupValidUntil <em>Old Group Valid Until</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Old Group Valid Until</em>'.
	 * @see de.darwinspl.feature.operation.DwGroupDeleteOperation#getOldGroupValidUntil()
	 * @see #getDwGroupDeleteOperation()
	 * @generated
	 */
	EAttribute getDwGroupDeleteOperation_OldGroupValidUntil();

	/**
	 * Returns the meta object for the containment reference list '{@link de.darwinspl.feature.operation.DwGroupDeleteOperation#getConsequentFeatureOperations <em>Consequent Feature Operations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Consequent Feature Operations</em>'.
	 * @see de.darwinspl.feature.operation.DwGroupDeleteOperation#getConsequentFeatureOperations()
	 * @see #getDwGroupDeleteOperation()
	 * @generated
	 */
	EReference getDwGroupDeleteOperation_ConsequentFeatureOperations();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.feature.operation.DwGroupDeleteOperation#getDetachOperation <em>Detach Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Detach Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwGroupDeleteOperation#getDetachOperation()
	 * @see #getDwGroupDeleteOperation()
	 * @generated
	 */
	EReference getDwGroupDeleteOperation_DetachOperation();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.operation.DwGroupDetachOperation <em>Dw Group Detach Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Group Detach Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwGroupDetachOperation
	 * @generated
	 */
	EClass getDwGroupDetachOperation();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwGroupDetachOperation#getOldContainer <em>Old Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Old Container</em>'.
	 * @see de.darwinspl.feature.operation.DwGroupDetachOperation#getOldContainer()
	 * @see #getDwGroupDetachOperation()
	 * @generated
	 */
	EReference getDwGroupDetachOperation_OldContainer();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwGroupDetachOperation#getOldParent <em>Old Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Old Parent</em>'.
	 * @see de.darwinspl.feature.operation.DwGroupDetachOperation#getOldParent()
	 * @see #getDwGroupDetachOperation()
	 * @generated
	 */
	EReference getDwGroupDetachOperation_OldParent();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.operation.DwGroupMoveOperation <em>Dw Group Move Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Group Move Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwGroupMoveOperation
	 * @generated
	 */
	EClass getDwGroupMoveOperation();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.feature.operation.DwGroupMoveOperation#getGroupDetachOperation <em>Group Detach Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Group Detach Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwGroupMoveOperation#getGroupDetachOperation()
	 * @see #getDwGroupMoveOperation()
	 * @generated
	 */
	EReference getDwGroupMoveOperation_GroupDetachOperation();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.feature.operation.DwGroupMoveOperation#getGroupAddOperation <em>Group Add Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Group Add Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwGroupMoveOperation#getGroupAddOperation()
	 * @see #getDwGroupMoveOperation()
	 * @generated
	 */
	EReference getDwGroupMoveOperation_GroupAddOperation();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.operation.DwGroupTypeChangeOperation <em>Dw Group Type Change Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Group Type Change Operation</em>'.
	 * @see de.darwinspl.feature.operation.DwGroupTypeChangeOperation
	 * @generated
	 */
	EClass getDwGroupTypeChangeOperation();

	/**
	 * Returns the meta object for the containment reference list '{@link de.darwinspl.feature.operation.DwGroupTypeChangeOperation#getConsequentOperations <em>Consequent Operations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Consequent Operations</em>'.
	 * @see de.darwinspl.feature.operation.DwGroupTypeChangeOperation#getConsequentOperations()
	 * @see #getDwGroupTypeChangeOperation()
	 * @generated
	 */
	EReference getDwGroupTypeChangeOperation_ConsequentOperations();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwGroupTypeChangeOperation#getNewGroupType <em>New Group Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>New Group Type</em>'.
	 * @see de.darwinspl.feature.operation.DwGroupTypeChangeOperation#getNewGroupType()
	 * @see #getDwGroupTypeChangeOperation()
	 * @generated
	 */
	EReference getDwGroupTypeChangeOperation_NewGroupType();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.operation.DwGroupTypeChangeOperation#getNewGroupTypeEnum <em>New Group Type Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>New Group Type Enum</em>'.
	 * @see de.darwinspl.feature.operation.DwGroupTypeChangeOperation#getNewGroupTypeEnum()
	 * @see #getDwGroupTypeChangeOperation()
	 * @generated
	 */
	EAttribute getDwGroupTypeChangeOperation_NewGroupTypeEnum();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.operation.DwGroupTypeChangeOperation#getOldGroupType <em>Old Group Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Old Group Type</em>'.
	 * @see de.darwinspl.feature.operation.DwGroupTypeChangeOperation#getOldGroupType()
	 * @see #getDwGroupTypeChangeOperation()
	 * @generated
	 */
	EReference getDwGroupTypeChangeOperation_OldGroupType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DwOperationFactory getDwOperationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.operation.impl.DwEvolutionOperationModelImpl <em>Dw Evolution Operation Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.operation.impl.DwEvolutionOperationModelImpl
		 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwEvolutionOperationModel()
		 * @generated
		 */
		EClass DW_EVOLUTION_OPERATION_MODEL = eINSTANCE.getDwEvolutionOperationModel();

		/**
		 * The meta object literal for the '<em><b>Evolution Operations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_EVOLUTION_OPERATION_MODEL__EVOLUTION_OPERATIONS = eINSTANCE.getDwEvolutionOperationModel_EvolutionOperations();

		/**
		 * The meta object literal for the '<em><b>Contributors</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_EVOLUTION_OPERATION_MODEL__CONTRIBUTORS = eINSTANCE.getDwEvolutionOperationModel_Contributors();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.operation.impl.DwEvolutionOperationImpl <em>Dw Evolution Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.operation.impl.DwEvolutionOperationImpl
		 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwEvolutionOperation()
		 * @generated
		 */
		EClass DW_EVOLUTION_OPERATION = eINSTANCE.getDwEvolutionOperation();

		/**
		 * The meta object literal for the '<em><b>Operation Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_EVOLUTION_OPERATION__OPERATION_DATE = eINSTANCE.getDwEvolutionOperation_OperationDate();

		/**
		 * The meta object literal for the '<em><b>Command Stack Index</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_EVOLUTION_OPERATION__COMMAND_STACK_INDEX = eINSTANCE.getDwEvolutionOperation_CommandStackIndex();

		/**
		 * The meta object literal for the '<em><b>Feature Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_EVOLUTION_OPERATION__FEATURE_MODEL = eINSTANCE.getDwEvolutionOperation_FeatureModel();

		/**
		 * The meta object literal for the '<em><b>Contributor</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_EVOLUTION_OPERATION__CONTRIBUTOR = eINSTANCE.getDwEvolutionOperation_Contributor();

		/**
		 * The meta object literal for the '<em><b>Intermediate Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_EVOLUTION_OPERATION__INTERMEDIATE_OPERATION = eINSTANCE.getDwEvolutionOperation_IntermediateOperation();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.operation.impl.DwFeatureOperationImpl <em>Dw Feature Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.operation.impl.DwFeatureOperationImpl
		 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureOperation()
		 * @generated
		 */
		EClass DW_FEATURE_OPERATION = eINSTANCE.getDwFeatureOperation();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_OPERATION__FEATURE = eINSTANCE.getDwFeatureOperation_Feature();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.operation.impl.DwFeatureCreateOperationImpl <em>Dw Feature Create Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.operation.impl.DwFeatureCreateOperationImpl
		 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureCreateOperation()
		 * @generated
		 */
		EClass DW_FEATURE_CREATE_OPERATION = eINSTANCE.getDwFeatureCreateOperation();

		/**
		 * The meta object literal for the '<em><b>Feature Add Operation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_CREATE_OPERATION__FEATURE_ADD_OPERATION = eINSTANCE.getDwFeatureCreateOperation_FeatureAddOperation();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.operation.impl.DwFeatureAddOperationImpl <em>Dw Feature Add Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.operation.impl.DwFeatureAddOperationImpl
		 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureAddOperation()
		 * @generated
		 */
		EClass DW_FEATURE_ADD_OPERATION = eINSTANCE.getDwFeatureAddOperation();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_ADD_OPERATION__PARENT = eINSTANCE.getDwFeatureAddOperation_Parent();

		/**
		 * The meta object literal for the '<em><b>Force Create Parent Group</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_FEATURE_ADD_OPERATION__FORCE_CREATE_PARENT_GROUP = eINSTANCE.getDwFeatureAddOperation_ForceCreateParentGroup();

		/**
		 * The meta object literal for the '<em><b>Old Group Composition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_ADD_OPERATION__OLD_GROUP_COMPOSITION = eINSTANCE.getDwFeatureAddOperation_OldGroupComposition();

		/**
		 * The meta object literal for the '<em><b>New Group Composition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_ADD_OPERATION__NEW_GROUP_COMPOSITION = eINSTANCE.getDwFeatureAddOperation_NewGroupComposition();

		/**
		 * The meta object literal for the '<em><b>Group Create Operation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_ADD_OPERATION__GROUP_CREATE_OPERATION = eINSTANCE.getDwFeatureAddOperation_GroupCreateOperation();

		/**
		 * The meta object literal for the '<em><b>Group Type Change Operation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_ADD_OPERATION__GROUP_TYPE_CHANGE_OPERATION = eINSTANCE.getDwFeatureAddOperation_GroupTypeChangeOperation();

		/**
		 * The meta object literal for the '<em><b>Group To Add To</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_ADD_OPERATION__GROUP_TO_ADD_TO = eINSTANCE.getDwFeatureAddOperation_GroupToAddTo();

		/**
		 * The meta object literal for the '<em><b>Consequent Feature Type Change Operation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_ADD_OPERATION__CONSEQUENT_FEATURE_TYPE_CHANGE_OPERATION = eINSTANCE.getDwFeatureAddOperation_ConsequentFeatureTypeChangeOperation();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.operation.impl.DwFeatureDeleteOperationImpl <em>Dw Feature Delete Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.operation.impl.DwFeatureDeleteOperationImpl
		 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureDeleteOperation()
		 * @generated
		 */
		EClass DW_FEATURE_DELETE_OPERATION = eINSTANCE.getDwFeatureDeleteOperation();

		/**
		 * The meta object literal for the '<em><b>Old Feature Valid Until</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_FEATURE_DELETE_OPERATION__OLD_FEATURE_VALID_UNTIL = eINSTANCE.getDwFeatureDeleteOperation_OldFeatureValidUntil();

		/**
		 * The meta object literal for the '<em><b>Consequent Group Operations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_DELETE_OPERATION__CONSEQUENT_GROUP_OPERATIONS = eINSTANCE.getDwFeatureDeleteOperation_ConsequentGroupOperations();

		/**
		 * The meta object literal for the '<em><b>Detach Operation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_DELETE_OPERATION__DETACH_OPERATION = eINSTANCE.getDwFeatureDeleteOperation_DetachOperation();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.operation.impl.DwFeatureDetachOperationImpl <em>Dw Feature Detach Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.operation.impl.DwFeatureDetachOperationImpl
		 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureDetachOperation()
		 * @generated
		 */
		EClass DW_FEATURE_DETACH_OPERATION = eINSTANCE.getDwFeatureDetachOperation();

		/**
		 * The meta object literal for the '<em><b>Old Group Membership</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_DETACH_OPERATION__OLD_GROUP_MEMBERSHIP = eINSTANCE.getDwFeatureDetachOperation_OldGroupMembership();

		/**
		 * The meta object literal for the '<em><b>New Group Membership</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_DETACH_OPERATION__NEW_GROUP_MEMBERSHIP = eINSTANCE.getDwFeatureDetachOperation_NewGroupMembership();

		/**
		 * The meta object literal for the '<em><b>Old Valid Until</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_FEATURE_DETACH_OPERATION__OLD_VALID_UNTIL = eINSTANCE.getDwFeatureDetachOperation_OldValidUntil();

		/**
		 * The meta object literal for the '<em><b>Consequent Group Operation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_DETACH_OPERATION__CONSEQUENT_GROUP_OPERATION = eINSTANCE.getDwFeatureDetachOperation_ConsequentGroupOperation();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.operation.impl.DwFeatureMoveOperationImpl <em>Dw Feature Move Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.operation.impl.DwFeatureMoveOperationImpl
		 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureMoveOperation()
		 * @generated
		 */
		EClass DW_FEATURE_MOVE_OPERATION = eINSTANCE.getDwFeatureMoveOperation();

		/**
		 * The meta object literal for the '<em><b>Detach Operation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_MOVE_OPERATION__DETACH_OPERATION = eINSTANCE.getDwFeatureMoveOperation_DetachOperation();

		/**
		 * The meta object literal for the '<em><b>Add Operation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_MOVE_OPERATION__ADD_OPERATION = eINSTANCE.getDwFeatureMoveOperation_AddOperation();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.operation.impl.DwFeatureRenameOperationImpl <em>Dw Feature Rename Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.operation.impl.DwFeatureRenameOperationImpl
		 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureRenameOperation()
		 * @generated
		 */
		EClass DW_FEATURE_RENAME_OPERATION = eINSTANCE.getDwFeatureRenameOperation();

		/**
		 * The meta object literal for the '<em><b>New Name</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_RENAME_OPERATION__NEW_NAME = eINSTANCE.getDwFeatureRenameOperation_NewName();

		/**
		 * The meta object literal for the '<em><b>New Name String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_FEATURE_RENAME_OPERATION__NEW_NAME_STRING = eINSTANCE.getDwFeatureRenameOperation_NewNameString();

		/**
		 * The meta object literal for the '<em><b>Old Name</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_RENAME_OPERATION__OLD_NAME = eINSTANCE.getDwFeatureRenameOperation_OldName();

		/**
		 * The meta object literal for the '<em><b>Old Name String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_FEATURE_RENAME_OPERATION__OLD_NAME_STRING = eINSTANCE.getDwFeatureRenameOperation_OldNameString();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.operation.impl.DwFeatureTypeChangeOperationImpl <em>Dw Feature Type Change Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.operation.impl.DwFeatureTypeChangeOperationImpl
		 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureTypeChangeOperation()
		 * @generated
		 */
		EClass DW_FEATURE_TYPE_CHANGE_OPERATION = eINSTANCE.getDwFeatureTypeChangeOperation();

		/**
		 * The meta object literal for the '<em><b>New Feature Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_TYPE_CHANGE_OPERATION__NEW_FEATURE_TYPE = eINSTANCE.getDwFeatureTypeChangeOperation_NewFeatureType();

		/**
		 * The meta object literal for the '<em><b>New Feature Type Enum</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_FEATURE_TYPE_CHANGE_OPERATION__NEW_FEATURE_TYPE_ENUM = eINSTANCE.getDwFeatureTypeChangeOperation_NewFeatureTypeEnum();

		/**
		 * The meta object literal for the '<em><b>Old Feature Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_TYPE_CHANGE_OPERATION__OLD_FEATURE_TYPE = eINSTANCE.getDwFeatureTypeChangeOperation_OldFeatureType();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.operation.impl.DwFeatureAttributeOperationImpl <em>Dw Feature Attribute Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.operation.impl.DwFeatureAttributeOperationImpl
		 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureAttributeOperation()
		 * @generated
		 */
		EClass DW_FEATURE_ATTRIBUTE_OPERATION = eINSTANCE.getDwFeatureAttributeOperation();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_ATTRIBUTE_OPERATION__ATTRIBUTE = eINSTANCE.getDwFeatureAttributeOperation_Attribute();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.operation.impl.DwFeatureAttributeCreateOperationImpl <em>Dw Feature Attribute Create Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.operation.impl.DwFeatureAttributeCreateOperationImpl
		 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureAttributeCreateOperation()
		 * @generated
		 */
		EClass DW_FEATURE_ATTRIBUTE_CREATE_OPERATION = eINSTANCE.getDwFeatureAttributeCreateOperation();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.operation.impl.DwFeatureAttributeDeleteOperationImpl <em>Dw Feature Attribute Delete Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.operation.impl.DwFeatureAttributeDeleteOperationImpl
		 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureAttributeDeleteOperation()
		 * @generated
		 */
		EClass DW_FEATURE_ATTRIBUTE_DELETE_OPERATION = eINSTANCE.getDwFeatureAttributeDeleteOperation();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.operation.impl.DwFeatureAttributeRenameOperationImpl <em>Dw Feature Attribute Rename Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.operation.impl.DwFeatureAttributeRenameOperationImpl
		 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureAttributeRenameOperation()
		 * @generated
		 */
		EClass DW_FEATURE_ATTRIBUTE_RENAME_OPERATION = eINSTANCE.getDwFeatureAttributeRenameOperation();

		/**
		 * The meta object literal for the '<em><b>Old Name</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__OLD_NAME = eINSTANCE.getDwFeatureAttributeRenameOperation_OldName();

		/**
		 * The meta object literal for the '<em><b>New Name</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_ATTRIBUTE_RENAME_OPERATION__NEW_NAME = eINSTANCE.getDwFeatureAttributeRenameOperation_NewName();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.operation.impl.DwFeatureVersionOperationImpl <em>Dw Feature Version Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.operation.impl.DwFeatureVersionOperationImpl
		 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureVersionOperation()
		 * @generated
		 */
		EClass DW_FEATURE_VERSION_OPERATION = eINSTANCE.getDwFeatureVersionOperation();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_VERSION_OPERATION__VERSION = eINSTANCE.getDwFeatureVersionOperation_Version();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.operation.impl.DwFeatureVersionCreateOperationImpl <em>Dw Feature Version Create Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.operation.impl.DwFeatureVersionCreateOperationImpl
		 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureVersionCreateOperation()
		 * @generated
		 */
		EClass DW_FEATURE_VERSION_CREATE_OPERATION = eINSTANCE.getDwFeatureVersionCreateOperation();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_VERSION_CREATE_OPERATION__PARENT = eINSTANCE.getDwFeatureVersionCreateOperation_Parent();

		/**
		 * The meta object literal for the '<em><b>Use Same Branch</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_FEATURE_VERSION_CREATE_OPERATION__USE_SAME_BRANCH = eINSTANCE.getDwFeatureVersionCreateOperation_UseSameBranch();

		/**
		 * The meta object literal for the '<em><b>Wire And Add After</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_FEATURE_VERSION_CREATE_OPERATION__WIRE_AND_ADD_AFTER = eINSTANCE.getDwFeatureVersionCreateOperation_WireAndAddAfter();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.operation.impl.DwFeatureVersionDeleteOperationImpl <em>Dw Feature Version Delete Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.operation.impl.DwFeatureVersionDeleteOperationImpl
		 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureVersionDeleteOperation()
		 * @generated
		 */
		EClass DW_FEATURE_VERSION_DELETE_OPERATION = eINSTANCE.getDwFeatureVersionDeleteOperation();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_VERSION_DELETE_OPERATION__PARENT = eINSTANCE.getDwFeatureVersionDeleteOperation_Parent();

		/**
		 * The meta object literal for the '<em><b>Old Valid Until</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_FEATURE_VERSION_DELETE_OPERATION__OLD_VALID_UNTIL = eINSTANCE.getDwFeatureVersionDeleteOperation_OldValidUntil();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.operation.impl.DwFeatureVersionRenameOperationImpl <em>Dw Feature Version Rename Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.operation.impl.DwFeatureVersionRenameOperationImpl
		 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwFeatureVersionRenameOperation()
		 * @generated
		 */
		EClass DW_FEATURE_VERSION_RENAME_OPERATION = eINSTANCE.getDwFeatureVersionRenameOperation();

		/**
		 * The meta object literal for the '<em><b>Old Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_FEATURE_VERSION_RENAME_OPERATION__OLD_NUMBER = eINSTANCE.getDwFeatureVersionRenameOperation_OldNumber();

		/**
		 * The meta object literal for the '<em><b>New Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_FEATURE_VERSION_RENAME_OPERATION__NEW_NUMBER = eINSTANCE.getDwFeatureVersionRenameOperation_NewNumber();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.operation.impl.DwGroupOperationImpl <em>Dw Group Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.operation.impl.DwGroupOperationImpl
		 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwGroupOperation()
		 * @generated
		 */
		EClass DW_GROUP_OPERATION = eINSTANCE.getDwGroupOperation();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_GROUP_OPERATION__GROUP = eINSTANCE.getDwGroupOperation_Group();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.operation.impl.DwGroupCreateOperationImpl <em>Dw Group Create Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.operation.impl.DwGroupCreateOperationImpl
		 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwGroupCreateOperation()
		 * @generated
		 */
		EClass DW_GROUP_CREATE_OPERATION = eINSTANCE.getDwGroupCreateOperation();

		/**
		 * The meta object literal for the '<em><b>Group Add Operation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_GROUP_CREATE_OPERATION__GROUP_ADD_OPERATION = eINSTANCE.getDwGroupCreateOperation_GroupAddOperation();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.operation.impl.DwGroupAddOperationImpl <em>Dw Group Add Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.operation.impl.DwGroupAddOperationImpl
		 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwGroupAddOperation()
		 * @generated
		 */
		EClass DW_GROUP_ADD_OPERATION = eINSTANCE.getDwGroupAddOperation();

		/**
		 * The meta object literal for the '<em><b>Parent Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_GROUP_ADD_OPERATION__PARENT_FEATURE = eINSTANCE.getDwGroupAddOperation_ParentFeature();

		/**
		 * The meta object literal for the '<em><b>New Container</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_GROUP_ADD_OPERATION__NEW_CONTAINER = eINSTANCE.getDwGroupAddOperation_NewContainer();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.operation.impl.DwGroupDeleteOperationImpl <em>Dw Group Delete Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.operation.impl.DwGroupDeleteOperationImpl
		 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwGroupDeleteOperation()
		 * @generated
		 */
		EClass DW_GROUP_DELETE_OPERATION = eINSTANCE.getDwGroupDeleteOperation();

		/**
		 * The meta object literal for the '<em><b>Old Group Valid Until</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_GROUP_DELETE_OPERATION__OLD_GROUP_VALID_UNTIL = eINSTANCE.getDwGroupDeleteOperation_OldGroupValidUntil();

		/**
		 * The meta object literal for the '<em><b>Consequent Feature Operations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_GROUP_DELETE_OPERATION__CONSEQUENT_FEATURE_OPERATIONS = eINSTANCE.getDwGroupDeleteOperation_ConsequentFeatureOperations();

		/**
		 * The meta object literal for the '<em><b>Detach Operation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_GROUP_DELETE_OPERATION__DETACH_OPERATION = eINSTANCE.getDwGroupDeleteOperation_DetachOperation();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.operation.impl.DwGroupDetachOperationImpl <em>Dw Group Detach Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.operation.impl.DwGroupDetachOperationImpl
		 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwGroupDetachOperation()
		 * @generated
		 */
		EClass DW_GROUP_DETACH_OPERATION = eINSTANCE.getDwGroupDetachOperation();

		/**
		 * The meta object literal for the '<em><b>Old Container</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_GROUP_DETACH_OPERATION__OLD_CONTAINER = eINSTANCE.getDwGroupDetachOperation_OldContainer();

		/**
		 * The meta object literal for the '<em><b>Old Parent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_GROUP_DETACH_OPERATION__OLD_PARENT = eINSTANCE.getDwGroupDetachOperation_OldParent();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.operation.impl.DwGroupMoveOperationImpl <em>Dw Group Move Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.operation.impl.DwGroupMoveOperationImpl
		 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwGroupMoveOperation()
		 * @generated
		 */
		EClass DW_GROUP_MOVE_OPERATION = eINSTANCE.getDwGroupMoveOperation();

		/**
		 * The meta object literal for the '<em><b>Group Detach Operation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_GROUP_MOVE_OPERATION__GROUP_DETACH_OPERATION = eINSTANCE.getDwGroupMoveOperation_GroupDetachOperation();

		/**
		 * The meta object literal for the '<em><b>Group Add Operation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_GROUP_MOVE_OPERATION__GROUP_ADD_OPERATION = eINSTANCE.getDwGroupMoveOperation_GroupAddOperation();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.operation.impl.DwGroupTypeChangeOperationImpl <em>Dw Group Type Change Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.operation.impl.DwGroupTypeChangeOperationImpl
		 * @see de.darwinspl.feature.operation.impl.DwOperationPackageImpl#getDwGroupTypeChangeOperation()
		 * @generated
		 */
		EClass DW_GROUP_TYPE_CHANGE_OPERATION = eINSTANCE.getDwGroupTypeChangeOperation();

		/**
		 * The meta object literal for the '<em><b>Consequent Operations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_GROUP_TYPE_CHANGE_OPERATION__CONSEQUENT_OPERATIONS = eINSTANCE.getDwGroupTypeChangeOperation_ConsequentOperations();

		/**
		 * The meta object literal for the '<em><b>New Group Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_GROUP_TYPE_CHANGE_OPERATION__NEW_GROUP_TYPE = eINSTANCE.getDwGroupTypeChangeOperation_NewGroupType();

		/**
		 * The meta object literal for the '<em><b>New Group Type Enum</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_GROUP_TYPE_CHANGE_OPERATION__NEW_GROUP_TYPE_ENUM = eINSTANCE.getDwGroupTypeChangeOperation_NewGroupTypeEnum();

		/**
		 * The meta object literal for the '<em><b>Old Group Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_GROUP_TYPE_CHANGE_OPERATION__OLD_GROUP_TYPE = eINSTANCE.getDwGroupTypeChangeOperation_OldGroupType();

	}

} //DwOperationPackage
