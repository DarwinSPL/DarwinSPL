/**
 */
package de.darwinspl.feature.operation;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Feature Create Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureCreateOperation#getFeatureAddOperation <em>Feature Add Operation</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureCreateOperation()
 * @model
 * @generated
 */
public interface DwFeatureCreateOperation extends DwFeatureOperation {
	/**
	 * Returns the value of the '<em><b>Feature Add Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Add Operation</em>' containment reference.
	 * @see #setFeatureAddOperation(DwFeatureAddOperation)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureCreateOperation_FeatureAddOperation()
	 * @model containment="true"
	 * @generated
	 */
	DwFeatureAddOperation getFeatureAddOperation();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureCreateOperation#getFeatureAddOperation <em>Feature Add Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature Add Operation</em>' containment reference.
	 * @see #getFeatureAddOperation()
	 * @generated
	 */
	void setFeatureAddOperation(DwFeatureAddOperation value);

} // DwFeatureCreateOperation
