/**
 */
package de.darwinspl.feature.operation;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Feature Attribute Create Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureAttributeCreateOperation()
 * @model
 * @generated
 */
public interface DwFeatureAttributeCreateOperation extends DwFeatureAttributeOperation {
} // DwFeatureAttributeCreateOperation
