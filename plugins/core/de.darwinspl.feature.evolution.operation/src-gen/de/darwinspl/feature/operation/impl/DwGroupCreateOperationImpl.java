/**
 */
package de.darwinspl.feature.operation.impl;

import de.darwinspl.feature.operation.DwGroupAddOperation;
import de.darwinspl.feature.operation.DwGroupCreateOperation;
import de.darwinspl.feature.operation.DwOperationPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Group Create Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwGroupCreateOperationImpl#getGroupAddOperation <em>Group Add Operation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwGroupCreateOperationImpl extends DwGroupOperationImpl implements DwGroupCreateOperation {
	/**
	 * The cached value of the '{@link #getGroupAddOperation() <em>Group Add Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroupAddOperation()
	 * @generated
	 * @ordered
	 */
	protected DwGroupAddOperation groupAddOperation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwGroupCreateOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwOperationPackage.Literals.DW_GROUP_CREATE_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroupAddOperation getGroupAddOperation() {
		return groupAddOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGroupAddOperation(DwGroupAddOperation newGroupAddOperation, NotificationChain msgs) {
		DwGroupAddOperation oldGroupAddOperation = groupAddOperation;
		groupAddOperation = newGroupAddOperation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_GROUP_CREATE_OPERATION__GROUP_ADD_OPERATION, oldGroupAddOperation, newGroupAddOperation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setGroupAddOperation(DwGroupAddOperation newGroupAddOperation) {
		if (newGroupAddOperation != groupAddOperation) {
			NotificationChain msgs = null;
			if (groupAddOperation != null)
				msgs = ((InternalEObject)groupAddOperation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DwOperationPackage.DW_GROUP_CREATE_OPERATION__GROUP_ADD_OPERATION, null, msgs);
			if (newGroupAddOperation != null)
				msgs = ((InternalEObject)newGroupAddOperation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DwOperationPackage.DW_GROUP_CREATE_OPERATION__GROUP_ADD_OPERATION, null, msgs);
			msgs = basicSetGroupAddOperation(newGroupAddOperation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_GROUP_CREATE_OPERATION__GROUP_ADD_OPERATION, newGroupAddOperation, newGroupAddOperation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwOperationPackage.DW_GROUP_CREATE_OPERATION__GROUP_ADD_OPERATION:
				return basicSetGroupAddOperation(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwOperationPackage.DW_GROUP_CREATE_OPERATION__GROUP_ADD_OPERATION:
				return getGroupAddOperation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwOperationPackage.DW_GROUP_CREATE_OPERATION__GROUP_ADD_OPERATION:
				setGroupAddOperation((DwGroupAddOperation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_GROUP_CREATE_OPERATION__GROUP_ADD_OPERATION:
				setGroupAddOperation((DwGroupAddOperation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_GROUP_CREATE_OPERATION__GROUP_ADD_OPERATION:
				return groupAddOperation != null;
		}
		return super.eIsSet(featureID);
	}

} //DwGroupCreateOperationImpl
