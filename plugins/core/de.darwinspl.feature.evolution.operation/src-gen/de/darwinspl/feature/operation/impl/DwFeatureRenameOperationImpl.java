/**
 */
package de.darwinspl.feature.operation.impl;

import de.darwinspl.feature.DwName;

import de.darwinspl.feature.operation.DwFeatureRenameOperation;
import de.darwinspl.feature.operation.DwOperationPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Feature Rename Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureRenameOperationImpl#getNewName <em>New Name</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureRenameOperationImpl#getNewNameString <em>New Name String</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureRenameOperationImpl#getOldName <em>Old Name</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwFeatureRenameOperationImpl#getOldNameString <em>Old Name String</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwFeatureRenameOperationImpl extends DwFeatureOperationImpl implements DwFeatureRenameOperation {
	/**
	 * The cached value of the '{@link #getNewName() <em>New Name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNewName()
	 * @generated
	 * @ordered
	 */
	protected DwName newName;

	/**
	 * The default value of the '{@link #getNewNameString() <em>New Name String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNewNameString()
	 * @generated
	 * @ordered
	 */
	protected static final String NEW_NAME_STRING_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNewNameString() <em>New Name String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNewNameString()
	 * @generated
	 * @ordered
	 */
	protected String newNameString = NEW_NAME_STRING_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOldName() <em>Old Name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOldName()
	 * @generated
	 * @ordered
	 */
	protected DwName oldName;

	/**
	 * The default value of the '{@link #getOldNameString() <em>Old Name String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOldNameString()
	 * @generated
	 * @ordered
	 */
	protected static final String OLD_NAME_STRING_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOldNameString() <em>Old Name String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOldNameString()
	 * @generated
	 * @ordered
	 */
	protected String oldNameString = OLD_NAME_STRING_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwFeatureRenameOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwOperationPackage.Literals.DW_FEATURE_RENAME_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwName getNewName() {
		if (newName != null && newName.eIsProxy()) {
			InternalEObject oldNewName = (InternalEObject)newName;
			newName = (DwName)eResolveProxy(oldNewName);
			if (newName != oldNewName) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwOperationPackage.DW_FEATURE_RENAME_OPERATION__NEW_NAME, oldNewName, newName));
			}
		}
		return newName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwName basicGetNewName() {
		return newName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNewName(DwName newNewName) {
		DwName oldNewName = newName;
		newName = newNewName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_RENAME_OPERATION__NEW_NAME, oldNewName, newName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getNewNameString() {
		return newNameString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNewNameString(String newNewNameString) {
		String oldNewNameString = newNameString;
		newNameString = newNewNameString;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_RENAME_OPERATION__NEW_NAME_STRING, oldNewNameString, newNameString));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwName getOldName() {
		if (oldName != null && oldName.eIsProxy()) {
			InternalEObject oldOldName = (InternalEObject)oldName;
			oldName = (DwName)eResolveProxy(oldOldName);
			if (oldName != oldOldName) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwOperationPackage.DW_FEATURE_RENAME_OPERATION__OLD_NAME, oldOldName, oldName));
			}
		}
		return oldName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwName basicGetOldName() {
		return oldName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOldName(DwName newOldName) {
		DwName oldOldName = oldName;
		oldName = newOldName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_RENAME_OPERATION__OLD_NAME, oldOldName, oldName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOldNameString() {
		return oldNameString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOldNameString(String newOldNameString) {
		String oldOldNameString = oldNameString;
		oldNameString = newOldNameString;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_FEATURE_RENAME_OPERATION__OLD_NAME_STRING, oldOldNameString, oldNameString));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_RENAME_OPERATION__NEW_NAME:
				if (resolve) return getNewName();
				return basicGetNewName();
			case DwOperationPackage.DW_FEATURE_RENAME_OPERATION__NEW_NAME_STRING:
				return getNewNameString();
			case DwOperationPackage.DW_FEATURE_RENAME_OPERATION__OLD_NAME:
				if (resolve) return getOldName();
				return basicGetOldName();
			case DwOperationPackage.DW_FEATURE_RENAME_OPERATION__OLD_NAME_STRING:
				return getOldNameString();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_RENAME_OPERATION__NEW_NAME:
				setNewName((DwName)newValue);
				return;
			case DwOperationPackage.DW_FEATURE_RENAME_OPERATION__NEW_NAME_STRING:
				setNewNameString((String)newValue);
				return;
			case DwOperationPackage.DW_FEATURE_RENAME_OPERATION__OLD_NAME:
				setOldName((DwName)newValue);
				return;
			case DwOperationPackage.DW_FEATURE_RENAME_OPERATION__OLD_NAME_STRING:
				setOldNameString((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_RENAME_OPERATION__NEW_NAME:
				setNewName((DwName)null);
				return;
			case DwOperationPackage.DW_FEATURE_RENAME_OPERATION__NEW_NAME_STRING:
				setNewNameString(NEW_NAME_STRING_EDEFAULT);
				return;
			case DwOperationPackage.DW_FEATURE_RENAME_OPERATION__OLD_NAME:
				setOldName((DwName)null);
				return;
			case DwOperationPackage.DW_FEATURE_RENAME_OPERATION__OLD_NAME_STRING:
				setOldNameString(OLD_NAME_STRING_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_FEATURE_RENAME_OPERATION__NEW_NAME:
				return newName != null;
			case DwOperationPackage.DW_FEATURE_RENAME_OPERATION__NEW_NAME_STRING:
				return NEW_NAME_STRING_EDEFAULT == null ? newNameString != null : !NEW_NAME_STRING_EDEFAULT.equals(newNameString);
			case DwOperationPackage.DW_FEATURE_RENAME_OPERATION__OLD_NAME:
				return oldName != null;
			case DwOperationPackage.DW_FEATURE_RENAME_OPERATION__OLD_NAME_STRING:
				return OLD_NAME_STRING_EDEFAULT == null ? oldNameString != null : !OLD_NAME_STRING_EDEFAULT.equals(oldNameString);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (newNameString: ");
		result.append(newNameString);
		result.append(", oldNameString: ");
		result.append(oldNameString);
		result.append(')');
		return result.toString();
	}

} //DwFeatureRenameOperationImpl
