/**
 */
package de.darwinspl.feature.operation;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Feature Version Rename Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureVersionRenameOperation#getOldNumber <em>Old Number</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureVersionRenameOperation#getNewNumber <em>New Number</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureVersionRenameOperation()
 * @model
 * @generated
 */
public interface DwFeatureVersionRenameOperation extends DwFeatureVersionOperation {
	/**
	 * Returns the value of the '<em><b>Old Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Old Number</em>' attribute.
	 * @see #setOldNumber(String)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureVersionRenameOperation_OldNumber()
	 * @model required="true"
	 * @generated
	 */
	String getOldNumber();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureVersionRenameOperation#getOldNumber <em>Old Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Old Number</em>' attribute.
	 * @see #getOldNumber()
	 * @generated
	 */
	void setOldNumber(String value);

	/**
	 * Returns the value of the '<em><b>New Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>New Number</em>' attribute.
	 * @see #setNewNumber(String)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureVersionRenameOperation_NewNumber()
	 * @model required="true"
	 * @generated
	 */
	String getNewNumber();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureVersionRenameOperation#getNewNumber <em>New Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>New Number</em>' attribute.
	 * @see #getNewNumber()
	 * @generated
	 */
	void setNewNumber(String value);

} // DwFeatureVersionRenameOperation
