/**
 */
package de.darwinspl.feature.operation;

import de.darwinspl.feature.contributor.DwContributor;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Evolution Operation Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.DwEvolutionOperationModel#getEvolutionOperations <em>Evolution Operations</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwEvolutionOperationModel#getContributors <em>Contributors</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwEvolutionOperationModel()
 * @model
 * @generated
 */
public interface DwEvolutionOperationModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Evolution Operations</b></em>' containment reference list.
	 * The list contents are of type {@link de.darwinspl.feature.operation.DwEvolutionOperation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Evolution Operations</em>' containment reference list.
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwEvolutionOperationModel_EvolutionOperations()
	 * @model containment="true"
	 * @generated
	 */
	EList<DwEvolutionOperation> getEvolutionOperations();

	/**
	 * Returns the value of the '<em><b>Contributors</b></em>' containment reference list.
	 * The list contents are of type {@link de.darwinspl.feature.contributor.DwContributor}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contributors</em>' containment reference list.
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwEvolutionOperationModel_Contributors()
	 * @model containment="true"
	 * @generated
	 */
	EList<DwContributor> getContributors();

} // DwEvolutionOperationModel
