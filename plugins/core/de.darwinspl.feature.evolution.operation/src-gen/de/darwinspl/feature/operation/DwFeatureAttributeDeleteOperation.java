/**
 */
package de.darwinspl.feature.operation;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Feature Attribute Delete Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureAttributeDeleteOperation()
 * @model
 * @generated
 */
public interface DwFeatureAttributeDeleteOperation extends DwFeatureAttributeOperation {
} // DwFeatureAttributeDeleteOperation
