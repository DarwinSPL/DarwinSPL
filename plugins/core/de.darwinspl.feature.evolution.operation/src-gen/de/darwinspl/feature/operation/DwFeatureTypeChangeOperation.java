/**
 */
package de.darwinspl.feature.operation;

import de.darwinspl.feature.DwFeatureType;
import de.darwinspl.feature.DwFeatureTypeEnum;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Feature Type Change Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureTypeChangeOperation#getNewFeatureType <em>New Feature Type</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureTypeChangeOperation#getNewFeatureTypeEnum <em>New Feature Type Enum</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwFeatureTypeChangeOperation#getOldFeatureType <em>Old Feature Type</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureTypeChangeOperation()
 * @model
 * @generated
 */
public interface DwFeatureTypeChangeOperation extends DwFeatureOperation {
	/**
	 * Returns the value of the '<em><b>New Feature Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>New Feature Type</em>' reference.
	 * @see #setNewFeatureType(DwFeatureType)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureTypeChangeOperation_NewFeatureType()
	 * @model
	 * @generated
	 */
	DwFeatureType getNewFeatureType();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureTypeChangeOperation#getNewFeatureType <em>New Feature Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>New Feature Type</em>' reference.
	 * @see #getNewFeatureType()
	 * @generated
	 */
	void setNewFeatureType(DwFeatureType value);

	/**
	 * Returns the value of the '<em><b>New Feature Type Enum</b></em>' attribute.
	 * The literals are from the enumeration {@link de.darwinspl.feature.DwFeatureTypeEnum}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>New Feature Type Enum</em>' attribute.
	 * @see de.darwinspl.feature.DwFeatureTypeEnum
	 * @see #setNewFeatureTypeEnum(DwFeatureTypeEnum)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureTypeChangeOperation_NewFeatureTypeEnum()
	 * @model required="true"
	 * @generated
	 */
	DwFeatureTypeEnum getNewFeatureTypeEnum();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureTypeChangeOperation#getNewFeatureTypeEnum <em>New Feature Type Enum</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>New Feature Type Enum</em>' attribute.
	 * @see de.darwinspl.feature.DwFeatureTypeEnum
	 * @see #getNewFeatureTypeEnum()
	 * @generated
	 */
	void setNewFeatureTypeEnum(DwFeatureTypeEnum value);

	/**
	 * Returns the value of the '<em><b>Old Feature Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Old Feature Type</em>' reference.
	 * @see #setOldFeatureType(DwFeatureType)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwFeatureTypeChangeOperation_OldFeatureType()
	 * @model
	 * @generated
	 */
	DwFeatureType getOldFeatureType();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwFeatureTypeChangeOperation#getOldFeatureType <em>Old Feature Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Old Feature Type</em>' reference.
	 * @see #getOldFeatureType()
	 * @generated
	 */
	void setOldFeatureType(DwFeatureType value);

} // DwFeatureTypeChangeOperation
