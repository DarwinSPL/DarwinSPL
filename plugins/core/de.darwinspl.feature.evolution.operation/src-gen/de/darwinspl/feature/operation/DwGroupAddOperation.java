/**
 */
package de.darwinspl.feature.operation;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwParentFeatureChildGroupContainer;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Group Add Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.DwGroupAddOperation#getParentFeature <em>Parent Feature</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwGroupAddOperation#getNewContainer <em>New Container</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwGroupAddOperation()
 * @model
 * @generated
 */
public interface DwGroupAddOperation extends DwGroupOperation {
	/**
	 * Returns the value of the '<em><b>Parent Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Feature</em>' reference.
	 * @see #setParentFeature(DwFeature)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwGroupAddOperation_ParentFeature()
	 * @model required="true"
	 * @generated
	 */
	DwFeature getParentFeature();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwGroupAddOperation#getParentFeature <em>Parent Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Feature</em>' reference.
	 * @see #getParentFeature()
	 * @generated
	 */
	void setParentFeature(DwFeature value);

	/**
	 * Returns the value of the '<em><b>New Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>New Container</em>' reference.
	 * @see #setNewContainer(DwParentFeatureChildGroupContainer)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwGroupAddOperation_NewContainer()
	 * @model
	 * @generated
	 */
	DwParentFeatureChildGroupContainer getNewContainer();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwGroupAddOperation#getNewContainer <em>New Container</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>New Container</em>' reference.
	 * @see #getNewContainer()
	 * @generated
	 */
	void setNewContainer(DwParentFeatureChildGroupContainer value);

} // DwGroupAddOperation
