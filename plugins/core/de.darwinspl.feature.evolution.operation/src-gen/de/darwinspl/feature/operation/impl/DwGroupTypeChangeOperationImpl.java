/**
 */
package de.darwinspl.feature.operation.impl;

import de.darwinspl.feature.DwGroupType;
import de.darwinspl.feature.DwGroupTypeEnum;

import de.darwinspl.feature.operation.DwFeatureTypeChangeOperation;
import de.darwinspl.feature.operation.DwGroupTypeChangeOperation;
import de.darwinspl.feature.operation.DwOperationPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Group Type Change Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwGroupTypeChangeOperationImpl#getConsequentOperations <em>Consequent Operations</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwGroupTypeChangeOperationImpl#getNewGroupType <em>New Group Type</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwGroupTypeChangeOperationImpl#getNewGroupTypeEnum <em>New Group Type Enum</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.impl.DwGroupTypeChangeOperationImpl#getOldGroupType <em>Old Group Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwGroupTypeChangeOperationImpl extends DwGroupOperationImpl implements DwGroupTypeChangeOperation {
	/**
	 * The cached value of the '{@link #getConsequentOperations() <em>Consequent Operations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConsequentOperations()
	 * @generated
	 * @ordered
	 */
	protected EList<DwFeatureTypeChangeOperation> consequentOperations;

	/**
	 * The cached value of the '{@link #getNewGroupType() <em>New Group Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNewGroupType()
	 * @generated
	 * @ordered
	 */
	protected DwGroupType newGroupType;

	/**
	 * The default value of the '{@link #getNewGroupTypeEnum() <em>New Group Type Enum</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNewGroupTypeEnum()
	 * @generated
	 * @ordered
	 */
	protected static final DwGroupTypeEnum NEW_GROUP_TYPE_ENUM_EDEFAULT = DwGroupTypeEnum.AND;

	/**
	 * The cached value of the '{@link #getNewGroupTypeEnum() <em>New Group Type Enum</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNewGroupTypeEnum()
	 * @generated
	 * @ordered
	 */
	protected DwGroupTypeEnum newGroupTypeEnum = NEW_GROUP_TYPE_ENUM_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOldGroupType() <em>Old Group Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOldGroupType()
	 * @generated
	 * @ordered
	 */
	protected DwGroupType oldGroupType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwGroupTypeChangeOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwOperationPackage.Literals.DW_GROUP_TYPE_CHANGE_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwFeatureTypeChangeOperation> getConsequentOperations() {
		if (consequentOperations == null) {
			consequentOperations = new EObjectContainmentEList<DwFeatureTypeChangeOperation>(DwFeatureTypeChangeOperation.class, this, DwOperationPackage.DW_GROUP_TYPE_CHANGE_OPERATION__CONSEQUENT_OPERATIONS);
		}
		return consequentOperations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroupType getNewGroupType() {
		if (newGroupType != null && newGroupType.eIsProxy()) {
			InternalEObject oldNewGroupType = (InternalEObject)newGroupType;
			newGroupType = (DwGroupType)eResolveProxy(oldNewGroupType);
			if (newGroupType != oldNewGroupType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwOperationPackage.DW_GROUP_TYPE_CHANGE_OPERATION__NEW_GROUP_TYPE, oldNewGroupType, newGroupType));
			}
		}
		return newGroupType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwGroupType basicGetNewGroupType() {
		return newGroupType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNewGroupType(DwGroupType newNewGroupType) {
		DwGroupType oldNewGroupType = newGroupType;
		newGroupType = newNewGroupType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_GROUP_TYPE_CHANGE_OPERATION__NEW_GROUP_TYPE, oldNewGroupType, newGroupType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroupTypeEnum getNewGroupTypeEnum() {
		return newGroupTypeEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNewGroupTypeEnum(DwGroupTypeEnum newNewGroupTypeEnum) {
		DwGroupTypeEnum oldNewGroupTypeEnum = newGroupTypeEnum;
		newGroupTypeEnum = newNewGroupTypeEnum == null ? NEW_GROUP_TYPE_ENUM_EDEFAULT : newNewGroupTypeEnum;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_GROUP_TYPE_CHANGE_OPERATION__NEW_GROUP_TYPE_ENUM, oldNewGroupTypeEnum, newGroupTypeEnum));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroupType getOldGroupType() {
		if (oldGroupType != null && oldGroupType.eIsProxy()) {
			InternalEObject oldOldGroupType = (InternalEObject)oldGroupType;
			oldGroupType = (DwGroupType)eResolveProxy(oldOldGroupType);
			if (oldGroupType != oldOldGroupType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwOperationPackage.DW_GROUP_TYPE_CHANGE_OPERATION__OLD_GROUP_TYPE, oldOldGroupType, oldGroupType));
			}
		}
		return oldGroupType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwGroupType basicGetOldGroupType() {
		return oldGroupType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOldGroupType(DwGroupType newOldGroupType) {
		DwGroupType oldOldGroupType = oldGroupType;
		oldGroupType = newOldGroupType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwOperationPackage.DW_GROUP_TYPE_CHANGE_OPERATION__OLD_GROUP_TYPE, oldOldGroupType, oldGroupType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwOperationPackage.DW_GROUP_TYPE_CHANGE_OPERATION__CONSEQUENT_OPERATIONS:
				return ((InternalEList<?>)getConsequentOperations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwOperationPackage.DW_GROUP_TYPE_CHANGE_OPERATION__CONSEQUENT_OPERATIONS:
				return getConsequentOperations();
			case DwOperationPackage.DW_GROUP_TYPE_CHANGE_OPERATION__NEW_GROUP_TYPE:
				if (resolve) return getNewGroupType();
				return basicGetNewGroupType();
			case DwOperationPackage.DW_GROUP_TYPE_CHANGE_OPERATION__NEW_GROUP_TYPE_ENUM:
				return getNewGroupTypeEnum();
			case DwOperationPackage.DW_GROUP_TYPE_CHANGE_OPERATION__OLD_GROUP_TYPE:
				if (resolve) return getOldGroupType();
				return basicGetOldGroupType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwOperationPackage.DW_GROUP_TYPE_CHANGE_OPERATION__CONSEQUENT_OPERATIONS:
				getConsequentOperations().clear();
				getConsequentOperations().addAll((Collection<? extends DwFeatureTypeChangeOperation>)newValue);
				return;
			case DwOperationPackage.DW_GROUP_TYPE_CHANGE_OPERATION__NEW_GROUP_TYPE:
				setNewGroupType((DwGroupType)newValue);
				return;
			case DwOperationPackage.DW_GROUP_TYPE_CHANGE_OPERATION__NEW_GROUP_TYPE_ENUM:
				setNewGroupTypeEnum((DwGroupTypeEnum)newValue);
				return;
			case DwOperationPackage.DW_GROUP_TYPE_CHANGE_OPERATION__OLD_GROUP_TYPE:
				setOldGroupType((DwGroupType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_GROUP_TYPE_CHANGE_OPERATION__CONSEQUENT_OPERATIONS:
				getConsequentOperations().clear();
				return;
			case DwOperationPackage.DW_GROUP_TYPE_CHANGE_OPERATION__NEW_GROUP_TYPE:
				setNewGroupType((DwGroupType)null);
				return;
			case DwOperationPackage.DW_GROUP_TYPE_CHANGE_OPERATION__NEW_GROUP_TYPE_ENUM:
				setNewGroupTypeEnum(NEW_GROUP_TYPE_ENUM_EDEFAULT);
				return;
			case DwOperationPackage.DW_GROUP_TYPE_CHANGE_OPERATION__OLD_GROUP_TYPE:
				setOldGroupType((DwGroupType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwOperationPackage.DW_GROUP_TYPE_CHANGE_OPERATION__CONSEQUENT_OPERATIONS:
				return consequentOperations != null && !consequentOperations.isEmpty();
			case DwOperationPackage.DW_GROUP_TYPE_CHANGE_OPERATION__NEW_GROUP_TYPE:
				return newGroupType != null;
			case DwOperationPackage.DW_GROUP_TYPE_CHANGE_OPERATION__NEW_GROUP_TYPE_ENUM:
				return newGroupTypeEnum != NEW_GROUP_TYPE_ENUM_EDEFAULT;
			case DwOperationPackage.DW_GROUP_TYPE_CHANGE_OPERATION__OLD_GROUP_TYPE:
				return oldGroupType != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (newGroupTypeEnum: ");
		result.append(newGroupTypeEnum);
		result.append(')');
		return result.toString();
	}

} //DwGroupTypeChangeOperationImpl
