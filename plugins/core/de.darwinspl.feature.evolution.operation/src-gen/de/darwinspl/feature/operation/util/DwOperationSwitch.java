/**
 */
package de.darwinspl.feature.operation.util;

import de.darwinspl.feature.operation.*;

import de.darwinspl.temporal.DwElementWithId;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see de.darwinspl.feature.operation.DwOperationPackage
 * @generated
 */
public class DwOperationSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DwOperationPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwOperationSwitch() {
		if (modelPackage == null) {
			modelPackage = DwOperationPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case DwOperationPackage.DW_EVOLUTION_OPERATION_MODEL: {
				DwEvolutionOperationModel dwEvolutionOperationModel = (DwEvolutionOperationModel)theEObject;
				T result = caseDwEvolutionOperationModel(dwEvolutionOperationModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwOperationPackage.DW_EVOLUTION_OPERATION: {
				DwEvolutionOperation dwEvolutionOperation = (DwEvolutionOperation)theEObject;
				T result = caseDwEvolutionOperation(dwEvolutionOperation);
				if (result == null) result = caseDwElementWithId(dwEvolutionOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwOperationPackage.DW_FEATURE_OPERATION: {
				DwFeatureOperation dwFeatureOperation = (DwFeatureOperation)theEObject;
				T result = caseDwFeatureOperation(dwFeatureOperation);
				if (result == null) result = caseDwEvolutionOperation(dwFeatureOperation);
				if (result == null) result = caseDwElementWithId(dwFeatureOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwOperationPackage.DW_FEATURE_CREATE_OPERATION: {
				DwFeatureCreateOperation dwFeatureCreateOperation = (DwFeatureCreateOperation)theEObject;
				T result = caseDwFeatureCreateOperation(dwFeatureCreateOperation);
				if (result == null) result = caseDwFeatureOperation(dwFeatureCreateOperation);
				if (result == null) result = caseDwEvolutionOperation(dwFeatureCreateOperation);
				if (result == null) result = caseDwElementWithId(dwFeatureCreateOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwOperationPackage.DW_FEATURE_ADD_OPERATION: {
				DwFeatureAddOperation dwFeatureAddOperation = (DwFeatureAddOperation)theEObject;
				T result = caseDwFeatureAddOperation(dwFeatureAddOperation);
				if (result == null) result = caseDwFeatureOperation(dwFeatureAddOperation);
				if (result == null) result = caseDwEvolutionOperation(dwFeatureAddOperation);
				if (result == null) result = caseDwElementWithId(dwFeatureAddOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwOperationPackage.DW_FEATURE_DELETE_OPERATION: {
				DwFeatureDeleteOperation dwFeatureDeleteOperation = (DwFeatureDeleteOperation)theEObject;
				T result = caseDwFeatureDeleteOperation(dwFeatureDeleteOperation);
				if (result == null) result = caseDwFeatureOperation(dwFeatureDeleteOperation);
				if (result == null) result = caseDwEvolutionOperation(dwFeatureDeleteOperation);
				if (result == null) result = caseDwElementWithId(dwFeatureDeleteOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwOperationPackage.DW_FEATURE_DETACH_OPERATION: {
				DwFeatureDetachOperation dwFeatureDetachOperation = (DwFeatureDetachOperation)theEObject;
				T result = caseDwFeatureDetachOperation(dwFeatureDetachOperation);
				if (result == null) result = caseDwFeatureOperation(dwFeatureDetachOperation);
				if (result == null) result = caseDwEvolutionOperation(dwFeatureDetachOperation);
				if (result == null) result = caseDwElementWithId(dwFeatureDetachOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwOperationPackage.DW_FEATURE_MOVE_OPERATION: {
				DwFeatureMoveOperation dwFeatureMoveOperation = (DwFeatureMoveOperation)theEObject;
				T result = caseDwFeatureMoveOperation(dwFeatureMoveOperation);
				if (result == null) result = caseDwFeatureOperation(dwFeatureMoveOperation);
				if (result == null) result = caseDwEvolutionOperation(dwFeatureMoveOperation);
				if (result == null) result = caseDwElementWithId(dwFeatureMoveOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwOperationPackage.DW_FEATURE_RENAME_OPERATION: {
				DwFeatureRenameOperation dwFeatureRenameOperation = (DwFeatureRenameOperation)theEObject;
				T result = caseDwFeatureRenameOperation(dwFeatureRenameOperation);
				if (result == null) result = caseDwFeatureOperation(dwFeatureRenameOperation);
				if (result == null) result = caseDwEvolutionOperation(dwFeatureRenameOperation);
				if (result == null) result = caseDwElementWithId(dwFeatureRenameOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwOperationPackage.DW_FEATURE_TYPE_CHANGE_OPERATION: {
				DwFeatureTypeChangeOperation dwFeatureTypeChangeOperation = (DwFeatureTypeChangeOperation)theEObject;
				T result = caseDwFeatureTypeChangeOperation(dwFeatureTypeChangeOperation);
				if (result == null) result = caseDwFeatureOperation(dwFeatureTypeChangeOperation);
				if (result == null) result = caseDwEvolutionOperation(dwFeatureTypeChangeOperation);
				if (result == null) result = caseDwElementWithId(dwFeatureTypeChangeOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwOperationPackage.DW_FEATURE_ATTRIBUTE_OPERATION: {
				DwFeatureAttributeOperation dwFeatureAttributeOperation = (DwFeatureAttributeOperation)theEObject;
				T result = caseDwFeatureAttributeOperation(dwFeatureAttributeOperation);
				if (result == null) result = caseDwFeatureOperation(dwFeatureAttributeOperation);
				if (result == null) result = caseDwEvolutionOperation(dwFeatureAttributeOperation);
				if (result == null) result = caseDwElementWithId(dwFeatureAttributeOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwOperationPackage.DW_FEATURE_ATTRIBUTE_CREATE_OPERATION: {
				DwFeatureAttributeCreateOperation dwFeatureAttributeCreateOperation = (DwFeatureAttributeCreateOperation)theEObject;
				T result = caseDwFeatureAttributeCreateOperation(dwFeatureAttributeCreateOperation);
				if (result == null) result = caseDwFeatureAttributeOperation(dwFeatureAttributeCreateOperation);
				if (result == null) result = caseDwFeatureOperation(dwFeatureAttributeCreateOperation);
				if (result == null) result = caseDwEvolutionOperation(dwFeatureAttributeCreateOperation);
				if (result == null) result = caseDwElementWithId(dwFeatureAttributeCreateOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwOperationPackage.DW_FEATURE_ATTRIBUTE_DELETE_OPERATION: {
				DwFeatureAttributeDeleteOperation dwFeatureAttributeDeleteOperation = (DwFeatureAttributeDeleteOperation)theEObject;
				T result = caseDwFeatureAttributeDeleteOperation(dwFeatureAttributeDeleteOperation);
				if (result == null) result = caseDwFeatureAttributeOperation(dwFeatureAttributeDeleteOperation);
				if (result == null) result = caseDwFeatureOperation(dwFeatureAttributeDeleteOperation);
				if (result == null) result = caseDwEvolutionOperation(dwFeatureAttributeDeleteOperation);
				if (result == null) result = caseDwElementWithId(dwFeatureAttributeDeleteOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwOperationPackage.DW_FEATURE_ATTRIBUTE_RENAME_OPERATION: {
				DwFeatureAttributeRenameOperation dwFeatureAttributeRenameOperation = (DwFeatureAttributeRenameOperation)theEObject;
				T result = caseDwFeatureAttributeRenameOperation(dwFeatureAttributeRenameOperation);
				if (result == null) result = caseDwFeatureAttributeOperation(dwFeatureAttributeRenameOperation);
				if (result == null) result = caseDwFeatureOperation(dwFeatureAttributeRenameOperation);
				if (result == null) result = caseDwEvolutionOperation(dwFeatureAttributeRenameOperation);
				if (result == null) result = caseDwElementWithId(dwFeatureAttributeRenameOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwOperationPackage.DW_FEATURE_VERSION_OPERATION: {
				DwFeatureVersionOperation dwFeatureVersionOperation = (DwFeatureVersionOperation)theEObject;
				T result = caseDwFeatureVersionOperation(dwFeatureVersionOperation);
				if (result == null) result = caseDwFeatureOperation(dwFeatureVersionOperation);
				if (result == null) result = caseDwEvolutionOperation(dwFeatureVersionOperation);
				if (result == null) result = caseDwElementWithId(dwFeatureVersionOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwOperationPackage.DW_FEATURE_VERSION_CREATE_OPERATION: {
				DwFeatureVersionCreateOperation dwFeatureVersionCreateOperation = (DwFeatureVersionCreateOperation)theEObject;
				T result = caseDwFeatureVersionCreateOperation(dwFeatureVersionCreateOperation);
				if (result == null) result = caseDwFeatureVersionOperation(dwFeatureVersionCreateOperation);
				if (result == null) result = caseDwFeatureOperation(dwFeatureVersionCreateOperation);
				if (result == null) result = caseDwEvolutionOperation(dwFeatureVersionCreateOperation);
				if (result == null) result = caseDwElementWithId(dwFeatureVersionCreateOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwOperationPackage.DW_FEATURE_VERSION_DELETE_OPERATION: {
				DwFeatureVersionDeleteOperation dwFeatureVersionDeleteOperation = (DwFeatureVersionDeleteOperation)theEObject;
				T result = caseDwFeatureVersionDeleteOperation(dwFeatureVersionDeleteOperation);
				if (result == null) result = caseDwFeatureVersionOperation(dwFeatureVersionDeleteOperation);
				if (result == null) result = caseDwFeatureOperation(dwFeatureVersionDeleteOperation);
				if (result == null) result = caseDwEvolutionOperation(dwFeatureVersionDeleteOperation);
				if (result == null) result = caseDwElementWithId(dwFeatureVersionDeleteOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwOperationPackage.DW_FEATURE_VERSION_RENAME_OPERATION: {
				DwFeatureVersionRenameOperation dwFeatureVersionRenameOperation = (DwFeatureVersionRenameOperation)theEObject;
				T result = caseDwFeatureVersionRenameOperation(dwFeatureVersionRenameOperation);
				if (result == null) result = caseDwFeatureVersionOperation(dwFeatureVersionRenameOperation);
				if (result == null) result = caseDwFeatureOperation(dwFeatureVersionRenameOperation);
				if (result == null) result = caseDwEvolutionOperation(dwFeatureVersionRenameOperation);
				if (result == null) result = caseDwElementWithId(dwFeatureVersionRenameOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwOperationPackage.DW_GROUP_OPERATION: {
				DwGroupOperation dwGroupOperation = (DwGroupOperation)theEObject;
				T result = caseDwGroupOperation(dwGroupOperation);
				if (result == null) result = caseDwEvolutionOperation(dwGroupOperation);
				if (result == null) result = caseDwElementWithId(dwGroupOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwOperationPackage.DW_GROUP_CREATE_OPERATION: {
				DwGroupCreateOperation dwGroupCreateOperation = (DwGroupCreateOperation)theEObject;
				T result = caseDwGroupCreateOperation(dwGroupCreateOperation);
				if (result == null) result = caseDwGroupOperation(dwGroupCreateOperation);
				if (result == null) result = caseDwEvolutionOperation(dwGroupCreateOperation);
				if (result == null) result = caseDwElementWithId(dwGroupCreateOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwOperationPackage.DW_GROUP_ADD_OPERATION: {
				DwGroupAddOperation dwGroupAddOperation = (DwGroupAddOperation)theEObject;
				T result = caseDwGroupAddOperation(dwGroupAddOperation);
				if (result == null) result = caseDwGroupOperation(dwGroupAddOperation);
				if (result == null) result = caseDwEvolutionOperation(dwGroupAddOperation);
				if (result == null) result = caseDwElementWithId(dwGroupAddOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwOperationPackage.DW_GROUP_DELETE_OPERATION: {
				DwGroupDeleteOperation dwGroupDeleteOperation = (DwGroupDeleteOperation)theEObject;
				T result = caseDwGroupDeleteOperation(dwGroupDeleteOperation);
				if (result == null) result = caseDwGroupOperation(dwGroupDeleteOperation);
				if (result == null) result = caseDwEvolutionOperation(dwGroupDeleteOperation);
				if (result == null) result = caseDwElementWithId(dwGroupDeleteOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwOperationPackage.DW_GROUP_DETACH_OPERATION: {
				DwGroupDetachOperation dwGroupDetachOperation = (DwGroupDetachOperation)theEObject;
				T result = caseDwGroupDetachOperation(dwGroupDetachOperation);
				if (result == null) result = caseDwGroupOperation(dwGroupDetachOperation);
				if (result == null) result = caseDwEvolutionOperation(dwGroupDetachOperation);
				if (result == null) result = caseDwElementWithId(dwGroupDetachOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwOperationPackage.DW_GROUP_MOVE_OPERATION: {
				DwGroupMoveOperation dwGroupMoveOperation = (DwGroupMoveOperation)theEObject;
				T result = caseDwGroupMoveOperation(dwGroupMoveOperation);
				if (result == null) result = caseDwGroupOperation(dwGroupMoveOperation);
				if (result == null) result = caseDwEvolutionOperation(dwGroupMoveOperation);
				if (result == null) result = caseDwElementWithId(dwGroupMoveOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwOperationPackage.DW_GROUP_TYPE_CHANGE_OPERATION: {
				DwGroupTypeChangeOperation dwGroupTypeChangeOperation = (DwGroupTypeChangeOperation)theEObject;
				T result = caseDwGroupTypeChangeOperation(dwGroupTypeChangeOperation);
				if (result == null) result = caseDwGroupOperation(dwGroupTypeChangeOperation);
				if (result == null) result = caseDwEvolutionOperation(dwGroupTypeChangeOperation);
				if (result == null) result = caseDwElementWithId(dwGroupTypeChangeOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Evolution Operation Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Evolution Operation Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwEvolutionOperationModel(DwEvolutionOperationModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Evolution Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Evolution Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwEvolutionOperation(DwEvolutionOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Feature Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Feature Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureOperation(DwFeatureOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Feature Create Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Feature Create Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureCreateOperation(DwFeatureCreateOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Feature Add Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Feature Add Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureAddOperation(DwFeatureAddOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Feature Delete Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Feature Delete Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureDeleteOperation(DwFeatureDeleteOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Feature Detach Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Feature Detach Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureDetachOperation(DwFeatureDetachOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Feature Move Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Feature Move Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureMoveOperation(DwFeatureMoveOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Feature Rename Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Feature Rename Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureRenameOperation(DwFeatureRenameOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Feature Type Change Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Feature Type Change Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureTypeChangeOperation(DwFeatureTypeChangeOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Feature Attribute Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Feature Attribute Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureAttributeOperation(DwFeatureAttributeOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Feature Attribute Create Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Feature Attribute Create Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureAttributeCreateOperation(DwFeatureAttributeCreateOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Feature Attribute Delete Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Feature Attribute Delete Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureAttributeDeleteOperation(DwFeatureAttributeDeleteOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Feature Attribute Rename Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Feature Attribute Rename Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureAttributeRenameOperation(DwFeatureAttributeRenameOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Feature Version Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Feature Version Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureVersionOperation(DwFeatureVersionOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Feature Version Create Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Feature Version Create Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureVersionCreateOperation(DwFeatureVersionCreateOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Feature Version Delete Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Feature Version Delete Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureVersionDeleteOperation(DwFeatureVersionDeleteOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Feature Version Rename Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Feature Version Rename Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureVersionRenameOperation(DwFeatureVersionRenameOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Group Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Group Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwGroupOperation(DwGroupOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Group Create Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Group Create Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwGroupCreateOperation(DwGroupCreateOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Group Add Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Group Add Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwGroupAddOperation(DwGroupAddOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Group Delete Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Group Delete Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwGroupDeleteOperation(DwGroupDeleteOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Group Detach Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Group Detach Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwGroupDetachOperation(DwGroupDetachOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Group Move Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Group Move Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwGroupMoveOperation(DwGroupMoveOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Group Type Change Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Group Type Change Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwGroupTypeChangeOperation(DwGroupTypeChangeOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Element With Id</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Element With Id</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwElementWithId(DwElementWithId object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //DwOperationSwitch
