/**
 */
package de.darwinspl.feature.operation;

import de.darwinspl.feature.DwTemporalFeatureModel;

import de.darwinspl.feature.contributor.DwContributor;

import de.darwinspl.temporal.DwElementWithId;
import java.util.Date;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Evolution Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.operation.DwEvolutionOperation#getOperationDate <em>Operation Date</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwEvolutionOperation#getCommandStackIndex <em>Command Stack Index</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwEvolutionOperation#getFeatureModel <em>Feature Model</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwEvolutionOperation#getContributor <em>Contributor</em>}</li>
 *   <li>{@link de.darwinspl.feature.operation.DwEvolutionOperation#isIntermediateOperation <em>Intermediate Operation</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwEvolutionOperation()
 * @model abstract="true"
 * @generated
 */
public interface DwEvolutionOperation extends DwElementWithId {
	/**
	 * Returns the value of the '<em><b>Operation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation Date</em>' attribute.
	 * @see #setOperationDate(Date)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwEvolutionOperation_OperationDate()
	 * @model required="true"
	 * @generated
	 */
	Date getOperationDate();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwEvolutionOperation#getOperationDate <em>Operation Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operation Date</em>' attribute.
	 * @see #getOperationDate()
	 * @generated
	 */
	void setOperationDate(Date value);

	/**
	 * Returns the value of the '<em><b>Command Stack Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Command Stack Index</em>' attribute.
	 * @see #setCommandStackIndex(int)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwEvolutionOperation_CommandStackIndex()
	 * @model required="true"
	 * @generated
	 */
	int getCommandStackIndex();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwEvolutionOperation#getCommandStackIndex <em>Command Stack Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Command Stack Index</em>' attribute.
	 * @see #getCommandStackIndex()
	 * @generated
	 */
	void setCommandStackIndex(int value);

	/**
	 * Returns the value of the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Model</em>' reference.
	 * @see #setFeatureModel(DwTemporalFeatureModel)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwEvolutionOperation_FeatureModel()
	 * @model required="true"
	 * @generated
	 */
	DwTemporalFeatureModel getFeatureModel();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwEvolutionOperation#getFeatureModel <em>Feature Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature Model</em>' reference.
	 * @see #getFeatureModel()
	 * @generated
	 */
	void setFeatureModel(DwTemporalFeatureModel value);

	/**
	 * Returns the value of the '<em><b>Contributor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contributor</em>' reference.
	 * @see #setContributor(DwContributor)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwEvolutionOperation_Contributor()
	 * @model
	 * @generated
	 */
	DwContributor getContributor();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwEvolutionOperation#getContributor <em>Contributor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Contributor</em>' reference.
	 * @see #getContributor()
	 * @generated
	 */
	void setContributor(DwContributor value);

	/**
	 * Returns the value of the '<em><b>Intermediate Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Intermediate Operation</em>' attribute.
	 * @see #setIntermediateOperation(boolean)
	 * @see de.darwinspl.feature.operation.DwOperationPackage#getDwEvolutionOperation_IntermediateOperation()
	 * @model
	 * @generated
	 */
	boolean isIntermediateOperation();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.operation.DwEvolutionOperation#isIntermediateOperation <em>Intermediate Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Intermediate Operation</em>' attribute.
	 * @see #isIntermediateOperation()
	 * @generated
	 */
	void setIntermediateOperation(boolean value);

} // DwEvolutionOperation
