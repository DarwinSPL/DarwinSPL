package de.darwinspl.properties;

import java.util.Date;

import org.eclipse.jface.dialogs.MessageDialog;

import de.darwinspl.common.eclipse.ui.DwDialogUtil;
import de.darwinspl.feature.DwName;
import de.darwinspl.temporal.DwTemporalInterval;
import de.darwinspl.temporal.util.DwDateResolverUtil;
import de.darwinspl.temporal.util.DwEvolutionUtil;

public class DwPropertyValueWriter {
	
//	private static final DwSimpleEvolutionOperationFactory operationFactory = new DwSimpleEvolutionOperationFactory();
//	private static Stack<DwEvolutionOperation> operationsStack = new Stack<>();
	


	private static void displayDialog() {
		DwDialogUtil.openDialogOnce("de.darwinspl.properties.DwPropertyValueWriter:NotImplemented",
				"Not yet supported!",
				"This functionality is not yet supported. We are working on it!",
				MessageDialog.ERROR, new String[] { "Okay" }
		);
	}
	
	
	
	public static void overrideDwName(DwName name, String newValue, Date date, boolean lastDateSelected) {
//		if(!operationsStack.empty()) {
//			DwEvolutionOperation lastOperation = operationsStack.peek();
//			if(lastOperation instanceof DwFeatureRenameOperation) {
//				if(((DwFeatureRenameOperation) lastOperation).getOldName().equals(name)) {
//					if(((DwFeatureRenameOperation) lastOperation).getOldName().getName().equals(newValue)) {
//						DwEvolutionOperationInterpreter.undo(lastOperation);
//						operationsStack.pop();
//						return;
//					}
//				}
//			}
//		}
//		
//		DwFeature containingFeature = (DwFeature) name.eContainer();
//		DwEvolutionOperation evoOp = operationFactory.createDwFeatureRenameOperation(containingFeature, newValue, date, lastDateSelected);
//		
//		DwEvolutionOperationInterpreter.execute(evoOp);
//		operationsStack.push(evoOp);
		
		// naive implementation!
		name.setName(newValue);
	}

	public static void overrideTemporalValidity(DwTemporalInterval interval, String target, Date newValue, Date date, boolean lastDateSelected) {
//		if(!lastDateSelected) {
//			DwDialogUtil.openDialogOnce("de.darwinspl.properties.DwPropertyValueWriter:notLastDate",
//					"Unsafe Modification!",
//					"You are trying to manipulate an element's validity at an intermediate state.\n\n"
//					+ "For now, DarwinSPL does not allow such modifications. We are working on it!",
//					MessageDialog.ERROR, new String[] { "Okay" }
//			);
//			
//			throw new AbortExecutionException();
//		}
//		
//		if(target.equals("to")) {
//			EObject element = interval.eContainer();
//			if(element instanceof DwFeature) {
//				DwEvolutionOperation detachEvoOp = operationFactory.createDwFeatureDetachOperation((DwFeature) element, newValue, lastDateSelected);
//				DwEvolutionOperation deleteEvoOp = operationFactory.createDwFeatureDeleteOperation((DwFeature) element, newValue, lastDateSelected);
//				
//				DwEvolutionOperationInterpreter.execute(detachEvoOp);
//				operationsStack.push(detachEvoOp);
//				DwEvolutionOperationInterpreter.execute(deleteEvoOp);
//				operationsStack.push(deleteEvoOp);
//			}
//		}
//		else if(target.equals("from")) {
//			DwDialogUtil.openDialogOnce("de.darwinspl.properties.DwPropertyValueWriter:onlyFromSupported",
//					"Unsupported Modification!",
//					"You are trying to manipulate an element's validity starting point.\n\n"
//					+ "For now, DarwinSPL does not allow such modifications. We are working on it!",
//					MessageDialog.ERROR, new String[] { "Okay" }
//			);
//			
//			throw new AbortExecutionException();
//		}
		
		// naive approach (is it?)
		
//		if(getDateFromInterval(interval, target) == null) {
//			if(newValue == null)
//				return;
//		} else if(getDateFromInterval(interval, target).equals(newValue)) {
//			return;
//		}
//		
//		DwTemporalInterval oldInterval = EcoreUtil.copy(interval);
//		
//		doOverrideValidity(interval, target, newValue);
//		
//		EObject element = interval.eContainer();
//		if(element instanceof DwFeature) {
//			DwFeature feature = (DwFeature) element;
//			DwGroupComposition groupComposition = DwEvolutionUtil.getValidTemporalElement(feature.getGroupMembership(), newValue);
//			
//			if(groupComposition != null ) {
//				doOverrideValidity(groupComposition.getValidity(), target, newValue);
//			}
//			else {
//				// This execution is an undo attempt
//				List<DwTemporalInterval> bla = DwEvolutionUtil.getTemporalElementsForEndDate(feature.getGroupMembership(), getDateFromInterval(oldInterval, target));
//				if(bla.size() == 1)
//					doOverrideValidity(bla.get(0), target, newValue);
//				else
//					throw new AbortExecutionException();
//			}
//		}
		
		displayDialog();
	}
	
	// TODO USE DwEvolutionUtil.setTemporalValidity(element, from, to);
//	private static void doOverrideValidity(DwTemporalInterval interval, String target, Date newValue) {
//		if(target.equals("from")) {
//			interval.setFrom(newValue);
//		}
//		else if(target.equals("to")) {
//			interval.setTo(newValue);
//		}
//	}
//	
//	private static Date getDateFromInterval(DwTemporalInterval interval, String target) {
//		if(target.equals("from")) {
//			return interval.getFrom();
//		}
//		else if(target.equals("to")) {
//			return interval.getTo();
//		}
//		return null;
//	}

}













