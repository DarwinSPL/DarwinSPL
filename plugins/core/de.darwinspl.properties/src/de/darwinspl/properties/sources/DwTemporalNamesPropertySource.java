package de.darwinspl.properties.sources;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.TextPropertyDescriptor;

import de.darwinspl.feature.DwName;
import de.darwinspl.properties.DwPropertyValueWriter;
import de.darwinspl.temporal.DwTemporalInterval;
import de.darwinspl.temporal.util.DwDateResolverUtil;

public class DwTemporalNamesPropertySource extends DwAbstractPropertySource {
	
	protected List<DwName> names;
	
	public DwTemporalNamesPropertySource(List<DwName> o, Date currentDate, boolean lastDateSelected) {
		super(currentDate, lastDateSelected);
		this.names = o;
	}

	@Override
	public Object getEditableValue() {
//		DwName currentName = DwEvolutionUtil.getValidTemporalElement(this.names, this.date);
//		if(currentName == null)
//			return null;
//		return currentName.getName();
		return null;
	}

	@Override
	public IPropertyDescriptor[] getPropertyDescriptors() {
		List<IPropertyDescriptor> descriptors = new LinkedList<>();
		
		for(int i=0; i<names.size(); i++) {
			DwName name = names.get(i);
			DwTemporalInterval validity = name.getValidity();
			if(validity != null) {
				Date startDate = validity.getFrom();
				descriptors.add(new TextPropertyDescriptor(name, "[" + (i+1) + "] Since " + DwDateResolverUtil.deresolveDate(startDate)));
			}
			else {
				descriptors.add(new TextPropertyDescriptor(name, "[" + (i+1) + "] No validity specified..."));
			}
		}
		
		return descriptors.toArray(new IPropertyDescriptor[] {});
	}

	@Override
	public Object getPropertyValue(Object id) {
		if(!(id instanceof DwName))
			return null;
		
		return ((DwName) id).getName();
	}

	@Override
	public boolean isPropertySet(Object id) {
		return false;
	}

	@Override
	public void resetPropertyValue(Object id) {
	}

	@Override
	public void setPropertyValue(Object id, Object value) {
		if(id instanceof DwName && value instanceof String) {
			DwPropertyValueWriter.overrideDwName((DwName) id, (String) value, this.date, this.lastDateSelected);
		}
	}

}
