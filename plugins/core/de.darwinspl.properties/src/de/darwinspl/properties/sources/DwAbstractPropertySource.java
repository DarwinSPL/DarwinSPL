package de.darwinspl.properties.sources;

import java.util.Date;
import java.util.Observable;

import org.eclipse.ui.views.properties.IPropertySource;

public abstract class DwAbstractPropertySource extends Observable implements IPropertySource {
	
	protected DwAbstractPropertySource(Date date, boolean lastDateSelected) {
		this.date = date;
		this.lastDateSelected = lastDateSelected;
	}
	
	protected Date date;
	protected boolean lastDateSelected;

}
