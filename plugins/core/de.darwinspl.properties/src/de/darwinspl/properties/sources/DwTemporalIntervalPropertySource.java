package de.darwinspl.properties.sources;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.ui.views.properties.IPropertyDescriptor;

import de.darwinspl.properties.DwPropertyValueWriter;
import de.darwinspl.properties.validity.DwDatePropertyDescriptor;
import de.darwinspl.temporal.DwTemporalInterval;
import de.darwinspl.temporal.util.DwDateResolverUtil;

public class DwTemporalIntervalPropertySource extends DwAbstractPropertySource {
	
	protected DwTemporalInterval interval;
	
	public DwTemporalIntervalPropertySource(DwTemporalInterval interval, Date currentDate, boolean lastDateSelected) {
		super(currentDate, lastDateSelected);
		this.interval = interval;
	}

	@Override
	public Object getEditableValue() {
//		return DwDateResolverUtil.deresolveInterval((DwTemporalInterval) interval);
		return null;
	}

	@Override
	public IPropertyDescriptor[] getPropertyDescriptors() {
		List<IPropertyDescriptor> descriptors = new LinkedList<>();

		Date from = interval.getFrom();
		Date to = interval.getTo();

		descriptors.add(new DwDatePropertyDescriptor(from == null ? DwDateResolverUtil.INITIAL_STATE_DATE_STRING : from, "from"));
		descriptors.add(new DwDatePropertyDescriptor(to == null ? DwDateResolverUtil.ETERNITY_DATE_STRING : to, "to"));
		
		return descriptors.toArray(new IPropertyDescriptor[] {});
	}

	@Override
	public Object getPropertyValue(Object id) {
		if(id instanceof Date)
			return DwDateResolverUtil.deresolveDate((Date) id);
		else if(id instanceof String)
			return id;
		
		return null;
	}

	@Override
	public boolean isPropertySet(Object id) {
		return false;
	}

	@Override
	public void resetPropertyValue(Object id) {
	}

	@Override
	public void setPropertyValue(Object id, Object value) {
		if(value instanceof String)
			value = DwDateResolverUtil.resolveDate((String) value);
		
		if(id.equals(DwDateResolverUtil.INITIAL_STATE_DATE_STRING) || (interval.getFrom() != null && interval.getFrom().equals(id))) {
			DwPropertyValueWriter.overrideTemporalValidity(interval, "from", (Date) value, date, lastDateSelected);
		}
		else if(id.equals(DwDateResolverUtil.ETERNITY_DATE_STRING) || (interval.getTo() != null && interval.getTo().equals(id))) {
			DwPropertyValueWriter.overrideTemporalValidity(interval, "to", (Date) value, date, lastDateSelected);
		}
	}

}
