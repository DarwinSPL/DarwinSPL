package de.darwinspl.properties.sources;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.PropertyDescriptor;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwNamedElement;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.evolution.operation.resource.DwEvolutionOperationModelResourceUtil;
import de.darwinspl.feature.evolution.operation.resource.DwEvolutionOperationUtil;
import de.darwinspl.feature.operation.DwEvolutionOperationModel;
import de.darwinspl.feature.util.custom.DwFeatureUtil;
import de.darwinspl.temporal.DwMultiTemporalIntervalElement;
import de.darwinspl.temporal.DwTemporalElement;
import de.darwinspl.temporal.util.DwDateResolverUtil;
import de.darwinspl.temporal.util.DwEvolutionUtil;

public class DwTemporalElementPropertySource extends DwAbstractPropertySource {
	
	protected DwTemporalElement wrappedObject;
	
	public DwTemporalElementPropertySource(DwTemporalElement o, Date currentDate, boolean lastDateSelected) {
		super(currentDate, lastDateSelected);
		this.wrappedObject = o;
	}
	
	public DwTemporalElement getWrappedObject() {
		return this.wrappedObject;
	}

	@Override
	public Object getEditableValue() {
		return null;
	}

	@Override
	public IPropertyDescriptor[] getPropertyDescriptors() {
		List<IPropertyDescriptor> descriptors = new LinkedList<>();
		
		if(wrappedObject instanceof DwTemporalElement) {
			descriptors.add(new PropertyDescriptor("id", "DarwinSPL ID"));
			descriptors.add(new PropertyDescriptor("validities", "Temporal Validity"));
			if(wrappedObject instanceof DwNamedElement)
				descriptors.add(new PropertyDescriptor("names", "Name" + (((DwNamedElement) wrappedObject).getNames().size() > 1 ? "s" : "")));
		}
		if(wrappedObject instanceof DwFeature) {
			if(DwFeatureUtil.isRootFeature((DwFeature) wrappedObject, date)) {
				descriptors.add(new PropertyDescriptor("f_count", "Feature Count"));
				descriptors.add(new PropertyDescriptor("g_count", "Group Count"));
				descriptors.add(new PropertyDescriptor("t_count", "Number of Timesteps"));
				descriptors.add(new PropertyDescriptor("op_count_user", "Number of Operations (user-level)"));
				descriptors.add(new PropertyDescriptor("op_count_all", "Number of Operations (all levels)"));
			}
		}
		
		return descriptors.toArray(new IPropertyDescriptor[] {});
	}

	@Override
	public Object getPropertyValue(Object id) {
		if(!(id instanceof String))
			return null;
		
		if(((String) id).equals("id")) {
			if(this.wrappedObject instanceof DwTemporalElement)
				return ((DwTemporalElement) wrappedObject).getId();
			else
				return null;
		}
		else if(((String) id).equals("names")) {
			if(wrappedObject instanceof DwNamedElement)
				return new DwTemporalNamesPropertySource(((DwNamedElement) wrappedObject).getNames(), this.date, this.lastDateSelected);
			else
				return null;
		}
		else if(((String) id).equals("validities")) {
			if(wrappedObject instanceof DwMultiTemporalIntervalElement)
				return new DwTemporalValiditiesPropertySource(((DwMultiTemporalIntervalElement) wrappedObject), this.date, this.lastDateSelected);
			else
				return null;
		}
		else if(((String) id).equals("f_count")) {
			if(wrappedObject instanceof DwFeature)
				return ((DwFeature) wrappedObject).getFeatureModel().getFeatures().size();
			else
				return null;
		}
		else if(((String) id).equals("g_count")) {
			if(wrappedObject instanceof DwFeature)
				return ((DwFeature) wrappedObject).getFeatureModel().getGroups().size();
			else
				return null;
		}
		else if(((String) id).equals("t_count")) {
			if(wrappedObject instanceof DwFeature) {
				int nonInitialTimePoints = DwEvolutionUtil.collectDates(((DwFeature) wrappedObject).getFeatureModel()).size();
				int initialModelSize = DwFeatureUtil.getFeatures(((DwFeature) wrappedObject).getFeatureModel(), DwDateResolverUtil.INITIAL_STATE_DATE).size();
				
				if(initialModelSize > 1)
					return nonInitialTimePoints+1;
				else
					return nonInitialTimePoints;
			}
			else
				return null;
		}
		else if(((String) id).equals("op_count_user")) {
			if(wrappedObject instanceof DwFeature) {
				DwTemporalFeatureModel tfm = ((DwFeature) wrappedObject).getFeatureModel();
				DwEvolutionOperationModel operationsModel = DwEvolutionOperationModelResourceUtil.loadAndCacheOperationsModel(tfm);
				return new DwOperationsPropertySource(operationsModel.getEvolutionOperations(), this.date, this.lastDateSelected);
			}
			else
				return null;
		}
		else if(((String) id).equals("op_count_all")) {
			if(wrappedObject instanceof DwFeature) {
				DwTemporalFeatureModel tfm = ((DwFeature) wrappedObject).getFeatureModel();
				DwEvolutionOperationModel operationsModel = DwEvolutionOperationModelResourceUtil.loadAndCacheOperationsModel(tfm);
				return new DwOperationsPropertySource(DwEvolutionOperationUtil.collectNestedOperations(operationsModel), this.date, this.lastDateSelected);
			}
			else
				return null;
		}
		
		return null;
//		throw new UnsupportedOperationException("Not implemented yet: Property Value for \"" + id + "\"");
	}

	@Override
	public boolean isPropertySet(Object id) {
		return false;
	}

	@Override
	public void resetPropertyValue(Object id) {
	}

	@Override
	public void setPropertyValue(Object id, Object value) {
		this.setChanged();
		this.notifyObservers();
	}

}
