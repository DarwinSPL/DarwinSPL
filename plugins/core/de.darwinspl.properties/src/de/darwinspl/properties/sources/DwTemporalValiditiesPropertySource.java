package de.darwinspl.properties.sources;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.PropertyDescriptor;

import de.darwinspl.temporal.DwMultiTemporalIntervalElement;
import de.darwinspl.temporal.DwTemporalInterval;
import de.darwinspl.temporal.DwTemporalModelFactory;
import de.darwinspl.temporal.util.DwDateResolverUtil;

public class DwTemporalValiditiesPropertySource extends DwAbstractPropertySource {
	
	protected DwMultiTemporalIntervalElement element;
	
	public DwTemporalValiditiesPropertySource(DwMultiTemporalIntervalElement elementWithInterval, Date currentDate, boolean lastDateSelected) {
		super(currentDate, lastDateSelected);
		this.element = elementWithInterval;
	}

	@Override
	public Object getEditableValue() {
//		int validityCount = this.element.getValidities().size();
//		return "Valid within " + validityCount + " interval" + (validityCount == 1 ? "" : "s");
		return null;
	}

	@Override
	public IPropertyDescriptor[] getPropertyDescriptors() {
		List<IPropertyDescriptor> descriptors = new LinkedList<>();
		
		int validityCount = this.element.getValidities().size();
		
		for(int i=0; i<validityCount; i++) {
			DwTemporalInterval validity = element.getValidities().get(i);
//			descriptors.add(new PropertyDescriptor(validity, "[" + (i+1) + "]"));
			descriptors.add(new PropertyDescriptor(validity, DwDateResolverUtil.deresolveInterval(validity)));
		}
		
		if(validityCount == 0) {
			descriptors.add(new PropertyDescriptor(DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval(), "[1]"));
		}
		
		return descriptors.toArray(new IPropertyDescriptor[] {});
	}

	@Override
	public Object getPropertyValue(Object id) {
		if(!(id instanceof DwTemporalInterval))
			return null;

		return new DwTemporalIntervalPropertySource((DwTemporalInterval) id, this.date, this.lastDateSelected);
	}

	@Override
	public boolean isPropertySet(Object id) {
		return false;
	}

	@Override
	public void resetPropertyValue(Object id) {
	}

	@Override
	public void setPropertyValue(Object id, Object value) {
	}

}
