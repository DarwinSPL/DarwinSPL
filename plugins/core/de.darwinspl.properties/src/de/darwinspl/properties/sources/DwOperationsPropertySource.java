package de.darwinspl.properties.sources;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.PropertyDescriptor;

import de.darwinspl.feature.DwName;
import de.darwinspl.feature.evolution.operation.resource.DwEvolutionOperationUtil;
import de.darwinspl.feature.operation.DwEvolutionOperation;
import de.darwinspl.properties.DwPropertyValueWriter;

public class DwOperationsPropertySource extends DwAbstractPropertySource {
	
	protected Collection<DwEvolutionOperation> operations;
	
	public DwOperationsPropertySource(Collection<DwEvolutionOperation> operations, Date currentDate, boolean lastDateSelected) {
		super(currentDate, lastDateSelected);
		this.operations = operations;
	}

	@Override
	public Object getEditableValue() {
		return operations.size();
	}

	@Override
	public IPropertyDescriptor[] getPropertyDescriptors() {
		List<IPropertyDescriptor> descriptors = new LinkedList<>();

		descriptors.add(new PropertyDescriptor("f_create", "Feature Create Operations"));
		descriptors.add(new PropertyDescriptor("f_move", "Feature Move Operations"));
		descriptors.add(new PropertyDescriptor("f_delete", "Feature Delete Operations"));
		descriptors.add(new PropertyDescriptor("f_rename", "Feature Rename Operations"));
		descriptors.add(new PropertyDescriptor("f_type", "Feature Change Type Operations"));
		
		descriptors.add(new PropertyDescriptor("g_create", "Group Create Operations"));
		descriptors.add(new PropertyDescriptor("g_move", "Group Move Operations"));
		descriptors.add(new PropertyDescriptor("g_type", "Group Change Type Operations"));
		
		return descriptors.toArray(new IPropertyDescriptor[] {});
	}

	@Override
	public Object getPropertyValue(Object id) {
		if(!(id instanceof String))
			return null;

		Map<String, Integer> opCounts = DwEvolutionOperationUtil.getOperationCounts(operations);
		return opCounts.get(id);
	}

	@Override
	public boolean isPropertySet(Object id) {
		return false;
	}

	@Override
	public void resetPropertyValue(Object id) {
	}

	@Override
	public void setPropertyValue(Object id, Object value) {
		if(id instanceof DwName && value instanceof String) {
			DwPropertyValueWriter.overrideDwName((DwName) id, (String) value, this.date, this.lastDateSelected);
		}
	}

}
