package de.darwinspl.properties.validity;

import java.util.Date;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.swt.graphics.Image;

import de.darwinspl.temporal.util.DwDateResolverUtil;

public class DwDatePropertyEditorLabelProvider implements ILabelProvider {
	
	private String displayName;

	public DwDatePropertyEditorLabelProvider(Object displayName) {
		if(displayName instanceof String)
			this.displayName = (String) displayName;
		else if(displayName instanceof Date)
			this.displayName = DwDateResolverUtil.deresolveDate((Date) displayName);
	}

	@Override
	public void addListener(ILabelProviderListener listener) {

	}

	@Override
	public void dispose() {

	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {

	}

	@Override
	public Image getImage(Object element) {
		return null;
	}

	@Override
	public String getText(Object element) {
		return displayName;
	}

}
