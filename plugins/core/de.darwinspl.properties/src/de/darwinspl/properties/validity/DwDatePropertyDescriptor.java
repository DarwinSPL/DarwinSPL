package de.darwinspl.properties.validity;

import java.util.Date;

import org.eclipse.emf.common.ui.celleditor.ExtendedDialogCellEditor;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.views.properties.PropertyDescriptor;

import de.darwinspl.common.eclipse.ui.DwDateDialog;

public class DwDatePropertyDescriptor extends PropertyDescriptor {
	
	private Date date;

	public DwDatePropertyDescriptor(Object id, String displayName) {
		super(id, displayName);

		if(this.getId() instanceof Date)
			date = (Date) this.getId();
	}

	@Override
	public CellEditor createPropertyEditor(Composite parent) {
		
		CellEditor editor = new ExtendedDialogCellEditor(parent, new DwDatePropertyEditorLabelProvider(this.getId())) {
			
			@Override
			protected Object openDialogBox(Control cellEditorWindow) {
				DwDateDialog dialog = new DwDateDialog(Display.getCurrent().getActiveShell(), date);
				dialog.open();
				return dialog.getValue();
			}
			
		};
		
//		setValidator(new ICellEditorValidator() {
//			@Override
//			public String isValid(Object value) {
//				if(value instanceof String) {
//					if(DwDateResolverUtil.resolveDate((String) value) == null)
//						return "Invalid date format!";
//				}
//				else {
//					return "Expected a string that encodes a date";
//				}
//				return null;
//			}
//		});
		
		if (getValidator() != null) {
			editor.setValidator(getValidator());
		}
		
		return editor;
	}

}
