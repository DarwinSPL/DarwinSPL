/**
 */
package de.darwinspl.fmec.keywords.impl;

import de.darwinspl.fmec.keywords.FeatureModelEvolutionConstraintKeyword;
import de.darwinspl.fmec.keywords.KeywordsPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature Model Evolution Constraint Keyword</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class FeatureModelEvolutionConstraintKeywordImpl extends MinimalEObjectImpl.Container implements FeatureModelEvolutionConstraintKeyword {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureModelEvolutionConstraintKeywordImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return KeywordsPackage.Literals.FEATURE_MODEL_EVOLUTION_CONSTRAINT_KEYWORD;
	}

} //FeatureModelEvolutionConstraintKeywordImpl
