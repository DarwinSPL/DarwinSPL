/**
 */
package de.darwinspl.fmec.keywords.timepoint.impl;

import de.darwinspl.fmec.keywords.timepoint.KeywordAt;
import de.darwinspl.fmec.keywords.timepoint.TimepointPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Keyword At</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class KeywordAtImpl extends TimePointKeywordImpl implements KeywordAt {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected KeywordAtImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimepointPackage.Literals.KEYWORD_AT;
	}

} //KeywordAtImpl
