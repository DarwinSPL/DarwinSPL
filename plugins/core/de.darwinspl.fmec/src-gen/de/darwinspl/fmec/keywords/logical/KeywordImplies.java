/**
 */
package de.darwinspl.fmec.keywords.logical;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Keyword Implies</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.keywords.logical.LogicalPackage#getKeywordImplies()
 * @model
 * @generated
 */
public interface KeywordImplies extends BinaryLogicalKeyword {
} // KeywordImplies
