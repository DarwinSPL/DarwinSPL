/**
 */
package de.darwinspl.fmec.keywords.logical.impl;

import de.darwinspl.fmec.keywords.logical.LogicalPackage;
import de.darwinspl.fmec.keywords.logical.UnaryLogicalKeyword;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Unary Logical Keyword</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class UnaryLogicalKeywordImpl extends LogicalKeywordImpl implements UnaryLogicalKeyword {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UnaryLogicalKeywordImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LogicalPackage.Literals.UNARY_LOGICAL_KEYWORD;
	}

} //UnaryLogicalKeywordImpl
