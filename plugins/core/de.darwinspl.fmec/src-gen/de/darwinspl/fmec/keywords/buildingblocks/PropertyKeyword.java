/**
 */
package de.darwinspl.fmec.keywords.buildingblocks;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Property Keyword</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage#getPropertyKeyword()
 * @model abstract="true"
 * @generated
 */
public interface PropertyKeyword extends EObject {
} // PropertyKeyword
