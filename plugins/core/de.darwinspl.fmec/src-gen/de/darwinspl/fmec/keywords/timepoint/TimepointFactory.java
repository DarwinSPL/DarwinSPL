/**
 */
package de.darwinspl.fmec.keywords.timepoint;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.darwinspl.fmec.keywords.timepoint.TimepointPackage
 * @generated
 */
public interface TimepointFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TimepointFactory eINSTANCE = de.darwinspl.fmec.keywords.timepoint.impl.TimepointFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Keyword Before</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Keyword Before</em>'.
	 * @generated
	 */
	KeywordBefore createKeywordBefore();

	/**
	 * Returns a new object of class '<em>Keyword Until</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Keyword Until</em>'.
	 * @generated
	 */
	KeywordUntil createKeywordUntil();

	/**
	 * Returns a new object of class '<em>Keyword After</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Keyword After</em>'.
	 * @generated
	 */
	KeywordAfter createKeywordAfter();

	/**
	 * Returns a new object of class '<em>Keyword When</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Keyword When</em>'.
	 * @generated
	 */
	KeywordWhen createKeywordWhen();

	/**
	 * Returns a new object of class '<em>Keyword At</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Keyword At</em>'.
	 * @generated
	 */
	KeywordAt createKeywordAt();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TimepointPackage getTimepointPackage();

} //TimepointFactory
