/**
 */
package de.darwinspl.fmec.keywords.buildingblocks.impl;

import de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage;
import de.darwinspl.fmec.keywords.buildingblocks.KeywordStops;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Keyword Stops</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class KeywordStopsImpl extends EventKeywordImpl implements KeywordStops {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected KeywordStopsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BuildingblocksPackage.Literals.KEYWORD_STOPS;
	}

} //KeywordStopsImpl
