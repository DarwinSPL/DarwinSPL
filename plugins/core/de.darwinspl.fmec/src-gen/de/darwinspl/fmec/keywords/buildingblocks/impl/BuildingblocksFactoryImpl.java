/**
 */
package de.darwinspl.fmec.keywords.buildingblocks.impl;

import de.darwinspl.fmec.keywords.buildingblocks.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BuildingblocksFactoryImpl extends EFactoryImpl implements BuildingblocksFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static BuildingblocksFactory init() {
		try {
			BuildingblocksFactory theBuildingblocksFactory = (BuildingblocksFactory)EPackage.Registry.INSTANCE.getEFactory(BuildingblocksPackage.eNS_URI);
			if (theBuildingblocksFactory != null) {
				return theBuildingblocksFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new BuildingblocksFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BuildingblocksFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case BuildingblocksPackage.KEYWORD_STARTS: return createKeywordStarts();
			case BuildingblocksPackage.KEYWORD_STOPS: return createKeywordStops();
			case BuildingblocksPackage.KEYWORD_VALID: return createKeywordValid();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public KeywordStarts createKeywordStarts() {
		KeywordStartsImpl keywordStarts = new KeywordStartsImpl();
		return keywordStarts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public KeywordStops createKeywordStops() {
		KeywordStopsImpl keywordStops = new KeywordStopsImpl();
		return keywordStops;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public KeywordValid createKeywordValid() {
		KeywordValidImpl keywordValid = new KeywordValidImpl();
		return keywordValid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BuildingblocksPackage getBuildingblocksPackage() {
		return (BuildingblocksPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static BuildingblocksPackage getPackage() {
		return BuildingblocksPackage.eINSTANCE;
	}

} //BuildingblocksFactoryImpl
