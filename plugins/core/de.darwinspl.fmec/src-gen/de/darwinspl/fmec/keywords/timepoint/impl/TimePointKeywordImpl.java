/**
 */
package de.darwinspl.fmec.keywords.timepoint.impl;

import de.darwinspl.fmec.keywords.impl.FeatureModelEvolutionConstraintKeywordImpl;

import de.darwinspl.fmec.keywords.timepoint.TimePointKeyword;
import de.darwinspl.fmec.keywords.timepoint.TimepointPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Time Point Keyword</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class TimePointKeywordImpl extends FeatureModelEvolutionConstraintKeywordImpl implements TimePointKeyword {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimePointKeywordImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimepointPackage.Literals.TIME_POINT_KEYWORD;
	}

} //TimePointKeywordImpl
