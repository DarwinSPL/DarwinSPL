/**
 */
package de.darwinspl.fmec.keywords.timepoint;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Keyword At</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.keywords.timepoint.TimepointPackage#getKeywordAt()
 * @model
 * @generated
 */
public interface KeywordAt extends TimePointKeyword {
} // KeywordAt
