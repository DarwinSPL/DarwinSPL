/**
 */
package de.darwinspl.fmec.keywords.buildingblocks;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage
 * @generated
 */
public interface BuildingblocksFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BuildingblocksFactory eINSTANCE = de.darwinspl.fmec.keywords.buildingblocks.impl.BuildingblocksFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Keyword Starts</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Keyword Starts</em>'.
	 * @generated
	 */
	KeywordStarts createKeywordStarts();

	/**
	 * Returns a new object of class '<em>Keyword Stops</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Keyword Stops</em>'.
	 * @generated
	 */
	KeywordStops createKeywordStops();

	/**
	 * Returns a new object of class '<em>Keyword Valid</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Keyword Valid</em>'.
	 * @generated
	 */
	KeywordValid createKeywordValid();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	BuildingblocksPackage getBuildingblocksPackage();

} //BuildingblocksFactory
