/**
 */
package de.darwinspl.fmec.keywords.timepoint.util;

import de.darwinspl.fmec.keywords.FeatureModelEvolutionConstraintKeyword;

import de.darwinspl.fmec.keywords.timepoint.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see de.darwinspl.fmec.keywords.timepoint.TimepointPackage
 * @generated
 */
public class TimepointSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TimepointPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimepointSwitch() {
		if (modelPackage == null) {
			modelPackage = TimepointPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case TimepointPackage.TIME_POINT_KEYWORD: {
				TimePointKeyword timePointKeyword = (TimePointKeyword)theEObject;
				T result = caseTimePointKeyword(timePointKeyword);
				if (result == null) result = caseFeatureModelEvolutionConstraintKeyword(timePointKeyword);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TimepointPackage.KEYWORD_BEFORE: {
				KeywordBefore keywordBefore = (KeywordBefore)theEObject;
				T result = caseKeywordBefore(keywordBefore);
				if (result == null) result = caseTimePointKeyword(keywordBefore);
				if (result == null) result = caseFeatureModelEvolutionConstraintKeyword(keywordBefore);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TimepointPackage.KEYWORD_UNTIL: {
				KeywordUntil keywordUntil = (KeywordUntil)theEObject;
				T result = caseKeywordUntil(keywordUntil);
				if (result == null) result = caseTimePointKeyword(keywordUntil);
				if (result == null) result = caseFeatureModelEvolutionConstraintKeyword(keywordUntil);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TimepointPackage.KEYWORD_AFTER: {
				KeywordAfter keywordAfter = (KeywordAfter)theEObject;
				T result = caseKeywordAfter(keywordAfter);
				if (result == null) result = caseTimePointKeyword(keywordAfter);
				if (result == null) result = caseFeatureModelEvolutionConstraintKeyword(keywordAfter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TimepointPackage.KEYWORD_WHEN: {
				KeywordWhen keywordWhen = (KeywordWhen)theEObject;
				T result = caseKeywordWhen(keywordWhen);
				if (result == null) result = caseTimePointKeyword(keywordWhen);
				if (result == null) result = caseFeatureModelEvolutionConstraintKeyword(keywordWhen);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TimepointPackage.KEYWORD_AT: {
				KeywordAt keywordAt = (KeywordAt)theEObject;
				T result = caseKeywordAt(keywordAt);
				if (result == null) result = caseTimePointKeyword(keywordAt);
				if (result == null) result = caseFeatureModelEvolutionConstraintKeyword(keywordAt);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Time Point Keyword</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Time Point Keyword</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTimePointKeyword(TimePointKeyword object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Keyword Before</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Keyword Before</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseKeywordBefore(KeywordBefore object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Keyword Until</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Keyword Until</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseKeywordUntil(KeywordUntil object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Keyword After</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Keyword After</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseKeywordAfter(KeywordAfter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Keyword When</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Keyword When</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseKeywordWhen(KeywordWhen object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Keyword At</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Keyword At</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseKeywordAt(KeywordAt object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature Model Evolution Constraint Keyword</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature Model Evolution Constraint Keyword</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeatureModelEvolutionConstraintKeyword(FeatureModelEvolutionConstraintKeyword object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //TimepointSwitch
