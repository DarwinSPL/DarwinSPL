/**
 */
package de.darwinspl.fmec.keywords.interval;

import de.darwinspl.fmec.keywords.FeatureModelEvolutionConstraintKeyword;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Keyword</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.keywords.interval.IntervalPackage#getIntervalKeyword()
 * @model abstract="true"
 * @generated
 */
public interface IntervalKeyword extends FeatureModelEvolutionConstraintKeyword {
} // IntervalKeyword
