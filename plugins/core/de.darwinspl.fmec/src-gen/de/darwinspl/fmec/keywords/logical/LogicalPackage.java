/**
 */
package de.darwinspl.fmec.keywords.logical;

import de.darwinspl.fmec.keywords.KeywordsPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.darwinspl.fmec.keywords.logical.LogicalFactory
 * @model kind="package"
 * @generated
 */
public interface LogicalPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "logical";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.darwinspl.de/feature-model-evolution-constraints/keywords/logical/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "logical";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LogicalPackage eINSTANCE = de.darwinspl.fmec.keywords.logical.impl.LogicalPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.keywords.logical.impl.LogicalKeywordImpl <em>Keyword</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.keywords.logical.impl.LogicalKeywordImpl
	 * @see de.darwinspl.fmec.keywords.logical.impl.LogicalPackageImpl#getLogicalKeyword()
	 * @generated
	 */
	int LOGICAL_KEYWORD = 0;

	/**
	 * The number of structural features of the '<em>Keyword</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_KEYWORD_FEATURE_COUNT = KeywordsPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_KEYWORD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Keyword</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_KEYWORD_OPERATION_COUNT = KeywordsPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_KEYWORD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.keywords.logical.impl.BinaryLogicalKeywordImpl <em>Binary Logical Keyword</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.keywords.logical.impl.BinaryLogicalKeywordImpl
	 * @see de.darwinspl.fmec.keywords.logical.impl.LogicalPackageImpl#getBinaryLogicalKeyword()
	 * @generated
	 */
	int BINARY_LOGICAL_KEYWORD = 1;

	/**
	 * The number of structural features of the '<em>Binary Logical Keyword</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_LOGICAL_KEYWORD_FEATURE_COUNT = LOGICAL_KEYWORD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Binary Logical Keyword</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_LOGICAL_KEYWORD_OPERATION_COUNT = LOGICAL_KEYWORD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.keywords.logical.impl.KeywordAndImpl <em>Keyword And</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.keywords.logical.impl.KeywordAndImpl
	 * @see de.darwinspl.fmec.keywords.logical.impl.LogicalPackageImpl#getKeywordAnd()
	 * @generated
	 */
	int KEYWORD_AND = 2;

	/**
	 * The number of structural features of the '<em>Keyword And</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_AND_FEATURE_COUNT = BINARY_LOGICAL_KEYWORD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Keyword And</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_AND_OPERATION_COUNT = BINARY_LOGICAL_KEYWORD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.keywords.logical.impl.KeywordOrImpl <em>Keyword Or</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.keywords.logical.impl.KeywordOrImpl
	 * @see de.darwinspl.fmec.keywords.logical.impl.LogicalPackageImpl#getKeywordOr()
	 * @generated
	 */
	int KEYWORD_OR = 3;

	/**
	 * The number of structural features of the '<em>Keyword Or</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_OR_FEATURE_COUNT = BINARY_LOGICAL_KEYWORD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Keyword Or</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_OR_OPERATION_COUNT = BINARY_LOGICAL_KEYWORD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.keywords.logical.impl.KeywordImpliesImpl <em>Keyword Implies</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.keywords.logical.impl.KeywordImpliesImpl
	 * @see de.darwinspl.fmec.keywords.logical.impl.LogicalPackageImpl#getKeywordImplies()
	 * @generated
	 */
	int KEYWORD_IMPLIES = 4;

	/**
	 * The number of structural features of the '<em>Keyword Implies</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_IMPLIES_FEATURE_COUNT = BINARY_LOGICAL_KEYWORD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Keyword Implies</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_IMPLIES_OPERATION_COUNT = BINARY_LOGICAL_KEYWORD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.keywords.logical.impl.UnaryLogicalKeywordImpl <em>Unary Logical Keyword</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.keywords.logical.impl.UnaryLogicalKeywordImpl
	 * @see de.darwinspl.fmec.keywords.logical.impl.LogicalPackageImpl#getUnaryLogicalKeyword()
	 * @generated
	 */
	int UNARY_LOGICAL_KEYWORD = 5;

	/**
	 * The number of structural features of the '<em>Unary Logical Keyword</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_LOGICAL_KEYWORD_FEATURE_COUNT = LOGICAL_KEYWORD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Unary Logical Keyword</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_LOGICAL_KEYWORD_OPERATION_COUNT = LOGICAL_KEYWORD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.keywords.logical.impl.KeywordNotImpl <em>Keyword Not</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.keywords.logical.impl.KeywordNotImpl
	 * @see de.darwinspl.fmec.keywords.logical.impl.LogicalPackageImpl#getKeywordNot()
	 * @generated
	 */
	int KEYWORD_NOT = 6;

	/**
	 * The number of structural features of the '<em>Keyword Not</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_NOT_FEATURE_COUNT = UNARY_LOGICAL_KEYWORD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Keyword Not</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_NOT_OPERATION_COUNT = UNARY_LOGICAL_KEYWORD_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.keywords.logical.LogicalKeyword <em>Keyword</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Keyword</em>'.
	 * @see de.darwinspl.fmec.keywords.logical.LogicalKeyword
	 * @generated
	 */
	EClass getLogicalKeyword();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.keywords.logical.BinaryLogicalKeyword <em>Binary Logical Keyword</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Logical Keyword</em>'.
	 * @see de.darwinspl.fmec.keywords.logical.BinaryLogicalKeyword
	 * @generated
	 */
	EClass getBinaryLogicalKeyword();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.keywords.logical.KeywordAnd <em>Keyword And</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Keyword And</em>'.
	 * @see de.darwinspl.fmec.keywords.logical.KeywordAnd
	 * @generated
	 */
	EClass getKeywordAnd();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.keywords.logical.KeywordOr <em>Keyword Or</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Keyword Or</em>'.
	 * @see de.darwinspl.fmec.keywords.logical.KeywordOr
	 * @generated
	 */
	EClass getKeywordOr();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.keywords.logical.KeywordImplies <em>Keyword Implies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Keyword Implies</em>'.
	 * @see de.darwinspl.fmec.keywords.logical.KeywordImplies
	 * @generated
	 */
	EClass getKeywordImplies();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.keywords.logical.UnaryLogicalKeyword <em>Unary Logical Keyword</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Logical Keyword</em>'.
	 * @see de.darwinspl.fmec.keywords.logical.UnaryLogicalKeyword
	 * @generated
	 */
	EClass getUnaryLogicalKeyword();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.keywords.logical.KeywordNot <em>Keyword Not</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Keyword Not</em>'.
	 * @see de.darwinspl.fmec.keywords.logical.KeywordNot
	 * @generated
	 */
	EClass getKeywordNot();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	LogicalFactory getLogicalFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.keywords.logical.impl.LogicalKeywordImpl <em>Keyword</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.keywords.logical.impl.LogicalKeywordImpl
		 * @see de.darwinspl.fmec.keywords.logical.impl.LogicalPackageImpl#getLogicalKeyword()
		 * @generated
		 */
		EClass LOGICAL_KEYWORD = eINSTANCE.getLogicalKeyword();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.keywords.logical.impl.BinaryLogicalKeywordImpl <em>Binary Logical Keyword</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.keywords.logical.impl.BinaryLogicalKeywordImpl
		 * @see de.darwinspl.fmec.keywords.logical.impl.LogicalPackageImpl#getBinaryLogicalKeyword()
		 * @generated
		 */
		EClass BINARY_LOGICAL_KEYWORD = eINSTANCE.getBinaryLogicalKeyword();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.keywords.logical.impl.KeywordAndImpl <em>Keyword And</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.keywords.logical.impl.KeywordAndImpl
		 * @see de.darwinspl.fmec.keywords.logical.impl.LogicalPackageImpl#getKeywordAnd()
		 * @generated
		 */
		EClass KEYWORD_AND = eINSTANCE.getKeywordAnd();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.keywords.logical.impl.KeywordOrImpl <em>Keyword Or</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.keywords.logical.impl.KeywordOrImpl
		 * @see de.darwinspl.fmec.keywords.logical.impl.LogicalPackageImpl#getKeywordOr()
		 * @generated
		 */
		EClass KEYWORD_OR = eINSTANCE.getKeywordOr();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.keywords.logical.impl.KeywordImpliesImpl <em>Keyword Implies</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.keywords.logical.impl.KeywordImpliesImpl
		 * @see de.darwinspl.fmec.keywords.logical.impl.LogicalPackageImpl#getKeywordImplies()
		 * @generated
		 */
		EClass KEYWORD_IMPLIES = eINSTANCE.getKeywordImplies();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.keywords.logical.impl.UnaryLogicalKeywordImpl <em>Unary Logical Keyword</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.keywords.logical.impl.UnaryLogicalKeywordImpl
		 * @see de.darwinspl.fmec.keywords.logical.impl.LogicalPackageImpl#getUnaryLogicalKeyword()
		 * @generated
		 */
		EClass UNARY_LOGICAL_KEYWORD = eINSTANCE.getUnaryLogicalKeyword();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.keywords.logical.impl.KeywordNotImpl <em>Keyword Not</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.keywords.logical.impl.KeywordNotImpl
		 * @see de.darwinspl.fmec.keywords.logical.impl.LogicalPackageImpl#getKeywordNot()
		 * @generated
		 */
		EClass KEYWORD_NOT = eINSTANCE.getKeywordNot();

	}

} //LogicalPackage
