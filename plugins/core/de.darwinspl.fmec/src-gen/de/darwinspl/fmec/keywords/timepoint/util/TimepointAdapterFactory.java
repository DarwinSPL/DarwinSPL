/**
 */
package de.darwinspl.fmec.keywords.timepoint.util;

import de.darwinspl.fmec.keywords.FeatureModelEvolutionConstraintKeyword;

import de.darwinspl.fmec.keywords.timepoint.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see de.darwinspl.fmec.keywords.timepoint.TimepointPackage
 * @generated
 */
public class TimepointAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TimepointPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimepointAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = TimepointPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimepointSwitch<Adapter> modelSwitch =
		new TimepointSwitch<Adapter>() {
			@Override
			public Adapter caseTimePointKeyword(TimePointKeyword object) {
				return createTimePointKeywordAdapter();
			}
			@Override
			public Adapter caseKeywordBefore(KeywordBefore object) {
				return createKeywordBeforeAdapter();
			}
			@Override
			public Adapter caseKeywordUntil(KeywordUntil object) {
				return createKeywordUntilAdapter();
			}
			@Override
			public Adapter caseKeywordAfter(KeywordAfter object) {
				return createKeywordAfterAdapter();
			}
			@Override
			public Adapter caseKeywordWhen(KeywordWhen object) {
				return createKeywordWhenAdapter();
			}
			@Override
			public Adapter caseKeywordAt(KeywordAt object) {
				return createKeywordAtAdapter();
			}
			@Override
			public Adapter caseFeatureModelEvolutionConstraintKeyword(FeatureModelEvolutionConstraintKeyword object) {
				return createFeatureModelEvolutionConstraintKeywordAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.fmec.keywords.timepoint.TimePointKeyword <em>Time Point Keyword</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.fmec.keywords.timepoint.TimePointKeyword
	 * @generated
	 */
	public Adapter createTimePointKeywordAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.fmec.keywords.timepoint.KeywordBefore <em>Keyword Before</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.fmec.keywords.timepoint.KeywordBefore
	 * @generated
	 */
	public Adapter createKeywordBeforeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.fmec.keywords.timepoint.KeywordUntil <em>Keyword Until</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.fmec.keywords.timepoint.KeywordUntil
	 * @generated
	 */
	public Adapter createKeywordUntilAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.fmec.keywords.timepoint.KeywordAfter <em>Keyword After</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.fmec.keywords.timepoint.KeywordAfter
	 * @generated
	 */
	public Adapter createKeywordAfterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.fmec.keywords.timepoint.KeywordWhen <em>Keyword When</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.fmec.keywords.timepoint.KeywordWhen
	 * @generated
	 */
	public Adapter createKeywordWhenAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.fmec.keywords.timepoint.KeywordAt <em>Keyword At</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.fmec.keywords.timepoint.KeywordAt
	 * @generated
	 */
	public Adapter createKeywordAtAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.fmec.keywords.FeatureModelEvolutionConstraintKeyword <em>Feature Model Evolution Constraint Keyword</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.fmec.keywords.FeatureModelEvolutionConstraintKeyword
	 * @generated
	 */
	public Adapter createFeatureModelEvolutionConstraintKeywordAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //TimepointAdapterFactory
