/**
 */
package de.darwinspl.fmec.keywords.buildingblocks;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Keyword Starts</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage#getKeywordStarts()
 * @model
 * @generated
 */
public interface KeywordStarts extends EventKeyword {
} // KeywordStarts
