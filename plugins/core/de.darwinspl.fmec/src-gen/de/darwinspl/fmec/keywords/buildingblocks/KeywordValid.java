/**
 */
package de.darwinspl.fmec.keywords.buildingblocks;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Keyword Valid</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage#getKeywordValid()
 * @model
 * @generated
 */
public interface KeywordValid extends PropertyKeyword {
} // KeywordValid
