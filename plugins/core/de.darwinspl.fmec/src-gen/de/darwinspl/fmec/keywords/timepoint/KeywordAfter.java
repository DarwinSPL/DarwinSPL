/**
 */
package de.darwinspl.fmec.keywords.timepoint;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Keyword After</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.keywords.timepoint.TimepointPackage#getKeywordAfter()
 * @model
 * @generated
 */
public interface KeywordAfter extends TimePointKeyword {
} // KeywordAfter
