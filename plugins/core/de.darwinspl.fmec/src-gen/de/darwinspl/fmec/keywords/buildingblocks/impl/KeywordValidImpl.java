/**
 */
package de.darwinspl.fmec.keywords.buildingblocks.impl;

import de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage;
import de.darwinspl.fmec.keywords.buildingblocks.KeywordValid;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Keyword Valid</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class KeywordValidImpl extends PropertyKeywordImpl implements KeywordValid {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected KeywordValidImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BuildingblocksPackage.Literals.KEYWORD_VALID;
	}

} //KeywordValidImpl
