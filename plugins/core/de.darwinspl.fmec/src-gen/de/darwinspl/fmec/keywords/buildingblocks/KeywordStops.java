/**
 */
package de.darwinspl.fmec.keywords.buildingblocks;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Keyword Stops</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage#getKeywordStops()
 * @model
 * @generated
 */
public interface KeywordStops extends EventKeyword {
} // KeywordStops
