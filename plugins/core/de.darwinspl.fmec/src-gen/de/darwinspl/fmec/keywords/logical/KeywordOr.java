/**
 */
package de.darwinspl.fmec.keywords.logical;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Keyword Or</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.keywords.logical.LogicalPackage#getKeywordOr()
 * @model
 * @generated
 */
public interface KeywordOr extends BinaryLogicalKeyword {
} // KeywordOr
