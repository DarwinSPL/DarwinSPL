/**
 */
package de.darwinspl.fmec.keywords.timepoint;

import de.darwinspl.fmec.keywords.FeatureModelEvolutionConstraintKeyword;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Time Point Keyword</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.keywords.timepoint.TimepointPackage#getTimePointKeyword()
 * @model abstract="true"
 * @generated
 */
public interface TimePointKeyword extends FeatureModelEvolutionConstraintKeyword {
} // TimePointKeyword
