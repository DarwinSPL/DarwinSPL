/**
 */
package de.darwinspl.fmec.keywords.logical;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unary Logical Keyword</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.keywords.logical.LogicalPackage#getUnaryLogicalKeyword()
 * @model abstract="true"
 * @generated
 */
public interface UnaryLogicalKeyword extends LogicalKeyword {
} // UnaryLogicalKeyword
