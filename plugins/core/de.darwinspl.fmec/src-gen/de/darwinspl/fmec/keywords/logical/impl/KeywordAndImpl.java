/**
 */
package de.darwinspl.fmec.keywords.logical.impl;

import de.darwinspl.fmec.keywords.logical.KeywordAnd;
import de.darwinspl.fmec.keywords.logical.LogicalPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Keyword And</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class KeywordAndImpl extends BinaryLogicalKeywordImpl implements KeywordAnd {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected KeywordAndImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LogicalPackage.Literals.KEYWORD_AND;
	}

} //KeywordAndImpl
