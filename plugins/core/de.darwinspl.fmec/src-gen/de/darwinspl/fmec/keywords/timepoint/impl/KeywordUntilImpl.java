/**
 */
package de.darwinspl.fmec.keywords.timepoint.impl;

import de.darwinspl.fmec.keywords.timepoint.KeywordUntil;
import de.darwinspl.fmec.keywords.timepoint.TimepointPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Keyword Until</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class KeywordUntilImpl extends TimePointKeywordImpl implements KeywordUntil {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected KeywordUntilImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimepointPackage.Literals.KEYWORD_UNTIL;
	}

} //KeywordUntilImpl
