/**
 */
package de.darwinspl.fmec.keywords;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Model Evolution Constraint Keyword</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.keywords.KeywordsPackage#getFeatureModelEvolutionConstraintKeyword()
 * @model abstract="true"
 * @generated
 */
public interface FeatureModelEvolutionConstraintKeyword extends EObject {
} // FeatureModelEvolutionConstraintKeyword
