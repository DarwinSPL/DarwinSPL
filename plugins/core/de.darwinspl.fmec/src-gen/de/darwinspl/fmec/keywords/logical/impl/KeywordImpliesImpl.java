/**
 */
package de.darwinspl.fmec.keywords.logical.impl;

import de.darwinspl.fmec.keywords.logical.KeywordImplies;
import de.darwinspl.fmec.keywords.logical.LogicalPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Keyword Implies</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class KeywordImpliesImpl extends BinaryLogicalKeywordImpl implements KeywordImplies {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected KeywordImpliesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LogicalPackage.Literals.KEYWORD_IMPLIES;
	}

} //KeywordImpliesImpl
