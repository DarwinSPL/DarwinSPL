/**
 */
package de.darwinspl.fmec.keywords.buildingblocks.impl;

import de.darwinspl.datatypes.DwDatatypesPackage;

import de.darwinspl.feature.DwFeaturePackage;

import de.darwinspl.fmec.FmecPackage;

import de.darwinspl.fmec.constraints.ConstraintsPackage;

import de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl;

import de.darwinspl.fmec.impl.FmecPackageImpl;

import de.darwinspl.fmec.keywords.KeywordsPackage;

import de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksFactory;
import de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage;
import de.darwinspl.fmec.keywords.buildingblocks.EventKeyword;
import de.darwinspl.fmec.keywords.buildingblocks.KeywordStarts;
import de.darwinspl.fmec.keywords.buildingblocks.KeywordStops;
import de.darwinspl.fmec.keywords.buildingblocks.KeywordValid;
import de.darwinspl.fmec.keywords.buildingblocks.PropertyKeyword;

import de.darwinspl.fmec.keywords.impl.KeywordsPackageImpl;

import de.darwinspl.fmec.keywords.interval.IntervalPackage;

import de.darwinspl.fmec.keywords.interval.impl.IntervalPackageImpl;

import de.darwinspl.fmec.keywords.logical.LogicalPackage;

import de.darwinspl.fmec.keywords.logical.impl.LogicalPackageImpl;

import de.darwinspl.fmec.property.PropertyPackage;

import de.darwinspl.fmec.property.impl.PropertyPackageImpl;

import de.darwinspl.fmec.timepoint.TimepointPackage;

import de.darwinspl.fmec.timepoint.impl.TimepointPackageImpl;

import de.darwinspl.temporal.DwTemporalModelPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BuildingblocksPackageImpl extends EPackageImpl implements BuildingblocksPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventKeywordEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass keywordStartsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass keywordStopsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propertyKeywordEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass keywordValidEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private BuildingblocksPackageImpl() {
		super(eNS_URI, BuildingblocksFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link BuildingblocksPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static BuildingblocksPackage init() {
		if (isInited) return (BuildingblocksPackage)EPackage.Registry.INSTANCE.getEPackage(BuildingblocksPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredBuildingblocksPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		BuildingblocksPackageImpl theBuildingblocksPackage = registeredBuildingblocksPackage instanceof BuildingblocksPackageImpl ? (BuildingblocksPackageImpl)registeredBuildingblocksPackage : new BuildingblocksPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		DwDatatypesPackage.eINSTANCE.eClass();
		DwFeaturePackage.eINSTANCE.eClass();
		DwTemporalModelPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(FmecPackage.eNS_URI);
		FmecPackageImpl theFmecPackage = (FmecPackageImpl)(registeredPackage instanceof FmecPackageImpl ? registeredPackage : FmecPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ConstraintsPackage.eNS_URI);
		ConstraintsPackageImpl theConstraintsPackage = (ConstraintsPackageImpl)(registeredPackage instanceof ConstraintsPackageImpl ? registeredPackage : ConstraintsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(PropertyPackage.eNS_URI);
		PropertyPackageImpl thePropertyPackage = (PropertyPackageImpl)(registeredPackage instanceof PropertyPackageImpl ? registeredPackage : PropertyPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimepointPackage.eNS_URI);
		TimepointPackageImpl theTimepointPackage = (TimepointPackageImpl)(registeredPackage instanceof TimepointPackageImpl ? registeredPackage : TimepointPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(KeywordsPackage.eNS_URI);
		KeywordsPackageImpl theKeywordsPackage = (KeywordsPackageImpl)(registeredPackage instanceof KeywordsPackageImpl ? registeredPackage : KeywordsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(LogicalPackage.eNS_URI);
		LogicalPackageImpl theLogicalPackage = (LogicalPackageImpl)(registeredPackage instanceof LogicalPackageImpl ? registeredPackage : LogicalPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(de.darwinspl.fmec.keywords.timepoint.TimepointPackage.eNS_URI);
		de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl theTimepointPackage_1 = (de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl)(registeredPackage instanceof de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl ? registeredPackage : de.darwinspl.fmec.keywords.timepoint.TimepointPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(IntervalPackage.eNS_URI);
		IntervalPackageImpl theIntervalPackage = (IntervalPackageImpl)(registeredPackage instanceof IntervalPackageImpl ? registeredPackage : IntervalPackage.eINSTANCE);

		// Create package meta-data objects
		theBuildingblocksPackage.createPackageContents();
		theFmecPackage.createPackageContents();
		theConstraintsPackage.createPackageContents();
		thePropertyPackage.createPackageContents();
		theTimepointPackage.createPackageContents();
		theKeywordsPackage.createPackageContents();
		theLogicalPackage.createPackageContents();
		theTimepointPackage_1.createPackageContents();
		theIntervalPackage.createPackageContents();

		// Initialize created meta-data
		theBuildingblocksPackage.initializePackageContents();
		theFmecPackage.initializePackageContents();
		theConstraintsPackage.initializePackageContents();
		thePropertyPackage.initializePackageContents();
		theTimepointPackage.initializePackageContents();
		theKeywordsPackage.initializePackageContents();
		theLogicalPackage.initializePackageContents();
		theTimepointPackage_1.initializePackageContents();
		theIntervalPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theBuildingblocksPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(BuildingblocksPackage.eNS_URI, theBuildingblocksPackage);
		return theBuildingblocksPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEventKeyword() {
		return eventKeywordEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getKeywordStarts() {
		return keywordStartsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getKeywordStops() {
		return keywordStopsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPropertyKeyword() {
		return propertyKeywordEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getKeywordValid() {
		return keywordValidEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BuildingblocksFactory getBuildingblocksFactory() {
		return (BuildingblocksFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		eventKeywordEClass = createEClass(EVENT_KEYWORD);

		keywordStartsEClass = createEClass(KEYWORD_STARTS);

		keywordStopsEClass = createEClass(KEYWORD_STOPS);

		propertyKeywordEClass = createEClass(PROPERTY_KEYWORD);

		keywordValidEClass = createEClass(KEYWORD_VALID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		KeywordsPackage theKeywordsPackage = (KeywordsPackage)EPackage.Registry.INSTANCE.getEPackage(KeywordsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		eventKeywordEClass.getESuperTypes().add(theKeywordsPackage.getFeatureModelEvolutionConstraintKeyword());
		keywordStartsEClass.getESuperTypes().add(this.getEventKeyword());
		keywordStopsEClass.getESuperTypes().add(this.getEventKeyword());
		keywordValidEClass.getESuperTypes().add(this.getPropertyKeyword());

		// Initialize classes, features, and operations; add parameters
		initEClass(eventKeywordEClass, EventKeyword.class, "EventKeyword", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(keywordStartsEClass, KeywordStarts.class, "KeywordStarts", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(keywordStopsEClass, KeywordStops.class, "KeywordStops", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(propertyKeywordEClass, PropertyKeyword.class, "PropertyKeyword", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(keywordValidEClass, KeywordValid.class, "KeywordValid", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
	}

} //BuildingblocksPackageImpl
