/**
 */
package de.darwinspl.fmec.keywords.timepoint;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Keyword Before</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.keywords.timepoint.TimepointPackage#getKeywordBefore()
 * @model
 * @generated
 */
public interface KeywordBefore extends TimePointKeyword {
} // KeywordBefore
