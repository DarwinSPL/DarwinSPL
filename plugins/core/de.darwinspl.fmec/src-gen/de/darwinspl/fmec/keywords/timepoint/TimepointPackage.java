/**
 */
package de.darwinspl.fmec.keywords.timepoint;

import de.darwinspl.fmec.keywords.KeywordsPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.darwinspl.fmec.keywords.timepoint.TimepointFactory
 * @model kind="package"
 * @generated
 */
public interface TimepointPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "timepoint";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.darwinspl.de/feature-model-evolution-constraints/keywords/timepoint/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "timepoint";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TimepointPackage eINSTANCE = de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.keywords.timepoint.impl.TimePointKeywordImpl <em>Time Point Keyword</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.keywords.timepoint.impl.TimePointKeywordImpl
	 * @see de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl#getTimePointKeyword()
	 * @generated
	 */
	int TIME_POINT_KEYWORD = 0;

	/**
	 * The number of structural features of the '<em>Time Point Keyword</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_POINT_KEYWORD_FEATURE_COUNT = KeywordsPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_KEYWORD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Time Point Keyword</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_POINT_KEYWORD_OPERATION_COUNT = KeywordsPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_KEYWORD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.keywords.timepoint.impl.KeywordBeforeImpl <em>Keyword Before</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.keywords.timepoint.impl.KeywordBeforeImpl
	 * @see de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl#getKeywordBefore()
	 * @generated
	 */
	int KEYWORD_BEFORE = 1;

	/**
	 * The number of structural features of the '<em>Keyword Before</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_BEFORE_FEATURE_COUNT = TIME_POINT_KEYWORD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Keyword Before</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_BEFORE_OPERATION_COUNT = TIME_POINT_KEYWORD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.keywords.timepoint.impl.KeywordUntilImpl <em>Keyword Until</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.keywords.timepoint.impl.KeywordUntilImpl
	 * @see de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl#getKeywordUntil()
	 * @generated
	 */
	int KEYWORD_UNTIL = 2;

	/**
	 * The number of structural features of the '<em>Keyword Until</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_UNTIL_FEATURE_COUNT = TIME_POINT_KEYWORD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Keyword Until</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_UNTIL_OPERATION_COUNT = TIME_POINT_KEYWORD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.keywords.timepoint.impl.KeywordAfterImpl <em>Keyword After</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.keywords.timepoint.impl.KeywordAfterImpl
	 * @see de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl#getKeywordAfter()
	 * @generated
	 */
	int KEYWORD_AFTER = 3;

	/**
	 * The number of structural features of the '<em>Keyword After</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_AFTER_FEATURE_COUNT = TIME_POINT_KEYWORD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Keyword After</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_AFTER_OPERATION_COUNT = TIME_POINT_KEYWORD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.keywords.timepoint.impl.KeywordWhenImpl <em>Keyword When</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.keywords.timepoint.impl.KeywordWhenImpl
	 * @see de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl#getKeywordWhen()
	 * @generated
	 */
	int KEYWORD_WHEN = 4;

	/**
	 * The number of structural features of the '<em>Keyword When</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_WHEN_FEATURE_COUNT = TIME_POINT_KEYWORD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Keyword When</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_WHEN_OPERATION_COUNT = TIME_POINT_KEYWORD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.keywords.timepoint.impl.KeywordAtImpl <em>Keyword At</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.keywords.timepoint.impl.KeywordAtImpl
	 * @see de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl#getKeywordAt()
	 * @generated
	 */
	int KEYWORD_AT = 5;

	/**
	 * The number of structural features of the '<em>Keyword At</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_AT_FEATURE_COUNT = TIME_POINT_KEYWORD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Keyword At</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_AT_OPERATION_COUNT = TIME_POINT_KEYWORD_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.keywords.timepoint.TimePointKeyword <em>Time Point Keyword</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Time Point Keyword</em>'.
	 * @see de.darwinspl.fmec.keywords.timepoint.TimePointKeyword
	 * @generated
	 */
	EClass getTimePointKeyword();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.keywords.timepoint.KeywordBefore <em>Keyword Before</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Keyword Before</em>'.
	 * @see de.darwinspl.fmec.keywords.timepoint.KeywordBefore
	 * @generated
	 */
	EClass getKeywordBefore();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.keywords.timepoint.KeywordUntil <em>Keyword Until</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Keyword Until</em>'.
	 * @see de.darwinspl.fmec.keywords.timepoint.KeywordUntil
	 * @generated
	 */
	EClass getKeywordUntil();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.keywords.timepoint.KeywordAfter <em>Keyword After</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Keyword After</em>'.
	 * @see de.darwinspl.fmec.keywords.timepoint.KeywordAfter
	 * @generated
	 */
	EClass getKeywordAfter();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.keywords.timepoint.KeywordWhen <em>Keyword When</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Keyword When</em>'.
	 * @see de.darwinspl.fmec.keywords.timepoint.KeywordWhen
	 * @generated
	 */
	EClass getKeywordWhen();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.keywords.timepoint.KeywordAt <em>Keyword At</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Keyword At</em>'.
	 * @see de.darwinspl.fmec.keywords.timepoint.KeywordAt
	 * @generated
	 */
	EClass getKeywordAt();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TimepointFactory getTimepointFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.keywords.timepoint.impl.TimePointKeywordImpl <em>Time Point Keyword</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.keywords.timepoint.impl.TimePointKeywordImpl
		 * @see de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl#getTimePointKeyword()
		 * @generated
		 */
		EClass TIME_POINT_KEYWORD = eINSTANCE.getTimePointKeyword();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.keywords.timepoint.impl.KeywordBeforeImpl <em>Keyword Before</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.keywords.timepoint.impl.KeywordBeforeImpl
		 * @see de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl#getKeywordBefore()
		 * @generated
		 */
		EClass KEYWORD_BEFORE = eINSTANCE.getKeywordBefore();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.keywords.timepoint.impl.KeywordUntilImpl <em>Keyword Until</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.keywords.timepoint.impl.KeywordUntilImpl
		 * @see de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl#getKeywordUntil()
		 * @generated
		 */
		EClass KEYWORD_UNTIL = eINSTANCE.getKeywordUntil();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.keywords.timepoint.impl.KeywordAfterImpl <em>Keyword After</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.keywords.timepoint.impl.KeywordAfterImpl
		 * @see de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl#getKeywordAfter()
		 * @generated
		 */
		EClass KEYWORD_AFTER = eINSTANCE.getKeywordAfter();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.keywords.timepoint.impl.KeywordWhenImpl <em>Keyword When</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.keywords.timepoint.impl.KeywordWhenImpl
		 * @see de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl#getKeywordWhen()
		 * @generated
		 */
		EClass KEYWORD_WHEN = eINSTANCE.getKeywordWhen();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.keywords.timepoint.impl.KeywordAtImpl <em>Keyword At</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.keywords.timepoint.impl.KeywordAtImpl
		 * @see de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl#getKeywordAt()
		 * @generated
		 */
		EClass KEYWORD_AT = eINSTANCE.getKeywordAt();

	}

} //TimepointPackage
