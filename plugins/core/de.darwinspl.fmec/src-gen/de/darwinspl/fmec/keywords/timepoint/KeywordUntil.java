/**
 */
package de.darwinspl.fmec.keywords.timepoint;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Keyword Until</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.keywords.timepoint.TimepointPackage#getKeywordUntil()
 * @model
 * @generated
 */
public interface KeywordUntil extends TimePointKeyword {
} // KeywordUntil
