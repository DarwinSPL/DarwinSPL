/**
 */
package de.darwinspl.fmec.keywords.interval.impl;

import de.darwinspl.fmec.keywords.interval.IntervalPackage;
import de.darwinspl.fmec.keywords.interval.KeywordDuring;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Keyword During</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class KeywordDuringImpl extends IntervalKeywordImpl implements KeywordDuring {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected KeywordDuringImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IntervalPackage.Literals.KEYWORD_DURING;
	}

} //KeywordDuringImpl
