/**
 */
package de.darwinspl.fmec.keywords.logical.impl;

import de.darwinspl.datatypes.DwDatatypesPackage;

import de.darwinspl.feature.DwFeaturePackage;

import de.darwinspl.fmec.FmecPackage;

import de.darwinspl.fmec.constraints.ConstraintsPackage;

import de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl;

import de.darwinspl.fmec.impl.FmecPackageImpl;

import de.darwinspl.fmec.keywords.KeywordsPackage;

import de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage;

import de.darwinspl.fmec.keywords.buildingblocks.impl.BuildingblocksPackageImpl;

import de.darwinspl.fmec.keywords.impl.KeywordsPackageImpl;

import de.darwinspl.fmec.keywords.interval.IntervalPackage;

import de.darwinspl.fmec.keywords.interval.impl.IntervalPackageImpl;

import de.darwinspl.fmec.keywords.logical.BinaryLogicalKeyword;
import de.darwinspl.fmec.keywords.logical.KeywordAnd;
import de.darwinspl.fmec.keywords.logical.KeywordImplies;
import de.darwinspl.fmec.keywords.logical.KeywordNot;
import de.darwinspl.fmec.keywords.logical.KeywordOr;
import de.darwinspl.fmec.keywords.logical.LogicalFactory;
import de.darwinspl.fmec.keywords.logical.LogicalKeyword;
import de.darwinspl.fmec.keywords.logical.LogicalPackage;
import de.darwinspl.fmec.keywords.logical.UnaryLogicalKeyword;

import de.darwinspl.fmec.property.PropertyPackage;

import de.darwinspl.fmec.property.impl.PropertyPackageImpl;

import de.darwinspl.fmec.timepoint.TimepointPackage;

import de.darwinspl.fmec.timepoint.impl.TimepointPackageImpl;

import de.darwinspl.temporal.DwTemporalModelPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class LogicalPackageImpl extends EPackageImpl implements LogicalPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass logicalKeywordEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass binaryLogicalKeywordEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass keywordAndEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass keywordOrEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass keywordImpliesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unaryLogicalKeywordEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass keywordNotEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.darwinspl.fmec.keywords.logical.LogicalPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private LogicalPackageImpl() {
		super(eNS_URI, LogicalFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link LogicalPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static LogicalPackage init() {
		if (isInited) return (LogicalPackage)EPackage.Registry.INSTANCE.getEPackage(LogicalPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredLogicalPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		LogicalPackageImpl theLogicalPackage = registeredLogicalPackage instanceof LogicalPackageImpl ? (LogicalPackageImpl)registeredLogicalPackage : new LogicalPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		DwDatatypesPackage.eINSTANCE.eClass();
		DwFeaturePackage.eINSTANCE.eClass();
		DwTemporalModelPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(FmecPackage.eNS_URI);
		FmecPackageImpl theFmecPackage = (FmecPackageImpl)(registeredPackage instanceof FmecPackageImpl ? registeredPackage : FmecPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ConstraintsPackage.eNS_URI);
		ConstraintsPackageImpl theConstraintsPackage = (ConstraintsPackageImpl)(registeredPackage instanceof ConstraintsPackageImpl ? registeredPackage : ConstraintsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(PropertyPackage.eNS_URI);
		PropertyPackageImpl thePropertyPackage = (PropertyPackageImpl)(registeredPackage instanceof PropertyPackageImpl ? registeredPackage : PropertyPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimepointPackage.eNS_URI);
		TimepointPackageImpl theTimepointPackage = (TimepointPackageImpl)(registeredPackage instanceof TimepointPackageImpl ? registeredPackage : TimepointPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(KeywordsPackage.eNS_URI);
		KeywordsPackageImpl theKeywordsPackage = (KeywordsPackageImpl)(registeredPackage instanceof KeywordsPackageImpl ? registeredPackage : KeywordsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(BuildingblocksPackage.eNS_URI);
		BuildingblocksPackageImpl theBuildingblocksPackage = (BuildingblocksPackageImpl)(registeredPackage instanceof BuildingblocksPackageImpl ? registeredPackage : BuildingblocksPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(de.darwinspl.fmec.keywords.timepoint.TimepointPackage.eNS_URI);
		de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl theTimepointPackage_1 = (de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl)(registeredPackage instanceof de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl ? registeredPackage : de.darwinspl.fmec.keywords.timepoint.TimepointPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(IntervalPackage.eNS_URI);
		IntervalPackageImpl theIntervalPackage = (IntervalPackageImpl)(registeredPackage instanceof IntervalPackageImpl ? registeredPackage : IntervalPackage.eINSTANCE);

		// Create package meta-data objects
		theLogicalPackage.createPackageContents();
		theFmecPackage.createPackageContents();
		theConstraintsPackage.createPackageContents();
		thePropertyPackage.createPackageContents();
		theTimepointPackage.createPackageContents();
		theKeywordsPackage.createPackageContents();
		theBuildingblocksPackage.createPackageContents();
		theTimepointPackage_1.createPackageContents();
		theIntervalPackage.createPackageContents();

		// Initialize created meta-data
		theLogicalPackage.initializePackageContents();
		theFmecPackage.initializePackageContents();
		theConstraintsPackage.initializePackageContents();
		thePropertyPackage.initializePackageContents();
		theTimepointPackage.initializePackageContents();
		theKeywordsPackage.initializePackageContents();
		theBuildingblocksPackage.initializePackageContents();
		theTimepointPackage_1.initializePackageContents();
		theIntervalPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theLogicalPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(LogicalPackage.eNS_URI, theLogicalPackage);
		return theLogicalPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLogicalKeyword() {
		return logicalKeywordEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBinaryLogicalKeyword() {
		return binaryLogicalKeywordEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getKeywordAnd() {
		return keywordAndEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getKeywordOr() {
		return keywordOrEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getKeywordImplies() {
		return keywordImpliesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUnaryLogicalKeyword() {
		return unaryLogicalKeywordEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getKeywordNot() {
		return keywordNotEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LogicalFactory getLogicalFactory() {
		return (LogicalFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		logicalKeywordEClass = createEClass(LOGICAL_KEYWORD);

		binaryLogicalKeywordEClass = createEClass(BINARY_LOGICAL_KEYWORD);

		keywordAndEClass = createEClass(KEYWORD_AND);

		keywordOrEClass = createEClass(KEYWORD_OR);

		keywordImpliesEClass = createEClass(KEYWORD_IMPLIES);

		unaryLogicalKeywordEClass = createEClass(UNARY_LOGICAL_KEYWORD);

		keywordNotEClass = createEClass(KEYWORD_NOT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		KeywordsPackage theKeywordsPackage = (KeywordsPackage)EPackage.Registry.INSTANCE.getEPackage(KeywordsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		logicalKeywordEClass.getESuperTypes().add(theKeywordsPackage.getFeatureModelEvolutionConstraintKeyword());
		binaryLogicalKeywordEClass.getESuperTypes().add(this.getLogicalKeyword());
		keywordAndEClass.getESuperTypes().add(this.getBinaryLogicalKeyword());
		keywordOrEClass.getESuperTypes().add(this.getBinaryLogicalKeyword());
		keywordImpliesEClass.getESuperTypes().add(this.getBinaryLogicalKeyword());
		unaryLogicalKeywordEClass.getESuperTypes().add(this.getLogicalKeyword());
		keywordNotEClass.getESuperTypes().add(this.getUnaryLogicalKeyword());

		// Initialize classes, features, and operations; add parameters
		initEClass(logicalKeywordEClass, LogicalKeyword.class, "LogicalKeyword", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(binaryLogicalKeywordEClass, BinaryLogicalKeyword.class, "BinaryLogicalKeyword", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(keywordAndEClass, KeywordAnd.class, "KeywordAnd", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(keywordOrEClass, KeywordOr.class, "KeywordOr", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(keywordImpliesEClass, KeywordImplies.class, "KeywordImplies", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(unaryLogicalKeywordEClass, UnaryLogicalKeyword.class, "UnaryLogicalKeyword", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(keywordNotEClass, KeywordNot.class, "KeywordNot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
	}

} //LogicalPackageImpl
