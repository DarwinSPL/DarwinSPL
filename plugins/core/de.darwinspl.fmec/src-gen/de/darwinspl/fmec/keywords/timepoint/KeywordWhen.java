/**
 */
package de.darwinspl.fmec.keywords.timepoint;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Keyword When</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.keywords.timepoint.TimepointPackage#getKeywordWhen()
 * @model
 * @generated
 */
public interface KeywordWhen extends TimePointKeyword {
} // KeywordWhen
