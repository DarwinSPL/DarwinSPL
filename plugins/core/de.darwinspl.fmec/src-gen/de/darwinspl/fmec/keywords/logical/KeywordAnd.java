/**
 */
package de.darwinspl.fmec.keywords.logical;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Keyword And</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.keywords.logical.LogicalPackage#getKeywordAnd()
 * @model
 * @generated
 */
public interface KeywordAnd extends BinaryLogicalKeyword {
} // KeywordAnd
