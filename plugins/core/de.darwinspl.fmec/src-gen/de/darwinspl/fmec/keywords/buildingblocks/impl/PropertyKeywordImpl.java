/**
 */
package de.darwinspl.fmec.keywords.buildingblocks.impl;

import de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage;
import de.darwinspl.fmec.keywords.buildingblocks.PropertyKeyword;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Property Keyword</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class PropertyKeywordImpl extends MinimalEObjectImpl.Container implements PropertyKeyword {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PropertyKeywordImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BuildingblocksPackage.Literals.PROPERTY_KEYWORD;
	}

} //PropertyKeywordImpl
