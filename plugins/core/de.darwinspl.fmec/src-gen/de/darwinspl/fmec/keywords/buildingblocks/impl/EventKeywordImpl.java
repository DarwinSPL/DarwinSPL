/**
 */
package de.darwinspl.fmec.keywords.buildingblocks.impl;

import de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage;
import de.darwinspl.fmec.keywords.buildingblocks.EventKeyword;

import de.darwinspl.fmec.keywords.impl.FeatureModelEvolutionConstraintKeywordImpl;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event Keyword</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class EventKeywordImpl extends FeatureModelEvolutionConstraintKeywordImpl implements EventKeyword {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EventKeywordImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BuildingblocksPackage.Literals.EVENT_KEYWORD;
	}

} //EventKeywordImpl
