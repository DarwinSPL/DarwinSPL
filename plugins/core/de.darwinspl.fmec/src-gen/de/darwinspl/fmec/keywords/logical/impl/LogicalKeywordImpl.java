/**
 */
package de.darwinspl.fmec.keywords.logical.impl;

import de.darwinspl.fmec.keywords.impl.FeatureModelEvolutionConstraintKeywordImpl;

import de.darwinspl.fmec.keywords.logical.LogicalKeyword;
import de.darwinspl.fmec.keywords.logical.LogicalPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Keyword</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class LogicalKeywordImpl extends FeatureModelEvolutionConstraintKeywordImpl implements LogicalKeyword {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LogicalKeywordImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LogicalPackage.Literals.LOGICAL_KEYWORD;
	}

} //LogicalKeywordImpl
