/**
 */
package de.darwinspl.fmec.keywords.timepoint.impl;

import de.darwinspl.fmec.keywords.timepoint.KeywordBefore;
import de.darwinspl.fmec.keywords.timepoint.TimepointPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Keyword Before</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class KeywordBeforeImpl extends TimePointKeywordImpl implements KeywordBefore {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected KeywordBeforeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimepointPackage.Literals.KEYWORD_BEFORE;
	}

} //KeywordBeforeImpl
