/**
 */
package de.darwinspl.fmec.keywords.logical;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary Logical Keyword</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.keywords.logical.LogicalPackage#getBinaryLogicalKeyword()
 * @model abstract="true"
 * @generated
 */
public interface BinaryLogicalKeyword extends LogicalKeyword {
} // BinaryLogicalKeyword
