/**
 */
package de.darwinspl.fmec.keywords.timepoint.impl;

import de.darwinspl.fmec.keywords.timepoint.KeywordAfter;
import de.darwinspl.fmec.keywords.timepoint.TimepointPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Keyword After</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class KeywordAfterImpl extends TimePointKeywordImpl implements KeywordAfter {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected KeywordAfterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimepointPackage.Literals.KEYWORD_AFTER;
	}

} //KeywordAfterImpl
