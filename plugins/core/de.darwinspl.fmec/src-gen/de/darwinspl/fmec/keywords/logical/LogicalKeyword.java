/**
 */
package de.darwinspl.fmec.keywords.logical;

import de.darwinspl.fmec.keywords.FeatureModelEvolutionConstraintKeyword;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Keyword</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.keywords.logical.LogicalPackage#getLogicalKeyword()
 * @model abstract="true"
 * @generated
 */
public interface LogicalKeyword extends FeatureModelEvolutionConstraintKeyword {
} // LogicalKeyword
