/**
 */
package de.darwinspl.fmec.keywords.buildingblocks;

import de.darwinspl.fmec.keywords.KeywordsPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksFactory
 * @model kind="package"
 * @generated
 */
public interface BuildingblocksPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "buildingblocks";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.darwinspl.de/feature-model-evolution-constraints/keywords/buildingblocks/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "buildingblocks";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BuildingblocksPackage eINSTANCE = de.darwinspl.fmec.keywords.buildingblocks.impl.BuildingblocksPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.keywords.buildingblocks.impl.EventKeywordImpl <em>Event Keyword</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.keywords.buildingblocks.impl.EventKeywordImpl
	 * @see de.darwinspl.fmec.keywords.buildingblocks.impl.BuildingblocksPackageImpl#getEventKeyword()
	 * @generated
	 */
	int EVENT_KEYWORD = 0;

	/**
	 * The number of structural features of the '<em>Event Keyword</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_KEYWORD_FEATURE_COUNT = KeywordsPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_KEYWORD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Event Keyword</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_KEYWORD_OPERATION_COUNT = KeywordsPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_KEYWORD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.keywords.buildingblocks.impl.KeywordStartsImpl <em>Keyword Starts</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.keywords.buildingblocks.impl.KeywordStartsImpl
	 * @see de.darwinspl.fmec.keywords.buildingblocks.impl.BuildingblocksPackageImpl#getKeywordStarts()
	 * @generated
	 */
	int KEYWORD_STARTS = 1;

	/**
	 * The number of structural features of the '<em>Keyword Starts</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_STARTS_FEATURE_COUNT = EVENT_KEYWORD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Keyword Starts</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_STARTS_OPERATION_COUNT = EVENT_KEYWORD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.keywords.buildingblocks.impl.KeywordStopsImpl <em>Keyword Stops</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.keywords.buildingblocks.impl.KeywordStopsImpl
	 * @see de.darwinspl.fmec.keywords.buildingblocks.impl.BuildingblocksPackageImpl#getKeywordStops()
	 * @generated
	 */
	int KEYWORD_STOPS = 2;

	/**
	 * The number of structural features of the '<em>Keyword Stops</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_STOPS_FEATURE_COUNT = EVENT_KEYWORD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Keyword Stops</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_STOPS_OPERATION_COUNT = EVENT_KEYWORD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.keywords.buildingblocks.impl.PropertyKeywordImpl <em>Property Keyword</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.keywords.buildingblocks.impl.PropertyKeywordImpl
	 * @see de.darwinspl.fmec.keywords.buildingblocks.impl.BuildingblocksPackageImpl#getPropertyKeyword()
	 * @generated
	 */
	int PROPERTY_KEYWORD = 3;

	/**
	 * The number of structural features of the '<em>Property Keyword</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_KEYWORD_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Property Keyword</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_KEYWORD_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.keywords.buildingblocks.impl.KeywordValidImpl <em>Keyword Valid</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.keywords.buildingblocks.impl.KeywordValidImpl
	 * @see de.darwinspl.fmec.keywords.buildingblocks.impl.BuildingblocksPackageImpl#getKeywordValid()
	 * @generated
	 */
	int KEYWORD_VALID = 4;

	/**
	 * The number of structural features of the '<em>Keyword Valid</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_VALID_FEATURE_COUNT = PROPERTY_KEYWORD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Keyword Valid</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_VALID_OPERATION_COUNT = PROPERTY_KEYWORD_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.keywords.buildingblocks.EventKeyword <em>Event Keyword</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Keyword</em>'.
	 * @see de.darwinspl.fmec.keywords.buildingblocks.EventKeyword
	 * @generated
	 */
	EClass getEventKeyword();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.keywords.buildingblocks.KeywordStarts <em>Keyword Starts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Keyword Starts</em>'.
	 * @see de.darwinspl.fmec.keywords.buildingblocks.KeywordStarts
	 * @generated
	 */
	EClass getKeywordStarts();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.keywords.buildingblocks.KeywordStops <em>Keyword Stops</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Keyword Stops</em>'.
	 * @see de.darwinspl.fmec.keywords.buildingblocks.KeywordStops
	 * @generated
	 */
	EClass getKeywordStops();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.keywords.buildingblocks.PropertyKeyword <em>Property Keyword</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property Keyword</em>'.
	 * @see de.darwinspl.fmec.keywords.buildingblocks.PropertyKeyword
	 * @generated
	 */
	EClass getPropertyKeyword();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.keywords.buildingblocks.KeywordValid <em>Keyword Valid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Keyword Valid</em>'.
	 * @see de.darwinspl.fmec.keywords.buildingblocks.KeywordValid
	 * @generated
	 */
	EClass getKeywordValid();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	BuildingblocksFactory getBuildingblocksFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.keywords.buildingblocks.impl.EventKeywordImpl <em>Event Keyword</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.keywords.buildingblocks.impl.EventKeywordImpl
		 * @see de.darwinspl.fmec.keywords.buildingblocks.impl.BuildingblocksPackageImpl#getEventKeyword()
		 * @generated
		 */
		EClass EVENT_KEYWORD = eINSTANCE.getEventKeyword();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.keywords.buildingblocks.impl.KeywordStartsImpl <em>Keyword Starts</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.keywords.buildingblocks.impl.KeywordStartsImpl
		 * @see de.darwinspl.fmec.keywords.buildingblocks.impl.BuildingblocksPackageImpl#getKeywordStarts()
		 * @generated
		 */
		EClass KEYWORD_STARTS = eINSTANCE.getKeywordStarts();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.keywords.buildingblocks.impl.KeywordStopsImpl <em>Keyword Stops</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.keywords.buildingblocks.impl.KeywordStopsImpl
		 * @see de.darwinspl.fmec.keywords.buildingblocks.impl.BuildingblocksPackageImpl#getKeywordStops()
		 * @generated
		 */
		EClass KEYWORD_STOPS = eINSTANCE.getKeywordStops();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.keywords.buildingblocks.impl.PropertyKeywordImpl <em>Property Keyword</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.keywords.buildingblocks.impl.PropertyKeywordImpl
		 * @see de.darwinspl.fmec.keywords.buildingblocks.impl.BuildingblocksPackageImpl#getPropertyKeyword()
		 * @generated
		 */
		EClass PROPERTY_KEYWORD = eINSTANCE.getPropertyKeyword();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.keywords.buildingblocks.impl.KeywordValidImpl <em>Keyword Valid</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.keywords.buildingblocks.impl.KeywordValidImpl
		 * @see de.darwinspl.fmec.keywords.buildingblocks.impl.BuildingblocksPackageImpl#getKeywordValid()
		 * @generated
		 */
		EClass KEYWORD_VALID = eINSTANCE.getKeywordValid();

	}

} //BuildingblocksPackage
