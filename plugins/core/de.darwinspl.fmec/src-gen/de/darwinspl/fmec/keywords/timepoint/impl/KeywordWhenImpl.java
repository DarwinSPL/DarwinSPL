/**
 */
package de.darwinspl.fmec.keywords.timepoint.impl;

import de.darwinspl.fmec.keywords.timepoint.KeywordWhen;
import de.darwinspl.fmec.keywords.timepoint.TimepointPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Keyword When</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class KeywordWhenImpl extends TimePointKeywordImpl implements KeywordWhen {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected KeywordWhenImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimepointPackage.Literals.KEYWORD_WHEN;
	}

} //KeywordWhenImpl
