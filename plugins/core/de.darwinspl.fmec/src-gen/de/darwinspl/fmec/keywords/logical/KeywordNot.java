/**
 */
package de.darwinspl.fmec.keywords.logical;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Keyword Not</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.keywords.logical.LogicalPackage#getKeywordNot()
 * @model
 * @generated
 */
public interface KeywordNot extends UnaryLogicalKeyword {
} // KeywordNot
