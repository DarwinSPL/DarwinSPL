/**
 */
package de.darwinspl.fmec.keywords.logical;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.darwinspl.fmec.keywords.logical.LogicalPackage
 * @generated
 */
public interface LogicalFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LogicalFactory eINSTANCE = de.darwinspl.fmec.keywords.logical.impl.LogicalFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Keyword And</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Keyword And</em>'.
	 * @generated
	 */
	KeywordAnd createKeywordAnd();

	/**
	 * Returns a new object of class '<em>Keyword Or</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Keyword Or</em>'.
	 * @generated
	 */
	KeywordOr createKeywordOr();

	/**
	 * Returns a new object of class '<em>Keyword Implies</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Keyword Implies</em>'.
	 * @generated
	 */
	KeywordImplies createKeywordImplies();

	/**
	 * Returns a new object of class '<em>Keyword Not</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Keyword Not</em>'.
	 * @generated
	 */
	KeywordNot createKeywordNot();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	LogicalPackage getLogicalPackage();

} //LogicalFactory
