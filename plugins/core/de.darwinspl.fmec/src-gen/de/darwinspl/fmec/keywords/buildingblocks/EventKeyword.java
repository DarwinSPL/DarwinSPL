/**
 */
package de.darwinspl.fmec.keywords.buildingblocks;

import de.darwinspl.fmec.keywords.FeatureModelEvolutionConstraintKeyword;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Keyword</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage#getEventKeyword()
 * @model abstract="true"
 * @generated
 */
public interface EventKeyword extends FeatureModelEvolutionConstraintKeyword {
} // EventKeyword
