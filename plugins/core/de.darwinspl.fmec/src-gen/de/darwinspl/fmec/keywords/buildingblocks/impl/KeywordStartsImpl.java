/**
 */
package de.darwinspl.fmec.keywords.buildingblocks.impl;

import de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage;
import de.darwinspl.fmec.keywords.buildingblocks.KeywordStarts;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Keyword Starts</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class KeywordStartsImpl extends EventKeywordImpl implements KeywordStarts {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected KeywordStartsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BuildingblocksPackage.Literals.KEYWORD_STARTS;
	}

} //KeywordStartsImpl
