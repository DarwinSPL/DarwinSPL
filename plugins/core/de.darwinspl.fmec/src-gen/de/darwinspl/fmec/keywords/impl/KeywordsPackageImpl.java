/**
 */
package de.darwinspl.fmec.keywords.impl;

import de.darwinspl.datatypes.DwDatatypesPackage;

import de.darwinspl.feature.DwFeaturePackage;

import de.darwinspl.fmec.FmecPackage;

import de.darwinspl.fmec.constraints.ConstraintsPackage;

import de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl;

import de.darwinspl.fmec.impl.FmecPackageImpl;

import de.darwinspl.fmec.keywords.FeatureModelEvolutionConstraintKeyword;
import de.darwinspl.fmec.keywords.KeywordsFactory;
import de.darwinspl.fmec.keywords.KeywordsPackage;

import de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage;

import de.darwinspl.fmec.keywords.buildingblocks.impl.BuildingblocksPackageImpl;

import de.darwinspl.fmec.keywords.interval.IntervalPackage;

import de.darwinspl.fmec.keywords.interval.impl.IntervalPackageImpl;

import de.darwinspl.fmec.keywords.logical.LogicalPackage;

import de.darwinspl.fmec.keywords.logical.impl.LogicalPackageImpl;

import de.darwinspl.fmec.property.PropertyPackage;

import de.darwinspl.fmec.property.impl.PropertyPackageImpl;

import de.darwinspl.fmec.timepoint.TimepointPackage;

import de.darwinspl.fmec.timepoint.impl.TimepointPackageImpl;

import de.darwinspl.temporal.DwTemporalModelPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class KeywordsPackageImpl extends EPackageImpl implements KeywordsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureModelEvolutionConstraintKeywordEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.darwinspl.fmec.keywords.KeywordsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private KeywordsPackageImpl() {
		super(eNS_URI, KeywordsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link KeywordsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static KeywordsPackage init() {
		if (isInited) return (KeywordsPackage)EPackage.Registry.INSTANCE.getEPackage(KeywordsPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredKeywordsPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		KeywordsPackageImpl theKeywordsPackage = registeredKeywordsPackage instanceof KeywordsPackageImpl ? (KeywordsPackageImpl)registeredKeywordsPackage : new KeywordsPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		DwDatatypesPackage.eINSTANCE.eClass();
		DwFeaturePackage.eINSTANCE.eClass();
		DwTemporalModelPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(FmecPackage.eNS_URI);
		FmecPackageImpl theFmecPackage = (FmecPackageImpl)(registeredPackage instanceof FmecPackageImpl ? registeredPackage : FmecPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ConstraintsPackage.eNS_URI);
		ConstraintsPackageImpl theConstraintsPackage = (ConstraintsPackageImpl)(registeredPackage instanceof ConstraintsPackageImpl ? registeredPackage : ConstraintsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(PropertyPackage.eNS_URI);
		PropertyPackageImpl thePropertyPackage = (PropertyPackageImpl)(registeredPackage instanceof PropertyPackageImpl ? registeredPackage : PropertyPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimepointPackage.eNS_URI);
		TimepointPackageImpl theTimepointPackage = (TimepointPackageImpl)(registeredPackage instanceof TimepointPackageImpl ? registeredPackage : TimepointPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(LogicalPackage.eNS_URI);
		LogicalPackageImpl theLogicalPackage = (LogicalPackageImpl)(registeredPackage instanceof LogicalPackageImpl ? registeredPackage : LogicalPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(BuildingblocksPackage.eNS_URI);
		BuildingblocksPackageImpl theBuildingblocksPackage = (BuildingblocksPackageImpl)(registeredPackage instanceof BuildingblocksPackageImpl ? registeredPackage : BuildingblocksPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(de.darwinspl.fmec.keywords.timepoint.TimepointPackage.eNS_URI);
		de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl theTimepointPackage_1 = (de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl)(registeredPackage instanceof de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl ? registeredPackage : de.darwinspl.fmec.keywords.timepoint.TimepointPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(IntervalPackage.eNS_URI);
		IntervalPackageImpl theIntervalPackage = (IntervalPackageImpl)(registeredPackage instanceof IntervalPackageImpl ? registeredPackage : IntervalPackage.eINSTANCE);

		// Create package meta-data objects
		theKeywordsPackage.createPackageContents();
		theFmecPackage.createPackageContents();
		theConstraintsPackage.createPackageContents();
		thePropertyPackage.createPackageContents();
		theTimepointPackage.createPackageContents();
		theLogicalPackage.createPackageContents();
		theBuildingblocksPackage.createPackageContents();
		theTimepointPackage_1.createPackageContents();
		theIntervalPackage.createPackageContents();

		// Initialize created meta-data
		theKeywordsPackage.initializePackageContents();
		theFmecPackage.initializePackageContents();
		theConstraintsPackage.initializePackageContents();
		thePropertyPackage.initializePackageContents();
		theTimepointPackage.initializePackageContents();
		theLogicalPackage.initializePackageContents();
		theBuildingblocksPackage.initializePackageContents();
		theTimepointPackage_1.initializePackageContents();
		theIntervalPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theKeywordsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(KeywordsPackage.eNS_URI, theKeywordsPackage);
		return theKeywordsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFeatureModelEvolutionConstraintKeyword() {
		return featureModelEvolutionConstraintKeywordEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public KeywordsFactory getKeywordsFactory() {
		return (KeywordsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		featureModelEvolutionConstraintKeywordEClass = createEClass(FEATURE_MODEL_EVOLUTION_CONSTRAINT_KEYWORD);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		LogicalPackage theLogicalPackage = (LogicalPackage)EPackage.Registry.INSTANCE.getEPackage(LogicalPackage.eNS_URI);
		BuildingblocksPackage theBuildingblocksPackage = (BuildingblocksPackage)EPackage.Registry.INSTANCE.getEPackage(BuildingblocksPackage.eNS_URI);
		de.darwinspl.fmec.keywords.timepoint.TimepointPackage theTimepointPackage_1 = (de.darwinspl.fmec.keywords.timepoint.TimepointPackage)EPackage.Registry.INSTANCE.getEPackage(de.darwinspl.fmec.keywords.timepoint.TimepointPackage.eNS_URI);
		IntervalPackage theIntervalPackage = (IntervalPackage)EPackage.Registry.INSTANCE.getEPackage(IntervalPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theLogicalPackage);
		getESubpackages().add(theBuildingblocksPackage);
		getESubpackages().add(theTimepointPackage_1);
		getESubpackages().add(theIntervalPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(featureModelEvolutionConstraintKeywordEClass, FeatureModelEvolutionConstraintKeyword.class, "FeatureModelEvolutionConstraintKeyword", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
	}

} //KeywordsPackageImpl
