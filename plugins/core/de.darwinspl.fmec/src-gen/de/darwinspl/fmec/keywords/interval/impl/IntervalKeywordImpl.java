/**
 */
package de.darwinspl.fmec.keywords.interval.impl;

import de.darwinspl.fmec.keywords.impl.FeatureModelEvolutionConstraintKeywordImpl;

import de.darwinspl.fmec.keywords.interval.IntervalKeyword;
import de.darwinspl.fmec.keywords.interval.IntervalPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Keyword</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class IntervalKeywordImpl extends FeatureModelEvolutionConstraintKeywordImpl implements IntervalKeyword {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IntervalKeywordImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IntervalPackage.Literals.INTERVAL_KEYWORD;
	}

} //IntervalKeywordImpl
