/**
 */
package de.darwinspl.fmec.keywords.logical.impl;

import de.darwinspl.fmec.keywords.logical.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class LogicalFactoryImpl extends EFactoryImpl implements LogicalFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static LogicalFactory init() {
		try {
			LogicalFactory theLogicalFactory = (LogicalFactory)EPackage.Registry.INSTANCE.getEFactory(LogicalPackage.eNS_URI);
			if (theLogicalFactory != null) {
				return theLogicalFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new LogicalFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogicalFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case LogicalPackage.KEYWORD_AND: return createKeywordAnd();
			case LogicalPackage.KEYWORD_OR: return createKeywordOr();
			case LogicalPackage.KEYWORD_IMPLIES: return createKeywordImplies();
			case LogicalPackage.KEYWORD_NOT: return createKeywordNot();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public KeywordAnd createKeywordAnd() {
		KeywordAndImpl keywordAnd = new KeywordAndImpl();
		return keywordAnd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public KeywordOr createKeywordOr() {
		KeywordOrImpl keywordOr = new KeywordOrImpl();
		return keywordOr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public KeywordImplies createKeywordImplies() {
		KeywordImpliesImpl keywordImplies = new KeywordImpliesImpl();
		return keywordImplies;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public KeywordNot createKeywordNot() {
		KeywordNotImpl keywordNot = new KeywordNotImpl();
		return keywordNot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LogicalPackage getLogicalPackage() {
		return (LogicalPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static LogicalPackage getPackage() {
		return LogicalPackage.eINSTANCE;
	}

} //LogicalFactoryImpl
