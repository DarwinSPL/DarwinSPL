/**
 */
package de.darwinspl.fmec.keywords.interval;

import de.darwinspl.fmec.keywords.KeywordsPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.darwinspl.fmec.keywords.interval.IntervalFactory
 * @model kind="package"
 * @generated
 */
public interface IntervalPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "interval";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.darwinspl.de/feature-model-evolution-constraints/keywords/interval/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "interval";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	IntervalPackage eINSTANCE = de.darwinspl.fmec.keywords.interval.impl.IntervalPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.keywords.interval.impl.IntervalKeywordImpl <em>Keyword</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.keywords.interval.impl.IntervalKeywordImpl
	 * @see de.darwinspl.fmec.keywords.interval.impl.IntervalPackageImpl#getIntervalKeyword()
	 * @generated
	 */
	int INTERVAL_KEYWORD = 0;

	/**
	 * The number of structural features of the '<em>Keyword</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERVAL_KEYWORD_FEATURE_COUNT = KeywordsPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_KEYWORD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Keyword</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERVAL_KEYWORD_OPERATION_COUNT = KeywordsPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_KEYWORD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.keywords.interval.impl.KeywordDuringImpl <em>Keyword During</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.keywords.interval.impl.KeywordDuringImpl
	 * @see de.darwinspl.fmec.keywords.interval.impl.IntervalPackageImpl#getKeywordDuring()
	 * @generated
	 */
	int KEYWORD_DURING = 1;

	/**
	 * The number of structural features of the '<em>Keyword During</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_DURING_FEATURE_COUNT = INTERVAL_KEYWORD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Keyword During</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYWORD_DURING_OPERATION_COUNT = INTERVAL_KEYWORD_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.keywords.interval.IntervalKeyword <em>Keyword</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Keyword</em>'.
	 * @see de.darwinspl.fmec.keywords.interval.IntervalKeyword
	 * @generated
	 */
	EClass getIntervalKeyword();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.keywords.interval.KeywordDuring <em>Keyword During</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Keyword During</em>'.
	 * @see de.darwinspl.fmec.keywords.interval.KeywordDuring
	 * @generated
	 */
	EClass getKeywordDuring();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	IntervalFactory getIntervalFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.keywords.interval.impl.IntervalKeywordImpl <em>Keyword</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.keywords.interval.impl.IntervalKeywordImpl
		 * @see de.darwinspl.fmec.keywords.interval.impl.IntervalPackageImpl#getIntervalKeyword()
		 * @generated
		 */
		EClass INTERVAL_KEYWORD = eINSTANCE.getIntervalKeyword();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.keywords.interval.impl.KeywordDuringImpl <em>Keyword During</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.keywords.interval.impl.KeywordDuringImpl
		 * @see de.darwinspl.fmec.keywords.interval.impl.IntervalPackageImpl#getKeywordDuring()
		 * @generated
		 */
		EClass KEYWORD_DURING = eINSTANCE.getKeywordDuring();

	}

} //IntervalPackage
