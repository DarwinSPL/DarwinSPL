/**
 */
package de.darwinspl.fmec.keywords.logical.impl;

import de.darwinspl.fmec.keywords.logical.KeywordOr;
import de.darwinspl.fmec.keywords.logical.LogicalPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Keyword Or</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class KeywordOrImpl extends BinaryLogicalKeywordImpl implements KeywordOr {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected KeywordOrImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LogicalPackage.Literals.KEYWORD_OR;
	}

} //KeywordOrImpl
