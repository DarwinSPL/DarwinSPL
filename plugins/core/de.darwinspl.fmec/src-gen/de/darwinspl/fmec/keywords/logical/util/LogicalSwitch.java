/**
 */
package de.darwinspl.fmec.keywords.logical.util;

import de.darwinspl.fmec.keywords.FeatureModelEvolutionConstraintKeyword;

import de.darwinspl.fmec.keywords.logical.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see de.darwinspl.fmec.keywords.logical.LogicalPackage
 * @generated
 */
public class LogicalSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static LogicalPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogicalSwitch() {
		if (modelPackage == null) {
			modelPackage = LogicalPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case LogicalPackage.LOGICAL_KEYWORD: {
				LogicalKeyword logicalKeyword = (LogicalKeyword)theEObject;
				T result = caseLogicalKeyword(logicalKeyword);
				if (result == null) result = caseFeatureModelEvolutionConstraintKeyword(logicalKeyword);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LogicalPackage.BINARY_LOGICAL_KEYWORD: {
				BinaryLogicalKeyword binaryLogicalKeyword = (BinaryLogicalKeyword)theEObject;
				T result = caseBinaryLogicalKeyword(binaryLogicalKeyword);
				if (result == null) result = caseLogicalKeyword(binaryLogicalKeyword);
				if (result == null) result = caseFeatureModelEvolutionConstraintKeyword(binaryLogicalKeyword);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LogicalPackage.KEYWORD_AND: {
				KeywordAnd keywordAnd = (KeywordAnd)theEObject;
				T result = caseKeywordAnd(keywordAnd);
				if (result == null) result = caseBinaryLogicalKeyword(keywordAnd);
				if (result == null) result = caseLogicalKeyword(keywordAnd);
				if (result == null) result = caseFeatureModelEvolutionConstraintKeyword(keywordAnd);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LogicalPackage.KEYWORD_OR: {
				KeywordOr keywordOr = (KeywordOr)theEObject;
				T result = caseKeywordOr(keywordOr);
				if (result == null) result = caseBinaryLogicalKeyword(keywordOr);
				if (result == null) result = caseLogicalKeyword(keywordOr);
				if (result == null) result = caseFeatureModelEvolutionConstraintKeyword(keywordOr);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LogicalPackage.KEYWORD_IMPLIES: {
				KeywordImplies keywordImplies = (KeywordImplies)theEObject;
				T result = caseKeywordImplies(keywordImplies);
				if (result == null) result = caseBinaryLogicalKeyword(keywordImplies);
				if (result == null) result = caseLogicalKeyword(keywordImplies);
				if (result == null) result = caseFeatureModelEvolutionConstraintKeyword(keywordImplies);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LogicalPackage.UNARY_LOGICAL_KEYWORD: {
				UnaryLogicalKeyword unaryLogicalKeyword = (UnaryLogicalKeyword)theEObject;
				T result = caseUnaryLogicalKeyword(unaryLogicalKeyword);
				if (result == null) result = caseLogicalKeyword(unaryLogicalKeyword);
				if (result == null) result = caseFeatureModelEvolutionConstraintKeyword(unaryLogicalKeyword);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LogicalPackage.KEYWORD_NOT: {
				KeywordNot keywordNot = (KeywordNot)theEObject;
				T result = caseKeywordNot(keywordNot);
				if (result == null) result = caseUnaryLogicalKeyword(keywordNot);
				if (result == null) result = caseLogicalKeyword(keywordNot);
				if (result == null) result = caseFeatureModelEvolutionConstraintKeyword(keywordNot);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Keyword</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Keyword</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLogicalKeyword(LogicalKeyword object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binary Logical Keyword</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary Logical Keyword</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBinaryLogicalKeyword(BinaryLogicalKeyword object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Keyword And</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Keyword And</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseKeywordAnd(KeywordAnd object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Keyword Or</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Keyword Or</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseKeywordOr(KeywordOr object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Keyword Implies</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Keyword Implies</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseKeywordImplies(KeywordImplies object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unary Logical Keyword</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unary Logical Keyword</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnaryLogicalKeyword(UnaryLogicalKeyword object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Keyword Not</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Keyword Not</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseKeywordNot(KeywordNot object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature Model Evolution Constraint Keyword</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature Model Evolution Constraint Keyword</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeatureModelEvolutionConstraintKeyword(FeatureModelEvolutionConstraintKeyword object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //LogicalSwitch
