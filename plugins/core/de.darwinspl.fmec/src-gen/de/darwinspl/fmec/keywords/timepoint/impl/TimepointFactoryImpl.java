/**
 */
package de.darwinspl.fmec.keywords.timepoint.impl;

import de.darwinspl.fmec.keywords.timepoint.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TimepointFactoryImpl extends EFactoryImpl implements TimepointFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TimepointFactory init() {
		try {
			TimepointFactory theTimepointFactory = (TimepointFactory)EPackage.Registry.INSTANCE.getEFactory(TimepointPackage.eNS_URI);
			if (theTimepointFactory != null) {
				return theTimepointFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TimepointFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimepointFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TimepointPackage.KEYWORD_BEFORE: return createKeywordBefore();
			case TimepointPackage.KEYWORD_UNTIL: return createKeywordUntil();
			case TimepointPackage.KEYWORD_AFTER: return createKeywordAfter();
			case TimepointPackage.KEYWORD_WHEN: return createKeywordWhen();
			case TimepointPackage.KEYWORD_AT: return createKeywordAt();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public KeywordBefore createKeywordBefore() {
		KeywordBeforeImpl keywordBefore = new KeywordBeforeImpl();
		return keywordBefore;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public KeywordUntil createKeywordUntil() {
		KeywordUntilImpl keywordUntil = new KeywordUntilImpl();
		return keywordUntil;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public KeywordAfter createKeywordAfter() {
		KeywordAfterImpl keywordAfter = new KeywordAfterImpl();
		return keywordAfter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public KeywordWhen createKeywordWhen() {
		KeywordWhenImpl keywordWhen = new KeywordWhenImpl();
		return keywordWhen;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public KeywordAt createKeywordAt() {
		KeywordAtImpl keywordAt = new KeywordAtImpl();
		return keywordAt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TimepointPackage getTimepointPackage() {
		return (TimepointPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TimepointPackage getPackage() {
		return TimepointPackage.eINSTANCE;
	}

} //TimepointFactoryImpl
