/**
 */
package de.darwinspl.fmec.keywords.interval;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Keyword During</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.keywords.interval.IntervalPackage#getKeywordDuring()
 * @model
 * @generated
 */
public interface KeywordDuring extends IntervalKeyword {
} // KeywordDuring
