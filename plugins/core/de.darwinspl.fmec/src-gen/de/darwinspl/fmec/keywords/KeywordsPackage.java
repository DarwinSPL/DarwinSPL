/**
 */
package de.darwinspl.fmec.keywords;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.darwinspl.fmec.keywords.KeywordsFactory
 * @model kind="package"
 * @generated
 */
public interface KeywordsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "keywords";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.darwinspl.de/feature-model-evolution-constraints/keywords/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "keywords";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	KeywordsPackage eINSTANCE = de.darwinspl.fmec.keywords.impl.KeywordsPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.keywords.impl.FeatureModelEvolutionConstraintKeywordImpl <em>Feature Model Evolution Constraint Keyword</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.keywords.impl.FeatureModelEvolutionConstraintKeywordImpl
	 * @see de.darwinspl.fmec.keywords.impl.KeywordsPackageImpl#getFeatureModelEvolutionConstraintKeyword()
	 * @generated
	 */
	int FEATURE_MODEL_EVOLUTION_CONSTRAINT_KEYWORD = 0;

	/**
	 * The number of structural features of the '<em>Feature Model Evolution Constraint Keyword</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_EVOLUTION_CONSTRAINT_KEYWORD_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Feature Model Evolution Constraint Keyword</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_EVOLUTION_CONSTRAINT_KEYWORD_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.keywords.FeatureModelEvolutionConstraintKeyword <em>Feature Model Evolution Constraint Keyword</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Model Evolution Constraint Keyword</em>'.
	 * @see de.darwinspl.fmec.keywords.FeatureModelEvolutionConstraintKeyword
	 * @generated
	 */
	EClass getFeatureModelEvolutionConstraintKeyword();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	KeywordsFactory getKeywordsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.keywords.impl.FeatureModelEvolutionConstraintKeywordImpl <em>Feature Model Evolution Constraint Keyword</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.keywords.impl.FeatureModelEvolutionConstraintKeywordImpl
		 * @see de.darwinspl.fmec.keywords.impl.KeywordsPackageImpl#getFeatureModelEvolutionConstraintKeyword()
		 * @generated
		 */
		EClass FEATURE_MODEL_EVOLUTION_CONSTRAINT_KEYWORD = eINSTANCE.getFeatureModelEvolutionConstraintKeyword();

	}

} //KeywordsPackage
