/**
 */
package de.darwinspl.fmec.keywords.logical.impl;

import de.darwinspl.fmec.keywords.logical.KeywordNot;
import de.darwinspl.fmec.keywords.logical.LogicalPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Keyword Not</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class KeywordNotImpl extends UnaryLogicalKeywordImpl implements KeywordNot {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected KeywordNotImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LogicalPackage.Literals.KEYWORD_NOT;
	}

} //KeywordNotImpl
