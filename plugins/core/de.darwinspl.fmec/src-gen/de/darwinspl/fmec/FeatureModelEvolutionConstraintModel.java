/**
 */
package de.darwinspl.fmec;

import de.darwinspl.feature.DwTemporalFeatureModel;

import de.darwinspl.fmec.constraints.FeatureModelEvolutionConstraint;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Model Evolution Constraint Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.FeatureModelEvolutionConstraintModel#getFeatureModel <em>Feature Model</em>}</li>
 *   <li>{@link de.darwinspl.fmec.FeatureModelEvolutionConstraintModel#getConstraints <em>Constraints</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.fmec.FmecPackage#getFeatureModelEvolutionConstraintModel()
 * @model
 * @generated
 */
public interface FeatureModelEvolutionConstraintModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Model</em>' reference.
	 * @see #setFeatureModel(DwTemporalFeatureModel)
	 * @see de.darwinspl.fmec.FmecPackage#getFeatureModelEvolutionConstraintModel_FeatureModel()
	 * @model required="true"
	 * @generated
	 */
	DwTemporalFeatureModel getFeatureModel();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.FeatureModelEvolutionConstraintModel#getFeatureModel <em>Feature Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature Model</em>' reference.
	 * @see #getFeatureModel()
	 * @generated
	 */
	void setFeatureModel(DwTemporalFeatureModel value);

	/**
	 * Returns the value of the '<em><b>Constraints</b></em>' containment reference list.
	 * The list contents are of type {@link de.darwinspl.fmec.constraints.FeatureModelEvolutionConstraint}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraints</em>' containment reference list.
	 * @see de.darwinspl.fmec.FmecPackage#getFeatureModelEvolutionConstraintModel_Constraints()
	 * @model containment="true"
	 * @generated
	 */
	EList<FeatureModelEvolutionConstraint> getConstraints();

} // FeatureModelEvolutionConstraintModel
