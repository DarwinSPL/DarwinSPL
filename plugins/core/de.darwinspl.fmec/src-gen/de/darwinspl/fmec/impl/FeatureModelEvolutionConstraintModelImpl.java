/**
 */
package de.darwinspl.fmec.impl;

import de.darwinspl.feature.DwTemporalFeatureModel;

import de.darwinspl.fmec.FeatureModelEvolutionConstraintModel;
import de.darwinspl.fmec.FmecPackage;

import de.darwinspl.fmec.constraints.FeatureModelEvolutionConstraint;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature Model Evolution Constraint Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.impl.FeatureModelEvolutionConstraintModelImpl#getFeatureModel <em>Feature Model</em>}</li>
 *   <li>{@link de.darwinspl.fmec.impl.FeatureModelEvolutionConstraintModelImpl#getConstraints <em>Constraints</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FeatureModelEvolutionConstraintModelImpl extends MinimalEObjectImpl.Container implements FeatureModelEvolutionConstraintModel {
	/**
	 * The cached value of the '{@link #getFeatureModel() <em>Feature Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatureModel()
	 * @generated
	 * @ordered
	 */
	protected DwTemporalFeatureModel featureModel;

	/**
	 * The cached value of the '{@link #getConstraints() <em>Constraints</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraints()
	 * @generated
	 * @ordered
	 */
	protected EList<FeatureModelEvolutionConstraint> constraints;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureModelEvolutionConstraintModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FmecPackage.Literals.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwTemporalFeatureModel getFeatureModel() {
		if (featureModel != null && featureModel.eIsProxy()) {
			InternalEObject oldFeatureModel = (InternalEObject)featureModel;
			featureModel = (DwTemporalFeatureModel)eResolveProxy(oldFeatureModel);
			if (featureModel != oldFeatureModel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__FEATURE_MODEL, oldFeatureModel, featureModel));
			}
		}
		return featureModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwTemporalFeatureModel basicGetFeatureModel() {
		return featureModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFeatureModel(DwTemporalFeatureModel newFeatureModel) {
		DwTemporalFeatureModel oldFeatureModel = featureModel;
		featureModel = newFeatureModel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__FEATURE_MODEL, oldFeatureModel, featureModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<FeatureModelEvolutionConstraint> getConstraints() {
		if (constraints == null) {
			constraints = new EObjectContainmentEList<FeatureModelEvolutionConstraint>(FeatureModelEvolutionConstraint.class, this, FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__CONSTRAINTS);
		}
		return constraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__CONSTRAINTS:
				return ((InternalEList<?>)getConstraints()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__FEATURE_MODEL:
				if (resolve) return getFeatureModel();
				return basicGetFeatureModel();
			case FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__CONSTRAINTS:
				return getConstraints();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__FEATURE_MODEL:
				setFeatureModel((DwTemporalFeatureModel)newValue);
				return;
			case FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__CONSTRAINTS:
				getConstraints().clear();
				getConstraints().addAll((Collection<? extends FeatureModelEvolutionConstraint>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__FEATURE_MODEL:
				setFeatureModel((DwTemporalFeatureModel)null);
				return;
			case FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__CONSTRAINTS:
				getConstraints().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__FEATURE_MODEL:
				return featureModel != null;
			case FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__CONSTRAINTS:
				return constraints != null && !constraints.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //FeatureModelEvolutionConstraintModelImpl
