/**
 */
package de.darwinspl.fmec.impl;

import de.darwinspl.fmec.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FmecFactoryImpl extends EFactoryImpl implements FmecFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static FmecFactory init() {
		try {
			FmecFactory theFmecFactory = (FmecFactory)EPackage.Registry.INSTANCE.getEFactory(FmecPackage.eNS_URI);
			if (theFmecFactory != null) {
				return theFmecFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new FmecFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FmecFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL: return createFeatureModelEvolutionConstraintModel();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureModelEvolutionConstraintModel createFeatureModelEvolutionConstraintModel() {
		FeatureModelEvolutionConstraintModelImpl featureModelEvolutionConstraintModel = new FeatureModelEvolutionConstraintModelImpl();
		return featureModelEvolutionConstraintModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FmecPackage getFmecPackage() {
		return (FmecPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static FmecPackage getPackage() {
		return FmecPackage.eINSTANCE;
	}

} //FmecFactoryImpl
