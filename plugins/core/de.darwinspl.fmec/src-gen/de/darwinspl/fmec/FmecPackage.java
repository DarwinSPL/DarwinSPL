/**
 */
package de.darwinspl.fmec;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.darwinspl.fmec.FmecFactory
 * @model kind="package"
 * @generated
 */
public interface FmecPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "fmec";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.darwinspl.de/feature-model-evolution-constraints/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "fmec";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FmecPackage eINSTANCE = de.darwinspl.fmec.impl.FmecPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.impl.FeatureModelEvolutionConstraintModelImpl <em>Feature Model Evolution Constraint Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.impl.FeatureModelEvolutionConstraintModelImpl
	 * @see de.darwinspl.fmec.impl.FmecPackageImpl#getFeatureModelEvolutionConstraintModel()
	 * @generated
	 */
	int FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__FEATURE_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__CONSTRAINTS = 1;

	/**
	 * The number of structural features of the '<em>Feature Model Evolution Constraint Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Feature Model Evolution Constraint Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.FeatureModelEvolutionConstraintModel <em>Feature Model Evolution Constraint Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Model Evolution Constraint Model</em>'.
	 * @see de.darwinspl.fmec.FeatureModelEvolutionConstraintModel
	 * @generated
	 */
	EClass getFeatureModelEvolutionConstraintModel();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.fmec.FeatureModelEvolutionConstraintModel#getFeatureModel <em>Feature Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Feature Model</em>'.
	 * @see de.darwinspl.fmec.FeatureModelEvolutionConstraintModel#getFeatureModel()
	 * @see #getFeatureModelEvolutionConstraintModel()
	 * @generated
	 */
	EReference getFeatureModelEvolutionConstraintModel_FeatureModel();

	/**
	 * Returns the meta object for the containment reference list '{@link de.darwinspl.fmec.FeatureModelEvolutionConstraintModel#getConstraints <em>Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constraints</em>'.
	 * @see de.darwinspl.fmec.FeatureModelEvolutionConstraintModel#getConstraints()
	 * @see #getFeatureModelEvolutionConstraintModel()
	 * @generated
	 */
	EReference getFeatureModelEvolutionConstraintModel_Constraints();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	FmecFactory getFmecFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.impl.FeatureModelEvolutionConstraintModelImpl <em>Feature Model Evolution Constraint Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.impl.FeatureModelEvolutionConstraintModelImpl
		 * @see de.darwinspl.fmec.impl.FmecPackageImpl#getFeatureModelEvolutionConstraintModel()
		 * @generated
		 */
		EClass FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL = eINSTANCE.getFeatureModelEvolutionConstraintModel();

		/**
		 * The meta object literal for the '<em><b>Feature Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__FEATURE_MODEL = eINSTANCE.getFeatureModelEvolutionConstraintModel_FeatureModel();

		/**
		 * The meta object literal for the '<em><b>Constraints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__CONSTRAINTS = eINSTANCE.getFeatureModelEvolutionConstraintModel_Constraints();

	}

} //FmecPackage
