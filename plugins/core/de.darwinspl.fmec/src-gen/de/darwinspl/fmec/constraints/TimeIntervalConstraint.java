/**
 */
package de.darwinspl.fmec.constraints;

import de.darwinspl.fmec.keywords.interval.IntervalKeyword;

import de.darwinspl.fmec.timepoint.TimePoint;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Time Interval Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.constraints.TimeIntervalConstraint#getPreArgument <em>Pre Argument</em>}</li>
 *   <li>{@link de.darwinspl.fmec.constraints.TimeIntervalConstraint#getKeyword <em>Keyword</em>}</li>
 *   <li>{@link de.darwinspl.fmec.constraints.TimeIntervalConstraint#getStartPoint <em>Start Point</em>}</li>
 *   <li>{@link de.darwinspl.fmec.constraints.TimeIntervalConstraint#getEndPoint <em>End Point</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.fmec.constraints.ConstraintsPackage#getTimeIntervalConstraint()
 * @model
 * @generated
 */
public interface TimeIntervalConstraint extends AtomicConstraint {
	/**
	 * Returns the value of the '<em><b>Pre Argument</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pre Argument</em>' containment reference.
	 * @see #setPreArgument(EvolutionaryAbstractBuidingBlock)
	 * @see de.darwinspl.fmec.constraints.ConstraintsPackage#getTimeIntervalConstraint_PreArgument()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EvolutionaryAbstractBuidingBlock getPreArgument();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.constraints.TimeIntervalConstraint#getPreArgument <em>Pre Argument</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pre Argument</em>' containment reference.
	 * @see #getPreArgument()
	 * @generated
	 */
	void setPreArgument(EvolutionaryAbstractBuidingBlock value);

	/**
	 * Returns the value of the '<em><b>Keyword</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Keyword</em>' containment reference.
	 * @see #setKeyword(IntervalKeyword)
	 * @see de.darwinspl.fmec.constraints.ConstraintsPackage#getTimeIntervalConstraint_Keyword()
	 * @model containment="true" required="true"
	 * @generated
	 */
	IntervalKeyword getKeyword();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.constraints.TimeIntervalConstraint#getKeyword <em>Keyword</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Keyword</em>' containment reference.
	 * @see #getKeyword()
	 * @generated
	 */
	void setKeyword(IntervalKeyword value);

	/**
	 * Returns the value of the '<em><b>Start Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Point</em>' containment reference.
	 * @see #setStartPoint(TimePoint)
	 * @see de.darwinspl.fmec.constraints.ConstraintsPackage#getTimeIntervalConstraint_StartPoint()
	 * @model containment="true" required="true"
	 * @generated
	 */
	TimePoint getStartPoint();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.constraints.TimeIntervalConstraint#getStartPoint <em>Start Point</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Point</em>' containment reference.
	 * @see #getStartPoint()
	 * @generated
	 */
	void setStartPoint(TimePoint value);

	/**
	 * Returns the value of the '<em><b>End Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Point</em>' containment reference.
	 * @see #setEndPoint(TimePoint)
	 * @see de.darwinspl.fmec.constraints.ConstraintsPackage#getTimeIntervalConstraint_EndPoint()
	 * @model containment="true" required="true"
	 * @generated
	 */
	TimePoint getEndPoint();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.constraints.TimeIntervalConstraint#getEndPoint <em>End Point</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Point</em>' containment reference.
	 * @see #getEndPoint()
	 * @generated
	 */
	void setEndPoint(TimePoint value);

} // TimeIntervalConstraint
