/**
 */
package de.darwinspl.fmec.constraints.util;

import de.darwinspl.fmec.constraints.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see de.darwinspl.fmec.constraints.ConstraintsPackage
 * @generated
 */
public class ConstraintsSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ConstraintsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstraintsSwitch() {
		if (modelPackage == null) {
			modelPackage = ConstraintsPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ConstraintsPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT: {
				FeatureModelEvolutionConstraint featureModelEvolutionConstraint = (FeatureModelEvolutionConstraint)theEObject;
				T result = caseFeatureModelEvolutionConstraint(featureModelEvolutionConstraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConstraintsPackage.BINARY_CONSTRAINT: {
				BinaryConstraint binaryConstraint = (BinaryConstraint)theEObject;
				T result = caseBinaryConstraint(binaryConstraint);
				if (result == null) result = caseFeatureModelEvolutionConstraint(binaryConstraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConstraintsPackage.BINARY_CONSTRAINT_CHILD: {
				BinaryConstraintChild binaryConstraintChild = (BinaryConstraintChild)theEObject;
				T result = caseBinaryConstraintChild(binaryConstraintChild);
				if (result == null) result = caseFeatureModelEvolutionConstraint(binaryConstraintChild);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConstraintsPackage.UNARY_CONSTRAINT: {
				UnaryConstraint unaryConstraint = (UnaryConstraint)theEObject;
				T result = caseUnaryConstraint(unaryConstraint);
				if (result == null) result = caseBinaryConstraintChild(unaryConstraint);
				if (result == null) result = caseFeatureModelEvolutionConstraint(unaryConstraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConstraintsPackage.UNARY_CONSTRAINT_CHILD: {
				UnaryConstraintChild unaryConstraintChild = (UnaryConstraintChild)theEObject;
				T result = caseUnaryConstraintChild(unaryConstraintChild);
				if (result == null) result = caseBinaryConstraintChild(unaryConstraintChild);
				if (result == null) result = caseFeatureModelEvolutionConstraint(unaryConstraintChild);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConstraintsPackage.ATOMIC_CONSTRAINT: {
				AtomicConstraint atomicConstraint = (AtomicConstraint)theEObject;
				T result = caseAtomicConstraint(atomicConstraint);
				if (result == null) result = caseUnaryConstraintChild(atomicConstraint);
				if (result == null) result = caseBinaryConstraintChild(atomicConstraint);
				if (result == null) result = caseFeatureModelEvolutionConstraint(atomicConstraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConstraintsPackage.NESTED_CONSTRAINT: {
				NestedConstraint nestedConstraint = (NestedConstraint)theEObject;
				T result = caseNestedConstraint(nestedConstraint);
				if (result == null) result = caseUnaryConstraintChild(nestedConstraint);
				if (result == null) result = caseBinaryConstraintChild(nestedConstraint);
				if (result == null) result = caseFeatureModelEvolutionConstraint(nestedConstraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConstraintsPackage.TIME_POINT_CONSTRAINT: {
				TimePointConstraint timePointConstraint = (TimePointConstraint)theEObject;
				T result = caseTimePointConstraint(timePointConstraint);
				if (result == null) result = caseAtomicConstraint(timePointConstraint);
				if (result == null) result = caseUnaryConstraintChild(timePointConstraint);
				if (result == null) result = caseBinaryConstraintChild(timePointConstraint);
				if (result == null) result = caseFeatureModelEvolutionConstraint(timePointConstraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConstraintsPackage.TIME_INTERVAL_CONSTRAINT: {
				TimeIntervalConstraint timeIntervalConstraint = (TimeIntervalConstraint)theEObject;
				T result = caseTimeIntervalConstraint(timeIntervalConstraint);
				if (result == null) result = caseAtomicConstraint(timeIntervalConstraint);
				if (result == null) result = caseUnaryConstraintChild(timeIntervalConstraint);
				if (result == null) result = caseBinaryConstraintChild(timeIntervalConstraint);
				if (result == null) result = caseFeatureModelEvolutionConstraint(timeIntervalConstraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConstraintsPackage.EVOLUTIONARY_ABSTRACT_BUIDING_BLOCK: {
				EvolutionaryAbstractBuidingBlock evolutionaryAbstractBuidingBlock = (EvolutionaryAbstractBuidingBlock)theEObject;
				T result = caseEvolutionaryAbstractBuidingBlock(evolutionaryAbstractBuidingBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature Model Evolution Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature Model Evolution Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeatureModelEvolutionConstraint(FeatureModelEvolutionConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binary Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBinaryConstraint(BinaryConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binary Constraint Child</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary Constraint Child</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBinaryConstraintChild(BinaryConstraintChild object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unary Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unary Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnaryConstraint(UnaryConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unary Constraint Child</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unary Constraint Child</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnaryConstraintChild(UnaryConstraintChild object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Atomic Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Atomic Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAtomicConstraint(AtomicConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Nested Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Nested Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNestedConstraint(NestedConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Time Point Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Time Point Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTimePointConstraint(TimePointConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Time Interval Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Time Interval Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTimeIntervalConstraint(TimeIntervalConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Evolutionary Abstract Buiding Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Evolutionary Abstract Buiding Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEvolutionaryAbstractBuidingBlock(EvolutionaryAbstractBuidingBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ConstraintsSwitch
