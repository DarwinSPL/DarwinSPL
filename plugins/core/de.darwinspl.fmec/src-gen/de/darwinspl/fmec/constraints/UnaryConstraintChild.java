/**
 */
package de.darwinspl.fmec.constraints;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unary Constraint Child</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.constraints.ConstraintsPackage#getUnaryConstraintChild()
 * @model abstract="true"
 * @generated
 */
public interface UnaryConstraintChild extends BinaryConstraintChild {
} // UnaryConstraintChild
