/**
 */
package de.darwinspl.fmec.constraints;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Nested Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.constraints.NestedConstraint#getNestedConstraint <em>Nested Constraint</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.fmec.constraints.ConstraintsPackage#getNestedConstraint()
 * @model
 * @generated
 */
public interface NestedConstraint extends UnaryConstraintChild {
	/**
	 * Returns the value of the '<em><b>Nested Constraint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nested Constraint</em>' containment reference.
	 * @see #setNestedConstraint(FeatureModelEvolutionConstraint)
	 * @see de.darwinspl.fmec.constraints.ConstraintsPackage#getNestedConstraint_NestedConstraint()
	 * @model containment="true" required="true"
	 * @generated
	 */
	FeatureModelEvolutionConstraint getNestedConstraint();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.constraints.NestedConstraint#getNestedConstraint <em>Nested Constraint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nested Constraint</em>' containment reference.
	 * @see #getNestedConstraint()
	 * @generated
	 */
	void setNestedConstraint(FeatureModelEvolutionConstraint value);

} // NestedConstraint
