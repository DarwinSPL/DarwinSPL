/**
 */
package de.darwinspl.fmec.constraints;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Model Evolution Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.constraints.ConstraintsPackage#getFeatureModelEvolutionConstraint()
 * @model abstract="true"
 * @generated
 */
public interface FeatureModelEvolutionConstraint extends EObject {
} // FeatureModelEvolutionConstraint
