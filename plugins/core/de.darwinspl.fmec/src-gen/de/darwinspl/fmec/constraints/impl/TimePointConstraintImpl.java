/**
 */
package de.darwinspl.fmec.constraints.impl;

import de.darwinspl.fmec.constraints.ConstraintsPackage;
import de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock;
import de.darwinspl.fmec.constraints.TimePointConstraint;

import de.darwinspl.fmec.keywords.timepoint.TimePointKeyword;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Time Point Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.constraints.impl.TimePointConstraintImpl#getPreArgument <em>Pre Argument</em>}</li>
 *   <li>{@link de.darwinspl.fmec.constraints.impl.TimePointConstraintImpl#getKeyword <em>Keyword</em>}</li>
 *   <li>{@link de.darwinspl.fmec.constraints.impl.TimePointConstraintImpl#getPostArgument <em>Post Argument</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimePointConstraintImpl extends AtomicConstraintImpl implements TimePointConstraint {
	/**
	 * The cached value of the '{@link #getPreArgument() <em>Pre Argument</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreArgument()
	 * @generated
	 * @ordered
	 */
	protected EvolutionaryAbstractBuidingBlock preArgument;

	/**
	 * The cached value of the '{@link #getKeyword() <em>Keyword</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKeyword()
	 * @generated
	 * @ordered
	 */
	protected TimePointKeyword keyword;

	/**
	 * The cached value of the '{@link #getPostArgument() <em>Post Argument</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPostArgument()
	 * @generated
	 * @ordered
	 */
	protected EvolutionaryAbstractBuidingBlock postArgument;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimePointConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConstraintsPackage.Literals.TIME_POINT_CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EvolutionaryAbstractBuidingBlock getPreArgument() {
		return preArgument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPreArgument(EvolutionaryAbstractBuidingBlock newPreArgument, NotificationChain msgs) {
		EvolutionaryAbstractBuidingBlock oldPreArgument = preArgument;
		preArgument = newPreArgument;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConstraintsPackage.TIME_POINT_CONSTRAINT__PRE_ARGUMENT, oldPreArgument, newPreArgument);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPreArgument(EvolutionaryAbstractBuidingBlock newPreArgument) {
		if (newPreArgument != preArgument) {
			NotificationChain msgs = null;
			if (preArgument != null)
				msgs = ((InternalEObject)preArgument).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConstraintsPackage.TIME_POINT_CONSTRAINT__PRE_ARGUMENT, null, msgs);
			if (newPreArgument != null)
				msgs = ((InternalEObject)newPreArgument).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConstraintsPackage.TIME_POINT_CONSTRAINT__PRE_ARGUMENT, null, msgs);
			msgs = basicSetPreArgument(newPreArgument, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConstraintsPackage.TIME_POINT_CONSTRAINT__PRE_ARGUMENT, newPreArgument, newPreArgument));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TimePointKeyword getKeyword() {
		return keyword;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetKeyword(TimePointKeyword newKeyword, NotificationChain msgs) {
		TimePointKeyword oldKeyword = keyword;
		keyword = newKeyword;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConstraintsPackage.TIME_POINT_CONSTRAINT__KEYWORD, oldKeyword, newKeyword);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setKeyword(TimePointKeyword newKeyword) {
		if (newKeyword != keyword) {
			NotificationChain msgs = null;
			if (keyword != null)
				msgs = ((InternalEObject)keyword).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConstraintsPackage.TIME_POINT_CONSTRAINT__KEYWORD, null, msgs);
			if (newKeyword != null)
				msgs = ((InternalEObject)newKeyword).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConstraintsPackage.TIME_POINT_CONSTRAINT__KEYWORD, null, msgs);
			msgs = basicSetKeyword(newKeyword, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConstraintsPackage.TIME_POINT_CONSTRAINT__KEYWORD, newKeyword, newKeyword));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EvolutionaryAbstractBuidingBlock getPostArgument() {
		return postArgument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPostArgument(EvolutionaryAbstractBuidingBlock newPostArgument, NotificationChain msgs) {
		EvolutionaryAbstractBuidingBlock oldPostArgument = postArgument;
		postArgument = newPostArgument;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConstraintsPackage.TIME_POINT_CONSTRAINT__POST_ARGUMENT, oldPostArgument, newPostArgument);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPostArgument(EvolutionaryAbstractBuidingBlock newPostArgument) {
		if (newPostArgument != postArgument) {
			NotificationChain msgs = null;
			if (postArgument != null)
				msgs = ((InternalEObject)postArgument).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConstraintsPackage.TIME_POINT_CONSTRAINT__POST_ARGUMENT, null, msgs);
			if (newPostArgument != null)
				msgs = ((InternalEObject)newPostArgument).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConstraintsPackage.TIME_POINT_CONSTRAINT__POST_ARGUMENT, null, msgs);
			msgs = basicSetPostArgument(newPostArgument, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConstraintsPackage.TIME_POINT_CONSTRAINT__POST_ARGUMENT, newPostArgument, newPostArgument));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConstraintsPackage.TIME_POINT_CONSTRAINT__PRE_ARGUMENT:
				return basicSetPreArgument(null, msgs);
			case ConstraintsPackage.TIME_POINT_CONSTRAINT__KEYWORD:
				return basicSetKeyword(null, msgs);
			case ConstraintsPackage.TIME_POINT_CONSTRAINT__POST_ARGUMENT:
				return basicSetPostArgument(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConstraintsPackage.TIME_POINT_CONSTRAINT__PRE_ARGUMENT:
				return getPreArgument();
			case ConstraintsPackage.TIME_POINT_CONSTRAINT__KEYWORD:
				return getKeyword();
			case ConstraintsPackage.TIME_POINT_CONSTRAINT__POST_ARGUMENT:
				return getPostArgument();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConstraintsPackage.TIME_POINT_CONSTRAINT__PRE_ARGUMENT:
				setPreArgument((EvolutionaryAbstractBuidingBlock)newValue);
				return;
			case ConstraintsPackage.TIME_POINT_CONSTRAINT__KEYWORD:
				setKeyword((TimePointKeyword)newValue);
				return;
			case ConstraintsPackage.TIME_POINT_CONSTRAINT__POST_ARGUMENT:
				setPostArgument((EvolutionaryAbstractBuidingBlock)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConstraintsPackage.TIME_POINT_CONSTRAINT__PRE_ARGUMENT:
				setPreArgument((EvolutionaryAbstractBuidingBlock)null);
				return;
			case ConstraintsPackage.TIME_POINT_CONSTRAINT__KEYWORD:
				setKeyword((TimePointKeyword)null);
				return;
			case ConstraintsPackage.TIME_POINT_CONSTRAINT__POST_ARGUMENT:
				setPostArgument((EvolutionaryAbstractBuidingBlock)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConstraintsPackage.TIME_POINT_CONSTRAINT__PRE_ARGUMENT:
				return preArgument != null;
			case ConstraintsPackage.TIME_POINT_CONSTRAINT__KEYWORD:
				return keyword != null;
			case ConstraintsPackage.TIME_POINT_CONSTRAINT__POST_ARGUMENT:
				return postArgument != null;
		}
		return super.eIsSet(featureID);
	}

} //TimePointConstraintImpl
