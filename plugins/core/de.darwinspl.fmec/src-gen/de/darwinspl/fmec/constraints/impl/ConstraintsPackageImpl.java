/**
 */
package de.darwinspl.fmec.constraints.impl;

import de.darwinspl.datatypes.DwDatatypesPackage;

import de.darwinspl.feature.DwFeaturePackage;

import de.darwinspl.fmec.FmecPackage;

import de.darwinspl.fmec.constraints.AtomicConstraint;
import de.darwinspl.fmec.constraints.BinaryConstraint;
import de.darwinspl.fmec.constraints.BinaryConstraintChild;
import de.darwinspl.fmec.constraints.ConstraintsFactory;
import de.darwinspl.fmec.constraints.ConstraintsPackage;
import de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock;
import de.darwinspl.fmec.constraints.FeatureModelEvolutionConstraint;
import de.darwinspl.fmec.constraints.NestedConstraint;
import de.darwinspl.fmec.constraints.TimeIntervalConstraint;
import de.darwinspl.fmec.constraints.TimePointConstraint;
import de.darwinspl.fmec.constraints.UnaryConstraint;
import de.darwinspl.fmec.constraints.UnaryConstraintChild;

import de.darwinspl.fmec.impl.FmecPackageImpl;

import de.darwinspl.fmec.keywords.KeywordsPackage;

import de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage;

import de.darwinspl.fmec.keywords.buildingblocks.impl.BuildingblocksPackageImpl;

import de.darwinspl.fmec.keywords.impl.KeywordsPackageImpl;

import de.darwinspl.fmec.keywords.interval.IntervalPackage;

import de.darwinspl.fmec.keywords.interval.impl.IntervalPackageImpl;

import de.darwinspl.fmec.keywords.logical.LogicalPackage;

import de.darwinspl.fmec.keywords.logical.impl.LogicalPackageImpl;

import de.darwinspl.fmec.property.PropertyPackage;

import de.darwinspl.fmec.property.impl.PropertyPackageImpl;

import de.darwinspl.fmec.timepoint.TimepointPackage;

import de.darwinspl.fmec.timepoint.impl.TimepointPackageImpl;

import de.darwinspl.temporal.DwTemporalModelPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ConstraintsPackageImpl extends EPackageImpl implements ConstraintsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureModelEvolutionConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass binaryConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass binaryConstraintChildEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unaryConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unaryConstraintChildEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass atomicConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nestedConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timePointConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timeIntervalConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass evolutionaryAbstractBuidingBlockEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.darwinspl.fmec.constraints.ConstraintsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ConstraintsPackageImpl() {
		super(eNS_URI, ConstraintsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link ConstraintsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ConstraintsPackage init() {
		if (isInited) return (ConstraintsPackage)EPackage.Registry.INSTANCE.getEPackage(ConstraintsPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredConstraintsPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		ConstraintsPackageImpl theConstraintsPackage = registeredConstraintsPackage instanceof ConstraintsPackageImpl ? (ConstraintsPackageImpl)registeredConstraintsPackage : new ConstraintsPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		DwDatatypesPackage.eINSTANCE.eClass();
		DwFeaturePackage.eINSTANCE.eClass();
		DwTemporalModelPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(FmecPackage.eNS_URI);
		FmecPackageImpl theFmecPackage = (FmecPackageImpl)(registeredPackage instanceof FmecPackageImpl ? registeredPackage : FmecPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(PropertyPackage.eNS_URI);
		PropertyPackageImpl thePropertyPackage = (PropertyPackageImpl)(registeredPackage instanceof PropertyPackageImpl ? registeredPackage : PropertyPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimepointPackage.eNS_URI);
		TimepointPackageImpl theTimepointPackage = (TimepointPackageImpl)(registeredPackage instanceof TimepointPackageImpl ? registeredPackage : TimepointPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(KeywordsPackage.eNS_URI);
		KeywordsPackageImpl theKeywordsPackage = (KeywordsPackageImpl)(registeredPackage instanceof KeywordsPackageImpl ? registeredPackage : KeywordsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(LogicalPackage.eNS_URI);
		LogicalPackageImpl theLogicalPackage = (LogicalPackageImpl)(registeredPackage instanceof LogicalPackageImpl ? registeredPackage : LogicalPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(BuildingblocksPackage.eNS_URI);
		BuildingblocksPackageImpl theBuildingblocksPackage = (BuildingblocksPackageImpl)(registeredPackage instanceof BuildingblocksPackageImpl ? registeredPackage : BuildingblocksPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(de.darwinspl.fmec.keywords.timepoint.TimepointPackage.eNS_URI);
		de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl theTimepointPackage_1 = (de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl)(registeredPackage instanceof de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl ? registeredPackage : de.darwinspl.fmec.keywords.timepoint.TimepointPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(IntervalPackage.eNS_URI);
		IntervalPackageImpl theIntervalPackage = (IntervalPackageImpl)(registeredPackage instanceof IntervalPackageImpl ? registeredPackage : IntervalPackage.eINSTANCE);

		// Create package meta-data objects
		theConstraintsPackage.createPackageContents();
		theFmecPackage.createPackageContents();
		thePropertyPackage.createPackageContents();
		theTimepointPackage.createPackageContents();
		theKeywordsPackage.createPackageContents();
		theLogicalPackage.createPackageContents();
		theBuildingblocksPackage.createPackageContents();
		theTimepointPackage_1.createPackageContents();
		theIntervalPackage.createPackageContents();

		// Initialize created meta-data
		theConstraintsPackage.initializePackageContents();
		theFmecPackage.initializePackageContents();
		thePropertyPackage.initializePackageContents();
		theTimepointPackage.initializePackageContents();
		theKeywordsPackage.initializePackageContents();
		theLogicalPackage.initializePackageContents();
		theBuildingblocksPackage.initializePackageContents();
		theTimepointPackage_1.initializePackageContents();
		theIntervalPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theConstraintsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ConstraintsPackage.eNS_URI, theConstraintsPackage);
		return theConstraintsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFeatureModelEvolutionConstraint() {
		return featureModelEvolutionConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBinaryConstraint() {
		return binaryConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBinaryConstraint_LeftChild() {
		return (EReference)binaryConstraintEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBinaryConstraint_Keyword() {
		return (EReference)binaryConstraintEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBinaryConstraint_RightChild() {
		return (EReference)binaryConstraintEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBinaryConstraintChild() {
		return binaryConstraintChildEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUnaryConstraint() {
		return unaryConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUnaryConstraint_Child() {
		return (EReference)unaryConstraintEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUnaryConstraint_Keyword() {
		return (EReference)unaryConstraintEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUnaryConstraintChild() {
		return unaryConstraintChildEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAtomicConstraint() {
		return atomicConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getNestedConstraint() {
		return nestedConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getNestedConstraint_NestedConstraint() {
		return (EReference)nestedConstraintEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTimePointConstraint() {
		return timePointConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTimePointConstraint_PreArgument() {
		return (EReference)timePointConstraintEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTimePointConstraint_Keyword() {
		return (EReference)timePointConstraintEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTimePointConstraint_PostArgument() {
		return (EReference)timePointConstraintEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTimeIntervalConstraint() {
		return timeIntervalConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTimeIntervalConstraint_PreArgument() {
		return (EReference)timeIntervalConstraintEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTimeIntervalConstraint_Keyword() {
		return (EReference)timeIntervalConstraintEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTimeIntervalConstraint_StartPoint() {
		return (EReference)timeIntervalConstraintEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTimeIntervalConstraint_EndPoint() {
		return (EReference)timeIntervalConstraintEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEvolutionaryAbstractBuidingBlock() {
		return evolutionaryAbstractBuidingBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ConstraintsFactory getConstraintsFactory() {
		return (ConstraintsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		featureModelEvolutionConstraintEClass = createEClass(FEATURE_MODEL_EVOLUTION_CONSTRAINT);

		binaryConstraintEClass = createEClass(BINARY_CONSTRAINT);
		createEReference(binaryConstraintEClass, BINARY_CONSTRAINT__LEFT_CHILD);
		createEReference(binaryConstraintEClass, BINARY_CONSTRAINT__KEYWORD);
		createEReference(binaryConstraintEClass, BINARY_CONSTRAINT__RIGHT_CHILD);

		binaryConstraintChildEClass = createEClass(BINARY_CONSTRAINT_CHILD);

		unaryConstraintEClass = createEClass(UNARY_CONSTRAINT);
		createEReference(unaryConstraintEClass, UNARY_CONSTRAINT__CHILD);
		createEReference(unaryConstraintEClass, UNARY_CONSTRAINT__KEYWORD);

		unaryConstraintChildEClass = createEClass(UNARY_CONSTRAINT_CHILD);

		atomicConstraintEClass = createEClass(ATOMIC_CONSTRAINT);

		nestedConstraintEClass = createEClass(NESTED_CONSTRAINT);
		createEReference(nestedConstraintEClass, NESTED_CONSTRAINT__NESTED_CONSTRAINT);

		timePointConstraintEClass = createEClass(TIME_POINT_CONSTRAINT);
		createEReference(timePointConstraintEClass, TIME_POINT_CONSTRAINT__PRE_ARGUMENT);
		createEReference(timePointConstraintEClass, TIME_POINT_CONSTRAINT__KEYWORD);
		createEReference(timePointConstraintEClass, TIME_POINT_CONSTRAINT__POST_ARGUMENT);

		timeIntervalConstraintEClass = createEClass(TIME_INTERVAL_CONSTRAINT);
		createEReference(timeIntervalConstraintEClass, TIME_INTERVAL_CONSTRAINT__PRE_ARGUMENT);
		createEReference(timeIntervalConstraintEClass, TIME_INTERVAL_CONSTRAINT__KEYWORD);
		createEReference(timeIntervalConstraintEClass, TIME_INTERVAL_CONSTRAINT__START_POINT);
		createEReference(timeIntervalConstraintEClass, TIME_INTERVAL_CONSTRAINT__END_POINT);

		evolutionaryAbstractBuidingBlockEClass = createEClass(EVOLUTIONARY_ABSTRACT_BUIDING_BLOCK);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		LogicalPackage theLogicalPackage = (LogicalPackage)EPackage.Registry.INSTANCE.getEPackage(LogicalPackage.eNS_URI);
		de.darwinspl.fmec.keywords.timepoint.TimepointPackage theTimepointPackage_1 = (de.darwinspl.fmec.keywords.timepoint.TimepointPackage)EPackage.Registry.INSTANCE.getEPackage(de.darwinspl.fmec.keywords.timepoint.TimepointPackage.eNS_URI);
		IntervalPackage theIntervalPackage = (IntervalPackage)EPackage.Registry.INSTANCE.getEPackage(IntervalPackage.eNS_URI);
		TimepointPackage theTimepointPackage = (TimepointPackage)EPackage.Registry.INSTANCE.getEPackage(TimepointPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		binaryConstraintEClass.getESuperTypes().add(this.getFeatureModelEvolutionConstraint());
		binaryConstraintChildEClass.getESuperTypes().add(this.getFeatureModelEvolutionConstraint());
		unaryConstraintEClass.getESuperTypes().add(this.getBinaryConstraintChild());
		unaryConstraintChildEClass.getESuperTypes().add(this.getBinaryConstraintChild());
		atomicConstraintEClass.getESuperTypes().add(this.getUnaryConstraintChild());
		nestedConstraintEClass.getESuperTypes().add(this.getUnaryConstraintChild());
		timePointConstraintEClass.getESuperTypes().add(this.getAtomicConstraint());
		timeIntervalConstraintEClass.getESuperTypes().add(this.getAtomicConstraint());

		// Initialize classes, features, and operations; add parameters
		initEClass(featureModelEvolutionConstraintEClass, FeatureModelEvolutionConstraint.class, "FeatureModelEvolutionConstraint", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(binaryConstraintEClass, BinaryConstraint.class, "BinaryConstraint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBinaryConstraint_LeftChild(), this.getBinaryConstraintChild(), null, "leftChild", null, 1, 1, BinaryConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBinaryConstraint_Keyword(), theLogicalPackage.getBinaryLogicalKeyword(), null, "keyword", null, 1, 1, BinaryConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBinaryConstraint_RightChild(), this.getBinaryConstraintChild(), null, "rightChild", null, 1, 1, BinaryConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(binaryConstraintChildEClass, BinaryConstraintChild.class, "BinaryConstraintChild", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(unaryConstraintEClass, UnaryConstraint.class, "UnaryConstraint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUnaryConstraint_Child(), this.getUnaryConstraintChild(), null, "child", null, 1, 1, UnaryConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUnaryConstraint_Keyword(), theLogicalPackage.getUnaryLogicalKeyword(), null, "keyword", null, 1, 1, UnaryConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(unaryConstraintChildEClass, UnaryConstraintChild.class, "UnaryConstraintChild", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(atomicConstraintEClass, AtomicConstraint.class, "AtomicConstraint", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(nestedConstraintEClass, NestedConstraint.class, "NestedConstraint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNestedConstraint_NestedConstraint(), this.getFeatureModelEvolutionConstraint(), null, "nestedConstraint", null, 1, 1, NestedConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(timePointConstraintEClass, TimePointConstraint.class, "TimePointConstraint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTimePointConstraint_PreArgument(), this.getEvolutionaryAbstractBuidingBlock(), null, "preArgument", null, 1, 1, TimePointConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTimePointConstraint_Keyword(), theTimepointPackage_1.getTimePointKeyword(), null, "keyword", null, 1, 1, TimePointConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTimePointConstraint_PostArgument(), this.getEvolutionaryAbstractBuidingBlock(), null, "postArgument", null, 1, 1, TimePointConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(timeIntervalConstraintEClass, TimeIntervalConstraint.class, "TimeIntervalConstraint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTimeIntervalConstraint_PreArgument(), this.getEvolutionaryAbstractBuidingBlock(), null, "preArgument", null, 1, 1, TimeIntervalConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTimeIntervalConstraint_Keyword(), theIntervalPackage.getIntervalKeyword(), null, "keyword", null, 1, 1, TimeIntervalConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTimeIntervalConstraint_StartPoint(), theTimepointPackage.getTimePoint(), null, "startPoint", null, 1, 1, TimeIntervalConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTimeIntervalConstraint_EndPoint(), theTimepointPackage.getTimePoint(), null, "endPoint", null, 1, 1, TimeIntervalConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(evolutionaryAbstractBuidingBlockEClass, EvolutionaryAbstractBuidingBlock.class, "EvolutionaryAbstractBuidingBlock", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
	}

} //ConstraintsPackageImpl
