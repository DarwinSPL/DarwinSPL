/**
 */
package de.darwinspl.fmec.constraints.impl;

import de.darwinspl.fmec.constraints.ConstraintsPackage;
import de.darwinspl.fmec.constraints.FeatureModelEvolutionConstraint;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature Model Evolution Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class FeatureModelEvolutionConstraintImpl extends MinimalEObjectImpl.Container implements FeatureModelEvolutionConstraint {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureModelEvolutionConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConstraintsPackage.Literals.FEATURE_MODEL_EVOLUTION_CONSTRAINT;
	}

} //FeatureModelEvolutionConstraintImpl
