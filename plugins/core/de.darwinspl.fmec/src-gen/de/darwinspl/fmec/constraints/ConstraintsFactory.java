/**
 */
package de.darwinspl.fmec.constraints;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.darwinspl.fmec.constraints.ConstraintsPackage
 * @generated
 */
public interface ConstraintsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ConstraintsFactory eINSTANCE = de.darwinspl.fmec.constraints.impl.ConstraintsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Binary Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Binary Constraint</em>'.
	 * @generated
	 */
	BinaryConstraint createBinaryConstraint();

	/**
	 * Returns a new object of class '<em>Unary Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unary Constraint</em>'.
	 * @generated
	 */
	UnaryConstraint createUnaryConstraint();

	/**
	 * Returns a new object of class '<em>Nested Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Nested Constraint</em>'.
	 * @generated
	 */
	NestedConstraint createNestedConstraint();

	/**
	 * Returns a new object of class '<em>Time Point Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Time Point Constraint</em>'.
	 * @generated
	 */
	TimePointConstraint createTimePointConstraint();

	/**
	 * Returns a new object of class '<em>Time Interval Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Time Interval Constraint</em>'.
	 * @generated
	 */
	TimeIntervalConstraint createTimeIntervalConstraint();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ConstraintsPackage getConstraintsPackage();

} //ConstraintsFactory
