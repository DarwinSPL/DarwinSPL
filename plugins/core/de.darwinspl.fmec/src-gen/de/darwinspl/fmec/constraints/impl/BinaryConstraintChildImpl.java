/**
 */
package de.darwinspl.fmec.constraints.impl;

import de.darwinspl.fmec.constraints.BinaryConstraintChild;
import de.darwinspl.fmec.constraints.ConstraintsPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Binary Constraint Child</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class BinaryConstraintChildImpl extends FeatureModelEvolutionConstraintImpl implements BinaryConstraintChild {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BinaryConstraintChildImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConstraintsPackage.Literals.BINARY_CONSTRAINT_CHILD;
	}

} //BinaryConstraintChildImpl
