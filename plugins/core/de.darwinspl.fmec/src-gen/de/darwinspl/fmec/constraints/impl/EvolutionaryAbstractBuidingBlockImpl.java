/**
 */
package de.darwinspl.fmec.constraints.impl;

import de.darwinspl.fmec.constraints.ConstraintsPackage;
import de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Evolutionary Abstract Buiding Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class EvolutionaryAbstractBuidingBlockImpl extends MinimalEObjectImpl.Container implements EvolutionaryAbstractBuidingBlock {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EvolutionaryAbstractBuidingBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConstraintsPackage.Literals.EVOLUTIONARY_ABSTRACT_BUIDING_BLOCK;
	}

} //EvolutionaryAbstractBuidingBlockImpl
