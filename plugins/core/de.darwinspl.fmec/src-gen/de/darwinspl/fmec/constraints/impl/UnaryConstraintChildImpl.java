/**
 */
package de.darwinspl.fmec.constraints.impl;

import de.darwinspl.fmec.constraints.ConstraintsPackage;
import de.darwinspl.fmec.constraints.UnaryConstraintChild;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Unary Constraint Child</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class UnaryConstraintChildImpl extends BinaryConstraintChildImpl implements UnaryConstraintChild {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UnaryConstraintChildImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConstraintsPackage.Literals.UNARY_CONSTRAINT_CHILD;
	}

} //UnaryConstraintChildImpl
