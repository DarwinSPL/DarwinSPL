/**
 */
package de.darwinspl.fmec.constraints;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary Constraint Child</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.constraints.ConstraintsPackage#getBinaryConstraintChild()
 * @model abstract="true"
 * @generated
 */
public interface BinaryConstraintChild extends FeatureModelEvolutionConstraint {
} // BinaryConstraintChild
