/**
 */
package de.darwinspl.fmec.constraints;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Atomic Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.constraints.ConstraintsPackage#getAtomicConstraint()
 * @model abstract="true"
 * @generated
 */
public interface AtomicConstraint extends UnaryConstraintChild {
} // AtomicConstraint
