/**
 */
package de.darwinspl.fmec.constraints.util;

import de.darwinspl.fmec.constraints.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see de.darwinspl.fmec.constraints.ConstraintsPackage
 * @generated
 */
public class ConstraintsAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ConstraintsPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstraintsAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ConstraintsPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConstraintsSwitch<Adapter> modelSwitch =
		new ConstraintsSwitch<Adapter>() {
			@Override
			public Adapter caseFeatureModelEvolutionConstraint(FeatureModelEvolutionConstraint object) {
				return createFeatureModelEvolutionConstraintAdapter();
			}
			@Override
			public Adapter caseBinaryConstraint(BinaryConstraint object) {
				return createBinaryConstraintAdapter();
			}
			@Override
			public Adapter caseBinaryConstraintChild(BinaryConstraintChild object) {
				return createBinaryConstraintChildAdapter();
			}
			@Override
			public Adapter caseUnaryConstraint(UnaryConstraint object) {
				return createUnaryConstraintAdapter();
			}
			@Override
			public Adapter caseUnaryConstraintChild(UnaryConstraintChild object) {
				return createUnaryConstraintChildAdapter();
			}
			@Override
			public Adapter caseAtomicConstraint(AtomicConstraint object) {
				return createAtomicConstraintAdapter();
			}
			@Override
			public Adapter caseNestedConstraint(NestedConstraint object) {
				return createNestedConstraintAdapter();
			}
			@Override
			public Adapter caseTimePointConstraint(TimePointConstraint object) {
				return createTimePointConstraintAdapter();
			}
			@Override
			public Adapter caseTimeIntervalConstraint(TimeIntervalConstraint object) {
				return createTimeIntervalConstraintAdapter();
			}
			@Override
			public Adapter caseEvolutionaryAbstractBuidingBlock(EvolutionaryAbstractBuidingBlock object) {
				return createEvolutionaryAbstractBuidingBlockAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.fmec.constraints.FeatureModelEvolutionConstraint <em>Feature Model Evolution Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.fmec.constraints.FeatureModelEvolutionConstraint
	 * @generated
	 */
	public Adapter createFeatureModelEvolutionConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.fmec.constraints.BinaryConstraint <em>Binary Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.fmec.constraints.BinaryConstraint
	 * @generated
	 */
	public Adapter createBinaryConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.fmec.constraints.BinaryConstraintChild <em>Binary Constraint Child</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.fmec.constraints.BinaryConstraintChild
	 * @generated
	 */
	public Adapter createBinaryConstraintChildAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.fmec.constraints.UnaryConstraint <em>Unary Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.fmec.constraints.UnaryConstraint
	 * @generated
	 */
	public Adapter createUnaryConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.fmec.constraints.UnaryConstraintChild <em>Unary Constraint Child</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.fmec.constraints.UnaryConstraintChild
	 * @generated
	 */
	public Adapter createUnaryConstraintChildAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.fmec.constraints.AtomicConstraint <em>Atomic Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.fmec.constraints.AtomicConstraint
	 * @generated
	 */
	public Adapter createAtomicConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.fmec.constraints.NestedConstraint <em>Nested Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.fmec.constraints.NestedConstraint
	 * @generated
	 */
	public Adapter createNestedConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.fmec.constraints.TimePointConstraint <em>Time Point Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.fmec.constraints.TimePointConstraint
	 * @generated
	 */
	public Adapter createTimePointConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.fmec.constraints.TimeIntervalConstraint <em>Time Interval Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.fmec.constraints.TimeIntervalConstraint
	 * @generated
	 */
	public Adapter createTimeIntervalConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock <em>Evolutionary Abstract Buiding Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock
	 * @generated
	 */
	public Adapter createEvolutionaryAbstractBuidingBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ConstraintsAdapterFactory
