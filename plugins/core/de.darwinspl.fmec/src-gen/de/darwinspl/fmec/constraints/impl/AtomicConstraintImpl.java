/**
 */
package de.darwinspl.fmec.constraints.impl;

import de.darwinspl.fmec.constraints.AtomicConstraint;
import de.darwinspl.fmec.constraints.ConstraintsPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Atomic Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class AtomicConstraintImpl extends UnaryConstraintChildImpl implements AtomicConstraint {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AtomicConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConstraintsPackage.Literals.ATOMIC_CONSTRAINT;
	}

} //AtomicConstraintImpl
