/**
 */
package de.darwinspl.fmec.constraints.impl;

import de.darwinspl.fmec.constraints.BinaryConstraint;
import de.darwinspl.fmec.constraints.BinaryConstraintChild;
import de.darwinspl.fmec.constraints.ConstraintsPackage;

import de.darwinspl.fmec.keywords.logical.BinaryLogicalKeyword;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Binary Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.constraints.impl.BinaryConstraintImpl#getLeftChild <em>Left Child</em>}</li>
 *   <li>{@link de.darwinspl.fmec.constraints.impl.BinaryConstraintImpl#getKeyword <em>Keyword</em>}</li>
 *   <li>{@link de.darwinspl.fmec.constraints.impl.BinaryConstraintImpl#getRightChild <em>Right Child</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BinaryConstraintImpl extends FeatureModelEvolutionConstraintImpl implements BinaryConstraint {
	/**
	 * The cached value of the '{@link #getLeftChild() <em>Left Child</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeftChild()
	 * @generated
	 * @ordered
	 */
	protected BinaryConstraintChild leftChild;

	/**
	 * The cached value of the '{@link #getKeyword() <em>Keyword</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKeyword()
	 * @generated
	 * @ordered
	 */
	protected BinaryLogicalKeyword keyword;

	/**
	 * The cached value of the '{@link #getRightChild() <em>Right Child</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRightChild()
	 * @generated
	 * @ordered
	 */
	protected BinaryConstraintChild rightChild;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BinaryConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConstraintsPackage.Literals.BINARY_CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BinaryConstraintChild getLeftChild() {
		return leftChild;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLeftChild(BinaryConstraintChild newLeftChild, NotificationChain msgs) {
		BinaryConstraintChild oldLeftChild = leftChild;
		leftChild = newLeftChild;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConstraintsPackage.BINARY_CONSTRAINT__LEFT_CHILD, oldLeftChild, newLeftChild);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLeftChild(BinaryConstraintChild newLeftChild) {
		if (newLeftChild != leftChild) {
			NotificationChain msgs = null;
			if (leftChild != null)
				msgs = ((InternalEObject)leftChild).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConstraintsPackage.BINARY_CONSTRAINT__LEFT_CHILD, null, msgs);
			if (newLeftChild != null)
				msgs = ((InternalEObject)newLeftChild).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConstraintsPackage.BINARY_CONSTRAINT__LEFT_CHILD, null, msgs);
			msgs = basicSetLeftChild(newLeftChild, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConstraintsPackage.BINARY_CONSTRAINT__LEFT_CHILD, newLeftChild, newLeftChild));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BinaryLogicalKeyword getKeyword() {
		return keyword;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetKeyword(BinaryLogicalKeyword newKeyword, NotificationChain msgs) {
		BinaryLogicalKeyword oldKeyword = keyword;
		keyword = newKeyword;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConstraintsPackage.BINARY_CONSTRAINT__KEYWORD, oldKeyword, newKeyword);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setKeyword(BinaryLogicalKeyword newKeyword) {
		if (newKeyword != keyword) {
			NotificationChain msgs = null;
			if (keyword != null)
				msgs = ((InternalEObject)keyword).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConstraintsPackage.BINARY_CONSTRAINT__KEYWORD, null, msgs);
			if (newKeyword != null)
				msgs = ((InternalEObject)newKeyword).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConstraintsPackage.BINARY_CONSTRAINT__KEYWORD, null, msgs);
			msgs = basicSetKeyword(newKeyword, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConstraintsPackage.BINARY_CONSTRAINT__KEYWORD, newKeyword, newKeyword));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BinaryConstraintChild getRightChild() {
		return rightChild;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRightChild(BinaryConstraintChild newRightChild, NotificationChain msgs) {
		BinaryConstraintChild oldRightChild = rightChild;
		rightChild = newRightChild;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConstraintsPackage.BINARY_CONSTRAINT__RIGHT_CHILD, oldRightChild, newRightChild);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRightChild(BinaryConstraintChild newRightChild) {
		if (newRightChild != rightChild) {
			NotificationChain msgs = null;
			if (rightChild != null)
				msgs = ((InternalEObject)rightChild).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConstraintsPackage.BINARY_CONSTRAINT__RIGHT_CHILD, null, msgs);
			if (newRightChild != null)
				msgs = ((InternalEObject)newRightChild).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConstraintsPackage.BINARY_CONSTRAINT__RIGHT_CHILD, null, msgs);
			msgs = basicSetRightChild(newRightChild, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConstraintsPackage.BINARY_CONSTRAINT__RIGHT_CHILD, newRightChild, newRightChild));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConstraintsPackage.BINARY_CONSTRAINT__LEFT_CHILD:
				return basicSetLeftChild(null, msgs);
			case ConstraintsPackage.BINARY_CONSTRAINT__KEYWORD:
				return basicSetKeyword(null, msgs);
			case ConstraintsPackage.BINARY_CONSTRAINT__RIGHT_CHILD:
				return basicSetRightChild(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConstraintsPackage.BINARY_CONSTRAINT__LEFT_CHILD:
				return getLeftChild();
			case ConstraintsPackage.BINARY_CONSTRAINT__KEYWORD:
				return getKeyword();
			case ConstraintsPackage.BINARY_CONSTRAINT__RIGHT_CHILD:
				return getRightChild();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConstraintsPackage.BINARY_CONSTRAINT__LEFT_CHILD:
				setLeftChild((BinaryConstraintChild)newValue);
				return;
			case ConstraintsPackage.BINARY_CONSTRAINT__KEYWORD:
				setKeyword((BinaryLogicalKeyword)newValue);
				return;
			case ConstraintsPackage.BINARY_CONSTRAINT__RIGHT_CHILD:
				setRightChild((BinaryConstraintChild)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConstraintsPackage.BINARY_CONSTRAINT__LEFT_CHILD:
				setLeftChild((BinaryConstraintChild)null);
				return;
			case ConstraintsPackage.BINARY_CONSTRAINT__KEYWORD:
				setKeyword((BinaryLogicalKeyword)null);
				return;
			case ConstraintsPackage.BINARY_CONSTRAINT__RIGHT_CHILD:
				setRightChild((BinaryConstraintChild)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConstraintsPackage.BINARY_CONSTRAINT__LEFT_CHILD:
				return leftChild != null;
			case ConstraintsPackage.BINARY_CONSTRAINT__KEYWORD:
				return keyword != null;
			case ConstraintsPackage.BINARY_CONSTRAINT__RIGHT_CHILD:
				return rightChild != null;
		}
		return super.eIsSet(featureID);
	}

} //BinaryConstraintImpl
