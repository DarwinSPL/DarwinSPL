/**
 */
package de.darwinspl.fmec.constraints.impl;

import de.darwinspl.fmec.constraints.ConstraintsPackage;
import de.darwinspl.fmec.constraints.FeatureModelEvolutionConstraint;
import de.darwinspl.fmec.constraints.NestedConstraint;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Nested Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.constraints.impl.NestedConstraintImpl#getNestedConstraint <em>Nested Constraint</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NestedConstraintImpl extends UnaryConstraintChildImpl implements NestedConstraint {
	/**
	 * The cached value of the '{@link #getNestedConstraint() <em>Nested Constraint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNestedConstraint()
	 * @generated
	 * @ordered
	 */
	protected FeatureModelEvolutionConstraint nestedConstraint;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NestedConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConstraintsPackage.Literals.NESTED_CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureModelEvolutionConstraint getNestedConstraint() {
		return nestedConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNestedConstraint(FeatureModelEvolutionConstraint newNestedConstraint, NotificationChain msgs) {
		FeatureModelEvolutionConstraint oldNestedConstraint = nestedConstraint;
		nestedConstraint = newNestedConstraint;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConstraintsPackage.NESTED_CONSTRAINT__NESTED_CONSTRAINT, oldNestedConstraint, newNestedConstraint);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNestedConstraint(FeatureModelEvolutionConstraint newNestedConstraint) {
		if (newNestedConstraint != nestedConstraint) {
			NotificationChain msgs = null;
			if (nestedConstraint != null)
				msgs = ((InternalEObject)nestedConstraint).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConstraintsPackage.NESTED_CONSTRAINT__NESTED_CONSTRAINT, null, msgs);
			if (newNestedConstraint != null)
				msgs = ((InternalEObject)newNestedConstraint).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConstraintsPackage.NESTED_CONSTRAINT__NESTED_CONSTRAINT, null, msgs);
			msgs = basicSetNestedConstraint(newNestedConstraint, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConstraintsPackage.NESTED_CONSTRAINT__NESTED_CONSTRAINT, newNestedConstraint, newNestedConstraint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConstraintsPackage.NESTED_CONSTRAINT__NESTED_CONSTRAINT:
				return basicSetNestedConstraint(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConstraintsPackage.NESTED_CONSTRAINT__NESTED_CONSTRAINT:
				return getNestedConstraint();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConstraintsPackage.NESTED_CONSTRAINT__NESTED_CONSTRAINT:
				setNestedConstraint((FeatureModelEvolutionConstraint)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConstraintsPackage.NESTED_CONSTRAINT__NESTED_CONSTRAINT:
				setNestedConstraint((FeatureModelEvolutionConstraint)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConstraintsPackage.NESTED_CONSTRAINT__NESTED_CONSTRAINT:
				return nestedConstraint != null;
		}
		return super.eIsSet(featureID);
	}

} //NestedConstraintImpl
