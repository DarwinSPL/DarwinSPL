/**
 */
package de.darwinspl.fmec.constraints;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Evolutionary Abstract Buiding Block</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.constraints.ConstraintsPackage#getEvolutionaryAbstractBuidingBlock()
 * @model abstract="true"
 * @generated
 */
public interface EvolutionaryAbstractBuidingBlock extends EObject {
} // EvolutionaryAbstractBuidingBlock
