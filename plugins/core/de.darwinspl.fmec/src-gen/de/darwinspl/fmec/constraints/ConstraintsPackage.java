/**
 */
package de.darwinspl.fmec.constraints;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.darwinspl.fmec.constraints.ConstraintsFactory
 * @model kind="package"
 * @generated
 */
public interface ConstraintsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "constraints";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.darwinspl.de/feature-model-evolution-constraints/constraints/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "constraints";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ConstraintsPackage eINSTANCE = de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.constraints.impl.FeatureModelEvolutionConstraintImpl <em>Feature Model Evolution Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.constraints.impl.FeatureModelEvolutionConstraintImpl
	 * @see de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl#getFeatureModelEvolutionConstraint()
	 * @generated
	 */
	int FEATURE_MODEL_EVOLUTION_CONSTRAINT = 0;

	/**
	 * The number of structural features of the '<em>Feature Model Evolution Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_EVOLUTION_CONSTRAINT_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Feature Model Evolution Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_EVOLUTION_CONSTRAINT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.constraints.impl.BinaryConstraintImpl <em>Binary Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.constraints.impl.BinaryConstraintImpl
	 * @see de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl#getBinaryConstraint()
	 * @generated
	 */
	int BINARY_CONSTRAINT = 1;

	/**
	 * The feature id for the '<em><b>Left Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_CONSTRAINT__LEFT_CHILD = FEATURE_MODEL_EVOLUTION_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Keyword</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_CONSTRAINT__KEYWORD = FEATURE_MODEL_EVOLUTION_CONSTRAINT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Right Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_CONSTRAINT__RIGHT_CHILD = FEATURE_MODEL_EVOLUTION_CONSTRAINT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Binary Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_CONSTRAINT_FEATURE_COUNT = FEATURE_MODEL_EVOLUTION_CONSTRAINT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Binary Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_CONSTRAINT_OPERATION_COUNT = FEATURE_MODEL_EVOLUTION_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.constraints.impl.BinaryConstraintChildImpl <em>Binary Constraint Child</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.constraints.impl.BinaryConstraintChildImpl
	 * @see de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl#getBinaryConstraintChild()
	 * @generated
	 */
	int BINARY_CONSTRAINT_CHILD = 2;

	/**
	 * The number of structural features of the '<em>Binary Constraint Child</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_CONSTRAINT_CHILD_FEATURE_COUNT = FEATURE_MODEL_EVOLUTION_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Binary Constraint Child</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_CONSTRAINT_CHILD_OPERATION_COUNT = FEATURE_MODEL_EVOLUTION_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.constraints.impl.UnaryConstraintImpl <em>Unary Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.constraints.impl.UnaryConstraintImpl
	 * @see de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl#getUnaryConstraint()
	 * @generated
	 */
	int UNARY_CONSTRAINT = 3;

	/**
	 * The feature id for the '<em><b>Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_CONSTRAINT__CHILD = BINARY_CONSTRAINT_CHILD_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Keyword</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_CONSTRAINT__KEYWORD = BINARY_CONSTRAINT_CHILD_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Unary Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_CONSTRAINT_FEATURE_COUNT = BINARY_CONSTRAINT_CHILD_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Unary Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_CONSTRAINT_OPERATION_COUNT = BINARY_CONSTRAINT_CHILD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.constraints.impl.UnaryConstraintChildImpl <em>Unary Constraint Child</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.constraints.impl.UnaryConstraintChildImpl
	 * @see de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl#getUnaryConstraintChild()
	 * @generated
	 */
	int UNARY_CONSTRAINT_CHILD = 4;

	/**
	 * The number of structural features of the '<em>Unary Constraint Child</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_CONSTRAINT_CHILD_FEATURE_COUNT = BINARY_CONSTRAINT_CHILD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Unary Constraint Child</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_CONSTRAINT_CHILD_OPERATION_COUNT = BINARY_CONSTRAINT_CHILD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.constraints.impl.AtomicConstraintImpl <em>Atomic Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.constraints.impl.AtomicConstraintImpl
	 * @see de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl#getAtomicConstraint()
	 * @generated
	 */
	int ATOMIC_CONSTRAINT = 5;

	/**
	 * The number of structural features of the '<em>Atomic Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC_CONSTRAINT_FEATURE_COUNT = UNARY_CONSTRAINT_CHILD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Atomic Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC_CONSTRAINT_OPERATION_COUNT = UNARY_CONSTRAINT_CHILD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.constraints.impl.NestedConstraintImpl <em>Nested Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.constraints.impl.NestedConstraintImpl
	 * @see de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl#getNestedConstraint()
	 * @generated
	 */
	int NESTED_CONSTRAINT = 6;

	/**
	 * The feature id for the '<em><b>Nested Constraint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NESTED_CONSTRAINT__NESTED_CONSTRAINT = UNARY_CONSTRAINT_CHILD_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Nested Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NESTED_CONSTRAINT_FEATURE_COUNT = UNARY_CONSTRAINT_CHILD_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Nested Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NESTED_CONSTRAINT_OPERATION_COUNT = UNARY_CONSTRAINT_CHILD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.constraints.impl.TimePointConstraintImpl <em>Time Point Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.constraints.impl.TimePointConstraintImpl
	 * @see de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl#getTimePointConstraint()
	 * @generated
	 */
	int TIME_POINT_CONSTRAINT = 7;

	/**
	 * The feature id for the '<em><b>Pre Argument</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_POINT_CONSTRAINT__PRE_ARGUMENT = ATOMIC_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Keyword</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_POINT_CONSTRAINT__KEYWORD = ATOMIC_CONSTRAINT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Post Argument</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_POINT_CONSTRAINT__POST_ARGUMENT = ATOMIC_CONSTRAINT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Time Point Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_POINT_CONSTRAINT_FEATURE_COUNT = ATOMIC_CONSTRAINT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Time Point Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_POINT_CONSTRAINT_OPERATION_COUNT = ATOMIC_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.constraints.impl.TimeIntervalConstraintImpl <em>Time Interval Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.constraints.impl.TimeIntervalConstraintImpl
	 * @see de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl#getTimeIntervalConstraint()
	 * @generated
	 */
	int TIME_INTERVAL_CONSTRAINT = 8;

	/**
	 * The feature id for the '<em><b>Pre Argument</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_INTERVAL_CONSTRAINT__PRE_ARGUMENT = ATOMIC_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Keyword</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_INTERVAL_CONSTRAINT__KEYWORD = ATOMIC_CONSTRAINT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Start Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_INTERVAL_CONSTRAINT__START_POINT = ATOMIC_CONSTRAINT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>End Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_INTERVAL_CONSTRAINT__END_POINT = ATOMIC_CONSTRAINT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Time Interval Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_INTERVAL_CONSTRAINT_FEATURE_COUNT = ATOMIC_CONSTRAINT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Time Interval Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_INTERVAL_CONSTRAINT_OPERATION_COUNT = ATOMIC_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.constraints.impl.EvolutionaryAbstractBuidingBlockImpl <em>Evolutionary Abstract Buiding Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.constraints.impl.EvolutionaryAbstractBuidingBlockImpl
	 * @see de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl#getEvolutionaryAbstractBuidingBlock()
	 * @generated
	 */
	int EVOLUTIONARY_ABSTRACT_BUIDING_BLOCK = 9;

	/**
	 * The number of structural features of the '<em>Evolutionary Abstract Buiding Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVOLUTIONARY_ABSTRACT_BUIDING_BLOCK_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Evolutionary Abstract Buiding Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVOLUTIONARY_ABSTRACT_BUIDING_BLOCK_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.constraints.FeatureModelEvolutionConstraint <em>Feature Model Evolution Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Model Evolution Constraint</em>'.
	 * @see de.darwinspl.fmec.constraints.FeatureModelEvolutionConstraint
	 * @generated
	 */
	EClass getFeatureModelEvolutionConstraint();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.constraints.BinaryConstraint <em>Binary Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Constraint</em>'.
	 * @see de.darwinspl.fmec.constraints.BinaryConstraint
	 * @generated
	 */
	EClass getBinaryConstraint();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.fmec.constraints.BinaryConstraint#getLeftChild <em>Left Child</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left Child</em>'.
	 * @see de.darwinspl.fmec.constraints.BinaryConstraint#getLeftChild()
	 * @see #getBinaryConstraint()
	 * @generated
	 */
	EReference getBinaryConstraint_LeftChild();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.fmec.constraints.BinaryConstraint#getKeyword <em>Keyword</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Keyword</em>'.
	 * @see de.darwinspl.fmec.constraints.BinaryConstraint#getKeyword()
	 * @see #getBinaryConstraint()
	 * @generated
	 */
	EReference getBinaryConstraint_Keyword();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.fmec.constraints.BinaryConstraint#getRightChild <em>Right Child</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right Child</em>'.
	 * @see de.darwinspl.fmec.constraints.BinaryConstraint#getRightChild()
	 * @see #getBinaryConstraint()
	 * @generated
	 */
	EReference getBinaryConstraint_RightChild();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.constraints.BinaryConstraintChild <em>Binary Constraint Child</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Constraint Child</em>'.
	 * @see de.darwinspl.fmec.constraints.BinaryConstraintChild
	 * @generated
	 */
	EClass getBinaryConstraintChild();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.constraints.UnaryConstraint <em>Unary Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Constraint</em>'.
	 * @see de.darwinspl.fmec.constraints.UnaryConstraint
	 * @generated
	 */
	EClass getUnaryConstraint();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.fmec.constraints.UnaryConstraint#getChild <em>Child</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Child</em>'.
	 * @see de.darwinspl.fmec.constraints.UnaryConstraint#getChild()
	 * @see #getUnaryConstraint()
	 * @generated
	 */
	EReference getUnaryConstraint_Child();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.fmec.constraints.UnaryConstraint#getKeyword <em>Keyword</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Keyword</em>'.
	 * @see de.darwinspl.fmec.constraints.UnaryConstraint#getKeyword()
	 * @see #getUnaryConstraint()
	 * @generated
	 */
	EReference getUnaryConstraint_Keyword();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.constraints.UnaryConstraintChild <em>Unary Constraint Child</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Constraint Child</em>'.
	 * @see de.darwinspl.fmec.constraints.UnaryConstraintChild
	 * @generated
	 */
	EClass getUnaryConstraintChild();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.constraints.AtomicConstraint <em>Atomic Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Atomic Constraint</em>'.
	 * @see de.darwinspl.fmec.constraints.AtomicConstraint
	 * @generated
	 */
	EClass getAtomicConstraint();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.constraints.NestedConstraint <em>Nested Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Nested Constraint</em>'.
	 * @see de.darwinspl.fmec.constraints.NestedConstraint
	 * @generated
	 */
	EClass getNestedConstraint();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.fmec.constraints.NestedConstraint#getNestedConstraint <em>Nested Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Nested Constraint</em>'.
	 * @see de.darwinspl.fmec.constraints.NestedConstraint#getNestedConstraint()
	 * @see #getNestedConstraint()
	 * @generated
	 */
	EReference getNestedConstraint_NestedConstraint();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.constraints.TimePointConstraint <em>Time Point Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Time Point Constraint</em>'.
	 * @see de.darwinspl.fmec.constraints.TimePointConstraint
	 * @generated
	 */
	EClass getTimePointConstraint();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.fmec.constraints.TimePointConstraint#getPreArgument <em>Pre Argument</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Pre Argument</em>'.
	 * @see de.darwinspl.fmec.constraints.TimePointConstraint#getPreArgument()
	 * @see #getTimePointConstraint()
	 * @generated
	 */
	EReference getTimePointConstraint_PreArgument();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.fmec.constraints.TimePointConstraint#getKeyword <em>Keyword</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Keyword</em>'.
	 * @see de.darwinspl.fmec.constraints.TimePointConstraint#getKeyword()
	 * @see #getTimePointConstraint()
	 * @generated
	 */
	EReference getTimePointConstraint_Keyword();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.fmec.constraints.TimePointConstraint#getPostArgument <em>Post Argument</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Post Argument</em>'.
	 * @see de.darwinspl.fmec.constraints.TimePointConstraint#getPostArgument()
	 * @see #getTimePointConstraint()
	 * @generated
	 */
	EReference getTimePointConstraint_PostArgument();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.constraints.TimeIntervalConstraint <em>Time Interval Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Time Interval Constraint</em>'.
	 * @see de.darwinspl.fmec.constraints.TimeIntervalConstraint
	 * @generated
	 */
	EClass getTimeIntervalConstraint();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.fmec.constraints.TimeIntervalConstraint#getPreArgument <em>Pre Argument</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Pre Argument</em>'.
	 * @see de.darwinspl.fmec.constraints.TimeIntervalConstraint#getPreArgument()
	 * @see #getTimeIntervalConstraint()
	 * @generated
	 */
	EReference getTimeIntervalConstraint_PreArgument();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.fmec.constraints.TimeIntervalConstraint#getKeyword <em>Keyword</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Keyword</em>'.
	 * @see de.darwinspl.fmec.constraints.TimeIntervalConstraint#getKeyword()
	 * @see #getTimeIntervalConstraint()
	 * @generated
	 */
	EReference getTimeIntervalConstraint_Keyword();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.fmec.constraints.TimeIntervalConstraint#getStartPoint <em>Start Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Start Point</em>'.
	 * @see de.darwinspl.fmec.constraints.TimeIntervalConstraint#getStartPoint()
	 * @see #getTimeIntervalConstraint()
	 * @generated
	 */
	EReference getTimeIntervalConstraint_StartPoint();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.fmec.constraints.TimeIntervalConstraint#getEndPoint <em>End Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>End Point</em>'.
	 * @see de.darwinspl.fmec.constraints.TimeIntervalConstraint#getEndPoint()
	 * @see #getTimeIntervalConstraint()
	 * @generated
	 */
	EReference getTimeIntervalConstraint_EndPoint();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock <em>Evolutionary Abstract Buiding Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Evolutionary Abstract Buiding Block</em>'.
	 * @see de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock
	 * @generated
	 */
	EClass getEvolutionaryAbstractBuidingBlock();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ConstraintsFactory getConstraintsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.constraints.impl.FeatureModelEvolutionConstraintImpl <em>Feature Model Evolution Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.constraints.impl.FeatureModelEvolutionConstraintImpl
		 * @see de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl#getFeatureModelEvolutionConstraint()
		 * @generated
		 */
		EClass FEATURE_MODEL_EVOLUTION_CONSTRAINT = eINSTANCE.getFeatureModelEvolutionConstraint();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.constraints.impl.BinaryConstraintImpl <em>Binary Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.constraints.impl.BinaryConstraintImpl
		 * @see de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl#getBinaryConstraint()
		 * @generated
		 */
		EClass BINARY_CONSTRAINT = eINSTANCE.getBinaryConstraint();

		/**
		 * The meta object literal for the '<em><b>Left Child</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_CONSTRAINT__LEFT_CHILD = eINSTANCE.getBinaryConstraint_LeftChild();

		/**
		 * The meta object literal for the '<em><b>Keyword</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_CONSTRAINT__KEYWORD = eINSTANCE.getBinaryConstraint_Keyword();

		/**
		 * The meta object literal for the '<em><b>Right Child</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_CONSTRAINT__RIGHT_CHILD = eINSTANCE.getBinaryConstraint_RightChild();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.constraints.impl.BinaryConstraintChildImpl <em>Binary Constraint Child</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.constraints.impl.BinaryConstraintChildImpl
		 * @see de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl#getBinaryConstraintChild()
		 * @generated
		 */
		EClass BINARY_CONSTRAINT_CHILD = eINSTANCE.getBinaryConstraintChild();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.constraints.impl.UnaryConstraintImpl <em>Unary Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.constraints.impl.UnaryConstraintImpl
		 * @see de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl#getUnaryConstraint()
		 * @generated
		 */
		EClass UNARY_CONSTRAINT = eINSTANCE.getUnaryConstraint();

		/**
		 * The meta object literal for the '<em><b>Child</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNARY_CONSTRAINT__CHILD = eINSTANCE.getUnaryConstraint_Child();

		/**
		 * The meta object literal for the '<em><b>Keyword</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNARY_CONSTRAINT__KEYWORD = eINSTANCE.getUnaryConstraint_Keyword();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.constraints.impl.UnaryConstraintChildImpl <em>Unary Constraint Child</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.constraints.impl.UnaryConstraintChildImpl
		 * @see de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl#getUnaryConstraintChild()
		 * @generated
		 */
		EClass UNARY_CONSTRAINT_CHILD = eINSTANCE.getUnaryConstraintChild();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.constraints.impl.AtomicConstraintImpl <em>Atomic Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.constraints.impl.AtomicConstraintImpl
		 * @see de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl#getAtomicConstraint()
		 * @generated
		 */
		EClass ATOMIC_CONSTRAINT = eINSTANCE.getAtomicConstraint();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.constraints.impl.NestedConstraintImpl <em>Nested Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.constraints.impl.NestedConstraintImpl
		 * @see de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl#getNestedConstraint()
		 * @generated
		 */
		EClass NESTED_CONSTRAINT = eINSTANCE.getNestedConstraint();

		/**
		 * The meta object literal for the '<em><b>Nested Constraint</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NESTED_CONSTRAINT__NESTED_CONSTRAINT = eINSTANCE.getNestedConstraint_NestedConstraint();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.constraints.impl.TimePointConstraintImpl <em>Time Point Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.constraints.impl.TimePointConstraintImpl
		 * @see de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl#getTimePointConstraint()
		 * @generated
		 */
		EClass TIME_POINT_CONSTRAINT = eINSTANCE.getTimePointConstraint();

		/**
		 * The meta object literal for the '<em><b>Pre Argument</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIME_POINT_CONSTRAINT__PRE_ARGUMENT = eINSTANCE.getTimePointConstraint_PreArgument();

		/**
		 * The meta object literal for the '<em><b>Keyword</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIME_POINT_CONSTRAINT__KEYWORD = eINSTANCE.getTimePointConstraint_Keyword();

		/**
		 * The meta object literal for the '<em><b>Post Argument</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIME_POINT_CONSTRAINT__POST_ARGUMENT = eINSTANCE.getTimePointConstraint_PostArgument();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.constraints.impl.TimeIntervalConstraintImpl <em>Time Interval Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.constraints.impl.TimeIntervalConstraintImpl
		 * @see de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl#getTimeIntervalConstraint()
		 * @generated
		 */
		EClass TIME_INTERVAL_CONSTRAINT = eINSTANCE.getTimeIntervalConstraint();

		/**
		 * The meta object literal for the '<em><b>Pre Argument</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIME_INTERVAL_CONSTRAINT__PRE_ARGUMENT = eINSTANCE.getTimeIntervalConstraint_PreArgument();

		/**
		 * The meta object literal for the '<em><b>Keyword</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIME_INTERVAL_CONSTRAINT__KEYWORD = eINSTANCE.getTimeIntervalConstraint_Keyword();

		/**
		 * The meta object literal for the '<em><b>Start Point</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIME_INTERVAL_CONSTRAINT__START_POINT = eINSTANCE.getTimeIntervalConstraint_StartPoint();

		/**
		 * The meta object literal for the '<em><b>End Point</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIME_INTERVAL_CONSTRAINT__END_POINT = eINSTANCE.getTimeIntervalConstraint_EndPoint();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.constraints.impl.EvolutionaryAbstractBuidingBlockImpl <em>Evolutionary Abstract Buiding Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.constraints.impl.EvolutionaryAbstractBuidingBlockImpl
		 * @see de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl#getEvolutionaryAbstractBuidingBlock()
		 * @generated
		 */
		EClass EVOLUTIONARY_ABSTRACT_BUIDING_BLOCK = eINSTANCE.getEvolutionaryAbstractBuidingBlock();

	}

} //ConstraintsPackage
