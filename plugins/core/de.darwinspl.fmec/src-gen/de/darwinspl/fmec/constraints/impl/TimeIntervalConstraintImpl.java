/**
 */
package de.darwinspl.fmec.constraints.impl;

import de.darwinspl.fmec.constraints.ConstraintsPackage;
import de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock;
import de.darwinspl.fmec.constraints.TimeIntervalConstraint;

import de.darwinspl.fmec.keywords.interval.IntervalKeyword;

import de.darwinspl.fmec.timepoint.TimePoint;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Time Interval Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.constraints.impl.TimeIntervalConstraintImpl#getPreArgument <em>Pre Argument</em>}</li>
 *   <li>{@link de.darwinspl.fmec.constraints.impl.TimeIntervalConstraintImpl#getKeyword <em>Keyword</em>}</li>
 *   <li>{@link de.darwinspl.fmec.constraints.impl.TimeIntervalConstraintImpl#getStartPoint <em>Start Point</em>}</li>
 *   <li>{@link de.darwinspl.fmec.constraints.impl.TimeIntervalConstraintImpl#getEndPoint <em>End Point</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimeIntervalConstraintImpl extends AtomicConstraintImpl implements TimeIntervalConstraint {
	/**
	 * The cached value of the '{@link #getPreArgument() <em>Pre Argument</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreArgument()
	 * @generated
	 * @ordered
	 */
	protected EvolutionaryAbstractBuidingBlock preArgument;

	/**
	 * The cached value of the '{@link #getKeyword() <em>Keyword</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKeyword()
	 * @generated
	 * @ordered
	 */
	protected IntervalKeyword keyword;

	/**
	 * The cached value of the '{@link #getStartPoint() <em>Start Point</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartPoint()
	 * @generated
	 * @ordered
	 */
	protected TimePoint startPoint;

	/**
	 * The cached value of the '{@link #getEndPoint() <em>End Point</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndPoint()
	 * @generated
	 * @ordered
	 */
	protected TimePoint endPoint;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimeIntervalConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConstraintsPackage.Literals.TIME_INTERVAL_CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EvolutionaryAbstractBuidingBlock getPreArgument() {
		return preArgument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPreArgument(EvolutionaryAbstractBuidingBlock newPreArgument, NotificationChain msgs) {
		EvolutionaryAbstractBuidingBlock oldPreArgument = preArgument;
		preArgument = newPreArgument;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__PRE_ARGUMENT, oldPreArgument, newPreArgument);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPreArgument(EvolutionaryAbstractBuidingBlock newPreArgument) {
		if (newPreArgument != preArgument) {
			NotificationChain msgs = null;
			if (preArgument != null)
				msgs = ((InternalEObject)preArgument).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__PRE_ARGUMENT, null, msgs);
			if (newPreArgument != null)
				msgs = ((InternalEObject)newPreArgument).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__PRE_ARGUMENT, null, msgs);
			msgs = basicSetPreArgument(newPreArgument, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__PRE_ARGUMENT, newPreArgument, newPreArgument));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IntervalKeyword getKeyword() {
		return keyword;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetKeyword(IntervalKeyword newKeyword, NotificationChain msgs) {
		IntervalKeyword oldKeyword = keyword;
		keyword = newKeyword;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__KEYWORD, oldKeyword, newKeyword);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setKeyword(IntervalKeyword newKeyword) {
		if (newKeyword != keyword) {
			NotificationChain msgs = null;
			if (keyword != null)
				msgs = ((InternalEObject)keyword).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__KEYWORD, null, msgs);
			if (newKeyword != null)
				msgs = ((InternalEObject)newKeyword).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__KEYWORD, null, msgs);
			msgs = basicSetKeyword(newKeyword, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__KEYWORD, newKeyword, newKeyword));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TimePoint getStartPoint() {
		return startPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStartPoint(TimePoint newStartPoint, NotificationChain msgs) {
		TimePoint oldStartPoint = startPoint;
		startPoint = newStartPoint;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__START_POINT, oldStartPoint, newStartPoint);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStartPoint(TimePoint newStartPoint) {
		if (newStartPoint != startPoint) {
			NotificationChain msgs = null;
			if (startPoint != null)
				msgs = ((InternalEObject)startPoint).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__START_POINT, null, msgs);
			if (newStartPoint != null)
				msgs = ((InternalEObject)newStartPoint).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__START_POINT, null, msgs);
			msgs = basicSetStartPoint(newStartPoint, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__START_POINT, newStartPoint, newStartPoint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TimePoint getEndPoint() {
		return endPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEndPoint(TimePoint newEndPoint, NotificationChain msgs) {
		TimePoint oldEndPoint = endPoint;
		endPoint = newEndPoint;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__END_POINT, oldEndPoint, newEndPoint);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEndPoint(TimePoint newEndPoint) {
		if (newEndPoint != endPoint) {
			NotificationChain msgs = null;
			if (endPoint != null)
				msgs = ((InternalEObject)endPoint).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__END_POINT, null, msgs);
			if (newEndPoint != null)
				msgs = ((InternalEObject)newEndPoint).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__END_POINT, null, msgs);
			msgs = basicSetEndPoint(newEndPoint, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__END_POINT, newEndPoint, newEndPoint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__PRE_ARGUMENT:
				return basicSetPreArgument(null, msgs);
			case ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__KEYWORD:
				return basicSetKeyword(null, msgs);
			case ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__START_POINT:
				return basicSetStartPoint(null, msgs);
			case ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__END_POINT:
				return basicSetEndPoint(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__PRE_ARGUMENT:
				return getPreArgument();
			case ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__KEYWORD:
				return getKeyword();
			case ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__START_POINT:
				return getStartPoint();
			case ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__END_POINT:
				return getEndPoint();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__PRE_ARGUMENT:
				setPreArgument((EvolutionaryAbstractBuidingBlock)newValue);
				return;
			case ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__KEYWORD:
				setKeyword((IntervalKeyword)newValue);
				return;
			case ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__START_POINT:
				setStartPoint((TimePoint)newValue);
				return;
			case ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__END_POINT:
				setEndPoint((TimePoint)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__PRE_ARGUMENT:
				setPreArgument((EvolutionaryAbstractBuidingBlock)null);
				return;
			case ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__KEYWORD:
				setKeyword((IntervalKeyword)null);
				return;
			case ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__START_POINT:
				setStartPoint((TimePoint)null);
				return;
			case ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__END_POINT:
				setEndPoint((TimePoint)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__PRE_ARGUMENT:
				return preArgument != null;
			case ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__KEYWORD:
				return keyword != null;
			case ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__START_POINT:
				return startPoint != null;
			case ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__END_POINT:
				return endPoint != null;
		}
		return super.eIsSet(featureID);
	}

} //TimeIntervalConstraintImpl
