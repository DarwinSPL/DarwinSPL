/**
 */
package de.darwinspl.fmec.constraints;

import de.darwinspl.fmec.keywords.logical.UnaryLogicalKeyword;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unary Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.constraints.UnaryConstraint#getChild <em>Child</em>}</li>
 *   <li>{@link de.darwinspl.fmec.constraints.UnaryConstraint#getKeyword <em>Keyword</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.fmec.constraints.ConstraintsPackage#getUnaryConstraint()
 * @model
 * @generated
 */
public interface UnaryConstraint extends BinaryConstraintChild {
	/**
	 * Returns the value of the '<em><b>Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Child</em>' containment reference.
	 * @see #setChild(UnaryConstraintChild)
	 * @see de.darwinspl.fmec.constraints.ConstraintsPackage#getUnaryConstraint_Child()
	 * @model containment="true" required="true"
	 * @generated
	 */
	UnaryConstraintChild getChild();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.constraints.UnaryConstraint#getChild <em>Child</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Child</em>' containment reference.
	 * @see #getChild()
	 * @generated
	 */
	void setChild(UnaryConstraintChild value);

	/**
	 * Returns the value of the '<em><b>Keyword</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Keyword</em>' containment reference.
	 * @see #setKeyword(UnaryLogicalKeyword)
	 * @see de.darwinspl.fmec.constraints.ConstraintsPackage#getUnaryConstraint_Keyword()
	 * @model containment="true" required="true"
	 * @generated
	 */
	UnaryLogicalKeyword getKeyword();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.constraints.UnaryConstraint#getKeyword <em>Keyword</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Keyword</em>' containment reference.
	 * @see #getKeyword()
	 * @generated
	 */
	void setKeyword(UnaryLogicalKeyword value);

} // UnaryConstraint
