/**
 */
package de.darwinspl.fmec.constraints;

import de.darwinspl.fmec.keywords.logical.BinaryLogicalKeyword;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.constraints.BinaryConstraint#getLeftChild <em>Left Child</em>}</li>
 *   <li>{@link de.darwinspl.fmec.constraints.BinaryConstraint#getKeyword <em>Keyword</em>}</li>
 *   <li>{@link de.darwinspl.fmec.constraints.BinaryConstraint#getRightChild <em>Right Child</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.fmec.constraints.ConstraintsPackage#getBinaryConstraint()
 * @model
 * @generated
 */
public interface BinaryConstraint extends FeatureModelEvolutionConstraint {
	/**
	 * Returns the value of the '<em><b>Left Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left Child</em>' containment reference.
	 * @see #setLeftChild(BinaryConstraintChild)
	 * @see de.darwinspl.fmec.constraints.ConstraintsPackage#getBinaryConstraint_LeftChild()
	 * @model containment="true" required="true"
	 * @generated
	 */
	BinaryConstraintChild getLeftChild();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.constraints.BinaryConstraint#getLeftChild <em>Left Child</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left Child</em>' containment reference.
	 * @see #getLeftChild()
	 * @generated
	 */
	void setLeftChild(BinaryConstraintChild value);

	/**
	 * Returns the value of the '<em><b>Keyword</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Keyword</em>' containment reference.
	 * @see #setKeyword(BinaryLogicalKeyword)
	 * @see de.darwinspl.fmec.constraints.ConstraintsPackage#getBinaryConstraint_Keyword()
	 * @model containment="true" required="true"
	 * @generated
	 */
	BinaryLogicalKeyword getKeyword();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.constraints.BinaryConstraint#getKeyword <em>Keyword</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Keyword</em>' containment reference.
	 * @see #getKeyword()
	 * @generated
	 */
	void setKeyword(BinaryLogicalKeyword value);

	/**
	 * Returns the value of the '<em><b>Right Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right Child</em>' containment reference.
	 * @see #setRightChild(BinaryConstraintChild)
	 * @see de.darwinspl.fmec.constraints.ConstraintsPackage#getBinaryConstraint_RightChild()
	 * @model containment="true" required="true"
	 * @generated
	 */
	BinaryConstraintChild getRightChild();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.constraints.BinaryConstraint#getRightChild <em>Right Child</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right Child</em>' containment reference.
	 * @see #getRightChild()
	 * @generated
	 */
	void setRightChild(BinaryConstraintChild value);

} // BinaryConstraint
