/**
 */
package de.darwinspl.fmec.constraints;

import de.darwinspl.fmec.keywords.timepoint.TimePointKeyword;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Time Point Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.constraints.TimePointConstraint#getPreArgument <em>Pre Argument</em>}</li>
 *   <li>{@link de.darwinspl.fmec.constraints.TimePointConstraint#getKeyword <em>Keyword</em>}</li>
 *   <li>{@link de.darwinspl.fmec.constraints.TimePointConstraint#getPostArgument <em>Post Argument</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.fmec.constraints.ConstraintsPackage#getTimePointConstraint()
 * @model
 * @generated
 */
public interface TimePointConstraint extends AtomicConstraint {
	/**
	 * Returns the value of the '<em><b>Pre Argument</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pre Argument</em>' containment reference.
	 * @see #setPreArgument(EvolutionaryAbstractBuidingBlock)
	 * @see de.darwinspl.fmec.constraints.ConstraintsPackage#getTimePointConstraint_PreArgument()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EvolutionaryAbstractBuidingBlock getPreArgument();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.constraints.TimePointConstraint#getPreArgument <em>Pre Argument</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pre Argument</em>' containment reference.
	 * @see #getPreArgument()
	 * @generated
	 */
	void setPreArgument(EvolutionaryAbstractBuidingBlock value);

	/**
	 * Returns the value of the '<em><b>Keyword</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Keyword</em>' containment reference.
	 * @see #setKeyword(TimePointKeyword)
	 * @see de.darwinspl.fmec.constraints.ConstraintsPackage#getTimePointConstraint_Keyword()
	 * @model containment="true" required="true"
	 * @generated
	 */
	TimePointKeyword getKeyword();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.constraints.TimePointConstraint#getKeyword <em>Keyword</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Keyword</em>' containment reference.
	 * @see #getKeyword()
	 * @generated
	 */
	void setKeyword(TimePointKeyword value);

	/**
	 * Returns the value of the '<em><b>Post Argument</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Post Argument</em>' containment reference.
	 * @see #setPostArgument(EvolutionaryAbstractBuidingBlock)
	 * @see de.darwinspl.fmec.constraints.ConstraintsPackage#getTimePointConstraint_PostArgument()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EvolutionaryAbstractBuidingBlock getPostArgument();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.constraints.TimePointConstraint#getPostArgument <em>Post Argument</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Post Argument</em>' containment reference.
	 * @see #getPostArgument()
	 * @generated
	 */
	void setPostArgument(EvolutionaryAbstractBuidingBlock value);

} // TimePointConstraint
