/**
 */
package de.darwinspl.fmec.property.impl;

import de.darwinspl.feature.DwFeature;

import de.darwinspl.fmec.property.FeatureExistenceProperty;
import de.darwinspl.fmec.property.FeatureReference;
import de.darwinspl.fmec.property.PropertyPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature Existence Property</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.property.impl.FeatureExistencePropertyImpl#getReferencedFeature <em>Referenced Feature</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FeatureExistencePropertyImpl extends PropertyImpl implements FeatureExistenceProperty {
	/**
	 * The cached value of the '{@link #getReferencedFeature() <em>Referenced Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencedFeature()
	 * @generated
	 * @ordered
	 */
	protected DwFeature referencedFeature;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureExistencePropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PropertyPackage.Literals.FEATURE_EXISTENCE_PROPERTY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeature getReferencedFeature() {
		if (referencedFeature != null && referencedFeature.eIsProxy()) {
			InternalEObject oldReferencedFeature = (InternalEObject)referencedFeature;
			referencedFeature = (DwFeature)eResolveProxy(oldReferencedFeature);
			if (referencedFeature != oldReferencedFeature) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PropertyPackage.FEATURE_EXISTENCE_PROPERTY__REFERENCED_FEATURE, oldReferencedFeature, referencedFeature));
			}
		}
		return referencedFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwFeature basicGetReferencedFeature() {
		return referencedFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setReferencedFeature(DwFeature newReferencedFeature) {
		DwFeature oldReferencedFeature = referencedFeature;
		referencedFeature = newReferencedFeature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PropertyPackage.FEATURE_EXISTENCE_PROPERTY__REFERENCED_FEATURE, oldReferencedFeature, referencedFeature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PropertyPackage.FEATURE_EXISTENCE_PROPERTY__REFERENCED_FEATURE:
				if (resolve) return getReferencedFeature();
				return basicGetReferencedFeature();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PropertyPackage.FEATURE_EXISTENCE_PROPERTY__REFERENCED_FEATURE:
				setReferencedFeature((DwFeature)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PropertyPackage.FEATURE_EXISTENCE_PROPERTY__REFERENCED_FEATURE:
				setReferencedFeature((DwFeature)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PropertyPackage.FEATURE_EXISTENCE_PROPERTY__REFERENCED_FEATURE:
				return referencedFeature != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == FeatureReference.class) {
			switch (derivedFeatureID) {
				case PropertyPackage.FEATURE_EXISTENCE_PROPERTY__REFERENCED_FEATURE: return PropertyPackage.FEATURE_REFERENCE__REFERENCED_FEATURE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == FeatureReference.class) {
			switch (baseFeatureID) {
				case PropertyPackage.FEATURE_REFERENCE__REFERENCED_FEATURE: return PropertyPackage.FEATURE_EXISTENCE_PROPERTY__REFERENCED_FEATURE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //FeatureExistencePropertyImpl
