/**
 */
package de.darwinspl.fmec.property.impl;

import de.darwinspl.fmec.property.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PropertyFactoryImpl extends EFactoryImpl implements PropertyFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static PropertyFactory init() {
		try {
			PropertyFactory thePropertyFactory = (PropertyFactory)EPackage.Registry.INSTANCE.getEFactory(PropertyPackage.eNS_URI);
			if (thePropertyFactory != null) {
				return thePropertyFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new PropertyFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case PropertyPackage.EVOLUTIONARY_PROPERTY: return createEvolutionaryProperty();
			case PropertyPackage.FEATURE_EXISTENCE_PROPERTY: return createFeatureExistenceProperty();
			case PropertyPackage.FEATURE_TYPE_PROPERTY: return createFeatureTypeProperty();
			case PropertyPackage.FEATURE_PARENT_PROPERTY: return createFeatureParentProperty();
			case PropertyPackage.GROUP_EXISTENCE_PROPERTY: return createGroupExistenceProperty();
			case PropertyPackage.GROUP_TYPE_PROPERTY: return createGroupTypeProperty();
			case PropertyPackage.GROUP_PARENT_PROPERTY: return createGroupParentProperty();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EvolutionaryProperty createEvolutionaryProperty() {
		EvolutionaryPropertyImpl evolutionaryProperty = new EvolutionaryPropertyImpl();
		return evolutionaryProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureExistenceProperty createFeatureExistenceProperty() {
		FeatureExistencePropertyImpl featureExistenceProperty = new FeatureExistencePropertyImpl();
		return featureExistenceProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureTypeProperty createFeatureTypeProperty() {
		FeatureTypePropertyImpl featureTypeProperty = new FeatureTypePropertyImpl();
		return featureTypeProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureParentProperty createFeatureParentProperty() {
		FeatureParentPropertyImpl featureParentProperty = new FeatureParentPropertyImpl();
		return featureParentProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GroupExistenceProperty createGroupExistenceProperty() {
		GroupExistencePropertyImpl groupExistenceProperty = new GroupExistencePropertyImpl();
		return groupExistenceProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GroupTypeProperty createGroupTypeProperty() {
		GroupTypePropertyImpl groupTypeProperty = new GroupTypePropertyImpl();
		return groupTypeProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GroupParentProperty createGroupParentProperty() {
		GroupParentPropertyImpl groupParentProperty = new GroupParentPropertyImpl();
		return groupParentProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PropertyPackage getPropertyPackage() {
		return (PropertyPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static PropertyPackage getPackage() {
		return PropertyPackage.eINSTANCE;
	}

} //PropertyFactoryImpl
