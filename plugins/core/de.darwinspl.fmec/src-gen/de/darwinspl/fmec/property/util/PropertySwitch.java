/**
 */
package de.darwinspl.fmec.property.util;

import de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock;

import de.darwinspl.fmec.property.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see de.darwinspl.fmec.property.PropertyPackage
 * @generated
 */
public class PropertySwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static PropertyPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertySwitch() {
		if (modelPackage == null) {
			modelPackage = PropertyPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case PropertyPackage.EVOLUTIONARY_PROPERTY: {
				EvolutionaryProperty evolutionaryProperty = (EvolutionaryProperty)theEObject;
				T result = caseEvolutionaryProperty(evolutionaryProperty);
				if (result == null) result = caseEvolutionaryAbstractBuidingBlock(evolutionaryProperty);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PropertyPackage.FEATURE_REFERENCE: {
				FeatureReference featureReference = (FeatureReference)theEObject;
				T result = caseFeatureReference(featureReference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PropertyPackage.GROUP_REFERENCE: {
				GroupReference groupReference = (GroupReference)theEObject;
				T result = caseGroupReference(groupReference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PropertyPackage.PROPERTY: {
				Property property = (Property)theEObject;
				T result = caseProperty(property);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PropertyPackage.FEATURE_EXISTENCE_PROPERTY: {
				FeatureExistenceProperty featureExistenceProperty = (FeatureExistenceProperty)theEObject;
				T result = caseFeatureExistenceProperty(featureExistenceProperty);
				if (result == null) result = caseProperty(featureExistenceProperty);
				if (result == null) result = caseFeatureReference(featureExistenceProperty);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PropertyPackage.FEATURE_TYPE_PROPERTY: {
				FeatureTypeProperty featureTypeProperty = (FeatureTypeProperty)theEObject;
				T result = caseFeatureTypeProperty(featureTypeProperty);
				if (result == null) result = caseProperty(featureTypeProperty);
				if (result == null) result = caseFeatureReference(featureTypeProperty);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PropertyPackage.FEATURE_PARENT_PROPERTY: {
				FeatureParentProperty featureParentProperty = (FeatureParentProperty)theEObject;
				T result = caseFeatureParentProperty(featureParentProperty);
				if (result == null) result = caseProperty(featureParentProperty);
				if (result == null) result = caseFeatureReference(featureParentProperty);
				if (result == null) result = caseGroupReference(featureParentProperty);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PropertyPackage.GROUP_EXISTENCE_PROPERTY: {
				GroupExistenceProperty groupExistenceProperty = (GroupExistenceProperty)theEObject;
				T result = caseGroupExistenceProperty(groupExistenceProperty);
				if (result == null) result = caseProperty(groupExistenceProperty);
				if (result == null) result = caseGroupReference(groupExistenceProperty);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PropertyPackage.GROUP_TYPE_PROPERTY: {
				GroupTypeProperty groupTypeProperty = (GroupTypeProperty)theEObject;
				T result = caseGroupTypeProperty(groupTypeProperty);
				if (result == null) result = caseProperty(groupTypeProperty);
				if (result == null) result = caseGroupReference(groupTypeProperty);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PropertyPackage.GROUP_PARENT_PROPERTY: {
				GroupParentProperty groupParentProperty = (GroupParentProperty)theEObject;
				T result = caseGroupParentProperty(groupParentProperty);
				if (result == null) result = caseProperty(groupParentProperty);
				if (result == null) result = caseGroupReference(groupParentProperty);
				if (result == null) result = caseFeatureReference(groupParentProperty);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Evolutionary Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Evolutionary Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEvolutionaryProperty(EvolutionaryProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeatureReference(FeatureReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Group Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Group Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGroupReference(GroupReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProperty(Property object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature Existence Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature Existence Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeatureExistenceProperty(FeatureExistenceProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature Type Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature Type Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeatureTypeProperty(FeatureTypeProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature Parent Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature Parent Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeatureParentProperty(FeatureParentProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Group Existence Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Group Existence Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGroupExistenceProperty(GroupExistenceProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Group Type Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Group Type Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGroupTypeProperty(GroupTypeProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Group Parent Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Group Parent Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGroupParentProperty(GroupParentProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Evolutionary Abstract Buiding Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Evolutionary Abstract Buiding Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEvolutionaryAbstractBuidingBlock(EvolutionaryAbstractBuidingBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //PropertySwitch
