/**
 */
package de.darwinspl.fmec.property.impl;

import de.darwinspl.feature.DwGroup;

import de.darwinspl.fmec.property.GroupReference;
import de.darwinspl.fmec.property.PropertyPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Group Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.property.impl.GroupReferenceImpl#getReferencedGroup <em>Referenced Group</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class GroupReferenceImpl extends MinimalEObjectImpl.Container implements GroupReference {
	/**
	 * The cached value of the '{@link #getReferencedGroup() <em>Referenced Group</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencedGroup()
	 * @generated
	 * @ordered
	 */
	protected DwGroup referencedGroup;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GroupReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PropertyPackage.Literals.GROUP_REFERENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroup getReferencedGroup() {
		if (referencedGroup != null && referencedGroup.eIsProxy()) {
			InternalEObject oldReferencedGroup = (InternalEObject)referencedGroup;
			referencedGroup = (DwGroup)eResolveProxy(oldReferencedGroup);
			if (referencedGroup != oldReferencedGroup) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PropertyPackage.GROUP_REFERENCE__REFERENCED_GROUP, oldReferencedGroup, referencedGroup));
			}
		}
		return referencedGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwGroup basicGetReferencedGroup() {
		return referencedGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setReferencedGroup(DwGroup newReferencedGroup) {
		DwGroup oldReferencedGroup = referencedGroup;
		referencedGroup = newReferencedGroup;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PropertyPackage.GROUP_REFERENCE__REFERENCED_GROUP, oldReferencedGroup, referencedGroup));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PropertyPackage.GROUP_REFERENCE__REFERENCED_GROUP:
				if (resolve) return getReferencedGroup();
				return basicGetReferencedGroup();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PropertyPackage.GROUP_REFERENCE__REFERENCED_GROUP:
				setReferencedGroup((DwGroup)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PropertyPackage.GROUP_REFERENCE__REFERENCED_GROUP:
				setReferencedGroup((DwGroup)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PropertyPackage.GROUP_REFERENCE__REFERENCED_GROUP:
				return referencedGroup != null;
		}
		return super.eIsSet(featureID);
	}

} //GroupReferenceImpl
