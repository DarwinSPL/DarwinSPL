/**
 */
package de.darwinspl.fmec.property;

import de.darwinspl.feature.DwFeatureTypeEnum;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Type Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.property.FeatureTypeProperty#getFeatureType <em>Feature Type</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.fmec.property.PropertyPackage#getFeatureTypeProperty()
 * @model
 * @generated
 */
public interface FeatureTypeProperty extends Property, FeatureReference {
	/**
	 * Returns the value of the '<em><b>Feature Type</b></em>' attribute.
	 * The literals are from the enumeration {@link de.darwinspl.feature.DwFeatureTypeEnum}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Type</em>' attribute.
	 * @see de.darwinspl.feature.DwFeatureTypeEnum
	 * @see #setFeatureType(DwFeatureTypeEnum)
	 * @see de.darwinspl.fmec.property.PropertyPackage#getFeatureTypeProperty_FeatureType()
	 * @model required="true"
	 * @generated
	 */
	DwFeatureTypeEnum getFeatureType();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.property.FeatureTypeProperty#getFeatureType <em>Feature Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature Type</em>' attribute.
	 * @see de.darwinspl.feature.DwFeatureTypeEnum
	 * @see #getFeatureType()
	 * @generated
	 */
	void setFeatureType(DwFeatureTypeEnum value);

} // FeatureTypeProperty
