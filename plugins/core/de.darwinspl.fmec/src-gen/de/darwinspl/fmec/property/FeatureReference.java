/**
 */
package de.darwinspl.fmec.property;

import de.darwinspl.feature.DwFeature;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.property.FeatureReference#getReferencedFeature <em>Referenced Feature</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.fmec.property.PropertyPackage#getFeatureReference()
 * @model abstract="true"
 * @generated
 */
public interface FeatureReference extends EObject {
	/**
	 * Returns the value of the '<em><b>Referenced Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced Feature</em>' reference.
	 * @see #setReferencedFeature(DwFeature)
	 * @see de.darwinspl.fmec.property.PropertyPackage#getFeatureReference_ReferencedFeature()
	 * @model required="true"
	 * @generated
	 */
	DwFeature getReferencedFeature();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.property.FeatureReference#getReferencedFeature <em>Referenced Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Referenced Feature</em>' reference.
	 * @see #getReferencedFeature()
	 * @generated
	 */
	void setReferencedFeature(DwFeature value);

} // FeatureReference
