/**
 */
package de.darwinspl.fmec.property.impl;

import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupTypeEnum;

import de.darwinspl.fmec.property.GroupReference;
import de.darwinspl.fmec.property.GroupTypeProperty;
import de.darwinspl.fmec.property.PropertyPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Group Type Property</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.property.impl.GroupTypePropertyImpl#getReferencedGroup <em>Referenced Group</em>}</li>
 *   <li>{@link de.darwinspl.fmec.property.impl.GroupTypePropertyImpl#getGroupType <em>Group Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GroupTypePropertyImpl extends PropertyImpl implements GroupTypeProperty {
	/**
	 * The cached value of the '{@link #getReferencedGroup() <em>Referenced Group</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencedGroup()
	 * @generated
	 * @ordered
	 */
	protected DwGroup referencedGroup;

	/**
	 * The default value of the '{@link #getGroupType() <em>Group Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroupType()
	 * @generated
	 * @ordered
	 */
	protected static final DwGroupTypeEnum GROUP_TYPE_EDEFAULT = DwGroupTypeEnum.AND;

	/**
	 * The cached value of the '{@link #getGroupType() <em>Group Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroupType()
	 * @generated
	 * @ordered
	 */
	protected DwGroupTypeEnum groupType = GROUP_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GroupTypePropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PropertyPackage.Literals.GROUP_TYPE_PROPERTY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroup getReferencedGroup() {
		if (referencedGroup != null && referencedGroup.eIsProxy()) {
			InternalEObject oldReferencedGroup = (InternalEObject)referencedGroup;
			referencedGroup = (DwGroup)eResolveProxy(oldReferencedGroup);
			if (referencedGroup != oldReferencedGroup) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PropertyPackage.GROUP_TYPE_PROPERTY__REFERENCED_GROUP, oldReferencedGroup, referencedGroup));
			}
		}
		return referencedGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwGroup basicGetReferencedGroup() {
		return referencedGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setReferencedGroup(DwGroup newReferencedGroup) {
		DwGroup oldReferencedGroup = referencedGroup;
		referencedGroup = newReferencedGroup;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PropertyPackage.GROUP_TYPE_PROPERTY__REFERENCED_GROUP, oldReferencedGroup, referencedGroup));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroupTypeEnum getGroupType() {
		return groupType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setGroupType(DwGroupTypeEnum newGroupType) {
		DwGroupTypeEnum oldGroupType = groupType;
		groupType = newGroupType == null ? GROUP_TYPE_EDEFAULT : newGroupType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PropertyPackage.GROUP_TYPE_PROPERTY__GROUP_TYPE, oldGroupType, groupType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PropertyPackage.GROUP_TYPE_PROPERTY__REFERENCED_GROUP:
				if (resolve) return getReferencedGroup();
				return basicGetReferencedGroup();
			case PropertyPackage.GROUP_TYPE_PROPERTY__GROUP_TYPE:
				return getGroupType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PropertyPackage.GROUP_TYPE_PROPERTY__REFERENCED_GROUP:
				setReferencedGroup((DwGroup)newValue);
				return;
			case PropertyPackage.GROUP_TYPE_PROPERTY__GROUP_TYPE:
				setGroupType((DwGroupTypeEnum)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PropertyPackage.GROUP_TYPE_PROPERTY__REFERENCED_GROUP:
				setReferencedGroup((DwGroup)null);
				return;
			case PropertyPackage.GROUP_TYPE_PROPERTY__GROUP_TYPE:
				setGroupType(GROUP_TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PropertyPackage.GROUP_TYPE_PROPERTY__REFERENCED_GROUP:
				return referencedGroup != null;
			case PropertyPackage.GROUP_TYPE_PROPERTY__GROUP_TYPE:
				return groupType != GROUP_TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == GroupReference.class) {
			switch (derivedFeatureID) {
				case PropertyPackage.GROUP_TYPE_PROPERTY__REFERENCED_GROUP: return PropertyPackage.GROUP_REFERENCE__REFERENCED_GROUP;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == GroupReference.class) {
			switch (baseFeatureID) {
				case PropertyPackage.GROUP_REFERENCE__REFERENCED_GROUP: return PropertyPackage.GROUP_TYPE_PROPERTY__REFERENCED_GROUP;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (groupType: ");
		result.append(groupType);
		result.append(')');
		return result.toString();
	}

} //GroupTypePropertyImpl
