/**
 */
package de.darwinspl.fmec.property;

import de.darwinspl.fmec.constraints.ConstraintsPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.darwinspl.fmec.property.PropertyFactory
 * @model kind="package"
 * @generated
 */
public interface PropertyPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "property";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.darwinspl.de/feature-model-evolution-constraints/property/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "property";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PropertyPackage eINSTANCE = de.darwinspl.fmec.property.impl.PropertyPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.property.impl.EvolutionaryPropertyImpl <em>Evolutionary Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.property.impl.EvolutionaryPropertyImpl
	 * @see de.darwinspl.fmec.property.impl.PropertyPackageImpl#getEvolutionaryProperty()
	 * @generated
	 */
	int EVOLUTIONARY_PROPERTY = 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVOLUTIONARY_PROPERTY__PROPERTY = ConstraintsPackage.EVOLUTIONARY_ABSTRACT_BUIDING_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Keyword</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVOLUTIONARY_PROPERTY__KEYWORD = ConstraintsPackage.EVOLUTIONARY_ABSTRACT_BUIDING_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Evolutionary Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVOLUTIONARY_PROPERTY_FEATURE_COUNT = ConstraintsPackage.EVOLUTIONARY_ABSTRACT_BUIDING_BLOCK_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Evolutionary Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVOLUTIONARY_PROPERTY_OPERATION_COUNT = ConstraintsPackage.EVOLUTIONARY_ABSTRACT_BUIDING_BLOCK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.property.impl.FeatureReferenceImpl <em>Feature Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.property.impl.FeatureReferenceImpl
	 * @see de.darwinspl.fmec.property.impl.PropertyPackageImpl#getFeatureReference()
	 * @generated
	 */
	int FEATURE_REFERENCE = 1;

	/**
	 * The feature id for the '<em><b>Referenced Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_REFERENCE__REFERENCED_FEATURE = 0;

	/**
	 * The number of structural features of the '<em>Feature Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_REFERENCE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Feature Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_REFERENCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.property.impl.GroupReferenceImpl <em>Group Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.property.impl.GroupReferenceImpl
	 * @see de.darwinspl.fmec.property.impl.PropertyPackageImpl#getGroupReference()
	 * @generated
	 */
	int GROUP_REFERENCE = 2;

	/**
	 * The feature id for the '<em><b>Referenced Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_REFERENCE__REFERENCED_GROUP = 0;

	/**
	 * The number of structural features of the '<em>Group Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_REFERENCE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Group Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_REFERENCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.property.impl.PropertyImpl <em>Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.property.impl.PropertyImpl
	 * @see de.darwinspl.fmec.property.impl.PropertyPackageImpl#getProperty()
	 * @generated
	 */
	int PROPERTY = 3;

	/**
	 * The number of structural features of the '<em>Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.property.impl.FeatureExistencePropertyImpl <em>Feature Existence Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.property.impl.FeatureExistencePropertyImpl
	 * @see de.darwinspl.fmec.property.impl.PropertyPackageImpl#getFeatureExistenceProperty()
	 * @generated
	 */
	int FEATURE_EXISTENCE_PROPERTY = 4;

	/**
	 * The feature id for the '<em><b>Referenced Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_EXISTENCE_PROPERTY__REFERENCED_FEATURE = PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Feature Existence Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_EXISTENCE_PROPERTY_FEATURE_COUNT = PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Feature Existence Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_EXISTENCE_PROPERTY_OPERATION_COUNT = PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.property.impl.FeatureTypePropertyImpl <em>Feature Type Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.property.impl.FeatureTypePropertyImpl
	 * @see de.darwinspl.fmec.property.impl.PropertyPackageImpl#getFeatureTypeProperty()
	 * @generated
	 */
	int FEATURE_TYPE_PROPERTY = 5;

	/**
	 * The feature id for the '<em><b>Referenced Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TYPE_PROPERTY__REFERENCED_FEATURE = PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Feature Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TYPE_PROPERTY__FEATURE_TYPE = PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Feature Type Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TYPE_PROPERTY_FEATURE_COUNT = PROPERTY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Feature Type Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TYPE_PROPERTY_OPERATION_COUNT = PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.property.impl.FeatureParentPropertyImpl <em>Feature Parent Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.property.impl.FeatureParentPropertyImpl
	 * @see de.darwinspl.fmec.property.impl.PropertyPackageImpl#getFeatureParentProperty()
	 * @generated
	 */
	int FEATURE_PARENT_PROPERTY = 6;

	/**
	 * The feature id for the '<em><b>Referenced Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_PARENT_PROPERTY__REFERENCED_FEATURE = PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Referenced Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_PARENT_PROPERTY__REFERENCED_GROUP = PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Feature Parent Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_PARENT_PROPERTY_FEATURE_COUNT = PROPERTY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Feature Parent Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_PARENT_PROPERTY_OPERATION_COUNT = PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.property.impl.GroupExistencePropertyImpl <em>Group Existence Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.property.impl.GroupExistencePropertyImpl
	 * @see de.darwinspl.fmec.property.impl.PropertyPackageImpl#getGroupExistenceProperty()
	 * @generated
	 */
	int GROUP_EXISTENCE_PROPERTY = 7;

	/**
	 * The feature id for the '<em><b>Referenced Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_EXISTENCE_PROPERTY__REFERENCED_GROUP = PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Group Existence Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_EXISTENCE_PROPERTY_FEATURE_COUNT = PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Group Existence Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_EXISTENCE_PROPERTY_OPERATION_COUNT = PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.property.impl.GroupTypePropertyImpl <em>Group Type Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.property.impl.GroupTypePropertyImpl
	 * @see de.darwinspl.fmec.property.impl.PropertyPackageImpl#getGroupTypeProperty()
	 * @generated
	 */
	int GROUP_TYPE_PROPERTY = 8;

	/**
	 * The feature id for the '<em><b>Referenced Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_TYPE_PROPERTY__REFERENCED_GROUP = PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Group Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_TYPE_PROPERTY__GROUP_TYPE = PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Group Type Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_TYPE_PROPERTY_FEATURE_COUNT = PROPERTY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Group Type Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_TYPE_PROPERTY_OPERATION_COUNT = PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.property.impl.GroupParentPropertyImpl <em>Group Parent Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.property.impl.GroupParentPropertyImpl
	 * @see de.darwinspl.fmec.property.impl.PropertyPackageImpl#getGroupParentProperty()
	 * @generated
	 */
	int GROUP_PARENT_PROPERTY = 9;

	/**
	 * The feature id for the '<em><b>Referenced Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_PARENT_PROPERTY__REFERENCED_GROUP = PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Referenced Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_PARENT_PROPERTY__REFERENCED_FEATURE = PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Group Parent Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_PARENT_PROPERTY_FEATURE_COUNT = PROPERTY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Group Parent Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_PARENT_PROPERTY_OPERATION_COUNT = PROPERTY_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.property.EvolutionaryProperty <em>Evolutionary Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Evolutionary Property</em>'.
	 * @see de.darwinspl.fmec.property.EvolutionaryProperty
	 * @generated
	 */
	EClass getEvolutionaryProperty();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.fmec.property.EvolutionaryProperty#getProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Property</em>'.
	 * @see de.darwinspl.fmec.property.EvolutionaryProperty#getProperty()
	 * @see #getEvolutionaryProperty()
	 * @generated
	 */
	EReference getEvolutionaryProperty_Property();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.fmec.property.EvolutionaryProperty#getKeyword <em>Keyword</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Keyword</em>'.
	 * @see de.darwinspl.fmec.property.EvolutionaryProperty#getKeyword()
	 * @see #getEvolutionaryProperty()
	 * @generated
	 */
	EReference getEvolutionaryProperty_Keyword();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.property.FeatureReference <em>Feature Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Reference</em>'.
	 * @see de.darwinspl.fmec.property.FeatureReference
	 * @generated
	 */
	EClass getFeatureReference();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.fmec.property.FeatureReference#getReferencedFeature <em>Referenced Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Referenced Feature</em>'.
	 * @see de.darwinspl.fmec.property.FeatureReference#getReferencedFeature()
	 * @see #getFeatureReference()
	 * @generated
	 */
	EReference getFeatureReference_ReferencedFeature();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.property.GroupReference <em>Group Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Group Reference</em>'.
	 * @see de.darwinspl.fmec.property.GroupReference
	 * @generated
	 */
	EClass getGroupReference();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.fmec.property.GroupReference#getReferencedGroup <em>Referenced Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Referenced Group</em>'.
	 * @see de.darwinspl.fmec.property.GroupReference#getReferencedGroup()
	 * @see #getGroupReference()
	 * @generated
	 */
	EReference getGroupReference_ReferencedGroup();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.property.Property <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property</em>'.
	 * @see de.darwinspl.fmec.property.Property
	 * @generated
	 */
	EClass getProperty();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.property.FeatureExistenceProperty <em>Feature Existence Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Existence Property</em>'.
	 * @see de.darwinspl.fmec.property.FeatureExistenceProperty
	 * @generated
	 */
	EClass getFeatureExistenceProperty();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.property.FeatureTypeProperty <em>Feature Type Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Type Property</em>'.
	 * @see de.darwinspl.fmec.property.FeatureTypeProperty
	 * @generated
	 */
	EClass getFeatureTypeProperty();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.fmec.property.FeatureTypeProperty#getFeatureType <em>Feature Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Feature Type</em>'.
	 * @see de.darwinspl.fmec.property.FeatureTypeProperty#getFeatureType()
	 * @see #getFeatureTypeProperty()
	 * @generated
	 */
	EAttribute getFeatureTypeProperty_FeatureType();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.property.FeatureParentProperty <em>Feature Parent Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Parent Property</em>'.
	 * @see de.darwinspl.fmec.property.FeatureParentProperty
	 * @generated
	 */
	EClass getFeatureParentProperty();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.property.GroupExistenceProperty <em>Group Existence Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Group Existence Property</em>'.
	 * @see de.darwinspl.fmec.property.GroupExistenceProperty
	 * @generated
	 */
	EClass getGroupExistenceProperty();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.property.GroupTypeProperty <em>Group Type Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Group Type Property</em>'.
	 * @see de.darwinspl.fmec.property.GroupTypeProperty
	 * @generated
	 */
	EClass getGroupTypeProperty();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.fmec.property.GroupTypeProperty#getGroupType <em>Group Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Group Type</em>'.
	 * @see de.darwinspl.fmec.property.GroupTypeProperty#getGroupType()
	 * @see #getGroupTypeProperty()
	 * @generated
	 */
	EAttribute getGroupTypeProperty_GroupType();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.property.GroupParentProperty <em>Group Parent Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Group Parent Property</em>'.
	 * @see de.darwinspl.fmec.property.GroupParentProperty
	 * @generated
	 */
	EClass getGroupParentProperty();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	PropertyFactory getPropertyFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.property.impl.EvolutionaryPropertyImpl <em>Evolutionary Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.property.impl.EvolutionaryPropertyImpl
		 * @see de.darwinspl.fmec.property.impl.PropertyPackageImpl#getEvolutionaryProperty()
		 * @generated
		 */
		EClass EVOLUTIONARY_PROPERTY = eINSTANCE.getEvolutionaryProperty();

		/**
		 * The meta object literal for the '<em><b>Property</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVOLUTIONARY_PROPERTY__PROPERTY = eINSTANCE.getEvolutionaryProperty_Property();

		/**
		 * The meta object literal for the '<em><b>Keyword</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVOLUTIONARY_PROPERTY__KEYWORD = eINSTANCE.getEvolutionaryProperty_Keyword();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.property.impl.FeatureReferenceImpl <em>Feature Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.property.impl.FeatureReferenceImpl
		 * @see de.darwinspl.fmec.property.impl.PropertyPackageImpl#getFeatureReference()
		 * @generated
		 */
		EClass FEATURE_REFERENCE = eINSTANCE.getFeatureReference();

		/**
		 * The meta object literal for the '<em><b>Referenced Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_REFERENCE__REFERENCED_FEATURE = eINSTANCE.getFeatureReference_ReferencedFeature();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.property.impl.GroupReferenceImpl <em>Group Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.property.impl.GroupReferenceImpl
		 * @see de.darwinspl.fmec.property.impl.PropertyPackageImpl#getGroupReference()
		 * @generated
		 */
		EClass GROUP_REFERENCE = eINSTANCE.getGroupReference();

		/**
		 * The meta object literal for the '<em><b>Referenced Group</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GROUP_REFERENCE__REFERENCED_GROUP = eINSTANCE.getGroupReference_ReferencedGroup();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.property.impl.PropertyImpl <em>Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.property.impl.PropertyImpl
		 * @see de.darwinspl.fmec.property.impl.PropertyPackageImpl#getProperty()
		 * @generated
		 */
		EClass PROPERTY = eINSTANCE.getProperty();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.property.impl.FeatureExistencePropertyImpl <em>Feature Existence Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.property.impl.FeatureExistencePropertyImpl
		 * @see de.darwinspl.fmec.property.impl.PropertyPackageImpl#getFeatureExistenceProperty()
		 * @generated
		 */
		EClass FEATURE_EXISTENCE_PROPERTY = eINSTANCE.getFeatureExistenceProperty();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.property.impl.FeatureTypePropertyImpl <em>Feature Type Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.property.impl.FeatureTypePropertyImpl
		 * @see de.darwinspl.fmec.property.impl.PropertyPackageImpl#getFeatureTypeProperty()
		 * @generated
		 */
		EClass FEATURE_TYPE_PROPERTY = eINSTANCE.getFeatureTypeProperty();

		/**
		 * The meta object literal for the '<em><b>Feature Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_TYPE_PROPERTY__FEATURE_TYPE = eINSTANCE.getFeatureTypeProperty_FeatureType();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.property.impl.FeatureParentPropertyImpl <em>Feature Parent Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.property.impl.FeatureParentPropertyImpl
		 * @see de.darwinspl.fmec.property.impl.PropertyPackageImpl#getFeatureParentProperty()
		 * @generated
		 */
		EClass FEATURE_PARENT_PROPERTY = eINSTANCE.getFeatureParentProperty();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.property.impl.GroupExistencePropertyImpl <em>Group Existence Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.property.impl.GroupExistencePropertyImpl
		 * @see de.darwinspl.fmec.property.impl.PropertyPackageImpl#getGroupExistenceProperty()
		 * @generated
		 */
		EClass GROUP_EXISTENCE_PROPERTY = eINSTANCE.getGroupExistenceProperty();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.property.impl.GroupTypePropertyImpl <em>Group Type Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.property.impl.GroupTypePropertyImpl
		 * @see de.darwinspl.fmec.property.impl.PropertyPackageImpl#getGroupTypeProperty()
		 * @generated
		 */
		EClass GROUP_TYPE_PROPERTY = eINSTANCE.getGroupTypeProperty();

		/**
		 * The meta object literal for the '<em><b>Group Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GROUP_TYPE_PROPERTY__GROUP_TYPE = eINSTANCE.getGroupTypeProperty_GroupType();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.property.impl.GroupParentPropertyImpl <em>Group Parent Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.property.impl.GroupParentPropertyImpl
		 * @see de.darwinspl.fmec.property.impl.PropertyPackageImpl#getGroupParentProperty()
		 * @generated
		 */
		EClass GROUP_PARENT_PROPERTY = eINSTANCE.getGroupParentProperty();

	}

} //PropertyPackage
