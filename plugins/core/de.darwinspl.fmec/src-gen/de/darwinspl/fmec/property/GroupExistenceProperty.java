/**
 */
package de.darwinspl.fmec.property;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Group Existence Property</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.property.PropertyPackage#getGroupExistenceProperty()
 * @model
 * @generated
 */
public interface GroupExistenceProperty extends Property, GroupReference {
} // GroupExistenceProperty
