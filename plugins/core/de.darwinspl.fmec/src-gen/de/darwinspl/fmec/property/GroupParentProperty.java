/**
 */
package de.darwinspl.fmec.property;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Group Parent Property</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.property.PropertyPackage#getGroupParentProperty()
 * @model
 * @generated
 */
public interface GroupParentProperty extends Property, GroupReference, FeatureReference {
} // GroupParentProperty
