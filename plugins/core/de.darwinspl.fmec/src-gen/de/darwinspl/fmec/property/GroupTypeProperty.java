/**
 */
package de.darwinspl.fmec.property;

import de.darwinspl.feature.DwGroupTypeEnum;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Group Type Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.property.GroupTypeProperty#getGroupType <em>Group Type</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.fmec.property.PropertyPackage#getGroupTypeProperty()
 * @model
 * @generated
 */
public interface GroupTypeProperty extends Property, GroupReference {
	/**
	 * Returns the value of the '<em><b>Group Type</b></em>' attribute.
	 * The literals are from the enumeration {@link de.darwinspl.feature.DwGroupTypeEnum}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group Type</em>' attribute.
	 * @see de.darwinspl.feature.DwGroupTypeEnum
	 * @see #setGroupType(DwGroupTypeEnum)
	 * @see de.darwinspl.fmec.property.PropertyPackage#getGroupTypeProperty_GroupType()
	 * @model required="true"
	 * @generated
	 */
	DwGroupTypeEnum getGroupType();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.property.GroupTypeProperty#getGroupType <em>Group Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group Type</em>' attribute.
	 * @see de.darwinspl.feature.DwGroupTypeEnum
	 * @see #getGroupType()
	 * @generated
	 */
	void setGroupType(DwGroupTypeEnum value);

} // GroupTypeProperty
