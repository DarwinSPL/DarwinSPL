/**
 */
package de.darwinspl.fmec.property;

import de.darwinspl.feature.DwGroup;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Group Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.property.GroupReference#getReferencedGroup <em>Referenced Group</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.fmec.property.PropertyPackage#getGroupReference()
 * @model abstract="true"
 * @generated
 */
public interface GroupReference extends EObject {
	/**
	 * Returns the value of the '<em><b>Referenced Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced Group</em>' reference.
	 * @see #setReferencedGroup(DwGroup)
	 * @see de.darwinspl.fmec.property.PropertyPackage#getGroupReference_ReferencedGroup()
	 * @model required="true"
	 * @generated
	 */
	DwGroup getReferencedGroup();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.property.GroupReference#getReferencedGroup <em>Referenced Group</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Referenced Group</em>' reference.
	 * @see #getReferencedGroup()
	 * @generated
	 */
	void setReferencedGroup(DwGroup value);

} // GroupReference
