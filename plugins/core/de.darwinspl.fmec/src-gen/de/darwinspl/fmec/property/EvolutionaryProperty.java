/**
 */
package de.darwinspl.fmec.property;

import de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock;

import de.darwinspl.fmec.keywords.buildingblocks.PropertyKeyword;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Evolutionary Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.property.EvolutionaryProperty#getProperty <em>Property</em>}</li>
 *   <li>{@link de.darwinspl.fmec.property.EvolutionaryProperty#getKeyword <em>Keyword</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.fmec.property.PropertyPackage#getEvolutionaryProperty()
 * @model
 * @generated
 */
public interface EvolutionaryProperty extends EvolutionaryAbstractBuidingBlock {
	/**
	 * Returns the value of the '<em><b>Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property</em>' containment reference.
	 * @see #setProperty(Property)
	 * @see de.darwinspl.fmec.property.PropertyPackage#getEvolutionaryProperty_Property()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Property getProperty();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.property.EvolutionaryProperty#getProperty <em>Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property</em>' containment reference.
	 * @see #getProperty()
	 * @generated
	 */
	void setProperty(Property value);

	/**
	 * Returns the value of the '<em><b>Keyword</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Keyword</em>' containment reference.
	 * @see #setKeyword(PropertyKeyword)
	 * @see de.darwinspl.fmec.property.PropertyPackage#getEvolutionaryProperty_Keyword()
	 * @model containment="true" required="true"
	 * @generated
	 */
	PropertyKeyword getKeyword();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.property.EvolutionaryProperty#getKeyword <em>Keyword</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Keyword</em>' containment reference.
	 * @see #getKeyword()
	 * @generated
	 */
	void setKeyword(PropertyKeyword value);

} // EvolutionaryProperty
