/**
 */
package de.darwinspl.fmec.property;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Existence Property</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.property.PropertyPackage#getFeatureExistenceProperty()
 * @model
 * @generated
 */
public interface FeatureExistenceProperty extends Property, FeatureReference {
} // FeatureExistenceProperty
