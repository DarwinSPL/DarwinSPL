/**
 */
package de.darwinspl.fmec.property.impl;

import de.darwinspl.datatypes.DwDatatypesPackage;

import de.darwinspl.feature.DwFeaturePackage;

import de.darwinspl.fmec.FmecPackage;

import de.darwinspl.fmec.constraints.ConstraintsPackage;

import de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl;

import de.darwinspl.fmec.impl.FmecPackageImpl;

import de.darwinspl.fmec.keywords.KeywordsPackage;

import de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage;

import de.darwinspl.fmec.keywords.buildingblocks.impl.BuildingblocksPackageImpl;

import de.darwinspl.fmec.keywords.impl.KeywordsPackageImpl;

import de.darwinspl.fmec.keywords.interval.IntervalPackage;

import de.darwinspl.fmec.keywords.interval.impl.IntervalPackageImpl;

import de.darwinspl.fmec.keywords.logical.LogicalPackage;

import de.darwinspl.fmec.keywords.logical.impl.LogicalPackageImpl;

import de.darwinspl.fmec.property.EvolutionaryProperty;
import de.darwinspl.fmec.property.FeatureExistenceProperty;
import de.darwinspl.fmec.property.FeatureParentProperty;
import de.darwinspl.fmec.property.FeatureReference;
import de.darwinspl.fmec.property.FeatureTypeProperty;
import de.darwinspl.fmec.property.GroupExistenceProperty;
import de.darwinspl.fmec.property.GroupParentProperty;
import de.darwinspl.fmec.property.GroupReference;
import de.darwinspl.fmec.property.GroupTypeProperty;
import de.darwinspl.fmec.property.Property;
import de.darwinspl.fmec.property.PropertyFactory;
import de.darwinspl.fmec.property.PropertyPackage;

import de.darwinspl.fmec.timepoint.TimepointPackage;

import de.darwinspl.fmec.timepoint.impl.TimepointPackageImpl;

import de.darwinspl.temporal.DwTemporalModelPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PropertyPackageImpl extends EPackageImpl implements PropertyPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass evolutionaryPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass groupReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureExistencePropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureTypePropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureParentPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass groupExistencePropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass groupTypePropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass groupParentPropertyEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.darwinspl.fmec.property.PropertyPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private PropertyPackageImpl() {
		super(eNS_URI, PropertyFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link PropertyPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static PropertyPackage init() {
		if (isInited) return (PropertyPackage)EPackage.Registry.INSTANCE.getEPackage(PropertyPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredPropertyPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		PropertyPackageImpl thePropertyPackage = registeredPropertyPackage instanceof PropertyPackageImpl ? (PropertyPackageImpl)registeredPropertyPackage : new PropertyPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		DwDatatypesPackage.eINSTANCE.eClass();
		DwFeaturePackage.eINSTANCE.eClass();
		DwTemporalModelPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(FmecPackage.eNS_URI);
		FmecPackageImpl theFmecPackage = (FmecPackageImpl)(registeredPackage instanceof FmecPackageImpl ? registeredPackage : FmecPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ConstraintsPackage.eNS_URI);
		ConstraintsPackageImpl theConstraintsPackage = (ConstraintsPackageImpl)(registeredPackage instanceof ConstraintsPackageImpl ? registeredPackage : ConstraintsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimepointPackage.eNS_URI);
		TimepointPackageImpl theTimepointPackage = (TimepointPackageImpl)(registeredPackage instanceof TimepointPackageImpl ? registeredPackage : TimepointPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(KeywordsPackage.eNS_URI);
		KeywordsPackageImpl theKeywordsPackage = (KeywordsPackageImpl)(registeredPackage instanceof KeywordsPackageImpl ? registeredPackage : KeywordsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(LogicalPackage.eNS_URI);
		LogicalPackageImpl theLogicalPackage = (LogicalPackageImpl)(registeredPackage instanceof LogicalPackageImpl ? registeredPackage : LogicalPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(BuildingblocksPackage.eNS_URI);
		BuildingblocksPackageImpl theBuildingblocksPackage = (BuildingblocksPackageImpl)(registeredPackage instanceof BuildingblocksPackageImpl ? registeredPackage : BuildingblocksPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(de.darwinspl.fmec.keywords.timepoint.TimepointPackage.eNS_URI);
		de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl theTimepointPackage_1 = (de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl)(registeredPackage instanceof de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl ? registeredPackage : de.darwinspl.fmec.keywords.timepoint.TimepointPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(IntervalPackage.eNS_URI);
		IntervalPackageImpl theIntervalPackage = (IntervalPackageImpl)(registeredPackage instanceof IntervalPackageImpl ? registeredPackage : IntervalPackage.eINSTANCE);

		// Create package meta-data objects
		thePropertyPackage.createPackageContents();
		theFmecPackage.createPackageContents();
		theConstraintsPackage.createPackageContents();
		theTimepointPackage.createPackageContents();
		theKeywordsPackage.createPackageContents();
		theLogicalPackage.createPackageContents();
		theBuildingblocksPackage.createPackageContents();
		theTimepointPackage_1.createPackageContents();
		theIntervalPackage.createPackageContents();

		// Initialize created meta-data
		thePropertyPackage.initializePackageContents();
		theFmecPackage.initializePackageContents();
		theConstraintsPackage.initializePackageContents();
		theTimepointPackage.initializePackageContents();
		theKeywordsPackage.initializePackageContents();
		theLogicalPackage.initializePackageContents();
		theBuildingblocksPackage.initializePackageContents();
		theTimepointPackage_1.initializePackageContents();
		theIntervalPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		thePropertyPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(PropertyPackage.eNS_URI, thePropertyPackage);
		return thePropertyPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEvolutionaryProperty() {
		return evolutionaryPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getEvolutionaryProperty_Property() {
		return (EReference)evolutionaryPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getEvolutionaryProperty_Keyword() {
		return (EReference)evolutionaryPropertyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFeatureReference() {
		return featureReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFeatureReference_ReferencedFeature() {
		return (EReference)featureReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGroupReference() {
		return groupReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getGroupReference_ReferencedGroup() {
		return (EReference)groupReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getProperty() {
		return propertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFeatureExistenceProperty() {
		return featureExistencePropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFeatureTypeProperty() {
		return featureTypePropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFeatureTypeProperty_FeatureType() {
		return (EAttribute)featureTypePropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFeatureParentProperty() {
		return featureParentPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGroupExistenceProperty() {
		return groupExistencePropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGroupTypeProperty() {
		return groupTypePropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGroupTypeProperty_GroupType() {
		return (EAttribute)groupTypePropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGroupParentProperty() {
		return groupParentPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PropertyFactory getPropertyFactory() {
		return (PropertyFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		evolutionaryPropertyEClass = createEClass(EVOLUTIONARY_PROPERTY);
		createEReference(evolutionaryPropertyEClass, EVOLUTIONARY_PROPERTY__PROPERTY);
		createEReference(evolutionaryPropertyEClass, EVOLUTIONARY_PROPERTY__KEYWORD);

		featureReferenceEClass = createEClass(FEATURE_REFERENCE);
		createEReference(featureReferenceEClass, FEATURE_REFERENCE__REFERENCED_FEATURE);

		groupReferenceEClass = createEClass(GROUP_REFERENCE);
		createEReference(groupReferenceEClass, GROUP_REFERENCE__REFERENCED_GROUP);

		propertyEClass = createEClass(PROPERTY);

		featureExistencePropertyEClass = createEClass(FEATURE_EXISTENCE_PROPERTY);

		featureTypePropertyEClass = createEClass(FEATURE_TYPE_PROPERTY);
		createEAttribute(featureTypePropertyEClass, FEATURE_TYPE_PROPERTY__FEATURE_TYPE);

		featureParentPropertyEClass = createEClass(FEATURE_PARENT_PROPERTY);

		groupExistencePropertyEClass = createEClass(GROUP_EXISTENCE_PROPERTY);

		groupTypePropertyEClass = createEClass(GROUP_TYPE_PROPERTY);
		createEAttribute(groupTypePropertyEClass, GROUP_TYPE_PROPERTY__GROUP_TYPE);

		groupParentPropertyEClass = createEClass(GROUP_PARENT_PROPERTY);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ConstraintsPackage theConstraintsPackage = (ConstraintsPackage)EPackage.Registry.INSTANCE.getEPackage(ConstraintsPackage.eNS_URI);
		BuildingblocksPackage theBuildingblocksPackage = (BuildingblocksPackage)EPackage.Registry.INSTANCE.getEPackage(BuildingblocksPackage.eNS_URI);
		DwFeaturePackage theDwFeaturePackage = (DwFeaturePackage)EPackage.Registry.INSTANCE.getEPackage(DwFeaturePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		evolutionaryPropertyEClass.getESuperTypes().add(theConstraintsPackage.getEvolutionaryAbstractBuidingBlock());
		featureExistencePropertyEClass.getESuperTypes().add(this.getProperty());
		featureExistencePropertyEClass.getESuperTypes().add(this.getFeatureReference());
		featureTypePropertyEClass.getESuperTypes().add(this.getProperty());
		featureTypePropertyEClass.getESuperTypes().add(this.getFeatureReference());
		featureParentPropertyEClass.getESuperTypes().add(this.getProperty());
		featureParentPropertyEClass.getESuperTypes().add(this.getFeatureReference());
		featureParentPropertyEClass.getESuperTypes().add(this.getGroupReference());
		groupExistencePropertyEClass.getESuperTypes().add(this.getProperty());
		groupExistencePropertyEClass.getESuperTypes().add(this.getGroupReference());
		groupTypePropertyEClass.getESuperTypes().add(this.getProperty());
		groupTypePropertyEClass.getESuperTypes().add(this.getGroupReference());
		groupParentPropertyEClass.getESuperTypes().add(this.getProperty());
		groupParentPropertyEClass.getESuperTypes().add(this.getGroupReference());
		groupParentPropertyEClass.getESuperTypes().add(this.getFeatureReference());

		// Initialize classes, features, and operations; add parameters
		initEClass(evolutionaryPropertyEClass, EvolutionaryProperty.class, "EvolutionaryProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEvolutionaryProperty_Property(), this.getProperty(), null, "property", null, 1, 1, EvolutionaryProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEvolutionaryProperty_Keyword(), theBuildingblocksPackage.getPropertyKeyword(), null, "keyword", null, 1, 1, EvolutionaryProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(featureReferenceEClass, FeatureReference.class, "FeatureReference", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFeatureReference_ReferencedFeature(), theDwFeaturePackage.getDwFeature(), null, "referencedFeature", null, 1, 1, FeatureReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(groupReferenceEClass, GroupReference.class, "GroupReference", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGroupReference_ReferencedGroup(), theDwFeaturePackage.getDwGroup(), null, "referencedGroup", null, 1, 1, GroupReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(propertyEClass, Property.class, "Property", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(featureExistencePropertyEClass, FeatureExistenceProperty.class, "FeatureExistenceProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(featureTypePropertyEClass, FeatureTypeProperty.class, "FeatureTypeProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFeatureTypeProperty_FeatureType(), theDwFeaturePackage.getDwFeatureTypeEnum(), "featureType", null, 1, 1, FeatureTypeProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(featureParentPropertyEClass, FeatureParentProperty.class, "FeatureParentProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(groupExistencePropertyEClass, GroupExistenceProperty.class, "GroupExistenceProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(groupTypePropertyEClass, GroupTypeProperty.class, "GroupTypeProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGroupTypeProperty_GroupType(), theDwFeaturePackage.getDwGroupTypeEnum(), "groupType", null, 1, 1, GroupTypeProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(groupParentPropertyEClass, GroupParentProperty.class, "GroupParentProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
	}

} //PropertyPackageImpl
