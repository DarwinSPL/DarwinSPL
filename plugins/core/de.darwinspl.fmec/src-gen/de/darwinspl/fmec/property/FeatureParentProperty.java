/**
 */
package de.darwinspl.fmec.property;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Parent Property</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.property.PropertyPackage#getFeatureParentProperty()
 * @model
 * @generated
 */
public interface FeatureParentProperty extends Property, FeatureReference, GroupReference {
} // FeatureParentProperty
