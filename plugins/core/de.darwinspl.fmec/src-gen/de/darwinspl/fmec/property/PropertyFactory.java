/**
 */
package de.darwinspl.fmec.property;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.darwinspl.fmec.property.PropertyPackage
 * @generated
 */
public interface PropertyFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PropertyFactory eINSTANCE = de.darwinspl.fmec.property.impl.PropertyFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Evolutionary Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Evolutionary Property</em>'.
	 * @generated
	 */
	EvolutionaryProperty createEvolutionaryProperty();

	/**
	 * Returns a new object of class '<em>Feature Existence Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature Existence Property</em>'.
	 * @generated
	 */
	FeatureExistenceProperty createFeatureExistenceProperty();

	/**
	 * Returns a new object of class '<em>Feature Type Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature Type Property</em>'.
	 * @generated
	 */
	FeatureTypeProperty createFeatureTypeProperty();

	/**
	 * Returns a new object of class '<em>Feature Parent Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature Parent Property</em>'.
	 * @generated
	 */
	FeatureParentProperty createFeatureParentProperty();

	/**
	 * Returns a new object of class '<em>Group Existence Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Group Existence Property</em>'.
	 * @generated
	 */
	GroupExistenceProperty createGroupExistenceProperty();

	/**
	 * Returns a new object of class '<em>Group Type Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Group Type Property</em>'.
	 * @generated
	 */
	GroupTypeProperty createGroupTypeProperty();

	/**
	 * Returns a new object of class '<em>Group Parent Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Group Parent Property</em>'.
	 * @generated
	 */
	GroupParentProperty createGroupParentProperty();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	PropertyPackage getPropertyPackage();

} //PropertyFactory
