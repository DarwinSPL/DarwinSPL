/**
 */
package de.darwinspl.fmec.property.impl;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwGroup;

import de.darwinspl.fmec.property.FeatureReference;
import de.darwinspl.fmec.property.GroupParentProperty;
import de.darwinspl.fmec.property.GroupReference;
import de.darwinspl.fmec.property.PropertyPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Group Parent Property</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.property.impl.GroupParentPropertyImpl#getReferencedGroup <em>Referenced Group</em>}</li>
 *   <li>{@link de.darwinspl.fmec.property.impl.GroupParentPropertyImpl#getReferencedFeature <em>Referenced Feature</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GroupParentPropertyImpl extends PropertyImpl implements GroupParentProperty {
	/**
	 * The cached value of the '{@link #getReferencedGroup() <em>Referenced Group</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencedGroup()
	 * @generated
	 * @ordered
	 */
	protected DwGroup referencedGroup;

	/**
	 * The cached value of the '{@link #getReferencedFeature() <em>Referenced Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencedFeature()
	 * @generated
	 * @ordered
	 */
	protected DwFeature referencedFeature;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GroupParentPropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PropertyPackage.Literals.GROUP_PARENT_PROPERTY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroup getReferencedGroup() {
		if (referencedGroup != null && referencedGroup.eIsProxy()) {
			InternalEObject oldReferencedGroup = (InternalEObject)referencedGroup;
			referencedGroup = (DwGroup)eResolveProxy(oldReferencedGroup);
			if (referencedGroup != oldReferencedGroup) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_GROUP, oldReferencedGroup, referencedGroup));
			}
		}
		return referencedGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwGroup basicGetReferencedGroup() {
		return referencedGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setReferencedGroup(DwGroup newReferencedGroup) {
		DwGroup oldReferencedGroup = referencedGroup;
		referencedGroup = newReferencedGroup;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_GROUP, oldReferencedGroup, referencedGroup));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeature getReferencedFeature() {
		if (referencedFeature != null && referencedFeature.eIsProxy()) {
			InternalEObject oldReferencedFeature = (InternalEObject)referencedFeature;
			referencedFeature = (DwFeature)eResolveProxy(oldReferencedFeature);
			if (referencedFeature != oldReferencedFeature) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_FEATURE, oldReferencedFeature, referencedFeature));
			}
		}
		return referencedFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwFeature basicGetReferencedFeature() {
		return referencedFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setReferencedFeature(DwFeature newReferencedFeature) {
		DwFeature oldReferencedFeature = referencedFeature;
		referencedFeature = newReferencedFeature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_FEATURE, oldReferencedFeature, referencedFeature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_GROUP:
				if (resolve) return getReferencedGroup();
				return basicGetReferencedGroup();
			case PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_FEATURE:
				if (resolve) return getReferencedFeature();
				return basicGetReferencedFeature();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_GROUP:
				setReferencedGroup((DwGroup)newValue);
				return;
			case PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_FEATURE:
				setReferencedFeature((DwFeature)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_GROUP:
				setReferencedGroup((DwGroup)null);
				return;
			case PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_FEATURE:
				setReferencedFeature((DwFeature)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_GROUP:
				return referencedGroup != null;
			case PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_FEATURE:
				return referencedFeature != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == GroupReference.class) {
			switch (derivedFeatureID) {
				case PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_GROUP: return PropertyPackage.GROUP_REFERENCE__REFERENCED_GROUP;
				default: return -1;
			}
		}
		if (baseClass == FeatureReference.class) {
			switch (derivedFeatureID) {
				case PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_FEATURE: return PropertyPackage.FEATURE_REFERENCE__REFERENCED_FEATURE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == GroupReference.class) {
			switch (baseFeatureID) {
				case PropertyPackage.GROUP_REFERENCE__REFERENCED_GROUP: return PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_GROUP;
				default: return -1;
			}
		}
		if (baseClass == FeatureReference.class) {
			switch (baseFeatureID) {
				case PropertyPackage.FEATURE_REFERENCE__REFERENCED_FEATURE: return PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_FEATURE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //GroupParentPropertyImpl
