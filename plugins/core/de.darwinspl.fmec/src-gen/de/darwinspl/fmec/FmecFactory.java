/**
 */
package de.darwinspl.fmec;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.darwinspl.fmec.FmecPackage
 * @generated
 */
public interface FmecFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FmecFactory eINSTANCE = de.darwinspl.fmec.impl.FmecFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Feature Model Evolution Constraint Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature Model Evolution Constraint Model</em>'.
	 * @generated
	 */
	FeatureModelEvolutionConstraintModel createFeatureModelEvolutionConstraintModel();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	FmecPackage getFmecPackage();

} //FmecFactory
