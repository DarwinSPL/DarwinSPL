/**
 */
package de.darwinspl.fmec.timepoint.impl;

import de.darwinspl.fmec.timepoint.TimeOffset;
import de.darwinspl.fmec.timepoint.TimepointPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Time Offset</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.timepoint.impl.TimeOffsetImpl#isPositiveSignature <em>Positive Signature</em>}</li>
 *   <li>{@link de.darwinspl.fmec.timepoint.impl.TimeOffsetImpl#getYears <em>Years</em>}</li>
 *   <li>{@link de.darwinspl.fmec.timepoint.impl.TimeOffsetImpl#getMonths <em>Months</em>}</li>
 *   <li>{@link de.darwinspl.fmec.timepoint.impl.TimeOffsetImpl#getDays <em>Days</em>}</li>
 *   <li>{@link de.darwinspl.fmec.timepoint.impl.TimeOffsetImpl#getHours <em>Hours</em>}</li>
 *   <li>{@link de.darwinspl.fmec.timepoint.impl.TimeOffsetImpl#getMinutes <em>Minutes</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimeOffsetImpl extends MinimalEObjectImpl.Container implements TimeOffset {
	/**
	 * The default value of the '{@link #isPositiveSignature() <em>Positive Signature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPositiveSignature()
	 * @generated
	 * @ordered
	 */
	protected static final boolean POSITIVE_SIGNATURE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isPositiveSignature() <em>Positive Signature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPositiveSignature()
	 * @generated
	 * @ordered
	 */
	protected boolean positiveSignature = POSITIVE_SIGNATURE_EDEFAULT;

	/**
	 * The default value of the '{@link #getYears() <em>Years</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYears()
	 * @generated
	 * @ordered
	 */
	protected static final int YEARS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getYears() <em>Years</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYears()
	 * @generated
	 * @ordered
	 */
	protected int years = YEARS_EDEFAULT;

	/**
	 * The default value of the '{@link #getMonths() <em>Months</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMonths()
	 * @generated
	 * @ordered
	 */
	protected static final int MONTHS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMonths() <em>Months</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMonths()
	 * @generated
	 * @ordered
	 */
	protected int months = MONTHS_EDEFAULT;

	/**
	 * The default value of the '{@link #getDays() <em>Days</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDays()
	 * @generated
	 * @ordered
	 */
	protected static final int DAYS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getDays() <em>Days</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDays()
	 * @generated
	 * @ordered
	 */
	protected int days = DAYS_EDEFAULT;

	/**
	 * The default value of the '{@link #getHours() <em>Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHours()
	 * @generated
	 * @ordered
	 */
	protected static final int HOURS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getHours() <em>Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHours()
	 * @generated
	 * @ordered
	 */
	protected int hours = HOURS_EDEFAULT;

	/**
	 * The default value of the '{@link #getMinutes() <em>Minutes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinutes()
	 * @generated
	 * @ordered
	 */
	protected static final int MINUTES_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMinutes() <em>Minutes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinutes()
	 * @generated
	 * @ordered
	 */
	protected int minutes = MINUTES_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimeOffsetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimepointPackage.Literals.TIME_OFFSET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isPositiveSignature() {
		return positiveSignature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPositiveSignature(boolean newPositiveSignature) {
		boolean oldPositiveSignature = positiveSignature;
		positiveSignature = newPositiveSignature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimepointPackage.TIME_OFFSET__POSITIVE_SIGNATURE, oldPositiveSignature, positiveSignature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getYears() {
		return years;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setYears(int newYears) {
		int oldYears = years;
		years = newYears;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimepointPackage.TIME_OFFSET__YEARS, oldYears, years));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getMonths() {
		return months;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMonths(int newMonths) {
		int oldMonths = months;
		months = newMonths;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimepointPackage.TIME_OFFSET__MONTHS, oldMonths, months));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getDays() {
		return days;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDays(int newDays) {
		int oldDays = days;
		days = newDays;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimepointPackage.TIME_OFFSET__DAYS, oldDays, days));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getHours() {
		return hours;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setHours(int newHours) {
		int oldHours = hours;
		hours = newHours;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimepointPackage.TIME_OFFSET__HOURS, oldHours, hours));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getMinutes() {
		return minutes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMinutes(int newMinutes) {
		int oldMinutes = minutes;
		minutes = newMinutes;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimepointPackage.TIME_OFFSET__MINUTES, oldMinutes, minutes));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TimepointPackage.TIME_OFFSET__POSITIVE_SIGNATURE:
				return isPositiveSignature();
			case TimepointPackage.TIME_OFFSET__YEARS:
				return getYears();
			case TimepointPackage.TIME_OFFSET__MONTHS:
				return getMonths();
			case TimepointPackage.TIME_OFFSET__DAYS:
				return getDays();
			case TimepointPackage.TIME_OFFSET__HOURS:
				return getHours();
			case TimepointPackage.TIME_OFFSET__MINUTES:
				return getMinutes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TimepointPackage.TIME_OFFSET__POSITIVE_SIGNATURE:
				setPositiveSignature((Boolean)newValue);
				return;
			case TimepointPackage.TIME_OFFSET__YEARS:
				setYears((Integer)newValue);
				return;
			case TimepointPackage.TIME_OFFSET__MONTHS:
				setMonths((Integer)newValue);
				return;
			case TimepointPackage.TIME_OFFSET__DAYS:
				setDays((Integer)newValue);
				return;
			case TimepointPackage.TIME_OFFSET__HOURS:
				setHours((Integer)newValue);
				return;
			case TimepointPackage.TIME_OFFSET__MINUTES:
				setMinutes((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TimepointPackage.TIME_OFFSET__POSITIVE_SIGNATURE:
				setPositiveSignature(POSITIVE_SIGNATURE_EDEFAULT);
				return;
			case TimepointPackage.TIME_OFFSET__YEARS:
				setYears(YEARS_EDEFAULT);
				return;
			case TimepointPackage.TIME_OFFSET__MONTHS:
				setMonths(MONTHS_EDEFAULT);
				return;
			case TimepointPackage.TIME_OFFSET__DAYS:
				setDays(DAYS_EDEFAULT);
				return;
			case TimepointPackage.TIME_OFFSET__HOURS:
				setHours(HOURS_EDEFAULT);
				return;
			case TimepointPackage.TIME_OFFSET__MINUTES:
				setMinutes(MINUTES_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TimepointPackage.TIME_OFFSET__POSITIVE_SIGNATURE:
				return positiveSignature != POSITIVE_SIGNATURE_EDEFAULT;
			case TimepointPackage.TIME_OFFSET__YEARS:
				return years != YEARS_EDEFAULT;
			case TimepointPackage.TIME_OFFSET__MONTHS:
				return months != MONTHS_EDEFAULT;
			case TimepointPackage.TIME_OFFSET__DAYS:
				return days != DAYS_EDEFAULT;
			case TimepointPackage.TIME_OFFSET__HOURS:
				return hours != HOURS_EDEFAULT;
			case TimepointPackage.TIME_OFFSET__MINUTES:
				return minutes != MINUTES_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (positiveSignature: ");
		result.append(positiveSignature);
		result.append(", years: ");
		result.append(years);
		result.append(", months: ");
		result.append(months);
		result.append(", days: ");
		result.append(days);
		result.append(", hours: ");
		result.append(hours);
		result.append(", minutes: ");
		result.append(minutes);
		result.append(')');
		return result.toString();
	}

} //TimeOffsetImpl
