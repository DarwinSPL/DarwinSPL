/**
 */
package de.darwinspl.fmec.timepoint;

import de.darwinspl.fmec.constraints.ConstraintsPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.darwinspl.fmec.timepoint.TimepointFactory
 * @model kind="package"
 * @generated
 */
public interface TimepointPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "timepoint";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.darwinspl.de/feature-model-evolution-constraints/timepoint/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "timepoint";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TimepointPackage eINSTANCE = de.darwinspl.fmec.timepoint.impl.TimepointPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.timepoint.impl.TimePointImpl <em>Time Point</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.timepoint.impl.TimePointImpl
	 * @see de.darwinspl.fmec.timepoint.impl.TimepointPackageImpl#getTimePoint()
	 * @generated
	 */
	int TIME_POINT = 0;

	/**
	 * The feature id for the '<em><b>Time Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_POINT__TIME_POINT = ConstraintsPackage.EVOLUTIONARY_ABSTRACT_BUIDING_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Offset</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_POINT__OFFSET = ConstraintsPackage.EVOLUTIONARY_ABSTRACT_BUIDING_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Time Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_POINT_FEATURE_COUNT = ConstraintsPackage.EVOLUTIONARY_ABSTRACT_BUIDING_BLOCK_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Time Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_POINT_OPERATION_COUNT = ConstraintsPackage.EVOLUTIONARY_ABSTRACT_BUIDING_BLOCK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.timepoint.impl.TimePointWithoutOffsetImpl <em>Time Point Without Offset</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.timepoint.impl.TimePointWithoutOffsetImpl
	 * @see de.darwinspl.fmec.timepoint.impl.TimepointPackageImpl#getTimePointWithoutOffset()
	 * @generated
	 */
	int TIME_POINT_WITHOUT_OFFSET = 1;

	/**
	 * The number of structural features of the '<em>Time Point Without Offset</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_POINT_WITHOUT_OFFSET_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Time Point Without Offset</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_POINT_WITHOUT_OFFSET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.timepoint.impl.ExplicitTimePointImpl <em>Explicit Time Point</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.timepoint.impl.ExplicitTimePointImpl
	 * @see de.darwinspl.fmec.timepoint.impl.TimepointPackageImpl#getExplicitTimePoint()
	 * @generated
	 */
	int EXPLICIT_TIME_POINT = 2;

	/**
	 * The feature id for the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPLICIT_TIME_POINT__DATE = TIME_POINT_WITHOUT_OFFSET_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Explicit Time Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPLICIT_TIME_POINT_FEATURE_COUNT = TIME_POINT_WITHOUT_OFFSET_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Explicit Time Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPLICIT_TIME_POINT_OPERATION_COUNT = TIME_POINT_WITHOUT_OFFSET_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.timepoint.impl.EvolutionaryEventImpl <em>Evolutionary Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.timepoint.impl.EvolutionaryEventImpl
	 * @see de.darwinspl.fmec.timepoint.impl.TimepointPackageImpl#getEvolutionaryEvent()
	 * @generated
	 */
	int EVOLUTIONARY_EVENT = 3;

	/**
	 * The feature id for the '<em><b>Referenced Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVOLUTIONARY_EVENT__REFERENCED_FEATURE = TIME_POINT_WITHOUT_OFFSET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Keyword</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVOLUTIONARY_EVENT__KEYWORD = TIME_POINT_WITHOUT_OFFSET_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Evolutionary Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVOLUTIONARY_EVENT_FEATURE_COUNT = TIME_POINT_WITHOUT_OFFSET_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Evolutionary Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVOLUTIONARY_EVENT_OPERATION_COUNT = TIME_POINT_WITHOUT_OFFSET_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.fmec.timepoint.impl.TimeOffsetImpl <em>Time Offset</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.fmec.timepoint.impl.TimeOffsetImpl
	 * @see de.darwinspl.fmec.timepoint.impl.TimepointPackageImpl#getTimeOffset()
	 * @generated
	 */
	int TIME_OFFSET = 4;

	/**
	 * The feature id for the '<em><b>Positive Signature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_OFFSET__POSITIVE_SIGNATURE = 0;

	/**
	 * The feature id for the '<em><b>Years</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_OFFSET__YEARS = 1;

	/**
	 * The feature id for the '<em><b>Months</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_OFFSET__MONTHS = 2;

	/**
	 * The feature id for the '<em><b>Days</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_OFFSET__DAYS = 3;

	/**
	 * The feature id for the '<em><b>Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_OFFSET__HOURS = 4;

	/**
	 * The feature id for the '<em><b>Minutes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_OFFSET__MINUTES = 5;

	/**
	 * The number of structural features of the '<em>Time Offset</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_OFFSET_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Time Offset</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_OFFSET_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.timepoint.TimePoint <em>Time Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Time Point</em>'.
	 * @see de.darwinspl.fmec.timepoint.TimePoint
	 * @generated
	 */
	EClass getTimePoint();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.fmec.timepoint.TimePoint#getTimePoint <em>Time Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Time Point</em>'.
	 * @see de.darwinspl.fmec.timepoint.TimePoint#getTimePoint()
	 * @see #getTimePoint()
	 * @generated
	 */
	EReference getTimePoint_TimePoint();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.fmec.timepoint.TimePoint#getOffset <em>Offset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Offset</em>'.
	 * @see de.darwinspl.fmec.timepoint.TimePoint#getOffset()
	 * @see #getTimePoint()
	 * @generated
	 */
	EReference getTimePoint_Offset();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.timepoint.TimePointWithoutOffset <em>Time Point Without Offset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Time Point Without Offset</em>'.
	 * @see de.darwinspl.fmec.timepoint.TimePointWithoutOffset
	 * @generated
	 */
	EClass getTimePointWithoutOffset();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.timepoint.ExplicitTimePoint <em>Explicit Time Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Explicit Time Point</em>'.
	 * @see de.darwinspl.fmec.timepoint.ExplicitTimePoint
	 * @generated
	 */
	EClass getExplicitTimePoint();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.fmec.timepoint.ExplicitTimePoint#getDate <em>Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date</em>'.
	 * @see de.darwinspl.fmec.timepoint.ExplicitTimePoint#getDate()
	 * @see #getExplicitTimePoint()
	 * @generated
	 */
	EAttribute getExplicitTimePoint_Date();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.timepoint.EvolutionaryEvent <em>Evolutionary Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Evolutionary Event</em>'.
	 * @see de.darwinspl.fmec.timepoint.EvolutionaryEvent
	 * @generated
	 */
	EClass getEvolutionaryEvent();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.fmec.timepoint.EvolutionaryEvent#getKeyword <em>Keyword</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Keyword</em>'.
	 * @see de.darwinspl.fmec.timepoint.EvolutionaryEvent#getKeyword()
	 * @see #getEvolutionaryEvent()
	 * @generated
	 */
	EReference getEvolutionaryEvent_Keyword();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.fmec.timepoint.TimeOffset <em>Time Offset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Time Offset</em>'.
	 * @see de.darwinspl.fmec.timepoint.TimeOffset
	 * @generated
	 */
	EClass getTimeOffset();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.fmec.timepoint.TimeOffset#isPositiveSignature <em>Positive Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Positive Signature</em>'.
	 * @see de.darwinspl.fmec.timepoint.TimeOffset#isPositiveSignature()
	 * @see #getTimeOffset()
	 * @generated
	 */
	EAttribute getTimeOffset_PositiveSignature();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.fmec.timepoint.TimeOffset#getYears <em>Years</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Years</em>'.
	 * @see de.darwinspl.fmec.timepoint.TimeOffset#getYears()
	 * @see #getTimeOffset()
	 * @generated
	 */
	EAttribute getTimeOffset_Years();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.fmec.timepoint.TimeOffset#getMonths <em>Months</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Months</em>'.
	 * @see de.darwinspl.fmec.timepoint.TimeOffset#getMonths()
	 * @see #getTimeOffset()
	 * @generated
	 */
	EAttribute getTimeOffset_Months();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.fmec.timepoint.TimeOffset#getDays <em>Days</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Days</em>'.
	 * @see de.darwinspl.fmec.timepoint.TimeOffset#getDays()
	 * @see #getTimeOffset()
	 * @generated
	 */
	EAttribute getTimeOffset_Days();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.fmec.timepoint.TimeOffset#getHours <em>Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hours</em>'.
	 * @see de.darwinspl.fmec.timepoint.TimeOffset#getHours()
	 * @see #getTimeOffset()
	 * @generated
	 */
	EAttribute getTimeOffset_Hours();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.fmec.timepoint.TimeOffset#getMinutes <em>Minutes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Minutes</em>'.
	 * @see de.darwinspl.fmec.timepoint.TimeOffset#getMinutes()
	 * @see #getTimeOffset()
	 * @generated
	 */
	EAttribute getTimeOffset_Minutes();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TimepointFactory getTimepointFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.timepoint.impl.TimePointImpl <em>Time Point</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.timepoint.impl.TimePointImpl
		 * @see de.darwinspl.fmec.timepoint.impl.TimepointPackageImpl#getTimePoint()
		 * @generated
		 */
		EClass TIME_POINT = eINSTANCE.getTimePoint();

		/**
		 * The meta object literal for the '<em><b>Time Point</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIME_POINT__TIME_POINT = eINSTANCE.getTimePoint_TimePoint();

		/**
		 * The meta object literal for the '<em><b>Offset</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIME_POINT__OFFSET = eINSTANCE.getTimePoint_Offset();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.timepoint.impl.TimePointWithoutOffsetImpl <em>Time Point Without Offset</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.timepoint.impl.TimePointWithoutOffsetImpl
		 * @see de.darwinspl.fmec.timepoint.impl.TimepointPackageImpl#getTimePointWithoutOffset()
		 * @generated
		 */
		EClass TIME_POINT_WITHOUT_OFFSET = eINSTANCE.getTimePointWithoutOffset();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.timepoint.impl.ExplicitTimePointImpl <em>Explicit Time Point</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.timepoint.impl.ExplicitTimePointImpl
		 * @see de.darwinspl.fmec.timepoint.impl.TimepointPackageImpl#getExplicitTimePoint()
		 * @generated
		 */
		EClass EXPLICIT_TIME_POINT = eINSTANCE.getExplicitTimePoint();

		/**
		 * The meta object literal for the '<em><b>Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXPLICIT_TIME_POINT__DATE = eINSTANCE.getExplicitTimePoint_Date();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.timepoint.impl.EvolutionaryEventImpl <em>Evolutionary Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.timepoint.impl.EvolutionaryEventImpl
		 * @see de.darwinspl.fmec.timepoint.impl.TimepointPackageImpl#getEvolutionaryEvent()
		 * @generated
		 */
		EClass EVOLUTIONARY_EVENT = eINSTANCE.getEvolutionaryEvent();

		/**
		 * The meta object literal for the '<em><b>Keyword</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVOLUTIONARY_EVENT__KEYWORD = eINSTANCE.getEvolutionaryEvent_Keyword();

		/**
		 * The meta object literal for the '{@link de.darwinspl.fmec.timepoint.impl.TimeOffsetImpl <em>Time Offset</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.fmec.timepoint.impl.TimeOffsetImpl
		 * @see de.darwinspl.fmec.timepoint.impl.TimepointPackageImpl#getTimeOffset()
		 * @generated
		 */
		EClass TIME_OFFSET = eINSTANCE.getTimeOffset();

		/**
		 * The meta object literal for the '<em><b>Positive Signature</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_OFFSET__POSITIVE_SIGNATURE = eINSTANCE.getTimeOffset_PositiveSignature();

		/**
		 * The meta object literal for the '<em><b>Years</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_OFFSET__YEARS = eINSTANCE.getTimeOffset_Years();

		/**
		 * The meta object literal for the '<em><b>Months</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_OFFSET__MONTHS = eINSTANCE.getTimeOffset_Months();

		/**
		 * The meta object literal for the '<em><b>Days</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_OFFSET__DAYS = eINSTANCE.getTimeOffset_Days();

		/**
		 * The meta object literal for the '<em><b>Hours</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_OFFSET__HOURS = eINSTANCE.getTimeOffset_Hours();

		/**
		 * The meta object literal for the '<em><b>Minutes</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_OFFSET__MINUTES = eINSTANCE.getTimeOffset_Minutes();

	}

} //TimepointPackage
