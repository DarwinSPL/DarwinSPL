/**
 */
package de.darwinspl.fmec.timepoint.util;

import de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock;

import de.darwinspl.fmec.property.FeatureReference;

import de.darwinspl.fmec.timepoint.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see de.darwinspl.fmec.timepoint.TimepointPackage
 * @generated
 */
public class TimepointSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TimepointPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimepointSwitch() {
		if (modelPackage == null) {
			modelPackage = TimepointPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case TimepointPackage.TIME_POINT: {
				TimePoint timePoint = (TimePoint)theEObject;
				T result = caseTimePoint(timePoint);
				if (result == null) result = caseEvolutionaryAbstractBuidingBlock(timePoint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TimepointPackage.TIME_POINT_WITHOUT_OFFSET: {
				TimePointWithoutOffset timePointWithoutOffset = (TimePointWithoutOffset)theEObject;
				T result = caseTimePointWithoutOffset(timePointWithoutOffset);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TimepointPackage.EXPLICIT_TIME_POINT: {
				ExplicitTimePoint explicitTimePoint = (ExplicitTimePoint)theEObject;
				T result = caseExplicitTimePoint(explicitTimePoint);
				if (result == null) result = caseTimePointWithoutOffset(explicitTimePoint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TimepointPackage.EVOLUTIONARY_EVENT: {
				EvolutionaryEvent evolutionaryEvent = (EvolutionaryEvent)theEObject;
				T result = caseEvolutionaryEvent(evolutionaryEvent);
				if (result == null) result = caseTimePointWithoutOffset(evolutionaryEvent);
				if (result == null) result = caseFeatureReference(evolutionaryEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TimepointPackage.TIME_OFFSET: {
				TimeOffset timeOffset = (TimeOffset)theEObject;
				T result = caseTimeOffset(timeOffset);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Time Point</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Time Point</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTimePoint(TimePoint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Time Point Without Offset</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Time Point Without Offset</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTimePointWithoutOffset(TimePointWithoutOffset object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Explicit Time Point</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Explicit Time Point</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExplicitTimePoint(ExplicitTimePoint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Evolutionary Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Evolutionary Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEvolutionaryEvent(EvolutionaryEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Time Offset</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Time Offset</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTimeOffset(TimeOffset object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Evolutionary Abstract Buiding Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Evolutionary Abstract Buiding Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEvolutionaryAbstractBuidingBlock(EvolutionaryAbstractBuidingBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeatureReference(FeatureReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //TimepointSwitch
