/**
 */
package de.darwinspl.fmec.timepoint.impl;

import de.darwinspl.fmec.constraints.impl.EvolutionaryAbstractBuidingBlockImpl;

import de.darwinspl.fmec.timepoint.TimeOffset;
import de.darwinspl.fmec.timepoint.TimePoint;
import de.darwinspl.fmec.timepoint.TimePointWithoutOffset;
import de.darwinspl.fmec.timepoint.TimepointPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Time Point</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.timepoint.impl.TimePointImpl#getTimePoint <em>Time Point</em>}</li>
 *   <li>{@link de.darwinspl.fmec.timepoint.impl.TimePointImpl#getOffset <em>Offset</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimePointImpl extends EvolutionaryAbstractBuidingBlockImpl implements TimePoint {
	/**
	 * The cached value of the '{@link #getTimePoint() <em>Time Point</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimePoint()
	 * @generated
	 * @ordered
	 */
	protected TimePointWithoutOffset timePoint;

	/**
	 * The cached value of the '{@link #getOffset() <em>Offset</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOffset()
	 * @generated
	 * @ordered
	 */
	protected TimeOffset offset;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimePointImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimepointPackage.Literals.TIME_POINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TimePointWithoutOffset getTimePoint() {
		return timePoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTimePoint(TimePointWithoutOffset newTimePoint, NotificationChain msgs) {
		TimePointWithoutOffset oldTimePoint = timePoint;
		timePoint = newTimePoint;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TimepointPackage.TIME_POINT__TIME_POINT, oldTimePoint, newTimePoint);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTimePoint(TimePointWithoutOffset newTimePoint) {
		if (newTimePoint != timePoint) {
			NotificationChain msgs = null;
			if (timePoint != null)
				msgs = ((InternalEObject)timePoint).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TimepointPackage.TIME_POINT__TIME_POINT, null, msgs);
			if (newTimePoint != null)
				msgs = ((InternalEObject)newTimePoint).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TimepointPackage.TIME_POINT__TIME_POINT, null, msgs);
			msgs = basicSetTimePoint(newTimePoint, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimepointPackage.TIME_POINT__TIME_POINT, newTimePoint, newTimePoint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TimeOffset getOffset() {
		return offset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOffset(TimeOffset newOffset, NotificationChain msgs) {
		TimeOffset oldOffset = offset;
		offset = newOffset;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TimepointPackage.TIME_POINT__OFFSET, oldOffset, newOffset);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOffset(TimeOffset newOffset) {
		if (newOffset != offset) {
			NotificationChain msgs = null;
			if (offset != null)
				msgs = ((InternalEObject)offset).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TimepointPackage.TIME_POINT__OFFSET, null, msgs);
			if (newOffset != null)
				msgs = ((InternalEObject)newOffset).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TimepointPackage.TIME_POINT__OFFSET, null, msgs);
			msgs = basicSetOffset(newOffset, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimepointPackage.TIME_POINT__OFFSET, newOffset, newOffset));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TimepointPackage.TIME_POINT__TIME_POINT:
				return basicSetTimePoint(null, msgs);
			case TimepointPackage.TIME_POINT__OFFSET:
				return basicSetOffset(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TimepointPackage.TIME_POINT__TIME_POINT:
				return getTimePoint();
			case TimepointPackage.TIME_POINT__OFFSET:
				return getOffset();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TimepointPackage.TIME_POINT__TIME_POINT:
				setTimePoint((TimePointWithoutOffset)newValue);
				return;
			case TimepointPackage.TIME_POINT__OFFSET:
				setOffset((TimeOffset)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TimepointPackage.TIME_POINT__TIME_POINT:
				setTimePoint((TimePointWithoutOffset)null);
				return;
			case TimepointPackage.TIME_POINT__OFFSET:
				setOffset((TimeOffset)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TimepointPackage.TIME_POINT__TIME_POINT:
				return timePoint != null;
			case TimepointPackage.TIME_POINT__OFFSET:
				return offset != null;
		}
		return super.eIsSet(featureID);
	}

} //TimePointImpl
