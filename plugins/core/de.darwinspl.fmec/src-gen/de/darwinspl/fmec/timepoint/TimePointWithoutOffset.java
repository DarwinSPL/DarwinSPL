/**
 */
package de.darwinspl.fmec.timepoint;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Time Point Without Offset</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.fmec.timepoint.TimepointPackage#getTimePointWithoutOffset()
 * @model abstract="true"
 * @generated
 */
public interface TimePointWithoutOffset extends EObject {
} // TimePointWithoutOffset
