/**
 */
package de.darwinspl.fmec.timepoint;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Time Offset</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.timepoint.TimeOffset#isPositiveSignature <em>Positive Signature</em>}</li>
 *   <li>{@link de.darwinspl.fmec.timepoint.TimeOffset#getYears <em>Years</em>}</li>
 *   <li>{@link de.darwinspl.fmec.timepoint.TimeOffset#getMonths <em>Months</em>}</li>
 *   <li>{@link de.darwinspl.fmec.timepoint.TimeOffset#getDays <em>Days</em>}</li>
 *   <li>{@link de.darwinspl.fmec.timepoint.TimeOffset#getHours <em>Hours</em>}</li>
 *   <li>{@link de.darwinspl.fmec.timepoint.TimeOffset#getMinutes <em>Minutes</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.fmec.timepoint.TimepointPackage#getTimeOffset()
 * @model
 * @generated
 */
public interface TimeOffset extends EObject {
	/**
	 * Returns the value of the '<em><b>Positive Signature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Positive Signature</em>' attribute.
	 * @see #setPositiveSignature(boolean)
	 * @see de.darwinspl.fmec.timepoint.TimepointPackage#getTimeOffset_PositiveSignature()
	 * @model required="true"
	 * @generated
	 */
	boolean isPositiveSignature();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.timepoint.TimeOffset#isPositiveSignature <em>Positive Signature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Positive Signature</em>' attribute.
	 * @see #isPositiveSignature()
	 * @generated
	 */
	void setPositiveSignature(boolean value);

	/**
	 * Returns the value of the '<em><b>Years</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Years</em>' attribute.
	 * @see #setYears(int)
	 * @see de.darwinspl.fmec.timepoint.TimepointPackage#getTimeOffset_Years()
	 * @model
	 * @generated
	 */
	int getYears();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.timepoint.TimeOffset#getYears <em>Years</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Years</em>' attribute.
	 * @see #getYears()
	 * @generated
	 */
	void setYears(int value);

	/**
	 * Returns the value of the '<em><b>Months</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Months</em>' attribute.
	 * @see #setMonths(int)
	 * @see de.darwinspl.fmec.timepoint.TimepointPackage#getTimeOffset_Months()
	 * @model
	 * @generated
	 */
	int getMonths();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.timepoint.TimeOffset#getMonths <em>Months</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Months</em>' attribute.
	 * @see #getMonths()
	 * @generated
	 */
	void setMonths(int value);

	/**
	 * Returns the value of the '<em><b>Days</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Days</em>' attribute.
	 * @see #setDays(int)
	 * @see de.darwinspl.fmec.timepoint.TimepointPackage#getTimeOffset_Days()
	 * @model
	 * @generated
	 */
	int getDays();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.timepoint.TimeOffset#getDays <em>Days</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Days</em>' attribute.
	 * @see #getDays()
	 * @generated
	 */
	void setDays(int value);

	/**
	 * Returns the value of the '<em><b>Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hours</em>' attribute.
	 * @see #setHours(int)
	 * @see de.darwinspl.fmec.timepoint.TimepointPackage#getTimeOffset_Hours()
	 * @model
	 * @generated
	 */
	int getHours();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.timepoint.TimeOffset#getHours <em>Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hours</em>' attribute.
	 * @see #getHours()
	 * @generated
	 */
	void setHours(int value);

	/**
	 * Returns the value of the '<em><b>Minutes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Minutes</em>' attribute.
	 * @see #setMinutes(int)
	 * @see de.darwinspl.fmec.timepoint.TimepointPackage#getTimeOffset_Minutes()
	 * @model
	 * @generated
	 */
	int getMinutes();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.timepoint.TimeOffset#getMinutes <em>Minutes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Minutes</em>' attribute.
	 * @see #getMinutes()
	 * @generated
	 */
	void setMinutes(int value);

} // TimeOffset
