/**
 */
package de.darwinspl.fmec.timepoint;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.darwinspl.fmec.timepoint.TimepointPackage
 * @generated
 */
public interface TimepointFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TimepointFactory eINSTANCE = de.darwinspl.fmec.timepoint.impl.TimepointFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Time Point</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Time Point</em>'.
	 * @generated
	 */
	TimePoint createTimePoint();

	/**
	 * Returns a new object of class '<em>Explicit Time Point</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Explicit Time Point</em>'.
	 * @generated
	 */
	ExplicitTimePoint createExplicitTimePoint();

	/**
	 * Returns a new object of class '<em>Evolutionary Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Evolutionary Event</em>'.
	 * @generated
	 */
	EvolutionaryEvent createEvolutionaryEvent();

	/**
	 * Returns a new object of class '<em>Time Offset</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Time Offset</em>'.
	 * @generated
	 */
	TimeOffset createTimeOffset();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TimepointPackage getTimepointPackage();

} //TimepointFactory
