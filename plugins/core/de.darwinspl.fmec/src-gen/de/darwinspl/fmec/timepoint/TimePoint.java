/**
 */
package de.darwinspl.fmec.timepoint;

import de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Time Point</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.timepoint.TimePoint#getTimePoint <em>Time Point</em>}</li>
 *   <li>{@link de.darwinspl.fmec.timepoint.TimePoint#getOffset <em>Offset</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.fmec.timepoint.TimepointPackage#getTimePoint()
 * @model
 * @generated
 */
public interface TimePoint extends EvolutionaryAbstractBuidingBlock {
	/**
	 * Returns the value of the '<em><b>Time Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Point</em>' containment reference.
	 * @see #setTimePoint(TimePointWithoutOffset)
	 * @see de.darwinspl.fmec.timepoint.TimepointPackage#getTimePoint_TimePoint()
	 * @model containment="true" required="true"
	 * @generated
	 */
	TimePointWithoutOffset getTimePoint();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.timepoint.TimePoint#getTimePoint <em>Time Point</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time Point</em>' containment reference.
	 * @see #getTimePoint()
	 * @generated
	 */
	void setTimePoint(TimePointWithoutOffset value);

	/**
	 * Returns the value of the '<em><b>Offset</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Offset</em>' containment reference.
	 * @see #setOffset(TimeOffset)
	 * @see de.darwinspl.fmec.timepoint.TimepointPackage#getTimePoint_Offset()
	 * @model containment="true"
	 * @generated
	 */
	TimeOffset getOffset();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.timepoint.TimePoint#getOffset <em>Offset</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Offset</em>' containment reference.
	 * @see #getOffset()
	 * @generated
	 */
	void setOffset(TimeOffset value);

} // TimePoint
