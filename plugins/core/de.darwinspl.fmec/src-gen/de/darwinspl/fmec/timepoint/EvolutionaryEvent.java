/**
 */
package de.darwinspl.fmec.timepoint;

import de.darwinspl.fmec.keywords.buildingblocks.EventKeyword;

import de.darwinspl.fmec.property.FeatureReference;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Evolutionary Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.timepoint.EvolutionaryEvent#getKeyword <em>Keyword</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.fmec.timepoint.TimepointPackage#getEvolutionaryEvent()
 * @model
 * @generated
 */
public interface EvolutionaryEvent extends TimePointWithoutOffset, FeatureReference {
	/**
	 * Returns the value of the '<em><b>Keyword</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Keyword</em>' containment reference.
	 * @see #setKeyword(EventKeyword)
	 * @see de.darwinspl.fmec.timepoint.TimepointPackage#getEvolutionaryEvent_Keyword()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EventKeyword getKeyword();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.timepoint.EvolutionaryEvent#getKeyword <em>Keyword</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Keyword</em>' containment reference.
	 * @see #getKeyword()
	 * @generated
	 */
	void setKeyword(EventKeyword value);

} // EvolutionaryEvent
