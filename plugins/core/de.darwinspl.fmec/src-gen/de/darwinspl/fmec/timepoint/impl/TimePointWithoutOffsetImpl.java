/**
 */
package de.darwinspl.fmec.timepoint.impl;

import de.darwinspl.fmec.timepoint.TimePointWithoutOffset;
import de.darwinspl.fmec.timepoint.TimepointPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Time Point Without Offset</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class TimePointWithoutOffsetImpl extends MinimalEObjectImpl.Container implements TimePointWithoutOffset {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimePointWithoutOffsetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimepointPackage.Literals.TIME_POINT_WITHOUT_OFFSET;
	}

} //TimePointWithoutOffsetImpl
