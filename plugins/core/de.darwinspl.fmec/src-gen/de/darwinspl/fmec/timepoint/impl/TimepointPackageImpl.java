/**
 */
package de.darwinspl.fmec.timepoint.impl;

import de.darwinspl.datatypes.DwDatatypesPackage;

import de.darwinspl.feature.DwFeaturePackage;

import de.darwinspl.fmec.FmecPackage;

import de.darwinspl.fmec.constraints.ConstraintsPackage;

import de.darwinspl.fmec.constraints.impl.ConstraintsPackageImpl;

import de.darwinspl.fmec.impl.FmecPackageImpl;

import de.darwinspl.fmec.keywords.KeywordsPackage;

import de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage;

import de.darwinspl.fmec.keywords.buildingblocks.impl.BuildingblocksPackageImpl;

import de.darwinspl.fmec.keywords.impl.KeywordsPackageImpl;

import de.darwinspl.fmec.keywords.interval.IntervalPackage;

import de.darwinspl.fmec.keywords.interval.impl.IntervalPackageImpl;

import de.darwinspl.fmec.keywords.logical.LogicalPackage;

import de.darwinspl.fmec.keywords.logical.impl.LogicalPackageImpl;

import de.darwinspl.fmec.property.PropertyPackage;

import de.darwinspl.fmec.property.impl.PropertyPackageImpl;

import de.darwinspl.fmec.timepoint.EvolutionaryEvent;
import de.darwinspl.fmec.timepoint.ExplicitTimePoint;
import de.darwinspl.fmec.timepoint.TimeOffset;
import de.darwinspl.fmec.timepoint.TimePoint;
import de.darwinspl.fmec.timepoint.TimePointWithoutOffset;
import de.darwinspl.fmec.timepoint.TimepointFactory;
import de.darwinspl.fmec.timepoint.TimepointPackage;

import de.darwinspl.temporal.DwTemporalModelPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TimepointPackageImpl extends EPackageImpl implements TimepointPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timePointEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timePointWithoutOffsetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass explicitTimePointEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass evolutionaryEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timeOffsetEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.darwinspl.fmec.timepoint.TimepointPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TimepointPackageImpl() {
		super(eNS_URI, TimepointFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link TimepointPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TimepointPackage init() {
		if (isInited) return (TimepointPackage)EPackage.Registry.INSTANCE.getEPackage(TimepointPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredTimepointPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		TimepointPackageImpl theTimepointPackage = registeredTimepointPackage instanceof TimepointPackageImpl ? (TimepointPackageImpl)registeredTimepointPackage : new TimepointPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		DwDatatypesPackage.eINSTANCE.eClass();
		DwFeaturePackage.eINSTANCE.eClass();
		DwTemporalModelPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(FmecPackage.eNS_URI);
		FmecPackageImpl theFmecPackage = (FmecPackageImpl)(registeredPackage instanceof FmecPackageImpl ? registeredPackage : FmecPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ConstraintsPackage.eNS_URI);
		ConstraintsPackageImpl theConstraintsPackage = (ConstraintsPackageImpl)(registeredPackage instanceof ConstraintsPackageImpl ? registeredPackage : ConstraintsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(PropertyPackage.eNS_URI);
		PropertyPackageImpl thePropertyPackage = (PropertyPackageImpl)(registeredPackage instanceof PropertyPackageImpl ? registeredPackage : PropertyPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(KeywordsPackage.eNS_URI);
		KeywordsPackageImpl theKeywordsPackage = (KeywordsPackageImpl)(registeredPackage instanceof KeywordsPackageImpl ? registeredPackage : KeywordsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(LogicalPackage.eNS_URI);
		LogicalPackageImpl theLogicalPackage = (LogicalPackageImpl)(registeredPackage instanceof LogicalPackageImpl ? registeredPackage : LogicalPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(BuildingblocksPackage.eNS_URI);
		BuildingblocksPackageImpl theBuildingblocksPackage = (BuildingblocksPackageImpl)(registeredPackage instanceof BuildingblocksPackageImpl ? registeredPackage : BuildingblocksPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(de.darwinspl.fmec.keywords.timepoint.TimepointPackage.eNS_URI);
		de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl theTimepointPackage_1 = (de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl)(registeredPackage instanceof de.darwinspl.fmec.keywords.timepoint.impl.TimepointPackageImpl ? registeredPackage : de.darwinspl.fmec.keywords.timepoint.TimepointPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(IntervalPackage.eNS_URI);
		IntervalPackageImpl theIntervalPackage = (IntervalPackageImpl)(registeredPackage instanceof IntervalPackageImpl ? registeredPackage : IntervalPackage.eINSTANCE);

		// Create package meta-data objects
		theTimepointPackage.createPackageContents();
		theFmecPackage.createPackageContents();
		theConstraintsPackage.createPackageContents();
		thePropertyPackage.createPackageContents();
		theKeywordsPackage.createPackageContents();
		theLogicalPackage.createPackageContents();
		theBuildingblocksPackage.createPackageContents();
		theTimepointPackage_1.createPackageContents();
		theIntervalPackage.createPackageContents();

		// Initialize created meta-data
		theTimepointPackage.initializePackageContents();
		theFmecPackage.initializePackageContents();
		theConstraintsPackage.initializePackageContents();
		thePropertyPackage.initializePackageContents();
		theKeywordsPackage.initializePackageContents();
		theLogicalPackage.initializePackageContents();
		theBuildingblocksPackage.initializePackageContents();
		theTimepointPackage_1.initializePackageContents();
		theIntervalPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTimepointPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TimepointPackage.eNS_URI, theTimepointPackage);
		return theTimepointPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTimePoint() {
		return timePointEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTimePoint_TimePoint() {
		return (EReference)timePointEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTimePoint_Offset() {
		return (EReference)timePointEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTimePointWithoutOffset() {
		return timePointWithoutOffsetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getExplicitTimePoint() {
		return explicitTimePointEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getExplicitTimePoint_Date() {
		return (EAttribute)explicitTimePointEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEvolutionaryEvent() {
		return evolutionaryEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getEvolutionaryEvent_Keyword() {
		return (EReference)evolutionaryEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTimeOffset() {
		return timeOffsetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTimeOffset_PositiveSignature() {
		return (EAttribute)timeOffsetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTimeOffset_Years() {
		return (EAttribute)timeOffsetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTimeOffset_Months() {
		return (EAttribute)timeOffsetEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTimeOffset_Days() {
		return (EAttribute)timeOffsetEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTimeOffset_Hours() {
		return (EAttribute)timeOffsetEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTimeOffset_Minutes() {
		return (EAttribute)timeOffsetEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TimepointFactory getTimepointFactory() {
		return (TimepointFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		timePointEClass = createEClass(TIME_POINT);
		createEReference(timePointEClass, TIME_POINT__TIME_POINT);
		createEReference(timePointEClass, TIME_POINT__OFFSET);

		timePointWithoutOffsetEClass = createEClass(TIME_POINT_WITHOUT_OFFSET);

		explicitTimePointEClass = createEClass(EXPLICIT_TIME_POINT);
		createEAttribute(explicitTimePointEClass, EXPLICIT_TIME_POINT__DATE);

		evolutionaryEventEClass = createEClass(EVOLUTIONARY_EVENT);
		createEReference(evolutionaryEventEClass, EVOLUTIONARY_EVENT__KEYWORD);

		timeOffsetEClass = createEClass(TIME_OFFSET);
		createEAttribute(timeOffsetEClass, TIME_OFFSET__POSITIVE_SIGNATURE);
		createEAttribute(timeOffsetEClass, TIME_OFFSET__YEARS);
		createEAttribute(timeOffsetEClass, TIME_OFFSET__MONTHS);
		createEAttribute(timeOffsetEClass, TIME_OFFSET__DAYS);
		createEAttribute(timeOffsetEClass, TIME_OFFSET__HOURS);
		createEAttribute(timeOffsetEClass, TIME_OFFSET__MINUTES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ConstraintsPackage theConstraintsPackage = (ConstraintsPackage)EPackage.Registry.INSTANCE.getEPackage(ConstraintsPackage.eNS_URI);
		PropertyPackage thePropertyPackage = (PropertyPackage)EPackage.Registry.INSTANCE.getEPackage(PropertyPackage.eNS_URI);
		BuildingblocksPackage theBuildingblocksPackage = (BuildingblocksPackage)EPackage.Registry.INSTANCE.getEPackage(BuildingblocksPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		timePointEClass.getESuperTypes().add(theConstraintsPackage.getEvolutionaryAbstractBuidingBlock());
		explicitTimePointEClass.getESuperTypes().add(this.getTimePointWithoutOffset());
		evolutionaryEventEClass.getESuperTypes().add(this.getTimePointWithoutOffset());
		evolutionaryEventEClass.getESuperTypes().add(thePropertyPackage.getFeatureReference());

		// Initialize classes, features, and operations; add parameters
		initEClass(timePointEClass, TimePoint.class, "TimePoint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTimePoint_TimePoint(), this.getTimePointWithoutOffset(), null, "timePoint", null, 1, 1, TimePoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTimePoint_Offset(), this.getTimeOffset(), null, "offset", null, 0, 1, TimePoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(timePointWithoutOffsetEClass, TimePointWithoutOffset.class, "TimePointWithoutOffset", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(explicitTimePointEClass, ExplicitTimePoint.class, "ExplicitTimePoint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExplicitTimePoint_Date(), ecorePackage.getEDate(), "date", null, 1, 1, ExplicitTimePoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(evolutionaryEventEClass, EvolutionaryEvent.class, "EvolutionaryEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEvolutionaryEvent_Keyword(), theBuildingblocksPackage.getEventKeyword(), null, "keyword", null, 1, 1, EvolutionaryEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(timeOffsetEClass, TimeOffset.class, "TimeOffset", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTimeOffset_PositiveSignature(), ecorePackage.getEBoolean(), "positiveSignature", null, 1, 1, TimeOffset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimeOffset_Years(), ecorePackage.getEInt(), "years", null, 0, 1, TimeOffset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimeOffset_Months(), ecorePackage.getEInt(), "months", null, 0, 1, TimeOffset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimeOffset_Days(), ecorePackage.getEInt(), "days", null, 0, 1, TimeOffset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimeOffset_Hours(), ecorePackage.getEInt(), "hours", null, 0, 1, TimeOffset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimeOffset_Minutes(), ecorePackage.getEInt(), "minutes", null, 0, 1, TimeOffset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
	}

} //TimepointPackageImpl
