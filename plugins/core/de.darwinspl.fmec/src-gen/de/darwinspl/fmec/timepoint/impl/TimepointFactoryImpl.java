/**
 */
package de.darwinspl.fmec.timepoint.impl;

import de.darwinspl.fmec.timepoint.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TimepointFactoryImpl extends EFactoryImpl implements TimepointFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TimepointFactory init() {
		try {
			TimepointFactory theTimepointFactory = (TimepointFactory)EPackage.Registry.INSTANCE.getEFactory(TimepointPackage.eNS_URI);
			if (theTimepointFactory != null) {
				return theTimepointFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TimepointFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimepointFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TimepointPackage.TIME_POINT: return createTimePoint();
			case TimepointPackage.EXPLICIT_TIME_POINT: return createExplicitTimePoint();
			case TimepointPackage.EVOLUTIONARY_EVENT: return createEvolutionaryEvent();
			case TimepointPackage.TIME_OFFSET: return createTimeOffset();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TimePoint createTimePoint() {
		TimePointImpl timePoint = new TimePointImpl();
		return timePoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ExplicitTimePoint createExplicitTimePoint() {
		ExplicitTimePointImpl explicitTimePoint = new ExplicitTimePointImpl();
		return explicitTimePoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EvolutionaryEvent createEvolutionaryEvent() {
		EvolutionaryEventImpl evolutionaryEvent = new EvolutionaryEventImpl();
		return evolutionaryEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TimeOffset createTimeOffset() {
		TimeOffsetImpl timeOffset = new TimeOffsetImpl();
		return timeOffset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TimepointPackage getTimepointPackage() {
		return (TimepointPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TimepointPackage getPackage() {
		return TimepointPackage.eINSTANCE;
	}

} //TimepointFactoryImpl
