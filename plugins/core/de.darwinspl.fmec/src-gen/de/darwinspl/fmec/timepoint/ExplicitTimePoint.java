/**
 */
package de.darwinspl.fmec.timepoint;

import java.util.Date;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Explicit Time Point</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.timepoint.ExplicitTimePoint#getDate <em>Date</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.fmec.timepoint.TimepointPackage#getExplicitTimePoint()
 * @model
 * @generated
 */
public interface ExplicitTimePoint extends TimePointWithoutOffset {
	/**
	 * Returns the value of the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date</em>' attribute.
	 * @see #setDate(Date)
	 * @see de.darwinspl.fmec.timepoint.TimepointPackage#getExplicitTimePoint_Date()
	 * @model required="true"
	 * @generated
	 */
	Date getDate();

	/**
	 * Sets the value of the '{@link de.darwinspl.fmec.timepoint.ExplicitTimePoint#getDate <em>Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date</em>' attribute.
	 * @see #getDate()
	 * @generated
	 */
	void setDate(Date value);

} // ExplicitTimePoint
