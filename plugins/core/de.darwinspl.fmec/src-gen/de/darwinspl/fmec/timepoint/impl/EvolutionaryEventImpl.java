/**
 */
package de.darwinspl.fmec.timepoint.impl;

import de.darwinspl.feature.DwFeature;

import de.darwinspl.fmec.keywords.buildingblocks.EventKeyword;

import de.darwinspl.fmec.property.FeatureReference;
import de.darwinspl.fmec.property.PropertyPackage;

import de.darwinspl.fmec.timepoint.EvolutionaryEvent;
import de.darwinspl.fmec.timepoint.TimepointPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Evolutionary Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.fmec.timepoint.impl.EvolutionaryEventImpl#getReferencedFeature <em>Referenced Feature</em>}</li>
 *   <li>{@link de.darwinspl.fmec.timepoint.impl.EvolutionaryEventImpl#getKeyword <em>Keyword</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EvolutionaryEventImpl extends TimePointWithoutOffsetImpl implements EvolutionaryEvent {
	/**
	 * The cached value of the '{@link #getReferencedFeature() <em>Referenced Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencedFeature()
	 * @generated
	 * @ordered
	 */
	protected DwFeature referencedFeature;

	/**
	 * The cached value of the '{@link #getKeyword() <em>Keyword</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKeyword()
	 * @generated
	 * @ordered
	 */
	protected EventKeyword keyword;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EvolutionaryEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TimepointPackage.Literals.EVOLUTIONARY_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeature getReferencedFeature() {
		if (referencedFeature != null && referencedFeature.eIsProxy()) {
			InternalEObject oldReferencedFeature = (InternalEObject)referencedFeature;
			referencedFeature = (DwFeature)eResolveProxy(oldReferencedFeature);
			if (referencedFeature != oldReferencedFeature) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TimepointPackage.EVOLUTIONARY_EVENT__REFERENCED_FEATURE, oldReferencedFeature, referencedFeature));
			}
		}
		return referencedFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwFeature basicGetReferencedFeature() {
		return referencedFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setReferencedFeature(DwFeature newReferencedFeature) {
		DwFeature oldReferencedFeature = referencedFeature;
		referencedFeature = newReferencedFeature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimepointPackage.EVOLUTIONARY_EVENT__REFERENCED_FEATURE, oldReferencedFeature, referencedFeature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EventKeyword getKeyword() {
		return keyword;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetKeyword(EventKeyword newKeyword, NotificationChain msgs) {
		EventKeyword oldKeyword = keyword;
		keyword = newKeyword;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TimepointPackage.EVOLUTIONARY_EVENT__KEYWORD, oldKeyword, newKeyword);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setKeyword(EventKeyword newKeyword) {
		if (newKeyword != keyword) {
			NotificationChain msgs = null;
			if (keyword != null)
				msgs = ((InternalEObject)keyword).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TimepointPackage.EVOLUTIONARY_EVENT__KEYWORD, null, msgs);
			if (newKeyword != null)
				msgs = ((InternalEObject)newKeyword).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TimepointPackage.EVOLUTIONARY_EVENT__KEYWORD, null, msgs);
			msgs = basicSetKeyword(newKeyword, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimepointPackage.EVOLUTIONARY_EVENT__KEYWORD, newKeyword, newKeyword));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TimepointPackage.EVOLUTIONARY_EVENT__KEYWORD:
				return basicSetKeyword(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TimepointPackage.EVOLUTIONARY_EVENT__REFERENCED_FEATURE:
				if (resolve) return getReferencedFeature();
				return basicGetReferencedFeature();
			case TimepointPackage.EVOLUTIONARY_EVENT__KEYWORD:
				return getKeyword();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TimepointPackage.EVOLUTIONARY_EVENT__REFERENCED_FEATURE:
				setReferencedFeature((DwFeature)newValue);
				return;
			case TimepointPackage.EVOLUTIONARY_EVENT__KEYWORD:
				setKeyword((EventKeyword)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TimepointPackage.EVOLUTIONARY_EVENT__REFERENCED_FEATURE:
				setReferencedFeature((DwFeature)null);
				return;
			case TimepointPackage.EVOLUTIONARY_EVENT__KEYWORD:
				setKeyword((EventKeyword)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TimepointPackage.EVOLUTIONARY_EVENT__REFERENCED_FEATURE:
				return referencedFeature != null;
			case TimepointPackage.EVOLUTIONARY_EVENT__KEYWORD:
				return keyword != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == FeatureReference.class) {
			switch (derivedFeatureID) {
				case TimepointPackage.EVOLUTIONARY_EVENT__REFERENCED_FEATURE: return PropertyPackage.FEATURE_REFERENCE__REFERENCED_FEATURE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == FeatureReference.class) {
			switch (baseFeatureID) {
				case PropertyPackage.FEATURE_REFERENCE__REFERENCED_FEATURE: return TimepointPackage.EVOLUTIONARY_EVENT__REFERENCED_FEATURE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //EvolutionaryEventImpl
