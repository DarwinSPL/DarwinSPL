package de.darwinspl.fmec.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.internal.resources.ResourceException;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.EObject;

import de.christophseidl.util.ecore.EcoreIOUtil;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.util.custom.DwFeatureUtil;
import de.darwinspl.fmec.FeatureModelEvolutionConstraintModel;

public class DwTemporalConstraintsUtil {
	
	// threshhold for cache map overrides
	private static final Long REFRESH_RATE = 1000L; // 1 sec.

	private static Map<DwTemporalFeatureModel, List<FeatureModelEvolutionConstraintModel>> tfmToConstraintsCacheMap = new HashMap<>();
	private static Map<DwTemporalFeatureModel, Long> tfmToConstraintsCacheMapUpdates = new HashMap<>();

	
	
	public static List<FeatureModelEvolutionConstraintModel> getFMECModelsForTFM(DwTemporalFeatureModel temporalFM) throws CoreException {
		Long lastCacheMapUpdateForGivenTFM = tfmToConstraintsCacheMapUpdates.get(temporalFM);
		if(lastCacheMapUpdateForGivenTFM != null && System.currentTimeMillis() - lastCacheMapUpdateForGivenTFM < REFRESH_RATE) {
			List<FeatureModelEvolutionConstraintModel> cachedList = tfmToConstraintsCacheMap.get(temporalFM);
			if(cachedList != null)
				return cachedList;
		}
		
		IFile featureModelFile = EcoreIOUtil.getFile(temporalFM.eResource());
		if(featureModelFile == null)
			return null;
		IProject project = featureModelFile.getProject();
		
		List<FeatureModelEvolutionConstraintModel> matchingConstraintModels = new ArrayList<>();
		searchForConstraintModels(project, matchingConstraintModels, temporalFM);
		
		// update cache map
		tfmToConstraintsCacheMap.put(temporalFM, matchingConstraintModels);
		tfmToConstraintsCacheMapUpdates.put(temporalFM, System.currentTimeMillis());
		return matchingConstraintModels;
	}

	
	
	private static void searchForConstraintModels(IContainer parent, List<FeatureModelEvolutionConstraintModel> matchingModels, DwTemporalFeatureModel temporalFM) throws CoreException {
		for(IResource member : parent.members()) {
			if(member instanceof IContainer)
				searchForConstraintModels((IContainer) member, matchingModels, temporalFM);
			
			if(member instanceof IFile) {
				IFile file = (IFile) member;
				String temporalConstraintsFileExtension = DwFeatureUtil.getTemporalConstraintModelFileExtensionForXmi();
				if(file.getFileExtension().equalsIgnoreCase(temporalConstraintsFileExtension)) {
					EObject constraintsModel = EcoreIOUtil.loadModel((IFile) file);
					if(constraintsModel instanceof FeatureModelEvolutionConstraintModel) {
						DwTemporalFeatureModel referencedTFM = ((FeatureModelEvolutionConstraintModel) constraintsModel).getFeatureModel();
						if(referencedTFM.eResource() != null)
							if(temporalFM.eResource().getURI().equals(referencedTFM.eResource().getURI()))
								matchingModels.add((FeatureModelEvolutionConstraintModel) constraintsModel);
					}
					else
						throw new ResourceException(0, file.getFullPath(),
								"File with extension \"" + temporalConstraintsFileExtension + "\" did not contain expected model!",
								new RuntimeException());
				}
			}
		}
	}

}


