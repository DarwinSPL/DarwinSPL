SYNTAXDEF fmec
FOR <http://www.darwinspl.de/feature-model-evolution-constraints/1.0>
START FeatureModelEvolutionConstraintModel

IMPORTS {
	constraints : <http://www.darwinspl.de/feature-model-evolution-constraints/constraints/1.0>
	property : <http://www.darwinspl.de/feature-model-evolution-constraints/property/1.0>
	timepoint : <http://www.darwinspl.de/feature-model-evolution-constraints/timepoint/1.0>
	
	keywordlogical : <http://www.darwinspl.de/feature-model-evolution-constraints/keywords/logical/1.0>
	keywordbuildingblocks : <http://www.darwinspl.de/feature-model-evolution-constraints/keywords/buildingblocks/1.0>
	keywordtimepoint : <http://www.darwinspl.de/feature-model-evolution-constraints/keywords/timepoint/1.0>
	keywordinterval : <http://www.darwinspl.de/feature-model-evolution-constraints/keywords/interval/1.0>
}

OPTIONS {
	reloadGeneratorModel = "true";
	generateCodeFromGeneratorModel = "false";
	
	usePredefinedTokens = "false";
	defaultTokenName = "IDENTIFIER";
	
	disableLaunchSupport = "true";
	disableDebugSupport = "true";
	disableNewProjectWizard = "true";
	disableBuilder = "true";
	
	overrideNewFileContentProvider = "false";
	overrideUIPluginXML = "false";
}

TOKENS {
	DEFINE SL_COMMENT $'//'(~('\n'|'\r'|'\uffff'))* $;
	DEFINE ML_COMMENT $'/*'.*'*/'$;
	
	DEFINE WHITESPACE $(' '|'\t'|'\f'|'\r'|'\n')+$;
	DEFINE IDENTIFIER $'"'('A'..'Z'|'a'..'z'|'0'..'9'|'_'|'-'|' ')+'"'$;
	
	DEFINE PATH_TO_FILE $'<' ('A'..'Z'|'a'..'z'|'0'..'9'|'_'|' '|'/')+ '.tfm>'$;
	
	DEFINE DATE $('0'..'3')?('0'..'9') '.' ('0'..'1')?('0'..'9') '.' ('0'..'2')('0'..'9')('0'..'9')('0'..'9') ((' '|'\t'|'\f') ('0'..'2')?('0'..'9') ':' ('0'..'5')('0'..'9'))?$;
	DEFINE INTEGER $('0'|'1'..'9''0'..'9'*)$;
}

TOKENSTYLES {
	"ML_COMMENT" COLOR #555555, ITALIC;
	"SL_COMMENT" COLOR #555555, ITALIC;
	
	// Names and references in green: #287233
	"feature", "model" COLOR #287233, BOLD;
	"PATH_TO_FILE" COLOR #287233, ITALIC;
	"IDENTIFIER" COLOR #287233;
	
	// Logical keywords in bold black
	"and", "implies", "or", "not", ";", "{", "}" COLOR #000000, BOLD;
	
	// Property keywords in normal black (italic for types)
	"type", "parentGroup", "parentFeature", "==", "." COLOR #000000;
	"OPTIONAL", "MANDATORY", "AND", "ALTERNATIVE", "OR" COLOR #000000, ITALIC;
	"[", ",", ")" COLOR #000000, BOLD;
	"-", "+", "INTEGER", "DATE", "years", "months", "days", "hours", "minutes" COLOR #000000;
	
	// Building block keywords in purple: #602872
	"starts", "ends", "valid" COLOR #602872, BOLD;
	
	// constraint keywords in blue: #284d72
	"before", "until", "after", "when", "at", "during" COLOR #284d72, BOLD;
}


RULES {
	
	FeatureModelEvolutionConstraintModel ::=
		"feature" "model" featureModel[PATH_TO_FILE] !0!0
		(constraints ";" !0!0)*
	;
	
	
	
	constraints.BinaryConstraint ::= leftChild !0 keyword !0 rightChild;
	
	constraints.UnaryConstraint ::= keyword child;
	
	constraints.NestedConstraint ::= "{" !1 nestedConstraint !0 "}";
	
	constraints.TimePointConstraint ::= preArgument keyword postArgument;
	constraints.TimeIntervalConstraint ::= preArgument keyword "[" startPoint "," endPoint ")";
	
	
	
	property.EvolutionaryProperty		::= property keyword;
	
	property.FeatureExistenceProperty	::= referencedFeature[] ;
	property.FeatureTypeProperty		::= referencedFeature[]	"." "type"			"==" featureType[OPTIONAL : "OPTIONAL", MANDATORY : "MANDATORY"];
	property.FeatureParentProperty		::= referencedFeature[]	"." "parentGroup"	"==" referencedGroup[];
	
	property.GroupExistenceProperty		::= referencedGroup[] ;
	property.GroupTypeProperty 			::= referencedGroup[]	"." "type"			"==" groupType[AND : "AND", ALTERNATIVE : "ALTERNATIVE", OR : "OR"];
	property.GroupParentProperty		::= referencedGroup[]	"." "parentFeature"	"==" referencedFeature[];
	
	
	
	timepoint.TimePoint				::= timePoint offset?;
	timepoint.TimeOffset			::= positiveSignature["+" : "-"] (years[INTEGER] "years")? (months[INTEGER] "months")? (days[INTEGER] "days")? (hours[INTEGER] "hours")? (minutes[INTEGER] "minutes")?;
	timepoint.ExplicitTimePoint		::= date[DATE];
	timepoint.EvolutionaryEvent		::= referencedFeature[] keyword;
	
	
	
	// KEYWORDS
	
	// logical connectors
	keywordlogical.KeywordAnd			::= "and";
	keywordlogical.KeywordOr			::= "or";
	keywordlogical.KeywordImplies		::= "implies";
	keywordlogical.KeywordNot 			::= "not";
	
	// timepoint events
	keywordbuildingblocks.KeywordStarts	::= "starts";
	keywordbuildingblocks.KeywordStops	::= "ends";
	keywordbuildingblocks.KeywordValid	::= "valid";
	
	// timepoint relations
	keywordtimepoint.KeywordBefore		::= "before";
	keywordtimepoint.KeywordUntil		::= "until";
	keywordtimepoint.KeywordAfter		::= "after";
	keywordtimepoint.KeywordWhen		::= "when";
	keywordtimepoint.KeywordAt			::= "at";
	
	// interval relations
	keywordinterval.KeywordDuring 		::= "during";
	
	
}