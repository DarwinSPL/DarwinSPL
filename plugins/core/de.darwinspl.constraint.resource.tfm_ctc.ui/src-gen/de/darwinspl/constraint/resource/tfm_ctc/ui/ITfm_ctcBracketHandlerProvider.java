/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.ui;


/**
 * A provider for BracketHandler objects.
 */
public interface ITfm_ctcBracketHandlerProvider {
	
	/**
	 * Returns the bracket handler.
	 */
	public de.darwinspl.constraint.resource.tfm_ctc.ui.ITfm_ctcBracketHandler getBracketHandler();
	
}
