/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.ui;


/**
 * An enumeration of all position categories.
 */
public enum Tfm_ctcPositionCategory {
	BRACKET, DEFINITION, PROXY;
}
