/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.ui;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.ui.texteditor.MarkerAnnotation;
import org.eclipse.ui.texteditor.ResourceMarkerAnnotationModel;

public class Tfm_ctcAnnotationModel extends ResourceMarkerAnnotationModel {
	
	public Tfm_ctcAnnotationModel(IResource resource) {
		super(resource);
	}
	
	protected MarkerAnnotation createMarkerAnnotation(IMarker marker) {
		return new de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcMarkerAnnotation(marker);
	}
	
}
