/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.ui;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.widgets.Display;

public abstract class AbstractTfm_ctcOutlinePageAction extends Action {
	
	private String preferenceKey = this.getClass().getSimpleName() + ".isChecked";
	
	private de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcOutlinePageTreeViewer treeViewer;
	
	public AbstractTfm_ctcOutlinePageAction(de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcOutlinePageTreeViewer treeViewer, String text, int style) {
		super(text, style);
		this.treeViewer = treeViewer;
	}
	
	public void initialize(String imagePath) {
		ImageDescriptor descriptor = de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcImageProvider.INSTANCE.getImageDescriptor(imagePath);
		setDisabledImageDescriptor(descriptor);
		setImageDescriptor(descriptor);
		setHoverImageDescriptor(descriptor);
		boolean checked = de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcUIPlugin.getDefault().getPreferenceStore().getBoolean(preferenceKey);
		valueChanged(checked, false);
	}
	
	@Override
	public void run() {
		if (keepState()) {
			valueChanged(isChecked(), true);
		} else {
			runBusy(true);
		}
	}
	
	public void runBusy(final boolean on) {
		BusyIndicator.showWhile(Display.getCurrent(), new Runnable() {
			public void run() {
				runInternal(on);
			}
		});
	}
	
	public abstract void runInternal(boolean on);
	
	private void valueChanged(boolean on, boolean store) {
		setChecked(on);
		runBusy(on);
		if (store) {
			de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcUIPlugin.getDefault().getPreferenceStore().setValue(preferenceKey, on);
		}
	}
	
	public boolean keepState() {
		return true;
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcOutlinePageTreeViewer getTreeViewer() {
		return treeViewer;
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcOutlinePageTreeViewerComparator getTreeViewerComparator() {
		return (de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcOutlinePageTreeViewerComparator) treeViewer.getComparator();
	}
	
}
