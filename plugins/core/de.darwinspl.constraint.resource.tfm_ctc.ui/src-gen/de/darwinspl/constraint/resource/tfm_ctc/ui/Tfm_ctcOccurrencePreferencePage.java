/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.ui;


/**
 * The preference page to set the occurrence highlighting is not used anymore.
 * This empty class is only generated to override old existing preference page
 * code to avoid compile errors.
 */
public class Tfm_ctcOccurrencePreferencePage {
}
