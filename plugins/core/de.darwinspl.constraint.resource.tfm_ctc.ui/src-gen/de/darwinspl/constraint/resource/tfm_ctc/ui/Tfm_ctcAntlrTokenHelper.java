/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.ui;


/**
 * This class is only generated for backwards compatiblity. The original contents
 * of this class have been moved to class
 * de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcAntlrTokenHelper.
 */
public class Tfm_ctcAntlrTokenHelper {
	// This class is intentionally left empty.
}
