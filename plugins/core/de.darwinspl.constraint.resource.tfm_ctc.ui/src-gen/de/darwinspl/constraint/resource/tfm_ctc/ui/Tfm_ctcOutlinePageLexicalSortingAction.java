/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.ui;

import org.eclipse.jface.action.IAction;

public class Tfm_ctcOutlinePageLexicalSortingAction extends de.darwinspl.constraint.resource.tfm_ctc.ui.AbstractTfm_ctcOutlinePageAction {
	
	public Tfm_ctcOutlinePageLexicalSortingAction(de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Sort alphabetically", IAction.AS_CHECK_BOX);
		initialize("icons/sort_lexically_icon.gif");
	}
	
	public void runInternal(boolean on) {
		getTreeViewerComparator().setSortLexically(on);
		getTreeViewer().refresh();
	}
	
}
