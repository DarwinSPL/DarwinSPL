/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.ui.launch;


/**
 * <p>
 * A class that provides the main tab to parameterize launch configurations
 * (currently disabled).
 * </p>
 * <p>
 * Set the overrideLaunchConfigurationMainTab option to false to customize this
 * class.
 * </p>
 */
public class Tfm_ctcLaunchConfigurationMainTab {
	
}
