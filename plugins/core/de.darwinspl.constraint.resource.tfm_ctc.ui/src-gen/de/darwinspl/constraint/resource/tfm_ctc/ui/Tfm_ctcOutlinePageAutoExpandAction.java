/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.ui;

import org.eclipse.jface.action.IAction;

public class Tfm_ctcOutlinePageAutoExpandAction extends de.darwinspl.constraint.resource.tfm_ctc.ui.AbstractTfm_ctcOutlinePageAction {
	
	public Tfm_ctcOutlinePageAutoExpandAction(de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Auto expand", IAction.AS_CHECK_BOX);
		initialize("icons/auto_expand_icon.gif");
	}
	
	public void runInternal(boolean on) {
		getTreeViewer().setAutoExpand(on);
		getTreeViewer().refresh();
	}
	
}
