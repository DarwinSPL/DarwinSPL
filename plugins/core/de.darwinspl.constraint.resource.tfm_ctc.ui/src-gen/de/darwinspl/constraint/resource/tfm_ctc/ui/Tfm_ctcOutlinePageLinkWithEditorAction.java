/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.ui;

import org.eclipse.jface.action.IAction;

public class Tfm_ctcOutlinePageLinkWithEditorAction extends de.darwinspl.constraint.resource.tfm_ctc.ui.AbstractTfm_ctcOutlinePageAction {
	
	public Tfm_ctcOutlinePageLinkWithEditorAction(de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Link with Editor", IAction.AS_CHECK_BOX);
		initialize("icons/link_with_editor_icon.gif");
	}
	
	public void runInternal(boolean on) {
		getTreeViewer().setLinkWithEditor(on);
	}
	
}
