/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.ui.launch;


/**
 * <p>
 * A class that converts the current selection or active editor to a launch
 * configuration (currently disabled).
 * </p>
 * <p>
 * Set the overrideLaunchShortcut option to false to customize this class.
 * </p>
 */
public class Tfm_ctcLaunchShortcut {
	
}
