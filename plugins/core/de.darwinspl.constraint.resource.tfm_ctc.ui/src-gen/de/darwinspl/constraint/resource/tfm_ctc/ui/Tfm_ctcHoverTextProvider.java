/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.ui;

import org.eclipse.emf.ecore.EObject;

public class Tfm_ctcHoverTextProvider implements de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcHoverTextProvider {
	
	private de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcDefaultHoverTextProvider defaultProvider = new de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcDefaultHoverTextProvider();
	
	public String getHoverText(EObject container, EObject referencedObject) {
		// Set option overrideHoverTextProvider to false and customize this method to
		// obtain custom hover texts.
		return defaultProvider.getHoverText(referencedObject);
	}
	
	public String getHoverText(EObject object) {
		// Set option overrideHoverTextProvider to false and customize this method to
		// obtain custom hover texts.
		return defaultProvider.getHoverText(object);
	}
	
}
