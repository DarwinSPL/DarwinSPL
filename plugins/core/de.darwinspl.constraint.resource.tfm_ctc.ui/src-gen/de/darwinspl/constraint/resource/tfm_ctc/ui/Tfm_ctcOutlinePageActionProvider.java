/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.ui;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.jface.action.IAction;

public class Tfm_ctcOutlinePageActionProvider {
	
	public List<IAction> getActions(de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcOutlinePageTreeViewer treeViewer) {
		// To add custom actions to the outline view, set the
		// 'overrideOutlinePageActionProvider' option to <code>false</code> and modify
		// this method.
		List<IAction> defaultActions = new ArrayList<IAction>();
		defaultActions.add(new de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcOutlinePageLinkWithEditorAction(treeViewer));
		defaultActions.add(new de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcOutlinePageCollapseAllAction(treeViewer));
		defaultActions.add(new de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcOutlinePageExpandAllAction(treeViewer));
		defaultActions.add(new de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcOutlinePageAutoExpandAction(treeViewer));
		defaultActions.add(new de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcOutlinePageLexicalSortingAction(treeViewer));
		defaultActions.add(new de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcOutlinePageTypeSortingAction(treeViewer));
		return defaultActions;
	}
	
}
