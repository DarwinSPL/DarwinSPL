/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.ui;

import org.eclipse.jface.action.IAction;

public class Tfm_ctcOutlinePageTypeSortingAction extends de.darwinspl.constraint.resource.tfm_ctc.ui.AbstractTfm_ctcOutlinePageAction {
	
	public Tfm_ctcOutlinePageTypeSortingAction(de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Group types", IAction.AS_CHECK_BOX);
		initialize("icons/group_types_icon.gif");
	}
	
	public void runInternal(boolean on) {
		getTreeViewerComparator().setGroupTypes(on);
		getTreeViewer().refresh();
	}
	
}
