/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.ui;


/**
 * This class is deprecated and not used as of EMFText 1.4.1. The original
 * contents of this class have been moved to
 * de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcSourceViewerConfiguration.
 * This class is only generated to avoid compile errors with existing versions of
 * this class.
 */
@Deprecated
public class Tfm_ctcEditorConfiguration {
	
}
