/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.ui;

import org.eclipse.core.resources.IResource;

public class Tfm_ctcUIMetaInformation extends de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcMetaInformation {
	
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcHoverTextProvider getHoverTextProvider() {
		return new de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcHoverTextProvider();
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcImageProvider getImageProvider() {
		return de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcImageProvider.INSTANCE;
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcColorManager createColorManager() {
		return new de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcColorManager();
	}
	
	/**
	 * @deprecated this method is only provided to preserve API compatibility. Use
	 * createTokenScanner(de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource
	 * , de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcColorManager) instead.
	 */
	public de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcTokenScanner createTokenScanner(de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcColorManager colorManager) {
		return (de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcTokenScanner) createTokenScanner(null, colorManager);
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.ui.ITfm_ctcTokenScanner createTokenScanner(de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource resource, de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcColorManager colorManager) {
		return new de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcTokenScanner(resource, colorManager);
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcCodeCompletionHelper createCodeCompletionHelper() {
		return new de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcCodeCompletionHelper();
	}
	
	@SuppressWarnings("rawtypes")
	public Object createResourceAdapter(Object adaptableObject, Class adapterType, IResource resource) {
		return new de.darwinspl.constraint.resource.tfm_ctc.ui.debug.Tfm_ctcLineBreakpointAdapter();
	}
	
}
