/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.ui;

import org.eclipse.jface.text.rules.ITokenScanner;

public interface ITfm_ctcTokenScanner extends ITokenScanner {
	
	public String getTokenText();
	
}
