/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.ui;

import org.eclipse.jface.text.AbstractReusableInformationControlCreator;
import org.eclipse.jface.text.DefaultInformationControl;
import org.eclipse.jface.text.DefaultInformationControl.IInformationPresenter;
import org.eclipse.jface.text.IInformationControl;
import org.eclipse.jface.text.quickassist.IQuickAssistAssistant;
import org.eclipse.jface.text.quickassist.IQuickAssistInvocationContext;
import org.eclipse.jface.text.quickassist.QuickAssistAssistant;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.swt.widgets.Shell;

public class Tfm_ctcQuickAssistAssistant extends QuickAssistAssistant implements IQuickAssistAssistant {
	
	public Tfm_ctcQuickAssistAssistant(de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcResourceProvider resourceProvider, de.darwinspl.constraint.resource.tfm_ctc.ui.ITfm_ctcAnnotationModelProvider annotationModelProvider) {
		setQuickAssistProcessor(new de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcQuickAssistProcessor(resourceProvider, annotationModelProvider));
		setInformationControlCreator(new AbstractReusableInformationControlCreator() {
			public IInformationControl doCreateInformationControl(Shell parent) {
				return new DefaultInformationControl(parent, (IInformationPresenter) null);
			}
		});
	}
	
	public boolean canAssist(IQuickAssistInvocationContext invocationContext) {
		return false;
	}
	
	public boolean canFix(Annotation annotation) {
		return true;
	}
	
}
