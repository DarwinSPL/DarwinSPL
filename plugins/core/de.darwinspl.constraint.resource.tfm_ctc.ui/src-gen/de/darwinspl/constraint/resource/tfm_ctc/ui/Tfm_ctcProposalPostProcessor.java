/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.ui;

import java.util.List;

/**
 * A class which can be overridden to customize code completion proposals.
 */
public class Tfm_ctcProposalPostProcessor {
	
	public List<de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcCompletionProposal> process(List<de.darwinspl.constraint.resource.tfm_ctc.ui.Tfm_ctcCompletionProposal> proposals) {
		// the default implementation does returns the proposals as they are
		return proposals;
	}
	
}
