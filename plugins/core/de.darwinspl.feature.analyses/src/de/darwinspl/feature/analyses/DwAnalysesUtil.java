package de.darwinspl.feature.analyses;

import java.util.Date;
import java.util.List;

import de.darwinspl.constraint.DwConstraint;
import de.darwinspl.constraint.DwConstraintModel;
import de.darwinspl.expression.DwAndExpression;
import de.darwinspl.expression.DwBinaryExpression;
import de.darwinspl.expression.DwBooleanValueExpression;
import de.darwinspl.expression.DwEquivalenceExpression;
import de.darwinspl.expression.DwExpression;
import de.darwinspl.expression.DwFeatureReferenceExpression;
import de.darwinspl.expression.DwImpliesExpression;
import de.darwinspl.expression.DwNestedExpression;
import de.darwinspl.expression.DwNotExpression;
import de.darwinspl.expression.DwOrExpression;
import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureType;
import de.darwinspl.feature.DwFeatureTypeEnum;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupType;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.configuration.DwConfiguration;
import de.darwinspl.feature.configuration.util.DwConfigurationUtil;
import de.darwinspl.feature.util.custom.DwFeatureUtil;
import de.darwinspl.temporal.util.DwEvolutionUtil;


/**
 * Currently without evolution support of configurations
 * @author Michael Nieke
 *
 */
public class DwAnalysesUtil {

	
	public static boolean checkConfigurationValidity(DwTemporalFeatureModel featureModel, List<DwConstraintModel> constraintModels, DwConfiguration configuration, Date date) {
		if (!satisfiesTreeConstraints(featureModel, configuration, date)) {
			return false;
		}
		
		for (DwConstraintModel constraintModel : constraintModels) {
			for (DwConstraint constraint : DwEvolutionUtil.getValidTemporalElements(constraintModel.getConstraints(), date)) {
				if (!satisfiesConstraint(constraint, configuration)) {
					return false;
				}
			}
		}
		
		return true;
	}
	
	public static boolean satisfiesTreeConstraints(DwTemporalFeatureModel featureModel, DwConfiguration configuration, Date date) {
		List<DwFeature> selectedFeatures = DwConfigurationUtil.getSelectedFeatures(configuration.getFeatureConfigurations());
		
		DwFeature rootFeature = DwFeatureUtil.getRootFeature(featureModel, date);
		if (!selectedFeatures.contains(rootFeature)) {
			return false;
		}
		
		for(DwGroup childGroup : DwFeatureUtil.getChildGroupsOfFeature(rootFeature, date)) {
			if(!satisfiesGroupConstraints(childGroup, selectedFeatures, date)) {
				return false;
			}
		}
		
		for (DwFeature selectedFeature : selectedFeatures) {
			if (selectedFeature.equals(rootFeature)) {
				continue;
			}
			DwFeature parentFeature = DwFeatureUtil.getParentFeatureOfFeature(selectedFeature, date);
			if (!selectedFeatures.contains(parentFeature)) {
				return false;
			}
		}
		
		return true;
	}
	
	private static boolean satisfiesGroupConstraints(DwGroup group, List<DwFeature> selectedFeatures, Date date) {
		DwGroupType groupType = DwFeatureUtil.getType(group, date);
		
		List<DwFeature> groupFeatures = DwFeatureUtil.getChildFeaturesOfGroup(group, date);
		
		switch(groupType.getType()) {
		
		case ALTERNATIVE:
			if (countSelectedFeaturesOfGroup(groupFeatures, selectedFeatures) != 1) {
				return false;
			}
			break;
			
		case AND:
			for (DwFeature groupFeature : groupFeatures) {
				DwFeatureType featureType = DwFeatureUtil.getType(groupFeature, date);
				if (featureType.getType().equals(DwFeatureTypeEnum.MANDATORY)) {
					if (!selectedFeatures.contains(groupFeature)) {
						return false;
					}
				}
			}
			break;
			
		case OR:
			if (countSelectedFeaturesOfGroup(groupFeatures, selectedFeatures) == 0) {
				return false;
			}
			break;
		}
		
		for (DwFeature groupFeature : groupFeatures) {
			if (selectedFeatures.contains(groupFeature)) {
				for(DwGroup childGroup : DwFeatureUtil.getChildGroupsOfFeature(groupFeature, date)) {
					if(!satisfiesGroupConstraints(childGroup, selectedFeatures, date)) {
						return false;
					}
				}
			}
		}
		
		return true;
	}
	
	private static int countSelectedFeaturesOfGroup(List<DwFeature> groupFeatures, List<DwFeature> selectedFeatures) {
		int count = 0;
		
		for (DwFeature groupFeature : groupFeatures) {
			if (selectedFeatures.contains(groupFeature)) {
				count++;
			}
		}
		
		return count;
	}
	
	public static boolean satisfiesConstraint(DwConstraint constraint, DwConfiguration configuration) {
		return satisfiesConstraint(constraint, DwConfigurationUtil.getSelectedFeatures(configuration.getFeatureConfigurations()));
	}
	
	public static boolean satisfiesConstraint(DwConstraint constraint, List<DwFeature> selectedFeatures) {
		return isSatisfiedByFeatureSelection(constraint.getRootExpression(), selectedFeatures);
	}
	
	
	public static boolean isSatisfiedByConfiguration(DwExpression expression, DwConfiguration configuration) {		
		return isSatisfiedByFeatureSelection(expression, DwConfigurationUtil.getSelectedFeatures(configuration.getFeatureConfigurations()));
	}
	
	public static boolean isSatisfiedByFeatureSelection(DwExpression expression, List<DwFeature> selectedFeatures) {
		if (expression instanceof DwFeatureReferenceExpression) {
			if (selectedFeatures.contains(((DwFeatureReferenceExpression) expression).getFeature())) {
				return true;
			}
			else {
				return false;
			}
		}
		else if (expression instanceof DwBooleanValueExpression) {
			return ((DwBooleanValueExpression) expression).isValue();
		}
		else if (expression instanceof DwNestedExpression) {
			return isSatisfiedByFeatureSelection(((DwNestedExpression) expression).getOperand(), selectedFeatures);
		}
		else if (expression instanceof DwNotExpression) {
			return !isSatisfiedByFeatureSelection(((DwNotExpression) expression).getOperand(), selectedFeatures);
		}
		else if (expression instanceof DwBinaryExpression) {
			DwExpression operand1 = ((DwBinaryExpression) expression).getOperand1();
			DwExpression operand2 = ((DwBinaryExpression) expression).getOperand2();
			
			if (expression instanceof DwAndExpression) {
				return (isSatisfiedByFeatureSelection(operand1, selectedFeatures) && isSatisfiedByFeatureSelection(operand2, selectedFeatures));
			}
			else if (expression instanceof DwOrExpression) {
				return (isSatisfiedByFeatureSelection(operand1, selectedFeatures) || isSatisfiedByFeatureSelection(operand2, selectedFeatures));
			}
			else if (expression instanceof DwImpliesExpression) {
				return (!isSatisfiedByFeatureSelection(operand1, selectedFeatures) || isSatisfiedByFeatureSelection(operand2, selectedFeatures));
			}
			else if (expression instanceof DwEquivalenceExpression) {
				return (isSatisfiedByFeatureSelection(operand1, selectedFeatures) == isSatisfiedByFeatureSelection(operand2, selectedFeatures));
			}
		}
		
		return false;
	}
	
}
