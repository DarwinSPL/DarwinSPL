/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.mopp;

import org.eclipse.emf.ecore.EClass;

public class FmecSyntaxCoverageInformationProvider {
	
	public EClass[] getClassesWithSyntax() {
		return new EClass[] {
			de.darwinspl.fmec.FmecPackage.eINSTANCE.getFeatureModelEvolutionConstraintModel(),
			de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getBinaryConstraint(),
			de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getUnaryConstraint(),
			de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getNestedConstraint(),
			de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(),
			de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(),
			de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getEvolutionaryProperty(),
			de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getFeatureExistenceProperty(),
			de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getFeatureTypeProperty(),
			de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getFeatureParentProperty(),
			de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getGroupExistenceProperty(),
			de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getGroupTypeProperty(),
			de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getGroupParentProperty(),
			de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimePoint(),
			de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimeOffset(),
			de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getExplicitTimePoint(),
			de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getEvolutionaryEvent(),
			de.darwinspl.fmec.keywords.logical.LogicalPackage.eINSTANCE.getKeywordAnd(),
			de.darwinspl.fmec.keywords.logical.LogicalPackage.eINSTANCE.getKeywordOr(),
			de.darwinspl.fmec.keywords.logical.LogicalPackage.eINSTANCE.getKeywordImplies(),
			de.darwinspl.fmec.keywords.logical.LogicalPackage.eINSTANCE.getKeywordNot(),
			de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage.eINSTANCE.getKeywordStarts(),
			de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage.eINSTANCE.getKeywordStops(),
			de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage.eINSTANCE.getKeywordValid(),
			de.darwinspl.fmec.keywords.timepoint.TimepointPackage.eINSTANCE.getKeywordBefore(),
			de.darwinspl.fmec.keywords.timepoint.TimepointPackage.eINSTANCE.getKeywordUntil(),
			de.darwinspl.fmec.keywords.timepoint.TimepointPackage.eINSTANCE.getKeywordAfter(),
			de.darwinspl.fmec.keywords.timepoint.TimepointPackage.eINSTANCE.getKeywordWhen(),
			de.darwinspl.fmec.keywords.timepoint.TimepointPackage.eINSTANCE.getKeywordAt(),
			de.darwinspl.fmec.keywords.interval.IntervalPackage.eINSTANCE.getKeywordDuring(),
		};
	}
	
	public EClass[] getStartSymbols() {
		return new EClass[] {
			de.darwinspl.fmec.FmecPackage.eINSTANCE.getFeatureModelEvolutionConstraintModel(),
		};
	}
	
}
