/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.mopp;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import org.eclipse.emf.ecore.EClass;

/**
 * Abstract super class for all expected elements. Provides methods to add
 * followers.
 */
public abstract class FmecAbstractExpectedElement implements de.darwinspl.fmec.resource.fmec.IFmecExpectedElement {
	
	private EClass ruleMetaclass;
	
	private Set<de.darwinspl.fmec.resource.fmec.util.FmecPair<de.darwinspl.fmec.resource.fmec.IFmecExpectedElement, de.darwinspl.fmec.resource.fmec.mopp.FmecContainedFeature[]>> followers = new LinkedHashSet<de.darwinspl.fmec.resource.fmec.util.FmecPair<de.darwinspl.fmec.resource.fmec.IFmecExpectedElement, de.darwinspl.fmec.resource.fmec.mopp.FmecContainedFeature[]>>();
	
	public FmecAbstractExpectedElement(EClass ruleMetaclass) {
		super();
		this.ruleMetaclass = ruleMetaclass;
	}
	
	public EClass getRuleMetaclass() {
		return ruleMetaclass;
	}
	
	public void addFollower(de.darwinspl.fmec.resource.fmec.IFmecExpectedElement follower, de.darwinspl.fmec.resource.fmec.mopp.FmecContainedFeature[] path) {
		followers.add(new de.darwinspl.fmec.resource.fmec.util.FmecPair<de.darwinspl.fmec.resource.fmec.IFmecExpectedElement, de.darwinspl.fmec.resource.fmec.mopp.FmecContainedFeature[]>(follower, path));
	}
	
	public Collection<de.darwinspl.fmec.resource.fmec.util.FmecPair<de.darwinspl.fmec.resource.fmec.IFmecExpectedElement, de.darwinspl.fmec.resource.fmec.mopp.FmecContainedFeature[]>> getFollowers() {
		return followers;
	}
	
}
