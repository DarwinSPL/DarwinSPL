/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.grammar;


public class FmecWhiteSpace extends de.darwinspl.fmec.resource.fmec.grammar.FmecFormattingElement {
	
	private final int amount;
	
	public FmecWhiteSpace(int amount, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality cardinality) {
		super(cardinality);
		this.amount = amount;
	}
	
	public int getAmount() {
		return amount;
	}
	
	public String toString() {
		return "#" + getAmount();
	}
	
}
