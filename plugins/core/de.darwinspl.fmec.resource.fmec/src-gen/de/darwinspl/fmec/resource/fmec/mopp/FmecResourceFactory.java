/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.mopp;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;

public class FmecResourceFactory implements Resource.Factory {
	
	public FmecResourceFactory() {
		super();
	}
	
	public Resource createResource(URI uri) {
		return new de.darwinspl.fmec.resource.fmec.mopp.FmecResource(uri);
	}
	
}
