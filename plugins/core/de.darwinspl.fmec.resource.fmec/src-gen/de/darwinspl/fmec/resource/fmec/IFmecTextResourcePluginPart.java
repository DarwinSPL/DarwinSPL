/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec;


/**
 * This interface is extended by some other generated classes. It provides access
 * to the plug-in meta information.
 */
public interface IFmecTextResourcePluginPart {
	
	/**
	 * Returns a meta information object for the language plug-in that contains this
	 * part.
	 */
	public de.darwinspl.fmec.resource.fmec.IFmecMetaInformation getMetaInformation();
	
}
