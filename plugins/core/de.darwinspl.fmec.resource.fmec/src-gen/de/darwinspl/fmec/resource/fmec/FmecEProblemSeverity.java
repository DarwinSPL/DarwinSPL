/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec;


public enum FmecEProblemSeverity {
	WARNING, ERROR;
}
