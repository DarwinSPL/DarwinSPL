/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.mopp;


public class FmecTokenStyleInformationProvider {
	
	public static String TASK_ITEM_TOKEN_NAME = "TASK_ITEM";
	
	public de.darwinspl.fmec.resource.fmec.IFmecTokenStyle getDefaultTokenStyle(String tokenName) {
		if ("ML_COMMENT".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x55, 0x55, 0x55}, null, false, true, false, false);
		}
		if ("SL_COMMENT".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x55, 0x55, 0x55}, null, false, true, false, false);
		}
		if ("feature".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x28, 0x72, 0x33}, null, true, false, false, false);
		}
		if ("model".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x28, 0x72, 0x33}, null, true, false, false, false);
		}
		if ("PATH_TO_FILE".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x28, 0x72, 0x33}, null, false, true, false, false);
		}
		if ("IDENTIFIER".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x28, 0x72, 0x33}, null, false, false, false, false);
		}
		if ("and".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, true, false, false, false);
		}
		if ("implies".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, true, false, false, false);
		}
		if ("or".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, true, false, false, false);
		}
		if ("not".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, true, false, false, false);
		}
		if (";".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, true, false, false, false);
		}
		if ("{".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, true, false, false, false);
		}
		if ("}".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, true, false, false, false);
		}
		if ("type".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, false, false, false, false);
		}
		if ("parentGroup".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, false, false, false, false);
		}
		if ("parentFeature".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, false, false, false, false);
		}
		if ("==".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, false, false, false, false);
		}
		if (".".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, false, false, false, false);
		}
		if ("OPTIONAL".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, false, true, false, false);
		}
		if ("MANDATORY".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, false, true, false, false);
		}
		if ("AND".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, false, true, false, false);
		}
		if ("ALTERNATIVE".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, false, true, false, false);
		}
		if ("OR".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, false, true, false, false);
		}
		if ("[".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, true, false, false, false);
		}
		if (",".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, true, false, false, false);
		}
		if (")".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, true, false, false, false);
		}
		if ("-".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, false, false, false, false);
		}
		if ("+".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, false, false, false, false);
		}
		if ("INTEGER".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, false, false, false, false);
		}
		if ("DATE".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, false, false, false, false);
		}
		if ("years".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, false, false, false, false);
		}
		if ("months".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, false, false, false, false);
		}
		if ("days".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, false, false, false, false);
		}
		if ("hours".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, false, false, false, false);
		}
		if ("minutes".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x00, 0x00, 0x00}, null, false, false, false, false);
		}
		if ("starts".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x60, 0x28, 0x72}, null, true, false, false, false);
		}
		if ("ends".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x60, 0x28, 0x72}, null, true, false, false, false);
		}
		if ("valid".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x60, 0x28, 0x72}, null, true, false, false, false);
		}
		if ("before".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x28, 0x4d, 0x72}, null, true, false, false, false);
		}
		if ("until".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x28, 0x4d, 0x72}, null, true, false, false, false);
		}
		if ("after".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x28, 0x4d, 0x72}, null, true, false, false, false);
		}
		if ("when".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x28, 0x4d, 0x72}, null, true, false, false, false);
		}
		if ("at".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x28, 0x4d, 0x72}, null, true, false, false, false);
		}
		if ("during".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x28, 0x4d, 0x72}, null, true, false, false, false);
		}
		if ("TASK_ITEM".equals(tokenName)) {
			return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyle(new int[] {0x7F, 0x9F, 0xBF}, null, true, false, false, false);
		}
		return null;
	}
	
}
