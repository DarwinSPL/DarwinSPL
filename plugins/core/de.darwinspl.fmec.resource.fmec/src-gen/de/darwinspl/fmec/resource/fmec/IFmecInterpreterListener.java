/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec;

import org.eclipse.emf.ecore.EObject;

public interface IFmecInterpreterListener {
	
	public void handleInterpreteObject(EObject element);
}
