// $ANTLR 3.4

	package de.darwinspl.fmec.resource.fmec.mopp;
	
	import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.antlr.runtime3_4_0.ANTLRInputStream;
import org.antlr.runtime3_4_0.BitSet;
import org.antlr.runtime3_4_0.CommonToken;
import org.antlr.runtime3_4_0.CommonTokenStream;
import org.antlr.runtime3_4_0.IntStream;
import org.antlr.runtime3_4_0.Lexer;
import org.antlr.runtime3_4_0.RecognitionException;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;


import org.antlr.runtime3_4_0.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class FmecParser extends FmecANTLRParserBase {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "DATE", "IDENTIFIER", "INTEGER", "ML_COMMENT", "PATH_TO_FILE", "SL_COMMENT", "WHITESPACE", "')'", "'+'", "','", "'-'", "'.'", "';'", "'=='", "'ALTERNATIVE'", "'AND'", "'MANDATORY'", "'OPTIONAL'", "'OR'", "'['", "'after'", "'and'", "'at'", "'before'", "'days'", "'during'", "'ends'", "'feature'", "'hours'", "'implies'", "'minutes'", "'model'", "'months'", "'not'", "'or'", "'parentFeature'", "'parentGroup'", "'starts'", "'type'", "'until'", "'valid'", "'when'", "'years'", "'{'", "'}'"
    };

    public static final int EOF=-1;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__19=19;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__48=48;
    public static final int DATE=4;
    public static final int IDENTIFIER=5;
    public static final int INTEGER=6;
    public static final int ML_COMMENT=7;
    public static final int PATH_TO_FILE=8;
    public static final int SL_COMMENT=9;
    public static final int WHITESPACE=10;

    // delegates
    public FmecANTLRParserBase[] getDelegates() {
        return new FmecANTLRParserBase[] {};
    }

    // delegators


    public FmecParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public FmecParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
        this.state.initializeRuleMemo(77 + 1);
         

    }

    public String[] getTokenNames() { return FmecParser.tokenNames; }
    public String getGrammarFileName() { return "Fmec.g"; }


    	private de.darwinspl.fmec.resource.fmec.IFmecTokenResolverFactory tokenResolverFactory = new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenResolverFactory();
    	
    	/**
    	 * the index of the last token that was handled by collectHiddenTokens()
    	 */
    	private int lastPosition;
    	
    	/**
    	 * A flag that indicates whether the parser should remember all expected elements.
    	 * This flag is set to true when using the parse for code completion. Otherwise it
    	 * is set to false.
    	 */
    	private boolean rememberExpectedElements = false;
    	
    	private Object parseToIndexTypeObject;
    	private int lastTokenIndex = 0;
    	
    	/**
    	 * A list of expected elements the were collected while parsing the input stream.
    	 * This list is only filled if <code>rememberExpectedElements</code> is set to
    	 * true.
    	 */
    	private List<de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal> expectedElements = new ArrayList<de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal>();
    	
    	private int mismatchedTokenRecoveryTries = 0;
    	/**
    	 * A helper list to allow a lexer to pass errors to its parser
    	 */
    	protected List<RecognitionException> lexerExceptions = Collections.synchronizedList(new ArrayList<RecognitionException>());
    	
    	/**
    	 * Another helper list to allow a lexer to pass positions of errors to its parser
    	 */
    	protected List<Integer> lexerExceptionPositions = Collections.synchronizedList(new ArrayList<Integer>());
    	
    	/**
    	 * A stack for incomplete objects. This stack is used filled when the parser is
    	 * used for code completion. Whenever the parser starts to read an object it is
    	 * pushed on the stack. Once the element was parser completely it is popped from
    	 * the stack.
    	 */
    	List<EObject> incompleteObjects = new ArrayList<EObject>();
    	
    	private int stopIncludingHiddenTokens;
    	private int stopExcludingHiddenTokens;
    	private int tokenIndexOfLastCompleteElement;
    	
    	private int expectedElementsIndexOfLastCompleteElement;
    	
    	/**
    	 * The offset indicating the cursor position when the parser is used for code
    	 * completion by calling parseToExpectedElements().
    	 */
    	private int cursorOffset;
    	
    	/**
    	 * The offset of the first hidden token of the last expected element. This offset
    	 * is used to discard expected elements, which are not needed for code completion.
    	 */
    	private int lastStartIncludingHidden;
    	
    	private de.darwinspl.fmec.resource.fmec.IFmecLocationMap locationMap;
    	
    	private de.darwinspl.fmec.resource.fmec.mopp.FmecSyntaxErrorMessageConverter syntaxErrorMessageConverter = new de.darwinspl.fmec.resource.fmec.mopp.FmecSyntaxErrorMessageConverter(tokenNames);
    	
    	@Override
    	public void reportError(RecognitionException re) {
    		addErrorToResource(syntaxErrorMessageConverter.translateParseError(re));
    	}
    	
    	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
    		postParseCommands.add(new de.darwinspl.fmec.resource.fmec.IFmecCommand<de.darwinspl.fmec.resource.fmec.IFmecTextResource>() {
    			public boolean execute(de.darwinspl.fmec.resource.fmec.IFmecTextResource resource) {
    				if (resource == null) {
    					// the resource can be null if the parser is used for code completion
    					return true;
    				}
    				resource.addProblem(new de.darwinspl.fmec.resource.fmec.IFmecProblem() {
    					public de.darwinspl.fmec.resource.fmec.FmecEProblemSeverity getSeverity() {
    						return de.darwinspl.fmec.resource.fmec.FmecEProblemSeverity.ERROR;
    					}
    					public de.darwinspl.fmec.resource.fmec.FmecEProblemType getType() {
    						return de.darwinspl.fmec.resource.fmec.FmecEProblemType.SYNTAX_ERROR;
    					}
    					public String getMessage() {
    						return errorMessage;
    					}
    					public Collection<de.darwinspl.fmec.resource.fmec.IFmecQuickFix> getQuickFixes() {
    						return null;
    					}
    				}, column, line, startIndex, stopIndex);
    				return true;
    			}
    		});
    	}
    	
    	protected void addErrorToResource(de.darwinspl.fmec.resource.fmec.mopp.FmecLocalizedMessage message) {
    		if (message == null) {
    			return;
    		}
    		addErrorToResource(message.getMessage(), message.getColumn(), message.getLine(), message.getCharStart(), message.getCharEnd());
    	}
    	
    	public void addExpectedElement(EClass eClass, int expectationStartIndex, int expectationEndIndex) {
    		for (int expectationIndex = expectationStartIndex; expectationIndex <= expectationEndIndex; expectationIndex++) {
    			addExpectedElement(eClass, de.darwinspl.fmec.resource.fmec.mopp.FmecExpectationConstants.EXPECTATIONS[expectationIndex]);
    		}
    	}
    	
    	public void addExpectedElement(EClass eClass, int expectationIndex) {
    		addExpectedElement(eClass, de.darwinspl.fmec.resource.fmec.mopp.FmecExpectationConstants.EXPECTATIONS[expectationIndex]);
    	}
    	
    	public void addExpectedElement(EClass eClass, int[] ids) {
    		if (!this.rememberExpectedElements) {
    			return;
    		}
    		int terminalID = ids[0];
    		int followSetID = ids[1];
    		de.darwinspl.fmec.resource.fmec.IFmecExpectedElement terminal = de.darwinspl.fmec.resource.fmec.grammar.FmecFollowSetProvider.TERMINALS[terminalID];
    		de.darwinspl.fmec.resource.fmec.mopp.FmecContainedFeature[] containmentFeatures = new de.darwinspl.fmec.resource.fmec.mopp.FmecContainedFeature[ids.length - 2];
    		for (int i = 2; i < ids.length; i++) {
    			containmentFeatures[i - 2] = de.darwinspl.fmec.resource.fmec.grammar.FmecFollowSetProvider.LINKS[ids[i]];
    		}
    		de.darwinspl.fmec.resource.fmec.grammar.FmecContainmentTrace containmentTrace = new de.darwinspl.fmec.resource.fmec.grammar.FmecContainmentTrace(eClass, containmentFeatures);
    		EObject container = getLastIncompleteElement();
    		de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal expectedElement = new de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal(container, terminal, followSetID, containmentTrace);
    		setPosition(expectedElement, input.index());
    		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
    		lastStartIncludingHidden = startIncludingHiddenTokens;
    		this.expectedElements.add(expectedElement);
    	}
    	
    	protected void collectHiddenTokens(EObject element) {
    	}
    	
    	protected void copyLocalizationInfos(final EObject source, final EObject target) {
    		if (disableLocationMap) {
    			return;
    		}
    		final de.darwinspl.fmec.resource.fmec.IFmecLocationMap locationMap = this.locationMap;
    		if (locationMap == null) {
    			// the locationMap can be null if the parser is used for code completion
    			return;
    		}
    		postParseCommands.add(new de.darwinspl.fmec.resource.fmec.IFmecCommand<de.darwinspl.fmec.resource.fmec.IFmecTextResource>() {
    			public boolean execute(de.darwinspl.fmec.resource.fmec.IFmecTextResource resource) {
    				locationMap.setCharStart(target, locationMap.getCharStart(source));
    				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
    				locationMap.setColumn(target, locationMap.getColumn(source));
    				locationMap.setLine(target, locationMap.getLine(source));
    				return true;
    			}
    		});
    	}
    	
    	protected void copyLocalizationInfos(final CommonToken source, final EObject target) {
    		if (disableLocationMap) {
    			return;
    		}
    		final de.darwinspl.fmec.resource.fmec.IFmecLocationMap locationMap = this.locationMap;
    		if (locationMap == null) {
    			// the locationMap can be null if the parser is used for code completion
    			return;
    		}
    		postParseCommands.add(new de.darwinspl.fmec.resource.fmec.IFmecCommand<de.darwinspl.fmec.resource.fmec.IFmecTextResource>() {
    			public boolean execute(de.darwinspl.fmec.resource.fmec.IFmecTextResource resource) {
    				if (source == null) {
    					return true;
    				}
    				locationMap.setCharStart(target, source.getStartIndex());
    				locationMap.setCharEnd(target, source.getStopIndex());
    				locationMap.setColumn(target, source.getCharPositionInLine());
    				locationMap.setLine(target, source.getLine());
    				return true;
    			}
    		});
    	}
    	
    	/**
    	 * Sets the end character index and the last line for the given object in the
    	 * location map.
    	 */
    	protected void setLocalizationEnd(Collection<de.darwinspl.fmec.resource.fmec.IFmecCommand<de.darwinspl.fmec.resource.fmec.IFmecTextResource>> postParseCommands , final EObject object, final int endChar, final int endLine) {
    		if (disableLocationMap) {
    			return;
    		}
    		final de.darwinspl.fmec.resource.fmec.IFmecLocationMap locationMap = this.locationMap;
    		if (locationMap == null) {
    			// the locationMap can be null if the parser is used for code completion
    			return;
    		}
    		postParseCommands.add(new de.darwinspl.fmec.resource.fmec.IFmecCommand<de.darwinspl.fmec.resource.fmec.IFmecTextResource>() {
    			public boolean execute(de.darwinspl.fmec.resource.fmec.IFmecTextResource resource) {
    				locationMap.setCharEnd(object, endChar);
    				locationMap.setLine(object, endLine);
    				return true;
    			}
    		});
    	}
    	
    	public de.darwinspl.fmec.resource.fmec.IFmecTextParser createInstance(InputStream actualInputStream, String encoding) {
    		try {
    			if (encoding == null) {
    				return new FmecParser(new CommonTokenStream(new FmecLexer(new ANTLRInputStream(actualInputStream))));
    			} else {
    				return new FmecParser(new CommonTokenStream(new FmecLexer(new ANTLRInputStream(actualInputStream, encoding))));
    			}
    		} catch (IOException e) {
    			new de.darwinspl.fmec.resource.fmec.util.FmecRuntimeUtil().logError("Error while creating parser.", e);
    			return null;
    		}
    	}
    	
    	/**
    	 * This default constructor is only used to call createInstance() on it.
    	 */
    	public FmecParser() {
    		super(null);
    	}
    	
    	protected EObject doParse() throws RecognitionException {
    		this.lastPosition = 0;
    		// required because the lexer class can not be subclassed
    		((FmecLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
    		((FmecLexer) getTokenStream().getTokenSource()).lexerExceptionPositions = lexerExceptionPositions;
    		Object typeObject = getTypeObject();
    		if (typeObject == null) {
    			return start();
    		} else if (typeObject instanceof EClass) {
    			EClass type = (EClass) typeObject;
    			if (type.getInstanceClass() == de.darwinspl.fmec.FeatureModelEvolutionConstraintModel.class) {
    				return parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.constraints.BinaryConstraint.class) {
    				return parse_de_darwinspl_fmec_constraints_BinaryConstraint();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.constraints.UnaryConstraint.class) {
    				return parse_de_darwinspl_fmec_constraints_UnaryConstraint();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.constraints.NestedConstraint.class) {
    				return parse_de_darwinspl_fmec_constraints_NestedConstraint();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.constraints.TimePointConstraint.class) {
    				return parse_de_darwinspl_fmec_constraints_TimePointConstraint();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.constraints.TimeIntervalConstraint.class) {
    				return parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.property.EvolutionaryProperty.class) {
    				return parse_de_darwinspl_fmec_property_EvolutionaryProperty();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.property.FeatureExistenceProperty.class) {
    				return parse_de_darwinspl_fmec_property_FeatureExistenceProperty();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.property.FeatureTypeProperty.class) {
    				return parse_de_darwinspl_fmec_property_FeatureTypeProperty();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.property.FeatureParentProperty.class) {
    				return parse_de_darwinspl_fmec_property_FeatureParentProperty();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.property.GroupExistenceProperty.class) {
    				return parse_de_darwinspl_fmec_property_GroupExistenceProperty();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.property.GroupTypeProperty.class) {
    				return parse_de_darwinspl_fmec_property_GroupTypeProperty();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.property.GroupParentProperty.class) {
    				return parse_de_darwinspl_fmec_property_GroupParentProperty();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.timepoint.TimePoint.class) {
    				return parse_de_darwinspl_fmec_timepoint_TimePoint();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.timepoint.TimeOffset.class) {
    				return parse_de_darwinspl_fmec_timepoint_TimeOffset();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.timepoint.ExplicitTimePoint.class) {
    				return parse_de_darwinspl_fmec_timepoint_ExplicitTimePoint();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.timepoint.EvolutionaryEvent.class) {
    				return parse_de_darwinspl_fmec_timepoint_EvolutionaryEvent();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.logical.KeywordAnd.class) {
    				return parse_de_darwinspl_fmec_keywords_logical_KeywordAnd();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.logical.KeywordOr.class) {
    				return parse_de_darwinspl_fmec_keywords_logical_KeywordOr();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.logical.KeywordImplies.class) {
    				return parse_de_darwinspl_fmec_keywords_logical_KeywordImplies();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.logical.KeywordNot.class) {
    				return parse_de_darwinspl_fmec_keywords_logical_KeywordNot();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.buildingblocks.KeywordStarts.class) {
    				return parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStarts();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.buildingblocks.KeywordStops.class) {
    				return parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStops();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.buildingblocks.KeywordValid.class) {
    				return parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordValid();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.timepoint.KeywordBefore.class) {
    				return parse_de_darwinspl_fmec_keywords_timepoint_KeywordBefore();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.timepoint.KeywordUntil.class) {
    				return parse_de_darwinspl_fmec_keywords_timepoint_KeywordUntil();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.timepoint.KeywordAfter.class) {
    				return parse_de_darwinspl_fmec_keywords_timepoint_KeywordAfter();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.timepoint.KeywordWhen.class) {
    				return parse_de_darwinspl_fmec_keywords_timepoint_KeywordWhen();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.timepoint.KeywordAt.class) {
    				return parse_de_darwinspl_fmec_keywords_timepoint_KeywordAt();
    			}
    			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.interval.KeywordDuring.class) {
    				return parse_de_darwinspl_fmec_keywords_interval_KeywordDuring();
    			}
    		}
    		throw new de.darwinspl.fmec.resource.fmec.mopp.FmecUnexpectedContentTypeException(typeObject);
    	}
    	
    	public int getMismatchedTokenRecoveryTries() {
    		return mismatchedTokenRecoveryTries;
    	}
    	
    	public Object getMissingSymbol(IntStream arg0, RecognitionException arg1, int arg2, BitSet arg3) {
    		mismatchedTokenRecoveryTries++;
    		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
    	}
    	
    	public Object getParseToIndexTypeObject() {
    		return parseToIndexTypeObject;
    	}
    	
    	protected Object getTypeObject() {
    		Object typeObject = getParseToIndexTypeObject();
    		if (typeObject != null) {
    			return typeObject;
    		}
    		Map<?,?> options = getOptions();
    		if (options != null) {
    			typeObject = options.get(de.darwinspl.fmec.resource.fmec.IFmecOptions.RESOURCE_CONTENT_TYPE);
    		}
    		return typeObject;
    	}
    	
    	/**
    	 * Implementation that calls {@link #doParse()} and handles the thrown
    	 * RecognitionExceptions.
    	 */
    	public de.darwinspl.fmec.resource.fmec.IFmecParseResult parse() {
    		// Reset parser state
    		terminateParsing = false;
    		postParseCommands = new ArrayList<de.darwinspl.fmec.resource.fmec.IFmecCommand<de.darwinspl.fmec.resource.fmec.IFmecTextResource>>();
    		de.darwinspl.fmec.resource.fmec.mopp.FmecParseResult parseResult = new de.darwinspl.fmec.resource.fmec.mopp.FmecParseResult();
    		if (disableLocationMap) {
    			locationMap = new de.darwinspl.fmec.resource.fmec.mopp.FmecDevNullLocationMap();
    		} else {
    			locationMap = new de.darwinspl.fmec.resource.fmec.mopp.FmecLocationMap();
    		}
    		// Run parser
    		try {
    			EObject result =  doParse();
    			if (lexerExceptions.isEmpty()) {
    				parseResult.setRoot(result);
    				parseResult.setLocationMap(locationMap);
    			}
    		} catch (RecognitionException re) {
    			addErrorToResource(syntaxErrorMessageConverter.translateParseError(re));
    		} catch (IllegalArgumentException iae) {
    			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
    				// can be caused if a null is set on EMF models where not allowed. this will just
    				// happen if other errors occurred before
    			} else {
    				iae.printStackTrace();
    			}
    		}
    		for (RecognitionException re : lexerExceptions) {
    			addErrorToResource(syntaxErrorMessageConverter.translateLexicalError(re, lexerExceptions, lexerExceptionPositions));
    		}
    		parseResult.getPostParseCommands().addAll(postParseCommands);
    		return parseResult;
    	}
    	
    	public List<de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal> parseToExpectedElements(EClass type, de.darwinspl.fmec.resource.fmec.IFmecTextResource dummyResource, int cursorOffset) {
    		this.rememberExpectedElements = true;
    		this.parseToIndexTypeObject = type;
    		this.cursorOffset = cursorOffset;
    		this.lastStartIncludingHidden = -1;
    		final CommonTokenStream tokenStream = (CommonTokenStream) getTokenStream();
    		de.darwinspl.fmec.resource.fmec.IFmecParseResult result = parse();
    		for (EObject incompleteObject : incompleteObjects) {
    			Lexer lexer = (Lexer) tokenStream.getTokenSource();
    			int endChar = lexer.getCharIndex();
    			int endLine = lexer.getLine();
    			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
    		}
    		if (result != null) {
    			EObject root = result.getRoot();
    			if (root != null) {
    				dummyResource.getContentsInternal().add(root);
    			}
    			for (de.darwinspl.fmec.resource.fmec.IFmecCommand<de.darwinspl.fmec.resource.fmec.IFmecTextResource> command : result.getPostParseCommands()) {
    				command.execute(dummyResource);
    			}
    		}
    		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
    		Set<de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal> currentFollowSet = new LinkedHashSet<de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal>();
    		List<de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal> newFollowSet = new ArrayList<de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal>();
    		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
    			de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal expectedElementI = expectedElements.get(i);
    			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
    				currentFollowSet.add(expectedElementI);
    			} else {
    				break;
    			}
    		}
    		int followSetID = 83;
    		int i;
    		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
    			CommonToken nextToken = (CommonToken) tokenStream.get(i);
    			if (nextToken.getType() < 0) {
    				break;
    			}
    			if (nextToken.getChannel() == 99) {
    				// hidden tokens do not reduce the follow set
    			} else {
    				// now that we have found the next visible token the position for that expected
    				// terminals can be set
    				for (de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal nextFollow : newFollowSet) {
    					lastTokenIndex = 0;
    					setPosition(nextFollow, i);
    				}
    				newFollowSet.clear();
    				// normal tokens do reduce the follow set - only elements that match the token are
    				// kept
    				for (de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal nextFollow : currentFollowSet) {
    					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
    						// keep this one - it matches
    						Collection<de.darwinspl.fmec.resource.fmec.util.FmecPair<de.darwinspl.fmec.resource.fmec.IFmecExpectedElement, de.darwinspl.fmec.resource.fmec.mopp.FmecContainedFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
    						for (de.darwinspl.fmec.resource.fmec.util.FmecPair<de.darwinspl.fmec.resource.fmec.IFmecExpectedElement, de.darwinspl.fmec.resource.fmec.mopp.FmecContainedFeature[]> newFollowerPair : newFollowers) {
    							de.darwinspl.fmec.resource.fmec.IFmecExpectedElement newFollower = newFollowerPair.getLeft();
    							EObject container = getLastIncompleteElement();
    							de.darwinspl.fmec.resource.fmec.grammar.FmecContainmentTrace containmentTrace = new de.darwinspl.fmec.resource.fmec.grammar.FmecContainmentTrace(null, newFollowerPair.getRight());
    							de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal newFollowTerminal = new de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal(container, newFollower, followSetID, containmentTrace);
    							newFollowSet.add(newFollowTerminal);
    							expectedElements.add(newFollowTerminal);
    						}
    					}
    				}
    				currentFollowSet.clear();
    				currentFollowSet.addAll(newFollowSet);
    			}
    			followSetID++;
    		}
    		// after the last token in the stream we must set the position for the elements
    		// that were added during the last iteration of the loop
    		for (de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal nextFollow : newFollowSet) {
    			lastTokenIndex = 0;
    			setPosition(nextFollow, i);
    		}
    		return this.expectedElements;
    	}
    	
    	public void setPosition(de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal expectedElement, int tokenIndex) {
    		int currentIndex = Math.max(0, tokenIndex);
    		for (int index = lastTokenIndex; index < currentIndex; index++) {
    			if (index >= input.size()) {
    				break;
    			}
    			CommonToken tokenAtIndex = (CommonToken) input.get(index);
    			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			if (tokenAtIndex.getChannel() != 99 && !anonymousTokens.contains(tokenAtIndex)) {
    				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			}
    		}
    		lastTokenIndex = Math.max(0, currentIndex);
    		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
    	}
    	
    	public Object recoverFromMismatchedToken(IntStream input, int ttype, BitSet follow) throws RecognitionException {
    		if (!rememberExpectedElements) {
    			return super.recoverFromMismatchedToken(input, ttype, follow);
    		} else {
    			return null;
    		}
    	}
    	
    	private void startIncompleteElement(Object object) {
    		if (object instanceof EObject) {
    			this.incompleteObjects.add((EObject) object);
    		}
    	}
    	
    	private void completedElement(Object object, boolean isContainment) {
    		if (isContainment && !this.incompleteObjects.isEmpty()) {
    			this.incompleteObjects.remove(object);
    		}
    		if (object instanceof EObject) {
    			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
    			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
    		}
    	}
    	
    	private EObject getLastIncompleteElement() {
    		if (incompleteObjects.isEmpty()) {
    			return null;
    		}
    		return incompleteObjects.get(incompleteObjects.size() - 1);
    	}
    	



    // $ANTLR start "start"
    // Fmec.g:569:1: start returns [ EObject element = null] : (c0= parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel ) EOF ;
    public final EObject start() throws RecognitionException {
        EObject element =  null;

        int start_StartIndex = input.index();

        de.darwinspl.fmec.FeatureModelEvolutionConstraintModel c0 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 1) ) { return element; }

            // Fmec.g:570:2: ( (c0= parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel ) EOF )
            // Fmec.g:571:2: (c0= parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel ) EOF
            {
            if ( state.backtracking==0 ) {
            		// follow set for start rule(s)
            		addExpectedElement(null, 0);
            		expectedElementsIndexOfLastCompleteElement = 0;
            	}

            // Fmec.g:576:2: (c0= parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel )
            // Fmec.g:577:3: c0= parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel
            {
            pushFollow(FOLLOW_parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel_in_start82);
            c0=parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) { element = c0; }

            }


            match(input,EOF,FOLLOW_EOF_in_start89); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		retrieveLayoutInformation(element, null, null, false);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 1, start_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "start"



    // $ANTLR start "parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel"
    // Fmec.g:585:1: parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel returns [de.darwinspl.fmec.FeatureModelEvolutionConstraintModel element = null] : a0= 'feature' a1= 'model' (a2= PATH_TO_FILE ) ( ( (a3_0= parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint ) a4= ';' ) )* ;
    public final de.darwinspl.fmec.FeatureModelEvolutionConstraintModel parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel() throws RecognitionException {
        de.darwinspl.fmec.FeatureModelEvolutionConstraintModel element =  null;

        int parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a4=null;
        de.darwinspl.fmec.constraints.FeatureModelEvolutionConstraint a3_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 2) ) { return element; }

            // Fmec.g:588:2: (a0= 'feature' a1= 'model' (a2= PATH_TO_FILE ) ( ( (a3_0= parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint ) a4= ';' ) )* )
            // Fmec.g:589:2: a0= 'feature' a1= 'model' (a2= PATH_TO_FILE ) ( ( (a3_0= parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint ) a4= ';' ) )*
            {
            a0=(Token)match(input,31,FOLLOW_31_in_parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel115); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.FmecFactory.eINSTANCE.createFeatureModelEvolutionConstraintModel();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_0_0_0_0, null, true);
            		copyLocalizationInfos((CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 1);
            	}

            a1=(Token)match(input,35,FOLLOW_35_in_parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel129); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.FmecFactory.eINSTANCE.createFeatureModelEvolutionConstraintModel();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_0_0_0_1, null, true);
            		copyLocalizationInfos((CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 2);
            	}

            // Fmec.g:617:2: (a2= PATH_TO_FILE )
            // Fmec.g:618:3: a2= PATH_TO_FILE
            {
            a2=(Token)match(input,PATH_TO_FILE,FOLLOW_PATH_TO_FILE_in_parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel147); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.FmecFactory.eINSTANCE.createFeatureModelEvolutionConstraintModel();
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("PATH_TO_FILE");
            				tokenResolver.setOptions(getOptions());
            				de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__FEATURE_MODEL), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((CommonToken) a2).getLine(), ((CommonToken) a2).getCharPositionInLine(), ((CommonToken) a2).getStartIndex(), ((CommonToken) a2).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				de.darwinspl.feature.DwTemporalFeatureModel proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwTemporalFeatureModel();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new de.darwinspl.fmec.resource.fmec.mopp.FmecContextDependentURIFragmentFactory<de.darwinspl.fmec.FeatureModelEvolutionConstraintModel, de.darwinspl.feature.DwTemporalFeatureModel>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getFeatureModelEvolutionConstraintModelFeatureModelReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__FEATURE_MODEL), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__FEATURE_MODEL), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_0_0_0_2, proxy, true);
            				copyLocalizationInfos((CommonToken) a2, element);
            				copyLocalizationInfos((CommonToken) a2, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.darwinspl.fmec.FmecPackage.eINSTANCE.getFeatureModelEvolutionConstraintModel(), 3, 38);
            	}

            // Fmec.g:657:2: ( ( (a3_0= parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint ) a4= ';' ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0 >= DATE && LA1_0 <= IDENTIFIER)||LA1_0==37||LA1_0==47) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // Fmec.g:658:3: ( (a3_0= parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint ) a4= ';' )
            	    {
            	    // Fmec.g:658:3: ( (a3_0= parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint ) a4= ';' )
            	    // Fmec.g:659:4: (a3_0= parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint ) a4= ';'
            	    {
            	    // Fmec.g:659:4: (a3_0= parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint )
            	    // Fmec.g:660:5: a3_0= parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint
            	    {
            	    pushFollow(FOLLOW_parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint_in_parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel183);
            	    a3_0=parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = de.darwinspl.fmec.FmecFactory.eINSTANCE.createFeatureModelEvolutionConstraintModel();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a3_0 != null) {
            	    						if (a3_0 != null) {
            	    							Object value = a3_0;
            	    							addObjectToList(element, de.darwinspl.fmec.FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__CONSTRAINTS, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_0_0_0_5_0_0_0, a3_0, true);
            	    						copyLocalizationInfos(a3_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(null, 39);
            	    			}

            	    a4=(Token)match(input,16,FOLLOW_16_in_parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel211); if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (element == null) {
            	    					element = de.darwinspl.fmec.FmecFactory.eINSTANCE.createFeatureModelEvolutionConstraintModel();
            	    					startIncompleteElement(element);
            	    				}
            	    				collectHiddenTokens(element);
            	    				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_0_0_0_5_0_0_1, null, true);
            	    				copyLocalizationInfos((CommonToken)a4, element);
            	    			}

            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				// We've found the last token for this rule. The constructed EObject is now
            	    				// complete.
            	    				completedElement(element, true);
            	    				addExpectedElement(de.darwinspl.fmec.FmecPackage.eINSTANCE.getFeatureModelEvolutionConstraintModel(), 40, 75);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.fmec.FmecPackage.eINSTANCE.getFeatureModelEvolutionConstraintModel(), 76, 111);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 2, parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel"



    // $ANTLR start "parse_de_darwinspl_fmec_constraints_BinaryConstraint"
    // Fmec.g:714:1: parse_de_darwinspl_fmec_constraints_BinaryConstraint returns [de.darwinspl.fmec.constraints.BinaryConstraint element = null] : (a0_0= parse_de_darwinspl_fmec_constraints_BinaryConstraintChild ) (a1_0= parse_de_darwinspl_fmec_keywords_logical_BinaryLogicalKeyword ) (a2_0= parse_de_darwinspl_fmec_constraints_BinaryConstraintChild ) ;
    public final de.darwinspl.fmec.constraints.BinaryConstraint parse_de_darwinspl_fmec_constraints_BinaryConstraint() throws RecognitionException {
        de.darwinspl.fmec.constraints.BinaryConstraint element =  null;

        int parse_de_darwinspl_fmec_constraints_BinaryConstraint_StartIndex = input.index();

        de.darwinspl.fmec.constraints.BinaryConstraintChild a0_0 =null;

        de.darwinspl.fmec.keywords.logical.BinaryLogicalKeyword a1_0 =null;

        de.darwinspl.fmec.constraints.BinaryConstraintChild a2_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 3) ) { return element; }

            // Fmec.g:717:2: ( (a0_0= parse_de_darwinspl_fmec_constraints_BinaryConstraintChild ) (a1_0= parse_de_darwinspl_fmec_keywords_logical_BinaryLogicalKeyword ) (a2_0= parse_de_darwinspl_fmec_constraints_BinaryConstraintChild ) )
            // Fmec.g:718:2: (a0_0= parse_de_darwinspl_fmec_constraints_BinaryConstraintChild ) (a1_0= parse_de_darwinspl_fmec_keywords_logical_BinaryLogicalKeyword ) (a2_0= parse_de_darwinspl_fmec_constraints_BinaryConstraintChild )
            {
            // Fmec.g:718:2: (a0_0= parse_de_darwinspl_fmec_constraints_BinaryConstraintChild )
            // Fmec.g:719:3: a0_0= parse_de_darwinspl_fmec_constraints_BinaryConstraintChild
            {
            pushFollow(FOLLOW_parse_de_darwinspl_fmec_constraints_BinaryConstraintChild_in_parse_de_darwinspl_fmec_constraints_BinaryConstraint263);
            a0_0=parse_de_darwinspl_fmec_constraints_BinaryConstraintChild();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createBinaryConstraint();
            				startIncompleteElement(element);
            			}
            			if (a0_0 != null) {
            				if (a0_0 != null) {
            					Object value = a0_0;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.BINARY_CONSTRAINT__LEFT_CHILD), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_1_0_0_0, a0_0, true);
            				copyLocalizationInfos(a0_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getBinaryConstraint(), 112, 114);
            	}

            // Fmec.g:744:2: (a1_0= parse_de_darwinspl_fmec_keywords_logical_BinaryLogicalKeyword )
            // Fmec.g:745:3: a1_0= parse_de_darwinspl_fmec_keywords_logical_BinaryLogicalKeyword
            {
            pushFollow(FOLLOW_parse_de_darwinspl_fmec_keywords_logical_BinaryLogicalKeyword_in_parse_de_darwinspl_fmec_constraints_BinaryConstraint285);
            a1_0=parse_de_darwinspl_fmec_keywords_logical_BinaryLogicalKeyword();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createBinaryConstraint();
            				startIncompleteElement(element);
            			}
            			if (a1_0 != null) {
            				if (a1_0 != null) {
            					Object value = a1_0;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.BINARY_CONSTRAINT__KEYWORD), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_1_0_0_2, a1_0, true);
            				copyLocalizationInfos(a1_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getBinaryConstraint(), 115, 132);
            	}

            // Fmec.g:770:2: (a2_0= parse_de_darwinspl_fmec_constraints_BinaryConstraintChild )
            // Fmec.g:771:3: a2_0= parse_de_darwinspl_fmec_constraints_BinaryConstraintChild
            {
            pushFollow(FOLLOW_parse_de_darwinspl_fmec_constraints_BinaryConstraintChild_in_parse_de_darwinspl_fmec_constraints_BinaryConstraint307);
            a2_0=parse_de_darwinspl_fmec_constraints_BinaryConstraintChild();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createBinaryConstraint();
            				startIncompleteElement(element);
            			}
            			if (a2_0 != null) {
            				if (a2_0 != null) {
            					Object value = a2_0;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.BINARY_CONSTRAINT__RIGHT_CHILD), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_1_0_0_4, a2_0, true);
            				copyLocalizationInfos(a2_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(null, 133, 134);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 3, parse_de_darwinspl_fmec_constraints_BinaryConstraint_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_constraints_BinaryConstraint"



    // $ANTLR start "parse_de_darwinspl_fmec_constraints_UnaryConstraint"
    // Fmec.g:801:1: parse_de_darwinspl_fmec_constraints_UnaryConstraint returns [de.darwinspl.fmec.constraints.UnaryConstraint element = null] : (a0_0= parse_de_darwinspl_fmec_keywords_logical_UnaryLogicalKeyword ) (a1_0= parse_de_darwinspl_fmec_constraints_UnaryConstraintChild ) ;
    public final de.darwinspl.fmec.constraints.UnaryConstraint parse_de_darwinspl_fmec_constraints_UnaryConstraint() throws RecognitionException {
        de.darwinspl.fmec.constraints.UnaryConstraint element =  null;

        int parse_de_darwinspl_fmec_constraints_UnaryConstraint_StartIndex = input.index();

        de.darwinspl.fmec.keywords.logical.UnaryLogicalKeyword a0_0 =null;

        de.darwinspl.fmec.constraints.UnaryConstraintChild a1_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 4) ) { return element; }

            // Fmec.g:804:2: ( (a0_0= parse_de_darwinspl_fmec_keywords_logical_UnaryLogicalKeyword ) (a1_0= parse_de_darwinspl_fmec_constraints_UnaryConstraintChild ) )
            // Fmec.g:805:2: (a0_0= parse_de_darwinspl_fmec_keywords_logical_UnaryLogicalKeyword ) (a1_0= parse_de_darwinspl_fmec_constraints_UnaryConstraintChild )
            {
            // Fmec.g:805:2: (a0_0= parse_de_darwinspl_fmec_keywords_logical_UnaryLogicalKeyword )
            // Fmec.g:806:3: a0_0= parse_de_darwinspl_fmec_keywords_logical_UnaryLogicalKeyword
            {
            pushFollow(FOLLOW_parse_de_darwinspl_fmec_keywords_logical_UnaryLogicalKeyword_in_parse_de_darwinspl_fmec_constraints_UnaryConstraint344);
            a0_0=parse_de_darwinspl_fmec_keywords_logical_UnaryLogicalKeyword();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createUnaryConstraint();
            				startIncompleteElement(element);
            			}
            			if (a0_0 != null) {
            				if (a0_0 != null) {
            					Object value = a0_0;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.UNARY_CONSTRAINT__KEYWORD), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_2_0_0_0, a0_0, true);
            				copyLocalizationInfos(a0_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getUnaryConstraint(), 135, 151);
            	}

            // Fmec.g:831:2: (a1_0= parse_de_darwinspl_fmec_constraints_UnaryConstraintChild )
            // Fmec.g:832:3: a1_0= parse_de_darwinspl_fmec_constraints_UnaryConstraintChild
            {
            pushFollow(FOLLOW_parse_de_darwinspl_fmec_constraints_UnaryConstraintChild_in_parse_de_darwinspl_fmec_constraints_UnaryConstraint366);
            a1_0=parse_de_darwinspl_fmec_constraints_UnaryConstraintChild();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createUnaryConstraint();
            				startIncompleteElement(element);
            			}
            			if (a1_0 != null) {
            				if (a1_0 != null) {
            					Object value = a1_0;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.UNARY_CONSTRAINT__CHILD), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_2_0_0_1, a1_0, true);
            				copyLocalizationInfos(a1_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(null, 152);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getBinaryConstraint(), 153, 155);
            		addExpectedElement(null, 156);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 4, parse_de_darwinspl_fmec_constraints_UnaryConstraint_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_constraints_UnaryConstraint"



    // $ANTLR start "parse_de_darwinspl_fmec_constraints_NestedConstraint"
    // Fmec.g:864:1: parse_de_darwinspl_fmec_constraints_NestedConstraint returns [de.darwinspl.fmec.constraints.NestedConstraint element = null] : a0= '{' (a1_0= parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint ) a2= '}' ;
    public final de.darwinspl.fmec.constraints.NestedConstraint parse_de_darwinspl_fmec_constraints_NestedConstraint() throws RecognitionException {
        de.darwinspl.fmec.constraints.NestedConstraint element =  null;

        int parse_de_darwinspl_fmec_constraints_NestedConstraint_StartIndex = input.index();

        Token a0=null;
        Token a2=null;
        de.darwinspl.fmec.constraints.FeatureModelEvolutionConstraint a1_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 5) ) { return element; }

            // Fmec.g:867:2: (a0= '{' (a1_0= parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint ) a2= '}' )
            // Fmec.g:868:2: a0= '{' (a1_0= parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint ) a2= '}'
            {
            a0=(Token)match(input,47,FOLLOW_47_in_parse_de_darwinspl_fmec_constraints_NestedConstraint399); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createNestedConstraint();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_3_0_0_0, null, true);
            		copyLocalizationInfos((CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getNestedConstraint(), 157, 192);
            	}

            // Fmec.g:882:2: (a1_0= parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint )
            // Fmec.g:883:3: a1_0= parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint
            {
            pushFollow(FOLLOW_parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint_in_parse_de_darwinspl_fmec_constraints_NestedConstraint417);
            a1_0=parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createNestedConstraint();
            				startIncompleteElement(element);
            			}
            			if (a1_0 != null) {
            				if (a1_0 != null) {
            					Object value = a1_0;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.NESTED_CONSTRAINT__NESTED_CONSTRAINT), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_3_0_0_2, a1_0, true);
            				copyLocalizationInfos(a1_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 193);
            	}

            a2=(Token)match(input,48,FOLLOW_48_in_parse_de_darwinspl_fmec_constraints_NestedConstraint435); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createNestedConstraint();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_3_0_0_4, null, true);
            		copyLocalizationInfos((CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(null, 194);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getBinaryConstraint(), 195, 197);
            		addExpectedElement(null, 198);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 5, parse_de_darwinspl_fmec_constraints_NestedConstraint_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_constraints_NestedConstraint"



    // $ANTLR start "parse_de_darwinspl_fmec_constraints_TimePointConstraint"
    // Fmec.g:929:1: parse_de_darwinspl_fmec_constraints_TimePointConstraint returns [de.darwinspl.fmec.constraints.TimePointConstraint element = null] : (a0_0= parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock ) (a1_0= parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword ) (a2_0= parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock ) ;
    public final de.darwinspl.fmec.constraints.TimePointConstraint parse_de_darwinspl_fmec_constraints_TimePointConstraint() throws RecognitionException {
        de.darwinspl.fmec.constraints.TimePointConstraint element =  null;

        int parse_de_darwinspl_fmec_constraints_TimePointConstraint_StartIndex = input.index();

        de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock a0_0 =null;

        de.darwinspl.fmec.keywords.timepoint.TimePointKeyword a1_0 =null;

        de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock a2_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 6) ) { return element; }

            // Fmec.g:932:2: ( (a0_0= parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock ) (a1_0= parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword ) (a2_0= parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock ) )
            // Fmec.g:933:2: (a0_0= parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock ) (a1_0= parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword ) (a2_0= parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock )
            {
            // Fmec.g:933:2: (a0_0= parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock )
            // Fmec.g:934:3: a0_0= parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock
            {
            pushFollow(FOLLOW_parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock_in_parse_de_darwinspl_fmec_constraints_TimePointConstraint468);
            a0_0=parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createTimePointConstraint();
            				startIncompleteElement(element);
            			}
            			if (a0_0 != null) {
            				if (a0_0 != null) {
            					Object value = a0_0;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_POINT_CONSTRAINT__PRE_ARGUMENT), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_4_0_0_0, a0_0, true);
            				copyLocalizationInfos(a0_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 199, 203);
            	}

            // Fmec.g:959:2: (a1_0= parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword )
            // Fmec.g:960:3: a1_0= parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword
            {
            pushFollow(FOLLOW_parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword_in_parse_de_darwinspl_fmec_constraints_TimePointConstraint490);
            a1_0=parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createTimePointConstraint();
            				startIncompleteElement(element);
            			}
            			if (a1_0 != null) {
            				if (a1_0 != null) {
            					Object value = a1_0;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_POINT_CONSTRAINT__KEYWORD), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_4_0_0_1, a1_0, true);
            				copyLocalizationInfos(a1_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 204, 211);
            	}

            // Fmec.g:985:2: (a2_0= parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock )
            // Fmec.g:986:3: a2_0= parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock
            {
            pushFollow(FOLLOW_parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock_in_parse_de_darwinspl_fmec_constraints_TimePointConstraint512);
            a2_0=parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createTimePointConstraint();
            				startIncompleteElement(element);
            			}
            			if (a2_0 != null) {
            				if (a2_0 != null) {
            					Object value = a2_0;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_POINT_CONSTRAINT__POST_ARGUMENT), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_4_0_0_2, a2_0, true);
            				copyLocalizationInfos(a2_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(null, 212);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getBinaryConstraint(), 213, 215);
            		addExpectedElement(null, 216);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 6, parse_de_darwinspl_fmec_constraints_TimePointConstraint_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_constraints_TimePointConstraint"



    // $ANTLR start "parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint"
    // Fmec.g:1018:1: parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint returns [de.darwinspl.fmec.constraints.TimeIntervalConstraint element = null] : (a0_0= parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock ) (a1_0= parse_de_darwinspl_fmec_keywords_interval_IntervalKeyword ) a2= '[' (a3_0= parse_de_darwinspl_fmec_timepoint_TimePoint ) a4= ',' (a5_0= parse_de_darwinspl_fmec_timepoint_TimePoint ) a6= ')' ;
    public final de.darwinspl.fmec.constraints.TimeIntervalConstraint parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint() throws RecognitionException {
        de.darwinspl.fmec.constraints.TimeIntervalConstraint element =  null;

        int parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint_StartIndex = input.index();

        Token a2=null;
        Token a4=null;
        Token a6=null;
        de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock a0_0 =null;

        de.darwinspl.fmec.keywords.interval.IntervalKeyword a1_0 =null;

        de.darwinspl.fmec.timepoint.TimePoint a3_0 =null;

        de.darwinspl.fmec.timepoint.TimePoint a5_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 7) ) { return element; }

            // Fmec.g:1021:2: ( (a0_0= parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock ) (a1_0= parse_de_darwinspl_fmec_keywords_interval_IntervalKeyword ) a2= '[' (a3_0= parse_de_darwinspl_fmec_timepoint_TimePoint ) a4= ',' (a5_0= parse_de_darwinspl_fmec_timepoint_TimePoint ) a6= ')' )
            // Fmec.g:1022:2: (a0_0= parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock ) (a1_0= parse_de_darwinspl_fmec_keywords_interval_IntervalKeyword ) a2= '[' (a3_0= parse_de_darwinspl_fmec_timepoint_TimePoint ) a4= ',' (a5_0= parse_de_darwinspl_fmec_timepoint_TimePoint ) a6= ')'
            {
            // Fmec.g:1022:2: (a0_0= parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock )
            // Fmec.g:1023:3: a0_0= parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock
            {
            pushFollow(FOLLOW_parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock_in_parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint549);
            a0_0=parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createTimeIntervalConstraint();
            				startIncompleteElement(element);
            			}
            			if (a0_0 != null) {
            				if (a0_0 != null) {
            					Object value = a0_0;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__PRE_ARGUMENT), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_5_0_0_0, a0_0, true);
            				copyLocalizationInfos(a0_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 217);
            	}

            // Fmec.g:1048:2: (a1_0= parse_de_darwinspl_fmec_keywords_interval_IntervalKeyword )
            // Fmec.g:1049:3: a1_0= parse_de_darwinspl_fmec_keywords_interval_IntervalKeyword
            {
            pushFollow(FOLLOW_parse_de_darwinspl_fmec_keywords_interval_IntervalKeyword_in_parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint571);
            a1_0=parse_de_darwinspl_fmec_keywords_interval_IntervalKeyword();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createTimeIntervalConstraint();
            				startIncompleteElement(element);
            			}
            			if (a1_0 != null) {
            				if (a1_0 != null) {
            					Object value = a1_0;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__KEYWORD), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_5_0_0_1, a1_0, true);
            				copyLocalizationInfos(a1_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 218);
            	}

            a2=(Token)match(input,23,FOLLOW_23_in_parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint589); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createTimeIntervalConstraint();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_5_0_0_2, null, true);
            		copyLocalizationInfos((CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 219, 220);
            	}

            // Fmec.g:1088:2: (a3_0= parse_de_darwinspl_fmec_timepoint_TimePoint )
            // Fmec.g:1089:3: a3_0= parse_de_darwinspl_fmec_timepoint_TimePoint
            {
            pushFollow(FOLLOW_parse_de_darwinspl_fmec_timepoint_TimePoint_in_parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint607);
            a3_0=parse_de_darwinspl_fmec_timepoint_TimePoint();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createTimeIntervalConstraint();
            				startIncompleteElement(element);
            			}
            			if (a3_0 != null) {
            				if (a3_0 != null) {
            					Object value = a3_0;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__START_POINT), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_5_0_0_3, a3_0, true);
            				copyLocalizationInfos(a3_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 221);
            	}

            a4=(Token)match(input,13,FOLLOW_13_in_parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint625); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createTimeIntervalConstraint();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_5_0_0_4, null, true);
            		copyLocalizationInfos((CommonToken)a4, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 222, 223);
            	}

            // Fmec.g:1128:2: (a5_0= parse_de_darwinspl_fmec_timepoint_TimePoint )
            // Fmec.g:1129:3: a5_0= parse_de_darwinspl_fmec_timepoint_TimePoint
            {
            pushFollow(FOLLOW_parse_de_darwinspl_fmec_timepoint_TimePoint_in_parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint643);
            a5_0=parse_de_darwinspl_fmec_timepoint_TimePoint();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createTimeIntervalConstraint();
            				startIncompleteElement(element);
            			}
            			if (a5_0 != null) {
            				if (a5_0 != null) {
            					Object value = a5_0;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__END_POINT), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_5_0_0_5, a5_0, true);
            				copyLocalizationInfos(a5_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 224);
            	}

            a6=(Token)match(input,11,FOLLOW_11_in_parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint661); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createTimeIntervalConstraint();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_5_0_0_6, null, true);
            		copyLocalizationInfos((CommonToken)a6, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(null, 225);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getBinaryConstraint(), 226, 228);
            		addExpectedElement(null, 229);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 7, parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint"



    // $ANTLR start "parse_de_darwinspl_fmec_property_EvolutionaryProperty"
    // Fmec.g:1175:1: parse_de_darwinspl_fmec_property_EvolutionaryProperty returns [de.darwinspl.fmec.property.EvolutionaryProperty element = null] : (a0_0= parse_de_darwinspl_fmec_property_Property ) (a1_0= parse_de_darwinspl_fmec_keywords_buildingblocks_PropertyKeyword ) ;
    public final de.darwinspl.fmec.property.EvolutionaryProperty parse_de_darwinspl_fmec_property_EvolutionaryProperty() throws RecognitionException {
        de.darwinspl.fmec.property.EvolutionaryProperty element =  null;

        int parse_de_darwinspl_fmec_property_EvolutionaryProperty_StartIndex = input.index();

        de.darwinspl.fmec.property.Property a0_0 =null;

        de.darwinspl.fmec.keywords.buildingblocks.PropertyKeyword a1_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 8) ) { return element; }

            // Fmec.g:1178:2: ( (a0_0= parse_de_darwinspl_fmec_property_Property ) (a1_0= parse_de_darwinspl_fmec_keywords_buildingblocks_PropertyKeyword ) )
            // Fmec.g:1179:2: (a0_0= parse_de_darwinspl_fmec_property_Property ) (a1_0= parse_de_darwinspl_fmec_keywords_buildingblocks_PropertyKeyword )
            {
            // Fmec.g:1179:2: (a0_0= parse_de_darwinspl_fmec_property_Property )
            // Fmec.g:1180:3: a0_0= parse_de_darwinspl_fmec_property_Property
            {
            pushFollow(FOLLOW_parse_de_darwinspl_fmec_property_Property_in_parse_de_darwinspl_fmec_property_EvolutionaryProperty694);
            a0_0=parse_de_darwinspl_fmec_property_Property();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createEvolutionaryProperty();
            				startIncompleteElement(element);
            			}
            			if (a0_0 != null) {
            				if (a0_0 != null) {
            					Object value = a0_0;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.EVOLUTIONARY_PROPERTY__PROPERTY), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_6_0_0_0, a0_0, true);
            				copyLocalizationInfos(a0_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getEvolutionaryProperty(), 230);
            	}

            // Fmec.g:1205:2: (a1_0= parse_de_darwinspl_fmec_keywords_buildingblocks_PropertyKeyword )
            // Fmec.g:1206:3: a1_0= parse_de_darwinspl_fmec_keywords_buildingblocks_PropertyKeyword
            {
            pushFollow(FOLLOW_parse_de_darwinspl_fmec_keywords_buildingblocks_PropertyKeyword_in_parse_de_darwinspl_fmec_property_EvolutionaryProperty716);
            a1_0=parse_de_darwinspl_fmec_keywords_buildingblocks_PropertyKeyword();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createEvolutionaryProperty();
            				startIncompleteElement(element);
            			}
            			if (a1_0 != null) {
            				if (a1_0 != null) {
            					Object value = a1_0;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.EVOLUTIONARY_PROPERTY__KEYWORD), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_6_0_0_1, a1_0, true);
            				copyLocalizationInfos(a1_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 231, 235);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 236);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 8, parse_de_darwinspl_fmec_property_EvolutionaryProperty_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_property_EvolutionaryProperty"



    // $ANTLR start "parse_de_darwinspl_fmec_property_FeatureExistenceProperty"
    // Fmec.g:1237:1: parse_de_darwinspl_fmec_property_FeatureExistenceProperty returns [de.darwinspl.fmec.property.FeatureExistenceProperty element = null] : (a0= IDENTIFIER ) ;
    public final de.darwinspl.fmec.property.FeatureExistenceProperty parse_de_darwinspl_fmec_property_FeatureExistenceProperty() throws RecognitionException {
        de.darwinspl.fmec.property.FeatureExistenceProperty element =  null;

        int parse_de_darwinspl_fmec_property_FeatureExistenceProperty_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 9) ) { return element; }

            // Fmec.g:1240:2: ( (a0= IDENTIFIER ) )
            // Fmec.g:1241:2: (a0= IDENTIFIER )
            {
            // Fmec.g:1241:2: (a0= IDENTIFIER )
            // Fmec.g:1242:3: a0= IDENTIFIER
            {
            a0=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_de_darwinspl_fmec_property_FeatureExistenceProperty753); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createFeatureExistenceProperty();
            				startIncompleteElement(element);
            			}
            			if (a0 != null) {
            				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_EXISTENCE_PROPERTY__REFERENCED_FEATURE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((CommonToken) a0).getLine(), ((CommonToken) a0).getCharPositionInLine(), ((CommonToken) a0).getStartIndex(), ((CommonToken) a0).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				de.darwinspl.feature.DwFeature proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwFeature();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new de.darwinspl.fmec.resource.fmec.mopp.FmecContextDependentURIFragmentFactory<de.darwinspl.fmec.property.FeatureReference, de.darwinspl.feature.DwFeature>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getFeatureReferenceReferencedFeatureReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_EXISTENCE_PROPERTY__REFERENCED_FEATURE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_EXISTENCE_PROPERTY__REFERENCED_FEATURE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_7_0_0_0, proxy, true);
            				copyLocalizationInfos((CommonToken) a0, element);
            				copyLocalizationInfos((CommonToken) a0, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getEvolutionaryProperty(), 237);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 9, parse_de_darwinspl_fmec_property_FeatureExistenceProperty_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_property_FeatureExistenceProperty"



    // $ANTLR start "parse_de_darwinspl_fmec_property_FeatureTypeProperty"
    // Fmec.g:1286:1: parse_de_darwinspl_fmec_property_FeatureTypeProperty returns [de.darwinspl.fmec.property.FeatureTypeProperty element = null] : (a0= IDENTIFIER ) a1= '.' a2= 'type' a3= '==' ( (a4= 'OPTIONAL' |a5= 'MANDATORY' ) ) ;
    public final de.darwinspl.fmec.property.FeatureTypeProperty parse_de_darwinspl_fmec_property_FeatureTypeProperty() throws RecognitionException {
        de.darwinspl.fmec.property.FeatureTypeProperty element =  null;

        int parse_de_darwinspl_fmec_property_FeatureTypeProperty_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 10) ) { return element; }

            // Fmec.g:1289:2: ( (a0= IDENTIFIER ) a1= '.' a2= 'type' a3= '==' ( (a4= 'OPTIONAL' |a5= 'MANDATORY' ) ) )
            // Fmec.g:1290:2: (a0= IDENTIFIER ) a1= '.' a2= 'type' a3= '==' ( (a4= 'OPTIONAL' |a5= 'MANDATORY' ) )
            {
            // Fmec.g:1290:2: (a0= IDENTIFIER )
            // Fmec.g:1291:3: a0= IDENTIFIER
            {
            a0=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_de_darwinspl_fmec_property_FeatureTypeProperty793); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createFeatureTypeProperty();
            				startIncompleteElement(element);
            			}
            			if (a0 != null) {
            				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_TYPE_PROPERTY__REFERENCED_FEATURE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((CommonToken) a0).getLine(), ((CommonToken) a0).getCharPositionInLine(), ((CommonToken) a0).getStartIndex(), ((CommonToken) a0).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				de.darwinspl.feature.DwFeature proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwFeature();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new de.darwinspl.fmec.resource.fmec.mopp.FmecContextDependentURIFragmentFactory<de.darwinspl.fmec.property.FeatureReference, de.darwinspl.feature.DwFeature>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getFeatureReferenceReferencedFeatureReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_TYPE_PROPERTY__REFERENCED_FEATURE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_TYPE_PROPERTY__REFERENCED_FEATURE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_8_0_0_0, proxy, true);
            				copyLocalizationInfos((CommonToken) a0, element);
            				copyLocalizationInfos((CommonToken) a0, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 238);
            	}

            a1=(Token)match(input,15,FOLLOW_15_in_parse_de_darwinspl_fmec_property_FeatureTypeProperty814); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createFeatureTypeProperty();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_8_0_0_1, null, true);
            		copyLocalizationInfos((CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 239);
            	}

            a2=(Token)match(input,42,FOLLOW_42_in_parse_de_darwinspl_fmec_property_FeatureTypeProperty828); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createFeatureTypeProperty();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_8_0_0_2, null, true);
            		copyLocalizationInfos((CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 240);
            	}

            a3=(Token)match(input,17,FOLLOW_17_in_parse_de_darwinspl_fmec_property_FeatureTypeProperty842); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createFeatureTypeProperty();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_8_0_0_3, null, true);
            		copyLocalizationInfos((CommonToken)a3, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 241);
            	}

            // Fmec.g:1372:2: ( (a4= 'OPTIONAL' |a5= 'MANDATORY' ) )
            // Fmec.g:1373:3: (a4= 'OPTIONAL' |a5= 'MANDATORY' )
            {
            // Fmec.g:1373:3: (a4= 'OPTIONAL' |a5= 'MANDATORY' )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==21) ) {
                alt2=1;
            }
            else if ( (LA2_0==20) ) {
                alt2=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }
            switch (alt2) {
                case 1 :
                    // Fmec.g:1374:4: a4= 'OPTIONAL'
                    {
                    a4=(Token)match(input,21,FOLLOW_21_in_parse_de_darwinspl_fmec_property_FeatureTypeProperty865); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createFeatureTypeProperty();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_8_0_0_4, null, true);
                    				copyLocalizationInfos((CommonToken)a4, element);
                    				// set value of enumeration attribute
                    				Object value = de.darwinspl.feature.DwFeaturePackage.eINSTANCE.getDwFeatureTypeEnum().getEEnumLiteral(de.darwinspl.feature.DwFeatureTypeEnum.OPTIONAL_VALUE).getInstance();
                    				element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_TYPE_PROPERTY__FEATURE_TYPE), value);
                    				completedElement(value, false);
                    			}

                    }
                    break;
                case 2 :
                    // Fmec.g:1387:8: a5= 'MANDATORY'
                    {
                    a5=(Token)match(input,20,FOLLOW_20_in_parse_de_darwinspl_fmec_property_FeatureTypeProperty880); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createFeatureTypeProperty();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_8_0_0_4, null, true);
                    				copyLocalizationInfos((CommonToken)a5, element);
                    				// set value of enumeration attribute
                    				Object value = de.darwinspl.feature.DwFeaturePackage.eINSTANCE.getDwFeatureTypeEnum().getEEnumLiteral(de.darwinspl.feature.DwFeatureTypeEnum.MANDATORY_VALUE).getInstance();
                    				element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_TYPE_PROPERTY__FEATURE_TYPE), value);
                    				completedElement(value, false);
                    			}

                    }
                    break;

            }


            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getEvolutionaryProperty(), 242);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 10, parse_de_darwinspl_fmec_property_FeatureTypeProperty_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_property_FeatureTypeProperty"



    // $ANTLR start "parse_de_darwinspl_fmec_property_FeatureParentProperty"
    // Fmec.g:1412:1: parse_de_darwinspl_fmec_property_FeatureParentProperty returns [de.darwinspl.fmec.property.FeatureParentProperty element = null] : (a0= IDENTIFIER ) a1= '.' a2= 'parentGroup' a3= '==' (a4= IDENTIFIER ) ;
    public final de.darwinspl.fmec.property.FeatureParentProperty parse_de_darwinspl_fmec_property_FeatureParentProperty() throws RecognitionException {
        de.darwinspl.fmec.property.FeatureParentProperty element =  null;

        int parse_de_darwinspl_fmec_property_FeatureParentProperty_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 11) ) { return element; }

            // Fmec.g:1415:2: ( (a0= IDENTIFIER ) a1= '.' a2= 'parentGroup' a3= '==' (a4= IDENTIFIER ) )
            // Fmec.g:1416:2: (a0= IDENTIFIER ) a1= '.' a2= 'parentGroup' a3= '==' (a4= IDENTIFIER )
            {
            // Fmec.g:1416:2: (a0= IDENTIFIER )
            // Fmec.g:1417:3: a0= IDENTIFIER
            {
            a0=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_de_darwinspl_fmec_property_FeatureParentProperty920); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createFeatureParentProperty();
            				startIncompleteElement(element);
            			}
            			if (a0 != null) {
            				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_PARENT_PROPERTY__REFERENCED_FEATURE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((CommonToken) a0).getLine(), ((CommonToken) a0).getCharPositionInLine(), ((CommonToken) a0).getStartIndex(), ((CommonToken) a0).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				de.darwinspl.feature.DwFeature proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwFeature();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new de.darwinspl.fmec.resource.fmec.mopp.FmecContextDependentURIFragmentFactory<de.darwinspl.fmec.property.FeatureReference, de.darwinspl.feature.DwFeature>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getFeatureReferenceReferencedFeatureReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_PARENT_PROPERTY__REFERENCED_FEATURE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_PARENT_PROPERTY__REFERENCED_FEATURE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_9_0_0_0, proxy, true);
            				copyLocalizationInfos((CommonToken) a0, element);
            				copyLocalizationInfos((CommonToken) a0, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 243);
            	}

            a1=(Token)match(input,15,FOLLOW_15_in_parse_de_darwinspl_fmec_property_FeatureParentProperty941); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createFeatureParentProperty();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_9_0_0_1, null, true);
            		copyLocalizationInfos((CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 244);
            	}

            a2=(Token)match(input,40,FOLLOW_40_in_parse_de_darwinspl_fmec_property_FeatureParentProperty955); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createFeatureParentProperty();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_9_0_0_2, null, true);
            		copyLocalizationInfos((CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 245);
            	}

            a3=(Token)match(input,17,FOLLOW_17_in_parse_de_darwinspl_fmec_property_FeatureParentProperty969); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createFeatureParentProperty();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_9_0_0_3, null, true);
            		copyLocalizationInfos((CommonToken)a3, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 246);
            	}

            // Fmec.g:1498:2: (a4= IDENTIFIER )
            // Fmec.g:1499:3: a4= IDENTIFIER
            {
            a4=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_de_darwinspl_fmec_property_FeatureParentProperty987); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createFeatureParentProperty();
            				startIncompleteElement(element);
            			}
            			if (a4 != null) {
            				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_PARENT_PROPERTY__REFERENCED_GROUP), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((CommonToken) a4).getLine(), ((CommonToken) a4).getCharPositionInLine(), ((CommonToken) a4).getStartIndex(), ((CommonToken) a4).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				de.darwinspl.feature.DwGroup proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwGroup();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new de.darwinspl.fmec.resource.fmec.mopp.FmecContextDependentURIFragmentFactory<de.darwinspl.fmec.property.GroupReference, de.darwinspl.feature.DwGroup>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getGroupReferenceReferencedGroupReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_PARENT_PROPERTY__REFERENCED_GROUP), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_PARENT_PROPERTY__REFERENCED_GROUP), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_9_0_0_4, proxy, true);
            				copyLocalizationInfos((CommonToken) a4, element);
            				copyLocalizationInfos((CommonToken) a4, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getEvolutionaryProperty(), 247);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 11, parse_de_darwinspl_fmec_property_FeatureParentProperty_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_property_FeatureParentProperty"



    // $ANTLR start "parse_de_darwinspl_fmec_property_GroupExistenceProperty"
    // Fmec.g:1543:1: parse_de_darwinspl_fmec_property_GroupExistenceProperty returns [de.darwinspl.fmec.property.GroupExistenceProperty element = null] : (a0= IDENTIFIER ) ;
    public final de.darwinspl.fmec.property.GroupExistenceProperty parse_de_darwinspl_fmec_property_GroupExistenceProperty() throws RecognitionException {
        de.darwinspl.fmec.property.GroupExistenceProperty element =  null;

        int parse_de_darwinspl_fmec_property_GroupExistenceProperty_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 12) ) { return element; }

            // Fmec.g:1546:2: ( (a0= IDENTIFIER ) )
            // Fmec.g:1547:2: (a0= IDENTIFIER )
            {
            // Fmec.g:1547:2: (a0= IDENTIFIER )
            // Fmec.g:1548:3: a0= IDENTIFIER
            {
            a0=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_de_darwinspl_fmec_property_GroupExistenceProperty1027); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupExistenceProperty();
            				startIncompleteElement(element);
            			}
            			if (a0 != null) {
            				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_EXISTENCE_PROPERTY__REFERENCED_GROUP), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((CommonToken) a0).getLine(), ((CommonToken) a0).getCharPositionInLine(), ((CommonToken) a0).getStartIndex(), ((CommonToken) a0).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				de.darwinspl.feature.DwGroup proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwGroup();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new de.darwinspl.fmec.resource.fmec.mopp.FmecContextDependentURIFragmentFactory<de.darwinspl.fmec.property.GroupReference, de.darwinspl.feature.DwGroup>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getGroupReferenceReferencedGroupReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_EXISTENCE_PROPERTY__REFERENCED_GROUP), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_EXISTENCE_PROPERTY__REFERENCED_GROUP), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_10_0_0_0, proxy, true);
            				copyLocalizationInfos((CommonToken) a0, element);
            				copyLocalizationInfos((CommonToken) a0, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getEvolutionaryProperty(), 248);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 12, parse_de_darwinspl_fmec_property_GroupExistenceProperty_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_property_GroupExistenceProperty"



    // $ANTLR start "parse_de_darwinspl_fmec_property_GroupTypeProperty"
    // Fmec.g:1592:1: parse_de_darwinspl_fmec_property_GroupTypeProperty returns [de.darwinspl.fmec.property.GroupTypeProperty element = null] : (a0= IDENTIFIER ) a1= '.' a2= 'type' a3= '==' ( (a4= 'AND' |a5= 'ALTERNATIVE' |a6= 'OR' ) ) ;
    public final de.darwinspl.fmec.property.GroupTypeProperty parse_de_darwinspl_fmec_property_GroupTypeProperty() throws RecognitionException {
        de.darwinspl.fmec.property.GroupTypeProperty element =  null;

        int parse_de_darwinspl_fmec_property_GroupTypeProperty_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 13) ) { return element; }

            // Fmec.g:1595:2: ( (a0= IDENTIFIER ) a1= '.' a2= 'type' a3= '==' ( (a4= 'AND' |a5= 'ALTERNATIVE' |a6= 'OR' ) ) )
            // Fmec.g:1596:2: (a0= IDENTIFIER ) a1= '.' a2= 'type' a3= '==' ( (a4= 'AND' |a5= 'ALTERNATIVE' |a6= 'OR' ) )
            {
            // Fmec.g:1596:2: (a0= IDENTIFIER )
            // Fmec.g:1597:3: a0= IDENTIFIER
            {
            a0=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_de_darwinspl_fmec_property_GroupTypeProperty1067); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupTypeProperty();
            				startIncompleteElement(element);
            			}
            			if (a0 != null) {
            				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_TYPE_PROPERTY__REFERENCED_GROUP), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((CommonToken) a0).getLine(), ((CommonToken) a0).getCharPositionInLine(), ((CommonToken) a0).getStartIndex(), ((CommonToken) a0).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				de.darwinspl.feature.DwGroup proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwGroup();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new de.darwinspl.fmec.resource.fmec.mopp.FmecContextDependentURIFragmentFactory<de.darwinspl.fmec.property.GroupReference, de.darwinspl.feature.DwGroup>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getGroupReferenceReferencedGroupReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_TYPE_PROPERTY__REFERENCED_GROUP), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_TYPE_PROPERTY__REFERENCED_GROUP), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_11_0_0_0, proxy, true);
            				copyLocalizationInfos((CommonToken) a0, element);
            				copyLocalizationInfos((CommonToken) a0, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 249);
            	}

            a1=(Token)match(input,15,FOLLOW_15_in_parse_de_darwinspl_fmec_property_GroupTypeProperty1088); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupTypeProperty();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_11_0_0_1, null, true);
            		copyLocalizationInfos((CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 250);
            	}

            a2=(Token)match(input,42,FOLLOW_42_in_parse_de_darwinspl_fmec_property_GroupTypeProperty1102); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupTypeProperty();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_11_0_0_2, null, true);
            		copyLocalizationInfos((CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 251);
            	}

            a3=(Token)match(input,17,FOLLOW_17_in_parse_de_darwinspl_fmec_property_GroupTypeProperty1116); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupTypeProperty();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_11_0_0_3, null, true);
            		copyLocalizationInfos((CommonToken)a3, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 252);
            	}

            // Fmec.g:1678:2: ( (a4= 'AND' |a5= 'ALTERNATIVE' |a6= 'OR' ) )
            // Fmec.g:1679:3: (a4= 'AND' |a5= 'ALTERNATIVE' |a6= 'OR' )
            {
            // Fmec.g:1679:3: (a4= 'AND' |a5= 'ALTERNATIVE' |a6= 'OR' )
            int alt3=3;
            switch ( input.LA(1) ) {
            case 19:
                {
                alt3=1;
                }
                break;
            case 18:
                {
                alt3=2;
                }
                break;
            case 22:
                {
                alt3=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;

            }

            switch (alt3) {
                case 1 :
                    // Fmec.g:1680:4: a4= 'AND'
                    {
                    a4=(Token)match(input,19,FOLLOW_19_in_parse_de_darwinspl_fmec_property_GroupTypeProperty1139); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupTypeProperty();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_11_0_0_4, null, true);
                    				copyLocalizationInfos((CommonToken)a4, element);
                    				// set value of enumeration attribute
                    				Object value = de.darwinspl.feature.DwFeaturePackage.eINSTANCE.getDwGroupTypeEnum().getEEnumLiteral(de.darwinspl.feature.DwGroupTypeEnum.AND_VALUE).getInstance();
                    				element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_TYPE_PROPERTY__GROUP_TYPE), value);
                    				completedElement(value, false);
                    			}

                    }
                    break;
                case 2 :
                    // Fmec.g:1693:8: a5= 'ALTERNATIVE'
                    {
                    a5=(Token)match(input,18,FOLLOW_18_in_parse_de_darwinspl_fmec_property_GroupTypeProperty1154); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupTypeProperty();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_11_0_0_4, null, true);
                    				copyLocalizationInfos((CommonToken)a5, element);
                    				// set value of enumeration attribute
                    				Object value = de.darwinspl.feature.DwFeaturePackage.eINSTANCE.getDwGroupTypeEnum().getEEnumLiteral(de.darwinspl.feature.DwGroupTypeEnum.ALTERNATIVE_VALUE).getInstance();
                    				element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_TYPE_PROPERTY__GROUP_TYPE), value);
                    				completedElement(value, false);
                    			}

                    }
                    break;
                case 3 :
                    // Fmec.g:1706:8: a6= 'OR'
                    {
                    a6=(Token)match(input,22,FOLLOW_22_in_parse_de_darwinspl_fmec_property_GroupTypeProperty1169); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupTypeProperty();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_11_0_0_4, null, true);
                    				copyLocalizationInfos((CommonToken)a6, element);
                    				// set value of enumeration attribute
                    				Object value = de.darwinspl.feature.DwFeaturePackage.eINSTANCE.getDwGroupTypeEnum().getEEnumLiteral(de.darwinspl.feature.DwGroupTypeEnum.OR_VALUE).getInstance();
                    				element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_TYPE_PROPERTY__GROUP_TYPE), value);
                    				completedElement(value, false);
                    			}

                    }
                    break;

            }


            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getEvolutionaryProperty(), 253);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 13, parse_de_darwinspl_fmec_property_GroupTypeProperty_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_property_GroupTypeProperty"



    // $ANTLR start "parse_de_darwinspl_fmec_property_GroupParentProperty"
    // Fmec.g:1731:1: parse_de_darwinspl_fmec_property_GroupParentProperty returns [de.darwinspl.fmec.property.GroupParentProperty element = null] : (a0= IDENTIFIER ) a1= '.' a2= 'parentFeature' a3= '==' (a4= IDENTIFIER ) ;
    public final de.darwinspl.fmec.property.GroupParentProperty parse_de_darwinspl_fmec_property_GroupParentProperty() throws RecognitionException {
        de.darwinspl.fmec.property.GroupParentProperty element =  null;

        int parse_de_darwinspl_fmec_property_GroupParentProperty_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 14) ) { return element; }

            // Fmec.g:1734:2: ( (a0= IDENTIFIER ) a1= '.' a2= 'parentFeature' a3= '==' (a4= IDENTIFIER ) )
            // Fmec.g:1735:2: (a0= IDENTIFIER ) a1= '.' a2= 'parentFeature' a3= '==' (a4= IDENTIFIER )
            {
            // Fmec.g:1735:2: (a0= IDENTIFIER )
            // Fmec.g:1736:3: a0= IDENTIFIER
            {
            a0=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_de_darwinspl_fmec_property_GroupParentProperty1209); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupParentProperty();
            				startIncompleteElement(element);
            			}
            			if (a0 != null) {
            				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_GROUP), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((CommonToken) a0).getLine(), ((CommonToken) a0).getCharPositionInLine(), ((CommonToken) a0).getStartIndex(), ((CommonToken) a0).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				de.darwinspl.feature.DwGroup proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwGroup();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new de.darwinspl.fmec.resource.fmec.mopp.FmecContextDependentURIFragmentFactory<de.darwinspl.fmec.property.GroupReference, de.darwinspl.feature.DwGroup>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getGroupReferenceReferencedGroupReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_GROUP), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_GROUP), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_12_0_0_0, proxy, true);
            				copyLocalizationInfos((CommonToken) a0, element);
            				copyLocalizationInfos((CommonToken) a0, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 254);
            	}

            a1=(Token)match(input,15,FOLLOW_15_in_parse_de_darwinspl_fmec_property_GroupParentProperty1230); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupParentProperty();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_12_0_0_1, null, true);
            		copyLocalizationInfos((CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 255);
            	}

            a2=(Token)match(input,39,FOLLOW_39_in_parse_de_darwinspl_fmec_property_GroupParentProperty1244); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupParentProperty();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_12_0_0_2, null, true);
            		copyLocalizationInfos((CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 256);
            	}

            a3=(Token)match(input,17,FOLLOW_17_in_parse_de_darwinspl_fmec_property_GroupParentProperty1258); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupParentProperty();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_12_0_0_3, null, true);
            		copyLocalizationInfos((CommonToken)a3, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 257);
            	}

            // Fmec.g:1817:2: (a4= IDENTIFIER )
            // Fmec.g:1818:3: a4= IDENTIFIER
            {
            a4=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_de_darwinspl_fmec_property_GroupParentProperty1276); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupParentProperty();
            				startIncompleteElement(element);
            			}
            			if (a4 != null) {
            				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_FEATURE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((CommonToken) a4).getLine(), ((CommonToken) a4).getCharPositionInLine(), ((CommonToken) a4).getStartIndex(), ((CommonToken) a4).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				de.darwinspl.feature.DwFeature proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwFeature();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new de.darwinspl.fmec.resource.fmec.mopp.FmecContextDependentURIFragmentFactory<de.darwinspl.fmec.property.FeatureReference, de.darwinspl.feature.DwFeature>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getFeatureReferenceReferencedFeatureReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_FEATURE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_FEATURE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_12_0_0_4, proxy, true);
            				copyLocalizationInfos((CommonToken) a4, element);
            				copyLocalizationInfos((CommonToken) a4, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getEvolutionaryProperty(), 258);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 14, parse_de_darwinspl_fmec_property_GroupParentProperty_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_property_GroupParentProperty"



    // $ANTLR start "parse_de_darwinspl_fmec_timepoint_TimePoint"
    // Fmec.g:1862:1: parse_de_darwinspl_fmec_timepoint_TimePoint returns [de.darwinspl.fmec.timepoint.TimePoint element = null] : (a0_0= parse_de_darwinspl_fmec_timepoint_TimePointWithoutOffset ) ( (a1_0= parse_de_darwinspl_fmec_timepoint_TimeOffset ) )? ;
    public final de.darwinspl.fmec.timepoint.TimePoint parse_de_darwinspl_fmec_timepoint_TimePoint() throws RecognitionException {
        de.darwinspl.fmec.timepoint.TimePoint element =  null;

        int parse_de_darwinspl_fmec_timepoint_TimePoint_StartIndex = input.index();

        de.darwinspl.fmec.timepoint.TimePointWithoutOffset a0_0 =null;

        de.darwinspl.fmec.timepoint.TimeOffset a1_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 15) ) { return element; }

            // Fmec.g:1865:2: ( (a0_0= parse_de_darwinspl_fmec_timepoint_TimePointWithoutOffset ) ( (a1_0= parse_de_darwinspl_fmec_timepoint_TimeOffset ) )? )
            // Fmec.g:1866:2: (a0_0= parse_de_darwinspl_fmec_timepoint_TimePointWithoutOffset ) ( (a1_0= parse_de_darwinspl_fmec_timepoint_TimeOffset ) )?
            {
            // Fmec.g:1866:2: (a0_0= parse_de_darwinspl_fmec_timepoint_TimePointWithoutOffset )
            // Fmec.g:1867:3: a0_0= parse_de_darwinspl_fmec_timepoint_TimePointWithoutOffset
            {
            pushFollow(FOLLOW_parse_de_darwinspl_fmec_timepoint_TimePointWithoutOffset_in_parse_de_darwinspl_fmec_timepoint_TimePoint1316);
            a0_0=parse_de_darwinspl_fmec_timepoint_TimePointWithoutOffset();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimePoint();
            				startIncompleteElement(element);
            			}
            			if (a0_0 != null) {
            				if (a0_0 != null) {
            					Object value = a0_0;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_POINT__TIME_POINT), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_13_0_0_0, a0_0, true);
            				copyLocalizationInfos(a0_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimePoint(), 259);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 260, 264);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 265);
            	}

            // Fmec.g:1894:2: ( (a1_0= parse_de_darwinspl_fmec_timepoint_TimeOffset ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==12||LA4_0==14) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // Fmec.g:1895:3: (a1_0= parse_de_darwinspl_fmec_timepoint_TimeOffset )
                    {
                    // Fmec.g:1895:3: (a1_0= parse_de_darwinspl_fmec_timepoint_TimeOffset )
                    // Fmec.g:1896:4: a1_0= parse_de_darwinspl_fmec_timepoint_TimeOffset
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_timepoint_TimeOffset_in_parse_de_darwinspl_fmec_timepoint_TimePoint1343);
                    a1_0=parse_de_darwinspl_fmec_timepoint_TimeOffset();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (terminateParsing) {
                    					throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
                    				}
                    				if (element == null) {
                    					element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimePoint();
                    					startIncompleteElement(element);
                    				}
                    				if (a1_0 != null) {
                    					if (a1_0 != null) {
                    						Object value = a1_0;
                    						element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_POINT__OFFSET), value);
                    						completedElement(value, true);
                    					}
                    					collectHiddenTokens(element);
                    					retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_13_0_0_1, a1_0, true);
                    					copyLocalizationInfos(a1_0, element);
                    				}
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 266, 270);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 271);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 15, parse_de_darwinspl_fmec_timepoint_TimePoint_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_timepoint_TimePoint"



    // $ANTLR start "parse_de_darwinspl_fmec_timepoint_TimeOffset"
    // Fmec.g:1928:1: parse_de_darwinspl_fmec_timepoint_TimeOffset returns [de.darwinspl.fmec.timepoint.TimeOffset element = null] : ( (a0= '+' |a1= '-' ) ) ( ( (a3= INTEGER ) a4= 'years' ) )? ( ( (a5= INTEGER ) a6= 'months' ) )? ( ( (a7= INTEGER ) a8= 'days' ) )? ( ( (a9= INTEGER ) a10= 'hours' ) )? ( ( (a11= INTEGER ) a12= 'minutes' ) )? ;
    public final de.darwinspl.fmec.timepoint.TimeOffset parse_de_darwinspl_fmec_timepoint_TimeOffset() throws RecognitionException {
        de.darwinspl.fmec.timepoint.TimeOffset element =  null;

        int parse_de_darwinspl_fmec_timepoint_TimeOffset_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a8=null;
        Token a9=null;
        Token a10=null;
        Token a11=null;
        Token a12=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 16) ) { return element; }

            // Fmec.g:1931:2: ( ( (a0= '+' |a1= '-' ) ) ( ( (a3= INTEGER ) a4= 'years' ) )? ( ( (a5= INTEGER ) a6= 'months' ) )? ( ( (a7= INTEGER ) a8= 'days' ) )? ( ( (a9= INTEGER ) a10= 'hours' ) )? ( ( (a11= INTEGER ) a12= 'minutes' ) )? )
            // Fmec.g:1932:2: ( (a0= '+' |a1= '-' ) ) ( ( (a3= INTEGER ) a4= 'years' ) )? ( ( (a5= INTEGER ) a6= 'months' ) )? ( ( (a7= INTEGER ) a8= 'days' ) )? ( ( (a9= INTEGER ) a10= 'hours' ) )? ( ( (a11= INTEGER ) a12= 'minutes' ) )?
            {
            // Fmec.g:1932:2: ( (a0= '+' |a1= '-' ) )
            // Fmec.g:1933:3: (a0= '+' |a1= '-' )
            {
            // Fmec.g:1933:3: (a0= '+' |a1= '-' )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==12) ) {
                alt5=1;
            }
            else if ( (LA5_0==14) ) {
                alt5=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;

            }
            switch (alt5) {
                case 1 :
                    // Fmec.g:1934:4: a0= '+'
                    {
                    a0=(Token)match(input,12,FOLLOW_12_in_parse_de_darwinspl_fmec_timepoint_TimeOffset1393); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimeOffset();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_14_0_0_0, true, true);
                    				copyLocalizationInfos((CommonToken)a0, element);
                    				// set value of boolean attribute
                    				Object value = true;
                    				element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__POSITIVE_SIGNATURE), value);
                    				completedElement(value, false);
                    			}

                    }
                    break;
                case 2 :
                    // Fmec.g:1947:8: a1= '-'
                    {
                    a1=(Token)match(input,14,FOLLOW_14_in_parse_de_darwinspl_fmec_timepoint_TimeOffset1408); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimeOffset();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_14_0_0_0, false, true);
                    				copyLocalizationInfos((CommonToken)a1, element);
                    				// set value of boolean attribute
                    				Object value = false;
                    				element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__POSITIVE_SIGNATURE), value);
                    				completedElement(value, false);
                    			}

                    }
                    break;

            }


            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 272, 276);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 277, 281);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 282);
            	}

            // Fmec.g:1969:2: ( ( (a3= INTEGER ) a4= 'years' ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==INTEGER) ) {
                int LA6_1 = input.LA(2);

                if ( (LA6_1==46) ) {
                    alt6=1;
                }
            }
            switch (alt6) {
                case 1 :
                    // Fmec.g:1970:3: ( (a3= INTEGER ) a4= 'years' )
                    {
                    // Fmec.g:1970:3: ( (a3= INTEGER ) a4= 'years' )
                    // Fmec.g:1971:4: (a3= INTEGER ) a4= 'years'
                    {
                    // Fmec.g:1971:4: (a3= INTEGER )
                    // Fmec.g:1972:5: a3= INTEGER
                    {
                    a3=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_parse_de_darwinspl_fmec_timepoint_TimeOffset1444); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimeOffset();
                    						startIncompleteElement(element);
                    					}
                    					if (a3 != null) {
                    						de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("INTEGER");
                    						tokenResolver.setOptions(getOptions());
                    						de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__YEARS), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((CommonToken) a3).getLine(), ((CommonToken) a3).getCharPositionInLine(), ((CommonToken) a3).getStartIndex(), ((CommonToken) a3).getStopIndex());
                    						}
                    						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
                    						if (resolved != null) {
                    							Object value = resolved;
                    							element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__YEARS), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_14_0_0_1_0_0_0, resolved, true);
                    						copyLocalizationInfos((CommonToken) a3, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, 283);
                    			}

                    a4=(Token)match(input,46,FOLLOW_46_in_parse_de_darwinspl_fmec_timepoint_TimeOffset1477); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimeOffset();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_14_0_0_1_0_0_1, null, true);
                    				copyLocalizationInfos((CommonToken)a4, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, 284, 287);
                    				addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 288, 292);
                    				addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 293);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 294, 297);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 298, 302);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 303);
            	}

            // Fmec.g:2032:2: ( ( (a5= INTEGER ) a6= 'months' ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==INTEGER) ) {
                int LA7_1 = input.LA(2);

                if ( (LA7_1==36) ) {
                    alt7=1;
                }
            }
            switch (alt7) {
                case 1 :
                    // Fmec.g:2033:3: ( (a5= INTEGER ) a6= 'months' )
                    {
                    // Fmec.g:2033:3: ( (a5= INTEGER ) a6= 'months' )
                    // Fmec.g:2034:4: (a5= INTEGER ) a6= 'months'
                    {
                    // Fmec.g:2034:4: (a5= INTEGER )
                    // Fmec.g:2035:5: a5= INTEGER
                    {
                    a5=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_parse_de_darwinspl_fmec_timepoint_TimeOffset1525); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimeOffset();
                    						startIncompleteElement(element);
                    					}
                    					if (a5 != null) {
                    						de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("INTEGER");
                    						tokenResolver.setOptions(getOptions());
                    						de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a5.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__MONTHS), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((CommonToken) a5).getLine(), ((CommonToken) a5).getCharPositionInLine(), ((CommonToken) a5).getStartIndex(), ((CommonToken) a5).getStopIndex());
                    						}
                    						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
                    						if (resolved != null) {
                    							Object value = resolved;
                    							element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__MONTHS), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_14_0_0_2_0_0_0, resolved, true);
                    						copyLocalizationInfos((CommonToken) a5, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, 304);
                    			}

                    a6=(Token)match(input,36,FOLLOW_36_in_parse_de_darwinspl_fmec_timepoint_TimeOffset1558); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimeOffset();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_14_0_0_2_0_0_1, null, true);
                    				copyLocalizationInfos((CommonToken)a6, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, 305, 307);
                    				addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 308, 312);
                    				addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 313);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 314, 316);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 317, 321);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 322);
            	}

            // Fmec.g:2095:2: ( ( (a7= INTEGER ) a8= 'days' ) )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==INTEGER) ) {
                int LA8_1 = input.LA(2);

                if ( (LA8_1==28) ) {
                    alt8=1;
                }
            }
            switch (alt8) {
                case 1 :
                    // Fmec.g:2096:3: ( (a7= INTEGER ) a8= 'days' )
                    {
                    // Fmec.g:2096:3: ( (a7= INTEGER ) a8= 'days' )
                    // Fmec.g:2097:4: (a7= INTEGER ) a8= 'days'
                    {
                    // Fmec.g:2097:4: (a7= INTEGER )
                    // Fmec.g:2098:5: a7= INTEGER
                    {
                    a7=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_parse_de_darwinspl_fmec_timepoint_TimeOffset1606); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimeOffset();
                    						startIncompleteElement(element);
                    					}
                    					if (a7 != null) {
                    						de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("INTEGER");
                    						tokenResolver.setOptions(getOptions());
                    						de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__DAYS), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((CommonToken) a7).getLine(), ((CommonToken) a7).getCharPositionInLine(), ((CommonToken) a7).getStartIndex(), ((CommonToken) a7).getStopIndex());
                    						}
                    						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
                    						if (resolved != null) {
                    							Object value = resolved;
                    							element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__DAYS), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_14_0_0_3_0_0_0, resolved, true);
                    						copyLocalizationInfos((CommonToken) a7, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, 323);
                    			}

                    a8=(Token)match(input,28,FOLLOW_28_in_parse_de_darwinspl_fmec_timepoint_TimeOffset1639); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimeOffset();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_14_0_0_3_0_0_1, null, true);
                    				copyLocalizationInfos((CommonToken)a8, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, 324, 325);
                    				addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 326, 330);
                    				addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 331);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 332, 333);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 334, 338);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 339);
            	}

            // Fmec.g:2158:2: ( ( (a9= INTEGER ) a10= 'hours' ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==INTEGER) ) {
                int LA9_1 = input.LA(2);

                if ( (LA9_1==32) ) {
                    alt9=1;
                }
            }
            switch (alt9) {
                case 1 :
                    // Fmec.g:2159:3: ( (a9= INTEGER ) a10= 'hours' )
                    {
                    // Fmec.g:2159:3: ( (a9= INTEGER ) a10= 'hours' )
                    // Fmec.g:2160:4: (a9= INTEGER ) a10= 'hours'
                    {
                    // Fmec.g:2160:4: (a9= INTEGER )
                    // Fmec.g:2161:5: a9= INTEGER
                    {
                    a9=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_parse_de_darwinspl_fmec_timepoint_TimeOffset1687); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimeOffset();
                    						startIncompleteElement(element);
                    					}
                    					if (a9 != null) {
                    						de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("INTEGER");
                    						tokenResolver.setOptions(getOptions());
                    						de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a9.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__HOURS), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((CommonToken) a9).getLine(), ((CommonToken) a9).getCharPositionInLine(), ((CommonToken) a9).getStartIndex(), ((CommonToken) a9).getStopIndex());
                    						}
                    						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
                    						if (resolved != null) {
                    							Object value = resolved;
                    							element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__HOURS), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_14_0_0_4_0_0_0, resolved, true);
                    						copyLocalizationInfos((CommonToken) a9, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, 340);
                    			}

                    a10=(Token)match(input,32,FOLLOW_32_in_parse_de_darwinspl_fmec_timepoint_TimeOffset1720); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimeOffset();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_14_0_0_4_0_0_1, null, true);
                    				copyLocalizationInfos((CommonToken)a10, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, 341);
                    				addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 342, 346);
                    				addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 347);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 348);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 349, 353);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 354);
            	}

            // Fmec.g:2221:2: ( ( (a11= INTEGER ) a12= 'minutes' ) )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==INTEGER) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // Fmec.g:2222:3: ( (a11= INTEGER ) a12= 'minutes' )
                    {
                    // Fmec.g:2222:3: ( (a11= INTEGER ) a12= 'minutes' )
                    // Fmec.g:2223:4: (a11= INTEGER ) a12= 'minutes'
                    {
                    // Fmec.g:2223:4: (a11= INTEGER )
                    // Fmec.g:2224:5: a11= INTEGER
                    {
                    a11=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_parse_de_darwinspl_fmec_timepoint_TimeOffset1768); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimeOffset();
                    						startIncompleteElement(element);
                    					}
                    					if (a11 != null) {
                    						de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("INTEGER");
                    						tokenResolver.setOptions(getOptions());
                    						de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a11.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__MINUTES), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((CommonToken) a11).getLine(), ((CommonToken) a11).getCharPositionInLine(), ((CommonToken) a11).getStartIndex(), ((CommonToken) a11).getStopIndex());
                    						}
                    						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
                    						if (resolved != null) {
                    							Object value = resolved;
                    							element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__MINUTES), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_14_0_0_5_0_0_0, resolved, true);
                    						copyLocalizationInfos((CommonToken) a11, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, 355);
                    			}

                    a12=(Token)match(input,34,FOLLOW_34_in_parse_de_darwinspl_fmec_timepoint_TimeOffset1801); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimeOffset();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_14_0_0_5_0_0_1, null, true);
                    				copyLocalizationInfos((CommonToken)a12, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				// We've found the last token for this rule. The constructed EObject is now
                    				// complete.
                    				completedElement(element, true);
                    				addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 356, 360);
                    				addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 361);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 362, 366);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 367);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 16, parse_de_darwinspl_fmec_timepoint_TimeOffset_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_timepoint_TimeOffset"



    // $ANTLR start "parse_de_darwinspl_fmec_timepoint_ExplicitTimePoint"
    // Fmec.g:2290:1: parse_de_darwinspl_fmec_timepoint_ExplicitTimePoint returns [de.darwinspl.fmec.timepoint.ExplicitTimePoint element = null] : (a0= DATE ) ;
    public final de.darwinspl.fmec.timepoint.ExplicitTimePoint parse_de_darwinspl_fmec_timepoint_ExplicitTimePoint() throws RecognitionException {
        de.darwinspl.fmec.timepoint.ExplicitTimePoint element =  null;

        int parse_de_darwinspl_fmec_timepoint_ExplicitTimePoint_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 17) ) { return element; }

            // Fmec.g:2293:2: ( (a0= DATE ) )
            // Fmec.g:2294:2: (a0= DATE )
            {
            // Fmec.g:2294:2: (a0= DATE )
            // Fmec.g:2295:3: a0= DATE
            {
            a0=(Token)match(input,DATE,FOLLOW_DATE_in_parse_de_darwinspl_fmec_timepoint_ExplicitTimePoint1853); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createExplicitTimePoint();
            				startIncompleteElement(element);
            			}
            			if (a0 != null) {
            				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("DATE");
            				tokenResolver.setOptions(getOptions());
            				de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.EXPLICIT_TIME_POINT__DATE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((CommonToken) a0).getLine(), ((CommonToken) a0).getCharPositionInLine(), ((CommonToken) a0).getStartIndex(), ((CommonToken) a0).getStopIndex());
            				}
            				java.util.Date resolved = (java.util.Date) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.EXPLICIT_TIME_POINT__DATE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_15_0_0_0, resolved, true);
            				copyLocalizationInfos((CommonToken) a0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimePoint(), 368);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 369, 373);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 374);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 17, parse_de_darwinspl_fmec_timepoint_ExplicitTimePoint_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_timepoint_ExplicitTimePoint"



    // $ANTLR start "parse_de_darwinspl_fmec_timepoint_EvolutionaryEvent"
    // Fmec.g:2337:1: parse_de_darwinspl_fmec_timepoint_EvolutionaryEvent returns [de.darwinspl.fmec.timepoint.EvolutionaryEvent element = null] : (a0= IDENTIFIER ) (a1_0= parse_de_darwinspl_fmec_keywords_buildingblocks_EventKeyword ) ;
    public final de.darwinspl.fmec.timepoint.EvolutionaryEvent parse_de_darwinspl_fmec_timepoint_EvolutionaryEvent() throws RecognitionException {
        de.darwinspl.fmec.timepoint.EvolutionaryEvent element =  null;

        int parse_de_darwinspl_fmec_timepoint_EvolutionaryEvent_StartIndex = input.index();

        Token a0=null;
        de.darwinspl.fmec.keywords.buildingblocks.EventKeyword a1_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 18) ) { return element; }

            // Fmec.g:2340:2: ( (a0= IDENTIFIER ) (a1_0= parse_de_darwinspl_fmec_keywords_buildingblocks_EventKeyword ) )
            // Fmec.g:2341:2: (a0= IDENTIFIER ) (a1_0= parse_de_darwinspl_fmec_keywords_buildingblocks_EventKeyword )
            {
            // Fmec.g:2341:2: (a0= IDENTIFIER )
            // Fmec.g:2342:3: a0= IDENTIFIER
            {
            a0=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_de_darwinspl_fmec_timepoint_EvolutionaryEvent1893); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createEvolutionaryEvent();
            				startIncompleteElement(element);
            			}
            			if (a0 != null) {
            				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.EVOLUTIONARY_EVENT__REFERENCED_FEATURE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((CommonToken) a0).getLine(), ((CommonToken) a0).getCharPositionInLine(), ((CommonToken) a0).getStartIndex(), ((CommonToken) a0).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				de.darwinspl.feature.DwFeature proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwFeature();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new de.darwinspl.fmec.resource.fmec.mopp.FmecContextDependentURIFragmentFactory<de.darwinspl.fmec.property.FeatureReference, de.darwinspl.feature.DwFeature>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getFeatureReferenceReferencedFeatureReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.EVOLUTIONARY_EVENT__REFERENCED_FEATURE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.EVOLUTIONARY_EVENT__REFERENCED_FEATURE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_16_0_0_0, proxy, true);
            				copyLocalizationInfos((CommonToken) a0, element);
            				copyLocalizationInfos((CommonToken) a0, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getEvolutionaryEvent(), 375, 376);
            	}

            // Fmec.g:2381:2: (a1_0= parse_de_darwinspl_fmec_keywords_buildingblocks_EventKeyword )
            // Fmec.g:2382:3: a1_0= parse_de_darwinspl_fmec_keywords_buildingblocks_EventKeyword
            {
            pushFollow(FOLLOW_parse_de_darwinspl_fmec_keywords_buildingblocks_EventKeyword_in_parse_de_darwinspl_fmec_timepoint_EvolutionaryEvent1918);
            a1_0=parse_de_darwinspl_fmec_keywords_buildingblocks_EventKeyword();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createEvolutionaryEvent();
            				startIncompleteElement(element);
            			}
            			if (a1_0 != null) {
            				if (a1_0 != null) {
            					Object value = a1_0;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.EVOLUTIONARY_EVENT__KEYWORD), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_16_0_0_1, a1_0, true);
            				copyLocalizationInfos(a1_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimePoint(), 377);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 378, 382);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 383);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 18, parse_de_darwinspl_fmec_timepoint_EvolutionaryEvent_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_timepoint_EvolutionaryEvent"



    // $ANTLR start "parse_de_darwinspl_fmec_keywords_logical_KeywordAnd"
    // Fmec.g:2414:1: parse_de_darwinspl_fmec_keywords_logical_KeywordAnd returns [de.darwinspl.fmec.keywords.logical.KeywordAnd element = null] : a0= 'and' ;
    public final de.darwinspl.fmec.keywords.logical.KeywordAnd parse_de_darwinspl_fmec_keywords_logical_KeywordAnd() throws RecognitionException {
        de.darwinspl.fmec.keywords.logical.KeywordAnd element =  null;

        int parse_de_darwinspl_fmec_keywords_logical_KeywordAnd_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 19) ) { return element; }

            // Fmec.g:2417:2: (a0= 'and' )
            // Fmec.g:2418:2: a0= 'and'
            {
            a0=(Token)match(input,25,FOLLOW_25_in_parse_de_darwinspl_fmec_keywords_logical_KeywordAnd1951); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.keywords.logical.LogicalFactory.eINSTANCE.createKeywordAnd();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_17_0_0_0, null, true);
            		copyLocalizationInfos((CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getBinaryConstraint(), 384, 401);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 19, parse_de_darwinspl_fmec_keywords_logical_KeywordAnd_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_keywords_logical_KeywordAnd"



    // $ANTLR start "parse_de_darwinspl_fmec_keywords_logical_KeywordOr"
    // Fmec.g:2437:1: parse_de_darwinspl_fmec_keywords_logical_KeywordOr returns [de.darwinspl.fmec.keywords.logical.KeywordOr element = null] : a0= 'or' ;
    public final de.darwinspl.fmec.keywords.logical.KeywordOr parse_de_darwinspl_fmec_keywords_logical_KeywordOr() throws RecognitionException {
        de.darwinspl.fmec.keywords.logical.KeywordOr element =  null;

        int parse_de_darwinspl_fmec_keywords_logical_KeywordOr_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 20) ) { return element; }

            // Fmec.g:2440:2: (a0= 'or' )
            // Fmec.g:2441:2: a0= 'or'
            {
            a0=(Token)match(input,38,FOLLOW_38_in_parse_de_darwinspl_fmec_keywords_logical_KeywordOr1980); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.keywords.logical.LogicalFactory.eINSTANCE.createKeywordOr();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_18_0_0_0, null, true);
            		copyLocalizationInfos((CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getBinaryConstraint(), 402, 419);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 20, parse_de_darwinspl_fmec_keywords_logical_KeywordOr_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_keywords_logical_KeywordOr"



    // $ANTLR start "parse_de_darwinspl_fmec_keywords_logical_KeywordImplies"
    // Fmec.g:2460:1: parse_de_darwinspl_fmec_keywords_logical_KeywordImplies returns [de.darwinspl.fmec.keywords.logical.KeywordImplies element = null] : a0= 'implies' ;
    public final de.darwinspl.fmec.keywords.logical.KeywordImplies parse_de_darwinspl_fmec_keywords_logical_KeywordImplies() throws RecognitionException {
        de.darwinspl.fmec.keywords.logical.KeywordImplies element =  null;

        int parse_de_darwinspl_fmec_keywords_logical_KeywordImplies_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 21) ) { return element; }

            // Fmec.g:2463:2: (a0= 'implies' )
            // Fmec.g:2464:2: a0= 'implies'
            {
            a0=(Token)match(input,33,FOLLOW_33_in_parse_de_darwinspl_fmec_keywords_logical_KeywordImplies2009); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.keywords.logical.LogicalFactory.eINSTANCE.createKeywordImplies();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_19_0_0_0, null, true);
            		copyLocalizationInfos((CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getBinaryConstraint(), 420, 437);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 21, parse_de_darwinspl_fmec_keywords_logical_KeywordImplies_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_keywords_logical_KeywordImplies"



    // $ANTLR start "parse_de_darwinspl_fmec_keywords_logical_KeywordNot"
    // Fmec.g:2483:1: parse_de_darwinspl_fmec_keywords_logical_KeywordNot returns [de.darwinspl.fmec.keywords.logical.KeywordNot element = null] : a0= 'not' ;
    public final de.darwinspl.fmec.keywords.logical.KeywordNot parse_de_darwinspl_fmec_keywords_logical_KeywordNot() throws RecognitionException {
        de.darwinspl.fmec.keywords.logical.KeywordNot element =  null;

        int parse_de_darwinspl_fmec_keywords_logical_KeywordNot_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 22) ) { return element; }

            // Fmec.g:2486:2: (a0= 'not' )
            // Fmec.g:2487:2: a0= 'not'
            {
            a0=(Token)match(input,37,FOLLOW_37_in_parse_de_darwinspl_fmec_keywords_logical_KeywordNot2038); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.keywords.logical.LogicalFactory.eINSTANCE.createKeywordNot();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_20_0_0_0, null, true);
            		copyLocalizationInfos((CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getUnaryConstraint(), 438, 454);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 22, parse_de_darwinspl_fmec_keywords_logical_KeywordNot_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_keywords_logical_KeywordNot"



    // $ANTLR start "parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStarts"
    // Fmec.g:2506:1: parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStarts returns [de.darwinspl.fmec.keywords.buildingblocks.KeywordStarts element = null] : a0= 'starts' ;
    public final de.darwinspl.fmec.keywords.buildingblocks.KeywordStarts parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStarts() throws RecognitionException {
        de.darwinspl.fmec.keywords.buildingblocks.KeywordStarts element =  null;

        int parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStarts_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 23) ) { return element; }

            // Fmec.g:2509:2: (a0= 'starts' )
            // Fmec.g:2510:2: a0= 'starts'
            {
            a0=(Token)match(input,41,FOLLOW_41_in_parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStarts2067); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksFactory.eINSTANCE.createKeywordStarts();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_21_0_0_0, null, true);
            		copyLocalizationInfos((CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimePoint(), 455);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 456, 460);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 461);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 23, parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStarts_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStarts"



    // $ANTLR start "parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStops"
    // Fmec.g:2531:1: parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStops returns [de.darwinspl.fmec.keywords.buildingblocks.KeywordStops element = null] : a0= 'ends' ;
    public final de.darwinspl.fmec.keywords.buildingblocks.KeywordStops parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStops() throws RecognitionException {
        de.darwinspl.fmec.keywords.buildingblocks.KeywordStops element =  null;

        int parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStops_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 24) ) { return element; }

            // Fmec.g:2534:2: (a0= 'ends' )
            // Fmec.g:2535:2: a0= 'ends'
            {
            a0=(Token)match(input,30,FOLLOW_30_in_parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStops2096); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksFactory.eINSTANCE.createKeywordStops();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_22_0_0_0, null, true);
            		copyLocalizationInfos((CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimePoint(), 462);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 463, 467);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 468);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 24, parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStops_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStops"



    // $ANTLR start "parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordValid"
    // Fmec.g:2556:1: parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordValid returns [de.darwinspl.fmec.keywords.buildingblocks.KeywordValid element = null] : a0= 'valid' ;
    public final de.darwinspl.fmec.keywords.buildingblocks.KeywordValid parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordValid() throws RecognitionException {
        de.darwinspl.fmec.keywords.buildingblocks.KeywordValid element =  null;

        int parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordValid_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 25) ) { return element; }

            // Fmec.g:2559:2: (a0= 'valid' )
            // Fmec.g:2560:2: a0= 'valid'
            {
            a0=(Token)match(input,44,FOLLOW_44_in_parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordValid2125); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksFactory.eINSTANCE.createKeywordValid();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_23_0_0_0, null, true);
            		copyLocalizationInfos((CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 469, 473);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 474);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 25, parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordValid_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordValid"



    // $ANTLR start "parse_de_darwinspl_fmec_keywords_timepoint_KeywordBefore"
    // Fmec.g:2580:1: parse_de_darwinspl_fmec_keywords_timepoint_KeywordBefore returns [de.darwinspl.fmec.keywords.timepoint.KeywordBefore element = null] : a0= 'before' ;
    public final de.darwinspl.fmec.keywords.timepoint.KeywordBefore parse_de_darwinspl_fmec_keywords_timepoint_KeywordBefore() throws RecognitionException {
        de.darwinspl.fmec.keywords.timepoint.KeywordBefore element =  null;

        int parse_de_darwinspl_fmec_keywords_timepoint_KeywordBefore_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 26) ) { return element; }

            // Fmec.g:2583:2: (a0= 'before' )
            // Fmec.g:2584:2: a0= 'before'
            {
            a0=(Token)match(input,27,FOLLOW_27_in_parse_de_darwinspl_fmec_keywords_timepoint_KeywordBefore2154); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.keywords.timepoint.TimepointFactory.eINSTANCE.createKeywordBefore();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_24_0_0_0, null, true);
            		copyLocalizationInfos((CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 475, 482);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 26, parse_de_darwinspl_fmec_keywords_timepoint_KeywordBefore_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_keywords_timepoint_KeywordBefore"



    // $ANTLR start "parse_de_darwinspl_fmec_keywords_timepoint_KeywordUntil"
    // Fmec.g:2603:1: parse_de_darwinspl_fmec_keywords_timepoint_KeywordUntil returns [de.darwinspl.fmec.keywords.timepoint.KeywordUntil element = null] : a0= 'until' ;
    public final de.darwinspl.fmec.keywords.timepoint.KeywordUntil parse_de_darwinspl_fmec_keywords_timepoint_KeywordUntil() throws RecognitionException {
        de.darwinspl.fmec.keywords.timepoint.KeywordUntil element =  null;

        int parse_de_darwinspl_fmec_keywords_timepoint_KeywordUntil_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 27) ) { return element; }

            // Fmec.g:2606:2: (a0= 'until' )
            // Fmec.g:2607:2: a0= 'until'
            {
            a0=(Token)match(input,43,FOLLOW_43_in_parse_de_darwinspl_fmec_keywords_timepoint_KeywordUntil2183); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.keywords.timepoint.TimepointFactory.eINSTANCE.createKeywordUntil();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_25_0_0_0, null, true);
            		copyLocalizationInfos((CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 483, 490);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 27, parse_de_darwinspl_fmec_keywords_timepoint_KeywordUntil_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_keywords_timepoint_KeywordUntil"



    // $ANTLR start "parse_de_darwinspl_fmec_keywords_timepoint_KeywordAfter"
    // Fmec.g:2626:1: parse_de_darwinspl_fmec_keywords_timepoint_KeywordAfter returns [de.darwinspl.fmec.keywords.timepoint.KeywordAfter element = null] : a0= 'after' ;
    public final de.darwinspl.fmec.keywords.timepoint.KeywordAfter parse_de_darwinspl_fmec_keywords_timepoint_KeywordAfter() throws RecognitionException {
        de.darwinspl.fmec.keywords.timepoint.KeywordAfter element =  null;

        int parse_de_darwinspl_fmec_keywords_timepoint_KeywordAfter_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 28) ) { return element; }

            // Fmec.g:2629:2: (a0= 'after' )
            // Fmec.g:2630:2: a0= 'after'
            {
            a0=(Token)match(input,24,FOLLOW_24_in_parse_de_darwinspl_fmec_keywords_timepoint_KeywordAfter2212); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.keywords.timepoint.TimepointFactory.eINSTANCE.createKeywordAfter();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_26_0_0_0, null, true);
            		copyLocalizationInfos((CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 491, 498);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 28, parse_de_darwinspl_fmec_keywords_timepoint_KeywordAfter_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_keywords_timepoint_KeywordAfter"



    // $ANTLR start "parse_de_darwinspl_fmec_keywords_timepoint_KeywordWhen"
    // Fmec.g:2649:1: parse_de_darwinspl_fmec_keywords_timepoint_KeywordWhen returns [de.darwinspl.fmec.keywords.timepoint.KeywordWhen element = null] : a0= 'when' ;
    public final de.darwinspl.fmec.keywords.timepoint.KeywordWhen parse_de_darwinspl_fmec_keywords_timepoint_KeywordWhen() throws RecognitionException {
        de.darwinspl.fmec.keywords.timepoint.KeywordWhen element =  null;

        int parse_de_darwinspl_fmec_keywords_timepoint_KeywordWhen_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 29) ) { return element; }

            // Fmec.g:2652:2: (a0= 'when' )
            // Fmec.g:2653:2: a0= 'when'
            {
            a0=(Token)match(input,45,FOLLOW_45_in_parse_de_darwinspl_fmec_keywords_timepoint_KeywordWhen2241); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.keywords.timepoint.TimepointFactory.eINSTANCE.createKeywordWhen();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_27_0_0_0, null, true);
            		copyLocalizationInfos((CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 499, 506);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 29, parse_de_darwinspl_fmec_keywords_timepoint_KeywordWhen_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_keywords_timepoint_KeywordWhen"



    // $ANTLR start "parse_de_darwinspl_fmec_keywords_timepoint_KeywordAt"
    // Fmec.g:2672:1: parse_de_darwinspl_fmec_keywords_timepoint_KeywordAt returns [de.darwinspl.fmec.keywords.timepoint.KeywordAt element = null] : a0= 'at' ;
    public final de.darwinspl.fmec.keywords.timepoint.KeywordAt parse_de_darwinspl_fmec_keywords_timepoint_KeywordAt() throws RecognitionException {
        de.darwinspl.fmec.keywords.timepoint.KeywordAt element =  null;

        int parse_de_darwinspl_fmec_keywords_timepoint_KeywordAt_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 30) ) { return element; }

            // Fmec.g:2675:2: (a0= 'at' )
            // Fmec.g:2676:2: a0= 'at'
            {
            a0=(Token)match(input,26,FOLLOW_26_in_parse_de_darwinspl_fmec_keywords_timepoint_KeywordAt2270); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.keywords.timepoint.TimepointFactory.eINSTANCE.createKeywordAt();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_28_0_0_0, null, true);
            		copyLocalizationInfos((CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 507, 514);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 30, parse_de_darwinspl_fmec_keywords_timepoint_KeywordAt_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_keywords_timepoint_KeywordAt"



    // $ANTLR start "parse_de_darwinspl_fmec_keywords_interval_KeywordDuring"
    // Fmec.g:2695:1: parse_de_darwinspl_fmec_keywords_interval_KeywordDuring returns [de.darwinspl.fmec.keywords.interval.KeywordDuring element = null] : a0= 'during' ;
    public final de.darwinspl.fmec.keywords.interval.KeywordDuring parse_de_darwinspl_fmec_keywords_interval_KeywordDuring() throws RecognitionException {
        de.darwinspl.fmec.keywords.interval.KeywordDuring element =  null;

        int parse_de_darwinspl_fmec_keywords_interval_KeywordDuring_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 31) ) { return element; }

            // Fmec.g:2698:2: (a0= 'during' )
            // Fmec.g:2699:2: a0= 'during'
            {
            a0=(Token)match(input,29,FOLLOW_29_in_parse_de_darwinspl_fmec_keywords_interval_KeywordDuring2299); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.fmec.keywords.interval.IntervalFactory.eINSTANCE.createKeywordDuring();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_29_0_0_0, null, true);
            		copyLocalizationInfos((CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(null, 515);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 31, parse_de_darwinspl_fmec_keywords_interval_KeywordDuring_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_keywords_interval_KeywordDuring"



    // $ANTLR start "parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint"
    // Fmec.g:2718:1: parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint returns [de.darwinspl.fmec.constraints.FeatureModelEvolutionConstraint element = null] : (c0= parse_de_darwinspl_fmec_constraints_BinaryConstraint |c1= parse_de_darwinspl_fmec_constraints_UnaryConstraint |c2= parse_de_darwinspl_fmec_constraints_NestedConstraint |c3= parse_de_darwinspl_fmec_constraints_TimePointConstraint |c4= parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint );
    public final de.darwinspl.fmec.constraints.FeatureModelEvolutionConstraint parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint() throws RecognitionException {
        de.darwinspl.fmec.constraints.FeatureModelEvolutionConstraint element =  null;

        int parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint_StartIndex = input.index();

        de.darwinspl.fmec.constraints.BinaryConstraint c0 =null;

        de.darwinspl.fmec.constraints.UnaryConstraint c1 =null;

        de.darwinspl.fmec.constraints.NestedConstraint c2 =null;

        de.darwinspl.fmec.constraints.TimePointConstraint c3 =null;

        de.darwinspl.fmec.constraints.TimeIntervalConstraint c4 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 32) ) { return element; }

            // Fmec.g:2719:2: (c0= parse_de_darwinspl_fmec_constraints_BinaryConstraint |c1= parse_de_darwinspl_fmec_constraints_UnaryConstraint |c2= parse_de_darwinspl_fmec_constraints_NestedConstraint |c3= parse_de_darwinspl_fmec_constraints_TimePointConstraint |c4= parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint )
            int alt11=5;
            switch ( input.LA(1) ) {
            case 37:
                {
                int LA11_1 = input.LA(2);

                if ( (synpred12_Fmec()) ) {
                    alt11=1;
                }
                else if ( (synpred13_Fmec()) ) {
                    alt11=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 11, 1, input);

                    throw nvae;

                }
                }
                break;
            case 47:
                {
                int LA11_2 = input.LA(2);

                if ( (synpred12_Fmec()) ) {
                    alt11=1;
                }
                else if ( (synpred14_Fmec()) ) {
                    alt11=3;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 11, 2, input);

                    throw nvae;

                }
                }
                break;
            case IDENTIFIER:
                {
                int LA11_3 = input.LA(2);

                if ( (synpred12_Fmec()) ) {
                    alt11=1;
                }
                else if ( (synpred15_Fmec()) ) {
                    alt11=4;
                }
                else if ( (true) ) {
                    alt11=5;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 11, 3, input);

                    throw nvae;

                }
                }
                break;
            case DATE:
                {
                int LA11_4 = input.LA(2);

                if ( (synpred12_Fmec()) ) {
                    alt11=1;
                }
                else if ( (synpred15_Fmec()) ) {
                    alt11=4;
                }
                else if ( (true) ) {
                    alt11=5;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 11, 4, input);

                    throw nvae;

                }
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;

            }

            switch (alt11) {
                case 1 :
                    // Fmec.g:2720:2: c0= parse_de_darwinspl_fmec_constraints_BinaryConstraint
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_constraints_BinaryConstraint_in_parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint2324);
                    c0=parse_de_darwinspl_fmec_constraints_BinaryConstraint();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Fmec.g:2721:4: c1= parse_de_darwinspl_fmec_constraints_UnaryConstraint
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_constraints_UnaryConstraint_in_parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint2334);
                    c1=parse_de_darwinspl_fmec_constraints_UnaryConstraint();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 3 :
                    // Fmec.g:2722:4: c2= parse_de_darwinspl_fmec_constraints_NestedConstraint
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_constraints_NestedConstraint_in_parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint2344);
                    c2=parse_de_darwinspl_fmec_constraints_NestedConstraint();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c2; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 4 :
                    // Fmec.g:2723:4: c3= parse_de_darwinspl_fmec_constraints_TimePointConstraint
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_constraints_TimePointConstraint_in_parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint2354);
                    c3=parse_de_darwinspl_fmec_constraints_TimePointConstraint();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c3; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 5 :
                    // Fmec.g:2724:4: c4= parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint_in_parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint2364);
                    c4=parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c4; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 32, parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint"



    // $ANTLR start "parse_de_darwinspl_fmec_constraints_BinaryConstraintChild"
    // Fmec.g:2728:1: parse_de_darwinspl_fmec_constraints_BinaryConstraintChild returns [de.darwinspl.fmec.constraints.BinaryConstraintChild element = null] : (c0= parse_de_darwinspl_fmec_constraints_UnaryConstraint |c1= parse_de_darwinspl_fmec_constraints_NestedConstraint |c2= parse_de_darwinspl_fmec_constraints_TimePointConstraint |c3= parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint );
    public final de.darwinspl.fmec.constraints.BinaryConstraintChild parse_de_darwinspl_fmec_constraints_BinaryConstraintChild() throws RecognitionException {
        de.darwinspl.fmec.constraints.BinaryConstraintChild element =  null;

        int parse_de_darwinspl_fmec_constraints_BinaryConstraintChild_StartIndex = input.index();

        de.darwinspl.fmec.constraints.UnaryConstraint c0 =null;

        de.darwinspl.fmec.constraints.NestedConstraint c1 =null;

        de.darwinspl.fmec.constraints.TimePointConstraint c2 =null;

        de.darwinspl.fmec.constraints.TimeIntervalConstraint c3 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 33) ) { return element; }

            // Fmec.g:2729:2: (c0= parse_de_darwinspl_fmec_constraints_UnaryConstraint |c1= parse_de_darwinspl_fmec_constraints_NestedConstraint |c2= parse_de_darwinspl_fmec_constraints_TimePointConstraint |c3= parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint )
            int alt12=4;
            switch ( input.LA(1) ) {
            case 37:
                {
                alt12=1;
                }
                break;
            case 47:
                {
                alt12=2;
                }
                break;
            case IDENTIFIER:
                {
                switch ( input.LA(2) ) {
                case 44:
                    {
                    int LA12_5 = input.LA(3);

                    if ( (LA12_5==24||(LA12_5 >= 26 && LA12_5 <= 27)||LA12_5==43||LA12_5==45) ) {
                        alt12=3;
                    }
                    else if ( (LA12_5==29) ) {
                        alt12=4;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return element;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 12, 5, input);

                        throw nvae;

                    }
                    }
                    break;
                case 15:
                    {
                    switch ( input.LA(3) ) {
                    case 42:
                        {
                        int LA12_13 = input.LA(4);

                        if ( (LA12_13==17) ) {
                            switch ( input.LA(5) ) {
                            case 21:
                                {
                                int LA12_25 = input.LA(6);

                                if ( (LA12_25==44) ) {
                                    int LA12_5 = input.LA(7);

                                    if ( (LA12_5==24||(LA12_5 >= 26 && LA12_5 <= 27)||LA12_5==43||LA12_5==45) ) {
                                        alt12=3;
                                    }
                                    else if ( (LA12_5==29) ) {
                                        alt12=4;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 5, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 25, input);

                                    throw nvae;

                                }
                                }
                                break;
                            case 20:
                                {
                                int LA12_26 = input.LA(6);

                                if ( (LA12_26==44) ) {
                                    int LA12_5 = input.LA(7);

                                    if ( (LA12_5==24||(LA12_5 >= 26 && LA12_5 <= 27)||LA12_5==43||LA12_5==45) ) {
                                        alt12=3;
                                    }
                                    else if ( (LA12_5==29) ) {
                                        alt12=4;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 5, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 26, input);

                                    throw nvae;

                                }
                                }
                                break;
                            case 19:
                                {
                                int LA12_27 = input.LA(6);

                                if ( (LA12_27==44) ) {
                                    int LA12_5 = input.LA(7);

                                    if ( (LA12_5==24||(LA12_5 >= 26 && LA12_5 <= 27)||LA12_5==43||LA12_5==45) ) {
                                        alt12=3;
                                    }
                                    else if ( (LA12_5==29) ) {
                                        alt12=4;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 5, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 27, input);

                                    throw nvae;

                                }
                                }
                                break;
                            case 18:
                                {
                                int LA12_28 = input.LA(6);

                                if ( (LA12_28==44) ) {
                                    int LA12_5 = input.LA(7);

                                    if ( (LA12_5==24||(LA12_5 >= 26 && LA12_5 <= 27)||LA12_5==43||LA12_5==45) ) {
                                        alt12=3;
                                    }
                                    else if ( (LA12_5==29) ) {
                                        alt12=4;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 5, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 28, input);

                                    throw nvae;

                                }
                                }
                                break;
                            case 22:
                                {
                                int LA12_29 = input.LA(6);

                                if ( (LA12_29==44) ) {
                                    int LA12_5 = input.LA(7);

                                    if ( (LA12_5==24||(LA12_5 >= 26 && LA12_5 <= 27)||LA12_5==43||LA12_5==45) ) {
                                        alt12=3;
                                    }
                                    else if ( (LA12_5==29) ) {
                                        alt12=4;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 5, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 29, input);

                                    throw nvae;

                                }
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 12, 17, input);

                                throw nvae;

                            }

                        }
                        else {
                            if (state.backtracking>0) {state.failed=true; return element;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 12, 13, input);

                            throw nvae;

                        }
                        }
                        break;
                    case 40:
                        {
                        int LA12_14 = input.LA(4);

                        if ( (LA12_14==17) ) {
                            int LA12_18 = input.LA(5);

                            if ( (LA12_18==IDENTIFIER) ) {
                                int LA12_30 = input.LA(6);

                                if ( (LA12_30==44) ) {
                                    int LA12_5 = input.LA(7);

                                    if ( (LA12_5==24||(LA12_5 >= 26 && LA12_5 <= 27)||LA12_5==43||LA12_5==45) ) {
                                        alt12=3;
                                    }
                                    else if ( (LA12_5==29) ) {
                                        alt12=4;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 5, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 30, input);

                                    throw nvae;

                                }
                            }
                            else {
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 12, 18, input);

                                throw nvae;

                            }
                        }
                        else {
                            if (state.backtracking>0) {state.failed=true; return element;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 12, 14, input);

                            throw nvae;

                        }
                        }
                        break;
                    case 39:
                        {
                        int LA12_15 = input.LA(4);

                        if ( (LA12_15==17) ) {
                            int LA12_19 = input.LA(5);

                            if ( (LA12_19==IDENTIFIER) ) {
                                int LA12_31 = input.LA(6);

                                if ( (LA12_31==44) ) {
                                    int LA12_5 = input.LA(7);

                                    if ( (LA12_5==24||(LA12_5 >= 26 && LA12_5 <= 27)||LA12_5==43||LA12_5==45) ) {
                                        alt12=3;
                                    }
                                    else if ( (LA12_5==29) ) {
                                        alt12=4;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 5, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 31, input);

                                    throw nvae;

                                }
                            }
                            else {
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 12, 19, input);

                                throw nvae;

                            }
                        }
                        else {
                            if (state.backtracking>0) {state.failed=true; return element;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 12, 15, input);

                            throw nvae;

                        }
                        }
                        break;
                    default:
                        if (state.backtracking>0) {state.failed=true; return element;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 12, 6, input);

                        throw nvae;

                    }

                    }
                    break;
                case 41:
                    {
                    switch ( input.LA(3) ) {
                    case 12:
                        {
                        switch ( input.LA(4) ) {
                        case INTEGER:
                            {
                            switch ( input.LA(5) ) {
                            case 46:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    switch ( input.LA(7) ) {
                                    case 36:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            switch ( input.LA(9) ) {
                                            case 28:
                                                {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA12_34 = input.LA(11);

                                                    if ( (LA12_34==32) ) {
                                                        switch ( input.LA(12) ) {
                                                        case INTEGER:
                                                            {
                                                            int LA12_35 = input.LA(13);

                                                            if ( (LA12_35==34) ) {
                                                                int LA12_24 = input.LA(14);

                                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                                    alt12=3;
                                                                }
                                                                else if ( (LA12_24==29) ) {
                                                                    alt12=4;
                                                                }
                                                                else {
                                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                                    NoViableAltException nvae =
                                                                        new NoViableAltException("", 12, 24, input);

                                                                    throw nvae;

                                                                }
                                                            }
                                                            else {
                                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                                NoViableAltException nvae =
                                                                    new NoViableAltException("", 12, 35, input);

                                                                throw nvae;

                                                            }
                                                            }
                                                            break;
                                                        case 24:
                                                        case 26:
                                                        case 27:
                                                        case 43:
                                                        case 45:
                                                            {
                                                            alt12=3;
                                                            }
                                                            break;
                                                        case 29:
                                                            {
                                                            alt12=4;
                                                            }
                                                            break;
                                                        default:
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 12, 23, input);

                                                            throw nvae;

                                                        }

                                                    }
                                                    else if ( (LA12_34==34) ) {
                                                        int LA12_24 = input.LA(12);

                                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                            alt12=3;
                                                        }
                                                        else if ( (LA12_24==29) ) {
                                                            alt12=4;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 12, 24, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 34, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt12=3;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt12=4;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 22, input);

                                                    throw nvae;

                                                }

                                                }
                                                break;
                                            case 32:
                                                {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA12_35 = input.LA(11);

                                                    if ( (LA12_35==34) ) {
                                                        int LA12_24 = input.LA(12);

                                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                            alt12=3;
                                                        }
                                                        else if ( (LA12_24==29) ) {
                                                            alt12=4;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 12, 24, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 35, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt12=3;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt12=4;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 23, input);

                                                    throw nvae;

                                                }

                                                }
                                                break;
                                            case 34:
                                                {
                                                int LA12_24 = input.LA(10);

                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                    alt12=3;
                                                }
                                                else if ( (LA12_24==29) ) {
                                                    alt12=4;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 24, input);

                                                    throw nvae;

                                                }
                                                }
                                                break;
                                            default:
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 33, input);

                                                throw nvae;

                                            }

                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt12=3;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt12=4;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 21, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 28:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA12_34 = input.LA(9);

                                            if ( (LA12_34==32) ) {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA12_35 = input.LA(11);

                                                    if ( (LA12_35==34) ) {
                                                        int LA12_24 = input.LA(12);

                                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                            alt12=3;
                                                        }
                                                        else if ( (LA12_24==29) ) {
                                                            alt12=4;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 12, 24, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 35, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt12=3;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt12=4;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 23, input);

                                                    throw nvae;

                                                }

                                            }
                                            else if ( (LA12_34==34) ) {
                                                int LA12_24 = input.LA(10);

                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                    alt12=3;
                                                }
                                                else if ( (LA12_24==29) ) {
                                                    alt12=4;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 24, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 34, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt12=3;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt12=4;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 22, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 32:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA12_35 = input.LA(9);

                                            if ( (LA12_35==34) ) {
                                                int LA12_24 = input.LA(10);

                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                    alt12=3;
                                                }
                                                else if ( (LA12_24==29) ) {
                                                    alt12=4;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 24, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 35, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt12=3;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt12=4;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 23, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 34:
                                        {
                                        int LA12_24 = input.LA(8);

                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                            alt12=3;
                                        }
                                        else if ( (LA12_24==29) ) {
                                            alt12=4;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 24, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 32, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt12=3;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt12=4;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 20, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 36:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    switch ( input.LA(7) ) {
                                    case 28:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA12_34 = input.LA(9);

                                            if ( (LA12_34==32) ) {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA12_35 = input.LA(11);

                                                    if ( (LA12_35==34) ) {
                                                        int LA12_24 = input.LA(12);

                                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                            alt12=3;
                                                        }
                                                        else if ( (LA12_24==29) ) {
                                                            alt12=4;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 12, 24, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 35, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt12=3;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt12=4;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 23, input);

                                                    throw nvae;

                                                }

                                            }
                                            else if ( (LA12_34==34) ) {
                                                int LA12_24 = input.LA(10);

                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                    alt12=3;
                                                }
                                                else if ( (LA12_24==29) ) {
                                                    alt12=4;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 24, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 34, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt12=3;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt12=4;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 22, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 32:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA12_35 = input.LA(9);

                                            if ( (LA12_35==34) ) {
                                                int LA12_24 = input.LA(10);

                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                    alt12=3;
                                                }
                                                else if ( (LA12_24==29) ) {
                                                    alt12=4;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 24, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 35, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt12=3;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt12=4;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 23, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 34:
                                        {
                                        int LA12_24 = input.LA(8);

                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                            alt12=3;
                                        }
                                        else if ( (LA12_24==29) ) {
                                            alt12=4;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 24, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 33, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt12=3;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt12=4;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 21, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 28:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    int LA12_34 = input.LA(7);

                                    if ( (LA12_34==32) ) {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA12_35 = input.LA(9);

                                            if ( (LA12_35==34) ) {
                                                int LA12_24 = input.LA(10);

                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                    alt12=3;
                                                }
                                                else if ( (LA12_24==29) ) {
                                                    alt12=4;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 24, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 35, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt12=3;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt12=4;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 23, input);

                                            throw nvae;

                                        }

                                    }
                                    else if ( (LA12_34==34) ) {
                                        int LA12_24 = input.LA(8);

                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                            alt12=3;
                                        }
                                        else if ( (LA12_24==29) ) {
                                            alt12=4;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 24, input);

                                            throw nvae;

                                        }
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 34, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt12=3;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt12=4;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 22, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 32:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    int LA12_35 = input.LA(7);

                                    if ( (LA12_35==34) ) {
                                        int LA12_24 = input.LA(8);

                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                            alt12=3;
                                        }
                                        else if ( (LA12_24==29) ) {
                                            alt12=4;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 24, input);

                                            throw nvae;

                                        }
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 35, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt12=3;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt12=4;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 23, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 34:
                                {
                                int LA12_24 = input.LA(6);

                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                    alt12=3;
                                }
                                else if ( (LA12_24==29) ) {
                                    alt12=4;
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 24, input);

                                    throw nvae;

                                }
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 12, 16, input);

                                throw nvae;

                            }

                            }
                            break;
                        case 24:
                        case 26:
                        case 27:
                        case 43:
                        case 45:
                            {
                            alt12=3;
                            }
                            break;
                        case 29:
                            {
                            alt12=4;
                            }
                            break;
                        default:
                            if (state.backtracking>0) {state.failed=true; return element;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 12, 9, input);

                            throw nvae;

                        }

                        }
                        break;
                    case 14:
                        {
                        switch ( input.LA(4) ) {
                        case INTEGER:
                            {
                            switch ( input.LA(5) ) {
                            case 46:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    switch ( input.LA(7) ) {
                                    case 36:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            switch ( input.LA(9) ) {
                                            case 28:
                                                {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA12_34 = input.LA(11);

                                                    if ( (LA12_34==32) ) {
                                                        switch ( input.LA(12) ) {
                                                        case INTEGER:
                                                            {
                                                            int LA12_35 = input.LA(13);

                                                            if ( (LA12_35==34) ) {
                                                                int LA12_24 = input.LA(14);

                                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                                    alt12=3;
                                                                }
                                                                else if ( (LA12_24==29) ) {
                                                                    alt12=4;
                                                                }
                                                                else {
                                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                                    NoViableAltException nvae =
                                                                        new NoViableAltException("", 12, 24, input);

                                                                    throw nvae;

                                                                }
                                                            }
                                                            else {
                                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                                NoViableAltException nvae =
                                                                    new NoViableAltException("", 12, 35, input);

                                                                throw nvae;

                                                            }
                                                            }
                                                            break;
                                                        case 24:
                                                        case 26:
                                                        case 27:
                                                        case 43:
                                                        case 45:
                                                            {
                                                            alt12=3;
                                                            }
                                                            break;
                                                        case 29:
                                                            {
                                                            alt12=4;
                                                            }
                                                            break;
                                                        default:
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 12, 23, input);

                                                            throw nvae;

                                                        }

                                                    }
                                                    else if ( (LA12_34==34) ) {
                                                        int LA12_24 = input.LA(12);

                                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                            alt12=3;
                                                        }
                                                        else if ( (LA12_24==29) ) {
                                                            alt12=4;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 12, 24, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 34, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt12=3;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt12=4;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 22, input);

                                                    throw nvae;

                                                }

                                                }
                                                break;
                                            case 32:
                                                {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA12_35 = input.LA(11);

                                                    if ( (LA12_35==34) ) {
                                                        int LA12_24 = input.LA(12);

                                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                            alt12=3;
                                                        }
                                                        else if ( (LA12_24==29) ) {
                                                            alt12=4;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 12, 24, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 35, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt12=3;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt12=4;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 23, input);

                                                    throw nvae;

                                                }

                                                }
                                                break;
                                            case 34:
                                                {
                                                int LA12_24 = input.LA(10);

                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                    alt12=3;
                                                }
                                                else if ( (LA12_24==29) ) {
                                                    alt12=4;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 24, input);

                                                    throw nvae;

                                                }
                                                }
                                                break;
                                            default:
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 33, input);

                                                throw nvae;

                                            }

                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt12=3;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt12=4;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 21, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 28:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA12_34 = input.LA(9);

                                            if ( (LA12_34==32) ) {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA12_35 = input.LA(11);

                                                    if ( (LA12_35==34) ) {
                                                        int LA12_24 = input.LA(12);

                                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                            alt12=3;
                                                        }
                                                        else if ( (LA12_24==29) ) {
                                                            alt12=4;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 12, 24, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 35, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt12=3;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt12=4;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 23, input);

                                                    throw nvae;

                                                }

                                            }
                                            else if ( (LA12_34==34) ) {
                                                int LA12_24 = input.LA(10);

                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                    alt12=3;
                                                }
                                                else if ( (LA12_24==29) ) {
                                                    alt12=4;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 24, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 34, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt12=3;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt12=4;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 22, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 32:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA12_35 = input.LA(9);

                                            if ( (LA12_35==34) ) {
                                                int LA12_24 = input.LA(10);

                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                    alt12=3;
                                                }
                                                else if ( (LA12_24==29) ) {
                                                    alt12=4;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 24, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 35, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt12=3;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt12=4;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 23, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 34:
                                        {
                                        int LA12_24 = input.LA(8);

                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                            alt12=3;
                                        }
                                        else if ( (LA12_24==29) ) {
                                            alt12=4;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 24, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 32, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt12=3;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt12=4;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 20, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 36:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    switch ( input.LA(7) ) {
                                    case 28:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA12_34 = input.LA(9);

                                            if ( (LA12_34==32) ) {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA12_35 = input.LA(11);

                                                    if ( (LA12_35==34) ) {
                                                        int LA12_24 = input.LA(12);

                                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                            alt12=3;
                                                        }
                                                        else if ( (LA12_24==29) ) {
                                                            alt12=4;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 12, 24, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 35, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt12=3;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt12=4;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 23, input);

                                                    throw nvae;

                                                }

                                            }
                                            else if ( (LA12_34==34) ) {
                                                int LA12_24 = input.LA(10);

                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                    alt12=3;
                                                }
                                                else if ( (LA12_24==29) ) {
                                                    alt12=4;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 24, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 34, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt12=3;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt12=4;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 22, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 32:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA12_35 = input.LA(9);

                                            if ( (LA12_35==34) ) {
                                                int LA12_24 = input.LA(10);

                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                    alt12=3;
                                                }
                                                else if ( (LA12_24==29) ) {
                                                    alt12=4;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 24, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 35, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt12=3;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt12=4;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 23, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 34:
                                        {
                                        int LA12_24 = input.LA(8);

                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                            alt12=3;
                                        }
                                        else if ( (LA12_24==29) ) {
                                            alt12=4;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 24, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 33, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt12=3;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt12=4;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 21, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 28:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    int LA12_34 = input.LA(7);

                                    if ( (LA12_34==32) ) {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA12_35 = input.LA(9);

                                            if ( (LA12_35==34) ) {
                                                int LA12_24 = input.LA(10);

                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                    alt12=3;
                                                }
                                                else if ( (LA12_24==29) ) {
                                                    alt12=4;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 24, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 35, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt12=3;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt12=4;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 23, input);

                                            throw nvae;

                                        }

                                    }
                                    else if ( (LA12_34==34) ) {
                                        int LA12_24 = input.LA(8);

                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                            alt12=3;
                                        }
                                        else if ( (LA12_24==29) ) {
                                            alt12=4;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 24, input);

                                            throw nvae;

                                        }
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 34, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt12=3;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt12=4;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 22, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 32:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    int LA12_35 = input.LA(7);

                                    if ( (LA12_35==34) ) {
                                        int LA12_24 = input.LA(8);

                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                            alt12=3;
                                        }
                                        else if ( (LA12_24==29) ) {
                                            alt12=4;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 24, input);

                                            throw nvae;

                                        }
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 35, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt12=3;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt12=4;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 23, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 34:
                                {
                                int LA12_24 = input.LA(6);

                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                    alt12=3;
                                }
                                else if ( (LA12_24==29) ) {
                                    alt12=4;
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 24, input);

                                    throw nvae;

                                }
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 12, 16, input);

                                throw nvae;

                            }

                            }
                            break;
                        case 24:
                        case 26:
                        case 27:
                        case 43:
                        case 45:
                            {
                            alt12=3;
                            }
                            break;
                        case 29:
                            {
                            alt12=4;
                            }
                            break;
                        default:
                            if (state.backtracking>0) {state.failed=true; return element;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 12, 10, input);

                            throw nvae;

                        }

                        }
                        break;
                    case 24:
                    case 26:
                    case 27:
                    case 43:
                    case 45:
                        {
                        alt12=3;
                        }
                        break;
                    case 29:
                        {
                        alt12=4;
                        }
                        break;
                    default:
                        if (state.backtracking>0) {state.failed=true; return element;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 12, 7, input);

                        throw nvae;

                    }

                    }
                    break;
                case 30:
                    {
                    switch ( input.LA(3) ) {
                    case 12:
                        {
                        switch ( input.LA(4) ) {
                        case INTEGER:
                            {
                            switch ( input.LA(5) ) {
                            case 46:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    switch ( input.LA(7) ) {
                                    case 36:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            switch ( input.LA(9) ) {
                                            case 28:
                                                {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA12_34 = input.LA(11);

                                                    if ( (LA12_34==32) ) {
                                                        switch ( input.LA(12) ) {
                                                        case INTEGER:
                                                            {
                                                            int LA12_35 = input.LA(13);

                                                            if ( (LA12_35==34) ) {
                                                                int LA12_24 = input.LA(14);

                                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                                    alt12=3;
                                                                }
                                                                else if ( (LA12_24==29) ) {
                                                                    alt12=4;
                                                                }
                                                                else {
                                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                                    NoViableAltException nvae =
                                                                        new NoViableAltException("", 12, 24, input);

                                                                    throw nvae;

                                                                }
                                                            }
                                                            else {
                                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                                NoViableAltException nvae =
                                                                    new NoViableAltException("", 12, 35, input);

                                                                throw nvae;

                                                            }
                                                            }
                                                            break;
                                                        case 24:
                                                        case 26:
                                                        case 27:
                                                        case 43:
                                                        case 45:
                                                            {
                                                            alt12=3;
                                                            }
                                                            break;
                                                        case 29:
                                                            {
                                                            alt12=4;
                                                            }
                                                            break;
                                                        default:
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 12, 23, input);

                                                            throw nvae;

                                                        }

                                                    }
                                                    else if ( (LA12_34==34) ) {
                                                        int LA12_24 = input.LA(12);

                                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                            alt12=3;
                                                        }
                                                        else if ( (LA12_24==29) ) {
                                                            alt12=4;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 12, 24, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 34, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt12=3;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt12=4;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 22, input);

                                                    throw nvae;

                                                }

                                                }
                                                break;
                                            case 32:
                                                {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA12_35 = input.LA(11);

                                                    if ( (LA12_35==34) ) {
                                                        int LA12_24 = input.LA(12);

                                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                            alt12=3;
                                                        }
                                                        else if ( (LA12_24==29) ) {
                                                            alt12=4;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 12, 24, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 35, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt12=3;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt12=4;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 23, input);

                                                    throw nvae;

                                                }

                                                }
                                                break;
                                            case 34:
                                                {
                                                int LA12_24 = input.LA(10);

                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                    alt12=3;
                                                }
                                                else if ( (LA12_24==29) ) {
                                                    alt12=4;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 24, input);

                                                    throw nvae;

                                                }
                                                }
                                                break;
                                            default:
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 33, input);

                                                throw nvae;

                                            }

                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt12=3;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt12=4;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 21, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 28:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA12_34 = input.LA(9);

                                            if ( (LA12_34==32) ) {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA12_35 = input.LA(11);

                                                    if ( (LA12_35==34) ) {
                                                        int LA12_24 = input.LA(12);

                                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                            alt12=3;
                                                        }
                                                        else if ( (LA12_24==29) ) {
                                                            alt12=4;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 12, 24, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 35, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt12=3;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt12=4;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 23, input);

                                                    throw nvae;

                                                }

                                            }
                                            else if ( (LA12_34==34) ) {
                                                int LA12_24 = input.LA(10);

                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                    alt12=3;
                                                }
                                                else if ( (LA12_24==29) ) {
                                                    alt12=4;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 24, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 34, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt12=3;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt12=4;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 22, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 32:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA12_35 = input.LA(9);

                                            if ( (LA12_35==34) ) {
                                                int LA12_24 = input.LA(10);

                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                    alt12=3;
                                                }
                                                else if ( (LA12_24==29) ) {
                                                    alt12=4;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 24, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 35, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt12=3;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt12=4;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 23, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 34:
                                        {
                                        int LA12_24 = input.LA(8);

                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                            alt12=3;
                                        }
                                        else if ( (LA12_24==29) ) {
                                            alt12=4;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 24, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 32, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt12=3;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt12=4;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 20, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 36:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    switch ( input.LA(7) ) {
                                    case 28:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA12_34 = input.LA(9);

                                            if ( (LA12_34==32) ) {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA12_35 = input.LA(11);

                                                    if ( (LA12_35==34) ) {
                                                        int LA12_24 = input.LA(12);

                                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                            alt12=3;
                                                        }
                                                        else if ( (LA12_24==29) ) {
                                                            alt12=4;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 12, 24, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 35, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt12=3;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt12=4;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 23, input);

                                                    throw nvae;

                                                }

                                            }
                                            else if ( (LA12_34==34) ) {
                                                int LA12_24 = input.LA(10);

                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                    alt12=3;
                                                }
                                                else if ( (LA12_24==29) ) {
                                                    alt12=4;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 24, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 34, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt12=3;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt12=4;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 22, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 32:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA12_35 = input.LA(9);

                                            if ( (LA12_35==34) ) {
                                                int LA12_24 = input.LA(10);

                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                    alt12=3;
                                                }
                                                else if ( (LA12_24==29) ) {
                                                    alt12=4;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 24, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 35, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt12=3;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt12=4;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 23, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 34:
                                        {
                                        int LA12_24 = input.LA(8);

                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                            alt12=3;
                                        }
                                        else if ( (LA12_24==29) ) {
                                            alt12=4;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 24, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 33, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt12=3;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt12=4;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 21, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 28:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    int LA12_34 = input.LA(7);

                                    if ( (LA12_34==32) ) {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA12_35 = input.LA(9);

                                            if ( (LA12_35==34) ) {
                                                int LA12_24 = input.LA(10);

                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                    alt12=3;
                                                }
                                                else if ( (LA12_24==29) ) {
                                                    alt12=4;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 24, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 35, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt12=3;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt12=4;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 23, input);

                                            throw nvae;

                                        }

                                    }
                                    else if ( (LA12_34==34) ) {
                                        int LA12_24 = input.LA(8);

                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                            alt12=3;
                                        }
                                        else if ( (LA12_24==29) ) {
                                            alt12=4;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 24, input);

                                            throw nvae;

                                        }
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 34, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt12=3;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt12=4;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 22, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 32:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    int LA12_35 = input.LA(7);

                                    if ( (LA12_35==34) ) {
                                        int LA12_24 = input.LA(8);

                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                            alt12=3;
                                        }
                                        else if ( (LA12_24==29) ) {
                                            alt12=4;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 24, input);

                                            throw nvae;

                                        }
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 35, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt12=3;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt12=4;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 23, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 34:
                                {
                                int LA12_24 = input.LA(6);

                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                    alt12=3;
                                }
                                else if ( (LA12_24==29) ) {
                                    alt12=4;
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 24, input);

                                    throw nvae;

                                }
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 12, 16, input);

                                throw nvae;

                            }

                            }
                            break;
                        case 24:
                        case 26:
                        case 27:
                        case 43:
                        case 45:
                            {
                            alt12=3;
                            }
                            break;
                        case 29:
                            {
                            alt12=4;
                            }
                            break;
                        default:
                            if (state.backtracking>0) {state.failed=true; return element;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 12, 9, input);

                            throw nvae;

                        }

                        }
                        break;
                    case 14:
                        {
                        switch ( input.LA(4) ) {
                        case INTEGER:
                            {
                            switch ( input.LA(5) ) {
                            case 46:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    switch ( input.LA(7) ) {
                                    case 36:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            switch ( input.LA(9) ) {
                                            case 28:
                                                {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA12_34 = input.LA(11);

                                                    if ( (LA12_34==32) ) {
                                                        switch ( input.LA(12) ) {
                                                        case INTEGER:
                                                            {
                                                            int LA12_35 = input.LA(13);

                                                            if ( (LA12_35==34) ) {
                                                                int LA12_24 = input.LA(14);

                                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                                    alt12=3;
                                                                }
                                                                else if ( (LA12_24==29) ) {
                                                                    alt12=4;
                                                                }
                                                                else {
                                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                                    NoViableAltException nvae =
                                                                        new NoViableAltException("", 12, 24, input);

                                                                    throw nvae;

                                                                }
                                                            }
                                                            else {
                                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                                NoViableAltException nvae =
                                                                    new NoViableAltException("", 12, 35, input);

                                                                throw nvae;

                                                            }
                                                            }
                                                            break;
                                                        case 24:
                                                        case 26:
                                                        case 27:
                                                        case 43:
                                                        case 45:
                                                            {
                                                            alt12=3;
                                                            }
                                                            break;
                                                        case 29:
                                                            {
                                                            alt12=4;
                                                            }
                                                            break;
                                                        default:
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 12, 23, input);

                                                            throw nvae;

                                                        }

                                                    }
                                                    else if ( (LA12_34==34) ) {
                                                        int LA12_24 = input.LA(12);

                                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                            alt12=3;
                                                        }
                                                        else if ( (LA12_24==29) ) {
                                                            alt12=4;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 12, 24, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 34, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt12=3;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt12=4;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 22, input);

                                                    throw nvae;

                                                }

                                                }
                                                break;
                                            case 32:
                                                {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA12_35 = input.LA(11);

                                                    if ( (LA12_35==34) ) {
                                                        int LA12_24 = input.LA(12);

                                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                            alt12=3;
                                                        }
                                                        else if ( (LA12_24==29) ) {
                                                            alt12=4;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 12, 24, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 35, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt12=3;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt12=4;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 23, input);

                                                    throw nvae;

                                                }

                                                }
                                                break;
                                            case 34:
                                                {
                                                int LA12_24 = input.LA(10);

                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                    alt12=3;
                                                }
                                                else if ( (LA12_24==29) ) {
                                                    alt12=4;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 24, input);

                                                    throw nvae;

                                                }
                                                }
                                                break;
                                            default:
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 33, input);

                                                throw nvae;

                                            }

                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt12=3;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt12=4;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 21, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 28:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA12_34 = input.LA(9);

                                            if ( (LA12_34==32) ) {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA12_35 = input.LA(11);

                                                    if ( (LA12_35==34) ) {
                                                        int LA12_24 = input.LA(12);

                                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                            alt12=3;
                                                        }
                                                        else if ( (LA12_24==29) ) {
                                                            alt12=4;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 12, 24, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 35, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt12=3;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt12=4;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 23, input);

                                                    throw nvae;

                                                }

                                            }
                                            else if ( (LA12_34==34) ) {
                                                int LA12_24 = input.LA(10);

                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                    alt12=3;
                                                }
                                                else if ( (LA12_24==29) ) {
                                                    alt12=4;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 24, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 34, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt12=3;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt12=4;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 22, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 32:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA12_35 = input.LA(9);

                                            if ( (LA12_35==34) ) {
                                                int LA12_24 = input.LA(10);

                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                    alt12=3;
                                                }
                                                else if ( (LA12_24==29) ) {
                                                    alt12=4;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 24, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 35, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt12=3;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt12=4;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 23, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 34:
                                        {
                                        int LA12_24 = input.LA(8);

                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                            alt12=3;
                                        }
                                        else if ( (LA12_24==29) ) {
                                            alt12=4;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 24, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 32, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt12=3;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt12=4;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 20, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 36:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    switch ( input.LA(7) ) {
                                    case 28:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA12_34 = input.LA(9);

                                            if ( (LA12_34==32) ) {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA12_35 = input.LA(11);

                                                    if ( (LA12_35==34) ) {
                                                        int LA12_24 = input.LA(12);

                                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                            alt12=3;
                                                        }
                                                        else if ( (LA12_24==29) ) {
                                                            alt12=4;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 12, 24, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 35, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt12=3;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt12=4;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 23, input);

                                                    throw nvae;

                                                }

                                            }
                                            else if ( (LA12_34==34) ) {
                                                int LA12_24 = input.LA(10);

                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                    alt12=3;
                                                }
                                                else if ( (LA12_24==29) ) {
                                                    alt12=4;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 24, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 34, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt12=3;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt12=4;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 22, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 32:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA12_35 = input.LA(9);

                                            if ( (LA12_35==34) ) {
                                                int LA12_24 = input.LA(10);

                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                    alt12=3;
                                                }
                                                else if ( (LA12_24==29) ) {
                                                    alt12=4;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 24, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 35, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt12=3;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt12=4;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 23, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 34:
                                        {
                                        int LA12_24 = input.LA(8);

                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                            alt12=3;
                                        }
                                        else if ( (LA12_24==29) ) {
                                            alt12=4;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 24, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 33, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt12=3;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt12=4;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 21, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 28:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    int LA12_34 = input.LA(7);

                                    if ( (LA12_34==32) ) {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA12_35 = input.LA(9);

                                            if ( (LA12_35==34) ) {
                                                int LA12_24 = input.LA(10);

                                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                    alt12=3;
                                                }
                                                else if ( (LA12_24==29) ) {
                                                    alt12=4;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 24, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 35, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt12=3;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt12=4;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 23, input);

                                            throw nvae;

                                        }

                                    }
                                    else if ( (LA12_34==34) ) {
                                        int LA12_24 = input.LA(8);

                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                            alt12=3;
                                        }
                                        else if ( (LA12_24==29) ) {
                                            alt12=4;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 24, input);

                                            throw nvae;

                                        }
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 34, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt12=3;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt12=4;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 22, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 32:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    int LA12_35 = input.LA(7);

                                    if ( (LA12_35==34) ) {
                                        int LA12_24 = input.LA(8);

                                        if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                            alt12=3;
                                        }
                                        else if ( (LA12_24==29) ) {
                                            alt12=4;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 24, input);

                                            throw nvae;

                                        }
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 35, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt12=3;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt12=4;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 23, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 34:
                                {
                                int LA12_24 = input.LA(6);

                                if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                    alt12=3;
                                }
                                else if ( (LA12_24==29) ) {
                                    alt12=4;
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 24, input);

                                    throw nvae;

                                }
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 12, 16, input);

                                throw nvae;

                            }

                            }
                            break;
                        case 24:
                        case 26:
                        case 27:
                        case 43:
                        case 45:
                            {
                            alt12=3;
                            }
                            break;
                        case 29:
                            {
                            alt12=4;
                            }
                            break;
                        default:
                            if (state.backtracking>0) {state.failed=true; return element;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 12, 10, input);

                            throw nvae;

                        }

                        }
                        break;
                    case 24:
                    case 26:
                    case 27:
                    case 43:
                    case 45:
                        {
                        alt12=3;
                        }
                        break;
                    case 29:
                        {
                        alt12=4;
                        }
                        break;
                    default:
                        if (state.backtracking>0) {state.failed=true; return element;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 12, 8, input);

                        throw nvae;

                    }

                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 12, 3, input);

                    throw nvae;

                }

                }
                break;
            case DATE:
                {
                switch ( input.LA(2) ) {
                case 12:
                    {
                    switch ( input.LA(3) ) {
                    case INTEGER:
                        {
                        switch ( input.LA(4) ) {
                        case 46:
                            {
                            switch ( input.LA(5) ) {
                            case INTEGER:
                                {
                                switch ( input.LA(6) ) {
                                case 36:
                                    {
                                    switch ( input.LA(7) ) {
                                    case INTEGER:
                                        {
                                        switch ( input.LA(8) ) {
                                        case 28:
                                            {
                                            switch ( input.LA(9) ) {
                                            case INTEGER:
                                                {
                                                int LA12_34 = input.LA(10);

                                                if ( (LA12_34==32) ) {
                                                    switch ( input.LA(11) ) {
                                                    case INTEGER:
                                                        {
                                                        int LA12_35 = input.LA(12);

                                                        if ( (LA12_35==34) ) {
                                                            int LA12_24 = input.LA(13);

                                                            if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                                alt12=3;
                                                            }
                                                            else if ( (LA12_24==29) ) {
                                                                alt12=4;
                                                            }
                                                            else {
                                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                                NoViableAltException nvae =
                                                                    new NoViableAltException("", 12, 24, input);

                                                                throw nvae;

                                                            }
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 12, 35, input);

                                                            throw nvae;

                                                        }
                                                        }
                                                        break;
                                                    case 24:
                                                    case 26:
                                                    case 27:
                                                    case 43:
                                                    case 45:
                                                        {
                                                        alt12=3;
                                                        }
                                                        break;
                                                    case 29:
                                                        {
                                                        alt12=4;
                                                        }
                                                        break;
                                                    default:
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 23, input);

                                                        throw nvae;

                                                    }

                                                }
                                                else if ( (LA12_34==34) ) {
                                                    int LA12_24 = input.LA(11);

                                                    if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                        alt12=3;
                                                    }
                                                    else if ( (LA12_24==29) ) {
                                                        alt12=4;
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 24, input);

                                                        throw nvae;

                                                    }
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 34, input);

                                                    throw nvae;

                                                }
                                                }
                                                break;
                                            case 24:
                                            case 26:
                                            case 27:
                                            case 43:
                                            case 45:
                                                {
                                                alt12=3;
                                                }
                                                break;
                                            case 29:
                                                {
                                                alt12=4;
                                                }
                                                break;
                                            default:
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 22, input);

                                                throw nvae;

                                            }

                                            }
                                            break;
                                        case 32:
                                            {
                                            switch ( input.LA(9) ) {
                                            case INTEGER:
                                                {
                                                int LA12_35 = input.LA(10);

                                                if ( (LA12_35==34) ) {
                                                    int LA12_24 = input.LA(11);

                                                    if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                        alt12=3;
                                                    }
                                                    else if ( (LA12_24==29) ) {
                                                        alt12=4;
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 24, input);

                                                        throw nvae;

                                                    }
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 35, input);

                                                    throw nvae;

                                                }
                                                }
                                                break;
                                            case 24:
                                            case 26:
                                            case 27:
                                            case 43:
                                            case 45:
                                                {
                                                alt12=3;
                                                }
                                                break;
                                            case 29:
                                                {
                                                alt12=4;
                                                }
                                                break;
                                            default:
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 23, input);

                                                throw nvae;

                                            }

                                            }
                                            break;
                                        case 34:
                                            {
                                            int LA12_24 = input.LA(9);

                                            if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                alt12=3;
                                            }
                                            else if ( (LA12_24==29) ) {
                                                alt12=4;
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 24, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 33, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 24:
                                    case 26:
                                    case 27:
                                    case 43:
                                    case 45:
                                        {
                                        alt12=3;
                                        }
                                        break;
                                    case 29:
                                        {
                                        alt12=4;
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 21, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 28:
                                    {
                                    switch ( input.LA(7) ) {
                                    case INTEGER:
                                        {
                                        int LA12_34 = input.LA(8);

                                        if ( (LA12_34==32) ) {
                                            switch ( input.LA(9) ) {
                                            case INTEGER:
                                                {
                                                int LA12_35 = input.LA(10);

                                                if ( (LA12_35==34) ) {
                                                    int LA12_24 = input.LA(11);

                                                    if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                        alt12=3;
                                                    }
                                                    else if ( (LA12_24==29) ) {
                                                        alt12=4;
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 24, input);

                                                        throw nvae;

                                                    }
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 35, input);

                                                    throw nvae;

                                                }
                                                }
                                                break;
                                            case 24:
                                            case 26:
                                            case 27:
                                            case 43:
                                            case 45:
                                                {
                                                alt12=3;
                                                }
                                                break;
                                            case 29:
                                                {
                                                alt12=4;
                                                }
                                                break;
                                            default:
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 23, input);

                                                throw nvae;

                                            }

                                        }
                                        else if ( (LA12_34==34) ) {
                                            int LA12_24 = input.LA(9);

                                            if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                alt12=3;
                                            }
                                            else if ( (LA12_24==29) ) {
                                                alt12=4;
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 24, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 34, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    case 24:
                                    case 26:
                                    case 27:
                                    case 43:
                                    case 45:
                                        {
                                        alt12=3;
                                        }
                                        break;
                                    case 29:
                                        {
                                        alt12=4;
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 22, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 32:
                                    {
                                    switch ( input.LA(7) ) {
                                    case INTEGER:
                                        {
                                        int LA12_35 = input.LA(8);

                                        if ( (LA12_35==34) ) {
                                            int LA12_24 = input.LA(9);

                                            if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                alt12=3;
                                            }
                                            else if ( (LA12_24==29) ) {
                                                alt12=4;
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 24, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 35, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    case 24:
                                    case 26:
                                    case 27:
                                    case 43:
                                    case 45:
                                        {
                                        alt12=3;
                                        }
                                        break;
                                    case 29:
                                        {
                                        alt12=4;
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 23, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 34:
                                    {
                                    int LA12_24 = input.LA(7);

                                    if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                        alt12=3;
                                    }
                                    else if ( (LA12_24==29) ) {
                                        alt12=4;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 24, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 32, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 24:
                            case 26:
                            case 27:
                            case 43:
                            case 45:
                                {
                                alt12=3;
                                }
                                break;
                            case 29:
                                {
                                alt12=4;
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 12, 20, input);

                                throw nvae;

                            }

                            }
                            break;
                        case 36:
                            {
                            switch ( input.LA(5) ) {
                            case INTEGER:
                                {
                                switch ( input.LA(6) ) {
                                case 28:
                                    {
                                    switch ( input.LA(7) ) {
                                    case INTEGER:
                                        {
                                        int LA12_34 = input.LA(8);

                                        if ( (LA12_34==32) ) {
                                            switch ( input.LA(9) ) {
                                            case INTEGER:
                                                {
                                                int LA12_35 = input.LA(10);

                                                if ( (LA12_35==34) ) {
                                                    int LA12_24 = input.LA(11);

                                                    if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                        alt12=3;
                                                    }
                                                    else if ( (LA12_24==29) ) {
                                                        alt12=4;
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 24, input);

                                                        throw nvae;

                                                    }
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 35, input);

                                                    throw nvae;

                                                }
                                                }
                                                break;
                                            case 24:
                                            case 26:
                                            case 27:
                                            case 43:
                                            case 45:
                                                {
                                                alt12=3;
                                                }
                                                break;
                                            case 29:
                                                {
                                                alt12=4;
                                                }
                                                break;
                                            default:
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 23, input);

                                                throw nvae;

                                            }

                                        }
                                        else if ( (LA12_34==34) ) {
                                            int LA12_24 = input.LA(9);

                                            if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                alt12=3;
                                            }
                                            else if ( (LA12_24==29) ) {
                                                alt12=4;
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 24, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 34, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    case 24:
                                    case 26:
                                    case 27:
                                    case 43:
                                    case 45:
                                        {
                                        alt12=3;
                                        }
                                        break;
                                    case 29:
                                        {
                                        alt12=4;
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 22, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 32:
                                    {
                                    switch ( input.LA(7) ) {
                                    case INTEGER:
                                        {
                                        int LA12_35 = input.LA(8);

                                        if ( (LA12_35==34) ) {
                                            int LA12_24 = input.LA(9);

                                            if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                alt12=3;
                                            }
                                            else if ( (LA12_24==29) ) {
                                                alt12=4;
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 24, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 35, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    case 24:
                                    case 26:
                                    case 27:
                                    case 43:
                                    case 45:
                                        {
                                        alt12=3;
                                        }
                                        break;
                                    case 29:
                                        {
                                        alt12=4;
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 23, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 34:
                                    {
                                    int LA12_24 = input.LA(7);

                                    if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                        alt12=3;
                                    }
                                    else if ( (LA12_24==29) ) {
                                        alt12=4;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 24, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 33, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 24:
                            case 26:
                            case 27:
                            case 43:
                            case 45:
                                {
                                alt12=3;
                                }
                                break;
                            case 29:
                                {
                                alt12=4;
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 12, 21, input);

                                throw nvae;

                            }

                            }
                            break;
                        case 28:
                            {
                            switch ( input.LA(5) ) {
                            case INTEGER:
                                {
                                int LA12_34 = input.LA(6);

                                if ( (LA12_34==32) ) {
                                    switch ( input.LA(7) ) {
                                    case INTEGER:
                                        {
                                        int LA12_35 = input.LA(8);

                                        if ( (LA12_35==34) ) {
                                            int LA12_24 = input.LA(9);

                                            if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                alt12=3;
                                            }
                                            else if ( (LA12_24==29) ) {
                                                alt12=4;
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 24, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 35, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    case 24:
                                    case 26:
                                    case 27:
                                    case 43:
                                    case 45:
                                        {
                                        alt12=3;
                                        }
                                        break;
                                    case 29:
                                        {
                                        alt12=4;
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 23, input);

                                        throw nvae;

                                    }

                                }
                                else if ( (LA12_34==34) ) {
                                    int LA12_24 = input.LA(7);

                                    if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                        alt12=3;
                                    }
                                    else if ( (LA12_24==29) ) {
                                        alt12=4;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 24, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 34, input);

                                    throw nvae;

                                }
                                }
                                break;
                            case 24:
                            case 26:
                            case 27:
                            case 43:
                            case 45:
                                {
                                alt12=3;
                                }
                                break;
                            case 29:
                                {
                                alt12=4;
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 12, 22, input);

                                throw nvae;

                            }

                            }
                            break;
                        case 32:
                            {
                            switch ( input.LA(5) ) {
                            case INTEGER:
                                {
                                int LA12_35 = input.LA(6);

                                if ( (LA12_35==34) ) {
                                    int LA12_24 = input.LA(7);

                                    if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                        alt12=3;
                                    }
                                    else if ( (LA12_24==29) ) {
                                        alt12=4;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 24, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 35, input);

                                    throw nvae;

                                }
                                }
                                break;
                            case 24:
                            case 26:
                            case 27:
                            case 43:
                            case 45:
                                {
                                alt12=3;
                                }
                                break;
                            case 29:
                                {
                                alt12=4;
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 12, 23, input);

                                throw nvae;

                            }

                            }
                            break;
                        case 34:
                            {
                            int LA12_24 = input.LA(5);

                            if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                alt12=3;
                            }
                            else if ( (LA12_24==29) ) {
                                alt12=4;
                            }
                            else {
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 12, 24, input);

                                throw nvae;

                            }
                            }
                            break;
                        default:
                            if (state.backtracking>0) {state.failed=true; return element;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 12, 16, input);

                            throw nvae;

                        }

                        }
                        break;
                    case 24:
                    case 26:
                    case 27:
                    case 43:
                    case 45:
                        {
                        alt12=3;
                        }
                        break;
                    case 29:
                        {
                        alt12=4;
                        }
                        break;
                    default:
                        if (state.backtracking>0) {state.failed=true; return element;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 12, 9, input);

                        throw nvae;

                    }

                    }
                    break;
                case 14:
                    {
                    switch ( input.LA(3) ) {
                    case INTEGER:
                        {
                        switch ( input.LA(4) ) {
                        case 46:
                            {
                            switch ( input.LA(5) ) {
                            case INTEGER:
                                {
                                switch ( input.LA(6) ) {
                                case 36:
                                    {
                                    switch ( input.LA(7) ) {
                                    case INTEGER:
                                        {
                                        switch ( input.LA(8) ) {
                                        case 28:
                                            {
                                            switch ( input.LA(9) ) {
                                            case INTEGER:
                                                {
                                                int LA12_34 = input.LA(10);

                                                if ( (LA12_34==32) ) {
                                                    switch ( input.LA(11) ) {
                                                    case INTEGER:
                                                        {
                                                        int LA12_35 = input.LA(12);

                                                        if ( (LA12_35==34) ) {
                                                            int LA12_24 = input.LA(13);

                                                            if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                                alt12=3;
                                                            }
                                                            else if ( (LA12_24==29) ) {
                                                                alt12=4;
                                                            }
                                                            else {
                                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                                NoViableAltException nvae =
                                                                    new NoViableAltException("", 12, 24, input);

                                                                throw nvae;

                                                            }
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 12, 35, input);

                                                            throw nvae;

                                                        }
                                                        }
                                                        break;
                                                    case 24:
                                                    case 26:
                                                    case 27:
                                                    case 43:
                                                    case 45:
                                                        {
                                                        alt12=3;
                                                        }
                                                        break;
                                                    case 29:
                                                        {
                                                        alt12=4;
                                                        }
                                                        break;
                                                    default:
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 23, input);

                                                        throw nvae;

                                                    }

                                                }
                                                else if ( (LA12_34==34) ) {
                                                    int LA12_24 = input.LA(11);

                                                    if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                        alt12=3;
                                                    }
                                                    else if ( (LA12_24==29) ) {
                                                        alt12=4;
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 24, input);

                                                        throw nvae;

                                                    }
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 34, input);

                                                    throw nvae;

                                                }
                                                }
                                                break;
                                            case 24:
                                            case 26:
                                            case 27:
                                            case 43:
                                            case 45:
                                                {
                                                alt12=3;
                                                }
                                                break;
                                            case 29:
                                                {
                                                alt12=4;
                                                }
                                                break;
                                            default:
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 22, input);

                                                throw nvae;

                                            }

                                            }
                                            break;
                                        case 32:
                                            {
                                            switch ( input.LA(9) ) {
                                            case INTEGER:
                                                {
                                                int LA12_35 = input.LA(10);

                                                if ( (LA12_35==34) ) {
                                                    int LA12_24 = input.LA(11);

                                                    if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                        alt12=3;
                                                    }
                                                    else if ( (LA12_24==29) ) {
                                                        alt12=4;
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 24, input);

                                                        throw nvae;

                                                    }
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 35, input);

                                                    throw nvae;

                                                }
                                                }
                                                break;
                                            case 24:
                                            case 26:
                                            case 27:
                                            case 43:
                                            case 45:
                                                {
                                                alt12=3;
                                                }
                                                break;
                                            case 29:
                                                {
                                                alt12=4;
                                                }
                                                break;
                                            default:
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 23, input);

                                                throw nvae;

                                            }

                                            }
                                            break;
                                        case 34:
                                            {
                                            int LA12_24 = input.LA(9);

                                            if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                alt12=3;
                                            }
                                            else if ( (LA12_24==29) ) {
                                                alt12=4;
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 24, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 33, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 24:
                                    case 26:
                                    case 27:
                                    case 43:
                                    case 45:
                                        {
                                        alt12=3;
                                        }
                                        break;
                                    case 29:
                                        {
                                        alt12=4;
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 21, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 28:
                                    {
                                    switch ( input.LA(7) ) {
                                    case INTEGER:
                                        {
                                        int LA12_34 = input.LA(8);

                                        if ( (LA12_34==32) ) {
                                            switch ( input.LA(9) ) {
                                            case INTEGER:
                                                {
                                                int LA12_35 = input.LA(10);

                                                if ( (LA12_35==34) ) {
                                                    int LA12_24 = input.LA(11);

                                                    if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                        alt12=3;
                                                    }
                                                    else if ( (LA12_24==29) ) {
                                                        alt12=4;
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 24, input);

                                                        throw nvae;

                                                    }
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 35, input);

                                                    throw nvae;

                                                }
                                                }
                                                break;
                                            case 24:
                                            case 26:
                                            case 27:
                                            case 43:
                                            case 45:
                                                {
                                                alt12=3;
                                                }
                                                break;
                                            case 29:
                                                {
                                                alt12=4;
                                                }
                                                break;
                                            default:
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 23, input);

                                                throw nvae;

                                            }

                                        }
                                        else if ( (LA12_34==34) ) {
                                            int LA12_24 = input.LA(9);

                                            if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                alt12=3;
                                            }
                                            else if ( (LA12_24==29) ) {
                                                alt12=4;
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 24, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 34, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    case 24:
                                    case 26:
                                    case 27:
                                    case 43:
                                    case 45:
                                        {
                                        alt12=3;
                                        }
                                        break;
                                    case 29:
                                        {
                                        alt12=4;
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 22, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 32:
                                    {
                                    switch ( input.LA(7) ) {
                                    case INTEGER:
                                        {
                                        int LA12_35 = input.LA(8);

                                        if ( (LA12_35==34) ) {
                                            int LA12_24 = input.LA(9);

                                            if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                alt12=3;
                                            }
                                            else if ( (LA12_24==29) ) {
                                                alt12=4;
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 24, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 35, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    case 24:
                                    case 26:
                                    case 27:
                                    case 43:
                                    case 45:
                                        {
                                        alt12=3;
                                        }
                                        break;
                                    case 29:
                                        {
                                        alt12=4;
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 23, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 34:
                                    {
                                    int LA12_24 = input.LA(7);

                                    if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                        alt12=3;
                                    }
                                    else if ( (LA12_24==29) ) {
                                        alt12=4;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 24, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 32, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 24:
                            case 26:
                            case 27:
                            case 43:
                            case 45:
                                {
                                alt12=3;
                                }
                                break;
                            case 29:
                                {
                                alt12=4;
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 12, 20, input);

                                throw nvae;

                            }

                            }
                            break;
                        case 36:
                            {
                            switch ( input.LA(5) ) {
                            case INTEGER:
                                {
                                switch ( input.LA(6) ) {
                                case 28:
                                    {
                                    switch ( input.LA(7) ) {
                                    case INTEGER:
                                        {
                                        int LA12_34 = input.LA(8);

                                        if ( (LA12_34==32) ) {
                                            switch ( input.LA(9) ) {
                                            case INTEGER:
                                                {
                                                int LA12_35 = input.LA(10);

                                                if ( (LA12_35==34) ) {
                                                    int LA12_24 = input.LA(11);

                                                    if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                        alt12=3;
                                                    }
                                                    else if ( (LA12_24==29) ) {
                                                        alt12=4;
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 12, 24, input);

                                                        throw nvae;

                                                    }
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 12, 35, input);

                                                    throw nvae;

                                                }
                                                }
                                                break;
                                            case 24:
                                            case 26:
                                            case 27:
                                            case 43:
                                            case 45:
                                                {
                                                alt12=3;
                                                }
                                                break;
                                            case 29:
                                                {
                                                alt12=4;
                                                }
                                                break;
                                            default:
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 23, input);

                                                throw nvae;

                                            }

                                        }
                                        else if ( (LA12_34==34) ) {
                                            int LA12_24 = input.LA(9);

                                            if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                alt12=3;
                                            }
                                            else if ( (LA12_24==29) ) {
                                                alt12=4;
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 24, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 34, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    case 24:
                                    case 26:
                                    case 27:
                                    case 43:
                                    case 45:
                                        {
                                        alt12=3;
                                        }
                                        break;
                                    case 29:
                                        {
                                        alt12=4;
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 22, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 32:
                                    {
                                    switch ( input.LA(7) ) {
                                    case INTEGER:
                                        {
                                        int LA12_35 = input.LA(8);

                                        if ( (LA12_35==34) ) {
                                            int LA12_24 = input.LA(9);

                                            if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                alt12=3;
                                            }
                                            else if ( (LA12_24==29) ) {
                                                alt12=4;
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 24, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 35, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    case 24:
                                    case 26:
                                    case 27:
                                    case 43:
                                    case 45:
                                        {
                                        alt12=3;
                                        }
                                        break;
                                    case 29:
                                        {
                                        alt12=4;
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 23, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 34:
                                    {
                                    int LA12_24 = input.LA(7);

                                    if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                        alt12=3;
                                    }
                                    else if ( (LA12_24==29) ) {
                                        alt12=4;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 24, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 33, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 24:
                            case 26:
                            case 27:
                            case 43:
                            case 45:
                                {
                                alt12=3;
                                }
                                break;
                            case 29:
                                {
                                alt12=4;
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 12, 21, input);

                                throw nvae;

                            }

                            }
                            break;
                        case 28:
                            {
                            switch ( input.LA(5) ) {
                            case INTEGER:
                                {
                                int LA12_34 = input.LA(6);

                                if ( (LA12_34==32) ) {
                                    switch ( input.LA(7) ) {
                                    case INTEGER:
                                        {
                                        int LA12_35 = input.LA(8);

                                        if ( (LA12_35==34) ) {
                                            int LA12_24 = input.LA(9);

                                            if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                                alt12=3;
                                            }
                                            else if ( (LA12_24==29) ) {
                                                alt12=4;
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 12, 24, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 12, 35, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    case 24:
                                    case 26:
                                    case 27:
                                    case 43:
                                    case 45:
                                        {
                                        alt12=3;
                                        }
                                        break;
                                    case 29:
                                        {
                                        alt12=4;
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 23, input);

                                        throw nvae;

                                    }

                                }
                                else if ( (LA12_34==34) ) {
                                    int LA12_24 = input.LA(7);

                                    if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                        alt12=3;
                                    }
                                    else if ( (LA12_24==29) ) {
                                        alt12=4;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 24, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 34, input);

                                    throw nvae;

                                }
                                }
                                break;
                            case 24:
                            case 26:
                            case 27:
                            case 43:
                            case 45:
                                {
                                alt12=3;
                                }
                                break;
                            case 29:
                                {
                                alt12=4;
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 12, 22, input);

                                throw nvae;

                            }

                            }
                            break;
                        case 32:
                            {
                            switch ( input.LA(5) ) {
                            case INTEGER:
                                {
                                int LA12_35 = input.LA(6);

                                if ( (LA12_35==34) ) {
                                    int LA12_24 = input.LA(7);

                                    if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                        alt12=3;
                                    }
                                    else if ( (LA12_24==29) ) {
                                        alt12=4;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 12, 24, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 12, 35, input);

                                    throw nvae;

                                }
                                }
                                break;
                            case 24:
                            case 26:
                            case 27:
                            case 43:
                            case 45:
                                {
                                alt12=3;
                                }
                                break;
                            case 29:
                                {
                                alt12=4;
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 12, 23, input);

                                throw nvae;

                            }

                            }
                            break;
                        case 34:
                            {
                            int LA12_24 = input.LA(5);

                            if ( (LA12_24==24||(LA12_24 >= 26 && LA12_24 <= 27)||LA12_24==43||LA12_24==45) ) {
                                alt12=3;
                            }
                            else if ( (LA12_24==29) ) {
                                alt12=4;
                            }
                            else {
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 12, 24, input);

                                throw nvae;

                            }
                            }
                            break;
                        default:
                            if (state.backtracking>0) {state.failed=true; return element;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 12, 16, input);

                            throw nvae;

                        }

                        }
                        break;
                    case 24:
                    case 26:
                    case 27:
                    case 43:
                    case 45:
                        {
                        alt12=3;
                        }
                        break;
                    case 29:
                        {
                        alt12=4;
                        }
                        break;
                    default:
                        if (state.backtracking>0) {state.failed=true; return element;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 12, 10, input);

                        throw nvae;

                    }

                    }
                    break;
                case 24:
                case 26:
                case 27:
                case 43:
                case 45:
                    {
                    alt12=3;
                    }
                    break;
                case 29:
                    {
                    alt12=4;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 12, 4, input);

                    throw nvae;

                }

                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;

            }

            switch (alt12) {
                case 1 :
                    // Fmec.g:2730:2: c0= parse_de_darwinspl_fmec_constraints_UnaryConstraint
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_constraints_UnaryConstraint_in_parse_de_darwinspl_fmec_constraints_BinaryConstraintChild2385);
                    c0=parse_de_darwinspl_fmec_constraints_UnaryConstraint();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Fmec.g:2731:4: c1= parse_de_darwinspl_fmec_constraints_NestedConstraint
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_constraints_NestedConstraint_in_parse_de_darwinspl_fmec_constraints_BinaryConstraintChild2395);
                    c1=parse_de_darwinspl_fmec_constraints_NestedConstraint();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 3 :
                    // Fmec.g:2732:4: c2= parse_de_darwinspl_fmec_constraints_TimePointConstraint
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_constraints_TimePointConstraint_in_parse_de_darwinspl_fmec_constraints_BinaryConstraintChild2405);
                    c2=parse_de_darwinspl_fmec_constraints_TimePointConstraint();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c2; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 4 :
                    // Fmec.g:2733:4: c3= parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint_in_parse_de_darwinspl_fmec_constraints_BinaryConstraintChild2415);
                    c3=parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c3; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 33, parse_de_darwinspl_fmec_constraints_BinaryConstraintChild_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_constraints_BinaryConstraintChild"



    // $ANTLR start "parse_de_darwinspl_fmec_keywords_logical_BinaryLogicalKeyword"
    // Fmec.g:2737:1: parse_de_darwinspl_fmec_keywords_logical_BinaryLogicalKeyword returns [de.darwinspl.fmec.keywords.logical.BinaryLogicalKeyword element = null] : (c0= parse_de_darwinspl_fmec_keywords_logical_KeywordAnd |c1= parse_de_darwinspl_fmec_keywords_logical_KeywordOr |c2= parse_de_darwinspl_fmec_keywords_logical_KeywordImplies );
    public final de.darwinspl.fmec.keywords.logical.BinaryLogicalKeyword parse_de_darwinspl_fmec_keywords_logical_BinaryLogicalKeyword() throws RecognitionException {
        de.darwinspl.fmec.keywords.logical.BinaryLogicalKeyword element =  null;

        int parse_de_darwinspl_fmec_keywords_logical_BinaryLogicalKeyword_StartIndex = input.index();

        de.darwinspl.fmec.keywords.logical.KeywordAnd c0 =null;

        de.darwinspl.fmec.keywords.logical.KeywordOr c1 =null;

        de.darwinspl.fmec.keywords.logical.KeywordImplies c2 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 34) ) { return element; }

            // Fmec.g:2738:2: (c0= parse_de_darwinspl_fmec_keywords_logical_KeywordAnd |c1= parse_de_darwinspl_fmec_keywords_logical_KeywordOr |c2= parse_de_darwinspl_fmec_keywords_logical_KeywordImplies )
            int alt13=3;
            switch ( input.LA(1) ) {
            case 25:
                {
                alt13=1;
                }
                break;
            case 38:
                {
                alt13=2;
                }
                break;
            case 33:
                {
                alt13=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;

            }

            switch (alt13) {
                case 1 :
                    // Fmec.g:2739:2: c0= parse_de_darwinspl_fmec_keywords_logical_KeywordAnd
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_keywords_logical_KeywordAnd_in_parse_de_darwinspl_fmec_keywords_logical_BinaryLogicalKeyword2436);
                    c0=parse_de_darwinspl_fmec_keywords_logical_KeywordAnd();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Fmec.g:2740:4: c1= parse_de_darwinspl_fmec_keywords_logical_KeywordOr
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_keywords_logical_KeywordOr_in_parse_de_darwinspl_fmec_keywords_logical_BinaryLogicalKeyword2446);
                    c1=parse_de_darwinspl_fmec_keywords_logical_KeywordOr();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 3 :
                    // Fmec.g:2741:4: c2= parse_de_darwinspl_fmec_keywords_logical_KeywordImplies
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_keywords_logical_KeywordImplies_in_parse_de_darwinspl_fmec_keywords_logical_BinaryLogicalKeyword2456);
                    c2=parse_de_darwinspl_fmec_keywords_logical_KeywordImplies();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c2; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 34, parse_de_darwinspl_fmec_keywords_logical_BinaryLogicalKeyword_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_keywords_logical_BinaryLogicalKeyword"



    // $ANTLR start "parse_de_darwinspl_fmec_keywords_logical_UnaryLogicalKeyword"
    // Fmec.g:2745:1: parse_de_darwinspl_fmec_keywords_logical_UnaryLogicalKeyword returns [de.darwinspl.fmec.keywords.logical.UnaryLogicalKeyword element = null] : c0= parse_de_darwinspl_fmec_keywords_logical_KeywordNot ;
    public final de.darwinspl.fmec.keywords.logical.UnaryLogicalKeyword parse_de_darwinspl_fmec_keywords_logical_UnaryLogicalKeyword() throws RecognitionException {
        de.darwinspl.fmec.keywords.logical.UnaryLogicalKeyword element =  null;

        int parse_de_darwinspl_fmec_keywords_logical_UnaryLogicalKeyword_StartIndex = input.index();

        de.darwinspl.fmec.keywords.logical.KeywordNot c0 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 35) ) { return element; }

            // Fmec.g:2746:2: (c0= parse_de_darwinspl_fmec_keywords_logical_KeywordNot )
            // Fmec.g:2747:2: c0= parse_de_darwinspl_fmec_keywords_logical_KeywordNot
            {
            pushFollow(FOLLOW_parse_de_darwinspl_fmec_keywords_logical_KeywordNot_in_parse_de_darwinspl_fmec_keywords_logical_UnaryLogicalKeyword2477);
            c0=parse_de_darwinspl_fmec_keywords_logical_KeywordNot();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 35, parse_de_darwinspl_fmec_keywords_logical_UnaryLogicalKeyword_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_keywords_logical_UnaryLogicalKeyword"



    // $ANTLR start "parse_de_darwinspl_fmec_constraints_UnaryConstraintChild"
    // Fmec.g:2751:1: parse_de_darwinspl_fmec_constraints_UnaryConstraintChild returns [de.darwinspl.fmec.constraints.UnaryConstraintChild element = null] : (c0= parse_de_darwinspl_fmec_constraints_NestedConstraint |c1= parse_de_darwinspl_fmec_constraints_TimePointConstraint |c2= parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint );
    public final de.darwinspl.fmec.constraints.UnaryConstraintChild parse_de_darwinspl_fmec_constraints_UnaryConstraintChild() throws RecognitionException {
        de.darwinspl.fmec.constraints.UnaryConstraintChild element =  null;

        int parse_de_darwinspl_fmec_constraints_UnaryConstraintChild_StartIndex = input.index();

        de.darwinspl.fmec.constraints.NestedConstraint c0 =null;

        de.darwinspl.fmec.constraints.TimePointConstraint c1 =null;

        de.darwinspl.fmec.constraints.TimeIntervalConstraint c2 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 36) ) { return element; }

            // Fmec.g:2752:2: (c0= parse_de_darwinspl_fmec_constraints_NestedConstraint |c1= parse_de_darwinspl_fmec_constraints_TimePointConstraint |c2= parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint )
            int alt14=3;
            switch ( input.LA(1) ) {
            case 47:
                {
                alt14=1;
                }
                break;
            case IDENTIFIER:
                {
                switch ( input.LA(2) ) {
                case 44:
                    {
                    int LA14_4 = input.LA(3);

                    if ( (LA14_4==24||(LA14_4 >= 26 && LA14_4 <= 27)||LA14_4==43||LA14_4==45) ) {
                        alt14=2;
                    }
                    else if ( (LA14_4==29) ) {
                        alt14=3;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return element;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 14, 4, input);

                        throw nvae;

                    }
                    }
                    break;
                case 15:
                    {
                    switch ( input.LA(3) ) {
                    case 42:
                        {
                        int LA14_12 = input.LA(4);

                        if ( (LA14_12==17) ) {
                            switch ( input.LA(5) ) {
                            case 21:
                                {
                                int LA14_24 = input.LA(6);

                                if ( (LA14_24==44) ) {
                                    int LA14_4 = input.LA(7);

                                    if ( (LA14_4==24||(LA14_4 >= 26 && LA14_4 <= 27)||LA14_4==43||LA14_4==45) ) {
                                        alt14=2;
                                    }
                                    else if ( (LA14_4==29) ) {
                                        alt14=3;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 4, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 24, input);

                                    throw nvae;

                                }
                                }
                                break;
                            case 20:
                                {
                                int LA14_25 = input.LA(6);

                                if ( (LA14_25==44) ) {
                                    int LA14_4 = input.LA(7);

                                    if ( (LA14_4==24||(LA14_4 >= 26 && LA14_4 <= 27)||LA14_4==43||LA14_4==45) ) {
                                        alt14=2;
                                    }
                                    else if ( (LA14_4==29) ) {
                                        alt14=3;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 4, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 25, input);

                                    throw nvae;

                                }
                                }
                                break;
                            case 19:
                                {
                                int LA14_26 = input.LA(6);

                                if ( (LA14_26==44) ) {
                                    int LA14_4 = input.LA(7);

                                    if ( (LA14_4==24||(LA14_4 >= 26 && LA14_4 <= 27)||LA14_4==43||LA14_4==45) ) {
                                        alt14=2;
                                    }
                                    else if ( (LA14_4==29) ) {
                                        alt14=3;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 4, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 26, input);

                                    throw nvae;

                                }
                                }
                                break;
                            case 18:
                                {
                                int LA14_27 = input.LA(6);

                                if ( (LA14_27==44) ) {
                                    int LA14_4 = input.LA(7);

                                    if ( (LA14_4==24||(LA14_4 >= 26 && LA14_4 <= 27)||LA14_4==43||LA14_4==45) ) {
                                        alt14=2;
                                    }
                                    else if ( (LA14_4==29) ) {
                                        alt14=3;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 4, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 27, input);

                                    throw nvae;

                                }
                                }
                                break;
                            case 22:
                                {
                                int LA14_28 = input.LA(6);

                                if ( (LA14_28==44) ) {
                                    int LA14_4 = input.LA(7);

                                    if ( (LA14_4==24||(LA14_4 >= 26 && LA14_4 <= 27)||LA14_4==43||LA14_4==45) ) {
                                        alt14=2;
                                    }
                                    else if ( (LA14_4==29) ) {
                                        alt14=3;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 4, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 28, input);

                                    throw nvae;

                                }
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 14, 16, input);

                                throw nvae;

                            }

                        }
                        else {
                            if (state.backtracking>0) {state.failed=true; return element;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 14, 12, input);

                            throw nvae;

                        }
                        }
                        break;
                    case 40:
                        {
                        int LA14_13 = input.LA(4);

                        if ( (LA14_13==17) ) {
                            int LA14_17 = input.LA(5);

                            if ( (LA14_17==IDENTIFIER) ) {
                                int LA14_29 = input.LA(6);

                                if ( (LA14_29==44) ) {
                                    int LA14_4 = input.LA(7);

                                    if ( (LA14_4==24||(LA14_4 >= 26 && LA14_4 <= 27)||LA14_4==43||LA14_4==45) ) {
                                        alt14=2;
                                    }
                                    else if ( (LA14_4==29) ) {
                                        alt14=3;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 4, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 29, input);

                                    throw nvae;

                                }
                            }
                            else {
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 14, 17, input);

                                throw nvae;

                            }
                        }
                        else {
                            if (state.backtracking>0) {state.failed=true; return element;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 14, 13, input);

                            throw nvae;

                        }
                        }
                        break;
                    case 39:
                        {
                        int LA14_14 = input.LA(4);

                        if ( (LA14_14==17) ) {
                            int LA14_18 = input.LA(5);

                            if ( (LA14_18==IDENTIFIER) ) {
                                int LA14_30 = input.LA(6);

                                if ( (LA14_30==44) ) {
                                    int LA14_4 = input.LA(7);

                                    if ( (LA14_4==24||(LA14_4 >= 26 && LA14_4 <= 27)||LA14_4==43||LA14_4==45) ) {
                                        alt14=2;
                                    }
                                    else if ( (LA14_4==29) ) {
                                        alt14=3;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 4, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 30, input);

                                    throw nvae;

                                }
                            }
                            else {
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 14, 18, input);

                                throw nvae;

                            }
                        }
                        else {
                            if (state.backtracking>0) {state.failed=true; return element;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 14, 14, input);

                            throw nvae;

                        }
                        }
                        break;
                    default:
                        if (state.backtracking>0) {state.failed=true; return element;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 14, 5, input);

                        throw nvae;

                    }

                    }
                    break;
                case 41:
                    {
                    switch ( input.LA(3) ) {
                    case 12:
                        {
                        switch ( input.LA(4) ) {
                        case INTEGER:
                            {
                            switch ( input.LA(5) ) {
                            case 46:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    switch ( input.LA(7) ) {
                                    case 36:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            switch ( input.LA(9) ) {
                                            case 28:
                                                {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA14_33 = input.LA(11);

                                                    if ( (LA14_33==32) ) {
                                                        switch ( input.LA(12) ) {
                                                        case INTEGER:
                                                            {
                                                            int LA14_34 = input.LA(13);

                                                            if ( (LA14_34==34) ) {
                                                                int LA14_23 = input.LA(14);

                                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                                    alt14=2;
                                                                }
                                                                else if ( (LA14_23==29) ) {
                                                                    alt14=3;
                                                                }
                                                                else {
                                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                                    NoViableAltException nvae =
                                                                        new NoViableAltException("", 14, 23, input);

                                                                    throw nvae;

                                                                }
                                                            }
                                                            else {
                                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                                NoViableAltException nvae =
                                                                    new NoViableAltException("", 14, 34, input);

                                                                throw nvae;

                                                            }
                                                            }
                                                            break;
                                                        case 24:
                                                        case 26:
                                                        case 27:
                                                        case 43:
                                                        case 45:
                                                            {
                                                            alt14=2;
                                                            }
                                                            break;
                                                        case 29:
                                                            {
                                                            alt14=3;
                                                            }
                                                            break;
                                                        default:
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 14, 22, input);

                                                            throw nvae;

                                                        }

                                                    }
                                                    else if ( (LA14_33==34) ) {
                                                        int LA14_23 = input.LA(12);

                                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                            alt14=2;
                                                        }
                                                        else if ( (LA14_23==29) ) {
                                                            alt14=3;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 14, 23, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 33, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt14=2;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt14=3;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 21, input);

                                                    throw nvae;

                                                }

                                                }
                                                break;
                                            case 32:
                                                {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA14_34 = input.LA(11);

                                                    if ( (LA14_34==34) ) {
                                                        int LA14_23 = input.LA(12);

                                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                            alt14=2;
                                                        }
                                                        else if ( (LA14_23==29) ) {
                                                            alt14=3;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 14, 23, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 34, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt14=2;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt14=3;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 22, input);

                                                    throw nvae;

                                                }

                                                }
                                                break;
                                            case 34:
                                                {
                                                int LA14_23 = input.LA(10);

                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                    alt14=2;
                                                }
                                                else if ( (LA14_23==29) ) {
                                                    alt14=3;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 23, input);

                                                    throw nvae;

                                                }
                                                }
                                                break;
                                            default:
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 32, input);

                                                throw nvae;

                                            }

                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt14=2;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt14=3;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 20, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 28:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA14_33 = input.LA(9);

                                            if ( (LA14_33==32) ) {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA14_34 = input.LA(11);

                                                    if ( (LA14_34==34) ) {
                                                        int LA14_23 = input.LA(12);

                                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                            alt14=2;
                                                        }
                                                        else if ( (LA14_23==29) ) {
                                                            alt14=3;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 14, 23, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 34, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt14=2;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt14=3;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 22, input);

                                                    throw nvae;

                                                }

                                            }
                                            else if ( (LA14_33==34) ) {
                                                int LA14_23 = input.LA(10);

                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                    alt14=2;
                                                }
                                                else if ( (LA14_23==29) ) {
                                                    alt14=3;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 23, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 33, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt14=2;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt14=3;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 21, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 32:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA14_34 = input.LA(9);

                                            if ( (LA14_34==34) ) {
                                                int LA14_23 = input.LA(10);

                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                    alt14=2;
                                                }
                                                else if ( (LA14_23==29) ) {
                                                    alt14=3;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 23, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 34, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt14=2;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt14=3;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 22, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 34:
                                        {
                                        int LA14_23 = input.LA(8);

                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                            alt14=2;
                                        }
                                        else if ( (LA14_23==29) ) {
                                            alt14=3;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 23, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 31, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt14=2;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt14=3;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 19, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 36:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    switch ( input.LA(7) ) {
                                    case 28:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA14_33 = input.LA(9);

                                            if ( (LA14_33==32) ) {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA14_34 = input.LA(11);

                                                    if ( (LA14_34==34) ) {
                                                        int LA14_23 = input.LA(12);

                                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                            alt14=2;
                                                        }
                                                        else if ( (LA14_23==29) ) {
                                                            alt14=3;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 14, 23, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 34, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt14=2;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt14=3;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 22, input);

                                                    throw nvae;

                                                }

                                            }
                                            else if ( (LA14_33==34) ) {
                                                int LA14_23 = input.LA(10);

                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                    alt14=2;
                                                }
                                                else if ( (LA14_23==29) ) {
                                                    alt14=3;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 23, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 33, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt14=2;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt14=3;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 21, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 32:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA14_34 = input.LA(9);

                                            if ( (LA14_34==34) ) {
                                                int LA14_23 = input.LA(10);

                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                    alt14=2;
                                                }
                                                else if ( (LA14_23==29) ) {
                                                    alt14=3;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 23, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 34, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt14=2;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt14=3;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 22, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 34:
                                        {
                                        int LA14_23 = input.LA(8);

                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                            alt14=2;
                                        }
                                        else if ( (LA14_23==29) ) {
                                            alt14=3;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 23, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 32, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt14=2;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt14=3;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 20, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 28:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    int LA14_33 = input.LA(7);

                                    if ( (LA14_33==32) ) {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA14_34 = input.LA(9);

                                            if ( (LA14_34==34) ) {
                                                int LA14_23 = input.LA(10);

                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                    alt14=2;
                                                }
                                                else if ( (LA14_23==29) ) {
                                                    alt14=3;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 23, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 34, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt14=2;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt14=3;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 22, input);

                                            throw nvae;

                                        }

                                    }
                                    else if ( (LA14_33==34) ) {
                                        int LA14_23 = input.LA(8);

                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                            alt14=2;
                                        }
                                        else if ( (LA14_23==29) ) {
                                            alt14=3;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 23, input);

                                            throw nvae;

                                        }
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 33, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt14=2;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt14=3;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 21, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 32:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    int LA14_34 = input.LA(7);

                                    if ( (LA14_34==34) ) {
                                        int LA14_23 = input.LA(8);

                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                            alt14=2;
                                        }
                                        else if ( (LA14_23==29) ) {
                                            alt14=3;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 23, input);

                                            throw nvae;

                                        }
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 34, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt14=2;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt14=3;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 22, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 34:
                                {
                                int LA14_23 = input.LA(6);

                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                    alt14=2;
                                }
                                else if ( (LA14_23==29) ) {
                                    alt14=3;
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 23, input);

                                    throw nvae;

                                }
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 14, 15, input);

                                throw nvae;

                            }

                            }
                            break;
                        case 24:
                        case 26:
                        case 27:
                        case 43:
                        case 45:
                            {
                            alt14=2;
                            }
                            break;
                        case 29:
                            {
                            alt14=3;
                            }
                            break;
                        default:
                            if (state.backtracking>0) {state.failed=true; return element;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 14, 8, input);

                            throw nvae;

                        }

                        }
                        break;
                    case 14:
                        {
                        switch ( input.LA(4) ) {
                        case INTEGER:
                            {
                            switch ( input.LA(5) ) {
                            case 46:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    switch ( input.LA(7) ) {
                                    case 36:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            switch ( input.LA(9) ) {
                                            case 28:
                                                {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA14_33 = input.LA(11);

                                                    if ( (LA14_33==32) ) {
                                                        switch ( input.LA(12) ) {
                                                        case INTEGER:
                                                            {
                                                            int LA14_34 = input.LA(13);

                                                            if ( (LA14_34==34) ) {
                                                                int LA14_23 = input.LA(14);

                                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                                    alt14=2;
                                                                }
                                                                else if ( (LA14_23==29) ) {
                                                                    alt14=3;
                                                                }
                                                                else {
                                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                                    NoViableAltException nvae =
                                                                        new NoViableAltException("", 14, 23, input);

                                                                    throw nvae;

                                                                }
                                                            }
                                                            else {
                                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                                NoViableAltException nvae =
                                                                    new NoViableAltException("", 14, 34, input);

                                                                throw nvae;

                                                            }
                                                            }
                                                            break;
                                                        case 24:
                                                        case 26:
                                                        case 27:
                                                        case 43:
                                                        case 45:
                                                            {
                                                            alt14=2;
                                                            }
                                                            break;
                                                        case 29:
                                                            {
                                                            alt14=3;
                                                            }
                                                            break;
                                                        default:
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 14, 22, input);

                                                            throw nvae;

                                                        }

                                                    }
                                                    else if ( (LA14_33==34) ) {
                                                        int LA14_23 = input.LA(12);

                                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                            alt14=2;
                                                        }
                                                        else if ( (LA14_23==29) ) {
                                                            alt14=3;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 14, 23, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 33, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt14=2;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt14=3;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 21, input);

                                                    throw nvae;

                                                }

                                                }
                                                break;
                                            case 32:
                                                {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA14_34 = input.LA(11);

                                                    if ( (LA14_34==34) ) {
                                                        int LA14_23 = input.LA(12);

                                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                            alt14=2;
                                                        }
                                                        else if ( (LA14_23==29) ) {
                                                            alt14=3;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 14, 23, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 34, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt14=2;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt14=3;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 22, input);

                                                    throw nvae;

                                                }

                                                }
                                                break;
                                            case 34:
                                                {
                                                int LA14_23 = input.LA(10);

                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                    alt14=2;
                                                }
                                                else if ( (LA14_23==29) ) {
                                                    alt14=3;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 23, input);

                                                    throw nvae;

                                                }
                                                }
                                                break;
                                            default:
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 32, input);

                                                throw nvae;

                                            }

                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt14=2;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt14=3;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 20, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 28:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA14_33 = input.LA(9);

                                            if ( (LA14_33==32) ) {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA14_34 = input.LA(11);

                                                    if ( (LA14_34==34) ) {
                                                        int LA14_23 = input.LA(12);

                                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                            alt14=2;
                                                        }
                                                        else if ( (LA14_23==29) ) {
                                                            alt14=3;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 14, 23, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 34, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt14=2;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt14=3;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 22, input);

                                                    throw nvae;

                                                }

                                            }
                                            else if ( (LA14_33==34) ) {
                                                int LA14_23 = input.LA(10);

                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                    alt14=2;
                                                }
                                                else if ( (LA14_23==29) ) {
                                                    alt14=3;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 23, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 33, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt14=2;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt14=3;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 21, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 32:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA14_34 = input.LA(9);

                                            if ( (LA14_34==34) ) {
                                                int LA14_23 = input.LA(10);

                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                    alt14=2;
                                                }
                                                else if ( (LA14_23==29) ) {
                                                    alt14=3;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 23, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 34, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt14=2;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt14=3;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 22, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 34:
                                        {
                                        int LA14_23 = input.LA(8);

                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                            alt14=2;
                                        }
                                        else if ( (LA14_23==29) ) {
                                            alt14=3;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 23, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 31, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt14=2;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt14=3;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 19, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 36:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    switch ( input.LA(7) ) {
                                    case 28:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA14_33 = input.LA(9);

                                            if ( (LA14_33==32) ) {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA14_34 = input.LA(11);

                                                    if ( (LA14_34==34) ) {
                                                        int LA14_23 = input.LA(12);

                                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                            alt14=2;
                                                        }
                                                        else if ( (LA14_23==29) ) {
                                                            alt14=3;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 14, 23, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 34, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt14=2;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt14=3;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 22, input);

                                                    throw nvae;

                                                }

                                            }
                                            else if ( (LA14_33==34) ) {
                                                int LA14_23 = input.LA(10);

                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                    alt14=2;
                                                }
                                                else if ( (LA14_23==29) ) {
                                                    alt14=3;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 23, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 33, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt14=2;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt14=3;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 21, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 32:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA14_34 = input.LA(9);

                                            if ( (LA14_34==34) ) {
                                                int LA14_23 = input.LA(10);

                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                    alt14=2;
                                                }
                                                else if ( (LA14_23==29) ) {
                                                    alt14=3;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 23, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 34, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt14=2;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt14=3;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 22, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 34:
                                        {
                                        int LA14_23 = input.LA(8);

                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                            alt14=2;
                                        }
                                        else if ( (LA14_23==29) ) {
                                            alt14=3;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 23, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 32, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt14=2;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt14=3;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 20, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 28:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    int LA14_33 = input.LA(7);

                                    if ( (LA14_33==32) ) {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA14_34 = input.LA(9);

                                            if ( (LA14_34==34) ) {
                                                int LA14_23 = input.LA(10);

                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                    alt14=2;
                                                }
                                                else if ( (LA14_23==29) ) {
                                                    alt14=3;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 23, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 34, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt14=2;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt14=3;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 22, input);

                                            throw nvae;

                                        }

                                    }
                                    else if ( (LA14_33==34) ) {
                                        int LA14_23 = input.LA(8);

                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                            alt14=2;
                                        }
                                        else if ( (LA14_23==29) ) {
                                            alt14=3;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 23, input);

                                            throw nvae;

                                        }
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 33, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt14=2;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt14=3;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 21, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 32:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    int LA14_34 = input.LA(7);

                                    if ( (LA14_34==34) ) {
                                        int LA14_23 = input.LA(8);

                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                            alt14=2;
                                        }
                                        else if ( (LA14_23==29) ) {
                                            alt14=3;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 23, input);

                                            throw nvae;

                                        }
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 34, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt14=2;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt14=3;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 22, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 34:
                                {
                                int LA14_23 = input.LA(6);

                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                    alt14=2;
                                }
                                else if ( (LA14_23==29) ) {
                                    alt14=3;
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 23, input);

                                    throw nvae;

                                }
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 14, 15, input);

                                throw nvae;

                            }

                            }
                            break;
                        case 24:
                        case 26:
                        case 27:
                        case 43:
                        case 45:
                            {
                            alt14=2;
                            }
                            break;
                        case 29:
                            {
                            alt14=3;
                            }
                            break;
                        default:
                            if (state.backtracking>0) {state.failed=true; return element;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 14, 9, input);

                            throw nvae;

                        }

                        }
                        break;
                    case 24:
                    case 26:
                    case 27:
                    case 43:
                    case 45:
                        {
                        alt14=2;
                        }
                        break;
                    case 29:
                        {
                        alt14=3;
                        }
                        break;
                    default:
                        if (state.backtracking>0) {state.failed=true; return element;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 14, 6, input);

                        throw nvae;

                    }

                    }
                    break;
                case 30:
                    {
                    switch ( input.LA(3) ) {
                    case 12:
                        {
                        switch ( input.LA(4) ) {
                        case INTEGER:
                            {
                            switch ( input.LA(5) ) {
                            case 46:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    switch ( input.LA(7) ) {
                                    case 36:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            switch ( input.LA(9) ) {
                                            case 28:
                                                {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA14_33 = input.LA(11);

                                                    if ( (LA14_33==32) ) {
                                                        switch ( input.LA(12) ) {
                                                        case INTEGER:
                                                            {
                                                            int LA14_34 = input.LA(13);

                                                            if ( (LA14_34==34) ) {
                                                                int LA14_23 = input.LA(14);

                                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                                    alt14=2;
                                                                }
                                                                else if ( (LA14_23==29) ) {
                                                                    alt14=3;
                                                                }
                                                                else {
                                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                                    NoViableAltException nvae =
                                                                        new NoViableAltException("", 14, 23, input);

                                                                    throw nvae;

                                                                }
                                                            }
                                                            else {
                                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                                NoViableAltException nvae =
                                                                    new NoViableAltException("", 14, 34, input);

                                                                throw nvae;

                                                            }
                                                            }
                                                            break;
                                                        case 24:
                                                        case 26:
                                                        case 27:
                                                        case 43:
                                                        case 45:
                                                            {
                                                            alt14=2;
                                                            }
                                                            break;
                                                        case 29:
                                                            {
                                                            alt14=3;
                                                            }
                                                            break;
                                                        default:
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 14, 22, input);

                                                            throw nvae;

                                                        }

                                                    }
                                                    else if ( (LA14_33==34) ) {
                                                        int LA14_23 = input.LA(12);

                                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                            alt14=2;
                                                        }
                                                        else if ( (LA14_23==29) ) {
                                                            alt14=3;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 14, 23, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 33, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt14=2;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt14=3;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 21, input);

                                                    throw nvae;

                                                }

                                                }
                                                break;
                                            case 32:
                                                {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA14_34 = input.LA(11);

                                                    if ( (LA14_34==34) ) {
                                                        int LA14_23 = input.LA(12);

                                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                            alt14=2;
                                                        }
                                                        else if ( (LA14_23==29) ) {
                                                            alt14=3;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 14, 23, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 34, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt14=2;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt14=3;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 22, input);

                                                    throw nvae;

                                                }

                                                }
                                                break;
                                            case 34:
                                                {
                                                int LA14_23 = input.LA(10);

                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                    alt14=2;
                                                }
                                                else if ( (LA14_23==29) ) {
                                                    alt14=3;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 23, input);

                                                    throw nvae;

                                                }
                                                }
                                                break;
                                            default:
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 32, input);

                                                throw nvae;

                                            }

                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt14=2;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt14=3;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 20, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 28:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA14_33 = input.LA(9);

                                            if ( (LA14_33==32) ) {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA14_34 = input.LA(11);

                                                    if ( (LA14_34==34) ) {
                                                        int LA14_23 = input.LA(12);

                                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                            alt14=2;
                                                        }
                                                        else if ( (LA14_23==29) ) {
                                                            alt14=3;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 14, 23, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 34, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt14=2;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt14=3;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 22, input);

                                                    throw nvae;

                                                }

                                            }
                                            else if ( (LA14_33==34) ) {
                                                int LA14_23 = input.LA(10);

                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                    alt14=2;
                                                }
                                                else if ( (LA14_23==29) ) {
                                                    alt14=3;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 23, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 33, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt14=2;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt14=3;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 21, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 32:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA14_34 = input.LA(9);

                                            if ( (LA14_34==34) ) {
                                                int LA14_23 = input.LA(10);

                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                    alt14=2;
                                                }
                                                else if ( (LA14_23==29) ) {
                                                    alt14=3;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 23, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 34, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt14=2;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt14=3;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 22, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 34:
                                        {
                                        int LA14_23 = input.LA(8);

                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                            alt14=2;
                                        }
                                        else if ( (LA14_23==29) ) {
                                            alt14=3;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 23, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 31, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt14=2;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt14=3;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 19, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 36:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    switch ( input.LA(7) ) {
                                    case 28:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA14_33 = input.LA(9);

                                            if ( (LA14_33==32) ) {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA14_34 = input.LA(11);

                                                    if ( (LA14_34==34) ) {
                                                        int LA14_23 = input.LA(12);

                                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                            alt14=2;
                                                        }
                                                        else if ( (LA14_23==29) ) {
                                                            alt14=3;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 14, 23, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 34, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt14=2;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt14=3;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 22, input);

                                                    throw nvae;

                                                }

                                            }
                                            else if ( (LA14_33==34) ) {
                                                int LA14_23 = input.LA(10);

                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                    alt14=2;
                                                }
                                                else if ( (LA14_23==29) ) {
                                                    alt14=3;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 23, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 33, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt14=2;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt14=3;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 21, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 32:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA14_34 = input.LA(9);

                                            if ( (LA14_34==34) ) {
                                                int LA14_23 = input.LA(10);

                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                    alt14=2;
                                                }
                                                else if ( (LA14_23==29) ) {
                                                    alt14=3;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 23, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 34, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt14=2;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt14=3;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 22, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 34:
                                        {
                                        int LA14_23 = input.LA(8);

                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                            alt14=2;
                                        }
                                        else if ( (LA14_23==29) ) {
                                            alt14=3;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 23, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 32, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt14=2;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt14=3;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 20, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 28:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    int LA14_33 = input.LA(7);

                                    if ( (LA14_33==32) ) {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA14_34 = input.LA(9);

                                            if ( (LA14_34==34) ) {
                                                int LA14_23 = input.LA(10);

                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                    alt14=2;
                                                }
                                                else if ( (LA14_23==29) ) {
                                                    alt14=3;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 23, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 34, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt14=2;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt14=3;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 22, input);

                                            throw nvae;

                                        }

                                    }
                                    else if ( (LA14_33==34) ) {
                                        int LA14_23 = input.LA(8);

                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                            alt14=2;
                                        }
                                        else if ( (LA14_23==29) ) {
                                            alt14=3;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 23, input);

                                            throw nvae;

                                        }
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 33, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt14=2;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt14=3;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 21, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 32:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    int LA14_34 = input.LA(7);

                                    if ( (LA14_34==34) ) {
                                        int LA14_23 = input.LA(8);

                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                            alt14=2;
                                        }
                                        else if ( (LA14_23==29) ) {
                                            alt14=3;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 23, input);

                                            throw nvae;

                                        }
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 34, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt14=2;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt14=3;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 22, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 34:
                                {
                                int LA14_23 = input.LA(6);

                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                    alt14=2;
                                }
                                else if ( (LA14_23==29) ) {
                                    alt14=3;
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 23, input);

                                    throw nvae;

                                }
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 14, 15, input);

                                throw nvae;

                            }

                            }
                            break;
                        case 24:
                        case 26:
                        case 27:
                        case 43:
                        case 45:
                            {
                            alt14=2;
                            }
                            break;
                        case 29:
                            {
                            alt14=3;
                            }
                            break;
                        default:
                            if (state.backtracking>0) {state.failed=true; return element;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 14, 8, input);

                            throw nvae;

                        }

                        }
                        break;
                    case 14:
                        {
                        switch ( input.LA(4) ) {
                        case INTEGER:
                            {
                            switch ( input.LA(5) ) {
                            case 46:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    switch ( input.LA(7) ) {
                                    case 36:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            switch ( input.LA(9) ) {
                                            case 28:
                                                {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA14_33 = input.LA(11);

                                                    if ( (LA14_33==32) ) {
                                                        switch ( input.LA(12) ) {
                                                        case INTEGER:
                                                            {
                                                            int LA14_34 = input.LA(13);

                                                            if ( (LA14_34==34) ) {
                                                                int LA14_23 = input.LA(14);

                                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                                    alt14=2;
                                                                }
                                                                else if ( (LA14_23==29) ) {
                                                                    alt14=3;
                                                                }
                                                                else {
                                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                                    NoViableAltException nvae =
                                                                        new NoViableAltException("", 14, 23, input);

                                                                    throw nvae;

                                                                }
                                                            }
                                                            else {
                                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                                NoViableAltException nvae =
                                                                    new NoViableAltException("", 14, 34, input);

                                                                throw nvae;

                                                            }
                                                            }
                                                            break;
                                                        case 24:
                                                        case 26:
                                                        case 27:
                                                        case 43:
                                                        case 45:
                                                            {
                                                            alt14=2;
                                                            }
                                                            break;
                                                        case 29:
                                                            {
                                                            alt14=3;
                                                            }
                                                            break;
                                                        default:
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 14, 22, input);

                                                            throw nvae;

                                                        }

                                                    }
                                                    else if ( (LA14_33==34) ) {
                                                        int LA14_23 = input.LA(12);

                                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                            alt14=2;
                                                        }
                                                        else if ( (LA14_23==29) ) {
                                                            alt14=3;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 14, 23, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 33, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt14=2;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt14=3;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 21, input);

                                                    throw nvae;

                                                }

                                                }
                                                break;
                                            case 32:
                                                {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA14_34 = input.LA(11);

                                                    if ( (LA14_34==34) ) {
                                                        int LA14_23 = input.LA(12);

                                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                            alt14=2;
                                                        }
                                                        else if ( (LA14_23==29) ) {
                                                            alt14=3;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 14, 23, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 34, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt14=2;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt14=3;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 22, input);

                                                    throw nvae;

                                                }

                                                }
                                                break;
                                            case 34:
                                                {
                                                int LA14_23 = input.LA(10);

                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                    alt14=2;
                                                }
                                                else if ( (LA14_23==29) ) {
                                                    alt14=3;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 23, input);

                                                    throw nvae;

                                                }
                                                }
                                                break;
                                            default:
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 32, input);

                                                throw nvae;

                                            }

                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt14=2;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt14=3;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 20, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 28:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA14_33 = input.LA(9);

                                            if ( (LA14_33==32) ) {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA14_34 = input.LA(11);

                                                    if ( (LA14_34==34) ) {
                                                        int LA14_23 = input.LA(12);

                                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                            alt14=2;
                                                        }
                                                        else if ( (LA14_23==29) ) {
                                                            alt14=3;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 14, 23, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 34, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt14=2;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt14=3;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 22, input);

                                                    throw nvae;

                                                }

                                            }
                                            else if ( (LA14_33==34) ) {
                                                int LA14_23 = input.LA(10);

                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                    alt14=2;
                                                }
                                                else if ( (LA14_23==29) ) {
                                                    alt14=3;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 23, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 33, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt14=2;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt14=3;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 21, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 32:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA14_34 = input.LA(9);

                                            if ( (LA14_34==34) ) {
                                                int LA14_23 = input.LA(10);

                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                    alt14=2;
                                                }
                                                else if ( (LA14_23==29) ) {
                                                    alt14=3;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 23, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 34, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt14=2;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt14=3;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 22, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 34:
                                        {
                                        int LA14_23 = input.LA(8);

                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                            alt14=2;
                                        }
                                        else if ( (LA14_23==29) ) {
                                            alt14=3;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 23, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 31, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt14=2;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt14=3;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 19, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 36:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    switch ( input.LA(7) ) {
                                    case 28:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA14_33 = input.LA(9);

                                            if ( (LA14_33==32) ) {
                                                switch ( input.LA(10) ) {
                                                case INTEGER:
                                                    {
                                                    int LA14_34 = input.LA(11);

                                                    if ( (LA14_34==34) ) {
                                                        int LA14_23 = input.LA(12);

                                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                            alt14=2;
                                                        }
                                                        else if ( (LA14_23==29) ) {
                                                            alt14=3;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 14, 23, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 34, input);

                                                        throw nvae;

                                                    }
                                                    }
                                                    break;
                                                case 24:
                                                case 26:
                                                case 27:
                                                case 43:
                                                case 45:
                                                    {
                                                    alt14=2;
                                                    }
                                                    break;
                                                case 29:
                                                    {
                                                    alt14=3;
                                                    }
                                                    break;
                                                default:
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 22, input);

                                                    throw nvae;

                                                }

                                            }
                                            else if ( (LA14_33==34) ) {
                                                int LA14_23 = input.LA(10);

                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                    alt14=2;
                                                }
                                                else if ( (LA14_23==29) ) {
                                                    alt14=3;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 23, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 33, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt14=2;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt14=3;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 21, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 32:
                                        {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA14_34 = input.LA(9);

                                            if ( (LA14_34==34) ) {
                                                int LA14_23 = input.LA(10);

                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                    alt14=2;
                                                }
                                                else if ( (LA14_23==29) ) {
                                                    alt14=3;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 23, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 34, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt14=2;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt14=3;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 22, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 34:
                                        {
                                        int LA14_23 = input.LA(8);

                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                            alt14=2;
                                        }
                                        else if ( (LA14_23==29) ) {
                                            alt14=3;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 23, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 32, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt14=2;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt14=3;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 20, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 28:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    int LA14_33 = input.LA(7);

                                    if ( (LA14_33==32) ) {
                                        switch ( input.LA(8) ) {
                                        case INTEGER:
                                            {
                                            int LA14_34 = input.LA(9);

                                            if ( (LA14_34==34) ) {
                                                int LA14_23 = input.LA(10);

                                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                    alt14=2;
                                                }
                                                else if ( (LA14_23==29) ) {
                                                    alt14=3;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 23, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 34, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 24:
                                        case 26:
                                        case 27:
                                        case 43:
                                        case 45:
                                            {
                                            alt14=2;
                                            }
                                            break;
                                        case 29:
                                            {
                                            alt14=3;
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 22, input);

                                            throw nvae;

                                        }

                                    }
                                    else if ( (LA14_33==34) ) {
                                        int LA14_23 = input.LA(8);

                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                            alt14=2;
                                        }
                                        else if ( (LA14_23==29) ) {
                                            alt14=3;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 23, input);

                                            throw nvae;

                                        }
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 33, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt14=2;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt14=3;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 21, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 32:
                                {
                                switch ( input.LA(6) ) {
                                case INTEGER:
                                    {
                                    int LA14_34 = input.LA(7);

                                    if ( (LA14_34==34) ) {
                                        int LA14_23 = input.LA(8);

                                        if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                            alt14=2;
                                        }
                                        else if ( (LA14_23==29) ) {
                                            alt14=3;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 23, input);

                                            throw nvae;

                                        }
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 34, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                case 24:
                                case 26:
                                case 27:
                                case 43:
                                case 45:
                                    {
                                    alt14=2;
                                    }
                                    break;
                                case 29:
                                    {
                                    alt14=3;
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 22, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 34:
                                {
                                int LA14_23 = input.LA(6);

                                if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                    alt14=2;
                                }
                                else if ( (LA14_23==29) ) {
                                    alt14=3;
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 23, input);

                                    throw nvae;

                                }
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 14, 15, input);

                                throw nvae;

                            }

                            }
                            break;
                        case 24:
                        case 26:
                        case 27:
                        case 43:
                        case 45:
                            {
                            alt14=2;
                            }
                            break;
                        case 29:
                            {
                            alt14=3;
                            }
                            break;
                        default:
                            if (state.backtracking>0) {state.failed=true; return element;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 14, 9, input);

                            throw nvae;

                        }

                        }
                        break;
                    case 24:
                    case 26:
                    case 27:
                    case 43:
                    case 45:
                        {
                        alt14=2;
                        }
                        break;
                    case 29:
                        {
                        alt14=3;
                        }
                        break;
                    default:
                        if (state.backtracking>0) {state.failed=true; return element;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 14, 7, input);

                        throw nvae;

                    }

                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 14, 2, input);

                    throw nvae;

                }

                }
                break;
            case DATE:
                {
                switch ( input.LA(2) ) {
                case 12:
                    {
                    switch ( input.LA(3) ) {
                    case INTEGER:
                        {
                        switch ( input.LA(4) ) {
                        case 46:
                            {
                            switch ( input.LA(5) ) {
                            case INTEGER:
                                {
                                switch ( input.LA(6) ) {
                                case 36:
                                    {
                                    switch ( input.LA(7) ) {
                                    case INTEGER:
                                        {
                                        switch ( input.LA(8) ) {
                                        case 28:
                                            {
                                            switch ( input.LA(9) ) {
                                            case INTEGER:
                                                {
                                                int LA14_33 = input.LA(10);

                                                if ( (LA14_33==32) ) {
                                                    switch ( input.LA(11) ) {
                                                    case INTEGER:
                                                        {
                                                        int LA14_34 = input.LA(12);

                                                        if ( (LA14_34==34) ) {
                                                            int LA14_23 = input.LA(13);

                                                            if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                                alt14=2;
                                                            }
                                                            else if ( (LA14_23==29) ) {
                                                                alt14=3;
                                                            }
                                                            else {
                                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                                NoViableAltException nvae =
                                                                    new NoViableAltException("", 14, 23, input);

                                                                throw nvae;

                                                            }
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 14, 34, input);

                                                            throw nvae;

                                                        }
                                                        }
                                                        break;
                                                    case 24:
                                                    case 26:
                                                    case 27:
                                                    case 43:
                                                    case 45:
                                                        {
                                                        alt14=2;
                                                        }
                                                        break;
                                                    case 29:
                                                        {
                                                        alt14=3;
                                                        }
                                                        break;
                                                    default:
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 22, input);

                                                        throw nvae;

                                                    }

                                                }
                                                else if ( (LA14_33==34) ) {
                                                    int LA14_23 = input.LA(11);

                                                    if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                        alt14=2;
                                                    }
                                                    else if ( (LA14_23==29) ) {
                                                        alt14=3;
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 23, input);

                                                        throw nvae;

                                                    }
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 33, input);

                                                    throw nvae;

                                                }
                                                }
                                                break;
                                            case 24:
                                            case 26:
                                            case 27:
                                            case 43:
                                            case 45:
                                                {
                                                alt14=2;
                                                }
                                                break;
                                            case 29:
                                                {
                                                alt14=3;
                                                }
                                                break;
                                            default:
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 21, input);

                                                throw nvae;

                                            }

                                            }
                                            break;
                                        case 32:
                                            {
                                            switch ( input.LA(9) ) {
                                            case INTEGER:
                                                {
                                                int LA14_34 = input.LA(10);

                                                if ( (LA14_34==34) ) {
                                                    int LA14_23 = input.LA(11);

                                                    if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                        alt14=2;
                                                    }
                                                    else if ( (LA14_23==29) ) {
                                                        alt14=3;
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 23, input);

                                                        throw nvae;

                                                    }
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 34, input);

                                                    throw nvae;

                                                }
                                                }
                                                break;
                                            case 24:
                                            case 26:
                                            case 27:
                                            case 43:
                                            case 45:
                                                {
                                                alt14=2;
                                                }
                                                break;
                                            case 29:
                                                {
                                                alt14=3;
                                                }
                                                break;
                                            default:
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 22, input);

                                                throw nvae;

                                            }

                                            }
                                            break;
                                        case 34:
                                            {
                                            int LA14_23 = input.LA(9);

                                            if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                alt14=2;
                                            }
                                            else if ( (LA14_23==29) ) {
                                                alt14=3;
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 23, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 32, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 24:
                                    case 26:
                                    case 27:
                                    case 43:
                                    case 45:
                                        {
                                        alt14=2;
                                        }
                                        break;
                                    case 29:
                                        {
                                        alt14=3;
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 20, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 28:
                                    {
                                    switch ( input.LA(7) ) {
                                    case INTEGER:
                                        {
                                        int LA14_33 = input.LA(8);

                                        if ( (LA14_33==32) ) {
                                            switch ( input.LA(9) ) {
                                            case INTEGER:
                                                {
                                                int LA14_34 = input.LA(10);

                                                if ( (LA14_34==34) ) {
                                                    int LA14_23 = input.LA(11);

                                                    if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                        alt14=2;
                                                    }
                                                    else if ( (LA14_23==29) ) {
                                                        alt14=3;
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 23, input);

                                                        throw nvae;

                                                    }
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 34, input);

                                                    throw nvae;

                                                }
                                                }
                                                break;
                                            case 24:
                                            case 26:
                                            case 27:
                                            case 43:
                                            case 45:
                                                {
                                                alt14=2;
                                                }
                                                break;
                                            case 29:
                                                {
                                                alt14=3;
                                                }
                                                break;
                                            default:
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 22, input);

                                                throw nvae;

                                            }

                                        }
                                        else if ( (LA14_33==34) ) {
                                            int LA14_23 = input.LA(9);

                                            if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                alt14=2;
                                            }
                                            else if ( (LA14_23==29) ) {
                                                alt14=3;
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 23, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 33, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    case 24:
                                    case 26:
                                    case 27:
                                    case 43:
                                    case 45:
                                        {
                                        alt14=2;
                                        }
                                        break;
                                    case 29:
                                        {
                                        alt14=3;
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 21, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 32:
                                    {
                                    switch ( input.LA(7) ) {
                                    case INTEGER:
                                        {
                                        int LA14_34 = input.LA(8);

                                        if ( (LA14_34==34) ) {
                                            int LA14_23 = input.LA(9);

                                            if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                alt14=2;
                                            }
                                            else if ( (LA14_23==29) ) {
                                                alt14=3;
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 23, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 34, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    case 24:
                                    case 26:
                                    case 27:
                                    case 43:
                                    case 45:
                                        {
                                        alt14=2;
                                        }
                                        break;
                                    case 29:
                                        {
                                        alt14=3;
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 22, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 34:
                                    {
                                    int LA14_23 = input.LA(7);

                                    if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                        alt14=2;
                                    }
                                    else if ( (LA14_23==29) ) {
                                        alt14=3;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 23, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 31, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 24:
                            case 26:
                            case 27:
                            case 43:
                            case 45:
                                {
                                alt14=2;
                                }
                                break;
                            case 29:
                                {
                                alt14=3;
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 14, 19, input);

                                throw nvae;

                            }

                            }
                            break;
                        case 36:
                            {
                            switch ( input.LA(5) ) {
                            case INTEGER:
                                {
                                switch ( input.LA(6) ) {
                                case 28:
                                    {
                                    switch ( input.LA(7) ) {
                                    case INTEGER:
                                        {
                                        int LA14_33 = input.LA(8);

                                        if ( (LA14_33==32) ) {
                                            switch ( input.LA(9) ) {
                                            case INTEGER:
                                                {
                                                int LA14_34 = input.LA(10);

                                                if ( (LA14_34==34) ) {
                                                    int LA14_23 = input.LA(11);

                                                    if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                        alt14=2;
                                                    }
                                                    else if ( (LA14_23==29) ) {
                                                        alt14=3;
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 23, input);

                                                        throw nvae;

                                                    }
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 34, input);

                                                    throw nvae;

                                                }
                                                }
                                                break;
                                            case 24:
                                            case 26:
                                            case 27:
                                            case 43:
                                            case 45:
                                                {
                                                alt14=2;
                                                }
                                                break;
                                            case 29:
                                                {
                                                alt14=3;
                                                }
                                                break;
                                            default:
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 22, input);

                                                throw nvae;

                                            }

                                        }
                                        else if ( (LA14_33==34) ) {
                                            int LA14_23 = input.LA(9);

                                            if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                alt14=2;
                                            }
                                            else if ( (LA14_23==29) ) {
                                                alt14=3;
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 23, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 33, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    case 24:
                                    case 26:
                                    case 27:
                                    case 43:
                                    case 45:
                                        {
                                        alt14=2;
                                        }
                                        break;
                                    case 29:
                                        {
                                        alt14=3;
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 21, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 32:
                                    {
                                    switch ( input.LA(7) ) {
                                    case INTEGER:
                                        {
                                        int LA14_34 = input.LA(8);

                                        if ( (LA14_34==34) ) {
                                            int LA14_23 = input.LA(9);

                                            if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                alt14=2;
                                            }
                                            else if ( (LA14_23==29) ) {
                                                alt14=3;
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 23, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 34, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    case 24:
                                    case 26:
                                    case 27:
                                    case 43:
                                    case 45:
                                        {
                                        alt14=2;
                                        }
                                        break;
                                    case 29:
                                        {
                                        alt14=3;
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 22, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 34:
                                    {
                                    int LA14_23 = input.LA(7);

                                    if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                        alt14=2;
                                    }
                                    else if ( (LA14_23==29) ) {
                                        alt14=3;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 23, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 32, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 24:
                            case 26:
                            case 27:
                            case 43:
                            case 45:
                                {
                                alt14=2;
                                }
                                break;
                            case 29:
                                {
                                alt14=3;
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 14, 20, input);

                                throw nvae;

                            }

                            }
                            break;
                        case 28:
                            {
                            switch ( input.LA(5) ) {
                            case INTEGER:
                                {
                                int LA14_33 = input.LA(6);

                                if ( (LA14_33==32) ) {
                                    switch ( input.LA(7) ) {
                                    case INTEGER:
                                        {
                                        int LA14_34 = input.LA(8);

                                        if ( (LA14_34==34) ) {
                                            int LA14_23 = input.LA(9);

                                            if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                alt14=2;
                                            }
                                            else if ( (LA14_23==29) ) {
                                                alt14=3;
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 23, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 34, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    case 24:
                                    case 26:
                                    case 27:
                                    case 43:
                                    case 45:
                                        {
                                        alt14=2;
                                        }
                                        break;
                                    case 29:
                                        {
                                        alt14=3;
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 22, input);

                                        throw nvae;

                                    }

                                }
                                else if ( (LA14_33==34) ) {
                                    int LA14_23 = input.LA(7);

                                    if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                        alt14=2;
                                    }
                                    else if ( (LA14_23==29) ) {
                                        alt14=3;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 23, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 33, input);

                                    throw nvae;

                                }
                                }
                                break;
                            case 24:
                            case 26:
                            case 27:
                            case 43:
                            case 45:
                                {
                                alt14=2;
                                }
                                break;
                            case 29:
                                {
                                alt14=3;
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 14, 21, input);

                                throw nvae;

                            }

                            }
                            break;
                        case 32:
                            {
                            switch ( input.LA(5) ) {
                            case INTEGER:
                                {
                                int LA14_34 = input.LA(6);

                                if ( (LA14_34==34) ) {
                                    int LA14_23 = input.LA(7);

                                    if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                        alt14=2;
                                    }
                                    else if ( (LA14_23==29) ) {
                                        alt14=3;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 23, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 34, input);

                                    throw nvae;

                                }
                                }
                                break;
                            case 24:
                            case 26:
                            case 27:
                            case 43:
                            case 45:
                                {
                                alt14=2;
                                }
                                break;
                            case 29:
                                {
                                alt14=3;
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 14, 22, input);

                                throw nvae;

                            }

                            }
                            break;
                        case 34:
                            {
                            int LA14_23 = input.LA(5);

                            if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                alt14=2;
                            }
                            else if ( (LA14_23==29) ) {
                                alt14=3;
                            }
                            else {
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 14, 23, input);

                                throw nvae;

                            }
                            }
                            break;
                        default:
                            if (state.backtracking>0) {state.failed=true; return element;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 14, 15, input);

                            throw nvae;

                        }

                        }
                        break;
                    case 24:
                    case 26:
                    case 27:
                    case 43:
                    case 45:
                        {
                        alt14=2;
                        }
                        break;
                    case 29:
                        {
                        alt14=3;
                        }
                        break;
                    default:
                        if (state.backtracking>0) {state.failed=true; return element;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 14, 8, input);

                        throw nvae;

                    }

                    }
                    break;
                case 14:
                    {
                    switch ( input.LA(3) ) {
                    case INTEGER:
                        {
                        switch ( input.LA(4) ) {
                        case 46:
                            {
                            switch ( input.LA(5) ) {
                            case INTEGER:
                                {
                                switch ( input.LA(6) ) {
                                case 36:
                                    {
                                    switch ( input.LA(7) ) {
                                    case INTEGER:
                                        {
                                        switch ( input.LA(8) ) {
                                        case 28:
                                            {
                                            switch ( input.LA(9) ) {
                                            case INTEGER:
                                                {
                                                int LA14_33 = input.LA(10);

                                                if ( (LA14_33==32) ) {
                                                    switch ( input.LA(11) ) {
                                                    case INTEGER:
                                                        {
                                                        int LA14_34 = input.LA(12);

                                                        if ( (LA14_34==34) ) {
                                                            int LA14_23 = input.LA(13);

                                                            if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                                alt14=2;
                                                            }
                                                            else if ( (LA14_23==29) ) {
                                                                alt14=3;
                                                            }
                                                            else {
                                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                                NoViableAltException nvae =
                                                                    new NoViableAltException("", 14, 23, input);

                                                                throw nvae;

                                                            }
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 14, 34, input);

                                                            throw nvae;

                                                        }
                                                        }
                                                        break;
                                                    case 24:
                                                    case 26:
                                                    case 27:
                                                    case 43:
                                                    case 45:
                                                        {
                                                        alt14=2;
                                                        }
                                                        break;
                                                    case 29:
                                                        {
                                                        alt14=3;
                                                        }
                                                        break;
                                                    default:
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 22, input);

                                                        throw nvae;

                                                    }

                                                }
                                                else if ( (LA14_33==34) ) {
                                                    int LA14_23 = input.LA(11);

                                                    if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                        alt14=2;
                                                    }
                                                    else if ( (LA14_23==29) ) {
                                                        alt14=3;
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 23, input);

                                                        throw nvae;

                                                    }
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 33, input);

                                                    throw nvae;

                                                }
                                                }
                                                break;
                                            case 24:
                                            case 26:
                                            case 27:
                                            case 43:
                                            case 45:
                                                {
                                                alt14=2;
                                                }
                                                break;
                                            case 29:
                                                {
                                                alt14=3;
                                                }
                                                break;
                                            default:
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 21, input);

                                                throw nvae;

                                            }

                                            }
                                            break;
                                        case 32:
                                            {
                                            switch ( input.LA(9) ) {
                                            case INTEGER:
                                                {
                                                int LA14_34 = input.LA(10);

                                                if ( (LA14_34==34) ) {
                                                    int LA14_23 = input.LA(11);

                                                    if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                        alt14=2;
                                                    }
                                                    else if ( (LA14_23==29) ) {
                                                        alt14=3;
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 23, input);

                                                        throw nvae;

                                                    }
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 34, input);

                                                    throw nvae;

                                                }
                                                }
                                                break;
                                            case 24:
                                            case 26:
                                            case 27:
                                            case 43:
                                            case 45:
                                                {
                                                alt14=2;
                                                }
                                                break;
                                            case 29:
                                                {
                                                alt14=3;
                                                }
                                                break;
                                            default:
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 22, input);

                                                throw nvae;

                                            }

                                            }
                                            break;
                                        case 34:
                                            {
                                            int LA14_23 = input.LA(9);

                                            if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                alt14=2;
                                            }
                                            else if ( (LA14_23==29) ) {
                                                alt14=3;
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 23, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        default:
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 32, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    case 24:
                                    case 26:
                                    case 27:
                                    case 43:
                                    case 45:
                                        {
                                        alt14=2;
                                        }
                                        break;
                                    case 29:
                                        {
                                        alt14=3;
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 20, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 28:
                                    {
                                    switch ( input.LA(7) ) {
                                    case INTEGER:
                                        {
                                        int LA14_33 = input.LA(8);

                                        if ( (LA14_33==32) ) {
                                            switch ( input.LA(9) ) {
                                            case INTEGER:
                                                {
                                                int LA14_34 = input.LA(10);

                                                if ( (LA14_34==34) ) {
                                                    int LA14_23 = input.LA(11);

                                                    if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                        alt14=2;
                                                    }
                                                    else if ( (LA14_23==29) ) {
                                                        alt14=3;
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 23, input);

                                                        throw nvae;

                                                    }
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 34, input);

                                                    throw nvae;

                                                }
                                                }
                                                break;
                                            case 24:
                                            case 26:
                                            case 27:
                                            case 43:
                                            case 45:
                                                {
                                                alt14=2;
                                                }
                                                break;
                                            case 29:
                                                {
                                                alt14=3;
                                                }
                                                break;
                                            default:
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 22, input);

                                                throw nvae;

                                            }

                                        }
                                        else if ( (LA14_33==34) ) {
                                            int LA14_23 = input.LA(9);

                                            if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                alt14=2;
                                            }
                                            else if ( (LA14_23==29) ) {
                                                alt14=3;
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 23, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 33, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    case 24:
                                    case 26:
                                    case 27:
                                    case 43:
                                    case 45:
                                        {
                                        alt14=2;
                                        }
                                        break;
                                    case 29:
                                        {
                                        alt14=3;
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 21, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 32:
                                    {
                                    switch ( input.LA(7) ) {
                                    case INTEGER:
                                        {
                                        int LA14_34 = input.LA(8);

                                        if ( (LA14_34==34) ) {
                                            int LA14_23 = input.LA(9);

                                            if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                alt14=2;
                                            }
                                            else if ( (LA14_23==29) ) {
                                                alt14=3;
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 23, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 34, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    case 24:
                                    case 26:
                                    case 27:
                                    case 43:
                                    case 45:
                                        {
                                        alt14=2;
                                        }
                                        break;
                                    case 29:
                                        {
                                        alt14=3;
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 22, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 34:
                                    {
                                    int LA14_23 = input.LA(7);

                                    if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                        alt14=2;
                                    }
                                    else if ( (LA14_23==29) ) {
                                        alt14=3;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 23, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 31, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 24:
                            case 26:
                            case 27:
                            case 43:
                            case 45:
                                {
                                alt14=2;
                                }
                                break;
                            case 29:
                                {
                                alt14=3;
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 14, 19, input);

                                throw nvae;

                            }

                            }
                            break;
                        case 36:
                            {
                            switch ( input.LA(5) ) {
                            case INTEGER:
                                {
                                switch ( input.LA(6) ) {
                                case 28:
                                    {
                                    switch ( input.LA(7) ) {
                                    case INTEGER:
                                        {
                                        int LA14_33 = input.LA(8);

                                        if ( (LA14_33==32) ) {
                                            switch ( input.LA(9) ) {
                                            case INTEGER:
                                                {
                                                int LA14_34 = input.LA(10);

                                                if ( (LA14_34==34) ) {
                                                    int LA14_23 = input.LA(11);

                                                    if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                        alt14=2;
                                                    }
                                                    else if ( (LA14_23==29) ) {
                                                        alt14=3;
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 14, 23, input);

                                                        throw nvae;

                                                    }
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 14, 34, input);

                                                    throw nvae;

                                                }
                                                }
                                                break;
                                            case 24:
                                            case 26:
                                            case 27:
                                            case 43:
                                            case 45:
                                                {
                                                alt14=2;
                                                }
                                                break;
                                            case 29:
                                                {
                                                alt14=3;
                                                }
                                                break;
                                            default:
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 22, input);

                                                throw nvae;

                                            }

                                        }
                                        else if ( (LA14_33==34) ) {
                                            int LA14_23 = input.LA(9);

                                            if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                alt14=2;
                                            }
                                            else if ( (LA14_23==29) ) {
                                                alt14=3;
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 23, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 33, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    case 24:
                                    case 26:
                                    case 27:
                                    case 43:
                                    case 45:
                                        {
                                        alt14=2;
                                        }
                                        break;
                                    case 29:
                                        {
                                        alt14=3;
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 21, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 32:
                                    {
                                    switch ( input.LA(7) ) {
                                    case INTEGER:
                                        {
                                        int LA14_34 = input.LA(8);

                                        if ( (LA14_34==34) ) {
                                            int LA14_23 = input.LA(9);

                                            if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                alt14=2;
                                            }
                                            else if ( (LA14_23==29) ) {
                                                alt14=3;
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 23, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 34, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    case 24:
                                    case 26:
                                    case 27:
                                    case 43:
                                    case 45:
                                        {
                                        alt14=2;
                                        }
                                        break;
                                    case 29:
                                        {
                                        alt14=3;
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 22, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 34:
                                    {
                                    int LA14_23 = input.LA(7);

                                    if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                        alt14=2;
                                    }
                                    else if ( (LA14_23==29) ) {
                                        alt14=3;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 23, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                default:
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 32, input);

                                    throw nvae;

                                }

                                }
                                break;
                            case 24:
                            case 26:
                            case 27:
                            case 43:
                            case 45:
                                {
                                alt14=2;
                                }
                                break;
                            case 29:
                                {
                                alt14=3;
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 14, 20, input);

                                throw nvae;

                            }

                            }
                            break;
                        case 28:
                            {
                            switch ( input.LA(5) ) {
                            case INTEGER:
                                {
                                int LA14_33 = input.LA(6);

                                if ( (LA14_33==32) ) {
                                    switch ( input.LA(7) ) {
                                    case INTEGER:
                                        {
                                        int LA14_34 = input.LA(8);

                                        if ( (LA14_34==34) ) {
                                            int LA14_23 = input.LA(9);

                                            if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                                alt14=2;
                                            }
                                            else if ( (LA14_23==29) ) {
                                                alt14=3;
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 14, 23, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 14, 34, input);

                                            throw nvae;

                                        }
                                        }
                                        break;
                                    case 24:
                                    case 26:
                                    case 27:
                                    case 43:
                                    case 45:
                                        {
                                        alt14=2;
                                        }
                                        break;
                                    case 29:
                                        {
                                        alt14=3;
                                        }
                                        break;
                                    default:
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 22, input);

                                        throw nvae;

                                    }

                                }
                                else if ( (LA14_33==34) ) {
                                    int LA14_23 = input.LA(7);

                                    if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                        alt14=2;
                                    }
                                    else if ( (LA14_23==29) ) {
                                        alt14=3;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 23, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 33, input);

                                    throw nvae;

                                }
                                }
                                break;
                            case 24:
                            case 26:
                            case 27:
                            case 43:
                            case 45:
                                {
                                alt14=2;
                                }
                                break;
                            case 29:
                                {
                                alt14=3;
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 14, 21, input);

                                throw nvae;

                            }

                            }
                            break;
                        case 32:
                            {
                            switch ( input.LA(5) ) {
                            case INTEGER:
                                {
                                int LA14_34 = input.LA(6);

                                if ( (LA14_34==34) ) {
                                    int LA14_23 = input.LA(7);

                                    if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                        alt14=2;
                                    }
                                    else if ( (LA14_23==29) ) {
                                        alt14=3;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 14, 23, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 14, 34, input);

                                    throw nvae;

                                }
                                }
                                break;
                            case 24:
                            case 26:
                            case 27:
                            case 43:
                            case 45:
                                {
                                alt14=2;
                                }
                                break;
                            case 29:
                                {
                                alt14=3;
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 14, 22, input);

                                throw nvae;

                            }

                            }
                            break;
                        case 34:
                            {
                            int LA14_23 = input.LA(5);

                            if ( (LA14_23==24||(LA14_23 >= 26 && LA14_23 <= 27)||LA14_23==43||LA14_23==45) ) {
                                alt14=2;
                            }
                            else if ( (LA14_23==29) ) {
                                alt14=3;
                            }
                            else {
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 14, 23, input);

                                throw nvae;

                            }
                            }
                            break;
                        default:
                            if (state.backtracking>0) {state.failed=true; return element;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 14, 15, input);

                            throw nvae;

                        }

                        }
                        break;
                    case 24:
                    case 26:
                    case 27:
                    case 43:
                    case 45:
                        {
                        alt14=2;
                        }
                        break;
                    case 29:
                        {
                        alt14=3;
                        }
                        break;
                    default:
                        if (state.backtracking>0) {state.failed=true; return element;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 14, 9, input);

                        throw nvae;

                    }

                    }
                    break;
                case 24:
                case 26:
                case 27:
                case 43:
                case 45:
                    {
                    alt14=2;
                    }
                    break;
                case 29:
                    {
                    alt14=3;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 14, 3, input);

                    throw nvae;

                }

                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;

            }

            switch (alt14) {
                case 1 :
                    // Fmec.g:2753:2: c0= parse_de_darwinspl_fmec_constraints_NestedConstraint
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_constraints_NestedConstraint_in_parse_de_darwinspl_fmec_constraints_UnaryConstraintChild2498);
                    c0=parse_de_darwinspl_fmec_constraints_NestedConstraint();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Fmec.g:2754:4: c1= parse_de_darwinspl_fmec_constraints_TimePointConstraint
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_constraints_TimePointConstraint_in_parse_de_darwinspl_fmec_constraints_UnaryConstraintChild2508);
                    c1=parse_de_darwinspl_fmec_constraints_TimePointConstraint();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 3 :
                    // Fmec.g:2755:4: c2= parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint_in_parse_de_darwinspl_fmec_constraints_UnaryConstraintChild2518);
                    c2=parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c2; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 36, parse_de_darwinspl_fmec_constraints_UnaryConstraintChild_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_constraints_UnaryConstraintChild"



    // $ANTLR start "parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock"
    // Fmec.g:2759:1: parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock returns [de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock element = null] : (c0= parse_de_darwinspl_fmec_property_EvolutionaryProperty |c1= parse_de_darwinspl_fmec_timepoint_TimePoint );
    public final de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock() throws RecognitionException {
        de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock element =  null;

        int parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock_StartIndex = input.index();

        de.darwinspl.fmec.property.EvolutionaryProperty c0 =null;

        de.darwinspl.fmec.timepoint.TimePoint c1 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 37) ) { return element; }

            // Fmec.g:2760:2: (c0= parse_de_darwinspl_fmec_property_EvolutionaryProperty |c1= parse_de_darwinspl_fmec_timepoint_TimePoint )
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==IDENTIFIER) ) {
                int LA15_1 = input.LA(2);

                if ( (LA15_1==15||LA15_1==44) ) {
                    alt15=1;
                }
                else if ( (LA15_1==30||LA15_1==41) ) {
                    alt15=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 15, 1, input);

                    throw nvae;

                }
            }
            else if ( (LA15_0==DATE) ) {
                alt15=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;

            }
            switch (alt15) {
                case 1 :
                    // Fmec.g:2761:2: c0= parse_de_darwinspl_fmec_property_EvolutionaryProperty
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_property_EvolutionaryProperty_in_parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock2539);
                    c0=parse_de_darwinspl_fmec_property_EvolutionaryProperty();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Fmec.g:2762:4: c1= parse_de_darwinspl_fmec_timepoint_TimePoint
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_timepoint_TimePoint_in_parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock2549);
                    c1=parse_de_darwinspl_fmec_timepoint_TimePoint();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 37, parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock"



    // $ANTLR start "parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword"
    // Fmec.g:2766:1: parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword returns [de.darwinspl.fmec.keywords.timepoint.TimePointKeyword element = null] : (c0= parse_de_darwinspl_fmec_keywords_timepoint_KeywordBefore |c1= parse_de_darwinspl_fmec_keywords_timepoint_KeywordUntil |c2= parse_de_darwinspl_fmec_keywords_timepoint_KeywordAfter |c3= parse_de_darwinspl_fmec_keywords_timepoint_KeywordWhen |c4= parse_de_darwinspl_fmec_keywords_timepoint_KeywordAt );
    public final de.darwinspl.fmec.keywords.timepoint.TimePointKeyword parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword() throws RecognitionException {
        de.darwinspl.fmec.keywords.timepoint.TimePointKeyword element =  null;

        int parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword_StartIndex = input.index();

        de.darwinspl.fmec.keywords.timepoint.KeywordBefore c0 =null;

        de.darwinspl.fmec.keywords.timepoint.KeywordUntil c1 =null;

        de.darwinspl.fmec.keywords.timepoint.KeywordAfter c2 =null;

        de.darwinspl.fmec.keywords.timepoint.KeywordWhen c3 =null;

        de.darwinspl.fmec.keywords.timepoint.KeywordAt c4 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 38) ) { return element; }

            // Fmec.g:2767:2: (c0= parse_de_darwinspl_fmec_keywords_timepoint_KeywordBefore |c1= parse_de_darwinspl_fmec_keywords_timepoint_KeywordUntil |c2= parse_de_darwinspl_fmec_keywords_timepoint_KeywordAfter |c3= parse_de_darwinspl_fmec_keywords_timepoint_KeywordWhen |c4= parse_de_darwinspl_fmec_keywords_timepoint_KeywordAt )
            int alt16=5;
            switch ( input.LA(1) ) {
            case 27:
                {
                alt16=1;
                }
                break;
            case 43:
                {
                alt16=2;
                }
                break;
            case 24:
                {
                alt16=3;
                }
                break;
            case 45:
                {
                alt16=4;
                }
                break;
            case 26:
                {
                alt16=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;

            }

            switch (alt16) {
                case 1 :
                    // Fmec.g:2768:2: c0= parse_de_darwinspl_fmec_keywords_timepoint_KeywordBefore
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_keywords_timepoint_KeywordBefore_in_parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword2570);
                    c0=parse_de_darwinspl_fmec_keywords_timepoint_KeywordBefore();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Fmec.g:2769:4: c1= parse_de_darwinspl_fmec_keywords_timepoint_KeywordUntil
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_keywords_timepoint_KeywordUntil_in_parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword2580);
                    c1=parse_de_darwinspl_fmec_keywords_timepoint_KeywordUntil();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 3 :
                    // Fmec.g:2770:4: c2= parse_de_darwinspl_fmec_keywords_timepoint_KeywordAfter
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_keywords_timepoint_KeywordAfter_in_parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword2590);
                    c2=parse_de_darwinspl_fmec_keywords_timepoint_KeywordAfter();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c2; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 4 :
                    // Fmec.g:2771:4: c3= parse_de_darwinspl_fmec_keywords_timepoint_KeywordWhen
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_keywords_timepoint_KeywordWhen_in_parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword2600);
                    c3=parse_de_darwinspl_fmec_keywords_timepoint_KeywordWhen();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c3; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 5 :
                    // Fmec.g:2772:4: c4= parse_de_darwinspl_fmec_keywords_timepoint_KeywordAt
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_keywords_timepoint_KeywordAt_in_parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword2610);
                    c4=parse_de_darwinspl_fmec_keywords_timepoint_KeywordAt();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c4; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 38, parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword"



    // $ANTLR start "parse_de_darwinspl_fmec_keywords_interval_IntervalKeyword"
    // Fmec.g:2776:1: parse_de_darwinspl_fmec_keywords_interval_IntervalKeyword returns [de.darwinspl.fmec.keywords.interval.IntervalKeyword element = null] : c0= parse_de_darwinspl_fmec_keywords_interval_KeywordDuring ;
    public final de.darwinspl.fmec.keywords.interval.IntervalKeyword parse_de_darwinspl_fmec_keywords_interval_IntervalKeyword() throws RecognitionException {
        de.darwinspl.fmec.keywords.interval.IntervalKeyword element =  null;

        int parse_de_darwinspl_fmec_keywords_interval_IntervalKeyword_StartIndex = input.index();

        de.darwinspl.fmec.keywords.interval.KeywordDuring c0 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 39) ) { return element; }

            // Fmec.g:2777:2: (c0= parse_de_darwinspl_fmec_keywords_interval_KeywordDuring )
            // Fmec.g:2778:2: c0= parse_de_darwinspl_fmec_keywords_interval_KeywordDuring
            {
            pushFollow(FOLLOW_parse_de_darwinspl_fmec_keywords_interval_KeywordDuring_in_parse_de_darwinspl_fmec_keywords_interval_IntervalKeyword2631);
            c0=parse_de_darwinspl_fmec_keywords_interval_KeywordDuring();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 39, parse_de_darwinspl_fmec_keywords_interval_IntervalKeyword_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_keywords_interval_IntervalKeyword"



    // $ANTLR start "parse_de_darwinspl_fmec_property_Property"
    // Fmec.g:2782:1: parse_de_darwinspl_fmec_property_Property returns [de.darwinspl.fmec.property.Property element = null] : (c0= parse_de_darwinspl_fmec_property_FeatureExistenceProperty |c1= parse_de_darwinspl_fmec_property_FeatureTypeProperty |c2= parse_de_darwinspl_fmec_property_FeatureParentProperty |c3= parse_de_darwinspl_fmec_property_GroupExistenceProperty |c4= parse_de_darwinspl_fmec_property_GroupTypeProperty |c5= parse_de_darwinspl_fmec_property_GroupParentProperty );
    public final de.darwinspl.fmec.property.Property parse_de_darwinspl_fmec_property_Property() throws RecognitionException {
        de.darwinspl.fmec.property.Property element =  null;

        int parse_de_darwinspl_fmec_property_Property_StartIndex = input.index();

        de.darwinspl.fmec.property.FeatureExistenceProperty c0 =null;

        de.darwinspl.fmec.property.FeatureTypeProperty c1 =null;

        de.darwinspl.fmec.property.FeatureParentProperty c2 =null;

        de.darwinspl.fmec.property.GroupExistenceProperty c3 =null;

        de.darwinspl.fmec.property.GroupTypeProperty c4 =null;

        de.darwinspl.fmec.property.GroupParentProperty c5 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 40) ) { return element; }

            // Fmec.g:2783:2: (c0= parse_de_darwinspl_fmec_property_FeatureExistenceProperty |c1= parse_de_darwinspl_fmec_property_FeatureTypeProperty |c2= parse_de_darwinspl_fmec_property_FeatureParentProperty |c3= parse_de_darwinspl_fmec_property_GroupExistenceProperty |c4= parse_de_darwinspl_fmec_property_GroupTypeProperty |c5= parse_de_darwinspl_fmec_property_GroupParentProperty )
            int alt17=6;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==IDENTIFIER) ) {
                int LA17_1 = input.LA(2);

                if ( (LA17_1==15) ) {
                    switch ( input.LA(3) ) {
                    case 42:
                        {
                        int LA17_5 = input.LA(4);

                        if ( (LA17_5==17) ) {
                            int LA17_8 = input.LA(5);

                            if ( ((LA17_8 >= 20 && LA17_8 <= 21)) ) {
                                alt17=2;
                            }
                            else if ( ((LA17_8 >= 18 && LA17_8 <= 19)||LA17_8==22) ) {
                                alt17=5;
                            }
                            else {
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 17, 8, input);

                                throw nvae;

                            }
                        }
                        else {
                            if (state.backtracking>0) {state.failed=true; return element;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 17, 5, input);

                            throw nvae;

                        }
                        }
                        break;
                    case 40:
                        {
                        alt17=3;
                        }
                        break;
                    case 39:
                        {
                        alt17=6;
                        }
                        break;
                    default:
                        if (state.backtracking>0) {state.failed=true; return element;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 17, 2, input);

                        throw nvae;

                    }

                }
                else if ( (synpred28_Fmec()) ) {
                    alt17=1;
                }
                else if ( (synpred31_Fmec()) ) {
                    alt17=4;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 17, 1, input);

                    throw nvae;

                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;

            }
            switch (alt17) {
                case 1 :
                    // Fmec.g:2784:2: c0= parse_de_darwinspl_fmec_property_FeatureExistenceProperty
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_property_FeatureExistenceProperty_in_parse_de_darwinspl_fmec_property_Property2652);
                    c0=parse_de_darwinspl_fmec_property_FeatureExistenceProperty();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Fmec.g:2785:4: c1= parse_de_darwinspl_fmec_property_FeatureTypeProperty
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_property_FeatureTypeProperty_in_parse_de_darwinspl_fmec_property_Property2662);
                    c1=parse_de_darwinspl_fmec_property_FeatureTypeProperty();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 3 :
                    // Fmec.g:2786:4: c2= parse_de_darwinspl_fmec_property_FeatureParentProperty
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_property_FeatureParentProperty_in_parse_de_darwinspl_fmec_property_Property2672);
                    c2=parse_de_darwinspl_fmec_property_FeatureParentProperty();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c2; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 4 :
                    // Fmec.g:2787:4: c3= parse_de_darwinspl_fmec_property_GroupExistenceProperty
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_property_GroupExistenceProperty_in_parse_de_darwinspl_fmec_property_Property2682);
                    c3=parse_de_darwinspl_fmec_property_GroupExistenceProperty();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c3; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 5 :
                    // Fmec.g:2788:4: c4= parse_de_darwinspl_fmec_property_GroupTypeProperty
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_property_GroupTypeProperty_in_parse_de_darwinspl_fmec_property_Property2692);
                    c4=parse_de_darwinspl_fmec_property_GroupTypeProperty();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c4; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 6 :
                    // Fmec.g:2789:4: c5= parse_de_darwinspl_fmec_property_GroupParentProperty
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_property_GroupParentProperty_in_parse_de_darwinspl_fmec_property_Property2702);
                    c5=parse_de_darwinspl_fmec_property_GroupParentProperty();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c5; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 40, parse_de_darwinspl_fmec_property_Property_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_property_Property"



    // $ANTLR start "parse_de_darwinspl_fmec_keywords_buildingblocks_PropertyKeyword"
    // Fmec.g:2793:1: parse_de_darwinspl_fmec_keywords_buildingblocks_PropertyKeyword returns [de.darwinspl.fmec.keywords.buildingblocks.PropertyKeyword element = null] : c0= parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordValid ;
    public final de.darwinspl.fmec.keywords.buildingblocks.PropertyKeyword parse_de_darwinspl_fmec_keywords_buildingblocks_PropertyKeyword() throws RecognitionException {
        de.darwinspl.fmec.keywords.buildingblocks.PropertyKeyword element =  null;

        int parse_de_darwinspl_fmec_keywords_buildingblocks_PropertyKeyword_StartIndex = input.index();

        de.darwinspl.fmec.keywords.buildingblocks.KeywordValid c0 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 41) ) { return element; }

            // Fmec.g:2794:2: (c0= parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordValid )
            // Fmec.g:2795:2: c0= parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordValid
            {
            pushFollow(FOLLOW_parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordValid_in_parse_de_darwinspl_fmec_keywords_buildingblocks_PropertyKeyword2723);
            c0=parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordValid();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 41, parse_de_darwinspl_fmec_keywords_buildingblocks_PropertyKeyword_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_keywords_buildingblocks_PropertyKeyword"



    // $ANTLR start "parse_de_darwinspl_fmec_timepoint_TimePointWithoutOffset"
    // Fmec.g:2799:1: parse_de_darwinspl_fmec_timepoint_TimePointWithoutOffset returns [de.darwinspl.fmec.timepoint.TimePointWithoutOffset element = null] : (c0= parse_de_darwinspl_fmec_timepoint_ExplicitTimePoint |c1= parse_de_darwinspl_fmec_timepoint_EvolutionaryEvent );
    public final de.darwinspl.fmec.timepoint.TimePointWithoutOffset parse_de_darwinspl_fmec_timepoint_TimePointWithoutOffset() throws RecognitionException {
        de.darwinspl.fmec.timepoint.TimePointWithoutOffset element =  null;

        int parse_de_darwinspl_fmec_timepoint_TimePointWithoutOffset_StartIndex = input.index();

        de.darwinspl.fmec.timepoint.ExplicitTimePoint c0 =null;

        de.darwinspl.fmec.timepoint.EvolutionaryEvent c1 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 42) ) { return element; }

            // Fmec.g:2800:2: (c0= parse_de_darwinspl_fmec_timepoint_ExplicitTimePoint |c1= parse_de_darwinspl_fmec_timepoint_EvolutionaryEvent )
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==DATE) ) {
                alt18=1;
            }
            else if ( (LA18_0==IDENTIFIER) ) {
                alt18=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;

            }
            switch (alt18) {
                case 1 :
                    // Fmec.g:2801:2: c0= parse_de_darwinspl_fmec_timepoint_ExplicitTimePoint
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_timepoint_ExplicitTimePoint_in_parse_de_darwinspl_fmec_timepoint_TimePointWithoutOffset2744);
                    c0=parse_de_darwinspl_fmec_timepoint_ExplicitTimePoint();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Fmec.g:2802:4: c1= parse_de_darwinspl_fmec_timepoint_EvolutionaryEvent
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_timepoint_EvolutionaryEvent_in_parse_de_darwinspl_fmec_timepoint_TimePointWithoutOffset2754);
                    c1=parse_de_darwinspl_fmec_timepoint_EvolutionaryEvent();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 42, parse_de_darwinspl_fmec_timepoint_TimePointWithoutOffset_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_timepoint_TimePointWithoutOffset"



    // $ANTLR start "parse_de_darwinspl_fmec_keywords_buildingblocks_EventKeyword"
    // Fmec.g:2806:1: parse_de_darwinspl_fmec_keywords_buildingblocks_EventKeyword returns [de.darwinspl.fmec.keywords.buildingblocks.EventKeyword element = null] : (c0= parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStarts |c1= parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStops );
    public final de.darwinspl.fmec.keywords.buildingblocks.EventKeyword parse_de_darwinspl_fmec_keywords_buildingblocks_EventKeyword() throws RecognitionException {
        de.darwinspl.fmec.keywords.buildingblocks.EventKeyword element =  null;

        int parse_de_darwinspl_fmec_keywords_buildingblocks_EventKeyword_StartIndex = input.index();

        de.darwinspl.fmec.keywords.buildingblocks.KeywordStarts c0 =null;

        de.darwinspl.fmec.keywords.buildingblocks.KeywordStops c1 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 43) ) { return element; }

            // Fmec.g:2807:2: (c0= parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStarts |c1= parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStops )
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==41) ) {
                alt19=1;
            }
            else if ( (LA19_0==30) ) {
                alt19=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;

            }
            switch (alt19) {
                case 1 :
                    // Fmec.g:2808:2: c0= parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStarts
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStarts_in_parse_de_darwinspl_fmec_keywords_buildingblocks_EventKeyword2775);
                    c0=parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStarts();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Fmec.g:2809:4: c1= parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStops
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStops_in_parse_de_darwinspl_fmec_keywords_buildingblocks_EventKeyword2785);
                    c1=parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStops();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 43, parse_de_darwinspl_fmec_keywords_buildingblocks_EventKeyword_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_fmec_keywords_buildingblocks_EventKeyword"

    // $ANTLR start synpred12_Fmec
    public final void synpred12_Fmec_fragment() throws RecognitionException {
        de.darwinspl.fmec.constraints.BinaryConstraint c0 =null;


        // Fmec.g:2720:2: (c0= parse_de_darwinspl_fmec_constraints_BinaryConstraint )
        // Fmec.g:2720:2: c0= parse_de_darwinspl_fmec_constraints_BinaryConstraint
        {
        pushFollow(FOLLOW_parse_de_darwinspl_fmec_constraints_BinaryConstraint_in_synpred12_Fmec2324);
        c0=parse_de_darwinspl_fmec_constraints_BinaryConstraint();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred12_Fmec

    // $ANTLR start synpred13_Fmec
    public final void synpred13_Fmec_fragment() throws RecognitionException {
        de.darwinspl.fmec.constraints.UnaryConstraint c1 =null;


        // Fmec.g:2721:4: (c1= parse_de_darwinspl_fmec_constraints_UnaryConstraint )
        // Fmec.g:2721:4: c1= parse_de_darwinspl_fmec_constraints_UnaryConstraint
        {
        pushFollow(FOLLOW_parse_de_darwinspl_fmec_constraints_UnaryConstraint_in_synpred13_Fmec2334);
        c1=parse_de_darwinspl_fmec_constraints_UnaryConstraint();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred13_Fmec

    // $ANTLR start synpred14_Fmec
    public final void synpred14_Fmec_fragment() throws RecognitionException {
        de.darwinspl.fmec.constraints.NestedConstraint c2 =null;


        // Fmec.g:2722:4: (c2= parse_de_darwinspl_fmec_constraints_NestedConstraint )
        // Fmec.g:2722:4: c2= parse_de_darwinspl_fmec_constraints_NestedConstraint
        {
        pushFollow(FOLLOW_parse_de_darwinspl_fmec_constraints_NestedConstraint_in_synpred14_Fmec2344);
        c2=parse_de_darwinspl_fmec_constraints_NestedConstraint();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred14_Fmec

    // $ANTLR start synpred15_Fmec
    public final void synpred15_Fmec_fragment() throws RecognitionException {
        de.darwinspl.fmec.constraints.TimePointConstraint c3 =null;


        // Fmec.g:2723:4: (c3= parse_de_darwinspl_fmec_constraints_TimePointConstraint )
        // Fmec.g:2723:4: c3= parse_de_darwinspl_fmec_constraints_TimePointConstraint
        {
        pushFollow(FOLLOW_parse_de_darwinspl_fmec_constraints_TimePointConstraint_in_synpred15_Fmec2354);
        c3=parse_de_darwinspl_fmec_constraints_TimePointConstraint();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred15_Fmec

    // $ANTLR start synpred28_Fmec
    public final void synpred28_Fmec_fragment() throws RecognitionException {
        de.darwinspl.fmec.property.FeatureExistenceProperty c0 =null;


        // Fmec.g:2784:2: (c0= parse_de_darwinspl_fmec_property_FeatureExistenceProperty )
        // Fmec.g:2784:2: c0= parse_de_darwinspl_fmec_property_FeatureExistenceProperty
        {
        pushFollow(FOLLOW_parse_de_darwinspl_fmec_property_FeatureExistenceProperty_in_synpred28_Fmec2652);
        c0=parse_de_darwinspl_fmec_property_FeatureExistenceProperty();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred28_Fmec

    // $ANTLR start synpred31_Fmec
    public final void synpred31_Fmec_fragment() throws RecognitionException {
        de.darwinspl.fmec.property.GroupExistenceProperty c3 =null;


        // Fmec.g:2787:4: (c3= parse_de_darwinspl_fmec_property_GroupExistenceProperty )
        // Fmec.g:2787:4: c3= parse_de_darwinspl_fmec_property_GroupExistenceProperty
        {
        pushFollow(FOLLOW_parse_de_darwinspl_fmec_property_GroupExistenceProperty_in_synpred31_Fmec2682);
        c3=parse_de_darwinspl_fmec_property_GroupExistenceProperty();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred31_Fmec

    // Delegated rules

    public final boolean synpred12_Fmec() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred12_Fmec_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred13_Fmec() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred13_Fmec_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred31_Fmec() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred31_Fmec_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred28_Fmec() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred28_Fmec_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred15_Fmec() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred15_Fmec_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred14_Fmec() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred14_Fmec_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


 

    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel_in_start82 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_start89 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel115 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_35_in_parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel129 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_PATH_TO_FILE_in_parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel147 = new BitSet(new long[]{0x0000802000000032L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint_in_parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel183 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel211 = new BitSet(new long[]{0x0000802000000032L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_constraints_BinaryConstraintChild_in_parse_de_darwinspl_fmec_constraints_BinaryConstraint263 = new BitSet(new long[]{0x0000004202000000L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_keywords_logical_BinaryLogicalKeyword_in_parse_de_darwinspl_fmec_constraints_BinaryConstraint285 = new BitSet(new long[]{0x0000802000000030L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_constraints_BinaryConstraintChild_in_parse_de_darwinspl_fmec_constraints_BinaryConstraint307 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_keywords_logical_UnaryLogicalKeyword_in_parse_de_darwinspl_fmec_constraints_UnaryConstraint344 = new BitSet(new long[]{0x0000800000000030L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_constraints_UnaryConstraintChild_in_parse_de_darwinspl_fmec_constraints_UnaryConstraint366 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_47_in_parse_de_darwinspl_fmec_constraints_NestedConstraint399 = new BitSet(new long[]{0x0000802000000030L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint_in_parse_de_darwinspl_fmec_constraints_NestedConstraint417 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_48_in_parse_de_darwinspl_fmec_constraints_NestedConstraint435 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock_in_parse_de_darwinspl_fmec_constraints_TimePointConstraint468 = new BitSet(new long[]{0x000028000D000000L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword_in_parse_de_darwinspl_fmec_constraints_TimePointConstraint490 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock_in_parse_de_darwinspl_fmec_constraints_TimePointConstraint512 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock_in_parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint549 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_keywords_interval_IntervalKeyword_in_parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint571 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint589 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_timepoint_TimePoint_in_parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint607 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint625 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_timepoint_TimePoint_in_parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint643 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint661 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_property_Property_in_parse_de_darwinspl_fmec_property_EvolutionaryProperty694 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_keywords_buildingblocks_PropertyKeyword_in_parse_de_darwinspl_fmec_property_EvolutionaryProperty716 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_de_darwinspl_fmec_property_FeatureExistenceProperty753 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_de_darwinspl_fmec_property_FeatureTypeProperty793 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_parse_de_darwinspl_fmec_property_FeatureTypeProperty814 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_42_in_parse_de_darwinspl_fmec_property_FeatureTypeProperty828 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_parse_de_darwinspl_fmec_property_FeatureTypeProperty842 = new BitSet(new long[]{0x0000000000300000L});
    public static final BitSet FOLLOW_21_in_parse_de_darwinspl_fmec_property_FeatureTypeProperty865 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_parse_de_darwinspl_fmec_property_FeatureTypeProperty880 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_de_darwinspl_fmec_property_FeatureParentProperty920 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_parse_de_darwinspl_fmec_property_FeatureParentProperty941 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_40_in_parse_de_darwinspl_fmec_property_FeatureParentProperty955 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_parse_de_darwinspl_fmec_property_FeatureParentProperty969 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_de_darwinspl_fmec_property_FeatureParentProperty987 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_de_darwinspl_fmec_property_GroupExistenceProperty1027 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_de_darwinspl_fmec_property_GroupTypeProperty1067 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_parse_de_darwinspl_fmec_property_GroupTypeProperty1088 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_42_in_parse_de_darwinspl_fmec_property_GroupTypeProperty1102 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_parse_de_darwinspl_fmec_property_GroupTypeProperty1116 = new BitSet(new long[]{0x00000000004C0000L});
    public static final BitSet FOLLOW_19_in_parse_de_darwinspl_fmec_property_GroupTypeProperty1139 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_parse_de_darwinspl_fmec_property_GroupTypeProperty1154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_parse_de_darwinspl_fmec_property_GroupTypeProperty1169 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_de_darwinspl_fmec_property_GroupParentProperty1209 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_parse_de_darwinspl_fmec_property_GroupParentProperty1230 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_39_in_parse_de_darwinspl_fmec_property_GroupParentProperty1244 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_parse_de_darwinspl_fmec_property_GroupParentProperty1258 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_de_darwinspl_fmec_property_GroupParentProperty1276 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_timepoint_TimePointWithoutOffset_in_parse_de_darwinspl_fmec_timepoint_TimePoint1316 = new BitSet(new long[]{0x0000000000005002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_timepoint_TimeOffset_in_parse_de_darwinspl_fmec_timepoint_TimePoint1343 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_parse_de_darwinspl_fmec_timepoint_TimeOffset1393 = new BitSet(new long[]{0x0000000000000042L});
    public static final BitSet FOLLOW_14_in_parse_de_darwinspl_fmec_timepoint_TimeOffset1408 = new BitSet(new long[]{0x0000000000000042L});
    public static final BitSet FOLLOW_INTEGER_in_parse_de_darwinspl_fmec_timepoint_TimeOffset1444 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_46_in_parse_de_darwinspl_fmec_timepoint_TimeOffset1477 = new BitSet(new long[]{0x0000000000000042L});
    public static final BitSet FOLLOW_INTEGER_in_parse_de_darwinspl_fmec_timepoint_TimeOffset1525 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_36_in_parse_de_darwinspl_fmec_timepoint_TimeOffset1558 = new BitSet(new long[]{0x0000000000000042L});
    public static final BitSet FOLLOW_INTEGER_in_parse_de_darwinspl_fmec_timepoint_TimeOffset1606 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_28_in_parse_de_darwinspl_fmec_timepoint_TimeOffset1639 = new BitSet(new long[]{0x0000000000000042L});
    public static final BitSet FOLLOW_INTEGER_in_parse_de_darwinspl_fmec_timepoint_TimeOffset1687 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_32_in_parse_de_darwinspl_fmec_timepoint_TimeOffset1720 = new BitSet(new long[]{0x0000000000000042L});
    public static final BitSet FOLLOW_INTEGER_in_parse_de_darwinspl_fmec_timepoint_TimeOffset1768 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_34_in_parse_de_darwinspl_fmec_timepoint_TimeOffset1801 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DATE_in_parse_de_darwinspl_fmec_timepoint_ExplicitTimePoint1853 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_de_darwinspl_fmec_timepoint_EvolutionaryEvent1893 = new BitSet(new long[]{0x0000020040000000L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_keywords_buildingblocks_EventKeyword_in_parse_de_darwinspl_fmec_timepoint_EvolutionaryEvent1918 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_parse_de_darwinspl_fmec_keywords_logical_KeywordAnd1951 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_parse_de_darwinspl_fmec_keywords_logical_KeywordOr1980 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_parse_de_darwinspl_fmec_keywords_logical_KeywordImplies2009 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_parse_de_darwinspl_fmec_keywords_logical_KeywordNot2038 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStarts2067 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStops2096 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_44_in_parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordValid2125 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_parse_de_darwinspl_fmec_keywords_timepoint_KeywordBefore2154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_43_in_parse_de_darwinspl_fmec_keywords_timepoint_KeywordUntil2183 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_parse_de_darwinspl_fmec_keywords_timepoint_KeywordAfter2212 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_45_in_parse_de_darwinspl_fmec_keywords_timepoint_KeywordWhen2241 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_parse_de_darwinspl_fmec_keywords_timepoint_KeywordAt2270 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_parse_de_darwinspl_fmec_keywords_interval_KeywordDuring2299 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_constraints_BinaryConstraint_in_parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint2324 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_constraints_UnaryConstraint_in_parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint2334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_constraints_NestedConstraint_in_parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint2344 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_constraints_TimePointConstraint_in_parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint2354 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint_in_parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint2364 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_constraints_UnaryConstraint_in_parse_de_darwinspl_fmec_constraints_BinaryConstraintChild2385 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_constraints_NestedConstraint_in_parse_de_darwinspl_fmec_constraints_BinaryConstraintChild2395 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_constraints_TimePointConstraint_in_parse_de_darwinspl_fmec_constraints_BinaryConstraintChild2405 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint_in_parse_de_darwinspl_fmec_constraints_BinaryConstraintChild2415 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_keywords_logical_KeywordAnd_in_parse_de_darwinspl_fmec_keywords_logical_BinaryLogicalKeyword2436 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_keywords_logical_KeywordOr_in_parse_de_darwinspl_fmec_keywords_logical_BinaryLogicalKeyword2446 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_keywords_logical_KeywordImplies_in_parse_de_darwinspl_fmec_keywords_logical_BinaryLogicalKeyword2456 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_keywords_logical_KeywordNot_in_parse_de_darwinspl_fmec_keywords_logical_UnaryLogicalKeyword2477 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_constraints_NestedConstraint_in_parse_de_darwinspl_fmec_constraints_UnaryConstraintChild2498 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_constraints_TimePointConstraint_in_parse_de_darwinspl_fmec_constraints_UnaryConstraintChild2508 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint_in_parse_de_darwinspl_fmec_constraints_UnaryConstraintChild2518 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_property_EvolutionaryProperty_in_parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock2539 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_timepoint_TimePoint_in_parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock2549 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_keywords_timepoint_KeywordBefore_in_parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword2570 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_keywords_timepoint_KeywordUntil_in_parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword2580 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_keywords_timepoint_KeywordAfter_in_parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword2590 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_keywords_timepoint_KeywordWhen_in_parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword2600 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_keywords_timepoint_KeywordAt_in_parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword2610 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_keywords_interval_KeywordDuring_in_parse_de_darwinspl_fmec_keywords_interval_IntervalKeyword2631 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_property_FeatureExistenceProperty_in_parse_de_darwinspl_fmec_property_Property2652 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_property_FeatureTypeProperty_in_parse_de_darwinspl_fmec_property_Property2662 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_property_FeatureParentProperty_in_parse_de_darwinspl_fmec_property_Property2672 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_property_GroupExistenceProperty_in_parse_de_darwinspl_fmec_property_Property2682 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_property_GroupTypeProperty_in_parse_de_darwinspl_fmec_property_Property2692 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_property_GroupParentProperty_in_parse_de_darwinspl_fmec_property_Property2702 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordValid_in_parse_de_darwinspl_fmec_keywords_buildingblocks_PropertyKeyword2723 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_timepoint_ExplicitTimePoint_in_parse_de_darwinspl_fmec_timepoint_TimePointWithoutOffset2744 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_timepoint_EvolutionaryEvent_in_parse_de_darwinspl_fmec_timepoint_TimePointWithoutOffset2754 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStarts_in_parse_de_darwinspl_fmec_keywords_buildingblocks_EventKeyword2775 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStops_in_parse_de_darwinspl_fmec_keywords_buildingblocks_EventKeyword2785 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_constraints_BinaryConstraint_in_synpred12_Fmec2324 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_constraints_UnaryConstraint_in_synpred13_Fmec2334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_constraints_NestedConstraint_in_synpred14_Fmec2344 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_constraints_TimePointConstraint_in_synpred15_Fmec2354 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_property_FeatureExistenceProperty_in_synpred28_Fmec2652 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_fmec_property_GroupExistenceProperty_in_synpred31_Fmec2682 = new BitSet(new long[]{0x0000000000000002L});

}