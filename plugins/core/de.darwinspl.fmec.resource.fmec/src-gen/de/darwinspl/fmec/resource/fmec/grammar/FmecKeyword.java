/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.grammar;


/**
 * A class to represent a keyword in the grammar.
 */
public class FmecKeyword extends de.darwinspl.fmec.resource.fmec.grammar.FmecSyntaxElement {
	
	private final String value;
	
	public FmecKeyword(String value, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality cardinality) {
		super(cardinality, null);
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	public String toString() {
		return "\"" + value + "\"";
	}
	
}
