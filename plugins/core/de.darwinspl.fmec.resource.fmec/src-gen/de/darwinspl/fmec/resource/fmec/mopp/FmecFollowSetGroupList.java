/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.mopp;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used when computing code completion proposals hold groups of
 * expected elements which belong to the same follow set.
 */
public class FmecFollowSetGroupList {
	
	private int lastFollowSetID = -1;
	private List<de.darwinspl.fmec.resource.fmec.mopp.FmecFollowSetGroup> followSetGroups = new ArrayList<de.darwinspl.fmec.resource.fmec.mopp.FmecFollowSetGroup>();
	
	public FmecFollowSetGroupList(List<de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal> expectedTerminals) {
		for (de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal expectedTerminal : expectedTerminals) {
			addExpectedTerminal(expectedTerminal);
		}
	}
	
	private void addExpectedTerminal(de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal expectedTerminal) {
		de.darwinspl.fmec.resource.fmec.mopp.FmecFollowSetGroup group;
		
		int followSetID = expectedTerminal.getFollowSetID();
		if (followSetID == lastFollowSetID) {
			if (followSetGroups.isEmpty()) {
				group = addNewGroup();
			} else {
				group = followSetGroups.get(followSetGroups.size() - 1);
			}
		} else {
			group = addNewGroup();
			lastFollowSetID = followSetID;
		}
		
		group.add(expectedTerminal);
	}
	
	public List<de.darwinspl.fmec.resource.fmec.mopp.FmecFollowSetGroup> getFollowSetGroups() {
		return followSetGroups;
	}
	
	private de.darwinspl.fmec.resource.fmec.mopp.FmecFollowSetGroup addNewGroup() {
		de.darwinspl.fmec.resource.fmec.mopp.FmecFollowSetGroup group = new de.darwinspl.fmec.resource.fmec.mopp.FmecFollowSetGroup();
		followSetGroups.add(group);
		return group;
	}
	
}
