grammar Fmec;

options {
	superClass = FmecANTLRParserBase;
	backtrack = true;
	memoize = true;
}

@lexer::header {
	package de.darwinspl.fmec.resource.fmec.mopp;
	
	import java.util.ArrayList;
import java.util.List;
import org.antlr.runtime3_4_0.ANTLRStringStream;
import org.antlr.runtime3_4_0.RecognitionException;
}

@lexer::members {
	public List<RecognitionException> lexerExceptions  = new ArrayList<RecognitionException>();
	public List<Integer> lexerExceptionPositions = new ArrayList<Integer>();
	
	public void reportError(RecognitionException e) {
		lexerExceptions.add(e);
		lexerExceptionPositions.add(((ANTLRStringStream) input).index());
	}
}
@header{
	package de.darwinspl.fmec.resource.fmec.mopp;
	
	import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.antlr.runtime3_4_0.ANTLRInputStream;
import org.antlr.runtime3_4_0.BitSet;
import org.antlr.runtime3_4_0.CommonToken;
import org.antlr.runtime3_4_0.CommonTokenStream;
import org.antlr.runtime3_4_0.IntStream;
import org.antlr.runtime3_4_0.Lexer;
import org.antlr.runtime3_4_0.RecognitionException;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
}

@members{
	private de.darwinspl.fmec.resource.fmec.IFmecTokenResolverFactory tokenResolverFactory = new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenResolverFactory();
	
	/**
	 * the index of the last token that was handled by collectHiddenTokens()
	 */
	private int lastPosition;
	
	/**
	 * A flag that indicates whether the parser should remember all expected elements.
	 * This flag is set to true when using the parse for code completion. Otherwise it
	 * is set to false.
	 */
	private boolean rememberExpectedElements = false;
	
	private Object parseToIndexTypeObject;
	private int lastTokenIndex = 0;
	
	/**
	 * A list of expected elements the were collected while parsing the input stream.
	 * This list is only filled if <code>rememberExpectedElements</code> is set to
	 * true.
	 */
	private List<de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal> expectedElements = new ArrayList<de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal>();
	
	private int mismatchedTokenRecoveryTries = 0;
	/**
	 * A helper list to allow a lexer to pass errors to its parser
	 */
	protected List<RecognitionException> lexerExceptions = Collections.synchronizedList(new ArrayList<RecognitionException>());
	
	/**
	 * Another helper list to allow a lexer to pass positions of errors to its parser
	 */
	protected List<Integer> lexerExceptionPositions = Collections.synchronizedList(new ArrayList<Integer>());
	
	/**
	 * A stack for incomplete objects. This stack is used filled when the parser is
	 * used for code completion. Whenever the parser starts to read an object it is
	 * pushed on the stack. Once the element was parser completely it is popped from
	 * the stack.
	 */
	List<EObject> incompleteObjects = new ArrayList<EObject>();
	
	private int stopIncludingHiddenTokens;
	private int stopExcludingHiddenTokens;
	private int tokenIndexOfLastCompleteElement;
	
	private int expectedElementsIndexOfLastCompleteElement;
	
	/**
	 * The offset indicating the cursor position when the parser is used for code
	 * completion by calling parseToExpectedElements().
	 */
	private int cursorOffset;
	
	/**
	 * The offset of the first hidden token of the last expected element. This offset
	 * is used to discard expected elements, which are not needed for code completion.
	 */
	private int lastStartIncludingHidden;
	
	private de.darwinspl.fmec.resource.fmec.IFmecLocationMap locationMap;
	
	private de.darwinspl.fmec.resource.fmec.mopp.FmecSyntaxErrorMessageConverter syntaxErrorMessageConverter = new de.darwinspl.fmec.resource.fmec.mopp.FmecSyntaxErrorMessageConverter(tokenNames);
	
	@Override
	public void reportError(RecognitionException re) {
		addErrorToResource(syntaxErrorMessageConverter.translateParseError(re));
	}
	
	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
		postParseCommands.add(new de.darwinspl.fmec.resource.fmec.IFmecCommand<de.darwinspl.fmec.resource.fmec.IFmecTextResource>() {
			public boolean execute(de.darwinspl.fmec.resource.fmec.IFmecTextResource resource) {
				if (resource == null) {
					// the resource can be null if the parser is used for code completion
					return true;
				}
				resource.addProblem(new de.darwinspl.fmec.resource.fmec.IFmecProblem() {
					public de.darwinspl.fmec.resource.fmec.FmecEProblemSeverity getSeverity() {
						return de.darwinspl.fmec.resource.fmec.FmecEProblemSeverity.ERROR;
					}
					public de.darwinspl.fmec.resource.fmec.FmecEProblemType getType() {
						return de.darwinspl.fmec.resource.fmec.FmecEProblemType.SYNTAX_ERROR;
					}
					public String getMessage() {
						return errorMessage;
					}
					public Collection<de.darwinspl.fmec.resource.fmec.IFmecQuickFix> getQuickFixes() {
						return null;
					}
				}, column, line, startIndex, stopIndex);
				return true;
			}
		});
	}
	
	protected void addErrorToResource(de.darwinspl.fmec.resource.fmec.mopp.FmecLocalizedMessage message) {
		if (message == null) {
			return;
		}
		addErrorToResource(message.getMessage(), message.getColumn(), message.getLine(), message.getCharStart(), message.getCharEnd());
	}
	
	public void addExpectedElement(EClass eClass, int expectationStartIndex, int expectationEndIndex) {
		for (int expectationIndex = expectationStartIndex; expectationIndex <= expectationEndIndex; expectationIndex++) {
			addExpectedElement(eClass, de.darwinspl.fmec.resource.fmec.mopp.FmecExpectationConstants.EXPECTATIONS[expectationIndex]);
		}
	}
	
	public void addExpectedElement(EClass eClass, int expectationIndex) {
		addExpectedElement(eClass, de.darwinspl.fmec.resource.fmec.mopp.FmecExpectationConstants.EXPECTATIONS[expectationIndex]);
	}
	
	public void addExpectedElement(EClass eClass, int[] ids) {
		if (!this.rememberExpectedElements) {
			return;
		}
		int terminalID = ids[0];
		int followSetID = ids[1];
		de.darwinspl.fmec.resource.fmec.IFmecExpectedElement terminal = de.darwinspl.fmec.resource.fmec.grammar.FmecFollowSetProvider.TERMINALS[terminalID];
		de.darwinspl.fmec.resource.fmec.mopp.FmecContainedFeature[] containmentFeatures = new de.darwinspl.fmec.resource.fmec.mopp.FmecContainedFeature[ids.length - 2];
		for (int i = 2; i < ids.length; i++) {
			containmentFeatures[i - 2] = de.darwinspl.fmec.resource.fmec.grammar.FmecFollowSetProvider.LINKS[ids[i]];
		}
		de.darwinspl.fmec.resource.fmec.grammar.FmecContainmentTrace containmentTrace = new de.darwinspl.fmec.resource.fmec.grammar.FmecContainmentTrace(eClass, containmentFeatures);
		EObject container = getLastIncompleteElement();
		de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal expectedElement = new de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal(container, terminal, followSetID, containmentTrace);
		setPosition(expectedElement, input.index());
		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
		lastStartIncludingHidden = startIncludingHiddenTokens;
		this.expectedElements.add(expectedElement);
	}
	
	protected void collectHiddenTokens(EObject element) {
	}
	
	protected void copyLocalizationInfos(final EObject source, final EObject target) {
		if (disableLocationMap) {
			return;
		}
		final de.darwinspl.fmec.resource.fmec.IFmecLocationMap locationMap = this.locationMap;
		if (locationMap == null) {
			// the locationMap can be null if the parser is used for code completion
			return;
		}
		postParseCommands.add(new de.darwinspl.fmec.resource.fmec.IFmecCommand<de.darwinspl.fmec.resource.fmec.IFmecTextResource>() {
			public boolean execute(de.darwinspl.fmec.resource.fmec.IFmecTextResource resource) {
				locationMap.setCharStart(target, locationMap.getCharStart(source));
				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
				locationMap.setColumn(target, locationMap.getColumn(source));
				locationMap.setLine(target, locationMap.getLine(source));
				return true;
			}
		});
	}
	
	protected void copyLocalizationInfos(final CommonToken source, final EObject target) {
		if (disableLocationMap) {
			return;
		}
		final de.darwinspl.fmec.resource.fmec.IFmecLocationMap locationMap = this.locationMap;
		if (locationMap == null) {
			// the locationMap can be null if the parser is used for code completion
			return;
		}
		postParseCommands.add(new de.darwinspl.fmec.resource.fmec.IFmecCommand<de.darwinspl.fmec.resource.fmec.IFmecTextResource>() {
			public boolean execute(de.darwinspl.fmec.resource.fmec.IFmecTextResource resource) {
				if (source == null) {
					return true;
				}
				locationMap.setCharStart(target, source.getStartIndex());
				locationMap.setCharEnd(target, source.getStopIndex());
				locationMap.setColumn(target, source.getCharPositionInLine());
				locationMap.setLine(target, source.getLine());
				return true;
			}
		});
	}
	
	/**
	 * Sets the end character index and the last line for the given object in the
	 * location map.
	 */
	protected void setLocalizationEnd(Collection<de.darwinspl.fmec.resource.fmec.IFmecCommand<de.darwinspl.fmec.resource.fmec.IFmecTextResource>> postParseCommands , final EObject object, final int endChar, final int endLine) {
		if (disableLocationMap) {
			return;
		}
		final de.darwinspl.fmec.resource.fmec.IFmecLocationMap locationMap = this.locationMap;
		if (locationMap == null) {
			// the locationMap can be null if the parser is used for code completion
			return;
		}
		postParseCommands.add(new de.darwinspl.fmec.resource.fmec.IFmecCommand<de.darwinspl.fmec.resource.fmec.IFmecTextResource>() {
			public boolean execute(de.darwinspl.fmec.resource.fmec.IFmecTextResource resource) {
				locationMap.setCharEnd(object, endChar);
				locationMap.setLine(object, endLine);
				return true;
			}
		});
	}
	
	public de.darwinspl.fmec.resource.fmec.IFmecTextParser createInstance(InputStream actualInputStream, String encoding) {
		try {
			if (encoding == null) {
				return new FmecParser(new CommonTokenStream(new FmecLexer(new ANTLRInputStream(actualInputStream))));
			} else {
				return new FmecParser(new CommonTokenStream(new FmecLexer(new ANTLRInputStream(actualInputStream, encoding))));
			}
		} catch (IOException e) {
			new de.darwinspl.fmec.resource.fmec.util.FmecRuntimeUtil().logError("Error while creating parser.", e);
			return null;
		}
	}
	
	/**
	 * This default constructor is only used to call createInstance() on it.
	 */
	public FmecParser() {
		super(null);
	}
	
	protected EObject doParse() throws RecognitionException {
		this.lastPosition = 0;
		// required because the lexer class can not be subclassed
		((FmecLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
		((FmecLexer) getTokenStream().getTokenSource()).lexerExceptionPositions = lexerExceptionPositions;
		Object typeObject = getTypeObject();
		if (typeObject == null) {
			return start();
		} else if (typeObject instanceof EClass) {
			EClass type = (EClass) typeObject;
			if (type.getInstanceClass() == de.darwinspl.fmec.FeatureModelEvolutionConstraintModel.class) {
				return parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.constraints.BinaryConstraint.class) {
				return parse_de_darwinspl_fmec_constraints_BinaryConstraint();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.constraints.UnaryConstraint.class) {
				return parse_de_darwinspl_fmec_constraints_UnaryConstraint();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.constraints.NestedConstraint.class) {
				return parse_de_darwinspl_fmec_constraints_NestedConstraint();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.constraints.TimePointConstraint.class) {
				return parse_de_darwinspl_fmec_constraints_TimePointConstraint();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.constraints.TimeIntervalConstraint.class) {
				return parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.property.EvolutionaryProperty.class) {
				return parse_de_darwinspl_fmec_property_EvolutionaryProperty();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.property.FeatureExistenceProperty.class) {
				return parse_de_darwinspl_fmec_property_FeatureExistenceProperty();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.property.FeatureTypeProperty.class) {
				return parse_de_darwinspl_fmec_property_FeatureTypeProperty();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.property.FeatureParentProperty.class) {
				return parse_de_darwinspl_fmec_property_FeatureParentProperty();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.property.GroupExistenceProperty.class) {
				return parse_de_darwinspl_fmec_property_GroupExistenceProperty();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.property.GroupTypeProperty.class) {
				return parse_de_darwinspl_fmec_property_GroupTypeProperty();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.property.GroupParentProperty.class) {
				return parse_de_darwinspl_fmec_property_GroupParentProperty();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.timepoint.TimePoint.class) {
				return parse_de_darwinspl_fmec_timepoint_TimePoint();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.timepoint.TimeOffset.class) {
				return parse_de_darwinspl_fmec_timepoint_TimeOffset();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.timepoint.ExplicitTimePoint.class) {
				return parse_de_darwinspl_fmec_timepoint_ExplicitTimePoint();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.timepoint.EvolutionaryEvent.class) {
				return parse_de_darwinspl_fmec_timepoint_EvolutionaryEvent();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.logical.KeywordAnd.class) {
				return parse_de_darwinspl_fmec_keywords_logical_KeywordAnd();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.logical.KeywordOr.class) {
				return parse_de_darwinspl_fmec_keywords_logical_KeywordOr();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.logical.KeywordImplies.class) {
				return parse_de_darwinspl_fmec_keywords_logical_KeywordImplies();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.logical.KeywordNot.class) {
				return parse_de_darwinspl_fmec_keywords_logical_KeywordNot();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.buildingblocks.KeywordStarts.class) {
				return parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStarts();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.buildingblocks.KeywordStops.class) {
				return parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStops();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.buildingblocks.KeywordValid.class) {
				return parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordValid();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.timepoint.KeywordBefore.class) {
				return parse_de_darwinspl_fmec_keywords_timepoint_KeywordBefore();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.timepoint.KeywordUntil.class) {
				return parse_de_darwinspl_fmec_keywords_timepoint_KeywordUntil();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.timepoint.KeywordAfter.class) {
				return parse_de_darwinspl_fmec_keywords_timepoint_KeywordAfter();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.timepoint.KeywordWhen.class) {
				return parse_de_darwinspl_fmec_keywords_timepoint_KeywordWhen();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.timepoint.KeywordAt.class) {
				return parse_de_darwinspl_fmec_keywords_timepoint_KeywordAt();
			}
			if (type.getInstanceClass() == de.darwinspl.fmec.keywords.interval.KeywordDuring.class) {
				return parse_de_darwinspl_fmec_keywords_interval_KeywordDuring();
			}
		}
		throw new de.darwinspl.fmec.resource.fmec.mopp.FmecUnexpectedContentTypeException(typeObject);
	}
	
	public int getMismatchedTokenRecoveryTries() {
		return mismatchedTokenRecoveryTries;
	}
	
	public Object getMissingSymbol(IntStream arg0, RecognitionException arg1, int arg2, BitSet arg3) {
		mismatchedTokenRecoveryTries++;
		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
	}
	
	public Object getParseToIndexTypeObject() {
		return parseToIndexTypeObject;
	}
	
	protected Object getTypeObject() {
		Object typeObject = getParseToIndexTypeObject();
		if (typeObject != null) {
			return typeObject;
		}
		Map<?,?> options = getOptions();
		if (options != null) {
			typeObject = options.get(de.darwinspl.fmec.resource.fmec.IFmecOptions.RESOURCE_CONTENT_TYPE);
		}
		return typeObject;
	}
	
	/**
	 * Implementation that calls {@link #doParse()} and handles the thrown
	 * RecognitionExceptions.
	 */
	public de.darwinspl.fmec.resource.fmec.IFmecParseResult parse() {
		// Reset parser state
		terminateParsing = false;
		postParseCommands = new ArrayList<de.darwinspl.fmec.resource.fmec.IFmecCommand<de.darwinspl.fmec.resource.fmec.IFmecTextResource>>();
		de.darwinspl.fmec.resource.fmec.mopp.FmecParseResult parseResult = new de.darwinspl.fmec.resource.fmec.mopp.FmecParseResult();
		if (disableLocationMap) {
			locationMap = new de.darwinspl.fmec.resource.fmec.mopp.FmecDevNullLocationMap();
		} else {
			locationMap = new de.darwinspl.fmec.resource.fmec.mopp.FmecLocationMap();
		}
		// Run parser
		try {
			EObject result =  doParse();
			if (lexerExceptions.isEmpty()) {
				parseResult.setRoot(result);
				parseResult.setLocationMap(locationMap);
			}
		} catch (RecognitionException re) {
			addErrorToResource(syntaxErrorMessageConverter.translateParseError(re));
		} catch (IllegalArgumentException iae) {
			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
				// can be caused if a null is set on EMF models where not allowed. this will just
				// happen if other errors occurred before
			} else {
				iae.printStackTrace();
			}
		}
		for (RecognitionException re : lexerExceptions) {
			addErrorToResource(syntaxErrorMessageConverter.translateLexicalError(re, lexerExceptions, lexerExceptionPositions));
		}
		parseResult.getPostParseCommands().addAll(postParseCommands);
		return parseResult;
	}
	
	public List<de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal> parseToExpectedElements(EClass type, de.darwinspl.fmec.resource.fmec.IFmecTextResource dummyResource, int cursorOffset) {
		this.rememberExpectedElements = true;
		this.parseToIndexTypeObject = type;
		this.cursorOffset = cursorOffset;
		this.lastStartIncludingHidden = -1;
		final CommonTokenStream tokenStream = (CommonTokenStream) getTokenStream();
		de.darwinspl.fmec.resource.fmec.IFmecParseResult result = parse();
		for (EObject incompleteObject : incompleteObjects) {
			Lexer lexer = (Lexer) tokenStream.getTokenSource();
			int endChar = lexer.getCharIndex();
			int endLine = lexer.getLine();
			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
		}
		if (result != null) {
			EObject root = result.getRoot();
			if (root != null) {
				dummyResource.getContentsInternal().add(root);
			}
			for (de.darwinspl.fmec.resource.fmec.IFmecCommand<de.darwinspl.fmec.resource.fmec.IFmecTextResource> command : result.getPostParseCommands()) {
				command.execute(dummyResource);
			}
		}
		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
		Set<de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal> currentFollowSet = new LinkedHashSet<de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal>();
		List<de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal> newFollowSet = new ArrayList<de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal>();
		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
			de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal expectedElementI = expectedElements.get(i);
			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
				currentFollowSet.add(expectedElementI);
			} else {
				break;
			}
		}
		int followSetID = 83;
		int i;
		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
			CommonToken nextToken = (CommonToken) tokenStream.get(i);
			if (nextToken.getType() < 0) {
				break;
			}
			if (nextToken.getChannel() == 99) {
				// hidden tokens do not reduce the follow set
			} else {
				// now that we have found the next visible token the position for that expected
				// terminals can be set
				for (de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal nextFollow : newFollowSet) {
					lastTokenIndex = 0;
					setPosition(nextFollow, i);
				}
				newFollowSet.clear();
				// normal tokens do reduce the follow set - only elements that match the token are
				// kept
				for (de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal nextFollow : currentFollowSet) {
					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
						// keep this one - it matches
						Collection<de.darwinspl.fmec.resource.fmec.util.FmecPair<de.darwinspl.fmec.resource.fmec.IFmecExpectedElement, de.darwinspl.fmec.resource.fmec.mopp.FmecContainedFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
						for (de.darwinspl.fmec.resource.fmec.util.FmecPair<de.darwinspl.fmec.resource.fmec.IFmecExpectedElement, de.darwinspl.fmec.resource.fmec.mopp.FmecContainedFeature[]> newFollowerPair : newFollowers) {
							de.darwinspl.fmec.resource.fmec.IFmecExpectedElement newFollower = newFollowerPair.getLeft();
							EObject container = getLastIncompleteElement();
							de.darwinspl.fmec.resource.fmec.grammar.FmecContainmentTrace containmentTrace = new de.darwinspl.fmec.resource.fmec.grammar.FmecContainmentTrace(null, newFollowerPair.getRight());
							de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal newFollowTerminal = new de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal(container, newFollower, followSetID, containmentTrace);
							newFollowSet.add(newFollowTerminal);
							expectedElements.add(newFollowTerminal);
						}
					}
				}
				currentFollowSet.clear();
				currentFollowSet.addAll(newFollowSet);
			}
			followSetID++;
		}
		// after the last token in the stream we must set the position for the elements
		// that were added during the last iteration of the loop
		for (de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal nextFollow : newFollowSet) {
			lastTokenIndex = 0;
			setPosition(nextFollow, i);
		}
		return this.expectedElements;
	}
	
	public void setPosition(de.darwinspl.fmec.resource.fmec.mopp.FmecExpectedTerminal expectedElement, int tokenIndex) {
		int currentIndex = Math.max(0, tokenIndex);
		for (int index = lastTokenIndex; index < currentIndex; index++) {
			if (index >= input.size()) {
				break;
			}
			CommonToken tokenAtIndex = (CommonToken) input.get(index);
			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			if (tokenAtIndex.getChannel() != 99 && !anonymousTokens.contains(tokenAtIndex)) {
				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			}
		}
		lastTokenIndex = Math.max(0, currentIndex);
		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
	}
	
	public Object recoverFromMismatchedToken(IntStream input, int ttype, BitSet follow) throws RecognitionException {
		if (!rememberExpectedElements) {
			return super.recoverFromMismatchedToken(input, ttype, follow);
		} else {
			return null;
		}
	}
	
	private void startIncompleteElement(Object object) {
		if (object instanceof EObject) {
			this.incompleteObjects.add((EObject) object);
		}
	}
	
	private void completedElement(Object object, boolean isContainment) {
		if (isContainment && !this.incompleteObjects.isEmpty()) {
			this.incompleteObjects.remove(object);
		}
		if (object instanceof EObject) {
			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
		}
	}
	
	private EObject getLastIncompleteElement() {
		if (incompleteObjects.isEmpty()) {
			return null;
		}
		return incompleteObjects.get(incompleteObjects.size() - 1);
	}
	
}

start returns [ EObject element = null]
:
	{
		// follow set for start rule(s)
		addExpectedElement(null, 0);
		expectedElementsIndexOfLastCompleteElement = 0;
	}
	(
		c0 = parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel{ element = c0; }
	)
	EOF	{
		retrieveLayoutInformation(element, null, null, false);
	}
	
;

parse_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel returns [de.darwinspl.fmec.FeatureModelEvolutionConstraintModel element = null]
@init{
}
:
	a0 = 'feature' {
		if (element == null) {
			element = de.darwinspl.fmec.FmecFactory.eINSTANCE.createFeatureModelEvolutionConstraintModel();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_0_0_0_0, null, true);
		copyLocalizationInfos((CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, 1);
	}
	
	a1 = 'model' {
		if (element == null) {
			element = de.darwinspl.fmec.FmecFactory.eINSTANCE.createFeatureModelEvolutionConstraintModel();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_0_0_0_1, null, true);
		copyLocalizationInfos((CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, 2);
	}
	
	(
		a2 = PATH_TO_FILE		
		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.FmecFactory.eINSTANCE.createFeatureModelEvolutionConstraintModel();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("PATH_TO_FILE");
				tokenResolver.setOptions(getOptions());
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__FEATURE_MODEL), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((CommonToken) a2).getLine(), ((CommonToken) a2).getCharPositionInLine(), ((CommonToken) a2).getStartIndex(), ((CommonToken) a2).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				de.darwinspl.feature.DwTemporalFeatureModel proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwTemporalFeatureModel();
				collectHiddenTokens(element);
				registerContextDependentProxy(new de.darwinspl.fmec.resource.fmec.mopp.FmecContextDependentURIFragmentFactory<de.darwinspl.fmec.FeatureModelEvolutionConstraintModel, de.darwinspl.feature.DwTemporalFeatureModel>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getFeatureModelEvolutionConstraintModelFeatureModelReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__FEATURE_MODEL), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__FEATURE_MODEL), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_0_0_0_2, proxy, true);
				copyLocalizationInfos((CommonToken) a2, element);
				copyLocalizationInfos((CommonToken) a2, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(de.darwinspl.fmec.FmecPackage.eINSTANCE.getFeatureModelEvolutionConstraintModel(), 3, 38);
	}
	
	(
		(
			(
				a3_0 = parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint				{
					if (terminateParsing) {
						throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
					}
					if (element == null) {
						element = de.darwinspl.fmec.FmecFactory.eINSTANCE.createFeatureModelEvolutionConstraintModel();
						startIncompleteElement(element);
					}
					if (a3_0 != null) {
						if (a3_0 != null) {
							Object value = a3_0;
							addObjectToList(element, de.darwinspl.fmec.FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__CONSTRAINTS, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_0_0_0_5_0_0_0, a3_0, true);
						copyLocalizationInfos(a3_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, 39);
			}
			
			a4 = ';' {
				if (element == null) {
					element = de.darwinspl.fmec.FmecFactory.eINSTANCE.createFeatureModelEvolutionConstraintModel();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_0_0_0_5_0_0_1, null, true);
				copyLocalizationInfos((CommonToken)a4, element);
			}
			{
				// expected elements (follow set)
				// We've found the last token for this rule. The constructed EObject is now
				// complete.
				completedElement(element, true);
				addExpectedElement(de.darwinspl.fmec.FmecPackage.eINSTANCE.getFeatureModelEvolutionConstraintModel(), 40, 75);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.fmec.FmecPackage.eINSTANCE.getFeatureModelEvolutionConstraintModel(), 76, 111);
	}
	
;

parse_de_darwinspl_fmec_constraints_BinaryConstraint returns [de.darwinspl.fmec.constraints.BinaryConstraint element = null]
@init{
}
:
	(
		a0_0 = parse_de_darwinspl_fmec_constraints_BinaryConstraintChild		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createBinaryConstraint();
				startIncompleteElement(element);
			}
			if (a0_0 != null) {
				if (a0_0 != null) {
					Object value = a0_0;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.BINARY_CONSTRAINT__LEFT_CHILD), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_1_0_0_0, a0_0, true);
				copyLocalizationInfos(a0_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getBinaryConstraint(), 112, 114);
	}
	
	(
		a1_0 = parse_de_darwinspl_fmec_keywords_logical_BinaryLogicalKeyword		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createBinaryConstraint();
				startIncompleteElement(element);
			}
			if (a1_0 != null) {
				if (a1_0 != null) {
					Object value = a1_0;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.BINARY_CONSTRAINT__KEYWORD), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_1_0_0_2, a1_0, true);
				copyLocalizationInfos(a1_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getBinaryConstraint(), 115, 132);
	}
	
	(
		a2_0 = parse_de_darwinspl_fmec_constraints_BinaryConstraintChild		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createBinaryConstraint();
				startIncompleteElement(element);
			}
			if (a2_0 != null) {
				if (a2_0 != null) {
					Object value = a2_0;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.BINARY_CONSTRAINT__RIGHT_CHILD), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_1_0_0_4, a2_0, true);
				copyLocalizationInfos(a2_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(null, 133, 134);
	}
	
;

parse_de_darwinspl_fmec_constraints_UnaryConstraint returns [de.darwinspl.fmec.constraints.UnaryConstraint element = null]
@init{
}
:
	(
		a0_0 = parse_de_darwinspl_fmec_keywords_logical_UnaryLogicalKeyword		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createUnaryConstraint();
				startIncompleteElement(element);
			}
			if (a0_0 != null) {
				if (a0_0 != null) {
					Object value = a0_0;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.UNARY_CONSTRAINT__KEYWORD), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_2_0_0_0, a0_0, true);
				copyLocalizationInfos(a0_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getUnaryConstraint(), 135, 151);
	}
	
	(
		a1_0 = parse_de_darwinspl_fmec_constraints_UnaryConstraintChild		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createUnaryConstraint();
				startIncompleteElement(element);
			}
			if (a1_0 != null) {
				if (a1_0 != null) {
					Object value = a1_0;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.UNARY_CONSTRAINT__CHILD), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_2_0_0_1, a1_0, true);
				copyLocalizationInfos(a1_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(null, 152);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getBinaryConstraint(), 153, 155);
		addExpectedElement(null, 156);
	}
	
;

parse_de_darwinspl_fmec_constraints_NestedConstraint returns [de.darwinspl.fmec.constraints.NestedConstraint element = null]
@init{
}
:
	a0 = '{' {
		if (element == null) {
			element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createNestedConstraint();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_3_0_0_0, null, true);
		copyLocalizationInfos((CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getNestedConstraint(), 157, 192);
	}
	
	(
		a1_0 = parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createNestedConstraint();
				startIncompleteElement(element);
			}
			if (a1_0 != null) {
				if (a1_0 != null) {
					Object value = a1_0;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.NESTED_CONSTRAINT__NESTED_CONSTRAINT), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_3_0_0_2, a1_0, true);
				copyLocalizationInfos(a1_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, 193);
	}
	
	a2 = '}' {
		if (element == null) {
			element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createNestedConstraint();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_3_0_0_4, null, true);
		copyLocalizationInfos((CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(null, 194);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getBinaryConstraint(), 195, 197);
		addExpectedElement(null, 198);
	}
	
;

parse_de_darwinspl_fmec_constraints_TimePointConstraint returns [de.darwinspl.fmec.constraints.TimePointConstraint element = null]
@init{
}
:
	(
		a0_0 = parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createTimePointConstraint();
				startIncompleteElement(element);
			}
			if (a0_0 != null) {
				if (a0_0 != null) {
					Object value = a0_0;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_POINT_CONSTRAINT__PRE_ARGUMENT), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_4_0_0_0, a0_0, true);
				copyLocalizationInfos(a0_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 199, 203);
	}
	
	(
		a1_0 = parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createTimePointConstraint();
				startIncompleteElement(element);
			}
			if (a1_0 != null) {
				if (a1_0 != null) {
					Object value = a1_0;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_POINT_CONSTRAINT__KEYWORD), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_4_0_0_1, a1_0, true);
				copyLocalizationInfos(a1_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 204, 211);
	}
	
	(
		a2_0 = parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createTimePointConstraint();
				startIncompleteElement(element);
			}
			if (a2_0 != null) {
				if (a2_0 != null) {
					Object value = a2_0;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_POINT_CONSTRAINT__POST_ARGUMENT), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_4_0_0_2, a2_0, true);
				copyLocalizationInfos(a2_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(null, 212);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getBinaryConstraint(), 213, 215);
		addExpectedElement(null, 216);
	}
	
;

parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint returns [de.darwinspl.fmec.constraints.TimeIntervalConstraint element = null]
@init{
}
:
	(
		a0_0 = parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createTimeIntervalConstraint();
				startIncompleteElement(element);
			}
			if (a0_0 != null) {
				if (a0_0 != null) {
					Object value = a0_0;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__PRE_ARGUMENT), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_5_0_0_0, a0_0, true);
				copyLocalizationInfos(a0_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 217);
	}
	
	(
		a1_0 = parse_de_darwinspl_fmec_keywords_interval_IntervalKeyword		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createTimeIntervalConstraint();
				startIncompleteElement(element);
			}
			if (a1_0 != null) {
				if (a1_0 != null) {
					Object value = a1_0;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__KEYWORD), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_5_0_0_1, a1_0, true);
				copyLocalizationInfos(a1_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, 218);
	}
	
	a2 = '[' {
		if (element == null) {
			element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createTimeIntervalConstraint();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_5_0_0_2, null, true);
		copyLocalizationInfos((CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 219, 220);
	}
	
	(
		a3_0 = parse_de_darwinspl_fmec_timepoint_TimePoint		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createTimeIntervalConstraint();
				startIncompleteElement(element);
			}
			if (a3_0 != null) {
				if (a3_0 != null) {
					Object value = a3_0;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__START_POINT), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_5_0_0_3, a3_0, true);
				copyLocalizationInfos(a3_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, 221);
	}
	
	a4 = ',' {
		if (element == null) {
			element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createTimeIntervalConstraint();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_5_0_0_4, null, true);
		copyLocalizationInfos((CommonToken)a4, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 222, 223);
	}
	
	(
		a5_0 = parse_de_darwinspl_fmec_timepoint_TimePoint		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createTimeIntervalConstraint();
				startIncompleteElement(element);
			}
			if (a5_0 != null) {
				if (a5_0 != null) {
					Object value = a5_0;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__END_POINT), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_5_0_0_5, a5_0, true);
				copyLocalizationInfos(a5_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, 224);
	}
	
	a6 = ')' {
		if (element == null) {
			element = de.darwinspl.fmec.constraints.ConstraintsFactory.eINSTANCE.createTimeIntervalConstraint();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_5_0_0_6, null, true);
		copyLocalizationInfos((CommonToken)a6, element);
	}
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(null, 225);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getBinaryConstraint(), 226, 228);
		addExpectedElement(null, 229);
	}
	
;

parse_de_darwinspl_fmec_property_EvolutionaryProperty returns [de.darwinspl.fmec.property.EvolutionaryProperty element = null]
@init{
}
:
	(
		a0_0 = parse_de_darwinspl_fmec_property_Property		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createEvolutionaryProperty();
				startIncompleteElement(element);
			}
			if (a0_0 != null) {
				if (a0_0 != null) {
					Object value = a0_0;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.EVOLUTIONARY_PROPERTY__PROPERTY), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_6_0_0_0, a0_0, true);
				copyLocalizationInfos(a0_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getEvolutionaryProperty(), 230);
	}
	
	(
		a1_0 = parse_de_darwinspl_fmec_keywords_buildingblocks_PropertyKeyword		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createEvolutionaryProperty();
				startIncompleteElement(element);
			}
			if (a1_0 != null) {
				if (a1_0 != null) {
					Object value = a1_0;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.EVOLUTIONARY_PROPERTY__KEYWORD), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_6_0_0_1, a1_0, true);
				copyLocalizationInfos(a1_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 231, 235);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 236);
	}
	
;

parse_de_darwinspl_fmec_property_FeatureExistenceProperty returns [de.darwinspl.fmec.property.FeatureExistenceProperty element = null]
@init{
}
:
	(
		a0 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createFeatureExistenceProperty();
				startIncompleteElement(element);
			}
			if (a0 != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_EXISTENCE_PROPERTY__REFERENCED_FEATURE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((CommonToken) a0).getLine(), ((CommonToken) a0).getCharPositionInLine(), ((CommonToken) a0).getStartIndex(), ((CommonToken) a0).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				de.darwinspl.feature.DwFeature proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwFeature();
				collectHiddenTokens(element);
				registerContextDependentProxy(new de.darwinspl.fmec.resource.fmec.mopp.FmecContextDependentURIFragmentFactory<de.darwinspl.fmec.property.FeatureReference, de.darwinspl.feature.DwFeature>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getFeatureReferenceReferencedFeatureReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_EXISTENCE_PROPERTY__REFERENCED_FEATURE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_EXISTENCE_PROPERTY__REFERENCED_FEATURE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_7_0_0_0, proxy, true);
				copyLocalizationInfos((CommonToken) a0, element);
				copyLocalizationInfos((CommonToken) a0, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getEvolutionaryProperty(), 237);
	}
	
;

parse_de_darwinspl_fmec_property_FeatureTypeProperty returns [de.darwinspl.fmec.property.FeatureTypeProperty element = null]
@init{
}
:
	(
		a0 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createFeatureTypeProperty();
				startIncompleteElement(element);
			}
			if (a0 != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_TYPE_PROPERTY__REFERENCED_FEATURE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((CommonToken) a0).getLine(), ((CommonToken) a0).getCharPositionInLine(), ((CommonToken) a0).getStartIndex(), ((CommonToken) a0).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				de.darwinspl.feature.DwFeature proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwFeature();
				collectHiddenTokens(element);
				registerContextDependentProxy(new de.darwinspl.fmec.resource.fmec.mopp.FmecContextDependentURIFragmentFactory<de.darwinspl.fmec.property.FeatureReference, de.darwinspl.feature.DwFeature>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getFeatureReferenceReferencedFeatureReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_TYPE_PROPERTY__REFERENCED_FEATURE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_TYPE_PROPERTY__REFERENCED_FEATURE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_8_0_0_0, proxy, true);
				copyLocalizationInfos((CommonToken) a0, element);
				copyLocalizationInfos((CommonToken) a0, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, 238);
	}
	
	a1 = '.' {
		if (element == null) {
			element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createFeatureTypeProperty();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_8_0_0_1, null, true);
		copyLocalizationInfos((CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, 239);
	}
	
	a2 = 'type' {
		if (element == null) {
			element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createFeatureTypeProperty();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_8_0_0_2, null, true);
		copyLocalizationInfos((CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, 240);
	}
	
	a3 = '==' {
		if (element == null) {
			element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createFeatureTypeProperty();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_8_0_0_3, null, true);
		copyLocalizationInfos((CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, 241);
	}
	
	(
		(
			a4 = 'OPTIONAL' {
				if (element == null) {
					element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createFeatureTypeProperty();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_8_0_0_4, null, true);
				copyLocalizationInfos((CommonToken)a4, element);
				// set value of enumeration attribute
				Object value = de.darwinspl.feature.DwFeaturePackage.eINSTANCE.getDwFeatureTypeEnum().getEEnumLiteral(de.darwinspl.feature.DwFeatureTypeEnum.OPTIONAL_VALUE).getInstance();
				element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_TYPE_PROPERTY__FEATURE_TYPE), value);
				completedElement(value, false);
			}
			|			a5 = 'MANDATORY' {
				if (element == null) {
					element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createFeatureTypeProperty();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_8_0_0_4, null, true);
				copyLocalizationInfos((CommonToken)a5, element);
				// set value of enumeration attribute
				Object value = de.darwinspl.feature.DwFeaturePackage.eINSTANCE.getDwFeatureTypeEnum().getEEnumLiteral(de.darwinspl.feature.DwFeatureTypeEnum.MANDATORY_VALUE).getInstance();
				element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_TYPE_PROPERTY__FEATURE_TYPE), value);
				completedElement(value, false);
			}
		)
	)
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getEvolutionaryProperty(), 242);
	}
	
;

parse_de_darwinspl_fmec_property_FeatureParentProperty returns [de.darwinspl.fmec.property.FeatureParentProperty element = null]
@init{
}
:
	(
		a0 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createFeatureParentProperty();
				startIncompleteElement(element);
			}
			if (a0 != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_PARENT_PROPERTY__REFERENCED_FEATURE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((CommonToken) a0).getLine(), ((CommonToken) a0).getCharPositionInLine(), ((CommonToken) a0).getStartIndex(), ((CommonToken) a0).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				de.darwinspl.feature.DwFeature proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwFeature();
				collectHiddenTokens(element);
				registerContextDependentProxy(new de.darwinspl.fmec.resource.fmec.mopp.FmecContextDependentURIFragmentFactory<de.darwinspl.fmec.property.FeatureReference, de.darwinspl.feature.DwFeature>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getFeatureReferenceReferencedFeatureReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_PARENT_PROPERTY__REFERENCED_FEATURE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_PARENT_PROPERTY__REFERENCED_FEATURE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_9_0_0_0, proxy, true);
				copyLocalizationInfos((CommonToken) a0, element);
				copyLocalizationInfos((CommonToken) a0, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, 243);
	}
	
	a1 = '.' {
		if (element == null) {
			element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createFeatureParentProperty();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_9_0_0_1, null, true);
		copyLocalizationInfos((CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, 244);
	}
	
	a2 = 'parentGroup' {
		if (element == null) {
			element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createFeatureParentProperty();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_9_0_0_2, null, true);
		copyLocalizationInfos((CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, 245);
	}
	
	a3 = '==' {
		if (element == null) {
			element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createFeatureParentProperty();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_9_0_0_3, null, true);
		copyLocalizationInfos((CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, 246);
	}
	
	(
		a4 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createFeatureParentProperty();
				startIncompleteElement(element);
			}
			if (a4 != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_PARENT_PROPERTY__REFERENCED_GROUP), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((CommonToken) a4).getLine(), ((CommonToken) a4).getCharPositionInLine(), ((CommonToken) a4).getStartIndex(), ((CommonToken) a4).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				de.darwinspl.feature.DwGroup proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwGroup();
				collectHiddenTokens(element);
				registerContextDependentProxy(new de.darwinspl.fmec.resource.fmec.mopp.FmecContextDependentURIFragmentFactory<de.darwinspl.fmec.property.GroupReference, de.darwinspl.feature.DwGroup>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getGroupReferenceReferencedGroupReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_PARENT_PROPERTY__REFERENCED_GROUP), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_PARENT_PROPERTY__REFERENCED_GROUP), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_9_0_0_4, proxy, true);
				copyLocalizationInfos((CommonToken) a4, element);
				copyLocalizationInfos((CommonToken) a4, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getEvolutionaryProperty(), 247);
	}
	
;

parse_de_darwinspl_fmec_property_GroupExistenceProperty returns [de.darwinspl.fmec.property.GroupExistenceProperty element = null]
@init{
}
:
	(
		a0 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupExistenceProperty();
				startIncompleteElement(element);
			}
			if (a0 != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_EXISTENCE_PROPERTY__REFERENCED_GROUP), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((CommonToken) a0).getLine(), ((CommonToken) a0).getCharPositionInLine(), ((CommonToken) a0).getStartIndex(), ((CommonToken) a0).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				de.darwinspl.feature.DwGroup proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwGroup();
				collectHiddenTokens(element);
				registerContextDependentProxy(new de.darwinspl.fmec.resource.fmec.mopp.FmecContextDependentURIFragmentFactory<de.darwinspl.fmec.property.GroupReference, de.darwinspl.feature.DwGroup>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getGroupReferenceReferencedGroupReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_EXISTENCE_PROPERTY__REFERENCED_GROUP), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_EXISTENCE_PROPERTY__REFERENCED_GROUP), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_10_0_0_0, proxy, true);
				copyLocalizationInfos((CommonToken) a0, element);
				copyLocalizationInfos((CommonToken) a0, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getEvolutionaryProperty(), 248);
	}
	
;

parse_de_darwinspl_fmec_property_GroupTypeProperty returns [de.darwinspl.fmec.property.GroupTypeProperty element = null]
@init{
}
:
	(
		a0 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupTypeProperty();
				startIncompleteElement(element);
			}
			if (a0 != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_TYPE_PROPERTY__REFERENCED_GROUP), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((CommonToken) a0).getLine(), ((CommonToken) a0).getCharPositionInLine(), ((CommonToken) a0).getStartIndex(), ((CommonToken) a0).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				de.darwinspl.feature.DwGroup proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwGroup();
				collectHiddenTokens(element);
				registerContextDependentProxy(new de.darwinspl.fmec.resource.fmec.mopp.FmecContextDependentURIFragmentFactory<de.darwinspl.fmec.property.GroupReference, de.darwinspl.feature.DwGroup>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getGroupReferenceReferencedGroupReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_TYPE_PROPERTY__REFERENCED_GROUP), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_TYPE_PROPERTY__REFERENCED_GROUP), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_11_0_0_0, proxy, true);
				copyLocalizationInfos((CommonToken) a0, element);
				copyLocalizationInfos((CommonToken) a0, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, 249);
	}
	
	a1 = '.' {
		if (element == null) {
			element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupTypeProperty();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_11_0_0_1, null, true);
		copyLocalizationInfos((CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, 250);
	}
	
	a2 = 'type' {
		if (element == null) {
			element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupTypeProperty();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_11_0_0_2, null, true);
		copyLocalizationInfos((CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, 251);
	}
	
	a3 = '==' {
		if (element == null) {
			element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupTypeProperty();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_11_0_0_3, null, true);
		copyLocalizationInfos((CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, 252);
	}
	
	(
		(
			a4 = 'AND' {
				if (element == null) {
					element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupTypeProperty();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_11_0_0_4, null, true);
				copyLocalizationInfos((CommonToken)a4, element);
				// set value of enumeration attribute
				Object value = de.darwinspl.feature.DwFeaturePackage.eINSTANCE.getDwGroupTypeEnum().getEEnumLiteral(de.darwinspl.feature.DwGroupTypeEnum.AND_VALUE).getInstance();
				element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_TYPE_PROPERTY__GROUP_TYPE), value);
				completedElement(value, false);
			}
			|			a5 = 'ALTERNATIVE' {
				if (element == null) {
					element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupTypeProperty();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_11_0_0_4, null, true);
				copyLocalizationInfos((CommonToken)a5, element);
				// set value of enumeration attribute
				Object value = de.darwinspl.feature.DwFeaturePackage.eINSTANCE.getDwGroupTypeEnum().getEEnumLiteral(de.darwinspl.feature.DwGroupTypeEnum.ALTERNATIVE_VALUE).getInstance();
				element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_TYPE_PROPERTY__GROUP_TYPE), value);
				completedElement(value, false);
			}
			|			a6 = 'OR' {
				if (element == null) {
					element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupTypeProperty();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_11_0_0_4, null, true);
				copyLocalizationInfos((CommonToken)a6, element);
				// set value of enumeration attribute
				Object value = de.darwinspl.feature.DwFeaturePackage.eINSTANCE.getDwGroupTypeEnum().getEEnumLiteral(de.darwinspl.feature.DwGroupTypeEnum.OR_VALUE).getInstance();
				element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_TYPE_PROPERTY__GROUP_TYPE), value);
				completedElement(value, false);
			}
		)
	)
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getEvolutionaryProperty(), 253);
	}
	
;

parse_de_darwinspl_fmec_property_GroupParentProperty returns [de.darwinspl.fmec.property.GroupParentProperty element = null]
@init{
}
:
	(
		a0 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupParentProperty();
				startIncompleteElement(element);
			}
			if (a0 != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_GROUP), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((CommonToken) a0).getLine(), ((CommonToken) a0).getCharPositionInLine(), ((CommonToken) a0).getStartIndex(), ((CommonToken) a0).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				de.darwinspl.feature.DwGroup proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwGroup();
				collectHiddenTokens(element);
				registerContextDependentProxy(new de.darwinspl.fmec.resource.fmec.mopp.FmecContextDependentURIFragmentFactory<de.darwinspl.fmec.property.GroupReference, de.darwinspl.feature.DwGroup>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getGroupReferenceReferencedGroupReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_GROUP), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_GROUP), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_12_0_0_0, proxy, true);
				copyLocalizationInfos((CommonToken) a0, element);
				copyLocalizationInfos((CommonToken) a0, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, 254);
	}
	
	a1 = '.' {
		if (element == null) {
			element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupParentProperty();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_12_0_0_1, null, true);
		copyLocalizationInfos((CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, 255);
	}
	
	a2 = 'parentFeature' {
		if (element == null) {
			element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupParentProperty();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_12_0_0_2, null, true);
		copyLocalizationInfos((CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, 256);
	}
	
	a3 = '==' {
		if (element == null) {
			element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupParentProperty();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_12_0_0_3, null, true);
		copyLocalizationInfos((CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, 257);
	}
	
	(
		a4 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.property.PropertyFactory.eINSTANCE.createGroupParentProperty();
				startIncompleteElement(element);
			}
			if (a4 != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_FEATURE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((CommonToken) a4).getLine(), ((CommonToken) a4).getCharPositionInLine(), ((CommonToken) a4).getStartIndex(), ((CommonToken) a4).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				de.darwinspl.feature.DwFeature proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwFeature();
				collectHiddenTokens(element);
				registerContextDependentProxy(new de.darwinspl.fmec.resource.fmec.mopp.FmecContextDependentURIFragmentFactory<de.darwinspl.fmec.property.FeatureReference, de.darwinspl.feature.DwFeature>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getFeatureReferenceReferencedFeatureReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_FEATURE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_FEATURE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_12_0_0_4, proxy, true);
				copyLocalizationInfos((CommonToken) a4, element);
				copyLocalizationInfos((CommonToken) a4, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getEvolutionaryProperty(), 258);
	}
	
;

parse_de_darwinspl_fmec_timepoint_TimePoint returns [de.darwinspl.fmec.timepoint.TimePoint element = null]
@init{
}
:
	(
		a0_0 = parse_de_darwinspl_fmec_timepoint_TimePointWithoutOffset		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimePoint();
				startIncompleteElement(element);
			}
			if (a0_0 != null) {
				if (a0_0 != null) {
					Object value = a0_0;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_POINT__TIME_POINT), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_13_0_0_0, a0_0, true);
				copyLocalizationInfos(a0_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimePoint(), 259);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 260, 264);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 265);
	}
	
	(
		(
			a1_0 = parse_de_darwinspl_fmec_timepoint_TimeOffset			{
				if (terminateParsing) {
					throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
				}
				if (element == null) {
					element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimePoint();
					startIncompleteElement(element);
				}
				if (a1_0 != null) {
					if (a1_0 != null) {
						Object value = a1_0;
						element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_POINT__OFFSET), value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_13_0_0_1, a1_0, true);
					copyLocalizationInfos(a1_0, element);
				}
			}
		)
		
	)?	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 266, 270);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 271);
	}
	
;

parse_de_darwinspl_fmec_timepoint_TimeOffset returns [de.darwinspl.fmec.timepoint.TimeOffset element = null]
@init{
}
:
	(
		(
			a0 = '+' {
				if (element == null) {
					element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimeOffset();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_14_0_0_0, true, true);
				copyLocalizationInfos((CommonToken)a0, element);
				// set value of boolean attribute
				Object value = true;
				element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__POSITIVE_SIGNATURE), value);
				completedElement(value, false);
			}
			|			a1 = '-' {
				if (element == null) {
					element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimeOffset();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_14_0_0_0, false, true);
				copyLocalizationInfos((CommonToken)a1, element);
				// set value of boolean attribute
				Object value = false;
				element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__POSITIVE_SIGNATURE), value);
				completedElement(value, false);
			}
		)
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, 272, 276);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 277, 281);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 282);
	}
	
	(
		(
			(
				a3 = INTEGER				
				{
					if (terminateParsing) {
						throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
					}
					if (element == null) {
						element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimeOffset();
						startIncompleteElement(element);
					}
					if (a3 != null) {
						de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("INTEGER");
						tokenResolver.setOptions(getOptions());
						de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__YEARS), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((CommonToken) a3).getLine(), ((CommonToken) a3).getCharPositionInLine(), ((CommonToken) a3).getStartIndex(), ((CommonToken) a3).getStopIndex());
						}
						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__YEARS), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_14_0_0_1_0_0_0, resolved, true);
						copyLocalizationInfos((CommonToken) a3, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, 283);
			}
			
			a4 = 'years' {
				if (element == null) {
					element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimeOffset();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_14_0_0_1_0_0_1, null, true);
				copyLocalizationInfos((CommonToken)a4, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, 284, 287);
				addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 288, 292);
				addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 293);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, 294, 297);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 298, 302);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 303);
	}
	
	(
		(
			(
				a5 = INTEGER				
				{
					if (terminateParsing) {
						throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
					}
					if (element == null) {
						element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimeOffset();
						startIncompleteElement(element);
					}
					if (a5 != null) {
						de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("INTEGER");
						tokenResolver.setOptions(getOptions());
						de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a5.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__MONTHS), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((CommonToken) a5).getLine(), ((CommonToken) a5).getCharPositionInLine(), ((CommonToken) a5).getStartIndex(), ((CommonToken) a5).getStopIndex());
						}
						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__MONTHS), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_14_0_0_2_0_0_0, resolved, true);
						copyLocalizationInfos((CommonToken) a5, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, 304);
			}
			
			a6 = 'months' {
				if (element == null) {
					element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimeOffset();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_14_0_0_2_0_0_1, null, true);
				copyLocalizationInfos((CommonToken)a6, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, 305, 307);
				addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 308, 312);
				addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 313);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, 314, 316);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 317, 321);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 322);
	}
	
	(
		(
			(
				a7 = INTEGER				
				{
					if (terminateParsing) {
						throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
					}
					if (element == null) {
						element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimeOffset();
						startIncompleteElement(element);
					}
					if (a7 != null) {
						de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("INTEGER");
						tokenResolver.setOptions(getOptions());
						de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__DAYS), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((CommonToken) a7).getLine(), ((CommonToken) a7).getCharPositionInLine(), ((CommonToken) a7).getStartIndex(), ((CommonToken) a7).getStopIndex());
						}
						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__DAYS), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_14_0_0_3_0_0_0, resolved, true);
						copyLocalizationInfos((CommonToken) a7, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, 323);
			}
			
			a8 = 'days' {
				if (element == null) {
					element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimeOffset();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_14_0_0_3_0_0_1, null, true);
				copyLocalizationInfos((CommonToken)a8, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, 324, 325);
				addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 326, 330);
				addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 331);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, 332, 333);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 334, 338);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 339);
	}
	
	(
		(
			(
				a9 = INTEGER				
				{
					if (terminateParsing) {
						throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
					}
					if (element == null) {
						element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimeOffset();
						startIncompleteElement(element);
					}
					if (a9 != null) {
						de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("INTEGER");
						tokenResolver.setOptions(getOptions());
						de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a9.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__HOURS), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((CommonToken) a9).getLine(), ((CommonToken) a9).getCharPositionInLine(), ((CommonToken) a9).getStartIndex(), ((CommonToken) a9).getStopIndex());
						}
						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__HOURS), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_14_0_0_4_0_0_0, resolved, true);
						copyLocalizationInfos((CommonToken) a9, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, 340);
			}
			
			a10 = 'hours' {
				if (element == null) {
					element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimeOffset();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_14_0_0_4_0_0_1, null, true);
				copyLocalizationInfos((CommonToken)a10, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, 341);
				addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 342, 346);
				addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 347);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, 348);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 349, 353);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 354);
	}
	
	(
		(
			(
				a11 = INTEGER				
				{
					if (terminateParsing) {
						throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
					}
					if (element == null) {
						element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimeOffset();
						startIncompleteElement(element);
					}
					if (a11 != null) {
						de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("INTEGER");
						tokenResolver.setOptions(getOptions());
						de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a11.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__MINUTES), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((CommonToken) a11).getLine(), ((CommonToken) a11).getCharPositionInLine(), ((CommonToken) a11).getStartIndex(), ((CommonToken) a11).getStopIndex());
						}
						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__MINUTES), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_14_0_0_5_0_0_0, resolved, true);
						copyLocalizationInfos((CommonToken) a11, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, 355);
			}
			
			a12 = 'minutes' {
				if (element == null) {
					element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createTimeOffset();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_14_0_0_5_0_0_1, null, true);
				copyLocalizationInfos((CommonToken)a12, element);
			}
			{
				// expected elements (follow set)
				// We've found the last token for this rule. The constructed EObject is now
				// complete.
				completedElement(element, true);
				addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 356, 360);
				addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 361);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 362, 366);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 367);
	}
	
;

parse_de_darwinspl_fmec_timepoint_ExplicitTimePoint returns [de.darwinspl.fmec.timepoint.ExplicitTimePoint element = null]
@init{
}
:
	(
		a0 = DATE		
		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createExplicitTimePoint();
				startIncompleteElement(element);
			}
			if (a0 != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("DATE");
				tokenResolver.setOptions(getOptions());
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.EXPLICIT_TIME_POINT__DATE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((CommonToken) a0).getLine(), ((CommonToken) a0).getCharPositionInLine(), ((CommonToken) a0).getStartIndex(), ((CommonToken) a0).getStopIndex());
				}
				java.util.Date resolved = (java.util.Date) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.EXPLICIT_TIME_POINT__DATE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_15_0_0_0, resolved, true);
				copyLocalizationInfos((CommonToken) a0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimePoint(), 368);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 369, 373);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 374);
	}
	
;

parse_de_darwinspl_fmec_timepoint_EvolutionaryEvent returns [de.darwinspl.fmec.timepoint.EvolutionaryEvent element = null]
@init{
}
:
	(
		a0 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createEvolutionaryEvent();
				startIncompleteElement(element);
			}
			if (a0 != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.EVOLUTIONARY_EVENT__REFERENCED_FEATURE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((CommonToken) a0).getLine(), ((CommonToken) a0).getCharPositionInLine(), ((CommonToken) a0).getStartIndex(), ((CommonToken) a0).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				de.darwinspl.feature.DwFeature proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwFeature();
				collectHiddenTokens(element);
				registerContextDependentProxy(new de.darwinspl.fmec.resource.fmec.mopp.FmecContextDependentURIFragmentFactory<de.darwinspl.fmec.property.FeatureReference, de.darwinspl.feature.DwFeature>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getFeatureReferenceReferencedFeatureReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.EVOLUTIONARY_EVENT__REFERENCED_FEATURE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.EVOLUTIONARY_EVENT__REFERENCED_FEATURE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_16_0_0_0, proxy, true);
				copyLocalizationInfos((CommonToken) a0, element);
				copyLocalizationInfos((CommonToken) a0, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getEvolutionaryEvent(), 375, 376);
	}
	
	(
		a1_0 = parse_de_darwinspl_fmec_keywords_buildingblocks_EventKeyword		{
			if (terminateParsing) {
				throw new de.darwinspl.fmec.resource.fmec.mopp.FmecTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.fmec.timepoint.TimepointFactory.eINSTANCE.createEvolutionaryEvent();
				startIncompleteElement(element);
			}
			if (a1_0 != null) {
				if (a1_0 != null) {
					Object value = a1_0;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.EVOLUTIONARY_EVENT__KEYWORD), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_16_0_0_1, a1_0, true);
				copyLocalizationInfos(a1_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimePoint(), 377);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 378, 382);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 383);
	}
	
;

parse_de_darwinspl_fmec_keywords_logical_KeywordAnd returns [de.darwinspl.fmec.keywords.logical.KeywordAnd element = null]
@init{
}
:
	a0 = 'and' {
		if (element == null) {
			element = de.darwinspl.fmec.keywords.logical.LogicalFactory.eINSTANCE.createKeywordAnd();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_17_0_0_0, null, true);
		copyLocalizationInfos((CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getBinaryConstraint(), 384, 401);
	}
	
;

parse_de_darwinspl_fmec_keywords_logical_KeywordOr returns [de.darwinspl.fmec.keywords.logical.KeywordOr element = null]
@init{
}
:
	a0 = 'or' {
		if (element == null) {
			element = de.darwinspl.fmec.keywords.logical.LogicalFactory.eINSTANCE.createKeywordOr();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_18_0_0_0, null, true);
		copyLocalizationInfos((CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getBinaryConstraint(), 402, 419);
	}
	
;

parse_de_darwinspl_fmec_keywords_logical_KeywordImplies returns [de.darwinspl.fmec.keywords.logical.KeywordImplies element = null]
@init{
}
:
	a0 = 'implies' {
		if (element == null) {
			element = de.darwinspl.fmec.keywords.logical.LogicalFactory.eINSTANCE.createKeywordImplies();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_19_0_0_0, null, true);
		copyLocalizationInfos((CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getBinaryConstraint(), 420, 437);
	}
	
;

parse_de_darwinspl_fmec_keywords_logical_KeywordNot returns [de.darwinspl.fmec.keywords.logical.KeywordNot element = null]
@init{
}
:
	a0 = 'not' {
		if (element == null) {
			element = de.darwinspl.fmec.keywords.logical.LogicalFactory.eINSTANCE.createKeywordNot();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_20_0_0_0, null, true);
		copyLocalizationInfos((CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getUnaryConstraint(), 438, 454);
	}
	
;

parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStarts returns [de.darwinspl.fmec.keywords.buildingblocks.KeywordStarts element = null]
@init{
}
:
	a0 = 'starts' {
		if (element == null) {
			element = de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksFactory.eINSTANCE.createKeywordStarts();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_21_0_0_0, null, true);
		copyLocalizationInfos((CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimePoint(), 455);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 456, 460);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 461);
	}
	
;

parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStops returns [de.darwinspl.fmec.keywords.buildingblocks.KeywordStops element = null]
@init{
}
:
	a0 = 'ends' {
		if (element == null) {
			element = de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksFactory.eINSTANCE.createKeywordStops();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_22_0_0_0, null, true);
		copyLocalizationInfos((CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimePoint(), 462);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 463, 467);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 468);
	}
	
;

parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordValid returns [de.darwinspl.fmec.keywords.buildingblocks.KeywordValid element = null]
@init{
}
:
	a0 = 'valid' {
		if (element == null) {
			element = de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksFactory.eINSTANCE.createKeywordValid();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_23_0_0_0, null, true);
		copyLocalizationInfos((CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 469, 473);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), 474);
	}
	
;

parse_de_darwinspl_fmec_keywords_timepoint_KeywordBefore returns [de.darwinspl.fmec.keywords.timepoint.KeywordBefore element = null]
@init{
}
:
	a0 = 'before' {
		if (element == null) {
			element = de.darwinspl.fmec.keywords.timepoint.TimepointFactory.eINSTANCE.createKeywordBefore();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_24_0_0_0, null, true);
		copyLocalizationInfos((CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 475, 482);
	}
	
;

parse_de_darwinspl_fmec_keywords_timepoint_KeywordUntil returns [de.darwinspl.fmec.keywords.timepoint.KeywordUntil element = null]
@init{
}
:
	a0 = 'until' {
		if (element == null) {
			element = de.darwinspl.fmec.keywords.timepoint.TimepointFactory.eINSTANCE.createKeywordUntil();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_25_0_0_0, null, true);
		copyLocalizationInfos((CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 483, 490);
	}
	
;

parse_de_darwinspl_fmec_keywords_timepoint_KeywordAfter returns [de.darwinspl.fmec.keywords.timepoint.KeywordAfter element = null]
@init{
}
:
	a0 = 'after' {
		if (element == null) {
			element = de.darwinspl.fmec.keywords.timepoint.TimepointFactory.eINSTANCE.createKeywordAfter();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_26_0_0_0, null, true);
		copyLocalizationInfos((CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 491, 498);
	}
	
;

parse_de_darwinspl_fmec_keywords_timepoint_KeywordWhen returns [de.darwinspl.fmec.keywords.timepoint.KeywordWhen element = null]
@init{
}
:
	a0 = 'when' {
		if (element == null) {
			element = de.darwinspl.fmec.keywords.timepoint.TimepointFactory.eINSTANCE.createKeywordWhen();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_27_0_0_0, null, true);
		copyLocalizationInfos((CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 499, 506);
	}
	
;

parse_de_darwinspl_fmec_keywords_timepoint_KeywordAt returns [de.darwinspl.fmec.keywords.timepoint.KeywordAt element = null]
@init{
}
:
	a0 = 'at' {
		if (element == null) {
			element = de.darwinspl.fmec.keywords.timepoint.TimepointFactory.eINSTANCE.createKeywordAt();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_28_0_0_0, null, true);
		copyLocalizationInfos((CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), 507, 514);
	}
	
;

parse_de_darwinspl_fmec_keywords_interval_KeywordDuring returns [de.darwinspl.fmec.keywords.interval.KeywordDuring element = null]
@init{
}
:
	a0 = 'during' {
		if (element == null) {
			element = de.darwinspl.fmec.keywords.interval.IntervalFactory.eINSTANCE.createKeywordDuring();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.FMEC_29_0_0_0, null, true);
		copyLocalizationInfos((CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(null, 515);
	}
	
;

parse_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint returns [de.darwinspl.fmec.constraints.FeatureModelEvolutionConstraint element = null]
:
	c0 = parse_de_darwinspl_fmec_constraints_BinaryConstraint{ element = c0; /* this is a subclass or primitive expression choice */ }
	|	c1 = parse_de_darwinspl_fmec_constraints_UnaryConstraint{ element = c1; /* this is a subclass or primitive expression choice */ }
	|	c2 = parse_de_darwinspl_fmec_constraints_NestedConstraint{ element = c2; /* this is a subclass or primitive expression choice */ }
	|	c3 = parse_de_darwinspl_fmec_constraints_TimePointConstraint{ element = c3; /* this is a subclass or primitive expression choice */ }
	|	c4 = parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint{ element = c4; /* this is a subclass or primitive expression choice */ }
	
;

parse_de_darwinspl_fmec_constraints_BinaryConstraintChild returns [de.darwinspl.fmec.constraints.BinaryConstraintChild element = null]
:
	c0 = parse_de_darwinspl_fmec_constraints_UnaryConstraint{ element = c0; /* this is a subclass or primitive expression choice */ }
	|	c1 = parse_de_darwinspl_fmec_constraints_NestedConstraint{ element = c1; /* this is a subclass or primitive expression choice */ }
	|	c2 = parse_de_darwinspl_fmec_constraints_TimePointConstraint{ element = c2; /* this is a subclass or primitive expression choice */ }
	|	c3 = parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint{ element = c3; /* this is a subclass or primitive expression choice */ }
	
;

parse_de_darwinspl_fmec_keywords_logical_BinaryLogicalKeyword returns [de.darwinspl.fmec.keywords.logical.BinaryLogicalKeyword element = null]
:
	c0 = parse_de_darwinspl_fmec_keywords_logical_KeywordAnd{ element = c0; /* this is a subclass or primitive expression choice */ }
	|	c1 = parse_de_darwinspl_fmec_keywords_logical_KeywordOr{ element = c1; /* this is a subclass or primitive expression choice */ }
	|	c2 = parse_de_darwinspl_fmec_keywords_logical_KeywordImplies{ element = c2; /* this is a subclass or primitive expression choice */ }
	
;

parse_de_darwinspl_fmec_keywords_logical_UnaryLogicalKeyword returns [de.darwinspl.fmec.keywords.logical.UnaryLogicalKeyword element = null]
:
	c0 = parse_de_darwinspl_fmec_keywords_logical_KeywordNot{ element = c0; /* this is a subclass or primitive expression choice */ }
	
;

parse_de_darwinspl_fmec_constraints_UnaryConstraintChild returns [de.darwinspl.fmec.constraints.UnaryConstraintChild element = null]
:
	c0 = parse_de_darwinspl_fmec_constraints_NestedConstraint{ element = c0; /* this is a subclass or primitive expression choice */ }
	|	c1 = parse_de_darwinspl_fmec_constraints_TimePointConstraint{ element = c1; /* this is a subclass or primitive expression choice */ }
	|	c2 = parse_de_darwinspl_fmec_constraints_TimeIntervalConstraint{ element = c2; /* this is a subclass or primitive expression choice */ }
	
;

parse_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock returns [de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock element = null]
:
	c0 = parse_de_darwinspl_fmec_property_EvolutionaryProperty{ element = c0; /* this is a subclass or primitive expression choice */ }
	|	c1 = parse_de_darwinspl_fmec_timepoint_TimePoint{ element = c1; /* this is a subclass or primitive expression choice */ }
	
;

parse_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword returns [de.darwinspl.fmec.keywords.timepoint.TimePointKeyword element = null]
:
	c0 = parse_de_darwinspl_fmec_keywords_timepoint_KeywordBefore{ element = c0; /* this is a subclass or primitive expression choice */ }
	|	c1 = parse_de_darwinspl_fmec_keywords_timepoint_KeywordUntil{ element = c1; /* this is a subclass or primitive expression choice */ }
	|	c2 = parse_de_darwinspl_fmec_keywords_timepoint_KeywordAfter{ element = c2; /* this is a subclass or primitive expression choice */ }
	|	c3 = parse_de_darwinspl_fmec_keywords_timepoint_KeywordWhen{ element = c3; /* this is a subclass or primitive expression choice */ }
	|	c4 = parse_de_darwinspl_fmec_keywords_timepoint_KeywordAt{ element = c4; /* this is a subclass or primitive expression choice */ }
	
;

parse_de_darwinspl_fmec_keywords_interval_IntervalKeyword returns [de.darwinspl.fmec.keywords.interval.IntervalKeyword element = null]
:
	c0 = parse_de_darwinspl_fmec_keywords_interval_KeywordDuring{ element = c0; /* this is a subclass or primitive expression choice */ }
	
;

parse_de_darwinspl_fmec_property_Property returns [de.darwinspl.fmec.property.Property element = null]
:
	c0 = parse_de_darwinspl_fmec_property_FeatureExistenceProperty{ element = c0; /* this is a subclass or primitive expression choice */ }
	|	c1 = parse_de_darwinspl_fmec_property_FeatureTypeProperty{ element = c1; /* this is a subclass or primitive expression choice */ }
	|	c2 = parse_de_darwinspl_fmec_property_FeatureParentProperty{ element = c2; /* this is a subclass or primitive expression choice */ }
	|	c3 = parse_de_darwinspl_fmec_property_GroupExistenceProperty{ element = c3; /* this is a subclass or primitive expression choice */ }
	|	c4 = parse_de_darwinspl_fmec_property_GroupTypeProperty{ element = c4; /* this is a subclass or primitive expression choice */ }
	|	c5 = parse_de_darwinspl_fmec_property_GroupParentProperty{ element = c5; /* this is a subclass or primitive expression choice */ }
	
;

parse_de_darwinspl_fmec_keywords_buildingblocks_PropertyKeyword returns [de.darwinspl.fmec.keywords.buildingblocks.PropertyKeyword element = null]
:
	c0 = parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordValid{ element = c0; /* this is a subclass or primitive expression choice */ }
	
;

parse_de_darwinspl_fmec_timepoint_TimePointWithoutOffset returns [de.darwinspl.fmec.timepoint.TimePointWithoutOffset element = null]
:
	c0 = parse_de_darwinspl_fmec_timepoint_ExplicitTimePoint{ element = c0; /* this is a subclass or primitive expression choice */ }
	|	c1 = parse_de_darwinspl_fmec_timepoint_EvolutionaryEvent{ element = c1; /* this is a subclass or primitive expression choice */ }
	
;

parse_de_darwinspl_fmec_keywords_buildingblocks_EventKeyword returns [de.darwinspl.fmec.keywords.buildingblocks.EventKeyword element = null]
:
	c0 = parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStarts{ element = c0; /* this is a subclass or primitive expression choice */ }
	|	c1 = parse_de_darwinspl_fmec_keywords_buildingblocks_KeywordStops{ element = c1; /* this is a subclass or primitive expression choice */ }
	
;

SL_COMMENT:
	('//'(~('\n'|'\r'|'\uffff'))* )
	{ _channel = 99; }
;
ML_COMMENT:
	('/*'.*'*/')
	{ _channel = 99; }
;
WHITESPACE:
	((' '|'\t'|'\f'|'\r'|'\n')+)
	{ _channel = 99; }
;
IDENTIFIER:
	('"'('A'..'Z'|'a'..'z'|'0'..'9'|'_'|'-'|' ')+'"')
;
PATH_TO_FILE:
	('<' ('A'..'Z'|'a'..'z'|'0'..'9'|'_'|' '|'/')+ '.tfm>')
;
DATE:
	(('0'..'3')?('0'..'9') '.' ('0'..'1')?('0'..'9') '.' ('0'..'2')('0'..'9')('0'..'9')('0'..'9') ((' '|'\t'|'\f') ('0'..'2')?('0'..'9') ':' ('0'..'5')('0'..'9'))?)
;
INTEGER:
	(('0'|'1'..'9''0'..'9'*))
;

