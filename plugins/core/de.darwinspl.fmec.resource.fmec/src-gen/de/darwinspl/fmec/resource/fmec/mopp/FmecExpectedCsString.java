/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.mopp;

import java.util.Collections;
import java.util.Set;

/**
 * A representation for a range in a document where a keyword (i.e., a static
 * string) is expected.
 */
public class FmecExpectedCsString extends de.darwinspl.fmec.resource.fmec.mopp.FmecAbstractExpectedElement {
	
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword keyword;
	
	public FmecExpectedCsString(de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword keyword) {
		super(keyword.getMetaclass());
		this.keyword = keyword;
	}
	
	public String getValue() {
		return keyword.getValue();
	}
	
	/**
	 * Returns the expected keyword.
	 */
	public de.darwinspl.fmec.resource.fmec.grammar.FmecSyntaxElement getSyntaxElement() {
		return keyword;
	}
	
	public Set<String> getTokenNames() {
		return Collections.singleton("'" + getValue() + "'");
	}
	
	public String toString() {
		return "CsString \"" + getValue() + "\"";
	}
	
	public boolean equals(Object o) {
		if (o instanceof FmecExpectedCsString) {
			return getValue().equals(((FmecExpectedCsString) o).getValue());
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return getValue().hashCode();
	}
	
}
