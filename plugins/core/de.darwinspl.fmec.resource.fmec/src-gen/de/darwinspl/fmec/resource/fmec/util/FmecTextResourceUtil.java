/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.util;

import java.io.File;
import java.util.Map;
import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;

/**
 * Class FmecTextResourceUtil can be used to perform common tasks on text
 * resources, such as loading and saving resources, as well as, checking them for
 * errors. This class is deprecated and has been replaced by
 * de.darwinspl.fmec.resource.fmec.util.FmecResourceUtil.
 */
public class FmecTextResourceUtil {
	
	/**
	 * Use de.darwinspl.fmec.resource.fmec.util.FmecResourceUtil.getResource() instead.
	 */
	@Deprecated
	public static de.darwinspl.fmec.resource.fmec.mopp.FmecResource getResource(IFile file) {
		return new de.darwinspl.fmec.resource.fmec.util.FmecEclipseProxy().getResource(file);
	}
	
	/**
	 * Use de.darwinspl.fmec.resource.fmec.util.FmecResourceUtil.getResource() instead.
	 */
	@Deprecated
	public static de.darwinspl.fmec.resource.fmec.mopp.FmecResource getResource(File file, Map<?,?> options) {
		return de.darwinspl.fmec.resource.fmec.util.FmecResourceUtil.getResource(file, options);
	}
	
	/**
	 * Use de.darwinspl.fmec.resource.fmec.util.FmecResourceUtil.getResource() instead.
	 */
	@Deprecated
	public static de.darwinspl.fmec.resource.fmec.mopp.FmecResource getResource(URI uri) {
		return de.darwinspl.fmec.resource.fmec.util.FmecResourceUtil.getResource(uri);
	}
	
	/**
	 * Use de.darwinspl.fmec.resource.fmec.util.FmecResourceUtil.getResource() instead.
	 */
	@Deprecated
	public static de.darwinspl.fmec.resource.fmec.mopp.FmecResource getResource(URI uri, Map<?,?> options) {
		return de.darwinspl.fmec.resource.fmec.util.FmecResourceUtil.getResource(uri, options);
	}
	
}
