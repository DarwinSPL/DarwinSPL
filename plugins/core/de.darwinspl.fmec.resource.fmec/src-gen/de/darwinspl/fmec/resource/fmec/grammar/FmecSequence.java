/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.grammar;


public class FmecSequence extends de.darwinspl.fmec.resource.fmec.grammar.FmecSyntaxElement {
	
	public FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality cardinality, de.darwinspl.fmec.resource.fmec.grammar.FmecSyntaxElement... elements) {
		super(cardinality, elements);
	}
	
	public String toString() {
		return de.darwinspl.fmec.resource.fmec.util.FmecStringUtil.explode(getChildren(), " ");
	}
	
}
