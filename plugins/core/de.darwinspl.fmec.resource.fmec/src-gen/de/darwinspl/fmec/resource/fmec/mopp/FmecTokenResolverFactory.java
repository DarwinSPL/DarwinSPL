/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.mopp;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * The FmecTokenResolverFactory class provides access to all generated token
 * resolvers. By giving the name of a defined token, the corresponding resolve can
 * be obtained. Despite the fact that this class is called TokenResolverFactory is
 * does NOT create new token resolvers whenever a client calls methods to obtain a
 * resolver. Rather, this class maintains a map of all resolvers and creates each
 * resolver at most once.
 */
public class FmecTokenResolverFactory implements de.darwinspl.fmec.resource.fmec.IFmecTokenResolverFactory {
	
	private Map<String, de.darwinspl.fmec.resource.fmec.IFmecTokenResolver> tokenName2TokenResolver;
	private Map<String, de.darwinspl.fmec.resource.fmec.IFmecTokenResolver> featureName2CollectInTokenResolver;
	private static de.darwinspl.fmec.resource.fmec.IFmecTokenResolver defaultResolver = new de.darwinspl.fmec.resource.fmec.analysis.FmecDefaultTokenResolver();
	
	public FmecTokenResolverFactory() {
		tokenName2TokenResolver = new LinkedHashMap<String, de.darwinspl.fmec.resource.fmec.IFmecTokenResolver>();
		featureName2CollectInTokenResolver = new LinkedHashMap<String, de.darwinspl.fmec.resource.fmec.IFmecTokenResolver>();
		registerTokenResolver("IDENTIFIER", new de.darwinspl.fmec.resource.fmec.analysis.FmecIDENTIFIERTokenResolver());
		registerTokenResolver("PATH_TO_FILE", new de.darwinspl.fmec.resource.fmec.analysis.FmecPATH_TO_FILETokenResolver());
		registerTokenResolver("DATE", new de.darwinspl.fmec.resource.fmec.analysis.FmecDATETokenResolver());
		registerTokenResolver("INTEGER", new de.darwinspl.fmec.resource.fmec.analysis.FmecINTEGERTokenResolver());
	}
	
	public de.darwinspl.fmec.resource.fmec.IFmecTokenResolver createTokenResolver(String tokenName) {
		return internalCreateResolver(tokenName2TokenResolver, tokenName);
	}
	
	public de.darwinspl.fmec.resource.fmec.IFmecTokenResolver createCollectInTokenResolver(String featureName) {
		return internalCreateResolver(featureName2CollectInTokenResolver, featureName);
	}
	
	protected boolean registerTokenResolver(String tokenName, de.darwinspl.fmec.resource.fmec.IFmecTokenResolver resolver){
		return internalRegisterTokenResolver(tokenName2TokenResolver, tokenName, resolver);
	}
	
	protected boolean registerCollectInTokenResolver(String featureName, de.darwinspl.fmec.resource.fmec.IFmecTokenResolver resolver){
		return internalRegisterTokenResolver(featureName2CollectInTokenResolver, featureName, resolver);
	}
	
	protected de.darwinspl.fmec.resource.fmec.IFmecTokenResolver deRegisterTokenResolver(String tokenName){
		return tokenName2TokenResolver.remove(tokenName);
	}
	
	private de.darwinspl.fmec.resource.fmec.IFmecTokenResolver internalCreateResolver(Map<String, de.darwinspl.fmec.resource.fmec.IFmecTokenResolver> resolverMap, String key) {
		if (resolverMap.containsKey(key)){
			return resolverMap.get(key);
		} else {
			return defaultResolver;
		}
	}
	
	private boolean internalRegisterTokenResolver(Map<String, de.darwinspl.fmec.resource.fmec.IFmecTokenResolver> resolverMap, String key, de.darwinspl.fmec.resource.fmec.IFmecTokenResolver resolver) {
		if (!resolverMap.containsKey(key)) {
			resolverMap.put(key,resolver);
			return true;
		}
		return false;
	}
	
}
