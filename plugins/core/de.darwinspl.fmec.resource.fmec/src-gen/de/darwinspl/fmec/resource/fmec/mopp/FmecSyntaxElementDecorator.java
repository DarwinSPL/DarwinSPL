/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.mopp;

import java.util.ArrayList;
import java.util.List;

public class FmecSyntaxElementDecorator {
	
	/**
	 * the syntax element to be decorated
	 */
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSyntaxElement decoratedElement;
	
	/**
	 * an array of child decorators (one decorator per child of the decorated syntax
	 * element
	 */
	private FmecSyntaxElementDecorator[] childDecorators;
	
	/**
	 * a list of the indices that must be printed
	 */
	private List<Integer> indicesToPrint = new ArrayList<Integer>();
	
	public FmecSyntaxElementDecorator(de.darwinspl.fmec.resource.fmec.grammar.FmecSyntaxElement decoratedElement, FmecSyntaxElementDecorator[] childDecorators) {
		super();
		this.decoratedElement = decoratedElement;
		this.childDecorators = childDecorators;
	}
	
	public void addIndexToPrint(Integer index) {
		indicesToPrint.add(index);
	}
	
	public de.darwinspl.fmec.resource.fmec.grammar.FmecSyntaxElement getDecoratedElement() {
		return decoratedElement;
	}
	
	public FmecSyntaxElementDecorator[] getChildDecorators() {
		return childDecorators;
	}
	
	public Integer getNextIndexToPrint() {
		if (indicesToPrint.size() == 0) {
			return null;
		}
		return indicesToPrint.remove(0);
	}
	
	public String toString() {
		return "" + getDecoratedElement();
	}
	
}
