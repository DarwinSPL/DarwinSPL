/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.grammar;


public class FmecCompound extends de.darwinspl.fmec.resource.fmec.grammar.FmecSyntaxElement {
	
	public FmecCompound(de.darwinspl.fmec.resource.fmec.grammar.FmecChoice choice, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality cardinality) {
		super(cardinality, new de.darwinspl.fmec.resource.fmec.grammar.FmecSyntaxElement[] {choice});
	}
	
	public String toString() {
		return "(" + getChildren()[0] + ")";
	}
	
}
