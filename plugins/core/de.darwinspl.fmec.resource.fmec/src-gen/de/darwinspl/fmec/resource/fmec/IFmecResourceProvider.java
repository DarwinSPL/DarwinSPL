/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec;


/**
 * Implementors of this interface provide an EMF resource.
 */
public interface IFmecResourceProvider {
	
	/**
	 * Returns the resource.
	 */
	public de.darwinspl.fmec.resource.fmec.IFmecTextResource getResource();
	
}
