/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.debug;


/**
 * A DebuggableInterpreter is a facade for interpreters that adds debug support.
 */
public class FmecDebuggableInterpreter {
	// The generator for this class is currently disabled by option
	// 'disableDebugSupport' in the .cs file.
}
