/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.mopp;

import org.antlr.runtime3_4_0.ANTLRStringStream;
import org.antlr.runtime3_4_0.Lexer;
import org.antlr.runtime3_4_0.Token;

public class FmecAntlrScanner implements de.darwinspl.fmec.resource.fmec.IFmecTextScanner {
	
	private Lexer antlrLexer;
	
	public FmecAntlrScanner(Lexer antlrLexer) {
		this.antlrLexer = antlrLexer;
	}
	
	public de.darwinspl.fmec.resource.fmec.IFmecTextToken getNextToken() {
		if (antlrLexer.getCharStream() == null) {
			return null;
		}
		final Token current = antlrLexer.nextToken();
		if (current == null || current.getType() < 0) {
			return null;
		}
		de.darwinspl.fmec.resource.fmec.IFmecTextToken result = new de.darwinspl.fmec.resource.fmec.mopp.FmecANTLRTextToken(current);
		return result;
	}
	
	public void setText(String text) {
		antlrLexer.setCharStream(new ANTLRStringStream(text));
	}
	
}
