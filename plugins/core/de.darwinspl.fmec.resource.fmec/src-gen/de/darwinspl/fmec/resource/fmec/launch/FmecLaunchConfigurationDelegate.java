/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.launch;


/**
 * A class that handles launch configurations.
 */
public class FmecLaunchConfigurationDelegate {
	// The generator for this class is currently disabled by option
	// 'disableLaunchSupport' in the .cs file.
}
