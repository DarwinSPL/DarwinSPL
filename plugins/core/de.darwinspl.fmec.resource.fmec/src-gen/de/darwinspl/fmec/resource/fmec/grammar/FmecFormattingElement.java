/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.grammar;


public abstract class FmecFormattingElement extends de.darwinspl.fmec.resource.fmec.grammar.FmecSyntaxElement {
	
	public FmecFormattingElement(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality cardinality) {
		super(cardinality, null);
	}
	
}
