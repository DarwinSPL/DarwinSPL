/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.grammar;

import java.lang.reflect.Field;
import java.util.LinkedHashSet;
import java.util.Set;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcoreFactory;

public class FmecGrammarInformationProvider {
	
	public final static EStructuralFeature ANONYMOUS_FEATURE = EcoreFactory.eINSTANCE.createEAttribute();
	static {
		ANONYMOUS_FEATURE.setName("_");
	}
	
	public final static FmecGrammarInformationProvider INSTANCE = new FmecGrammarInformationProvider();
	
	private Set<String> keywords;
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_0_0_0_0 = INSTANCE.getFMEC_0_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_0_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("feature", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_0_0_0_1 = INSTANCE.getFMEC_0_0_0_1();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_0_0_0_1() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("model", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder FMEC_0_0_0_2 = INSTANCE.getFMEC_0_0_0_2();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder getFMEC_0_0_0_2() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder(de.darwinspl.fmec.FmecPackage.eINSTANCE.getFeatureModelEvolutionConstraintModel().getEStructuralFeature(de.darwinspl.fmec.FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__FEATURE_MODEL), "PATH_TO_FILE", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecLineBreak FMEC_0_0_0_3 = INSTANCE.getFMEC_0_0_0_3();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecLineBreak getFMEC_0_0_0_3() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecLineBreak(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecLineBreak FMEC_0_0_0_4 = INSTANCE.getFMEC_0_0_0_4();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecLineBreak getFMEC_0_0_0_4() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecLineBreak(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecContainment FMEC_0_0_0_5_0_0_0 = INSTANCE.getFMEC_0_0_0_5_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecContainment getFMEC_0_0_0_5_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecContainment(de.darwinspl.fmec.FmecPackage.eINSTANCE.getFeatureModelEvolutionConstraintModel().getEStructuralFeature(de.darwinspl.fmec.FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__CONSTRAINTS), de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, new EClass[] {de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getFeatureModelEvolutionConstraint(), }, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_0_0_0_5_0_0_1 = INSTANCE.getFMEC_0_0_0_5_0_0_1();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_0_0_0_5_0_0_1() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword(";", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecLineBreak FMEC_0_0_0_5_0_0_2 = INSTANCE.getFMEC_0_0_0_5_0_0_2();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecLineBreak getFMEC_0_0_0_5_0_0_2() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecLineBreak(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecLineBreak FMEC_0_0_0_5_0_0_3 = INSTANCE.getFMEC_0_0_0_5_0_0_3();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecLineBreak getFMEC_0_0_0_5_0_0_3() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecLineBreak(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_0_0_0_5_0_0 = INSTANCE.getFMEC_0_0_0_5_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_0_0_0_5_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_0_0_0_5_0_0_0, FMEC_0_0_0_5_0_0_1, FMEC_0_0_0_5_0_0_2, FMEC_0_0_0_5_0_0_3);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_0_0_0_5_0 = INSTANCE.getFMEC_0_0_0_5_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_0_0_0_5_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_0_0_0_5_0_0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecCompound FMEC_0_0_0_5 = INSTANCE.getFMEC_0_0_0_5();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecCompound getFMEC_0_0_0_5() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecCompound(FMEC_0_0_0_5_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.STAR);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_0_0_0 = INSTANCE.getFMEC_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_0_0_0_0, FMEC_0_0_0_1, FMEC_0_0_0_2, FMEC_0_0_0_3, FMEC_0_0_0_4, FMEC_0_0_0_5);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_0_0 = INSTANCE.getFMEC_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_0_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * FeatureModelEvolutionConstraintModel
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_0 = INSTANCE.getFMEC_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.FmecPackage.eINSTANCE.getFeatureModelEvolutionConstraintModel(), FMEC_0_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecContainment FMEC_1_0_0_0 = INSTANCE.getFMEC_1_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecContainment getFMEC_1_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecContainment(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getBinaryConstraint().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.BINARY_CONSTRAINT__LEFT_CHILD), de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, new EClass[] {de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getBinaryConstraintChild(), }, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecLineBreak FMEC_1_0_0_1 = INSTANCE.getFMEC_1_0_0_1();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecLineBreak getFMEC_1_0_0_1() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecLineBreak(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecContainment FMEC_1_0_0_2 = INSTANCE.getFMEC_1_0_0_2();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecContainment getFMEC_1_0_0_2() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecContainment(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getBinaryConstraint().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.BINARY_CONSTRAINT__KEYWORD), de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, new EClass[] {de.darwinspl.fmec.keywords.logical.LogicalPackage.eINSTANCE.getBinaryLogicalKeyword(), }, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecLineBreak FMEC_1_0_0_3 = INSTANCE.getFMEC_1_0_0_3();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecLineBreak getFMEC_1_0_0_3() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecLineBreak(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecContainment FMEC_1_0_0_4 = INSTANCE.getFMEC_1_0_0_4();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecContainment getFMEC_1_0_0_4() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecContainment(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getBinaryConstraint().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.BINARY_CONSTRAINT__RIGHT_CHILD), de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, new EClass[] {de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getBinaryConstraintChild(), }, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_1_0_0 = INSTANCE.getFMEC_1_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_1_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_1_0_0_0, FMEC_1_0_0_1, FMEC_1_0_0_2, FMEC_1_0_0_3, FMEC_1_0_0_4);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_1_0 = INSTANCE.getFMEC_1_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_1_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_1_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * BinaryConstraint
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_1 = INSTANCE.getFMEC_1();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_1() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getBinaryConstraint(), FMEC_1_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecContainment FMEC_2_0_0_0 = INSTANCE.getFMEC_2_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecContainment getFMEC_2_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecContainment(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getUnaryConstraint().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.UNARY_CONSTRAINT__KEYWORD), de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, new EClass[] {de.darwinspl.fmec.keywords.logical.LogicalPackage.eINSTANCE.getUnaryLogicalKeyword(), }, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecContainment FMEC_2_0_0_1 = INSTANCE.getFMEC_2_0_0_1();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecContainment getFMEC_2_0_0_1() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecContainment(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getUnaryConstraint().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.UNARY_CONSTRAINT__CHILD), de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, new EClass[] {de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getUnaryConstraintChild(), }, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_2_0_0 = INSTANCE.getFMEC_2_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_2_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_2_0_0_0, FMEC_2_0_0_1);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_2_0 = INSTANCE.getFMEC_2_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_2_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_2_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * UnaryConstraint
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_2 = INSTANCE.getFMEC_2();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_2() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getUnaryConstraint(), FMEC_2_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_3_0_0_0 = INSTANCE.getFMEC_3_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_3_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("{", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecLineBreak FMEC_3_0_0_1 = INSTANCE.getFMEC_3_0_0_1();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecLineBreak getFMEC_3_0_0_1() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecLineBreak(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 1);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecContainment FMEC_3_0_0_2 = INSTANCE.getFMEC_3_0_0_2();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecContainment getFMEC_3_0_0_2() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecContainment(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getNestedConstraint().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.NESTED_CONSTRAINT__NESTED_CONSTRAINT), de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, new EClass[] {de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getFeatureModelEvolutionConstraint(), }, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecLineBreak FMEC_3_0_0_3 = INSTANCE.getFMEC_3_0_0_3();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecLineBreak getFMEC_3_0_0_3() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecLineBreak(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_3_0_0_4 = INSTANCE.getFMEC_3_0_0_4();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_3_0_0_4() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("}", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_3_0_0 = INSTANCE.getFMEC_3_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_3_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_3_0_0_0, FMEC_3_0_0_1, FMEC_3_0_0_2, FMEC_3_0_0_3, FMEC_3_0_0_4);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_3_0 = INSTANCE.getFMEC_3_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_3_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_3_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * NestedConstraint
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_3 = INSTANCE.getFMEC_3();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_3() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getNestedConstraint(), FMEC_3_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecContainment FMEC_4_0_0_0 = INSTANCE.getFMEC_4_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecContainment getFMEC_4_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecContainment(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_POINT_CONSTRAINT__PRE_ARGUMENT), de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, new EClass[] {de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getEvolutionaryAbstractBuidingBlock(), }, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecContainment FMEC_4_0_0_1 = INSTANCE.getFMEC_4_0_0_1();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecContainment getFMEC_4_0_0_1() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecContainment(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_POINT_CONSTRAINT__KEYWORD), de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, new EClass[] {de.darwinspl.fmec.keywords.timepoint.TimepointPackage.eINSTANCE.getTimePointKeyword(), }, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecContainment FMEC_4_0_0_2 = INSTANCE.getFMEC_4_0_0_2();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecContainment getFMEC_4_0_0_2() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecContainment(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_POINT_CONSTRAINT__POST_ARGUMENT), de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, new EClass[] {de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getEvolutionaryAbstractBuidingBlock(), }, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_4_0_0 = INSTANCE.getFMEC_4_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_4_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_4_0_0_0, FMEC_4_0_0_1, FMEC_4_0_0_2);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_4_0 = INSTANCE.getFMEC_4_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_4_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_4_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * TimePointConstraint
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_4 = INSTANCE.getFMEC_4();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_4() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimePointConstraint(), FMEC_4_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecContainment FMEC_5_0_0_0 = INSTANCE.getFMEC_5_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecContainment getFMEC_5_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecContainment(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__PRE_ARGUMENT), de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, new EClass[] {de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getEvolutionaryAbstractBuidingBlock(), }, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecContainment FMEC_5_0_0_1 = INSTANCE.getFMEC_5_0_0_1();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecContainment getFMEC_5_0_0_1() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecContainment(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__KEYWORD), de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, new EClass[] {de.darwinspl.fmec.keywords.interval.IntervalPackage.eINSTANCE.getIntervalKeyword(), }, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_5_0_0_2 = INSTANCE.getFMEC_5_0_0_2();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_5_0_0_2() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("[", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecContainment FMEC_5_0_0_3 = INSTANCE.getFMEC_5_0_0_3();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecContainment getFMEC_5_0_0_3() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecContainment(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__START_POINT), de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, new EClass[] {de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimePoint(), }, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_5_0_0_4 = INSTANCE.getFMEC_5_0_0_4();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_5_0_0_4() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword(",", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecContainment FMEC_5_0_0_5 = INSTANCE.getFMEC_5_0_0_5();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecContainment getFMEC_5_0_0_5() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecContainment(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__END_POINT), de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, new EClass[] {de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimePoint(), }, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_5_0_0_6 = INSTANCE.getFMEC_5_0_0_6();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_5_0_0_6() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword(")", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_5_0_0 = INSTANCE.getFMEC_5_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_5_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_5_0_0_0, FMEC_5_0_0_1, FMEC_5_0_0_2, FMEC_5_0_0_3, FMEC_5_0_0_4, FMEC_5_0_0_5, FMEC_5_0_0_6);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_5_0 = INSTANCE.getFMEC_5_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_5_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_5_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * TimeIntervalConstraint
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_5 = INSTANCE.getFMEC_5();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_5() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.constraints.ConstraintsPackage.eINSTANCE.getTimeIntervalConstraint(), FMEC_5_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecContainment FMEC_6_0_0_0 = INSTANCE.getFMEC_6_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecContainment getFMEC_6_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecContainment(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getEvolutionaryProperty().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.EVOLUTIONARY_PROPERTY__PROPERTY), de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, new EClass[] {de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getProperty(), }, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecContainment FMEC_6_0_0_1 = INSTANCE.getFMEC_6_0_0_1();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecContainment getFMEC_6_0_0_1() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecContainment(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getEvolutionaryProperty().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.EVOLUTIONARY_PROPERTY__KEYWORD), de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, new EClass[] {de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage.eINSTANCE.getPropertyKeyword(), }, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_6_0_0 = INSTANCE.getFMEC_6_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_6_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_6_0_0_0, FMEC_6_0_0_1);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_6_0 = INSTANCE.getFMEC_6_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_6_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_6_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * EvolutionaryProperty
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_6 = INSTANCE.getFMEC_6();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_6() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getEvolutionaryProperty(), FMEC_6_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder FMEC_7_0_0_0 = INSTANCE.getFMEC_7_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder getFMEC_7_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getFeatureExistenceProperty().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_EXISTENCE_PROPERTY__REFERENCED_FEATURE), "IDENTIFIER", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_7_0_0 = INSTANCE.getFMEC_7_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_7_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_7_0_0_0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_7_0 = INSTANCE.getFMEC_7_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_7_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_7_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * FeatureExistenceProperty
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_7 = INSTANCE.getFMEC_7();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_7() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getFeatureExistenceProperty(), FMEC_7_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder FMEC_8_0_0_0 = INSTANCE.getFMEC_8_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder getFMEC_8_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getFeatureTypeProperty().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_TYPE_PROPERTY__REFERENCED_FEATURE), "IDENTIFIER", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_8_0_0_1 = INSTANCE.getFMEC_8_0_0_1();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_8_0_0_1() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword(".", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_8_0_0_2 = INSTANCE.getFMEC_8_0_0_2();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_8_0_0_2() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("type", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_8_0_0_3 = INSTANCE.getFMEC_8_0_0_3();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_8_0_0_3() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("==", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecEnumerationTerminal FMEC_8_0_0_4 = INSTANCE.getFMEC_8_0_0_4();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecEnumerationTerminal getFMEC_8_0_0_4() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecEnumerationTerminal(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getFeatureTypeProperty().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_TYPE_PROPERTY__FEATURE_TYPE), new String[] {"OPTIONAL", "OPTIONAL", "MANDATORY", "MANDATORY", }, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_8_0_0 = INSTANCE.getFMEC_8_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_8_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_8_0_0_0, FMEC_8_0_0_1, FMEC_8_0_0_2, FMEC_8_0_0_3, FMEC_8_0_0_4);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_8_0 = INSTANCE.getFMEC_8_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_8_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_8_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * FeatureTypeProperty
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_8 = INSTANCE.getFMEC_8();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_8() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getFeatureTypeProperty(), FMEC_8_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder FMEC_9_0_0_0 = INSTANCE.getFMEC_9_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder getFMEC_9_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getFeatureParentProperty().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_PARENT_PROPERTY__REFERENCED_FEATURE), "IDENTIFIER", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_9_0_0_1 = INSTANCE.getFMEC_9_0_0_1();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_9_0_0_1() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword(".", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_9_0_0_2 = INSTANCE.getFMEC_9_0_0_2();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_9_0_0_2() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("parentGroup", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_9_0_0_3 = INSTANCE.getFMEC_9_0_0_3();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_9_0_0_3() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("==", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder FMEC_9_0_0_4 = INSTANCE.getFMEC_9_0_0_4();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder getFMEC_9_0_0_4() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getFeatureParentProperty().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_PARENT_PROPERTY__REFERENCED_GROUP), "IDENTIFIER", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_9_0_0 = INSTANCE.getFMEC_9_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_9_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_9_0_0_0, FMEC_9_0_0_1, FMEC_9_0_0_2, FMEC_9_0_0_3, FMEC_9_0_0_4);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_9_0 = INSTANCE.getFMEC_9_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_9_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_9_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * FeatureParentProperty
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_9 = INSTANCE.getFMEC_9();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_9() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getFeatureParentProperty(), FMEC_9_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder FMEC_10_0_0_0 = INSTANCE.getFMEC_10_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder getFMEC_10_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getGroupExistenceProperty().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_EXISTENCE_PROPERTY__REFERENCED_GROUP), "IDENTIFIER", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_10_0_0 = INSTANCE.getFMEC_10_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_10_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_10_0_0_0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_10_0 = INSTANCE.getFMEC_10_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_10_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_10_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * GroupExistenceProperty
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_10 = INSTANCE.getFMEC_10();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_10() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getGroupExistenceProperty(), FMEC_10_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder FMEC_11_0_0_0 = INSTANCE.getFMEC_11_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder getFMEC_11_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getGroupTypeProperty().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_TYPE_PROPERTY__REFERENCED_GROUP), "IDENTIFIER", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_11_0_0_1 = INSTANCE.getFMEC_11_0_0_1();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_11_0_0_1() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword(".", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_11_0_0_2 = INSTANCE.getFMEC_11_0_0_2();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_11_0_0_2() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("type", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_11_0_0_3 = INSTANCE.getFMEC_11_0_0_3();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_11_0_0_3() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("==", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecEnumerationTerminal FMEC_11_0_0_4 = INSTANCE.getFMEC_11_0_0_4();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecEnumerationTerminal getFMEC_11_0_0_4() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecEnumerationTerminal(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getGroupTypeProperty().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_TYPE_PROPERTY__GROUP_TYPE), new String[] {"AND", "AND", "ALTERNATIVE", "ALTERNATIVE", "OR", "OR", }, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_11_0_0 = INSTANCE.getFMEC_11_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_11_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_11_0_0_0, FMEC_11_0_0_1, FMEC_11_0_0_2, FMEC_11_0_0_3, FMEC_11_0_0_4);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_11_0 = INSTANCE.getFMEC_11_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_11_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_11_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * GroupTypeProperty
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_11 = INSTANCE.getFMEC_11();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_11() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getGroupTypeProperty(), FMEC_11_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder FMEC_12_0_0_0 = INSTANCE.getFMEC_12_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder getFMEC_12_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getGroupParentProperty().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_GROUP), "IDENTIFIER", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_12_0_0_1 = INSTANCE.getFMEC_12_0_0_1();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_12_0_0_1() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword(".", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_12_0_0_2 = INSTANCE.getFMEC_12_0_0_2();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_12_0_0_2() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("parentFeature", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_12_0_0_3 = INSTANCE.getFMEC_12_0_0_3();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_12_0_0_3() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("==", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder FMEC_12_0_0_4 = INSTANCE.getFMEC_12_0_0_4();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder getFMEC_12_0_0_4() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getGroupParentProperty().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_FEATURE), "IDENTIFIER", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_12_0_0 = INSTANCE.getFMEC_12_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_12_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_12_0_0_0, FMEC_12_0_0_1, FMEC_12_0_0_2, FMEC_12_0_0_3, FMEC_12_0_0_4);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_12_0 = INSTANCE.getFMEC_12_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_12_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_12_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * GroupParentProperty
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_12 = INSTANCE.getFMEC_12();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_12() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getGroupParentProperty(), FMEC_12_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecContainment FMEC_13_0_0_0 = INSTANCE.getFMEC_13_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecContainment getFMEC_13_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecContainment(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimePoint().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_POINT__TIME_POINT), de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, new EClass[] {de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimePointWithoutOffset(), }, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecContainment FMEC_13_0_0_1 = INSTANCE.getFMEC_13_0_0_1();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecContainment getFMEC_13_0_0_1() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecContainment(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimePoint().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_POINT__OFFSET), de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.QUESTIONMARK, new EClass[] {de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimeOffset(), }, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_13_0_0 = INSTANCE.getFMEC_13_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_13_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_13_0_0_0, FMEC_13_0_0_1);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_13_0 = INSTANCE.getFMEC_13_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_13_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_13_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class TimePoint
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_13 = INSTANCE.getFMEC_13();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_13() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimePoint(), FMEC_13_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecBooleanTerminal FMEC_14_0_0_0 = INSTANCE.getFMEC_14_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecBooleanTerminal getFMEC_14_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecBooleanTerminal(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimeOffset().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__POSITIVE_SIGNATURE), "+", "-", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder FMEC_14_0_0_1_0_0_0 = INSTANCE.getFMEC_14_0_0_1_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder getFMEC_14_0_0_1_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimeOffset().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__YEARS), "INTEGER", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_14_0_0_1_0_0_1 = INSTANCE.getFMEC_14_0_0_1_0_0_1();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_14_0_0_1_0_0_1() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("years", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_14_0_0_1_0_0 = INSTANCE.getFMEC_14_0_0_1_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_14_0_0_1_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_14_0_0_1_0_0_0, FMEC_14_0_0_1_0_0_1);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_14_0_0_1_0 = INSTANCE.getFMEC_14_0_0_1_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_14_0_0_1_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_14_0_0_1_0_0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecCompound FMEC_14_0_0_1 = INSTANCE.getFMEC_14_0_0_1();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecCompound getFMEC_14_0_0_1() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecCompound(FMEC_14_0_0_1_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.QUESTIONMARK);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder FMEC_14_0_0_2_0_0_0 = INSTANCE.getFMEC_14_0_0_2_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder getFMEC_14_0_0_2_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimeOffset().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__MONTHS), "INTEGER", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_14_0_0_2_0_0_1 = INSTANCE.getFMEC_14_0_0_2_0_0_1();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_14_0_0_2_0_0_1() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("months", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_14_0_0_2_0_0 = INSTANCE.getFMEC_14_0_0_2_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_14_0_0_2_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_14_0_0_2_0_0_0, FMEC_14_0_0_2_0_0_1);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_14_0_0_2_0 = INSTANCE.getFMEC_14_0_0_2_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_14_0_0_2_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_14_0_0_2_0_0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecCompound FMEC_14_0_0_2 = INSTANCE.getFMEC_14_0_0_2();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecCompound getFMEC_14_0_0_2() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecCompound(FMEC_14_0_0_2_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.QUESTIONMARK);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder FMEC_14_0_0_3_0_0_0 = INSTANCE.getFMEC_14_0_0_3_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder getFMEC_14_0_0_3_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimeOffset().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__DAYS), "INTEGER", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_14_0_0_3_0_0_1 = INSTANCE.getFMEC_14_0_0_3_0_0_1();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_14_0_0_3_0_0_1() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("days", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_14_0_0_3_0_0 = INSTANCE.getFMEC_14_0_0_3_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_14_0_0_3_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_14_0_0_3_0_0_0, FMEC_14_0_0_3_0_0_1);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_14_0_0_3_0 = INSTANCE.getFMEC_14_0_0_3_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_14_0_0_3_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_14_0_0_3_0_0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecCompound FMEC_14_0_0_3 = INSTANCE.getFMEC_14_0_0_3();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecCompound getFMEC_14_0_0_3() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecCompound(FMEC_14_0_0_3_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.QUESTIONMARK);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder FMEC_14_0_0_4_0_0_0 = INSTANCE.getFMEC_14_0_0_4_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder getFMEC_14_0_0_4_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimeOffset().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__HOURS), "INTEGER", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_14_0_0_4_0_0_1 = INSTANCE.getFMEC_14_0_0_4_0_0_1();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_14_0_0_4_0_0_1() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("hours", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_14_0_0_4_0_0 = INSTANCE.getFMEC_14_0_0_4_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_14_0_0_4_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_14_0_0_4_0_0_0, FMEC_14_0_0_4_0_0_1);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_14_0_0_4_0 = INSTANCE.getFMEC_14_0_0_4_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_14_0_0_4_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_14_0_0_4_0_0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecCompound FMEC_14_0_0_4 = INSTANCE.getFMEC_14_0_0_4();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecCompound getFMEC_14_0_0_4() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecCompound(FMEC_14_0_0_4_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.QUESTIONMARK);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder FMEC_14_0_0_5_0_0_0 = INSTANCE.getFMEC_14_0_0_5_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder getFMEC_14_0_0_5_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimeOffset().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__MINUTES), "INTEGER", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_14_0_0_5_0_0_1 = INSTANCE.getFMEC_14_0_0_5_0_0_1();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_14_0_0_5_0_0_1() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("minutes", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_14_0_0_5_0_0 = INSTANCE.getFMEC_14_0_0_5_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_14_0_0_5_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_14_0_0_5_0_0_0, FMEC_14_0_0_5_0_0_1);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_14_0_0_5_0 = INSTANCE.getFMEC_14_0_0_5_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_14_0_0_5_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_14_0_0_5_0_0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecCompound FMEC_14_0_0_5 = INSTANCE.getFMEC_14_0_0_5();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecCompound getFMEC_14_0_0_5() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecCompound(FMEC_14_0_0_5_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.QUESTIONMARK);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_14_0_0 = INSTANCE.getFMEC_14_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_14_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_14_0_0_0, FMEC_14_0_0_1, FMEC_14_0_0_2, FMEC_14_0_0_3, FMEC_14_0_0_4, FMEC_14_0_0_5);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_14_0 = INSTANCE.getFMEC_14_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_14_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_14_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class TimeOffset
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_14 = INSTANCE.getFMEC_14();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_14() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getTimeOffset(), FMEC_14_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder FMEC_15_0_0_0 = INSTANCE.getFMEC_15_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder getFMEC_15_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getExplicitTimePoint().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.EXPLICIT_TIME_POINT__DATE), "DATE", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_15_0_0 = INSTANCE.getFMEC_15_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_15_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_15_0_0_0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_15_0 = INSTANCE.getFMEC_15_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_15_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_15_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * ExplicitTimePoint
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_15 = INSTANCE.getFMEC_15();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_15() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getExplicitTimePoint(), FMEC_15_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder FMEC_16_0_0_0 = INSTANCE.getFMEC_16_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder getFMEC_16_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecPlaceholder(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getEvolutionaryEvent().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.EVOLUTIONARY_EVENT__REFERENCED_FEATURE), "IDENTIFIER", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecContainment FMEC_16_0_0_1 = INSTANCE.getFMEC_16_0_0_1();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecContainment getFMEC_16_0_0_1() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecContainment(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getEvolutionaryEvent().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.EVOLUTIONARY_EVENT__KEYWORD), de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, new EClass[] {de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage.eINSTANCE.getEventKeyword(), }, 0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_16_0_0 = INSTANCE.getFMEC_16_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_16_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_16_0_0_0, FMEC_16_0_0_1);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_16_0 = INSTANCE.getFMEC_16_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_16_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_16_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * EvolutionaryEvent
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_16 = INSTANCE.getFMEC_16();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_16() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.timepoint.TimepointPackage.eINSTANCE.getEvolutionaryEvent(), FMEC_16_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_17_0_0_0 = INSTANCE.getFMEC_17_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_17_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("and", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_17_0_0 = INSTANCE.getFMEC_17_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_17_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_17_0_0_0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_17_0 = INSTANCE.getFMEC_17_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_17_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_17_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class KeywordAnd
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_17 = INSTANCE.getFMEC_17();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_17() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.keywords.logical.LogicalPackage.eINSTANCE.getKeywordAnd(), FMEC_17_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_18_0_0_0 = INSTANCE.getFMEC_18_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_18_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("or", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_18_0_0 = INSTANCE.getFMEC_18_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_18_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_18_0_0_0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_18_0 = INSTANCE.getFMEC_18_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_18_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_18_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class KeywordOr
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_18 = INSTANCE.getFMEC_18();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_18() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.keywords.logical.LogicalPackage.eINSTANCE.getKeywordOr(), FMEC_18_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_19_0_0_0 = INSTANCE.getFMEC_19_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_19_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("implies", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_19_0_0 = INSTANCE.getFMEC_19_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_19_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_19_0_0_0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_19_0 = INSTANCE.getFMEC_19_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_19_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_19_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * KeywordImplies
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_19 = INSTANCE.getFMEC_19();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_19() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.keywords.logical.LogicalPackage.eINSTANCE.getKeywordImplies(), FMEC_19_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_20_0_0_0 = INSTANCE.getFMEC_20_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_20_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("not", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_20_0_0 = INSTANCE.getFMEC_20_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_20_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_20_0_0_0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_20_0 = INSTANCE.getFMEC_20_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_20_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_20_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class KeywordNot
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_20 = INSTANCE.getFMEC_20();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_20() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.keywords.logical.LogicalPackage.eINSTANCE.getKeywordNot(), FMEC_20_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_21_0_0_0 = INSTANCE.getFMEC_21_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_21_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("starts", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_21_0_0 = INSTANCE.getFMEC_21_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_21_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_21_0_0_0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_21_0 = INSTANCE.getFMEC_21_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_21_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_21_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * KeywordStarts
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_21 = INSTANCE.getFMEC_21();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_21() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage.eINSTANCE.getKeywordStarts(), FMEC_21_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_22_0_0_0 = INSTANCE.getFMEC_22_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_22_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("ends", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_22_0_0 = INSTANCE.getFMEC_22_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_22_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_22_0_0_0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_22_0 = INSTANCE.getFMEC_22_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_22_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_22_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class KeywordStops
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_22 = INSTANCE.getFMEC_22();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_22() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage.eINSTANCE.getKeywordStops(), FMEC_22_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_23_0_0_0 = INSTANCE.getFMEC_23_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_23_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("valid", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_23_0_0 = INSTANCE.getFMEC_23_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_23_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_23_0_0_0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_23_0 = INSTANCE.getFMEC_23_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_23_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_23_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class KeywordValid
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_23 = INSTANCE.getFMEC_23();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_23() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.keywords.buildingblocks.BuildingblocksPackage.eINSTANCE.getKeywordValid(), FMEC_23_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_24_0_0_0 = INSTANCE.getFMEC_24_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_24_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("before", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_24_0_0 = INSTANCE.getFMEC_24_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_24_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_24_0_0_0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_24_0 = INSTANCE.getFMEC_24_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_24_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_24_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * KeywordBefore
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_24 = INSTANCE.getFMEC_24();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_24() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.keywords.timepoint.TimepointPackage.eINSTANCE.getKeywordBefore(), FMEC_24_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_25_0_0_0 = INSTANCE.getFMEC_25_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_25_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("until", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_25_0_0 = INSTANCE.getFMEC_25_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_25_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_25_0_0_0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_25_0 = INSTANCE.getFMEC_25_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_25_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_25_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class KeywordUntil
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_25 = INSTANCE.getFMEC_25();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_25() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.keywords.timepoint.TimepointPackage.eINSTANCE.getKeywordUntil(), FMEC_25_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_26_0_0_0 = INSTANCE.getFMEC_26_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_26_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("after", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_26_0_0 = INSTANCE.getFMEC_26_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_26_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_26_0_0_0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_26_0 = INSTANCE.getFMEC_26_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_26_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_26_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class KeywordAfter
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_26 = INSTANCE.getFMEC_26();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_26() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.keywords.timepoint.TimepointPackage.eINSTANCE.getKeywordAfter(), FMEC_26_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_27_0_0_0 = INSTANCE.getFMEC_27_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_27_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("when", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_27_0_0 = INSTANCE.getFMEC_27_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_27_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_27_0_0_0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_27_0 = INSTANCE.getFMEC_27_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_27_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_27_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class KeywordWhen
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_27 = INSTANCE.getFMEC_27();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_27() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.keywords.timepoint.TimepointPackage.eINSTANCE.getKeywordWhen(), FMEC_27_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_28_0_0_0 = INSTANCE.getFMEC_28_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_28_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("at", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_28_0_0 = INSTANCE.getFMEC_28_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_28_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_28_0_0_0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_28_0 = INSTANCE.getFMEC_28_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_28_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_28_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class KeywordAt
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_28 = INSTANCE.getFMEC_28();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_28() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.keywords.timepoint.TimepointPackage.eINSTANCE.getKeywordAt(), FMEC_28_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword FMEC_29_0_0_0 = INSTANCE.getFMEC_29_0_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword getFMEC_29_0_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword("during", de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecSequence FMEC_29_0_0 = INSTANCE.getFMEC_29_0_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecSequence getFMEC_29_0_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecSequence(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_29_0_0_0);
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecChoice FMEC_29_0 = INSTANCE.getFMEC_29_0();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getFMEC_29_0() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE, FMEC_29_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * KeywordDuring
	 */
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule FMEC_29 = INSTANCE.getFMEC_29();
	private de.darwinspl.fmec.resource.fmec.grammar.FmecRule getFMEC_29() {
		return new de.darwinspl.fmec.resource.fmec.grammar.FmecRule(de.darwinspl.fmec.keywords.interval.IntervalPackage.eINSTANCE.getKeywordDuring(), FMEC_29_0, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality.ONE);
	}
	
	
	public static String getSyntaxElementID(de.darwinspl.fmec.resource.fmec.grammar.FmecSyntaxElement syntaxElement) {
		if (syntaxElement == null) {
			// null indicates EOF
			return "<EOF>";
		}
		for (Field field : de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.class.getFields()) {
			Object fieldValue;
			try {
				fieldValue = field.get(null);
				if (fieldValue == syntaxElement) {
					String id = field.getName();
					return id;
				}
			} catch (Exception e) { }
		}
		return null;
	}
	
	public static de.darwinspl.fmec.resource.fmec.grammar.FmecSyntaxElement getSyntaxElementByID(String syntaxElementID) {
		try {
			return (de.darwinspl.fmec.resource.fmec.grammar.FmecSyntaxElement) de.darwinspl.fmec.resource.fmec.grammar.FmecGrammarInformationProvider.class.getField(syntaxElementID).get(null);
		} catch (Exception e) {
			return null;
		}
	}
	
	public final static de.darwinspl.fmec.resource.fmec.grammar.FmecRule[] RULES = new de.darwinspl.fmec.resource.fmec.grammar.FmecRule[] {
		FMEC_0,
		FMEC_1,
		FMEC_2,
		FMEC_3,
		FMEC_4,
		FMEC_5,
		FMEC_6,
		FMEC_7,
		FMEC_8,
		FMEC_9,
		FMEC_10,
		FMEC_11,
		FMEC_12,
		FMEC_13,
		FMEC_14,
		FMEC_15,
		FMEC_16,
		FMEC_17,
		FMEC_18,
		FMEC_19,
		FMEC_20,
		FMEC_21,
		FMEC_22,
		FMEC_23,
		FMEC_24,
		FMEC_25,
		FMEC_26,
		FMEC_27,
		FMEC_28,
		FMEC_29,
	};
	
	/**
	 * Returns all keywords of the grammar. This includes all literals for boolean and
	 * enumeration terminals.
	 */
	public Set<String> getKeywords() {
		if (this.keywords == null) {
			this.keywords = new LinkedHashSet<String>();
			for (de.darwinspl.fmec.resource.fmec.grammar.FmecRule rule : RULES) {
				findKeywords(rule, this.keywords);
			}
		}
		return keywords;
	}
	
	/**
	 * Finds all keywords in the given element and its children and adds them to the
	 * set. This includes all literals for boolean and enumeration terminals.
	 */
	private void findKeywords(de.darwinspl.fmec.resource.fmec.grammar.FmecSyntaxElement element, Set<String> keywords) {
		if (element instanceof de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword) {
			keywords.add(((de.darwinspl.fmec.resource.fmec.grammar.FmecKeyword) element).getValue());
		} else if (element instanceof de.darwinspl.fmec.resource.fmec.grammar.FmecBooleanTerminal) {
			keywords.add(((de.darwinspl.fmec.resource.fmec.grammar.FmecBooleanTerminal) element).getTrueLiteral());
			keywords.add(((de.darwinspl.fmec.resource.fmec.grammar.FmecBooleanTerminal) element).getFalseLiteral());
		} else if (element instanceof de.darwinspl.fmec.resource.fmec.grammar.FmecEnumerationTerminal) {
			de.darwinspl.fmec.resource.fmec.grammar.FmecEnumerationTerminal terminal = (de.darwinspl.fmec.resource.fmec.grammar.FmecEnumerationTerminal) element;
			for (String key : terminal.getLiteralMapping().keySet()) {
				keywords.add(key);
			}
		}
		for (de.darwinspl.fmec.resource.fmec.grammar.FmecSyntaxElement child : element.getChildren()) {
			findKeywords(child, this.keywords);
		}
	}
	
}
