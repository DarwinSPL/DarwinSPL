/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.mopp;


public class FmecResourcePostProcessor implements de.darwinspl.fmec.resource.fmec.IFmecResourcePostProcessor {
	
	public void process(de.darwinspl.fmec.resource.fmec.mopp.FmecResource resource) {
		// Set the overrideResourcePostProcessor option to false to customize resource
		// post processing.
	}
	
	public void terminate() {
		// To signal termination to the process() method, setting a boolean field is
		// recommended. Depending on the value of this field process() can stop its
		// computation. However, this is only required for computation intensive
		// post-processors.
	}
	
}
