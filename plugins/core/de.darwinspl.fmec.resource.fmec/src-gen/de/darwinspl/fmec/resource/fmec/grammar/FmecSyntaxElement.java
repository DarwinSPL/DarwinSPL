/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.grammar;

import org.eclipse.emf.ecore.EClass;

/**
 * The abstract super class for all elements of a grammar. This class provides
 * methods to traverse the grammar rules.
 */
public abstract class FmecSyntaxElement {
	
	private FmecSyntaxElement[] children;
	private FmecSyntaxElement parent;
	private de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality cardinality;
	
	public FmecSyntaxElement(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality cardinality, FmecSyntaxElement[] children) {
		this.cardinality = cardinality;
		this.children = children;
		if (this.children != null) {
			for (FmecSyntaxElement child : this.children) {
				child.setParent(this);
			}
		}
	}
	
	/**
	 * Sets the parent of this syntax element. This method must be invoked at most
	 * once.
	 */
	public void setParent(FmecSyntaxElement parent) {
		assert this.parent == null;
		this.parent = parent;
	}
	
	/**
	 * Returns the parent of this syntax element. This parent is determined by the
	 * containment hierarchy in the CS model.
	 */
	public FmecSyntaxElement getParent() {
		return parent;
	}
	
	/**
	 * Returns the rule of this syntax element. The rule is determined by the
	 * containment hierarchy in the CS model.
	 */
	public de.darwinspl.fmec.resource.fmec.grammar.FmecRule getRule() {
		if (this instanceof de.darwinspl.fmec.resource.fmec.grammar.FmecRule) {
			return (de.darwinspl.fmec.resource.fmec.grammar.FmecRule) this;
		}
		return parent.getRule();
	}
	
	public FmecSyntaxElement[] getChildren() {
		if (children == null) {
			return new FmecSyntaxElement[0];
		}
		return children;
	}
	
	public EClass getMetaclass() {
		return parent.getMetaclass();
	}
	
	public de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality getCardinality() {
		return cardinality;
	}
	
	public boolean hasContainment(EClass metaclass) {
		de.darwinspl.fmec.resource.fmec.grammar.FmecSyntaxElement[] children = getChildren();
		for (de.darwinspl.fmec.resource.fmec.grammar.FmecSyntaxElement child : children) {
			if (child.hasContainment(metaclass)) {
				return true;
			}
		}
		return false;
	}
	
}
