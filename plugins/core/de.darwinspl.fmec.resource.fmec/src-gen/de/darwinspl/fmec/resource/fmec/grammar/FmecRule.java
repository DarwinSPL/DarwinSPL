/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.grammar;

import org.eclipse.emf.ecore.EClass;

/**
 * A class to represent a rules in the grammar.
 */
public class FmecRule extends de.darwinspl.fmec.resource.fmec.grammar.FmecSyntaxElement {
	
	private final EClass metaclass;
	
	public FmecRule(EClass metaclass, de.darwinspl.fmec.resource.fmec.grammar.FmecChoice choice, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality cardinality) {
		super(cardinality, new de.darwinspl.fmec.resource.fmec.grammar.FmecSyntaxElement[] {choice});
		this.metaclass = metaclass;
	}
	
	public EClass getMetaclass() {
		return metaclass;
	}
	
	public de.darwinspl.fmec.resource.fmec.grammar.FmecChoice getDefinition() {
		return (de.darwinspl.fmec.resource.fmec.grammar.FmecChoice) getChildren()[0];
	}
	
	@Deprecated
	public String toString() {
		return metaclass == null ? "null" : metaclass.getName() + " ::= " + getDefinition().toString();
	}
	
}

