/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.mopp;

import org.eclipse.emf.common.util.URI;

/**
 * <p>
 * A basic implementation of the de.darwinspl.fmec.resource.fmec.IFmecURIMapping
 * interface that can map identifiers to URIs.
 * </p>
 * 
 * @param <ReferenceType> unused type parameter which is needed to implement
 * de.darwinspl.fmec.resource.fmec.IFmecURIMapping.
 */
public class FmecURIMapping<ReferenceType> implements de.darwinspl.fmec.resource.fmec.IFmecURIMapping<ReferenceType> {
	
	private URI uri;
	private String identifier;
	private String warning;
	
	public FmecURIMapping(String identifier, URI newIdentifier, String warning) {
		super();
		this.uri = newIdentifier;
		this.identifier = identifier;
		this.warning = warning;
	}
	
	public URI getTargetIdentifier() {
		return uri;
	}
	
	public String getIdentifier() {
		return identifier;
	}
	
	public String getWarning() {
		return warning;
	}
	
}
