/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.mopp;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;

public class FmecReferenceResolverSwitch implements de.darwinspl.fmec.resource.fmec.IFmecReferenceResolverSwitch {
	
	/**
	 * This map stores a copy of the options the were set for loading the resource.
	 */
	private Map<Object, Object> options;
	
	protected de.darwinspl.fmec.resource.fmec.analysis.FeatureModelEvolutionConstraintModelFeatureModelReferenceResolver featureModelEvolutionConstraintModelFeatureModelReferenceResolver = new de.darwinspl.fmec.resource.fmec.analysis.FeatureModelEvolutionConstraintModelFeatureModelReferenceResolver();
	protected de.darwinspl.fmec.resource.fmec.analysis.FeatureReferenceReferencedFeatureReferenceResolver featureReferenceReferencedFeatureReferenceResolver = new de.darwinspl.fmec.resource.fmec.analysis.FeatureReferenceReferencedFeatureReferenceResolver();
	protected de.darwinspl.fmec.resource.fmec.analysis.GroupReferenceReferencedGroupReferenceResolver groupReferenceReferencedGroupReferenceResolver = new de.darwinspl.fmec.resource.fmec.analysis.GroupReferenceReferencedGroupReferenceResolver();
	
	public de.darwinspl.fmec.resource.fmec.IFmecReferenceResolver<de.darwinspl.fmec.FeatureModelEvolutionConstraintModel, de.darwinspl.feature.DwTemporalFeatureModel> getFeatureModelEvolutionConstraintModelFeatureModelReferenceResolver() {
		return getResolverChain(de.darwinspl.fmec.FmecPackage.eINSTANCE.getFeatureModelEvolutionConstraintModel_FeatureModel(), featureModelEvolutionConstraintModelFeatureModelReferenceResolver);
	}
	
	public de.darwinspl.fmec.resource.fmec.IFmecReferenceResolver<de.darwinspl.fmec.property.FeatureReference, de.darwinspl.feature.DwFeature> getFeatureReferenceReferencedFeatureReferenceResolver() {
		return getResolverChain(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getFeatureReference_ReferencedFeature(), featureReferenceReferencedFeatureReferenceResolver);
	}
	
	public de.darwinspl.fmec.resource.fmec.IFmecReferenceResolver<de.darwinspl.fmec.property.GroupReference, de.darwinspl.feature.DwGroup> getGroupReferenceReferencedGroupReferenceResolver() {
		return getResolverChain(de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getGroupReference_ReferencedGroup(), groupReferenceReferencedGroupReferenceResolver);
	}
	
	public void setOptions(Map<?, ?> options) {
		if (options != null) {
			this.options = new LinkedHashMap<Object, Object>();
			this.options.putAll(options);
		}
		featureModelEvolutionConstraintModelFeatureModelReferenceResolver.setOptions(options);
		featureReferenceReferencedFeatureReferenceResolver.setOptions(options);
		groupReferenceReferencedGroupReferenceResolver.setOptions(options);
	}
	
	public void resolveFuzzy(String identifier, EObject container, EReference reference, int position, de.darwinspl.fmec.resource.fmec.IFmecReferenceResolveResult<EObject> result) {
		if (container == null) {
			return;
		}
		if (de.darwinspl.fmec.FmecPackage.eINSTANCE.getFeatureModelEvolutionConstraintModel().isInstance(container)) {
			FmecFuzzyResolveResult<de.darwinspl.feature.DwTemporalFeatureModel> frr = new FmecFuzzyResolveResult<de.darwinspl.feature.DwTemporalFeatureModel>(result);
			String referenceName = reference.getName();
			EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof EReference && referenceName != null && referenceName.equals("featureModel")) {
				featureModelEvolutionConstraintModelFeatureModelReferenceResolver.resolve(identifier, (de.darwinspl.fmec.FeatureModelEvolutionConstraintModel) container, (EReference) feature, position, true, frr);
			}
		}
		if (de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getFeatureReference().isInstance(container)) {
			FmecFuzzyResolveResult<de.darwinspl.feature.DwFeature> frr = new FmecFuzzyResolveResult<de.darwinspl.feature.DwFeature>(result);
			String referenceName = reference.getName();
			EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof EReference && referenceName != null && referenceName.equals("referencedFeature")) {
				featureReferenceReferencedFeatureReferenceResolver.resolve(identifier, (de.darwinspl.fmec.property.FeatureReference) container, (EReference) feature, position, true, frr);
			}
		}
		if (de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getGroupReference().isInstance(container)) {
			FmecFuzzyResolveResult<de.darwinspl.feature.DwGroup> frr = new FmecFuzzyResolveResult<de.darwinspl.feature.DwGroup>(result);
			String referenceName = reference.getName();
			EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof EReference && referenceName != null && referenceName.equals("referencedGroup")) {
				groupReferenceReferencedGroupReferenceResolver.resolve(identifier, (de.darwinspl.fmec.property.GroupReference) container, (EReference) feature, position, true, frr);
			}
		}
	}
	
	public de.darwinspl.fmec.resource.fmec.IFmecReferenceResolver<? extends EObject, ? extends EObject> getResolver(EStructuralFeature reference) {
		if (reference == de.darwinspl.fmec.FmecPackage.eINSTANCE.getFeatureModelEvolutionConstraintModel_FeatureModel()) {
			return getResolverChain(reference, featureModelEvolutionConstraintModelFeatureModelReferenceResolver);
		}
		if (reference == de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getFeatureReference_ReferencedFeature()) {
			return getResolverChain(reference, featureReferenceReferencedFeatureReferenceResolver);
		}
		if (reference == de.darwinspl.fmec.property.PropertyPackage.eINSTANCE.getGroupReference_ReferencedGroup()) {
			return getResolverChain(reference, groupReferenceReferencedGroupReferenceResolver);
		}
		return null;
	}
	
	@SuppressWarnings({"rawtypes", "unchecked"})
	public <ContainerType extends EObject, ReferenceType extends EObject> de.darwinspl.fmec.resource.fmec.IFmecReferenceResolver<ContainerType, ReferenceType> getResolverChain(EStructuralFeature reference, de.darwinspl.fmec.resource.fmec.IFmecReferenceResolver<ContainerType, ReferenceType> originalResolver) {
		if (options == null) {
			return originalResolver;
		}
		Object value = options.get(de.darwinspl.fmec.resource.fmec.IFmecOptions.ADDITIONAL_REFERENCE_RESOLVERS);
		if (value == null) {
			return originalResolver;
		}
		if (!(value instanceof Map)) {
			// send this to the error log
			new de.darwinspl.fmec.resource.fmec.util.FmecRuntimeUtil().logWarning("Found value with invalid type for option " + de.darwinspl.fmec.resource.fmec.IFmecOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + Map.class.getName() + ", but was " + value.getClass().getName() + ")", null);
			return originalResolver;
		}
		Map<?,?> resolverMap = (Map<?,?>) value;
		Object resolverValue = resolverMap.get(reference);
		if (resolverValue == null) {
			return originalResolver;
		}
		if (resolverValue instanceof de.darwinspl.fmec.resource.fmec.IFmecReferenceResolver) {
			de.darwinspl.fmec.resource.fmec.IFmecReferenceResolver replacingResolver = (de.darwinspl.fmec.resource.fmec.IFmecReferenceResolver) resolverValue;
			if (replacingResolver instanceof de.darwinspl.fmec.resource.fmec.IFmecDelegatingReferenceResolver) {
				// pass original resolver to the replacing one
				((de.darwinspl.fmec.resource.fmec.IFmecDelegatingReferenceResolver) replacingResolver).setDelegate(originalResolver);
			}
			return replacingResolver;
		} else if (resolverValue instanceof Collection) {
			Collection replacingResolvers = (Collection) resolverValue;
			de.darwinspl.fmec.resource.fmec.IFmecReferenceResolver replacingResolver = originalResolver;
			for (Object next : replacingResolvers) {
				if (next instanceof de.darwinspl.fmec.resource.fmec.IFmecReferenceCache) {
					de.darwinspl.fmec.resource.fmec.IFmecReferenceResolver nextResolver = (de.darwinspl.fmec.resource.fmec.IFmecReferenceResolver) next;
					if (nextResolver instanceof de.darwinspl.fmec.resource.fmec.IFmecDelegatingReferenceResolver) {
						// pass original resolver to the replacing one
						((de.darwinspl.fmec.resource.fmec.IFmecDelegatingReferenceResolver) nextResolver).setDelegate(replacingResolver);
					}
					replacingResolver = nextResolver;
				} else {
					// The collection contains a non-resolver. Send a warning to the error log.
					new de.darwinspl.fmec.resource.fmec.util.FmecRuntimeUtil().logWarning("Found value with invalid type in value map for option " + de.darwinspl.fmec.resource.fmec.IFmecOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + de.darwinspl.fmec.resource.fmec.IFmecDelegatingReferenceResolver.class.getName() + ", but was " + next.getClass().getName() + ")", null);
				}
			}
			return replacingResolver;
		} else {
			// The value for the option ADDITIONAL_REFERENCE_RESOLVERS has an unknown type.
			new de.darwinspl.fmec.resource.fmec.util.FmecRuntimeUtil().logWarning("Found value with invalid type in value map for option " + de.darwinspl.fmec.resource.fmec.IFmecOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + de.darwinspl.fmec.resource.fmec.IFmecDelegatingReferenceResolver.class.getName() + ", but was " + resolverValue.getClass().getName() + ")", null);
			return originalResolver;
		}
	}
	
}
