/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.mopp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import org.eclipse.emf.common.util.URI;

/**
 * <p>
 * A basic implementation of the
 * de.darwinspl.fmec.resource.fmec.IFmecReferenceResolveResult interface that
 * collects mappings in a list.
 * </p>
 * 
 * @param <ReferenceType> the type of the references that can be contained in this
 * result
 */
public class FmecReferenceResolveResult<ReferenceType> implements de.darwinspl.fmec.resource.fmec.IFmecReferenceResolveResult<ReferenceType> {
	
	private Collection<de.darwinspl.fmec.resource.fmec.IFmecReferenceMapping<ReferenceType>> mappings;
	private String errorMessage;
	private boolean resolveFuzzy;
	private Set<de.darwinspl.fmec.resource.fmec.IFmecQuickFix> quickFixes;
	
	public FmecReferenceResolveResult(boolean resolveFuzzy) {
		super();
		this.resolveFuzzy = resolveFuzzy;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public Collection<de.darwinspl.fmec.resource.fmec.IFmecQuickFix> getQuickFixes() {
		if (quickFixes == null) {
			quickFixes = new LinkedHashSet<de.darwinspl.fmec.resource.fmec.IFmecQuickFix>();
		}
		return Collections.unmodifiableSet(quickFixes);
	}
	
	public void addQuickFix(de.darwinspl.fmec.resource.fmec.IFmecQuickFix quickFix) {
		if (quickFixes == null) {
			quickFixes = new LinkedHashSet<de.darwinspl.fmec.resource.fmec.IFmecQuickFix>();
		}
		quickFixes.add(quickFix);
	}
	
	public Collection<de.darwinspl.fmec.resource.fmec.IFmecReferenceMapping<ReferenceType>> getMappings() {
		return mappings;
	}
	
	public boolean wasResolved() {
		return mappings != null;
	}
	
	public boolean wasResolvedMultiple() {
		return mappings != null && mappings.size() > 1;
	}
	
	public boolean wasResolvedUniquely() {
		return mappings != null && mappings.size() == 1;
	}
	
	public void setErrorMessage(String message) {
		errorMessage = message;
	}
	
	public void addMapping(String identifier, ReferenceType target) {
		if (!resolveFuzzy && target == null) {
			throw new IllegalArgumentException("Mapping references to null is only allowed for fuzzy resolution.");
		}
		addMapping(identifier, target, null);
	}
	
	public void addMapping(String identifier, ReferenceType target, String warning) {
		if (mappings == null) {
			mappings = new ArrayList<de.darwinspl.fmec.resource.fmec.IFmecReferenceMapping<ReferenceType>>(1);
		}
		mappings.add(new de.darwinspl.fmec.resource.fmec.mopp.FmecElementMapping<ReferenceType>(identifier, target, warning));
		errorMessage = null;
	}
	
	public void addMapping(String identifier, URI uri) {
		addMapping(identifier, uri, null);
	}
	
	public void addMapping(String identifier, URI uri, String warning) {
		if (mappings == null) {
			mappings = new ArrayList<de.darwinspl.fmec.resource.fmec.IFmecReferenceMapping<ReferenceType>>(1);
		}
		mappings.add(new de.darwinspl.fmec.resource.fmec.mopp.FmecURIMapping<ReferenceType>(identifier, uri, warning));
	}
}
