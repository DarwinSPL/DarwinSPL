/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec;

import java.util.Collection;

public interface IFmecProblem {
	public String getMessage();
	public de.darwinspl.fmec.resource.fmec.FmecEProblemSeverity getSeverity();
	public de.darwinspl.fmec.resource.fmec.FmecEProblemType getType();
	public Collection<de.darwinspl.fmec.resource.fmec.IFmecQuickFix> getQuickFixes();
}
