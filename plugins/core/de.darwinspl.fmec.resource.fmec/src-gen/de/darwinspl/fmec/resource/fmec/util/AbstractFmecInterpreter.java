/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.util;

import de.darwinspl.fmec.FeatureModelEvolutionConstraintModel;
import de.darwinspl.fmec.constraints.AtomicConstraint;
import de.darwinspl.fmec.constraints.BinaryConstraint;
import de.darwinspl.fmec.constraints.BinaryConstraintChild;
import de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock;
import de.darwinspl.fmec.constraints.FeatureModelEvolutionConstraint;
import de.darwinspl.fmec.constraints.NestedConstraint;
import de.darwinspl.fmec.constraints.TimeIntervalConstraint;
import de.darwinspl.fmec.constraints.TimePointConstraint;
import de.darwinspl.fmec.constraints.UnaryConstraint;
import de.darwinspl.fmec.constraints.UnaryConstraintChild;
import de.darwinspl.fmec.keywords.buildingblocks.EventKeyword;
import de.darwinspl.fmec.keywords.buildingblocks.KeywordStarts;
import de.darwinspl.fmec.keywords.buildingblocks.KeywordStops;
import de.darwinspl.fmec.keywords.buildingblocks.KeywordValid;
import de.darwinspl.fmec.keywords.buildingblocks.PropertyKeyword;
import de.darwinspl.fmec.keywords.interval.IntervalKeyword;
import de.darwinspl.fmec.keywords.interval.KeywordDuring;
import de.darwinspl.fmec.keywords.logical.BinaryLogicalKeyword;
import de.darwinspl.fmec.keywords.logical.KeywordAnd;
import de.darwinspl.fmec.keywords.logical.KeywordImplies;
import de.darwinspl.fmec.keywords.logical.KeywordNot;
import de.darwinspl.fmec.keywords.logical.KeywordOr;
import de.darwinspl.fmec.keywords.logical.LogicalKeyword;
import de.darwinspl.fmec.keywords.logical.UnaryLogicalKeyword;
import de.darwinspl.fmec.keywords.timepoint.KeywordAfter;
import de.darwinspl.fmec.keywords.timepoint.KeywordAt;
import de.darwinspl.fmec.keywords.timepoint.KeywordBefore;
import de.darwinspl.fmec.keywords.timepoint.KeywordUntil;
import de.darwinspl.fmec.keywords.timepoint.KeywordWhen;
import de.darwinspl.fmec.keywords.timepoint.TimePointKeyword;
import de.darwinspl.fmec.property.EvolutionaryProperty;
import de.darwinspl.fmec.property.FeatureExistenceProperty;
import de.darwinspl.fmec.property.FeatureParentProperty;
import de.darwinspl.fmec.property.FeatureReference;
import de.darwinspl.fmec.property.FeatureTypeProperty;
import de.darwinspl.fmec.property.GroupExistenceProperty;
import de.darwinspl.fmec.property.GroupParentProperty;
import de.darwinspl.fmec.property.GroupReference;
import de.darwinspl.fmec.property.GroupTypeProperty;
import de.darwinspl.fmec.property.Property;
import de.darwinspl.fmec.timepoint.EvolutionaryEvent;
import de.darwinspl.fmec.timepoint.ExplicitTimePoint;
import de.darwinspl.fmec.timepoint.TimeOffset;
import de.darwinspl.fmec.timepoint.TimePoint;
import de.darwinspl.fmec.timepoint.TimePointWithoutOffset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EmptyStackException;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;
import org.eclipse.emf.ecore.EObject;

/**
 * This class provides basic infrastructure to interpret models. To implement
 * concrete interpreters, subclass this abstract interpreter and override the
 * interprete_* methods. The interpretation can be customized by binding the two
 * type parameters (ResultType, ContextType). The former is returned by all
 * interprete_* methods, while the latter is passed from method to method while
 * traversing the model. The concrete traversal strategy can also be exchanged.
 * One can use a static traversal strategy by pushing all objects to interpret on
 * the interpretation stack (using addObjectToInterprete()) before calling
 * interprete(). Alternatively, the traversal strategy can be dynamic by pushing
 * objects on the interpretation stack during interpretation.
 */
public class AbstractFmecInterpreter<ResultType, ContextType> {
	
	private Stack<EObject> interpretationStack = new Stack<EObject>();
	private List<de.darwinspl.fmec.resource.fmec.IFmecInterpreterListener> listeners = new ArrayList<de.darwinspl.fmec.resource.fmec.IFmecInterpreterListener>();
	private EObject nextObjectToInterprete;
	private ContextType currentContext;
	
	public ResultType interprete(ContextType context) {
		ResultType result = null;
		EObject next = null;
		currentContext = context;
		while (!interpretationStack.empty()) {
			try {
				next = interpretationStack.pop();
			} catch (EmptyStackException ese) {
				// this can happen when the interpreter was terminated between the call to empty()
				// and pop()
				break;
			}
			nextObjectToInterprete = next;
			notifyListeners(next);
			result = interprete(next, context);
			if (!continueInterpretation(context, result)) {
				break;
			}
		}
		currentContext = null;
		return result;
	}
	
	/**
	 * Override this method to stop the overall interpretation depending on the result
	 * of the interpretation of a single model elements.
	 */
	public boolean continueInterpretation(ContextType context, ResultType result) {
		return true;
	}
	
	public ResultType interprete(EObject object, ContextType context) {
		ResultType result = null;
		if (object instanceof de.darwinspl.fmec.FeatureModelEvolutionConstraintModel) {
			result = interprete_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel((de.darwinspl.fmec.FeatureModelEvolutionConstraintModel) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.constraints.BinaryConstraint) {
			result = interprete_de_darwinspl_fmec_constraints_BinaryConstraint((de.darwinspl.fmec.constraints.BinaryConstraint) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.constraints.UnaryConstraint) {
			result = interprete_de_darwinspl_fmec_constraints_UnaryConstraint((de.darwinspl.fmec.constraints.UnaryConstraint) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.constraints.AtomicConstraint) {
			result = interprete_de_darwinspl_fmec_constraints_AtomicConstraint((de.darwinspl.fmec.constraints.AtomicConstraint) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.constraints.NestedConstraint) {
			result = interprete_de_darwinspl_fmec_constraints_NestedConstraint((de.darwinspl.fmec.constraints.NestedConstraint) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.constraints.TimePointConstraint) {
			result = interprete_de_darwinspl_fmec_constraints_TimePointConstraint((de.darwinspl.fmec.constraints.TimePointConstraint) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.constraints.TimeIntervalConstraint) {
			result = interprete_de_darwinspl_fmec_constraints_TimeIntervalConstraint((de.darwinspl.fmec.constraints.TimeIntervalConstraint) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.constraints.UnaryConstraintChild) {
			result = interprete_de_darwinspl_fmec_constraints_UnaryConstraintChild((de.darwinspl.fmec.constraints.UnaryConstraintChild) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.constraints.BinaryConstraintChild) {
			result = interprete_de_darwinspl_fmec_constraints_BinaryConstraintChild((de.darwinspl.fmec.constraints.BinaryConstraintChild) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.constraints.FeatureModelEvolutionConstraint) {
			result = interprete_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint((de.darwinspl.fmec.constraints.FeatureModelEvolutionConstraint) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.property.EvolutionaryProperty) {
			result = interprete_de_darwinspl_fmec_property_EvolutionaryProperty((de.darwinspl.fmec.property.EvolutionaryProperty) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock) {
			result = interprete_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock((de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.property.FeatureTypeProperty) {
			result = interprete_de_darwinspl_fmec_property_FeatureTypeProperty((de.darwinspl.fmec.property.FeatureTypeProperty) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.property.FeatureParentProperty) {
			result = interprete_de_darwinspl_fmec_property_FeatureParentProperty((de.darwinspl.fmec.property.FeatureParentProperty) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.property.GroupParentProperty) {
			result = interprete_de_darwinspl_fmec_property_GroupParentProperty((de.darwinspl.fmec.property.GroupParentProperty) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.property.FeatureReference) {
			result = interprete_de_darwinspl_fmec_property_FeatureReference((de.darwinspl.fmec.property.FeatureReference) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.property.GroupReference) {
			result = interprete_de_darwinspl_fmec_property_GroupReference((de.darwinspl.fmec.property.GroupReference) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.property.FeatureExistenceProperty) {
			result = interprete_de_darwinspl_fmec_property_FeatureExistenceProperty((de.darwinspl.fmec.property.FeatureExistenceProperty) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.property.GroupExistenceProperty) {
			result = interprete_de_darwinspl_fmec_property_GroupExistenceProperty((de.darwinspl.fmec.property.GroupExistenceProperty) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.property.GroupTypeProperty) {
			result = interprete_de_darwinspl_fmec_property_GroupTypeProperty((de.darwinspl.fmec.property.GroupTypeProperty) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.property.Property) {
			result = interprete_de_darwinspl_fmec_property_Property((de.darwinspl.fmec.property.Property) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.timepoint.TimePoint) {
			result = interprete_de_darwinspl_fmec_timepoint_TimePoint((de.darwinspl.fmec.timepoint.TimePoint) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.timepoint.ExplicitTimePoint) {
			result = interprete_de_darwinspl_fmec_timepoint_ExplicitTimePoint((de.darwinspl.fmec.timepoint.ExplicitTimePoint) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.timepoint.EvolutionaryEvent) {
			result = interprete_de_darwinspl_fmec_timepoint_EvolutionaryEvent((de.darwinspl.fmec.timepoint.EvolutionaryEvent) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.timepoint.TimePointWithoutOffset) {
			result = interprete_de_darwinspl_fmec_timepoint_TimePointWithoutOffset((de.darwinspl.fmec.timepoint.TimePointWithoutOffset) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.timepoint.TimeOffset) {
			result = interprete_de_darwinspl_fmec_timepoint_TimeOffset((de.darwinspl.fmec.timepoint.TimeOffset) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.keywords.logical.KeywordAnd) {
			result = interprete_de_darwinspl_fmec_keywords_logical_KeywordAnd((de.darwinspl.fmec.keywords.logical.KeywordAnd) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.keywords.logical.KeywordOr) {
			result = interprete_de_darwinspl_fmec_keywords_logical_KeywordOr((de.darwinspl.fmec.keywords.logical.KeywordOr) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.keywords.logical.KeywordImplies) {
			result = interprete_de_darwinspl_fmec_keywords_logical_KeywordImplies((de.darwinspl.fmec.keywords.logical.KeywordImplies) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.keywords.logical.BinaryLogicalKeyword) {
			result = interprete_de_darwinspl_fmec_keywords_logical_BinaryLogicalKeyword((de.darwinspl.fmec.keywords.logical.BinaryLogicalKeyword) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.keywords.logical.KeywordNot) {
			result = interprete_de_darwinspl_fmec_keywords_logical_KeywordNot((de.darwinspl.fmec.keywords.logical.KeywordNot) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.keywords.logical.UnaryLogicalKeyword) {
			result = interprete_de_darwinspl_fmec_keywords_logical_UnaryLogicalKeyword((de.darwinspl.fmec.keywords.logical.UnaryLogicalKeyword) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.keywords.logical.LogicalKeyword) {
			result = interprete_de_darwinspl_fmec_keywords_logical_LogicalKeyword((de.darwinspl.fmec.keywords.logical.LogicalKeyword) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.keywords.buildingblocks.KeywordStarts) {
			result = interprete_de_darwinspl_fmec_keywords_buildingblocks_KeywordStarts((de.darwinspl.fmec.keywords.buildingblocks.KeywordStarts) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.keywords.buildingblocks.KeywordStops) {
			result = interprete_de_darwinspl_fmec_keywords_buildingblocks_KeywordStops((de.darwinspl.fmec.keywords.buildingblocks.KeywordStops) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.keywords.buildingblocks.EventKeyword) {
			result = interprete_de_darwinspl_fmec_keywords_buildingblocks_EventKeyword((de.darwinspl.fmec.keywords.buildingblocks.EventKeyword) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.keywords.buildingblocks.KeywordValid) {
			result = interprete_de_darwinspl_fmec_keywords_buildingblocks_KeywordValid((de.darwinspl.fmec.keywords.buildingblocks.KeywordValid) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.keywords.buildingblocks.PropertyKeyword) {
			result = interprete_de_darwinspl_fmec_keywords_buildingblocks_PropertyKeyword((de.darwinspl.fmec.keywords.buildingblocks.PropertyKeyword) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.keywords.timepoint.KeywordBefore) {
			result = interprete_de_darwinspl_fmec_keywords_timepoint_KeywordBefore((de.darwinspl.fmec.keywords.timepoint.KeywordBefore) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.keywords.timepoint.KeywordUntil) {
			result = interprete_de_darwinspl_fmec_keywords_timepoint_KeywordUntil((de.darwinspl.fmec.keywords.timepoint.KeywordUntil) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.keywords.timepoint.KeywordAfter) {
			result = interprete_de_darwinspl_fmec_keywords_timepoint_KeywordAfter((de.darwinspl.fmec.keywords.timepoint.KeywordAfter) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.keywords.timepoint.KeywordWhen) {
			result = interprete_de_darwinspl_fmec_keywords_timepoint_KeywordWhen((de.darwinspl.fmec.keywords.timepoint.KeywordWhen) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.keywords.timepoint.KeywordAt) {
			result = interprete_de_darwinspl_fmec_keywords_timepoint_KeywordAt((de.darwinspl.fmec.keywords.timepoint.KeywordAt) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.keywords.timepoint.TimePointKeyword) {
			result = interprete_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword((de.darwinspl.fmec.keywords.timepoint.TimePointKeyword) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.keywords.interval.KeywordDuring) {
			result = interprete_de_darwinspl_fmec_keywords_interval_KeywordDuring((de.darwinspl.fmec.keywords.interval.KeywordDuring) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.fmec.keywords.interval.IntervalKeyword) {
			result = interprete_de_darwinspl_fmec_keywords_interval_IntervalKeyword((de.darwinspl.fmec.keywords.interval.IntervalKeyword) object, context);
		}
		if (result != null) {
			return result;
		}
		return result;
	}
	
	public ResultType interprete_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel(FeatureModelEvolutionConstraintModel featureModelEvolutionConstraintModel, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_constraints_FeatureModelEvolutionConstraint(FeatureModelEvolutionConstraint featureModelEvolutionConstraint, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_constraints_BinaryConstraint(BinaryConstraint binaryConstraint, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_constraints_BinaryConstraintChild(BinaryConstraintChild binaryConstraintChild, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_constraints_UnaryConstraint(UnaryConstraint unaryConstraint, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_constraints_UnaryConstraintChild(UnaryConstraintChild unaryConstraintChild, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_constraints_AtomicConstraint(AtomicConstraint atomicConstraint, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_constraints_NestedConstraint(NestedConstraint nestedConstraint, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_constraints_TimePointConstraint(TimePointConstraint timePointConstraint, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_constraints_TimeIntervalConstraint(TimeIntervalConstraint timeIntervalConstraint, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_constraints_EvolutionaryAbstractBuidingBlock(EvolutionaryAbstractBuidingBlock evolutionaryAbstractBuidingBlock, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_property_EvolutionaryProperty(EvolutionaryProperty evolutionaryProperty, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_property_FeatureReference(FeatureReference featureReference, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_property_GroupReference(GroupReference groupReference, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_property_Property(Property property, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_property_FeatureExistenceProperty(FeatureExistenceProperty featureExistenceProperty, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_property_FeatureTypeProperty(FeatureTypeProperty featureTypeProperty, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_property_FeatureParentProperty(FeatureParentProperty featureParentProperty, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_property_GroupExistenceProperty(GroupExistenceProperty groupExistenceProperty, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_property_GroupTypeProperty(GroupTypeProperty groupTypeProperty, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_property_GroupParentProperty(GroupParentProperty groupParentProperty, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_timepoint_TimePoint(TimePoint timePoint, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_timepoint_TimePointWithoutOffset(TimePointWithoutOffset timePointWithoutOffset, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_timepoint_ExplicitTimePoint(ExplicitTimePoint explicitTimePoint, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_timepoint_EvolutionaryEvent(EvolutionaryEvent evolutionaryEvent, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_timepoint_TimeOffset(TimeOffset timeOffset, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_keywords_logical_LogicalKeyword(LogicalKeyword logicalKeyword, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_keywords_logical_BinaryLogicalKeyword(BinaryLogicalKeyword binaryLogicalKeyword, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_keywords_logical_KeywordAnd(KeywordAnd keywordAnd, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_keywords_logical_KeywordOr(KeywordOr keywordOr, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_keywords_logical_KeywordImplies(KeywordImplies keywordImplies, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_keywords_logical_UnaryLogicalKeyword(UnaryLogicalKeyword unaryLogicalKeyword, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_keywords_logical_KeywordNot(KeywordNot keywordNot, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_keywords_buildingblocks_EventKeyword(EventKeyword eventKeyword, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_keywords_buildingblocks_KeywordStarts(KeywordStarts keywordStarts, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_keywords_buildingblocks_KeywordStops(KeywordStops keywordStops, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_keywords_buildingblocks_PropertyKeyword(PropertyKeyword propertyKeyword, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_keywords_buildingblocks_KeywordValid(KeywordValid keywordValid, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_keywords_timepoint_TimePointKeyword(TimePointKeyword timePointKeyword, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_keywords_timepoint_KeywordBefore(KeywordBefore keywordBefore, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_keywords_timepoint_KeywordUntil(KeywordUntil keywordUntil, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_keywords_timepoint_KeywordAfter(KeywordAfter keywordAfter, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_keywords_timepoint_KeywordWhen(KeywordWhen keywordWhen, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_keywords_timepoint_KeywordAt(KeywordAt keywordAt, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_keywords_interval_IntervalKeyword(IntervalKeyword intervalKeyword, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_fmec_keywords_interval_KeywordDuring(KeywordDuring keywordDuring, ContextType context) {
		return null;
	}
	
	private void notifyListeners(EObject element) {
		for (de.darwinspl.fmec.resource.fmec.IFmecInterpreterListener listener : listeners) {
			listener.handleInterpreteObject(element);
		}
	}
	
	/**
	 * Adds the given object to the interpretation stack. Attention: Objects that are
	 * added first, are interpret last.
	 */
	public void addObjectToInterprete(EObject object) {
		interpretationStack.push(object);
	}
	
	/**
	 * Adds the given collection of objects to the interpretation stack. Attention:
	 * Collections that are added first, are interpret last.
	 */
	public void addObjectsToInterprete(Collection<? extends EObject> objects) {
		for (EObject object : objects) {
			addObjectToInterprete(object);
		}
	}
	
	/**
	 * Adds the given collection of objects in reverse order to the interpretation
	 * stack.
	 */
	public void addObjectsToInterpreteInReverseOrder(Collection<? extends EObject> objects) {
		List<EObject> reverse = new ArrayList<EObject>(objects.size());
		reverse.addAll(objects);
		Collections.reverse(reverse);
		addObjectsToInterprete(reverse);
	}
	
	/**
	 * Adds the given object and all its children to the interpretation stack such
	 * that they are interpret in top down order.
	 */
	public void addObjectTreeToInterpreteTopDown(EObject root) {
		List<EObject> objects = new ArrayList<EObject>();
		objects.add(root);
		Iterator<EObject> it = root.eAllContents();
		while (it.hasNext()) {
			EObject eObject = (EObject) it.next();
			objects.add(eObject);
		}
		addObjectsToInterpreteInReverseOrder(objects);
	}
	
	public void addListener(de.darwinspl.fmec.resource.fmec.IFmecInterpreterListener newListener) {
		listeners.add(newListener);
	}
	
	public boolean removeListener(de.darwinspl.fmec.resource.fmec.IFmecInterpreterListener listener) {
		return listeners.remove(listener);
	}
	
	public EObject getNextObjectToInterprete() {
		return nextObjectToInterprete;
	}
	
	public Stack<EObject> getInterpretationStack() {
		return interpretationStack;
	}
	
	public void terminate() {
		interpretationStack.clear();
	}
	
	public ContextType getCurrentContext() {
		return currentContext;
	}
	
}
