/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec;


/**
 * Implementors of this interface can provide a post-processor for text resources.
 */
public interface IFmecResourcePostProcessorProvider {
	
	/**
	 * Returns the processor that shall be called after text resource are successfully
	 * parsed.
	 */
	public de.darwinspl.fmec.resource.fmec.IFmecResourcePostProcessor getResourcePostProcessor();
	
}
