/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.launch;


/**
 * A class that provides common methods that are required by launch configuration
 * delegates.
 */
public class FmecLaunchConfigurationHelper {
	// The generator for this class is currently disabled by option
	// 'disableLaunchSupport' in the .cs file.
}
