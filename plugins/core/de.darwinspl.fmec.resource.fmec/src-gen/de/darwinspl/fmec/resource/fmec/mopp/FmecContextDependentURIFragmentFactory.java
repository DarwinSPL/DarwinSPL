/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.mopp;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

/**
 * <p>
 * A factory for ContextDependentURIFragments. Given a feasible reference
 * resolver, this factory returns a matching fragment that used the resolver to
 * resolver proxy objects.
 * </p>
 * 
 * @param <ContainerType> the type of the class containing the reference to be
 * resolved
 * @param <ReferenceType> the type of the reference to be resolved
 */
public class FmecContextDependentURIFragmentFactory<ContainerType extends EObject, ReferenceType extends EObject>  implements de.darwinspl.fmec.resource.fmec.IFmecContextDependentURIFragmentFactory<ContainerType, ReferenceType> {
	
	private final de.darwinspl.fmec.resource.fmec.IFmecReferenceResolver<ContainerType, ReferenceType> resolver;
	
	public FmecContextDependentURIFragmentFactory(de.darwinspl.fmec.resource.fmec.IFmecReferenceResolver<ContainerType, ReferenceType> resolver) {
		this.resolver = resolver;
	}
	
	public de.darwinspl.fmec.resource.fmec.IFmecContextDependentURIFragment<?> create(String identifier, ContainerType container, EReference reference, int positionInReference, EObject proxy) {
		
		return new de.darwinspl.fmec.resource.fmec.mopp.FmecContextDependentURIFragment<ContainerType, ReferenceType>(identifier, container, reference, positionInReference, proxy) {
			public de.darwinspl.fmec.resource.fmec.IFmecReferenceResolver<ContainerType, ReferenceType> getResolver() {
				return resolver;
			}
		};
	}
}
