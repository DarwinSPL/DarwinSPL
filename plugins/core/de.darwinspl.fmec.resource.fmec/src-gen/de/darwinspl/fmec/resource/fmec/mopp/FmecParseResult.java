/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.mopp;

import java.util.ArrayList;
import java.util.Collection;
import org.eclipse.emf.ecore.EObject;

public class FmecParseResult implements de.darwinspl.fmec.resource.fmec.IFmecParseResult {
	
	private EObject root;
	
	private de.darwinspl.fmec.resource.fmec.IFmecLocationMap locationMap;
	
	private Collection<de.darwinspl.fmec.resource.fmec.IFmecCommand<de.darwinspl.fmec.resource.fmec.IFmecTextResource>> commands = new ArrayList<de.darwinspl.fmec.resource.fmec.IFmecCommand<de.darwinspl.fmec.resource.fmec.IFmecTextResource>>();
	
	public FmecParseResult() {
		super();
	}
	
	public EObject getRoot() {
		return root;
	}
	
	public de.darwinspl.fmec.resource.fmec.IFmecLocationMap getLocationMap() {
		return locationMap;
	}
	
	public void setRoot(EObject root) {
		this.root = root;
	}
	
	public void setLocationMap(de.darwinspl.fmec.resource.fmec.IFmecLocationMap locationMap) {
		this.locationMap = locationMap;
	}
	
	public Collection<de.darwinspl.fmec.resource.fmec.IFmecCommand<de.darwinspl.fmec.resource.fmec.IFmecTextResource>> getPostParseCommands() {
		return commands;
	}
	
}
