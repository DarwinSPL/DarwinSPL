/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.grammar;

import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * A class to represent placeholders in a grammar.
 */
public class FmecPlaceholder extends de.darwinspl.fmec.resource.fmec.grammar.FmecTerminal {
	
	private final String tokenName;
	
	public FmecPlaceholder(EStructuralFeature feature, String tokenName, de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality cardinality, int mandatoryOccurencesAfter) {
		super(feature, cardinality, mandatoryOccurencesAfter);
		this.tokenName = tokenName;
	}
	
	public String getTokenName() {
		return tokenName;
	}
	
}
