/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.grammar;


public enum FmecCardinality {
	
	ONE, PLUS, QUESTIONMARK, STAR;
	
}
