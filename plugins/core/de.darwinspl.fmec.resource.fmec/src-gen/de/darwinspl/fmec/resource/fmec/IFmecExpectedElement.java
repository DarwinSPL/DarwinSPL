/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec;

import java.util.Collection;
import java.util.Set;
import org.eclipse.emf.ecore.EClass;

/**
 * An element that is expected at a given position in a resource stream.
 */
public interface IFmecExpectedElement {
	
	/**
	 * Returns the names of all tokens that are expected at the given position.
	 */
	public Set<String> getTokenNames();
	
	/**
	 * Returns the metaclass of the rule that contains the expected element.
	 */
	public EClass getRuleMetaclass();
	
	/**
	 * Returns the syntax element that is expected.
	 */
	public de.darwinspl.fmec.resource.fmec.grammar.FmecSyntaxElement getSyntaxElement();
	
	/**
	 * Adds an element that is a valid follower for this element.
	 */
	public void addFollower(de.darwinspl.fmec.resource.fmec.IFmecExpectedElement follower, de.darwinspl.fmec.resource.fmec.mopp.FmecContainedFeature[] path);
	
	/**
	 * Returns all valid followers for this element. Each follower is represented by a
	 * pair of an expected elements and the containment trace that leads from the
	 * current element to the follower.
	 */
	public Collection<de.darwinspl.fmec.resource.fmec.util.FmecPair<de.darwinspl.fmec.resource.fmec.IFmecExpectedElement, de.darwinspl.fmec.resource.fmec.mopp.FmecContainedFeature[]>> getFollowers();
	
}
