/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.mopp;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.resource.Resource.Factory;

public class FmecMetaInformation implements de.darwinspl.fmec.resource.fmec.IFmecMetaInformation {
	
	public String getSyntaxName() {
		return "fmec";
	}
	
	public String getURI() {
		return "http://www.darwinspl.de/feature-model-evolution-constraints/1.0";
	}
	
	public de.darwinspl.fmec.resource.fmec.IFmecTextScanner createLexer() {
		return new de.darwinspl.fmec.resource.fmec.mopp.FmecAntlrScanner(new de.darwinspl.fmec.resource.fmec.mopp.FmecLexer());
	}
	
	public de.darwinspl.fmec.resource.fmec.IFmecTextParser createParser(InputStream inputStream, String encoding) {
		return new de.darwinspl.fmec.resource.fmec.mopp.FmecParser().createInstance(inputStream, encoding);
	}
	
	public de.darwinspl.fmec.resource.fmec.IFmecTextPrinter createPrinter(OutputStream outputStream, de.darwinspl.fmec.resource.fmec.IFmecTextResource resource) {
		return new de.darwinspl.fmec.resource.fmec.mopp.FmecPrinter2(outputStream, resource);
	}
	
	public EClass[] getClassesWithSyntax() {
		return new de.darwinspl.fmec.resource.fmec.mopp.FmecSyntaxCoverageInformationProvider().getClassesWithSyntax();
	}
	
	public EClass[] getStartSymbols() {
		return new de.darwinspl.fmec.resource.fmec.mopp.FmecSyntaxCoverageInformationProvider().getStartSymbols();
	}
	
	public de.darwinspl.fmec.resource.fmec.IFmecReferenceResolverSwitch getReferenceResolverSwitch() {
		return new de.darwinspl.fmec.resource.fmec.mopp.FmecReferenceResolverSwitch();
	}
	
	public de.darwinspl.fmec.resource.fmec.IFmecTokenResolverFactory getTokenResolverFactory() {
		return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenResolverFactory();
	}
	
	public String getPathToCSDefinition() {
		return "de.darwinspl.fmec/model/FMEC.cs";
	}
	
	public String[] getTokenNames() {
		return de.darwinspl.fmec.resource.fmec.mopp.FmecParser.tokenNames;
	}
	
	public de.darwinspl.fmec.resource.fmec.IFmecTokenStyle getDefaultTokenStyle(String tokenName) {
		return new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyleInformationProvider().getDefaultTokenStyle(tokenName);
	}
	
	public Collection<de.darwinspl.fmec.resource.fmec.IFmecBracketPair> getBracketPairs() {
		return new de.darwinspl.fmec.resource.fmec.mopp.FmecBracketInformationProvider().getBracketPairs();
	}
	
	public EClass[] getFoldableClasses() {
		return new de.darwinspl.fmec.resource.fmec.mopp.FmecFoldingInformationProvider().getFoldableClasses();
	}
	
	public Factory createResourceFactory() {
		return new de.darwinspl.fmec.resource.fmec.mopp.FmecResourceFactory();
	}
	
	public de.darwinspl.fmec.resource.fmec.mopp.FmecNewFileContentProvider getNewFileContentProvider() {
		return new de.darwinspl.fmec.resource.fmec.mopp.FmecNewFileContentProvider();
	}
	
	public void registerResourceFactory() {
		// if no resource factory registered, register delegator
		if (Factory.Registry.INSTANCE.getExtensionToFactoryMap().get(getSyntaxName()) == null) {
			Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(getSyntaxName(), new de.darwinspl.fmec.resource.fmec.mopp.FmecResourceFactoryDelegator());
		}
	}
	
	/**
	 * Returns the key of the option that can be used to register a preprocessor that
	 * is used as a pipe when loading resources. This key is language-specific. To
	 * register one preprocessor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getInputStreamPreprocessorProviderOptionKey() {
		return getSyntaxName() + "_" + "INPUT_STREAM_PREPROCESSOR_PROVIDER";
	}
	
	/**
	 * Returns the key of the option that can be used to register a post-processors
	 * that are invoked after loading resources. This key is language-specific. To
	 * register one post-processor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getResourcePostProcessorProviderOptionKey() {
		return getSyntaxName() + "_" + "RESOURCE_POSTPROCESSOR_PROVIDER";
	}
	
	public String getLaunchConfigurationType() {
		return "de.darwinspl.fmec.resource.fmec.ui.launchConfigurationType";
	}
	
	public de.darwinspl.fmec.resource.fmec.IFmecNameProvider createNameProvider() {
		return new de.darwinspl.fmec.resource.fmec.analysis.FmecDefaultNameProvider();
	}
	
	public String[] getSyntaxHighlightableTokenNames() {
		de.darwinspl.fmec.resource.fmec.mopp.FmecAntlrTokenHelper tokenHelper = new de.darwinspl.fmec.resource.fmec.mopp.FmecAntlrTokenHelper();
		List<String> highlightableTokens = new ArrayList<String>();
		String[] parserTokenNames = getTokenNames();
		for (int i = 0; i < parserTokenNames.length; i++) {
			// If ANTLR is used we need to normalize the token names
			if (!tokenHelper.canBeUsedForSyntaxHighlighting(i)) {
				continue;
			}
			String tokenName = tokenHelper.getTokenName(parserTokenNames, i);
			if (tokenName == null) {
				continue;
			}
			highlightableTokens.add(tokenName);
		}
		highlightableTokens.add(de.darwinspl.fmec.resource.fmec.mopp.FmecTokenStyleInformationProvider.TASK_ITEM_TOKEN_NAME);
		return highlightableTokens.toArray(new String[highlightableTokens.size()]);
	}
	
}
