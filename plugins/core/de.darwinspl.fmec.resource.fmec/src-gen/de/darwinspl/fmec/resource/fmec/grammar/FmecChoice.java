/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.grammar;


public class FmecChoice extends de.darwinspl.fmec.resource.fmec.grammar.FmecSyntaxElement {
	
	public FmecChoice(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality cardinality, de.darwinspl.fmec.resource.fmec.grammar.FmecSyntaxElement... choices) {
		super(cardinality, choices);
	}
	
	public String toString() {
		return de.darwinspl.fmec.resource.fmec.util.FmecStringUtil.explode(getChildren(), "|");
	}
	
}
