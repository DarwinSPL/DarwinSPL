/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec;


/**
 * A simple interface for commands that can be executed and that return
 * information about the success of their execution.
 */
public interface IFmecCommand<ContextType> {
	
	public boolean execute(ContextType context);
}
