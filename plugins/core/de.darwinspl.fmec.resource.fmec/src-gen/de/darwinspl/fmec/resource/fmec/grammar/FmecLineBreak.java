/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.grammar;


public class FmecLineBreak extends de.darwinspl.fmec.resource.fmec.grammar.FmecFormattingElement {
	
	private final int tabs;
	
	public FmecLineBreak(de.darwinspl.fmec.resource.fmec.grammar.FmecCardinality cardinality, int tabs) {
		super(cardinality);
		this.tabs = tabs;
	}
	
	public int getTabs() {
		return tabs;
	}
	
	public String toString() {
		return "!" + getTabs();
	}
	
}
