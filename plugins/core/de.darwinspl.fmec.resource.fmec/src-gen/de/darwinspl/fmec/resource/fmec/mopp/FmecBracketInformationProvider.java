/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.mopp;

import java.util.ArrayList;
import java.util.Collection;

/**
 * This class provides information about how brackets must be handled in the
 * editor (e.g., whether they must be closed automatically).
 */
public class FmecBracketInformationProvider {
	
	public Collection<de.darwinspl.fmec.resource.fmec.IFmecBracketPair> getBracketPairs() {
		Collection<de.darwinspl.fmec.resource.fmec.IFmecBracketPair> result = new ArrayList<de.darwinspl.fmec.resource.fmec.IFmecBracketPair>();
		result.add(new de.darwinspl.fmec.resource.fmec.mopp.FmecBracketPair("{", "}", true, true));
		return result;
	}
	
}
