/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.mopp;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

/**
 * This class provides an implementation of the
 * de.darwinspl.fmec.resource.fmec.IFmecTextDiagnostic interface. However, it is
 * recommended to use the de.darwinspl.fmec.resource.fmec.mopp.FmecPrinter2
 * instead, because it provides advanced printing features. There are even some
 * features (e.g., printing enumeration terminals) which are only supported by
 * that class.
 */
public class FmecPrinter implements de.darwinspl.fmec.resource.fmec.IFmecTextPrinter {
	
	protected de.darwinspl.fmec.resource.fmec.IFmecTokenResolverFactory tokenResolverFactory = new de.darwinspl.fmec.resource.fmec.mopp.FmecTokenResolverFactory();
	
	protected OutputStream outputStream;
	
	/**
	 * Holds the resource that is associated with this printer. This may be null if
	 * the printer is used stand alone.
	 */
	private de.darwinspl.fmec.resource.fmec.IFmecTextResource resource;
	
	private Map<?, ?> options;
	private String encoding = System.getProperty("file.encoding");
	
	public FmecPrinter(OutputStream outputStream, de.darwinspl.fmec.resource.fmec.IFmecTextResource resource) {
		super();
		this.outputStream = outputStream;
		this.resource = resource;
	}
	
	protected int matchCount(Map<String, Integer> featureCounter, Collection<String> needed) {
		int pos = 0;
		int neg = 0;
		
		for (String featureName : featureCounter.keySet()) {
			if (needed.contains(featureName)) {
				int value = featureCounter.get(featureName);
				if (value == 0) {
					neg += 1;
				} else {
					pos += 1;
				}
			}
		}
		return neg > 0 ? -neg : pos;
	}
	
	protected void doPrint(EObject element, PrintWriter out, String globaltab) {
		if (element == null) {
			throw new IllegalArgumentException("Nothing to write.");
		}
		if (out == null) {
			throw new IllegalArgumentException("Nothing to write on.");
		}
		
		if (element instanceof de.darwinspl.fmec.FeatureModelEvolutionConstraintModel) {
			print_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel((de.darwinspl.fmec.FeatureModelEvolutionConstraintModel) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.constraints.BinaryConstraint) {
			print_de_darwinspl_fmec_constraints_BinaryConstraint((de.darwinspl.fmec.constraints.BinaryConstraint) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.constraints.UnaryConstraint) {
			print_de_darwinspl_fmec_constraints_UnaryConstraint((de.darwinspl.fmec.constraints.UnaryConstraint) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.constraints.NestedConstraint) {
			print_de_darwinspl_fmec_constraints_NestedConstraint((de.darwinspl.fmec.constraints.NestedConstraint) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.constraints.TimePointConstraint) {
			print_de_darwinspl_fmec_constraints_TimePointConstraint((de.darwinspl.fmec.constraints.TimePointConstraint) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.constraints.TimeIntervalConstraint) {
			print_de_darwinspl_fmec_constraints_TimeIntervalConstraint((de.darwinspl.fmec.constraints.TimeIntervalConstraint) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.property.EvolutionaryProperty) {
			print_de_darwinspl_fmec_property_EvolutionaryProperty((de.darwinspl.fmec.property.EvolutionaryProperty) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.property.FeatureExistenceProperty) {
			print_de_darwinspl_fmec_property_FeatureExistenceProperty((de.darwinspl.fmec.property.FeatureExistenceProperty) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.property.FeatureTypeProperty) {
			print_de_darwinspl_fmec_property_FeatureTypeProperty((de.darwinspl.fmec.property.FeatureTypeProperty) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.property.FeatureParentProperty) {
			print_de_darwinspl_fmec_property_FeatureParentProperty((de.darwinspl.fmec.property.FeatureParentProperty) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.property.GroupExistenceProperty) {
			print_de_darwinspl_fmec_property_GroupExistenceProperty((de.darwinspl.fmec.property.GroupExistenceProperty) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.property.GroupTypeProperty) {
			print_de_darwinspl_fmec_property_GroupTypeProperty((de.darwinspl.fmec.property.GroupTypeProperty) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.property.GroupParentProperty) {
			print_de_darwinspl_fmec_property_GroupParentProperty((de.darwinspl.fmec.property.GroupParentProperty) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.timepoint.TimePoint) {
			print_de_darwinspl_fmec_timepoint_TimePoint((de.darwinspl.fmec.timepoint.TimePoint) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.timepoint.TimeOffset) {
			print_de_darwinspl_fmec_timepoint_TimeOffset((de.darwinspl.fmec.timepoint.TimeOffset) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.timepoint.ExplicitTimePoint) {
			print_de_darwinspl_fmec_timepoint_ExplicitTimePoint((de.darwinspl.fmec.timepoint.ExplicitTimePoint) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.timepoint.EvolutionaryEvent) {
			print_de_darwinspl_fmec_timepoint_EvolutionaryEvent((de.darwinspl.fmec.timepoint.EvolutionaryEvent) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.keywords.logical.KeywordAnd) {
			print_de_darwinspl_fmec_keywords_logical_KeywordAnd((de.darwinspl.fmec.keywords.logical.KeywordAnd) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.keywords.logical.KeywordOr) {
			print_de_darwinspl_fmec_keywords_logical_KeywordOr((de.darwinspl.fmec.keywords.logical.KeywordOr) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.keywords.logical.KeywordImplies) {
			print_de_darwinspl_fmec_keywords_logical_KeywordImplies((de.darwinspl.fmec.keywords.logical.KeywordImplies) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.keywords.logical.KeywordNot) {
			print_de_darwinspl_fmec_keywords_logical_KeywordNot((de.darwinspl.fmec.keywords.logical.KeywordNot) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.keywords.buildingblocks.KeywordStarts) {
			print_de_darwinspl_fmec_keywords_buildingblocks_KeywordStarts((de.darwinspl.fmec.keywords.buildingblocks.KeywordStarts) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.keywords.buildingblocks.KeywordStops) {
			print_de_darwinspl_fmec_keywords_buildingblocks_KeywordStops((de.darwinspl.fmec.keywords.buildingblocks.KeywordStops) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.keywords.buildingblocks.KeywordValid) {
			print_de_darwinspl_fmec_keywords_buildingblocks_KeywordValid((de.darwinspl.fmec.keywords.buildingblocks.KeywordValid) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.keywords.timepoint.KeywordBefore) {
			print_de_darwinspl_fmec_keywords_timepoint_KeywordBefore((de.darwinspl.fmec.keywords.timepoint.KeywordBefore) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.keywords.timepoint.KeywordUntil) {
			print_de_darwinspl_fmec_keywords_timepoint_KeywordUntil((de.darwinspl.fmec.keywords.timepoint.KeywordUntil) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.keywords.timepoint.KeywordAfter) {
			print_de_darwinspl_fmec_keywords_timepoint_KeywordAfter((de.darwinspl.fmec.keywords.timepoint.KeywordAfter) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.keywords.timepoint.KeywordWhen) {
			print_de_darwinspl_fmec_keywords_timepoint_KeywordWhen((de.darwinspl.fmec.keywords.timepoint.KeywordWhen) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.keywords.timepoint.KeywordAt) {
			print_de_darwinspl_fmec_keywords_timepoint_KeywordAt((de.darwinspl.fmec.keywords.timepoint.KeywordAt) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.fmec.keywords.interval.KeywordDuring) {
			print_de_darwinspl_fmec_keywords_interval_KeywordDuring((de.darwinspl.fmec.keywords.interval.KeywordDuring) element, globaltab, out);
			return;
		}
		
		addWarningToResource("The printer can not handle " + element.eClass().getName() + " elements", element);
	}
	
	protected de.darwinspl.fmec.resource.fmec.mopp.FmecReferenceResolverSwitch getReferenceResolverSwitch() {
		return (de.darwinspl.fmec.resource.fmec.mopp.FmecReferenceResolverSwitch) new de.darwinspl.fmec.resource.fmec.mopp.FmecMetaInformation().getReferenceResolverSwitch();
	}
	
	protected void addWarningToResource(final String errorMessage, EObject cause) {
		de.darwinspl.fmec.resource.fmec.IFmecTextResource resource = getResource();
		if (resource == null) {
			// the resource can be null if the printer is used stand alone
			return;
		}
		resource.addProblem(new de.darwinspl.fmec.resource.fmec.mopp.FmecProblem(errorMessage, de.darwinspl.fmec.resource.fmec.FmecEProblemType.PRINT_PROBLEM, de.darwinspl.fmec.resource.fmec.FmecEProblemSeverity.WARNING), cause);
	}
	
	public void setOptions(Map<?,?> options) {
		this.options = options;
	}
	
	public Map<?,?> getOptions() {
		return options;
	}
	
	public void setEncoding(String encoding) {
		if (encoding != null) {
			this.encoding = encoding;
		}
	}
	
	public String getEncoding() {
		return encoding;
	}
	
	public de.darwinspl.fmec.resource.fmec.IFmecTextResource getResource() {
		return resource;
	}
	
	/**
	 * Calls {@link #doPrint(EObject, PrintWriter, String)} and writes the result to
	 * the underlying output stream.
	 */
	public void print(EObject element) throws java.io.IOException {
		PrintWriter out = new PrintWriter(new OutputStreamWriter(new BufferedOutputStream(outputStream), encoding));
		doPrint(element, out, "");
		out.flush();
	}
	
	public void print_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel(de.darwinspl.fmec.FeatureModelEvolutionConstraintModel element, String outertab, PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__FEATURE_MODEL));
		printCountingMap.put("featureModel", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__CONSTRAINTS));
		printCountingMap.put("constraints", temp == null ? 0 : ((Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		PrintWriter out1 = null;
		Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("feature");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("model");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("featureModel");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__FEATURE_MODEL));
			if (o != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver resolver = tokenResolverFactory.createTokenResolver("PATH_TO_FILE");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getFeatureModelEvolutionConstraintModelFeatureModelReferenceResolver().deResolve((de.darwinspl.feature.DwTemporalFeatureModel) o, element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__FEATURE_MODEL)), element.eClass().getEStructuralFeature(de.darwinspl.fmec.FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__FEATURE_MODEL), element));
				out.print(" ");
			}
			printCountingMap.put("featureModel", count - 1);
		}
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new StringWriter();
			out1 = new PrintWriter(sWriter);
			printCountingMap1 = new LinkedHashMap<String, Integer>(printCountingMap);
			print_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
	}
	
	public void print_de_darwinspl_fmec_FeatureModelEvolutionConstraintModel_0(de.darwinspl.fmec.FeatureModelEvolutionConstraintModel element, String outertab, PrintWriter out, Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("constraints");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.FmecPackage.FEATURE_MODEL_EVOLUTION_CONSTRAINT_MODEL__CONSTRAINTS));
			List<?> list = (List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("constraints", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(";");
		out.print(" ");
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
	}
	
	
	public void print_de_darwinspl_fmec_constraints_BinaryConstraint(de.darwinspl.fmec.constraints.BinaryConstraint element, String outertab, PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.BINARY_CONSTRAINT__LEFT_CHILD));
		printCountingMap.put("leftChild", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.BINARY_CONSTRAINT__KEYWORD));
		printCountingMap.put("keyword", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.BINARY_CONSTRAINT__RIGHT_CHILD));
		printCountingMap.put("rightChild", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("leftChild");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.BINARY_CONSTRAINT__LEFT_CHILD));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("leftChild", count - 1);
		}
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("keyword");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.BINARY_CONSTRAINT__KEYWORD));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("keyword", count - 1);
		}
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("rightChild");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.BINARY_CONSTRAINT__RIGHT_CHILD));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("rightChild", count - 1);
		}
	}
	
	
	public void print_de_darwinspl_fmec_constraints_UnaryConstraint(de.darwinspl.fmec.constraints.UnaryConstraint element, String outertab, PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.UNARY_CONSTRAINT__CHILD));
		printCountingMap.put("child", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.UNARY_CONSTRAINT__KEYWORD));
		printCountingMap.put("keyword", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("keyword");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.UNARY_CONSTRAINT__KEYWORD));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("keyword", count - 1);
		}
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("child");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.UNARY_CONSTRAINT__CHILD));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("child", count - 1);
		}
	}
	
	
	public void print_de_darwinspl_fmec_constraints_NestedConstraint(de.darwinspl.fmec.constraints.NestedConstraint element, String outertab, PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.NESTED_CONSTRAINT__NESTED_CONSTRAINT));
		printCountingMap.put("nestedConstraint", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (LineBreak)
		localtab += "	";
		out.println();
		out.print(localtab);
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("nestedConstraint");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.NESTED_CONSTRAINT__NESTED_CONSTRAINT));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("nestedConstraint", count - 1);
		}
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	
	public void print_de_darwinspl_fmec_constraints_TimePointConstraint(de.darwinspl.fmec.constraints.TimePointConstraint element, String outertab, PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_POINT_CONSTRAINT__PRE_ARGUMENT));
		printCountingMap.put("preArgument", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_POINT_CONSTRAINT__KEYWORD));
		printCountingMap.put("keyword", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_POINT_CONSTRAINT__POST_ARGUMENT));
		printCountingMap.put("postArgument", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("preArgument");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_POINT_CONSTRAINT__PRE_ARGUMENT));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("preArgument", count - 1);
		}
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("keyword");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_POINT_CONSTRAINT__KEYWORD));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("keyword", count - 1);
		}
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("postArgument");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_POINT_CONSTRAINT__POST_ARGUMENT));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("postArgument", count - 1);
		}
	}
	
	
	public void print_de_darwinspl_fmec_constraints_TimeIntervalConstraint(de.darwinspl.fmec.constraints.TimeIntervalConstraint element, String outertab, PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(4);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__PRE_ARGUMENT));
		printCountingMap.put("preArgument", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__KEYWORD));
		printCountingMap.put("keyword", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__START_POINT));
		printCountingMap.put("startPoint", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__END_POINT));
		printCountingMap.put("endPoint", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("preArgument");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__PRE_ARGUMENT));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("preArgument", count - 1);
		}
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("keyword");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__KEYWORD));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("keyword", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("[");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("startPoint");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__START_POINT));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("startPoint", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(",");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("endPoint");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.constraints.ConstraintsPackage.TIME_INTERVAL_CONSTRAINT__END_POINT));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("endPoint", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(")");
		out.print(" ");
	}
	
	
	public void print_de_darwinspl_fmec_property_EvolutionaryProperty(de.darwinspl.fmec.property.EvolutionaryProperty element, String outertab, PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.EVOLUTIONARY_PROPERTY__PROPERTY));
		printCountingMap.put("property", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.EVOLUTIONARY_PROPERTY__KEYWORD));
		printCountingMap.put("keyword", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("property");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.EVOLUTIONARY_PROPERTY__PROPERTY));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("property", count - 1);
		}
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("keyword");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.EVOLUTIONARY_PROPERTY__KEYWORD));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("keyword", count - 1);
		}
	}
	
	
	public void print_de_darwinspl_fmec_property_FeatureExistenceProperty(de.darwinspl.fmec.property.FeatureExistenceProperty element, String outertab, PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_EXISTENCE_PROPERTY__REFERENCED_FEATURE));
		printCountingMap.put("referencedFeature", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("referencedFeature");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_EXISTENCE_PROPERTY__REFERENCED_FEATURE));
			if (o != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getFeatureReferenceReferencedFeatureReferenceResolver().deResolve((de.darwinspl.feature.DwFeature) o, element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_EXISTENCE_PROPERTY__REFERENCED_FEATURE)), element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_EXISTENCE_PROPERTY__REFERENCED_FEATURE), element));
				out.print(" ");
			}
			printCountingMap.put("referencedFeature", count - 1);
		}
	}
	
	
	public void print_de_darwinspl_fmec_property_FeatureTypeProperty(de.darwinspl.fmec.property.FeatureTypeProperty element, String outertab, PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_TYPE_PROPERTY__REFERENCED_FEATURE));
		printCountingMap.put("referencedFeature", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_TYPE_PROPERTY__FEATURE_TYPE));
		printCountingMap.put("featureType", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("referencedFeature");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_TYPE_PROPERTY__REFERENCED_FEATURE));
			if (o != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getFeatureReferenceReferencedFeatureReferenceResolver().deResolve((de.darwinspl.feature.DwFeature) o, element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_TYPE_PROPERTY__REFERENCED_FEATURE)), element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_TYPE_PROPERTY__REFERENCED_FEATURE), element));
				out.print(" ");
			}
			printCountingMap.put("referencedFeature", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(".");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("type");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("==");
		out.print(" ");
		// DEFINITION PART BEGINS (EnumTerminal)
		count = printCountingMap.get("featureType");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_TYPE_PROPERTY__FEATURE_TYPE));
			if (o != null) {
			}
			printCountingMap.put("featureType", count - 1);
		}
	}
	
	
	public void print_de_darwinspl_fmec_property_FeatureParentProperty(de.darwinspl.fmec.property.FeatureParentProperty element, String outertab, PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_PARENT_PROPERTY__REFERENCED_FEATURE));
		printCountingMap.put("referencedFeature", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_PARENT_PROPERTY__REFERENCED_GROUP));
		printCountingMap.put("referencedGroup", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("referencedFeature");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_PARENT_PROPERTY__REFERENCED_FEATURE));
			if (o != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getFeatureReferenceReferencedFeatureReferenceResolver().deResolve((de.darwinspl.feature.DwFeature) o, element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_PARENT_PROPERTY__REFERENCED_FEATURE)), element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_PARENT_PROPERTY__REFERENCED_FEATURE), element));
				out.print(" ");
			}
			printCountingMap.put("referencedFeature", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(".");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("parentGroup");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("==");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("referencedGroup");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_PARENT_PROPERTY__REFERENCED_GROUP));
			if (o != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getGroupReferenceReferencedGroupReferenceResolver().deResolve((de.darwinspl.feature.DwGroup) o, element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_PARENT_PROPERTY__REFERENCED_GROUP)), element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.FEATURE_PARENT_PROPERTY__REFERENCED_GROUP), element));
				out.print(" ");
			}
			printCountingMap.put("referencedGroup", count - 1);
		}
	}
	
	
	public void print_de_darwinspl_fmec_property_GroupExistenceProperty(de.darwinspl.fmec.property.GroupExistenceProperty element, String outertab, PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_EXISTENCE_PROPERTY__REFERENCED_GROUP));
		printCountingMap.put("referencedGroup", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("referencedGroup");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_EXISTENCE_PROPERTY__REFERENCED_GROUP));
			if (o != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getGroupReferenceReferencedGroupReferenceResolver().deResolve((de.darwinspl.feature.DwGroup) o, element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_EXISTENCE_PROPERTY__REFERENCED_GROUP)), element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_EXISTENCE_PROPERTY__REFERENCED_GROUP), element));
				out.print(" ");
			}
			printCountingMap.put("referencedGroup", count - 1);
		}
	}
	
	
	public void print_de_darwinspl_fmec_property_GroupTypeProperty(de.darwinspl.fmec.property.GroupTypeProperty element, String outertab, PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_TYPE_PROPERTY__REFERENCED_GROUP));
		printCountingMap.put("referencedGroup", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_TYPE_PROPERTY__GROUP_TYPE));
		printCountingMap.put("groupType", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("referencedGroup");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_TYPE_PROPERTY__REFERENCED_GROUP));
			if (o != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getGroupReferenceReferencedGroupReferenceResolver().deResolve((de.darwinspl.feature.DwGroup) o, element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_TYPE_PROPERTY__REFERENCED_GROUP)), element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_TYPE_PROPERTY__REFERENCED_GROUP), element));
				out.print(" ");
			}
			printCountingMap.put("referencedGroup", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(".");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("type");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("==");
		out.print(" ");
		// DEFINITION PART BEGINS (EnumTerminal)
		count = printCountingMap.get("groupType");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_TYPE_PROPERTY__GROUP_TYPE));
			if (o != null) {
			}
			printCountingMap.put("groupType", count - 1);
		}
	}
	
	
	public void print_de_darwinspl_fmec_property_GroupParentProperty(de.darwinspl.fmec.property.GroupParentProperty element, String outertab, PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_GROUP));
		printCountingMap.put("referencedGroup", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_FEATURE));
		printCountingMap.put("referencedFeature", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("referencedGroup");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_GROUP));
			if (o != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getGroupReferenceReferencedGroupReferenceResolver().deResolve((de.darwinspl.feature.DwGroup) o, element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_GROUP)), element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_GROUP), element));
				out.print(" ");
			}
			printCountingMap.put("referencedGroup", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(".");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("parentFeature");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("==");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("referencedFeature");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_FEATURE));
			if (o != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getFeatureReferenceReferencedFeatureReferenceResolver().deResolve((de.darwinspl.feature.DwFeature) o, element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_FEATURE)), element.eClass().getEStructuralFeature(de.darwinspl.fmec.property.PropertyPackage.GROUP_PARENT_PROPERTY__REFERENCED_FEATURE), element));
				out.print(" ");
			}
			printCountingMap.put("referencedFeature", count - 1);
		}
	}
	
	
	public void print_de_darwinspl_fmec_timepoint_TimePoint(de.darwinspl.fmec.timepoint.TimePoint element, String outertab, PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_POINT__TIME_POINT));
		printCountingMap.put("timePoint", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_POINT__OFFSET));
		printCountingMap.put("offset", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("timePoint");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_POINT__TIME_POINT));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("timePoint", count - 1);
		}
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("offset");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_POINT__OFFSET));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("offset", count - 1);
		}
	}
	
	
	public void print_de_darwinspl_fmec_timepoint_TimeOffset(de.darwinspl.fmec.timepoint.TimeOffset element, String outertab, PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(6);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__POSITIVE_SIGNATURE));
		printCountingMap.put("positiveSignature", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__YEARS));
		printCountingMap.put("years", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__MONTHS));
		printCountingMap.put("months", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__DAYS));
		printCountingMap.put("days", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__HOURS));
		printCountingMap.put("hours", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__MINUTES));
		printCountingMap.put("minutes", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		java.io.StringWriter sWriter = null;
		PrintWriter out1 = null;
		Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (BooleanTerminal)
		count = printCountingMap.get("positiveSignature");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__POSITIVE_SIGNATURE));
			if (o != null) {
			}
			printCountingMap.put("positiveSignature", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new StringWriter();
		out1 = new PrintWriter(sWriter);
		printCountingMap1 = new LinkedHashMap<String, Integer>(printCountingMap);
		print_de_darwinspl_fmec_timepoint_TimeOffset_0(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new StringWriter();
		out1 = new PrintWriter(sWriter);
		printCountingMap1 = new LinkedHashMap<String, Integer>(printCountingMap);
		print_de_darwinspl_fmec_timepoint_TimeOffset_1(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new StringWriter();
		out1 = new PrintWriter(sWriter);
		printCountingMap1 = new LinkedHashMap<String, Integer>(printCountingMap);
		print_de_darwinspl_fmec_timepoint_TimeOffset_2(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new StringWriter();
		out1 = new PrintWriter(sWriter);
		printCountingMap1 = new LinkedHashMap<String, Integer>(printCountingMap);
		print_de_darwinspl_fmec_timepoint_TimeOffset_3(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new StringWriter();
		out1 = new PrintWriter(sWriter);
		printCountingMap1 = new LinkedHashMap<String, Integer>(printCountingMap);
		print_de_darwinspl_fmec_timepoint_TimeOffset_4(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
	}
	
	public void print_de_darwinspl_fmec_timepoint_TimeOffset_0(de.darwinspl.fmec.timepoint.TimeOffset element, String outertab, PrintWriter out, Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("years");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__YEARS));
			if (o != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver resolver = tokenResolverFactory.createTokenResolver("INTEGER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__YEARS), element));
				out.print(" ");
			}
			printCountingMap.put("years", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("years");
		out.print(" ");
	}
	
	public void print_de_darwinspl_fmec_timepoint_TimeOffset_1(de.darwinspl.fmec.timepoint.TimeOffset element, String outertab, PrintWriter out, Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("months");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__MONTHS));
			if (o != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver resolver = tokenResolverFactory.createTokenResolver("INTEGER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__MONTHS), element));
				out.print(" ");
			}
			printCountingMap.put("months", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("months");
		out.print(" ");
	}
	
	public void print_de_darwinspl_fmec_timepoint_TimeOffset_2(de.darwinspl.fmec.timepoint.TimeOffset element, String outertab, PrintWriter out, Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("days");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__DAYS));
			if (o != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver resolver = tokenResolverFactory.createTokenResolver("INTEGER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__DAYS), element));
				out.print(" ");
			}
			printCountingMap.put("days", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("days");
		out.print(" ");
	}
	
	public void print_de_darwinspl_fmec_timepoint_TimeOffset_3(de.darwinspl.fmec.timepoint.TimeOffset element, String outertab, PrintWriter out, Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("hours");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__HOURS));
			if (o != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver resolver = tokenResolverFactory.createTokenResolver("INTEGER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__HOURS), element));
				out.print(" ");
			}
			printCountingMap.put("hours", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("hours");
		out.print(" ");
	}
	
	public void print_de_darwinspl_fmec_timepoint_TimeOffset_4(de.darwinspl.fmec.timepoint.TimeOffset element, String outertab, PrintWriter out, Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("minutes");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__MINUTES));
			if (o != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver resolver = tokenResolverFactory.createTokenResolver("INTEGER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.TIME_OFFSET__MINUTES), element));
				out.print(" ");
			}
			printCountingMap.put("minutes", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("minutes");
		out.print(" ");
	}
	
	
	public void print_de_darwinspl_fmec_timepoint_ExplicitTimePoint(de.darwinspl.fmec.timepoint.ExplicitTimePoint element, String outertab, PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.EXPLICIT_TIME_POINT__DATE));
		printCountingMap.put("date", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("date");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.EXPLICIT_TIME_POINT__DATE));
			if (o != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver resolver = tokenResolverFactory.createTokenResolver("DATE");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.EXPLICIT_TIME_POINT__DATE), element));
				out.print(" ");
			}
			printCountingMap.put("date", count - 1);
		}
	}
	
	
	public void print_de_darwinspl_fmec_timepoint_EvolutionaryEvent(de.darwinspl.fmec.timepoint.EvolutionaryEvent element, String outertab, PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.EVOLUTIONARY_EVENT__REFERENCED_FEATURE));
		printCountingMap.put("referencedFeature", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.EVOLUTIONARY_EVENT__KEYWORD));
		printCountingMap.put("keyword", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("referencedFeature");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.EVOLUTIONARY_EVENT__REFERENCED_FEATURE));
			if (o != null) {
				de.darwinspl.fmec.resource.fmec.IFmecTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getFeatureReferenceReferencedFeatureReferenceResolver().deResolve((de.darwinspl.feature.DwFeature) o, element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.EVOLUTIONARY_EVENT__REFERENCED_FEATURE)), element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.EVOLUTIONARY_EVENT__REFERENCED_FEATURE), element));
				out.print(" ");
			}
			printCountingMap.put("referencedFeature", count - 1);
		}
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("keyword");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.fmec.timepoint.TimepointPackage.EVOLUTIONARY_EVENT__KEYWORD));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("keyword", count - 1);
		}
	}
	
	
	public void print_de_darwinspl_fmec_keywords_logical_KeywordAnd(de.darwinspl.fmec.keywords.logical.KeywordAnd element, String outertab, PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		// print collected hidden tokens
		// DEFINITION PART BEGINS (CsString)
		out.print("and");
		out.print(" ");
	}
	
	
	public void print_de_darwinspl_fmec_keywords_logical_KeywordOr(de.darwinspl.fmec.keywords.logical.KeywordOr element, String outertab, PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		// print collected hidden tokens
		// DEFINITION PART BEGINS (CsString)
		out.print("or");
		out.print(" ");
	}
	
	
	public void print_de_darwinspl_fmec_keywords_logical_KeywordImplies(de.darwinspl.fmec.keywords.logical.KeywordImplies element, String outertab, PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		// print collected hidden tokens
		// DEFINITION PART BEGINS (CsString)
		out.print("implies");
		out.print(" ");
	}
	
	
	public void print_de_darwinspl_fmec_keywords_logical_KeywordNot(de.darwinspl.fmec.keywords.logical.KeywordNot element, String outertab, PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		// print collected hidden tokens
		// DEFINITION PART BEGINS (CsString)
		out.print("not");
		out.print(" ");
	}
	
	
	public void print_de_darwinspl_fmec_keywords_buildingblocks_KeywordStarts(de.darwinspl.fmec.keywords.buildingblocks.KeywordStarts element, String outertab, PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		// print collected hidden tokens
		// DEFINITION PART BEGINS (CsString)
		out.print("starts");
		out.print(" ");
	}
	
	
	public void print_de_darwinspl_fmec_keywords_buildingblocks_KeywordStops(de.darwinspl.fmec.keywords.buildingblocks.KeywordStops element, String outertab, PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		// print collected hidden tokens
		// DEFINITION PART BEGINS (CsString)
		out.print("ends");
		out.print(" ");
	}
	
	
	public void print_de_darwinspl_fmec_keywords_buildingblocks_KeywordValid(de.darwinspl.fmec.keywords.buildingblocks.KeywordValid element, String outertab, PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		// print collected hidden tokens
		// DEFINITION PART BEGINS (CsString)
		out.print("valid");
		out.print(" ");
	}
	
	
	public void print_de_darwinspl_fmec_keywords_timepoint_KeywordBefore(de.darwinspl.fmec.keywords.timepoint.KeywordBefore element, String outertab, PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		// print collected hidden tokens
		// DEFINITION PART BEGINS (CsString)
		out.print("before");
		out.print(" ");
	}
	
	
	public void print_de_darwinspl_fmec_keywords_timepoint_KeywordUntil(de.darwinspl.fmec.keywords.timepoint.KeywordUntil element, String outertab, PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		// print collected hidden tokens
		// DEFINITION PART BEGINS (CsString)
		out.print("until");
		out.print(" ");
	}
	
	
	public void print_de_darwinspl_fmec_keywords_timepoint_KeywordAfter(de.darwinspl.fmec.keywords.timepoint.KeywordAfter element, String outertab, PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		// print collected hidden tokens
		// DEFINITION PART BEGINS (CsString)
		out.print("after");
		out.print(" ");
	}
	
	
	public void print_de_darwinspl_fmec_keywords_timepoint_KeywordWhen(de.darwinspl.fmec.keywords.timepoint.KeywordWhen element, String outertab, PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		// print collected hidden tokens
		// DEFINITION PART BEGINS (CsString)
		out.print("when");
		out.print(" ");
	}
	
	
	public void print_de_darwinspl_fmec_keywords_timepoint_KeywordAt(de.darwinspl.fmec.keywords.timepoint.KeywordAt element, String outertab, PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		// print collected hidden tokens
		// DEFINITION PART BEGINS (CsString)
		out.print("at");
		out.print(" ");
	}
	
	
	public void print_de_darwinspl_fmec_keywords_interval_KeywordDuring(de.darwinspl.fmec.keywords.interval.KeywordDuring element, String outertab, PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		// print collected hidden tokens
		// DEFINITION PART BEGINS (CsString)
		out.print("during");
		out.print(" ");
	}
	
	
}
