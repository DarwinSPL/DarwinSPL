/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.mopp;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;

public class FmecProblem implements de.darwinspl.fmec.resource.fmec.IFmecProblem {
	
	private String message;
	private de.darwinspl.fmec.resource.fmec.FmecEProblemType type;
	private de.darwinspl.fmec.resource.fmec.FmecEProblemSeverity severity;
	private Collection<de.darwinspl.fmec.resource.fmec.IFmecQuickFix> quickFixes;
	
	public FmecProblem(String message, de.darwinspl.fmec.resource.fmec.FmecEProblemType type, de.darwinspl.fmec.resource.fmec.FmecEProblemSeverity severity) {
		this(message, type, severity, Collections.<de.darwinspl.fmec.resource.fmec.IFmecQuickFix>emptySet());
	}
	
	public FmecProblem(String message, de.darwinspl.fmec.resource.fmec.FmecEProblemType type, de.darwinspl.fmec.resource.fmec.FmecEProblemSeverity severity, de.darwinspl.fmec.resource.fmec.IFmecQuickFix quickFix) {
		this(message, type, severity, Collections.singleton(quickFix));
	}
	
	public FmecProblem(String message, de.darwinspl.fmec.resource.fmec.FmecEProblemType type, de.darwinspl.fmec.resource.fmec.FmecEProblemSeverity severity, Collection<de.darwinspl.fmec.resource.fmec.IFmecQuickFix> quickFixes) {
		super();
		this.message = message;
		this.type = type;
		this.severity = severity;
		this.quickFixes = new LinkedHashSet<de.darwinspl.fmec.resource.fmec.IFmecQuickFix>();
		this.quickFixes.addAll(quickFixes);
	}
	
	public de.darwinspl.fmec.resource.fmec.FmecEProblemType getType() {
		return type;
	}
	
	public de.darwinspl.fmec.resource.fmec.FmecEProblemSeverity getSeverity() {
		return severity;
	}
	
	public String getMessage() {
		return message;
	}
	
	public Collection<de.darwinspl.fmec.resource.fmec.IFmecQuickFix> getQuickFixes() {
		return quickFixes;
	}
	
}
