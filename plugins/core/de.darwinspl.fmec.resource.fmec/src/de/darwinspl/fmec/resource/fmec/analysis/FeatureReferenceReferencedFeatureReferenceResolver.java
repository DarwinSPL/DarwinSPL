/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.analysis;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwName;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.fmec.resource.fmec.analysis.util.ReferenceResolveUtil;
import de.darwinspl.temporal.DwTemporalElement;
import de.darwinspl.fmec.FeatureModelEvolutionConstraintModel;

public class FeatureReferenceReferencedFeatureReferenceResolver implements de.darwinspl.fmec.resource.fmec.IFmecReferenceResolver<de.darwinspl.fmec.property.FeatureReference, de.darwinspl.feature.DwFeature> {
	
	private de.darwinspl.fmec.resource.fmec.analysis.FmecDefaultResolverDelegate<de.darwinspl.fmec.property.FeatureReference, de.darwinspl.feature.DwFeature> delegate = new de.darwinspl.fmec.resource.fmec.analysis.FmecDefaultResolverDelegate<de.darwinspl.fmec.property.FeatureReference, de.darwinspl.feature.DwFeature>();
	
	public void resolve(String identifier, de.darwinspl.fmec.property.FeatureReference container, EReference reference, int position, boolean resolveFuzzy, final de.darwinspl.fmec.resource.fmec.IFmecReferenceResolveResult<de.darwinspl.feature.DwFeature> result) {
		FeatureModelEvolutionConstraintModel root = ReferenceResolveUtil.getRoot(container);
		DwTemporalFeatureModel featureModel = root.getFeatureModel();
		if(featureModel == null)
			return;
		
		identifier = identifier.replaceAll("\"", "");
		List<DwFeature> matchingFeatures = getAllFeaturesWithMatchingName(new LinkedList<DwFeature>(), featureModel, identifier);
		
		if(matchingFeatures.size() > 1) {
			result.setErrorMessage(
					"Multiple features are named '" + identifier + "' at some point in the evolution"
							+ " history of '" + featureModel.eResource().getURI().lastSegment() + "'."
							+ " Please reference the feature ID instead!");
			return;
		}
		else if(matchingFeatures.size() == 1) {
			result.addMapping(identifier, matchingFeatures.get(0));
			return;
		}

		DwTemporalElement matchingFeature = ReferenceResolveUtil.getTemporalElementWithMatchingId(featureModel, identifier);
		if(matchingFeature != null && matchingFeature instanceof DwFeature) {
			result.addMapping(identifier, (DwFeature) matchingFeature);
			return;
		}
		
		result.setErrorMessage("The feature model '" + featureModel.eResource().getURI().lastSegment() + "' does not contain a feature with id or name '" + identifier + "'.");
	}
	
	private List<DwFeature> getAllFeaturesWithMatchingName(List<DwFeature> featureList, EObject object, String identifier) {
		for(EObject child : object.eContents()) {
			if(child instanceof DwFeature)
				for(DwName name : ((DwFeature) child).getNames())
					if(name.getName().equals(identifier))
						featureList.add((DwFeature) child);
			
			getAllFeaturesWithMatchingName(featureList, child, identifier);
		}
		
		return featureList;
	}
	
	
	
	public String deResolve(de.darwinspl.feature.DwFeature element, de.darwinspl.fmec.property.FeatureReference container, EReference reference) {
		return delegate.deResolve(element, container, reference);
	}
	
	public void setOptions(Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
