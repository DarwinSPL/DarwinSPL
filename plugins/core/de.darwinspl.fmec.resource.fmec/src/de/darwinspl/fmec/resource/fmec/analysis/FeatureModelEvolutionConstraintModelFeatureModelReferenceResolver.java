/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.analysis;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

import de.darwinspl.common.ecore.util.DwEcoreUtil;
import de.darwinspl.feature.DwTemporalFeatureModel;

public class FeatureModelEvolutionConstraintModelFeatureModelReferenceResolver implements de.darwinspl.fmec.resource.fmec.IFmecReferenceResolver<de.darwinspl.fmec.FeatureModelEvolutionConstraintModel, de.darwinspl.feature.DwTemporalFeatureModel> {
	
	private de.darwinspl.fmec.resource.fmec.analysis.FmecDefaultResolverDelegate<de.darwinspl.fmec.FeatureModelEvolutionConstraintModel, de.darwinspl.feature.DwTemporalFeatureModel> delegate = new de.darwinspl.fmec.resource.fmec.analysis.FmecDefaultResolverDelegate<de.darwinspl.fmec.FeatureModelEvolutionConstraintModel, de.darwinspl.feature.DwTemporalFeatureModel>();
	
	public void resolve(String identifier, de.darwinspl.fmec.FeatureModelEvolutionConstraintModel container, EReference reference, int position, boolean resolveFuzzy, final de.darwinspl.fmec.resource.fmec.IFmecReferenceResolveResult<de.darwinspl.feature.DwTemporalFeatureModel> result) {
		try {
			EObject root = DwEcoreUtil.resolveFeatureModelReference(identifier, container);
			
			if(root instanceof DwTemporalFeatureModel)
				result.addMapping(identifier, (DwTemporalFeatureModel) root);
		} catch(Exception e) {
			result.setErrorMessage(e.getMessage());
		}
	}
	
	public String deResolve(de.darwinspl.feature.DwTemporalFeatureModel element, de.darwinspl.fmec.FeatureModelEvolutionConstraintModel container, EReference reference) {
		return DwEcoreUtil.deresolveFeatureModelReference(element, container);
	}
	
	public void setOptions(Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
