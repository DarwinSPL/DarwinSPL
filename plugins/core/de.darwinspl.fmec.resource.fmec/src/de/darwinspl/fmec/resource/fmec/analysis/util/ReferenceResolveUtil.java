package de.darwinspl.fmec.resource.fmec.analysis.util;

import org.eclipse.emf.ecore.EObject;

import de.darwinspl.fmec.FeatureModelEvolutionConstraintModel;
import de.darwinspl.temporal.DwTemporalElement;

public class ReferenceResolveUtil {
	
	public static FeatureModelEvolutionConstraintModel getRoot(EObject o) {
		if(o.eContainer() == null) {
			if(o instanceof FeatureModelEvolutionConstraintModel)
				return (FeatureModelEvolutionConstraintModel) o;
			else
				throw new RuntimeException("Unexpected root element of type " + o.getClass().getSimpleName());
		}
		
		else return getRoot(o.eContainer());
	}
	
	public static DwTemporalElement getTemporalElementWithMatchingId(EObject object, String identifier) {
		for(EObject child : object.eContents()) {
			if(child instanceof DwTemporalElement)
				if(((DwTemporalElement) child).getId() != null)
					if(((DwTemporalElement) child).getId().equals(identifier))
						return (DwTemporalElement) child;
			
			DwTemporalElement matchingElement = getTemporalElementWithMatchingId(child, identifier);
			if(matchingElement != null)
				return matchingElement;
		}
		
		return null;
	}
	
}
