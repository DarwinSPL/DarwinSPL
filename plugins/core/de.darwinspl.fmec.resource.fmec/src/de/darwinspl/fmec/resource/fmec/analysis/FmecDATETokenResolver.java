/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.analysis;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

public class FmecDATETokenResolver implements de.darwinspl.fmec.resource.fmec.IFmecTokenResolver {
	
	private de.darwinspl.fmec.resource.fmec.analysis.FmecDefaultTokenResolver defaultTokenResolver = new de.darwinspl.fmec.resource.fmec.analysis.FmecDefaultTokenResolver(true);
	
	private SimpleDateFormat[] formats = new SimpleDateFormat[] {
			new SimpleDateFormat("dd.MM.yyyy H:mm"),
			new SimpleDateFormat("dd.MM.yyyy")
	};
	
	public String deResolve(Object value, EStructuralFeature feature, EObject container) {
		return formats[0].format(value);
	}
	
	public void resolve(String lexem, EStructuralFeature feature, de.darwinspl.fmec.resource.fmec.IFmecTokenResolveResult result) {
		for(SimpleDateFormat format : formats) {
			try {
				Date date = format.parse(lexem);
				result.setResolvedToken(date);
				return;
			} catch (ParseException e) {
				continue;
			}
		}
		
		result.setErrorMessage(lexem + " is not a valid date.");
	}
	
	public void setOptions(Map<?,?> options) {
		defaultTokenResolver.setOptions(options);
	}
	
}
