/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.fmec.resource.fmec.analysis;

import java.util.Map;
import org.eclipse.emf.ecore.EReference;

import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.fmec.resource.fmec.analysis.util.ReferenceResolveUtil;
import de.darwinspl.temporal.DwTemporalElement;
import de.darwinspl.fmec.FeatureModelEvolutionConstraintModel;

public class GroupReferenceReferencedGroupReferenceResolver implements de.darwinspl.fmec.resource.fmec.IFmecReferenceResolver<de.darwinspl.fmec.property.GroupReference, de.darwinspl.feature.DwGroup> {
	
	private de.darwinspl.fmec.resource.fmec.analysis.FmecDefaultResolverDelegate<de.darwinspl.fmec.property.GroupReference, de.darwinspl.feature.DwGroup> delegate = new de.darwinspl.fmec.resource.fmec.analysis.FmecDefaultResolverDelegate<de.darwinspl.fmec.property.GroupReference, de.darwinspl.feature.DwGroup>();
	
	public void resolve(String identifier, de.darwinspl.fmec.property.GroupReference container, EReference reference, int position, boolean resolveFuzzy, final de.darwinspl.fmec.resource.fmec.IFmecReferenceResolveResult<de.darwinspl.feature.DwGroup> result) {
		FeatureModelEvolutionConstraintModel root = ReferenceResolveUtil.getRoot(container);
		DwTemporalFeatureModel featureModel = root.getFeatureModel();

		identifier = identifier.replaceAll("\"", "");
		DwTemporalElement matchingFeature = ReferenceResolveUtil.getTemporalElementWithMatchingId(featureModel, identifier);
		if(matchingFeature != null && matchingFeature instanceof DwGroup) {
			result.addMapping(identifier, (DwGroup) matchingFeature);
			return;
		}
		
		result.setErrorMessage("The feature model '" + featureModel.eResource().getURI().lastSegment() + "' does not contain a group with id '" + identifier + "'.");
	}
	
	public String deResolve(de.darwinspl.feature.DwGroup element, de.darwinspl.fmec.property.GroupReference container, EReference reference) {
		return element.getId();
	}
	
	public void setOptions(Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
