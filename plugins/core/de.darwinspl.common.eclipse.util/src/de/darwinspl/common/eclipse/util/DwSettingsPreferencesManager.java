package de.darwinspl.common.eclipse.util;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.osgi.service.prefs.BackingStoreException;

public class DwSettingsPreferencesManager {
	
	private static String AUTO_FMEC;
	private static String MAUDE_DEBUG;
	private static String MAUDE_ADDRESS;
	
	public static final String MAUDE_ADDRESS_DEFAULT = "http://localhost:8080/";
	
	

	public static void init() {
		IEclipsePreferences pref = InstanceScope.INSTANCE.getNode("de.darwinspl.settings");

		if(pref.get("auto_fmec", null) == null) {
			pref.put("auto_fmec", String.valueOf(false));
		}
		if(pref.get("maude_debug", null) == null) {
			pref.put("maude_debug", String.valueOf(false));
		}
		if(pref.get("maude_address", null) == null) {
			pref.put("maude_address", MAUDE_ADDRESS_DEFAULT);
		}
		
		try {
			pref.flush();
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}

		AUTO_FMEC = pref.get("auto_fmec", null);
		MAUDE_DEBUG = pref.get("maude_debug", null);
		MAUDE_ADDRESS = pref.get("maude_address", null);
	}
	
	
	
	public static void setAutoFMEC(boolean value) {
		AUTO_FMEC = String.valueOf(value);
		
		IEclipsePreferences pref = InstanceScope.INSTANCE.getNode("de.darwinspl.settings");
		pref.put("auto_fmec", AUTO_FMEC);
		
		try {
			pref.flush();
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}
	}
	
	public static void setMaudeDebug(boolean value) {
		MAUDE_DEBUG = String.valueOf(value);
		
		IEclipsePreferences pref = InstanceScope.INSTANCE.getNode("de.darwinspl.settings");
		pref.put("maude_debug", MAUDE_DEBUG);
		
		try {
			pref.flush();
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}
	}
	
	public static void setMaudeAddress(String value) {
		MAUDE_ADDRESS = value;
		
		IEclipsePreferences pref = InstanceScope.INSTANCE.getNode("de.darwinspl.settings");
		pref.put("maude_address", MAUDE_ADDRESS);
		
		try {
			pref.flush();
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}
	}
	
	

	public static boolean isAutoFMEC() {
		if(AUTO_FMEC != null)
			return Boolean.valueOf(AUTO_FMEC);
		else
			init();
		
		return Boolean.valueOf(AUTO_FMEC);
	}

	public static boolean isMaudeDebug() {
		if(MAUDE_DEBUG != null)
			return Boolean.valueOf(MAUDE_DEBUG);
		else
			init();
		
		return Boolean.valueOf(MAUDE_DEBUG);
	}

	public static String getMaudeAddress() {
		if(MAUDE_ADDRESS != null)
			return MAUDE_ADDRESS;
		else
			init();
		
		return MAUDE_ADDRESS;
	}
	
}
