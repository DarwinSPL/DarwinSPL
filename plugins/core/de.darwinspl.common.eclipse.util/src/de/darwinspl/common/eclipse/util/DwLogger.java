package de.darwinspl.common.eclipse.util;

public class DwLogger {
	
	private static boolean supressLog = true;
//	private static String lastLog = null;
	
	public static void logWarning(String message) {
		if(!supressLog)
			log("+  DwWarning: " + message);
	}
	
	public static void logError(String message) {
		if(!supressLog)
			log("++   DwError: " + message);
	}
	
	private static void log(String message) {
//		if(message.equals(lastLog))
//			System.err.println("              again...");
//		else
			System.err.println(message);
		
//		lastLog = message;
	}

}
