/**
 */
package de.darwinspl.datatypes;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Enum Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.datatypes.DwEnumValue#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.datatypes.DwDatatypesPackage#getDwEnumValue()
 * @model
 * @generated
 */
public interface DwEnumValue extends DwValue {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' reference.
	 * @see #setValue(DwEnumLiteral)
	 * @see de.darwinspl.datatypes.DwDatatypesPackage#getDwEnumValue_Value()
	 * @model required="true"
	 * @generated
	 */
	DwEnumLiteral getValue();

	/**
	 * Sets the value of the '{@link de.darwinspl.datatypes.DwEnumValue#getValue <em>Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(DwEnumLiteral value);

} // DwEnumValue
