/**
 */
package de.darwinspl.datatypes;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Value</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.datatypes.DwDatatypesPackage#getDwValue()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface DwValue extends EObject {
} // DwValue
