/**
 */
package de.darwinspl.datatypes;

import de.darwinspl.temporal.DwTemporalModelPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.darwinspl.datatypes.DwDatatypesFactory
 * @model kind="package"
 * @generated
 */
public interface DwDatatypesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "datatypes";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.darwinspl.de/datatypes/2.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "datatypes";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DwDatatypesPackage eINSTANCE = de.darwinspl.datatypes.impl.DwDatatypesPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.darwinspl.datatypes.impl.DwEnumImpl <em>Dw Enum</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.datatypes.impl.DwEnumImpl
	 * @see de.darwinspl.datatypes.impl.DwDatatypesPackageImpl#getDwEnum()
	 * @generated
	 */
	int DW_ENUM = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM__ID = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Validities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM__VALIDITIES = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT__VALIDITIES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM__NAME = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Literals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM__LITERALS = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Dw Enum</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_FEATURE_COUNT = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM___CREATE_ID = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM___IS_VALID__DATE = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT___IS_VALID__DATE;

	/**
	 * The number of operations of the '<em>Dw Enum</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_OPERATION_COUNT = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.datatypes.impl.DwEnumLiteralImpl <em>Dw Enum Literal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.datatypes.impl.DwEnumLiteralImpl
	 * @see de.darwinspl.datatypes.impl.DwDatatypesPackageImpl#getDwEnumLiteral()
	 * @generated
	 */
	int DW_ENUM_LITERAL = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_LITERAL__ID = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Validities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_LITERAL__VALIDITIES = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT__VALIDITIES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_LITERAL__NAME = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_LITERAL__VALUE = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Enum</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_LITERAL__ENUM = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Dw Enum Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_LITERAL_FEATURE_COUNT = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_LITERAL___CREATE_ID = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_LITERAL___IS_VALID__DATE = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT___IS_VALID__DATE;

	/**
	 * The number of operations of the '<em>Dw Enum Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_LITERAL_OPERATION_COUNT = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.datatypes.DwValue <em>Dw Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.datatypes.DwValue
	 * @see de.darwinspl.datatypes.impl.DwDatatypesPackageImpl#getDwValue()
	 * @generated
	 */
	int DW_VALUE = 2;

	/**
	 * The number of structural features of the '<em>Dw Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_VALUE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Dw Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_VALUE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.datatypes.impl.DwNumberValueImpl <em>Dw Number Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.datatypes.impl.DwNumberValueImpl
	 * @see de.darwinspl.datatypes.impl.DwDatatypesPackageImpl#getDwNumberValue()
	 * @generated
	 */
	int DW_NUMBER_VALUE = 3;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NUMBER_VALUE__VALUE = DW_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Dw Number Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NUMBER_VALUE_FEATURE_COUNT = DW_VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Dw Number Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NUMBER_VALUE_OPERATION_COUNT = DW_VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.datatypes.impl.DwBooleanValueImpl <em>Dw Boolean Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.datatypes.impl.DwBooleanValueImpl
	 * @see de.darwinspl.datatypes.impl.DwDatatypesPackageImpl#getDwBooleanValue()
	 * @generated
	 */
	int DW_BOOLEAN_VALUE = 4;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_BOOLEAN_VALUE__VALUE = DW_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Dw Boolean Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_BOOLEAN_VALUE_FEATURE_COUNT = DW_VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Dw Boolean Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_BOOLEAN_VALUE_OPERATION_COUNT = DW_VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.datatypes.impl.DwStringValueImpl <em>Dw String Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.datatypes.impl.DwStringValueImpl
	 * @see de.darwinspl.datatypes.impl.DwDatatypesPackageImpl#getDwStringValue()
	 * @generated
	 */
	int DW_STRING_VALUE = 5;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_STRING_VALUE__VALUE = DW_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Dw String Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_STRING_VALUE_FEATURE_COUNT = DW_VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Dw String Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_STRING_VALUE_OPERATION_COUNT = DW_VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.datatypes.impl.DwEnumValueImpl <em>Dw Enum Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.datatypes.impl.DwEnumValueImpl
	 * @see de.darwinspl.datatypes.impl.DwDatatypesPackageImpl#getDwEnumValue()
	 * @generated
	 */
	int DW_ENUM_VALUE = 6;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_VALUE__VALUE = DW_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Dw Enum Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_VALUE_FEATURE_COUNT = DW_VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Dw Enum Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_VALUE_OPERATION_COUNT = DW_VALUE_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link de.darwinspl.datatypes.DwEnum <em>Dw Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Enum</em>'.
	 * @see de.darwinspl.datatypes.DwEnum
	 * @generated
	 */
	EClass getDwEnum();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.datatypes.DwEnum#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.darwinspl.datatypes.DwEnum#getName()
	 * @see #getDwEnum()
	 * @generated
	 */
	EAttribute getDwEnum_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link de.darwinspl.datatypes.DwEnum#getLiterals <em>Literals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Literals</em>'.
	 * @see de.darwinspl.datatypes.DwEnum#getLiterals()
	 * @see #getDwEnum()
	 * @generated
	 */
	EReference getDwEnum_Literals();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.datatypes.DwEnumLiteral <em>Dw Enum Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Enum Literal</em>'.
	 * @see de.darwinspl.datatypes.DwEnumLiteral
	 * @generated
	 */
	EClass getDwEnumLiteral();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.datatypes.DwEnumLiteral#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.darwinspl.datatypes.DwEnumLiteral#getName()
	 * @see #getDwEnumLiteral()
	 * @generated
	 */
	EAttribute getDwEnumLiteral_Name();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.datatypes.DwEnumLiteral#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see de.darwinspl.datatypes.DwEnumLiteral#getValue()
	 * @see #getDwEnumLiteral()
	 * @generated
	 */
	EAttribute getDwEnumLiteral_Value();

	/**
	 * Returns the meta object for the container reference '{@link de.darwinspl.datatypes.DwEnumLiteral#getEnum <em>Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Enum</em>'.
	 * @see de.darwinspl.datatypes.DwEnumLiteral#getEnum()
	 * @see #getDwEnumLiteral()
	 * @generated
	 */
	EReference getDwEnumLiteral_Enum();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.datatypes.DwValue <em>Dw Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Value</em>'.
	 * @see de.darwinspl.datatypes.DwValue
	 * @generated
	 */
	EClass getDwValue();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.datatypes.DwNumberValue <em>Dw Number Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Number Value</em>'.
	 * @see de.darwinspl.datatypes.DwNumberValue
	 * @generated
	 */
	EClass getDwNumberValue();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.datatypes.DwNumberValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see de.darwinspl.datatypes.DwNumberValue#getValue()
	 * @see #getDwNumberValue()
	 * @generated
	 */
	EAttribute getDwNumberValue_Value();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.datatypes.DwBooleanValue <em>Dw Boolean Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Boolean Value</em>'.
	 * @see de.darwinspl.datatypes.DwBooleanValue
	 * @generated
	 */
	EClass getDwBooleanValue();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.datatypes.DwBooleanValue#isValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see de.darwinspl.datatypes.DwBooleanValue#isValue()
	 * @see #getDwBooleanValue()
	 * @generated
	 */
	EAttribute getDwBooleanValue_Value();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.datatypes.DwStringValue <em>Dw String Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw String Value</em>'.
	 * @see de.darwinspl.datatypes.DwStringValue
	 * @generated
	 */
	EClass getDwStringValue();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.datatypes.DwStringValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see de.darwinspl.datatypes.DwStringValue#getValue()
	 * @see #getDwStringValue()
	 * @generated
	 */
	EAttribute getDwStringValue_Value();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.datatypes.DwEnumValue <em>Dw Enum Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Enum Value</em>'.
	 * @see de.darwinspl.datatypes.DwEnumValue
	 * @generated
	 */
	EClass getDwEnumValue();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.datatypes.DwEnumValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see de.darwinspl.datatypes.DwEnumValue#getValue()
	 * @see #getDwEnumValue()
	 * @generated
	 */
	EReference getDwEnumValue_Value();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DwDatatypesFactory getDwDatatypesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.darwinspl.datatypes.impl.DwEnumImpl <em>Dw Enum</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.datatypes.impl.DwEnumImpl
		 * @see de.darwinspl.datatypes.impl.DwDatatypesPackageImpl#getDwEnum()
		 * @generated
		 */
		EClass DW_ENUM = eINSTANCE.getDwEnum();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_ENUM__NAME = eINSTANCE.getDwEnum_Name();

		/**
		 * The meta object literal for the '<em><b>Literals</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_ENUM__LITERALS = eINSTANCE.getDwEnum_Literals();

		/**
		 * The meta object literal for the '{@link de.darwinspl.datatypes.impl.DwEnumLiteralImpl <em>Dw Enum Literal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.datatypes.impl.DwEnumLiteralImpl
		 * @see de.darwinspl.datatypes.impl.DwDatatypesPackageImpl#getDwEnumLiteral()
		 * @generated
		 */
		EClass DW_ENUM_LITERAL = eINSTANCE.getDwEnumLiteral();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_ENUM_LITERAL__NAME = eINSTANCE.getDwEnumLiteral_Name();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_ENUM_LITERAL__VALUE = eINSTANCE.getDwEnumLiteral_Value();

		/**
		 * The meta object literal for the '<em><b>Enum</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_ENUM_LITERAL__ENUM = eINSTANCE.getDwEnumLiteral_Enum();

		/**
		 * The meta object literal for the '{@link de.darwinspl.datatypes.DwValue <em>Dw Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.datatypes.DwValue
		 * @see de.darwinspl.datatypes.impl.DwDatatypesPackageImpl#getDwValue()
		 * @generated
		 */
		EClass DW_VALUE = eINSTANCE.getDwValue();

		/**
		 * The meta object literal for the '{@link de.darwinspl.datatypes.impl.DwNumberValueImpl <em>Dw Number Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.datatypes.impl.DwNumberValueImpl
		 * @see de.darwinspl.datatypes.impl.DwDatatypesPackageImpl#getDwNumberValue()
		 * @generated
		 */
		EClass DW_NUMBER_VALUE = eINSTANCE.getDwNumberValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_NUMBER_VALUE__VALUE = eINSTANCE.getDwNumberValue_Value();

		/**
		 * The meta object literal for the '{@link de.darwinspl.datatypes.impl.DwBooleanValueImpl <em>Dw Boolean Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.datatypes.impl.DwBooleanValueImpl
		 * @see de.darwinspl.datatypes.impl.DwDatatypesPackageImpl#getDwBooleanValue()
		 * @generated
		 */
		EClass DW_BOOLEAN_VALUE = eINSTANCE.getDwBooleanValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_BOOLEAN_VALUE__VALUE = eINSTANCE.getDwBooleanValue_Value();

		/**
		 * The meta object literal for the '{@link de.darwinspl.datatypes.impl.DwStringValueImpl <em>Dw String Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.datatypes.impl.DwStringValueImpl
		 * @see de.darwinspl.datatypes.impl.DwDatatypesPackageImpl#getDwStringValue()
		 * @generated
		 */
		EClass DW_STRING_VALUE = eINSTANCE.getDwStringValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_STRING_VALUE__VALUE = eINSTANCE.getDwStringValue_Value();

		/**
		 * The meta object literal for the '{@link de.darwinspl.datatypes.impl.DwEnumValueImpl <em>Dw Enum Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.datatypes.impl.DwEnumValueImpl
		 * @see de.darwinspl.datatypes.impl.DwDatatypesPackageImpl#getDwEnumValue()
		 * @generated
		 */
		EClass DW_ENUM_VALUE = eINSTANCE.getDwEnumValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_ENUM_VALUE__VALUE = eINSTANCE.getDwEnumValue_Value();

	}

} //DwDatatypesPackage
