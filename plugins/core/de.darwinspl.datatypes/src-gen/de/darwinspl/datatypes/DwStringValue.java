/**
 */
package de.darwinspl.datatypes;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw String Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.datatypes.DwStringValue#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.datatypes.DwDatatypesPackage#getDwStringValue()
 * @model
 * @generated
 */
public interface DwStringValue extends DwValue {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see de.darwinspl.datatypes.DwDatatypesPackage#getDwStringValue_Value()
	 * @model required="true"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link de.darwinspl.datatypes.DwStringValue#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

} // DwStringValue
