/**
 */
package de.darwinspl.datatypes;

import de.darwinspl.temporal.DwMultiTemporalIntervalElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Enum</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.datatypes.DwEnum#getName <em>Name</em>}</li>
 *   <li>{@link de.darwinspl.datatypes.DwEnum#getLiterals <em>Literals</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.datatypes.DwDatatypesPackage#getDwEnum()
 * @model
 * @generated
 */
public interface DwEnum extends DwMultiTemporalIntervalElement {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see de.darwinspl.datatypes.DwDatatypesPackage#getDwEnum_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link de.darwinspl.datatypes.DwEnum#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Literals</b></em>' containment reference list.
	 * The list contents are of type {@link de.darwinspl.datatypes.DwEnumLiteral}.
	 * It is bidirectional and its opposite is '{@link de.darwinspl.datatypes.DwEnumLiteral#getEnum <em>Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Literals</em>' containment reference list.
	 * @see de.darwinspl.datatypes.DwDatatypesPackage#getDwEnum_Literals()
	 * @see de.darwinspl.datatypes.DwEnumLiteral#getEnum
	 * @model opposite="enum" containment="true"
	 * @generated
	 */
	EList<DwEnumLiteral> getLiterals();

} // DwEnum
