/**
 */
package de.darwinspl.datatypes;

import de.darwinspl.temporal.DwMultiTemporalIntervalElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Enum Literal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.datatypes.DwEnumLiteral#getName <em>Name</em>}</li>
 *   <li>{@link de.darwinspl.datatypes.DwEnumLiteral#getValue <em>Value</em>}</li>
 *   <li>{@link de.darwinspl.datatypes.DwEnumLiteral#getEnum <em>Enum</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.datatypes.DwDatatypesPackage#getDwEnumLiteral()
 * @model
 * @generated
 */
public interface DwEnumLiteral extends DwMultiTemporalIntervalElement {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see de.darwinspl.datatypes.DwDatatypesPackage#getDwEnumLiteral_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link de.darwinspl.datatypes.DwEnumLiteral#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(int)
	 * @see de.darwinspl.datatypes.DwDatatypesPackage#getDwEnumLiteral_Value()
	 * @model required="true"
	 * @generated
	 */
	int getValue();

	/**
	 * Sets the value of the '{@link de.darwinspl.datatypes.DwEnumLiteral#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(int value);

	/**
	 * Returns the value of the '<em><b>Enum</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.darwinspl.datatypes.DwEnum#getLiterals <em>Literals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enum</em>' container reference.
	 * @see #setEnum(DwEnum)
	 * @see de.darwinspl.datatypes.DwDatatypesPackage#getDwEnumLiteral_Enum()
	 * @see de.darwinspl.datatypes.DwEnum#getLiterals
	 * @model opposite="literals" required="true" transient="false"
	 * @generated
	 */
	DwEnum getEnum();

	/**
	 * Sets the value of the '{@link de.darwinspl.datatypes.DwEnumLiteral#getEnum <em>Enum</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enum</em>' container reference.
	 * @see #getEnum()
	 * @generated
	 */
	void setEnum(DwEnum value);

} // DwEnumLiteral
