/**
 */
package de.darwinspl.datatypes.impl;

import de.darwinspl.datatypes.DwBooleanValue;
import de.darwinspl.datatypes.DwDatatypesFactory;
import de.darwinspl.datatypes.DwDatatypesPackage;
import de.darwinspl.datatypes.DwEnum;
import de.darwinspl.datatypes.DwEnumLiteral;
import de.darwinspl.datatypes.DwEnumValue;
import de.darwinspl.datatypes.DwNumberValue;
import de.darwinspl.datatypes.DwStringValue;
import de.darwinspl.datatypes.DwValue;

import de.darwinspl.temporal.DwTemporalModelPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DwDatatypesPackageImpl extends EPackageImpl implements DwDatatypesPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwEnumEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwEnumLiteralEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwNumberValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwBooleanValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwStringValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwEnumValueEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.darwinspl.datatypes.DwDatatypesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private DwDatatypesPackageImpl() {
		super(eNS_URI, DwDatatypesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link DwDatatypesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static DwDatatypesPackage init() {
		if (isInited) return (DwDatatypesPackage)EPackage.Registry.INSTANCE.getEPackage(DwDatatypesPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredDwDatatypesPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		DwDatatypesPackageImpl theDwDatatypesPackage = registeredDwDatatypesPackage instanceof DwDatatypesPackageImpl ? (DwDatatypesPackageImpl)registeredDwDatatypesPackage : new DwDatatypesPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		DwTemporalModelPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theDwDatatypesPackage.createPackageContents();

		// Initialize created meta-data
		theDwDatatypesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theDwDatatypesPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(DwDatatypesPackage.eNS_URI, theDwDatatypesPackage);
		return theDwDatatypesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwEnum() {
		return dwEnumEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwEnum_Name() {
		return (EAttribute)dwEnumEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwEnum_Literals() {
		return (EReference)dwEnumEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwEnumLiteral() {
		return dwEnumLiteralEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwEnumLiteral_Name() {
		return (EAttribute)dwEnumLiteralEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwEnumLiteral_Value() {
		return (EAttribute)dwEnumLiteralEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwEnumLiteral_Enum() {
		return (EReference)dwEnumLiteralEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwValue() {
		return dwValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwNumberValue() {
		return dwNumberValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwNumberValue_Value() {
		return (EAttribute)dwNumberValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwBooleanValue() {
		return dwBooleanValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwBooleanValue_Value() {
		return (EAttribute)dwBooleanValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwStringValue() {
		return dwStringValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwStringValue_Value() {
		return (EAttribute)dwStringValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwEnumValue() {
		return dwEnumValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwEnumValue_Value() {
		return (EReference)dwEnumValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwDatatypesFactory getDwDatatypesFactory() {
		return (DwDatatypesFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		dwEnumEClass = createEClass(DW_ENUM);
		createEAttribute(dwEnumEClass, DW_ENUM__NAME);
		createEReference(dwEnumEClass, DW_ENUM__LITERALS);

		dwEnumLiteralEClass = createEClass(DW_ENUM_LITERAL);
		createEAttribute(dwEnumLiteralEClass, DW_ENUM_LITERAL__NAME);
		createEAttribute(dwEnumLiteralEClass, DW_ENUM_LITERAL__VALUE);
		createEReference(dwEnumLiteralEClass, DW_ENUM_LITERAL__ENUM);

		dwValueEClass = createEClass(DW_VALUE);

		dwNumberValueEClass = createEClass(DW_NUMBER_VALUE);
		createEAttribute(dwNumberValueEClass, DW_NUMBER_VALUE__VALUE);

		dwBooleanValueEClass = createEClass(DW_BOOLEAN_VALUE);
		createEAttribute(dwBooleanValueEClass, DW_BOOLEAN_VALUE__VALUE);

		dwStringValueEClass = createEClass(DW_STRING_VALUE);
		createEAttribute(dwStringValueEClass, DW_STRING_VALUE__VALUE);

		dwEnumValueEClass = createEClass(DW_ENUM_VALUE);
		createEReference(dwEnumValueEClass, DW_ENUM_VALUE__VALUE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		DwTemporalModelPackage theDwTemporalModelPackage = (DwTemporalModelPackage)EPackage.Registry.INSTANCE.getEPackage(DwTemporalModelPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		dwEnumEClass.getESuperTypes().add(theDwTemporalModelPackage.getDwMultiTemporalIntervalElement());
		dwEnumLiteralEClass.getESuperTypes().add(theDwTemporalModelPackage.getDwMultiTemporalIntervalElement());
		dwNumberValueEClass.getESuperTypes().add(this.getDwValue());
		dwBooleanValueEClass.getESuperTypes().add(this.getDwValue());
		dwStringValueEClass.getESuperTypes().add(this.getDwValue());
		dwEnumValueEClass.getESuperTypes().add(this.getDwValue());

		// Initialize classes, features, and operations; add parameters
		initEClass(dwEnumEClass, DwEnum.class, "DwEnum", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDwEnum_Name(), ecorePackage.getEString(), "name", null, 1, 1, DwEnum.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwEnum_Literals(), this.getDwEnumLiteral(), this.getDwEnumLiteral_Enum(), "literals", null, 0, -1, DwEnum.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwEnumLiteralEClass, DwEnumLiteral.class, "DwEnumLiteral", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDwEnumLiteral_Name(), ecorePackage.getEString(), "name", null, 1, 1, DwEnumLiteral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDwEnumLiteral_Value(), ecorePackage.getEInt(), "value", null, 1, 1, DwEnumLiteral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwEnumLiteral_Enum(), this.getDwEnum(), this.getDwEnum_Literals(), "enum", null, 1, 1, DwEnumLiteral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwValueEClass, DwValue.class, "DwValue", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dwNumberValueEClass, DwNumberValue.class, "DwNumberValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDwNumberValue_Value(), ecorePackage.getEDouble(), "value", null, 1, 1, DwNumberValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwBooleanValueEClass, DwBooleanValue.class, "DwBooleanValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDwBooleanValue_Value(), ecorePackage.getEBoolean(), "value", null, 1, 1, DwBooleanValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwStringValueEClass, DwStringValue.class, "DwStringValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDwStringValue_Value(), ecorePackage.getEString(), "value", null, 1, 1, DwStringValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwEnumValueEClass, DwEnumValue.class, "DwEnumValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwEnumValue_Value(), this.getDwEnumLiteral(), null, "value", null, 1, 1, DwEnumValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //DwDatatypesPackageImpl
