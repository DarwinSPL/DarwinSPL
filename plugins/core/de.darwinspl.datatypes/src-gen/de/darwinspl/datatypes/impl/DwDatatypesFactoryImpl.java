/**
 */
package de.darwinspl.datatypes.impl;

import de.darwinspl.datatypes.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DwDatatypesFactoryImpl extends EFactoryImpl implements DwDatatypesFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DwDatatypesFactory init() {
		try {
			DwDatatypesFactory theDwDatatypesFactory = (DwDatatypesFactory)EPackage.Registry.INSTANCE.getEFactory(DwDatatypesPackage.eNS_URI);
			if (theDwDatatypesFactory != null) {
				return theDwDatatypesFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DwDatatypesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwDatatypesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case DwDatatypesPackage.DW_ENUM: return createDwEnum();
			case DwDatatypesPackage.DW_ENUM_LITERAL: return createDwEnumLiteral();
			case DwDatatypesPackage.DW_NUMBER_VALUE: return createDwNumberValue();
			case DwDatatypesPackage.DW_BOOLEAN_VALUE: return createDwBooleanValue();
			case DwDatatypesPackage.DW_STRING_VALUE: return createDwStringValue();
			case DwDatatypesPackage.DW_ENUM_VALUE: return createDwEnumValue();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwEnum createDwEnum() {
		DwEnumImpl dwEnum = new DwEnumImpl();
		return dwEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwEnumLiteral createDwEnumLiteral() {
		DwEnumLiteralImpl dwEnumLiteral = new DwEnumLiteralImpl();
		return dwEnumLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwNumberValue createDwNumberValue() {
		DwNumberValueImpl dwNumberValue = new DwNumberValueImpl();
		return dwNumberValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwBooleanValue createDwBooleanValue() {
		DwBooleanValueImpl dwBooleanValue = new DwBooleanValueImpl();
		return dwBooleanValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwStringValue createDwStringValue() {
		DwStringValueImpl dwStringValue = new DwStringValueImpl();
		return dwStringValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwEnumValue createDwEnumValue() {
		DwEnumValueImpl dwEnumValue = new DwEnumValueImpl();
		return dwEnumValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwDatatypesPackage getDwDatatypesPackage() {
		return (DwDatatypesPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DwDatatypesPackage getPackage() {
		return DwDatatypesPackage.eINSTANCE;
	}

} //DwDatatypesFactoryImpl
