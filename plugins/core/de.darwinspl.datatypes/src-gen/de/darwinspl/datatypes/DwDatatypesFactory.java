/**
 */
package de.darwinspl.datatypes;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.darwinspl.datatypes.DwDatatypesPackage
 * @generated
 */
public interface DwDatatypesFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DwDatatypesFactory eINSTANCE = de.darwinspl.datatypes.impl.DwDatatypesFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Dw Enum</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Enum</em>'.
	 * @generated
	 */
	DwEnum createDwEnum();

	/**
	 * Returns a new object of class '<em>Dw Enum Literal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Enum Literal</em>'.
	 * @generated
	 */
	DwEnumLiteral createDwEnumLiteral();

	/**
	 * Returns a new object of class '<em>Dw Number Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Number Value</em>'.
	 * @generated
	 */
	DwNumberValue createDwNumberValue();

	/**
	 * Returns a new object of class '<em>Dw Boolean Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Boolean Value</em>'.
	 * @generated
	 */
	DwBooleanValue createDwBooleanValue();

	/**
	 * Returns a new object of class '<em>Dw String Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw String Value</em>'.
	 * @generated
	 */
	DwStringValue createDwStringValue();

	/**
	 * Returns a new object of class '<em>Dw Enum Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Enum Value</em>'.
	 * @generated
	 */
	DwEnumValue createDwEnumValue();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	DwDatatypesPackage getDwDatatypesPackage();

} //DwDatatypesFactory
