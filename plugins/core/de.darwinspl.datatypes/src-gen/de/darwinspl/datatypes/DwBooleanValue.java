/**
 */
package de.darwinspl.datatypes;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Boolean Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.datatypes.DwBooleanValue#isValue <em>Value</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.datatypes.DwDatatypesPackage#getDwBooleanValue()
 * @model
 * @generated
 */
public interface DwBooleanValue extends DwValue {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(boolean)
	 * @see de.darwinspl.datatypes.DwDatatypesPackage#getDwBooleanValue_Value()
	 * @model required="true"
	 * @generated
	 */
	boolean isValue();

	/**
	 * Sets the value of the '{@link de.darwinspl.datatypes.DwBooleanValue#isValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #isValue()
	 * @generated
	 */
	void setValue(boolean value);

} // DwBooleanValue
