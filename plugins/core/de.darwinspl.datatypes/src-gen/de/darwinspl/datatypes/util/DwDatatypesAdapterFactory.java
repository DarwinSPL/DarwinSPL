/**
 */
package de.darwinspl.datatypes.util;

import de.darwinspl.datatypes.*;

import de.darwinspl.temporal.DwElementWithId;
import de.darwinspl.temporal.DwMultiTemporalIntervalElement;
import de.darwinspl.temporal.DwTemporalElement;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see de.darwinspl.datatypes.DwDatatypesPackage
 * @generated
 */
public class DwDatatypesAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DwDatatypesPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwDatatypesAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = DwDatatypesPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwDatatypesSwitch<Adapter> modelSwitch =
		new DwDatatypesSwitch<Adapter>() {
			@Override
			public Adapter caseDwEnum(DwEnum object) {
				return createDwEnumAdapter();
			}
			@Override
			public Adapter caseDwEnumLiteral(DwEnumLiteral object) {
				return createDwEnumLiteralAdapter();
			}
			@Override
			public Adapter caseDwValue(DwValue object) {
				return createDwValueAdapter();
			}
			@Override
			public Adapter caseDwNumberValue(DwNumberValue object) {
				return createDwNumberValueAdapter();
			}
			@Override
			public Adapter caseDwBooleanValue(DwBooleanValue object) {
				return createDwBooleanValueAdapter();
			}
			@Override
			public Adapter caseDwStringValue(DwStringValue object) {
				return createDwStringValueAdapter();
			}
			@Override
			public Adapter caseDwEnumValue(DwEnumValue object) {
				return createDwEnumValueAdapter();
			}
			@Override
			public Adapter caseDwElementWithId(DwElementWithId object) {
				return createDwElementWithIdAdapter();
			}
			@Override
			public Adapter caseDwTemporalElement(DwTemporalElement object) {
				return createDwTemporalElementAdapter();
			}
			@Override
			public Adapter caseDwMultiTemporalIntervalElement(DwMultiTemporalIntervalElement object) {
				return createDwMultiTemporalIntervalElementAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.datatypes.DwEnum <em>Dw Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.datatypes.DwEnum
	 * @generated
	 */
	public Adapter createDwEnumAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.datatypes.DwEnumLiteral <em>Dw Enum Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.datatypes.DwEnumLiteral
	 * @generated
	 */
	public Adapter createDwEnumLiteralAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.datatypes.DwValue <em>Dw Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.datatypes.DwValue
	 * @generated
	 */
	public Adapter createDwValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.datatypes.DwNumberValue <em>Dw Number Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.datatypes.DwNumberValue
	 * @generated
	 */
	public Adapter createDwNumberValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.datatypes.DwBooleanValue <em>Dw Boolean Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.datatypes.DwBooleanValue
	 * @generated
	 */
	public Adapter createDwBooleanValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.datatypes.DwStringValue <em>Dw String Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.datatypes.DwStringValue
	 * @generated
	 */
	public Adapter createDwStringValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.datatypes.DwEnumValue <em>Dw Enum Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.datatypes.DwEnumValue
	 * @generated
	 */
	public Adapter createDwEnumValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.temporal.DwElementWithId <em>Dw Element With Id</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.temporal.DwElementWithId
	 * @generated
	 */
	public Adapter createDwElementWithIdAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.temporal.DwTemporalElement <em>Dw Temporal Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.temporal.DwTemporalElement
	 * @generated
	 */
	public Adapter createDwTemporalElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.temporal.DwMultiTemporalIntervalElement <em>Dw Multi Temporal Interval Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.temporal.DwMultiTemporalIntervalElement
	 * @generated
	 */
	public Adapter createDwMultiTemporalIntervalElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //DwDatatypesAdapterFactory
