/**
 */
package de.darwinspl.datatypes;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Number Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.datatypes.DwNumberValue#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.datatypes.DwDatatypesPackage#getDwNumberValue()
 * @model
 * @generated
 */
public interface DwNumberValue extends DwValue {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(double)
	 * @see de.darwinspl.datatypes.DwDatatypesPackage#getDwNumberValue_Value()
	 * @model required="true"
	 * @generated
	 */
	double getValue();

	/**
	 * Sets the value of the '{@link de.darwinspl.datatypes.DwNumberValue#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(double value);

} // DwNumberValue
