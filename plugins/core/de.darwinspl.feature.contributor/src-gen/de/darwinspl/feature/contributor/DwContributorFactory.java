/**
 */
package de.darwinspl.feature.contributor;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.darwinspl.feature.contributor.DwContributorPackage
 * @generated
 */
public interface DwContributorFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DwContributorFactory eINSTANCE = de.darwinspl.feature.contributor.impl.DwContributorFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Dw Contributor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Contributor</em>'.
	 * @generated
	 */
	DwContributor createDwContributor();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	DwContributorPackage getDwContributorPackage();

} //DwContributorFactory
