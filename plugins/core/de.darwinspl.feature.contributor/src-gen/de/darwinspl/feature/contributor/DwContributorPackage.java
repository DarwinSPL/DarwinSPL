/**
 */
package de.darwinspl.feature.contributor;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.darwinspl.feature.contributor.DwContributorFactory
 * @model kind="package"
 * @generated
 */
public interface DwContributorPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "contributor";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.darwinspl.de/feature/contributor/2.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "contributor";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DwContributorPackage eINSTANCE = de.darwinspl.feature.contributor.impl.DwContributorPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.contributor.impl.DwContributorImpl <em>Dw Contributor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.contributor.impl.DwContributorImpl
	 * @see de.darwinspl.feature.contributor.impl.DwContributorPackageImpl#getDwContributor()
	 * @generated
	 */
	int DW_CONTRIBUTOR = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONTRIBUTOR__ID = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONTRIBUTOR__NAME = 1;

	/**
	 * The number of structural features of the '<em>Dw Contributor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONTRIBUTOR_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Dw Contributor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONTRIBUTOR_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.contributor.DwContributor <em>Dw Contributor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Contributor</em>'.
	 * @see de.darwinspl.feature.contributor.DwContributor
	 * @generated
	 */
	EClass getDwContributor();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.contributor.DwContributor#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see de.darwinspl.feature.contributor.DwContributor#getId()
	 * @see #getDwContributor()
	 * @generated
	 */
	EAttribute getDwContributor_Id();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.contributor.DwContributor#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.darwinspl.feature.contributor.DwContributor#getName()
	 * @see #getDwContributor()
	 * @generated
	 */
	EAttribute getDwContributor_Name();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DwContributorFactory getDwContributorFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.contributor.impl.DwContributorImpl <em>Dw Contributor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.contributor.impl.DwContributorImpl
		 * @see de.darwinspl.feature.contributor.impl.DwContributorPackageImpl#getDwContributor()
		 * @generated
		 */
		EClass DW_CONTRIBUTOR = eINSTANCE.getDwContributor();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_CONTRIBUTOR__ID = eINSTANCE.getDwContributor_Id();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_CONTRIBUTOR__NAME = eINSTANCE.getDwContributor_Name();

	}

} //DwContributorPackage
