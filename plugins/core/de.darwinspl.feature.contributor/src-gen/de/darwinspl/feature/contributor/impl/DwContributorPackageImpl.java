/**
 */
package de.darwinspl.feature.contributor.impl;

import de.darwinspl.feature.contributor.DwContributor;
import de.darwinspl.feature.contributor.DwContributorFactory;
import de.darwinspl.feature.contributor.DwContributorPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DwContributorPackageImpl extends EPackageImpl implements DwContributorPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwContributorEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.darwinspl.feature.contributor.DwContributorPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private DwContributorPackageImpl() {
		super(eNS_URI, DwContributorFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link DwContributorPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static DwContributorPackage init() {
		if (isInited) return (DwContributorPackage)EPackage.Registry.INSTANCE.getEPackage(DwContributorPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredDwContributorPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		DwContributorPackageImpl theDwContributorPackage = registeredDwContributorPackage instanceof DwContributorPackageImpl ? (DwContributorPackageImpl)registeredDwContributorPackage : new DwContributorPackageImpl();

		isInited = true;

		// Create package meta-data objects
		theDwContributorPackage.createPackageContents();

		// Initialize created meta-data
		theDwContributorPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theDwContributorPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(DwContributorPackage.eNS_URI, theDwContributorPackage);
		return theDwContributorPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwContributor() {
		return dwContributorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwContributor_Id() {
		return (EAttribute)dwContributorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwContributor_Name() {
		return (EAttribute)dwContributorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwContributorFactory getDwContributorFactory() {
		return (DwContributorFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		dwContributorEClass = createEClass(DW_CONTRIBUTOR);
		createEAttribute(dwContributorEClass, DW_CONTRIBUTOR__ID);
		createEAttribute(dwContributorEClass, DW_CONTRIBUTOR__NAME);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(dwContributorEClass, DwContributor.class, "DwContributor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDwContributor_Id(), ecorePackage.getEString(), "id", null, 1, 1, DwContributor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDwContributor_Name(), ecorePackage.getEString(), "name", null, 1, 1, DwContributor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //DwContributorPackageImpl
