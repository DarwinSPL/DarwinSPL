/**
 */
package de.darwinspl.feature.contributor.impl;

import de.darwinspl.feature.contributor.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DwContributorFactoryImpl extends EFactoryImpl implements DwContributorFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DwContributorFactory init() {
		try {
			DwContributorFactory theDwContributorFactory = (DwContributorFactory)EPackage.Registry.INSTANCE.getEFactory(DwContributorPackage.eNS_URI);
			if (theDwContributorFactory != null) {
				return theDwContributorFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DwContributorFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwContributorFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case DwContributorPackage.DW_CONTRIBUTOR: return createDwContributor();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwContributor createDwContributor() {
		DwContributorImpl dwContributor = new DwContributorImpl();
		return dwContributor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwContributorPackage getDwContributorPackage() {
		return (DwContributorPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DwContributorPackage getPackage() {
		return DwContributorPackage.eINSTANCE;
	}

} //DwContributorFactoryImpl
