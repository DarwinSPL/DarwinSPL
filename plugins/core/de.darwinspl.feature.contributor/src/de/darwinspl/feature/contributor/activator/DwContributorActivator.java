package de.darwinspl.feature.contributor.activator;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import de.darwinspl.feature.contributor.preferences.DwContributorPreferencesManager;

public class DwContributorActivator implements BundleActivator {

	@Override
	public void start(BundleContext context) throws Exception {
		DwContributorPreferencesManager.init();
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		// Do nothing
	}

}
