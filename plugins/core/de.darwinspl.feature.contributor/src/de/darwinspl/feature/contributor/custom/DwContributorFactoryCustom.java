package de.darwinspl.feature.contributor.custom;

import de.darwinspl.feature.contributor.DwContributor;
import de.darwinspl.feature.contributor.impl.DwContributorFactoryImpl;
import de.darwinspl.feature.contributor.impl.DwContributorImpl;

public class DwContributorFactoryCustom extends DwContributorFactoryImpl {
	

	@Override
	public DwContributor createDwContributor() {
		DwContributorImpl dwContributor = new DwContributorCustom();
		return dwContributor;
	}
	
}
