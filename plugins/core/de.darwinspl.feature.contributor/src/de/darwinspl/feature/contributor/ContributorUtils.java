package de.darwinspl.feature.contributor;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class ContributorUtils {
	
	public static String getEncodedStringUTF8(String stringWithSpecialCharacters) {
		try {
			return URLEncoder.encode(stringWithSpecialCharacters, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return stringWithSpecialCharacters.replaceAll(" ", "_");
		}
	}
	
	public static String getDecodedStringUTF8(String encodedStringWithoutSpecialCharactersButWithWeirdUTF8EscapeCharacters) {
		try {
			return URLDecoder.decode(encodedStringWithoutSpecialCharactersButWithWeirdUTF8EscapeCharacters, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return encodedStringWithoutSpecialCharactersButWithWeirdUTF8EscapeCharacters;
		}
	}

}
