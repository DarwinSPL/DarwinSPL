package de.darwinspl.feature.contributor.custom;

import de.darwinspl.feature.contributor.ContributorUtils;
import de.darwinspl.feature.contributor.impl.DwContributorImpl;

public class DwContributorCustom extends DwContributorImpl {
	
	public String toString() {
		return "DwContributorCustom: name=\"" + this.name + "\" id=\"" + ContributorUtils.getDecodedStringUTF8(id) + "\"";
	}

}
