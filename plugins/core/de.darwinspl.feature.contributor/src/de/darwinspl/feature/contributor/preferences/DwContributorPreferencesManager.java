package de.darwinspl.feature.contributor.preferences;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.osgi.service.prefs.BackingStoreException;

public class DwContributorPreferencesManager {
	
	private static String NAME;
	private static String ID;

	public static void init() {
		IEclipsePreferences pref = InstanceScope.INSTANCE.getNode("de.darwinspl.feature.contributor");
		
		if(pref.get("name", null) == null) {
			// No name set for the contributor
			String defaultName = System.getProperty("user.name");
			if(defaultName == null)
				defaultName = "anonymous";
			
			pref.put("name", defaultName);
		}
		
		if(pref.get("id", null) == null) {
			// No name set for the contributor
			String randomId = System.getProperty("user.name");
			if(randomId == null)
				randomId = "anonymous";
			
			randomId += (int) Math.floor(Math.random() * 9999) + "@someone.com";

			pref.put("id", randomId);
		}
		
		try {
			pref.flush();
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}
		
		NAME = pref.get("name", null);
		ID = pref.get("id", null);
	}
	
	public static void setContributor(String name, String id) {
		NAME = name;
		ID = id;
		
		IEclipsePreferences pref = InstanceScope.INSTANCE.getNode("de.darwinspl.feature.contributor");
		pref.put("name", name);
		pref.put("id", id);
		
		try {
			pref.flush();
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}
	}
	
	public static String getContributorName() {
		if(NAME != null)
			return NAME;
		else
			init();
		
		return NAME;
	}
	
	public static String getContributorId() {
		if(ID != null)
			return ID;
		else
			init();
		
		return ID;
	}
	
}
