/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.mapping.resource.dwmapping.analysis;

import java.util.Map;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

public class DwmappingIDENTIFIER_TOKENTokenResolver implements de.darwinspl.mapping.resource.dwmapping.IDwmappingTokenResolver {
	
	private de.darwinspl.mapping.resource.dwmapping.analysis.DwmappingDefaultTokenResolver defaultTokenResolver = new de.darwinspl.mapping.resource.dwmapping.analysis.DwmappingDefaultTokenResolver(true);
	
	public String deResolve(Object value, EStructuralFeature feature, EObject container) {
		// By default token de-resolving is delegated to the DefaultTokenResolver.
		String result = defaultTokenResolver.deResolve(value, feature, container, null, null, null);
		return result;
	}
	
	public void resolve(String lexem, EStructuralFeature feature, de.darwinspl.mapping.resource.dwmapping.IDwmappingTokenResolveResult result) {
		// By default token resolving is delegated to the DefaultTokenResolver.
		defaultTokenResolver.resolve(lexem, feature, result, null, null, null);
	}
	
	public void setOptions(Map<?,?> options) {
		defaultTokenResolver.setOptions(options);
	}
	
}
