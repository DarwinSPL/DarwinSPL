/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.mapping.resource.dwmapping.analysis;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

import de.darwinspl.common.ecore.util.DwEcoreUtil;
import de.darwinspl.feature.DwTemporalFeatureModel;

public class DwMappingModelFeatureModelReferenceResolver implements de.darwinspl.mapping.resource.dwmapping.IDwmappingReferenceResolver<de.darwinspl.mapping.DwMappingModel, de.darwinspl.feature.DwTemporalFeatureModel> {
	
	
	public void resolve(String identifier, de.darwinspl.mapping.DwMappingModel container, EReference reference, int position, boolean resolveFuzzy, final de.darwinspl.mapping.resource.dwmapping.IDwmappingReferenceResolveResult<de.darwinspl.feature.DwTemporalFeatureModel> result) {
		try {
			EObject root = DwEcoreUtil.resolveFeatureModelReference(identifier, container);
			
			if(root instanceof DwTemporalFeatureModel)
				result.addMapping(identifier, (DwTemporalFeatureModel) root);
		} catch(Exception e) {
			result.setErrorMessage(e.getMessage());
		}
	}
	
	public String deResolve(de.darwinspl.feature.DwTemporalFeatureModel element, de.darwinspl.mapping.DwMappingModel container, EReference reference) {
		return DwEcoreUtil.deresolveFeatureModelReference(element, container);
	}
	
	public void setOptions(Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
