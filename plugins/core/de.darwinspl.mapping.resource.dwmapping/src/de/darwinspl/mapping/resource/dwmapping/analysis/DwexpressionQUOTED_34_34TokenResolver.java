/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.mapping.resource.dwmapping.analysis;

import java.util.Map;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

public class DwexpressionQUOTED_34_34TokenResolver implements de.darwinspl.mapping.resource.dwmapping.IDwmappingTokenResolver {
	
	private de.darwinspl.expression.resource.dwexpression.analysis.DwexpressionQUOTED_34_34TokenResolver importedResolver = new de.darwinspl.expression.resource.dwexpression.analysis.DwexpressionQUOTED_34_34TokenResolver();
	
	public String deResolve(Object value, EStructuralFeature feature, EObject container) {
		String result = importedResolver.deResolve(value, feature, container);
		return result;
	}
	
	public void resolve(String lexem, EStructuralFeature feature, final de.darwinspl.mapping.resource.dwmapping.IDwmappingTokenResolveResult result) {
		importedResolver.resolve(lexem, feature, new de.darwinspl.expression.resource.dwexpression.IDwexpressionTokenResolveResult() {
			public String getErrorMessage() {
				return result.getErrorMessage();
			}
			
			public Object getResolvedToken() {
				return result.getResolvedToken();
			}
			
			public void setErrorMessage(String message) {
				result.setErrorMessage(message);
			}
			
			public void setResolvedToken(Object resolvedToken) {
				result.setResolvedToken(resolvedToken);
			}
			
		});
	}
	
	public void setOptions(Map<?,?> options) {
		importedResolver.setOptions(options);
	}
	
}
