/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.mapping.resource.dwmapping.analysis;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

import de.darwinspl.expression.util.ExpressionResolver;
import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.mapping.DwMappingModel;

public class DwFeatureReferenceExpressionFeatureReferenceResolver implements de.darwinspl.mapping.resource.dwmapping.IDwmappingReferenceResolver<de.darwinspl.expression.DwFeatureReferenceExpression, de.darwinspl.feature.DwFeature> {
	
	private de.darwinspl.expression.resource.dwexpression.analysis.DwFeatureReferenceExpressionFeatureReferenceResolver delegate = new de.darwinspl.expression.resource.dwexpression.analysis.DwFeatureReferenceExpressionFeatureReferenceResolver();
	
	public void resolve(String identifier, de.darwinspl.expression.DwFeatureReferenceExpression container, EReference reference, int position, boolean resolveFuzzy, final de.darwinspl.mapping.resource.dwmapping.IDwmappingReferenceResolveResult<de.darwinspl.feature.DwFeature> result) {
		
		// Go the containment hierarchy up until a mapping model is found or no container exists anymore.
		EObject mappingModelContainerCondidate = container;
		DwMappingModel mappingModel = null;
		
		while (mappingModel == null) {
			if (mappingModelContainerCondidate != null) {
				if (mappingModelContainerCondidate instanceof DwMappingModel) {
					mappingModel = (DwMappingModel) mappingModelContainerCondidate;
				}
				else {
					mappingModelContainerCondidate = mappingModelContainerCondidate.eContainer();
				}
			}
			else {
				break;
			}
		}
		
		if (mappingModel == null) {
			return;
		}
		 
		DwTemporalFeatureModel featureModel = null;
		featureModel = mappingModel.getFeatureModel();
		
		if (featureModel == null) {
			return;
		}
		
		DwFeature feature = ExpressionResolver.resolveFeatureFromFeatureModel(identifier, featureModel);
		
		if (feature != null) {
			result.addMapping(identifier, feature);
		}
		
//		delegate.resolve(identifier, container, reference, position, resolveFuzzy, new de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceResolveResult<de.darwinspl.feature.DwFeature>() {
//			
//			public boolean wasResolvedUniquely() {
//				return result.wasResolvedUniquely();
//			}
//			
//			public boolean wasResolvedMultiple() {
//				return result.wasResolvedMultiple();
//			}
//			
//			public boolean wasResolved() {
//				return result.wasResolved();
//			}
//			
//			public void setErrorMessage(String message) {
//				result.setErrorMessage(message);
//			}
//			
//			public Collection<de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceMapping<de.darwinspl.feature.DwFeature>> getMappings() {
//				throw new UnsupportedOperationException();
//			}
//			
//			public String getErrorMessage() {
//				return result.getErrorMessage();
//			}
//			
//			public void addMapping(String identifier, URI newIdentifier) {
//				result.addMapping(identifier, newIdentifier);
//			}
//			
//			public void addMapping(String identifier, URI newIdentifier, String warning) {
//				result.addMapping(identifier, newIdentifier, warning);
//			}
//			
//			public void addMapping(String identifier, de.darwinspl.feature.DwFeature target) {
//				result.addMapping(identifier, target);
//			}
//			
//			public void addMapping(String identifier, de.darwinspl.feature.DwFeature target, String warning) {
//				result.addMapping(identifier, target, warning);
//			}
//			
//			public Collection<de.darwinspl.expression.resource.dwexpression.IDwexpressionQuickFix> getQuickFixes() {
//				return Collections.emptySet();
//			}
//			
//			public void addQuickFix(final de.darwinspl.expression.resource.dwexpression.IDwexpressionQuickFix quickFix) {
//				result.addQuickFix(new de.darwinspl.mapping.resource.dwmapping.IDwmappingQuickFix() {
//					
//					public String getImageKey() {
//						return quickFix.getImageKey();
//					}
//					
//					public String getDisplayString() {
//						return quickFix.getDisplayString();
//					}
//					
//					public Collection<EObject> getContextObjects() {
//						return quickFix.getContextObjects();
//					}
//					
//					public String getContextAsString() {
//						return quickFix.getContextAsString();
//					}
//					
//					public String apply(String currentText) {
//						return quickFix.apply(currentText);
//					}
//				});
//			}
//		});
		
	}
	
	public String deResolve(de.darwinspl.feature.DwFeature element, de.darwinspl.expression.DwFeatureReferenceExpression container, EReference reference) {
		return delegate.deResolve(element, container, reference);
	}
	
	public void setOptions(Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
