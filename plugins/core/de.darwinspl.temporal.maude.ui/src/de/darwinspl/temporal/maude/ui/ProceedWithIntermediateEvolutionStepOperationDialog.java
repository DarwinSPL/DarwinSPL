package de.darwinspl.temporal.maude.ui;

import org.eclipse.jface.dialogs.MessageDialog;

import de.darwinspl.common.eclipse.ui.DwDialogUtil;
import de.darwinspl.feature.evolution.operation.interpreter.extensions.DwEvolutionOperationIntepreterExtension;
import de.darwinspl.feature.operation.DwEvolutionOperation;
import de.darwinspl.temporal.maude.analysis.DwEvolutionChecker;
import de.darwinspl.temporal.maude.analysis.DwEvolutionCheckerResultHook;
import de.darwinspl.temporal.maude.analysis.DwEvolutionCheckingResult;

public class ProceedWithIntermediateEvolutionStepOperationDialog implements DwEvolutionOperationIntepreterExtension, DwEvolutionCheckerResultHook {
	
	private boolean cancelExecution = false;

	private static final String CONTINUE_BUTTON_LABEL = "Continue at own risk";
	private static final String ABORT_BUTTON_LABEL = "Abort execution";

	@Override
	public void init() {
		DwEvolutionChecker.getInstance().addObserver(this);
	}

	@Override
	public boolean run(DwEvolutionOperation evoOp, String additionalInfo) {
		if(cancelExecution) {
			cancelExecution = false;
			return false;
		}
		return true;
	}

	@Override
	public boolean isResultValid(DwEvolutionCheckingResult result) {
		if(!result.isSuccess()) {
			String dialogMessage = result.getMessage();
			
//			if(result.getExplanation() != null) {
//				dialogMessage = result.getMessage() + System.lineSeparator() + System.lineSeparator() + result.getExplanation();
//			}
			
			int answer;
			if(result.getRawAnswer() != null) {
				answer = DwDialogUtil.openDwDialogWithTextfield(result.getTitle(), dialogMessage, result.getRawAnswer(), MessageDialog.ERROR, new String[] { CONTINUE_BUTTON_LABEL, ABORT_BUTTON_LABEL });
			}
			else {
				answer = DwDialogUtil.openDialog(result.getTitle(), dialogMessage, MessageDialog.ERROR, new String[] { CONTINUE_BUTTON_LABEL, ABORT_BUTTON_LABEL });
			}

			if(answer == 0) {
				// Perform Operation
				cancelExecution = false;
				return true;
			}
//			else if(answer == 2) {
//				// Show raw string and reopen the dialog
//				DwDialogUtil.openInfoDialog("Raw Maude Output", maudeResult.getRawAnswer());
//				return isResultValid(result);
//			}
			else {
				// Cancel
				cancelExecution = true;
				return false;
			}
		}
		
		return true;
	}

}
