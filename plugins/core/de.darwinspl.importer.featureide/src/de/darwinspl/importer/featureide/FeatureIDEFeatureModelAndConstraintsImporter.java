package de.darwinspl.importer.featureide;

import java.util.Date;

import org.eclipse.core.resources.IFile;
import de.darwinspl.constraint.DwConstraintModel;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.importer.DarwinSPLFeatureModelAndConstraintsImporter;
import de.darwinspl.importer.FeatureModelConstraintsTuple;
import de.ovgu.featureide.fm.core.base.IFeatureModel;

public class FeatureIDEFeatureModelAndConstraintsImporter implements DarwinSPLFeatureModelAndConstraintsImporter<IFeatureModel> {
	
	private Date currentDate;

	public FeatureIDEFeatureModelAndConstraintsImporter(Date initialDate) {
		this.currentDate = initialDate;
	}
	
	
	
	@Override
	public FeatureModelConstraintsTuple importFeatureModel(IFeatureModel featureModel) {
		FeatureIDEFeatureModelImporter featureModelImporter = new FeatureIDEFeatureModelImporter(currentDate);
		featureModelImporter.importFeatureModel(featureModel);
		
		return createTuple(featureModelImporter);
	}

	@Override
	public FeatureModelConstraintsTuple importFeatureModel(String pathToFile) {
		FeatureIDEFeatureModelImporter featureModelImporter = new FeatureIDEFeatureModelImporter(currentDate);
		featureModelImporter.importFeatureModel(pathToFile);
		
		return createTuple(featureModelImporter);
	}

	@Override
	public FeatureModelConstraintsTuple importFeatureModel(IFile file) {
		FeatureIDEFeatureModelImporter featureModelImporter = new FeatureIDEFeatureModelImporter(currentDate);
		featureModelImporter.importFeatureModel(file);
		
		return createTuple(featureModelImporter);
	}
	
	private FeatureModelConstraintsTuple createTuple(FeatureIDEFeatureModelImporter featureModelImporter) {
		DwTemporalFeatureModel darwinsplFeatureModel = featureModelImporter.getDarwinSPLfeatureModel();
		
		FeatureIDEConstraintsImporter constraintsImporter = new FeatureIDEConstraintsImporter(currentDate);
		DwConstraintModel constraintModel = constraintsImporter.importConstraints(featureModelImporter.getFeatureIDEfeatureModel().getConstraints(), darwinsplFeatureModel, featureModelImporter.getFeatureMap());
		
		return new FeatureModelConstraintsTuple(darwinsplFeatureModel, constraintModel);
	}
	

}
