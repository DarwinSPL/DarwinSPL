package de.darwinspl.importer.featureide;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.prop4j.And;
import org.prop4j.Equals;
import org.prop4j.Implies;
import org.prop4j.Literal;
import org.prop4j.Node;
import org.prop4j.Not;
import org.prop4j.Or;

import de.darwinspl.constraint.DwConstraint;
import de.darwinspl.constraint.DwConstraintFactory;
import de.darwinspl.constraint.DwConstraintModel;
import de.darwinspl.expression.DwBinaryExpression;
import de.darwinspl.expression.DwExpression;
import de.darwinspl.expression.DwExpressionFactory;
import de.darwinspl.expression.DwFeatureReferenceExpression;
import de.darwinspl.expression.DwNotExpression;
import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.temporal.util.DwEvolutionUtil;
import de.ovgu.featureide.fm.core.base.IConstraint;
import de.ovgu.featureide.fm.core.base.IFeature;

public class FeatureIDEConstraintsImporter {

	protected Map<IFeature, DwFeature> featureMap;
	
	private Date currentDate;
	
	
	
	public FeatureIDEConstraintsImporter(Date initialDate) {
		this.currentDate = initialDate;
	}
	
	
	
	public DwConstraintModel importConstraints(List<IConstraint> featureIDEconstraints, DwTemporalFeatureModel featureModel, Map<IFeature, DwFeature> featureMap) {
		
		this.featureMap = featureMap;
		
		DwConstraintModel constraintModel = DwConstraintFactory.eINSTANCE.createDwConstraintModel();
		constraintModel.setFeatureModel(featureModel);
		
		for(IConstraint constraint: featureIDEconstraints) {
			DwConstraint newConstraint = DwConstraintFactory.eINSTANCE.createDwConstraint();
			
			newConstraint.setRootExpression(createExpression(constraint.getNode()));
			DwEvolutionUtil.setTemporalValidity(newConstraint, currentDate, null);
			
			constraintModel.getConstraints().add(newConstraint);
		}
		
		return constraintModel;
	}
	
	private DwExpression createExpression(Node node) {
		DwExpression expression = null;
		
		if(node instanceof Not) {
				DwNotExpression notExpression = DwExpressionFactory.eINSTANCE.createDwNotExpression();
				
				notExpression.setOperand(createExpression(node.getChildren()[0]));
				
				expression = notExpression;	
		}
		else if(node instanceof Literal) {
			Literal literal = (Literal) node;
			
			
			if(literal.var instanceof String) {
				DwFeatureReferenceExpression featureReferenceExpression = DwExpressionFactory.eINSTANCE.createDwFeatureReferenceExpression();
				DwFeature feature = getFeature((String)literal.var);
				
				if(feature == null) {
					System.err.println("Could not find referenced feature of "+(String)literal.var);
				}
				
				featureReferenceExpression.setFeature(feature);
				
				expression = featureReferenceExpression;
			}
			else {
				System.err.println("Could not find referenced feature of literal: "+literal);
				// TODO proper error handling
			}
			
			// strange possible behavior of Nodes
			if(!literal.positive) {
				DwNotExpression notExpression = DwExpressionFactory.eINSTANCE.createDwNotExpression();
				
				notExpression.setOperand(expression);
				
				expression = notExpression;
				System.out.println("Strange Behavior");
			}
			
			
			
		}
		else {
			// In some FeatureIDE models, binary expressions only have 1 child (bug?). Catch this case.
			if (node.getChildren().length == 1) {
				return createExpression(node.getChildren()[0]);
			}
			
			DwBinaryExpression binaryExpression = null;
			if(node instanceof And) {
				binaryExpression = DwExpressionFactory.eINSTANCE.createDwAndExpression();
			} 
			else if(node instanceof Or) {
				binaryExpression = DwExpressionFactory.eINSTANCE.createDwOrExpression();
			}
			else if(node instanceof Implies) {
				binaryExpression = DwExpressionFactory.eINSTANCE.createDwImpliesExpression();
			}
			else if(node instanceof Equals) {
				binaryExpression = DwExpressionFactory.eINSTANCE.createDwEquivalenceExpression();
			}
			else {
				System.err.println("Unknown node type!");
			}
			
			DwExpression operand1 = createExpression(node.getChildren()[0]);
			DwExpression operand2 = createExpression(node.getChildren()[1]);
			
			if(operand1 == null || operand2 == null) {
				System.err.println("Operands were null!");
				return null;
			}
			
			binaryExpression.setOperand1(operand1);
			binaryExpression.setOperand2(operand2);
			
			expression = binaryExpression;
		}
		
		
		
		return expression;
	}
	
	private DwFeature getFeature(String featureName) {
		for(Entry<IFeature, DwFeature> entry: featureMap.entrySet()) {
			if(entry.getKey().getName().equals(featureName)) {
				return entry.getValue();
			}
		}
		
		return null;
	}
	
}
