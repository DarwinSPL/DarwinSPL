package de.darwinspl.importer.featureide;

import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureFactory;
import de.darwinspl.feature.DwFeatureTypeEnum;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupTypeEnum;
import de.darwinspl.feature.DwRootFeatureContainer;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.evolution.operation.factory.DwSimpleEvolutionOperationFactory;
import de.darwinspl.feature.evolution.operation.interpreter.DwEvolutionOperationInterpreter;
import de.darwinspl.feature.operation.DwFeatureCreateOperation;
import de.darwinspl.feature.operation.DwFeatureRenameOperation;
import de.darwinspl.feature.operation.DwFeatureTypeChangeOperation;
import de.darwinspl.feature.operation.DwGroupCreateOperation;
import de.darwinspl.feature.operation.DwGroupTypeChangeOperation;
import de.darwinspl.feature.util.custom.DwSimpleFeatureFactoryCustom;
import de.darwinspl.importer.DarwinSPLFeatureModelImporter;
import de.ovgu.featureide.fm.core.ExtensionManager.NoSuchExtensionException;
import de.ovgu.featureide.fm.core.base.IFeature;
import de.ovgu.featureide.fm.core.base.IFeatureModel;
import de.ovgu.featureide.fm.core.base.IFeatureStructure;
import de.ovgu.featureide.fm.core.base.impl.FMFactoryManager;
import de.ovgu.featureide.fm.core.io.IFeatureModelFormat;
import de.ovgu.featureide.fm.core.io.Problem;
import de.ovgu.featureide.fm.core.io.ProblemList;
import de.ovgu.featureide.fm.core.io.manager.FileHandler;
import de.ovgu.featureide.fm.core.io.xml.XmlFeatureModelFormat;

public class FeatureIDEFeatureModelImporter implements DarwinSPLFeatureModelImporter<IFeatureModel> {
	
	private DwSimpleEvolutionOperationFactory operationsFactory = new DwSimpleEvolutionOperationFactory();

	private Map<IFeature, DwFeature> featureMap;
	
	private IFeatureModel featureIDEfeatureModel;
	private DwTemporalFeatureModel darwinSPLfeatureModel;
	
	private Date currentDate;
	
	
	
	public FeatureIDEFeatureModelImporter(Date initialDate) {
		this.currentDate = initialDate;
	}
	


	@Override
	public DwTemporalFeatureModel importFeatureModel(IFile file) {
		return importFeatureModel(file.getRawLocation().makeAbsolute().toOSString());
	}
	
	@Override
	public DwTemporalFeatureModel importFeatureModel(String pathToFile) {
		XmlFeatureModelFormat format = new XmlFeatureModelFormat();
		try {
			IFeatureModel featureModel = loadFeatureModel(pathToFile, format);
			importFeatureModel(featureModel);
			
			(new ModelSanityCheck()).check(darwinSPLfeatureModel, this.currentDate);
			
			return darwinSPLfeatureModel;
		} catch (NoSuchExtensionException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	private static IFeatureModel loadFeatureModel(String filepath, IFeatureModelFormat format)
			throws NoSuchExtensionException {
		IFeatureModel fm = null;

		fm = FMFactoryManager.getFactory(filepath, format).createFeatureModel();

		final ProblemList errors = FileHandler.load(Paths.get(filepath), fm, format).getErrors();

		if (!errors.isEmpty()) {
			for (Problem p : errors) {
				System.err.println(p);
			}
		}

		return fm;
	}
	
	@Override
	public DwTemporalFeatureModel importFeatureModel(IFeatureModel featureModelToImport) {
		this.featureIDEfeatureModel = featureModelToImport;
		
		if (featureModelToImport == null) {
			// TODO proper logging
			System.err.println("Could not import FeatureIDE feature model as it was null");
			return null;
		}

		if (featureModelToImport.getStructure().getRoot() == null) {
			// TODO proper logging
			System.err.println("Could not import FeatureIDE feature model as its root was null");
			return null;
		}

		featureMap = new HashMap<IFeature, DwFeature>();
		
		DwTemporalFeatureModel dwFeatureModel = DwFeatureFactory.eINSTANCE.createDwTemporalFeatureModel();		

		IFeature rootFeature = featureModelToImport.getStructure().getRoot().getFeature();
		
		processRoot(dwFeatureModel, rootFeature);
		
//		processSubTree(hyFeatureModel, hyRootFeature, featureModel, rootFeature);
		
		this.darwinSPLfeatureModel = dwFeatureModel;
		return darwinSPLfeatureModel;
	}


	
	/**
	 * 
	 * @param hyFeatureModel
	 * @param rootFeature
	 * @return
	 */
	private DwFeature processRoot(DwTemporalFeatureModel dwFeatureModel, IFeature rootFeature) {
		DwFeature dwRootFeatureFeature = doImportFeature(rootFeature, dwFeatureModel, null);
		
		DwRootFeatureContainer dwRootFeatureContainer = DwSimpleFeatureFactoryCustom.createNewRootFeatureContainer(dwRootFeatureFeature, currentDate);
		dwFeatureModel.getRootFeatures().add(dwRootFeatureContainer);
		
		return dwRootFeatureFeature;
	}
	
	private DwFeature doImportFeature(IFeature feature, DwTemporalFeatureModel dwFeatureModel, DwGroup parentGroup) {
		String name = feature.getName();
		DwFeatureTypeEnum variationType = null;
		
		IFeatureStructure featureStructure = feature.getStructure();
		IFeatureStructure parentFeatureStructure = featureStructure.getParent();
		IFeature parentFeature = parentFeatureStructure == null ? null : parentFeatureStructure.getFeature();
		
		//Variation type of feature
		if (parentFeature == null) {
			//Root feature is always mandatory
			variationType = DwFeatureTypeEnum.MANDATORY;
		} else if (parentFeatureStructure.isOr() || parentFeatureStructure.isAlternative()) {
			//In dedicated groups, all features are perceived as being optional
			
			// If the feature's group is alternative or or but only one feature exists in this group, it has to be mandatory.
			if(parentFeatureStructure.getChildren().size() > 1) {
				variationType = DwFeatureTypeEnum.OPTIONAL;				
			}
			else {
				variationType = DwFeatureTypeEnum.MANDATORY;
			}
		} else if (featureStructure.isMandatory()) {
			variationType = DwFeatureTypeEnum.MANDATORY;
		} else {
			//Can only be optional
			variationType = DwFeatureTypeEnum.OPTIONAL;
		}
		
		DwFeatureCreateOperation featureCreateOperation = operationsFactory.createDwFeatureCreateOperation(dwFeatureModel, parentGroup, false, this.currentDate, true);
		DwEvolutionOperationInterpreter.executeWithoutAnalysis(featureCreateOperation);
		DwFeature dwFeature = featureCreateOperation.getFeature();
//		DwFeature dwFeature = DwFactoryCustom.createNewFeature(name, variationType, this.currentDate);
//		dwFeatureModel.getFeatures().add(dwFeature);
		
		featureMap.put(feature, dwFeature);
		
		DwFeatureRenameOperation featureNameOperation = operationsFactory.createDwFeatureRenameOperation(dwFeature.getFeatureModel(), dwFeature, name, this.currentDate, true);
		DwEvolutionOperationInterpreter.executeWithoutAnalysis(featureNameOperation);
		
		if(variationType != DwFeatureTypeEnum.OPTIONAL) {
			DwFeatureTypeChangeOperation featureTypeOperation = operationsFactory.createDwFeatureTypeChangeOperation(dwFeatureModel, dwFeature, variationType, this.currentDate, true);
			DwEvolutionOperationInterpreter.executeWithoutAnalysis(featureTypeOperation);
		}
		
		// convert children
		
		List<IFeatureStructure> childStructures = featureStructure.getChildren();
		
		if (!childStructures.isEmpty()) {
//			DwParentFeatureChildGroupContainer featureChild = DwFactoryCustom.createNewParentFeatureChildGroupContainer(dwFeature, currentDate);
//			DwGroup dwGroup = DwFactoryCustom.createNewGroup(currentDate);
//			dwGroup.getChildOf().add(featureChild);
			
			DwGroupCreateOperation groupCreateOperation = operationsFactory.createDwGroupCreateOperation(dwFeatureModel, dwFeature, this.currentDate, true);
			DwEvolutionOperationInterpreter.executeWithoutAnalysis(groupCreateOperation);
			DwGroup dwGroup = groupCreateOperation.getGroup();
//			dwFeatureModel.getGroups().add(dwGroup);
			
			
			// first, the child features must be added. Then, the type can be changed.
			
			for (IFeatureStructure childStructure : childStructures) {
				IFeature childFeature = childStructure.getFeature();
				doImportFeature(childFeature, dwFeatureModel, dwGroup);
				
//				operationsFactory.createDwFeatureAddOperation(featureModel, feature, parent, forceCreateParentGroup, date, lastDateSelected)
				
//				groupComposition.getChildFeatures().add(dwChildFeature);
			}
			
//			DwGroupComposition groupComposition = DwFactoryCustom.createNewGroupComposition(dwGroup, currentDate);
			
			//Variation type of group
//			DwGroupType groupType = DwFactoryCustom.createNewGroupType(currentDate);
//			dwGroup.getTypes().add(groupType);
			
			DwGroupTypeEnum groupType = DwGroupTypeEnum.AND;
			
			// can only be alternative or or if more than one feature.
			if (childStructures.size() > 1) {
				if (featureStructure.isAlternative()) {
					groupType = DwGroupTypeEnum.ALTERNATIVE;
				} else if (featureStructure.isOr()) {
					groupType = DwGroupTypeEnum.OR;
				} else if (featureStructure.isAnd()) {
					// Minimum is the number of mandatory child features
					groupType = DwGroupTypeEnum.AND;
				}
			}
			
			DwGroupTypeChangeOperation groupTypeOperation = operationsFactory.createDwGroupTypeChangeOperation(dwFeatureModel, dwGroup, groupType, currentDate, true);
			DwEvolutionOperationInterpreter.executeWithoutAnalysis(groupTypeOperation);

		}
		
		return dwFeature;
	}
	
	
	public Map<IFeature, DwFeature> getFeatureMap() {
		return featureMap;
	}

	public IFeatureModel getFeatureIDEfeatureModel() {
		return featureIDEfeatureModel;
	}

	public DwTemporalFeatureModel getDarwinSPLfeatureModel() {
		return darwinSPLfeatureModel;
	}
	
	
	
}
