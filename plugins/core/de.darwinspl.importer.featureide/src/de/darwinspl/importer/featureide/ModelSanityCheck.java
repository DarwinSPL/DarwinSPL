package de.darwinspl.importer.featureide;

import java.util.Date;
import java.util.List;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureTypeEnum;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupTypeEnum;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.util.custom.DwFeatureUtil;

public class ModelSanityCheck {
	
	public void check(DwTemporalFeatureModel tfm, Date date) {
		for(DwFeature feature : tfm.getFeatures()) {
			List<DwGroup> childGroups = DwFeatureUtil.getChildGroupsOfFeature(feature, date);
			
			if(childGroups.size() > 1)
				throw new RuntimeException("More than one child group. This should not be allowed in FeatureIDE.");
		}
		
		for(DwGroup group : tfm.getGroups()) {
			List<DwFeature> childFeatures = DwFeatureUtil.getChildFeaturesOfGroup(group, date);
			
			if(childFeatures.isEmpty())
				throw new RuntimeException("Ghost group detected.");
			
			if(!DwFeatureUtil.getType(group, date).getType().equals(DwGroupTypeEnum.AND)) {
				for(DwFeature childFeature : childFeatures) {
					if(DwFeatureUtil.getType(childFeature, date).getType().equals(DwFeatureTypeEnum.MANDATORY)) {
						throw new RuntimeException("Mandatory feature in illegal group.");
					}
				}
			}
		}
	}
	
}
