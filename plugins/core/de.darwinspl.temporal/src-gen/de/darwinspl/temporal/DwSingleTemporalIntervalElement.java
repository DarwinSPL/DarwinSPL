/**
 */
package de.darwinspl.temporal;

import java.util.Date;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Single Temporal Interval Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.temporal.DwSingleTemporalIntervalElement#getValidity <em>Validity</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.temporal.DwTemporalModelPackage#getDwSingleTemporalIntervalElement()
 * @model abstract="true"
 * @generated
 */
public interface DwSingleTemporalIntervalElement extends DwTemporalElement {
	/**
	 * Returns the value of the '<em><b>Validity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Validity</em>' containment reference.
	 * @see #setValidity(DwTemporalInterval)
	 * @see de.darwinspl.temporal.DwTemporalModelPackage#getDwSingleTemporalIntervalElement_Validity()
	 * @model containment="true" required="true"
	 * @generated
	 */
	DwTemporalInterval getValidity();

	/**
	 * Sets the value of the '{@link de.darwinspl.temporal.DwSingleTemporalIntervalElement#getValidity <em>Validity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Validity</em>' containment reference.
	 * @see #getValidity()
	 * @generated
	 */
	void setValidity(DwTemporalInterval value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='DwTemporalInterval validity = getValidity();\nif (validity == null) {\n   return null;\n}\nreturn validity.getFrom();'"
	 * @generated
	 */
	Date getFrom();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='DwTemporalInterval validity = getValidity();\nif (validity == null) {\n   return null;\n}\nreturn validity.getTo();'"
	 * @generated
	 */
	Date getTo();

} // DwSingleTemporalIntervalElement
