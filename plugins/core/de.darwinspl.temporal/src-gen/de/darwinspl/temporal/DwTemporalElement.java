/**
 */
package de.darwinspl.temporal;

import java.util.Date;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Temporal Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.temporal.DwTemporalModelPackage#getDwTemporalElement()
 * @model abstract="true"
 * @generated
 */
public interface DwTemporalElement extends DwElementWithId {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return de.darwinspl.temporal.util.DwEvolutionUtil.isValid(this, date);'"
	 * @generated
	 */
	boolean isValid(Date date);

} // DwTemporalElement
