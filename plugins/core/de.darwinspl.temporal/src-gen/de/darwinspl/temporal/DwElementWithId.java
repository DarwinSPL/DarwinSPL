/**
 */
package de.darwinspl.temporal;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Element With Id</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.temporal.DwElementWithId#getId <em>Id</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.temporal.DwTemporalModelPackage#getDwElementWithId()
 * @model abstract="true"
 * @generated
 */
public interface DwElementWithId extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see de.darwinspl.temporal.DwTemporalModelPackage#getDwElementWithId_Id()
	 * @model id="true" required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link de.darwinspl.temporal.DwElementWithId#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if(this.id == null || this.id.equals(\"\")) {\n   String newIdentifier = \"_\"+java.util.UUID.randomUUID().toString();\n   setId(newIdentifier);\n}\nreturn getId();'"
	 * @generated
	 */
	String createId();

} // DwElementWithId
