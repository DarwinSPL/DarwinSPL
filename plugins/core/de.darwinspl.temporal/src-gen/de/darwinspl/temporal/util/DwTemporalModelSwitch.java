/**
 */
package de.darwinspl.temporal.util;

import de.darwinspl.temporal.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see de.darwinspl.temporal.DwTemporalModelPackage
 * @generated
 */
public class DwTemporalModelSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DwTemporalModelPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwTemporalModelSwitch() {
		if (modelPackage == null) {
			modelPackage = DwTemporalModelPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case DwTemporalModelPackage.DW_TEMPORAL_ELEMENT: {
				DwTemporalElement dwTemporalElement = (DwTemporalElement)theEObject;
				T result = caseDwTemporalElement(dwTemporalElement);
				if (result == null) result = caseDwElementWithId(dwTemporalElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT: {
				DwSingleTemporalIntervalElement dwSingleTemporalIntervalElement = (DwSingleTemporalIntervalElement)theEObject;
				T result = caseDwSingleTemporalIntervalElement(dwSingleTemporalIntervalElement);
				if (result == null) result = caseDwTemporalElement(dwSingleTemporalIntervalElement);
				if (result == null) result = caseDwElementWithId(dwSingleTemporalIntervalElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT: {
				DwMultiTemporalIntervalElement dwMultiTemporalIntervalElement = (DwMultiTemporalIntervalElement)theEObject;
				T result = caseDwMultiTemporalIntervalElement(dwMultiTemporalIntervalElement);
				if (result == null) result = caseDwTemporalElement(dwMultiTemporalIntervalElement);
				if (result == null) result = caseDwElementWithId(dwMultiTemporalIntervalElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwTemporalModelPackage.DW_TEMPORAL_INTERVAL: {
				DwTemporalInterval dwTemporalInterval = (DwTemporalInterval)theEObject;
				T result = caseDwTemporalInterval(dwTemporalInterval);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwTemporalModelPackage.DW_ELEMENT_WITH_ID: {
				DwElementWithId dwElementWithId = (DwElementWithId)theEObject;
				T result = caseDwElementWithId(dwElementWithId);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Element With Id</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Element With Id</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwElementWithId(DwElementWithId object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Temporal Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Temporal Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwTemporalElement(DwTemporalElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Single Temporal Interval Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Single Temporal Interval Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwSingleTemporalIntervalElement(DwSingleTemporalIntervalElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Multi Temporal Interval Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Multi Temporal Interval Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwMultiTemporalIntervalElement(DwMultiTemporalIntervalElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Temporal Interval</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Temporal Interval</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwTemporalInterval(DwTemporalInterval object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //DwTemporalModelSwitch
