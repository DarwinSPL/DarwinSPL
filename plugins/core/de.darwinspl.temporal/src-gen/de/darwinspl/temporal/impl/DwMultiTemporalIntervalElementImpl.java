/**
 */
package de.darwinspl.temporal.impl;

import de.darwinspl.temporal.DwMultiTemporalIntervalElement;
import de.darwinspl.temporal.DwTemporalInterval;
import de.darwinspl.temporal.DwTemporalModelPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Multi Temporal Interval Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.temporal.impl.DwMultiTemporalIntervalElementImpl#getValidities <em>Validities</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class DwMultiTemporalIntervalElementImpl extends DwTemporalElementImpl implements DwMultiTemporalIntervalElement {
	/**
	 * The cached value of the '{@link #getValidities() <em>Validities</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValidities()
	 * @generated
	 * @ordered
	 */
	protected EList<DwTemporalInterval> validities;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwMultiTemporalIntervalElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwTemporalModelPackage.Literals.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwTemporalInterval> getValidities() {
		if (validities == null) {
			validities = new EObjectContainmentEList<DwTemporalInterval>(DwTemporalInterval.class, this, DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT__VALIDITIES);
		}
		return validities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT__VALIDITIES:
				return ((InternalEList<?>)getValidities()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT__VALIDITIES:
				return getValidities();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT__VALIDITIES:
				getValidities().clear();
				getValidities().addAll((Collection<? extends DwTemporalInterval>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT__VALIDITIES:
				getValidities().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT__VALIDITIES:
				return validities != null && !validities.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DwMultiTemporalIntervalElementImpl
