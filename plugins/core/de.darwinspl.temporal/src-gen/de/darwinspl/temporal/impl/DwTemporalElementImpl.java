/**
 */
package de.darwinspl.temporal.impl;

import de.darwinspl.temporal.DwTemporalElement;
import de.darwinspl.temporal.DwTemporalModelPackage;

import java.lang.reflect.InvocationTargetException;

import java.util.Date;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Temporal Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class DwTemporalElementImpl extends DwElementWithIdImpl implements DwTemporalElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwTemporalElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwTemporalModelPackage.Literals.DW_TEMPORAL_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid(final Date date) {
		return de.darwinspl.temporal.util.DwEvolutionUtil.isValid(this, date);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case DwTemporalModelPackage.DW_TEMPORAL_ELEMENT___IS_VALID__DATE:
				return isValid((Date)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //DwTemporalElementImpl
