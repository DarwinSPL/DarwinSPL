/**
 */
package de.darwinspl.temporal.impl;

import de.darwinspl.temporal.DwSingleTemporalIntervalElement;
import de.darwinspl.temporal.DwTemporalInterval;
import de.darwinspl.temporal.DwTemporalModelPackage;

import java.lang.reflect.InvocationTargetException;

import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Single Temporal Interval Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.temporal.impl.DwSingleTemporalIntervalElementImpl#getValidity <em>Validity</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class DwSingleTemporalIntervalElementImpl extends DwTemporalElementImpl implements DwSingleTemporalIntervalElement {
	/**
	 * The cached value of the '{@link #getValidity() <em>Validity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValidity()
	 * @generated
	 * @ordered
	 */
	protected DwTemporalInterval validity;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwSingleTemporalIntervalElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwTemporalModelPackage.Literals.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwTemporalInterval getValidity() {
		return validity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetValidity(DwTemporalInterval newValidity, NotificationChain msgs) {
		DwTemporalInterval oldValidity = validity;
		validity = newValidity;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__VALIDITY, oldValidity, newValidity);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setValidity(DwTemporalInterval newValidity) {
		if (newValidity != validity) {
			NotificationChain msgs = null;
			if (validity != null)
				msgs = ((InternalEObject)validity).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__VALIDITY, null, msgs);
			if (newValidity != null)
				msgs = ((InternalEObject)newValidity).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__VALIDITY, null, msgs);
			msgs = basicSetValidity(newValidity, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__VALIDITY, newValidity, newValidity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Date getFrom() {
		DwTemporalInterval validity = getValidity();
		if (validity == null) {
		   return null;
		}
		return validity.getFrom();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Date getTo() {
		DwTemporalInterval validity = getValidity();
		if (validity == null) {
		   return null;
		}
		return validity.getTo();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__VALIDITY:
				return basicSetValidity(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__VALIDITY:
				return getValidity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__VALIDITY:
				setValidity((DwTemporalInterval)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__VALIDITY:
				setValidity((DwTemporalInterval)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__VALIDITY:
				return validity != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_FROM:
				return getFrom();
			case DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_TO:
				return getTo();
		}
		return super.eInvoke(operationID, arguments);
	}

} //DwSingleTemporalIntervalElementImpl
