/**
 */
package de.darwinspl.temporal.impl;

import de.darwinspl.temporal.DwElementWithId;
import de.darwinspl.temporal.DwMultiTemporalIntervalElement;
import de.darwinspl.temporal.DwSingleTemporalIntervalElement;
import de.darwinspl.temporal.DwTemporalElement;
import de.darwinspl.temporal.DwTemporalInterval;
import de.darwinspl.temporal.DwTemporalModelFactory;
import de.darwinspl.temporal.DwTemporalModelPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DwTemporalModelPackageImpl extends EPackageImpl implements DwTemporalModelPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwElementWithIdEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwTemporalElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwSingleTemporalIntervalElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwMultiTemporalIntervalElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwTemporalIntervalEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.darwinspl.temporal.DwTemporalModelPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private DwTemporalModelPackageImpl() {
		super(eNS_URI, DwTemporalModelFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link DwTemporalModelPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static DwTemporalModelPackage init() {
		if (isInited) return (DwTemporalModelPackage)EPackage.Registry.INSTANCE.getEPackage(DwTemporalModelPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredDwTemporalModelPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		DwTemporalModelPackageImpl theDwTemporalModelPackage = registeredDwTemporalModelPackage instanceof DwTemporalModelPackageImpl ? (DwTemporalModelPackageImpl)registeredDwTemporalModelPackage : new DwTemporalModelPackageImpl();

		isInited = true;

		// Create package meta-data objects
		theDwTemporalModelPackage.createPackageContents();

		// Initialize created meta-data
		theDwTemporalModelPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theDwTemporalModelPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(DwTemporalModelPackage.eNS_URI, theDwTemporalModelPackage);
		return theDwTemporalModelPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwElementWithId() {
		return dwElementWithIdEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwElementWithId_Id() {
		return (EAttribute)dwElementWithIdEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getDwElementWithId__CreateId() {
		return dwElementWithIdEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwTemporalElement() {
		return dwTemporalElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getDwTemporalElement__IsValid__Date() {
		return dwTemporalElementEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwSingleTemporalIntervalElement() {
		return dwSingleTemporalIntervalElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwSingleTemporalIntervalElement_Validity() {
		return (EReference)dwSingleTemporalIntervalElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getDwSingleTemporalIntervalElement__GetFrom() {
		return dwSingleTemporalIntervalElementEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getDwSingleTemporalIntervalElement__GetTo() {
		return dwSingleTemporalIntervalElementEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwMultiTemporalIntervalElement() {
		return dwMultiTemporalIntervalElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwMultiTemporalIntervalElement_Validities() {
		return (EReference)dwMultiTemporalIntervalElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwTemporalInterval() {
		return dwTemporalIntervalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwTemporalInterval_From() {
		return (EAttribute)dwTemporalIntervalEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwTemporalInterval_To() {
		return (EAttribute)dwTemporalIntervalEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwTemporalModelFactory getDwTemporalModelFactory() {
		return (DwTemporalModelFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		dwTemporalElementEClass = createEClass(DW_TEMPORAL_ELEMENT);
		createEOperation(dwTemporalElementEClass, DW_TEMPORAL_ELEMENT___IS_VALID__DATE);

		dwSingleTemporalIntervalElementEClass = createEClass(DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT);
		createEReference(dwSingleTemporalIntervalElementEClass, DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__VALIDITY);
		createEOperation(dwSingleTemporalIntervalElementEClass, DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_FROM);
		createEOperation(dwSingleTemporalIntervalElementEClass, DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_TO);

		dwMultiTemporalIntervalElementEClass = createEClass(DW_MULTI_TEMPORAL_INTERVAL_ELEMENT);
		createEReference(dwMultiTemporalIntervalElementEClass, DW_MULTI_TEMPORAL_INTERVAL_ELEMENT__VALIDITIES);

		dwTemporalIntervalEClass = createEClass(DW_TEMPORAL_INTERVAL);
		createEAttribute(dwTemporalIntervalEClass, DW_TEMPORAL_INTERVAL__FROM);
		createEAttribute(dwTemporalIntervalEClass, DW_TEMPORAL_INTERVAL__TO);

		dwElementWithIdEClass = createEClass(DW_ELEMENT_WITH_ID);
		createEAttribute(dwElementWithIdEClass, DW_ELEMENT_WITH_ID__ID);
		createEOperation(dwElementWithIdEClass, DW_ELEMENT_WITH_ID___CREATE_ID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		dwTemporalElementEClass.getESuperTypes().add(this.getDwElementWithId());
		dwSingleTemporalIntervalElementEClass.getESuperTypes().add(this.getDwTemporalElement());
		dwMultiTemporalIntervalElementEClass.getESuperTypes().add(this.getDwTemporalElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(dwTemporalElementEClass, DwTemporalElement.class, "DwTemporalElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getDwTemporalElement__IsValid__Date(), ecorePackage.getEBoolean(), "isValid", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "date", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(dwSingleTemporalIntervalElementEClass, DwSingleTemporalIntervalElement.class, "DwSingleTemporalIntervalElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwSingleTemporalIntervalElement_Validity(), this.getDwTemporalInterval(), null, "validity", null, 1, 1, DwSingleTemporalIntervalElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getDwSingleTemporalIntervalElement__GetFrom(), ecorePackage.getEDate(), "getFrom", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getDwSingleTemporalIntervalElement__GetTo(), ecorePackage.getEDate(), "getTo", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(dwMultiTemporalIntervalElementEClass, DwMultiTemporalIntervalElement.class, "DwMultiTemporalIntervalElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwMultiTemporalIntervalElement_Validities(), this.getDwTemporalInterval(), null, "validities", null, 1, -1, DwMultiTemporalIntervalElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwTemporalIntervalEClass, DwTemporalInterval.class, "DwTemporalInterval", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDwTemporalInterval_From(), ecorePackage.getEDate(), "from", null, 0, 1, DwTemporalInterval.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDwTemporalInterval_To(), ecorePackage.getEDate(), "to", null, 0, 1, DwTemporalInterval.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwElementWithIdEClass, DwElementWithId.class, "DwElementWithId", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDwElementWithId_Id(), ecorePackage.getEString(), "id", null, 1, 1, DwElementWithId.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getDwElementWithId__CreateId(), ecorePackage.getEString(), "createId", 1, 1, IS_UNIQUE, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //DwTemporalModelPackageImpl
