/**
 */
package de.darwinspl.temporal.impl;

import de.darwinspl.temporal.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DwTemporalModelFactoryImpl extends EFactoryImpl implements DwTemporalModelFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DwTemporalModelFactory init() {
		try {
			DwTemporalModelFactory theDwTemporalModelFactory = (DwTemporalModelFactory)EPackage.Registry.INSTANCE.getEFactory(DwTemporalModelPackage.eNS_URI);
			if (theDwTemporalModelFactory != null) {
				return theDwTemporalModelFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DwTemporalModelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwTemporalModelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case DwTemporalModelPackage.DW_TEMPORAL_INTERVAL: return createDwTemporalInterval();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwTemporalInterval createDwTemporalInterval() {
		DwTemporalIntervalImpl dwTemporalInterval = new DwTemporalIntervalImpl();
		return dwTemporalInterval;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwTemporalModelPackage getDwTemporalModelPackage() {
		return (DwTemporalModelPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DwTemporalModelPackage getPackage() {
		return DwTemporalModelPackage.eINSTANCE;
	}

} //DwTemporalModelFactoryImpl
