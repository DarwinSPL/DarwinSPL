/**
 */
package de.darwinspl.temporal;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Multi Temporal Interval Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.temporal.DwMultiTemporalIntervalElement#getValidities <em>Validities</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.temporal.DwTemporalModelPackage#getDwMultiTemporalIntervalElement()
 * @model abstract="true"
 * @generated
 */
public interface DwMultiTemporalIntervalElement extends DwTemporalElement {
	/**
	 * Returns the value of the '<em><b>Validities</b></em>' containment reference list.
	 * The list contents are of type {@link de.darwinspl.temporal.DwTemporalInterval}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Validities</em>' containment reference list.
	 * @see de.darwinspl.temporal.DwTemporalModelPackage#getDwMultiTemporalIntervalElement_Validities()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<DwTemporalInterval> getValidities();

} // DwMultiTemporalIntervalElement
