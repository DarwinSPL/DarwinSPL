/**
 */
package de.darwinspl.temporal;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.darwinspl.temporal.DwTemporalModelFactory
 * @model kind="package"
 * @generated
 */
public interface DwTemporalModelPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "temporal";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "https://www.tu-braunschweig.de/isf/DarwinSPL/Temporal/2.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "temporal";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DwTemporalModelPackage eINSTANCE = de.darwinspl.temporal.impl.DwTemporalModelPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.darwinspl.temporal.impl.DwElementWithIdImpl <em>Dw Element With Id</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.temporal.impl.DwElementWithIdImpl
	 * @see de.darwinspl.temporal.impl.DwTemporalModelPackageImpl#getDwElementWithId()
	 * @generated
	 */
	int DW_ELEMENT_WITH_ID = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ELEMENT_WITH_ID__ID = 0;

	/**
	 * The number of structural features of the '<em>Dw Element With Id</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ELEMENT_WITH_ID_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ELEMENT_WITH_ID___CREATE_ID = 0;

	/**
	 * The number of operations of the '<em>Dw Element With Id</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ELEMENT_WITH_ID_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link de.darwinspl.temporal.impl.DwTemporalElementImpl <em>Dw Temporal Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.temporal.impl.DwTemporalElementImpl
	 * @see de.darwinspl.temporal.impl.DwTemporalModelPackageImpl#getDwTemporalElement()
	 * @generated
	 */
	int DW_TEMPORAL_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_TEMPORAL_ELEMENT__ID = DW_ELEMENT_WITH_ID__ID;

	/**
	 * The number of structural features of the '<em>Dw Temporal Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_TEMPORAL_ELEMENT_FEATURE_COUNT = DW_ELEMENT_WITH_ID_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_TEMPORAL_ELEMENT___CREATE_ID = DW_ELEMENT_WITH_ID___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_TEMPORAL_ELEMENT___IS_VALID__DATE = DW_ELEMENT_WITH_ID_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dw Temporal Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_TEMPORAL_ELEMENT_OPERATION_COUNT = DW_ELEMENT_WITH_ID_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.darwinspl.temporal.impl.DwSingleTemporalIntervalElementImpl <em>Dw Single Temporal Interval Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.temporal.impl.DwSingleTemporalIntervalElementImpl
	 * @see de.darwinspl.temporal.impl.DwTemporalModelPackageImpl#getDwSingleTemporalIntervalElement()
	 * @generated
	 */
	int DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__ID = DW_TEMPORAL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Validity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__VALIDITY = DW_TEMPORAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Dw Single Temporal Interval Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT = DW_TEMPORAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___CREATE_ID = DW_TEMPORAL_ELEMENT___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___IS_VALID__DATE = DW_TEMPORAL_ELEMENT___IS_VALID__DATE;

	/**
	 * The operation id for the '<em>Get From</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_FROM = DW_TEMPORAL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_TO = DW_TEMPORAL_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Dw Single Temporal Interval Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_OPERATION_COUNT = DW_TEMPORAL_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link de.darwinspl.temporal.impl.DwMultiTemporalIntervalElementImpl <em>Dw Multi Temporal Interval Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.temporal.impl.DwMultiTemporalIntervalElementImpl
	 * @see de.darwinspl.temporal.impl.DwTemporalModelPackageImpl#getDwMultiTemporalIntervalElement()
	 * @generated
	 */
	int DW_MULTI_TEMPORAL_INTERVAL_ELEMENT = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_MULTI_TEMPORAL_INTERVAL_ELEMENT__ID = DW_TEMPORAL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Validities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_MULTI_TEMPORAL_INTERVAL_ELEMENT__VALIDITIES = DW_TEMPORAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Dw Multi Temporal Interval Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT = DW_TEMPORAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_MULTI_TEMPORAL_INTERVAL_ELEMENT___CREATE_ID = DW_TEMPORAL_ELEMENT___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_MULTI_TEMPORAL_INTERVAL_ELEMENT___IS_VALID__DATE = DW_TEMPORAL_ELEMENT___IS_VALID__DATE;

	/**
	 * The number of operations of the '<em>Dw Multi Temporal Interval Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_OPERATION_COUNT = DW_TEMPORAL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.temporal.impl.DwTemporalIntervalImpl <em>Dw Temporal Interval</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.temporal.impl.DwTemporalIntervalImpl
	 * @see de.darwinspl.temporal.impl.DwTemporalModelPackageImpl#getDwTemporalInterval()
	 * @generated
	 */
	int DW_TEMPORAL_INTERVAL = 3;

	/**
	 * The feature id for the '<em><b>From</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_TEMPORAL_INTERVAL__FROM = 0;

	/**
	 * The feature id for the '<em><b>To</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_TEMPORAL_INTERVAL__TO = 1;

	/**
	 * The number of structural features of the '<em>Dw Temporal Interval</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_TEMPORAL_INTERVAL_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Dw Temporal Interval</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_TEMPORAL_INTERVAL_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link de.darwinspl.temporal.DwElementWithId <em>Dw Element With Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Element With Id</em>'.
	 * @see de.darwinspl.temporal.DwElementWithId
	 * @generated
	 */
	EClass getDwElementWithId();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.temporal.DwElementWithId#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see de.darwinspl.temporal.DwElementWithId#getId()
	 * @see #getDwElementWithId()
	 * @generated
	 */
	EAttribute getDwElementWithId_Id();

	/**
	 * Returns the meta object for the '{@link de.darwinspl.temporal.DwElementWithId#createId() <em>Create Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create Id</em>' operation.
	 * @see de.darwinspl.temporal.DwElementWithId#createId()
	 * @generated
	 */
	EOperation getDwElementWithId__CreateId();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.temporal.DwTemporalElement <em>Dw Temporal Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Temporal Element</em>'.
	 * @see de.darwinspl.temporal.DwTemporalElement
	 * @generated
	 */
	EClass getDwTemporalElement();

	/**
	 * Returns the meta object for the '{@link de.darwinspl.temporal.DwTemporalElement#isValid(java.util.Date) <em>Is Valid</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid</em>' operation.
	 * @see de.darwinspl.temporal.DwTemporalElement#isValid(java.util.Date)
	 * @generated
	 */
	EOperation getDwTemporalElement__IsValid__Date();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.temporal.DwSingleTemporalIntervalElement <em>Dw Single Temporal Interval Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Single Temporal Interval Element</em>'.
	 * @see de.darwinspl.temporal.DwSingleTemporalIntervalElement
	 * @generated
	 */
	EClass getDwSingleTemporalIntervalElement();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.temporal.DwSingleTemporalIntervalElement#getValidity <em>Validity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Validity</em>'.
	 * @see de.darwinspl.temporal.DwSingleTemporalIntervalElement#getValidity()
	 * @see #getDwSingleTemporalIntervalElement()
	 * @generated
	 */
	EReference getDwSingleTemporalIntervalElement_Validity();

	/**
	 * Returns the meta object for the '{@link de.darwinspl.temporal.DwSingleTemporalIntervalElement#getFrom() <em>Get From</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get From</em>' operation.
	 * @see de.darwinspl.temporal.DwSingleTemporalIntervalElement#getFrom()
	 * @generated
	 */
	EOperation getDwSingleTemporalIntervalElement__GetFrom();

	/**
	 * Returns the meta object for the '{@link de.darwinspl.temporal.DwSingleTemporalIntervalElement#getTo() <em>Get To</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get To</em>' operation.
	 * @see de.darwinspl.temporal.DwSingleTemporalIntervalElement#getTo()
	 * @generated
	 */
	EOperation getDwSingleTemporalIntervalElement__GetTo();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.temporal.DwMultiTemporalIntervalElement <em>Dw Multi Temporal Interval Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Multi Temporal Interval Element</em>'.
	 * @see de.darwinspl.temporal.DwMultiTemporalIntervalElement
	 * @generated
	 */
	EClass getDwMultiTemporalIntervalElement();

	/**
	 * Returns the meta object for the containment reference list '{@link de.darwinspl.temporal.DwMultiTemporalIntervalElement#getValidities <em>Validities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Validities</em>'.
	 * @see de.darwinspl.temporal.DwMultiTemporalIntervalElement#getValidities()
	 * @see #getDwMultiTemporalIntervalElement()
	 * @generated
	 */
	EReference getDwMultiTemporalIntervalElement_Validities();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.temporal.DwTemporalInterval <em>Dw Temporal Interval</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Temporal Interval</em>'.
	 * @see de.darwinspl.temporal.DwTemporalInterval
	 * @generated
	 */
	EClass getDwTemporalInterval();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.temporal.DwTemporalInterval#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>From</em>'.
	 * @see de.darwinspl.temporal.DwTemporalInterval#getFrom()
	 * @see #getDwTemporalInterval()
	 * @generated
	 */
	EAttribute getDwTemporalInterval_From();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.temporal.DwTemporalInterval#getTo <em>To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>To</em>'.
	 * @see de.darwinspl.temporal.DwTemporalInterval#getTo()
	 * @see #getDwTemporalInterval()
	 * @generated
	 */
	EAttribute getDwTemporalInterval_To();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DwTemporalModelFactory getDwTemporalModelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.darwinspl.temporal.impl.DwElementWithIdImpl <em>Dw Element With Id</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.temporal.impl.DwElementWithIdImpl
		 * @see de.darwinspl.temporal.impl.DwTemporalModelPackageImpl#getDwElementWithId()
		 * @generated
		 */
		EClass DW_ELEMENT_WITH_ID = eINSTANCE.getDwElementWithId();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_ELEMENT_WITH_ID__ID = eINSTANCE.getDwElementWithId_Id();

		/**
		 * The meta object literal for the '<em><b>Create Id</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DW_ELEMENT_WITH_ID___CREATE_ID = eINSTANCE.getDwElementWithId__CreateId();

		/**
		 * The meta object literal for the '{@link de.darwinspl.temporal.impl.DwTemporalElementImpl <em>Dw Temporal Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.temporal.impl.DwTemporalElementImpl
		 * @see de.darwinspl.temporal.impl.DwTemporalModelPackageImpl#getDwTemporalElement()
		 * @generated
		 */
		EClass DW_TEMPORAL_ELEMENT = eINSTANCE.getDwTemporalElement();

		/**
		 * The meta object literal for the '<em><b>Is Valid</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DW_TEMPORAL_ELEMENT___IS_VALID__DATE = eINSTANCE.getDwTemporalElement__IsValid__Date();

		/**
		 * The meta object literal for the '{@link de.darwinspl.temporal.impl.DwSingleTemporalIntervalElementImpl <em>Dw Single Temporal Interval Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.temporal.impl.DwSingleTemporalIntervalElementImpl
		 * @see de.darwinspl.temporal.impl.DwTemporalModelPackageImpl#getDwSingleTemporalIntervalElement()
		 * @generated
		 */
		EClass DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT = eINSTANCE.getDwSingleTemporalIntervalElement();

		/**
		 * The meta object literal for the '<em><b>Validity</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__VALIDITY = eINSTANCE.getDwSingleTemporalIntervalElement_Validity();

		/**
		 * The meta object literal for the '<em><b>Get From</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_FROM = eINSTANCE.getDwSingleTemporalIntervalElement__GetFrom();

		/**
		 * The meta object literal for the '<em><b>Get To</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_TO = eINSTANCE.getDwSingleTemporalIntervalElement__GetTo();

		/**
		 * The meta object literal for the '{@link de.darwinspl.temporal.impl.DwMultiTemporalIntervalElementImpl <em>Dw Multi Temporal Interval Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.temporal.impl.DwMultiTemporalIntervalElementImpl
		 * @see de.darwinspl.temporal.impl.DwTemporalModelPackageImpl#getDwMultiTemporalIntervalElement()
		 * @generated
		 */
		EClass DW_MULTI_TEMPORAL_INTERVAL_ELEMENT = eINSTANCE.getDwMultiTemporalIntervalElement();

		/**
		 * The meta object literal for the '<em><b>Validities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_MULTI_TEMPORAL_INTERVAL_ELEMENT__VALIDITIES = eINSTANCE.getDwMultiTemporalIntervalElement_Validities();

		/**
		 * The meta object literal for the '{@link de.darwinspl.temporal.impl.DwTemporalIntervalImpl <em>Dw Temporal Interval</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.temporal.impl.DwTemporalIntervalImpl
		 * @see de.darwinspl.temporal.impl.DwTemporalModelPackageImpl#getDwTemporalInterval()
		 * @generated
		 */
		EClass DW_TEMPORAL_INTERVAL = eINSTANCE.getDwTemporalInterval();

		/**
		 * The meta object literal for the '<em><b>From</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_TEMPORAL_INTERVAL__FROM = eINSTANCE.getDwTemporalInterval_From();

		/**
		 * The meta object literal for the '<em><b>To</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_TEMPORAL_INTERVAL__TO = eINSTANCE.getDwTemporalInterval_To();

	}

} //DwTemporalModelPackage
