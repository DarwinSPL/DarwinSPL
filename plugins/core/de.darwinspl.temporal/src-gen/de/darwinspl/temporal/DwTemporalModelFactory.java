/**
 */
package de.darwinspl.temporal;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.darwinspl.temporal.DwTemporalModelPackage
 * @generated
 */
public interface DwTemporalModelFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DwTemporalModelFactory eINSTANCE = de.darwinspl.temporal.impl.DwTemporalModelFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Dw Temporal Interval</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Temporal Interval</em>'.
	 * @generated
	 */
	DwTemporalInterval createDwTemporalInterval();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	DwTemporalModelPackage getDwTemporalModelPackage();

} //DwTemporalModelFactory
