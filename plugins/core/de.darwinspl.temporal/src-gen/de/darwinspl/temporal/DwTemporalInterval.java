/**
 */
package de.darwinspl.temporal;

import java.util.Date;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Temporal Interval</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.temporal.DwTemporalInterval#getFrom <em>From</em>}</li>
 *   <li>{@link de.darwinspl.temporal.DwTemporalInterval#getTo <em>To</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.temporal.DwTemporalModelPackage#getDwTemporalInterval()
 * @model
 * @generated
 */
public interface DwTemporalInterval extends EObject {
	/**
	 * Returns the value of the '<em><b>From</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From</em>' attribute.
	 * @see #setFrom(Date)
	 * @see de.darwinspl.temporal.DwTemporalModelPackage#getDwTemporalInterval_From()
	 * @model
	 * @generated
	 */
	Date getFrom();

	/**
	 * Sets the value of the '{@link de.darwinspl.temporal.DwTemporalInterval#getFrom <em>From</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From</em>' attribute.
	 * @see #getFrom()
	 * @generated
	 */
	void setFrom(Date value);

	/**
	 * Returns the value of the '<em><b>To</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To</em>' attribute.
	 * @see #setTo(Date)
	 * @see de.darwinspl.temporal.DwTemporalModelPackage#getDwTemporalInterval_To()
	 * @model
	 * @generated
	 */
	Date getTo();

	/**
	 * Sets the value of the '{@link de.darwinspl.temporal.DwTemporalInterval#getTo <em>To</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To</em>' attribute.
	 * @see #getTo()
	 * @generated
	 */
	void setTo(Date value);

} // DwTemporalInterval
