package de.darwinspl.temporal;

import de.darwinspl.temporal.impl.DwTemporalModelFactoryImpl;
import de.darwinspl.temporal.util.DwDateResolverUtil;

public class DwTemporalModelFactoryImplCustom extends DwTemporalModelFactoryImpl {

	public static DwTemporalModelFactory eINSTANCE = DwTemporalModelFactoryImplCustom.init();
	
	public static DwTemporalModelFactory init() {
		DwTemporalModelFactory featureFactory = DwTemporalModelFactoryImpl.init();
		if(!(featureFactory instanceof DwTemporalModelFactoryImplCustom)) {
			featureFactory = new DwTemporalModelFactoryImplCustom();
		}
		return featureFactory;
	}
	
	
	
	
	

	public DwTemporalModelFactoryImplCustom() {
		super();
	}
	
	@Override
	public DwTemporalInterval createDwTemporalInterval() {
		DwTemporalInterval dwTemporalInterval = new DwTemporalIntervalCustom();
		dwTemporalInterval.setFrom(DwDateResolverUtil.INITIAL_STATE_DATE);
		dwTemporalInterval.setTo(DwDateResolverUtil.ETERNITY_DATE);
		return dwTemporalInterval;
	}

}
