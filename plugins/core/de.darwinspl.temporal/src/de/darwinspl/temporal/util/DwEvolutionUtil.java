package de.darwinspl.temporal.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.darwinspl.common.eclipse.util.DwLogger;
import de.darwinspl.temporal.DwMultiTemporalIntervalElement;
import de.darwinspl.temporal.DwSingleTemporalIntervalElement;
import de.darwinspl.temporal.DwTemporalElement;
import de.darwinspl.temporal.DwTemporalInterval;
import de.darwinspl.temporal.DwTemporalModelFactory;

public class DwEvolutionUtil {

	/**
	 * Return type equals input type. IDs are preserved
	 * 
	 * @param model
	 * @param date
	 * @return
	 */
	public static <T extends EObject> T getCopyOfValidModel(T model, Date date) {
		DwTemporalModelCopier copier = new DwTemporalModelCopier();
		EObject result = copier.copy(model);
		copier.copyReferences();

		@SuppressWarnings("unchecked")
		T copiedModel = (T) result;

		removeAllInvalidElements(copiedModel, date);
		return copiedModel;
	}

	/**
	 * Return type equals input type. IDs NOT preserved
	 * 
	 * @param model
	 * @param date
	 * @return
	 */
	public static <T extends EObject> T getCopyOfValidModelWithNewIds(T model, Date date) {
		T copiedModel = EcoreUtil.copy(model);
		removeAllInvalidElements(copiedModel, date);
		return copiedModel;
	}
	
	/**
	 * Sorts depending on validSince (ascending, starting with validSince == null)
	 * @param elements
	 * @return
	 */
//	public static <T extends DwTemporalElement> List<T> sortInChronologicalOrder(List<T> elements) {
//		List<Date> dates = collectDates(elements);
//		
//		List<T> sortedElements = new LinkedList<T>();
//		
//		for (T element : elements) {
//			if (element.getFrom() == null) {
//				sortedElements.add(element);
//			}
//		}
//		
//		for (Date date : dates) {
//			for (T element : elements) {
//				if (element.getFrom() != null) {
//					if (element.getFrom().equals(date)) {
//						sortedElements.add(element);
//					}
//				}
//			}
//		}
//		
//		return sortedElements;
//	}
	
	/**
	 * 
	 * @param elements
	 * @return
	 */
//	public static <T extends DwTemporalElement> T getEarliest(Collection<T> elements) {
//		return getEarliestAfter(elements, null);
//	}
	
	/**
	 * 
	 * @param elements
	 * @param after
	 * @return
	 */
//	public static <T extends DwTemporalElement> T getEarliestAfter(Collection<T> elements, Date after) {
//		boolean first = true;
//		Date earliestSince = null;
//		T earliestElement = null;
//		
//		for (T element : elements) {
//			Date validSince = element.getFrom();
//			
//			if (after != null && (validSince == null || !validSince.after(after))) {
//				continue;
//			}
//			
//			if (first) {
//				earliestSince = validSince;
//				earliestElement = element;
//				first = false;
//				continue;
//			}
//			
//			if (validSince == null) {
//				earliestSince = null;
//				earliestElement = element;
//			}
//			else {
//				if(earliestSince != null && validSince.before(earliestSince)) {
//					earliestSince = validSince;
//					earliestElement = element;
//				}
//			}
//		}
//		
//		return earliestElement;
//	}

	/**
	 * Use with caution! Deletes elements from model with DwTemporalElements!
	 * 
	 * @param featureModel
	 * @param date
	 */
	public static void removeAllInvalidElements(EObject model, Date date) {
		List<EObject> eObjectsToRemove = new LinkedList<EObject>();
		{
			TreeIterator<EObject> iterator = model.eAllContents();

			while (iterator.hasNext()) {
				EObject content = iterator.next();
				if (content instanceof DwTemporalElement) {
					if (!isValid(((DwTemporalElement) content), date)) {
						// EcoreUtil.remove(content);
						eObjectsToRemove.add(content);
					}
				}
			}

		}

		for (EObject eObjectToRemove : eObjectsToRemove) {
			EcoreUtil.delete(eObjectToRemove, true);
		}
	}

	/**
	 * Checks if an element is valid at a given date
	 * 
	 * @param temporalElement
	 * @param date
	 * @return
	 */
	public static boolean isValid(DwTemporalElement temporalElement, Date date) {
		if (temporalElement instanceof DwSingleTemporalIntervalElement) {
			DwSingleTemporalIntervalElement singleTemporalElement = (DwSingleTemporalIntervalElement) temporalElement;
			
			if(singleTemporalElement.getValidity() == null) {
				DwLogger.logWarning("Please check your model!!! No validity set for a " + singleTemporalElement.getClass().getSimpleName() + " (id = " + singleTemporalElement.getId() + ")");
				return true;
			}
			else {
				return isInInterval(date, singleTemporalElement.getValidity());
			}
		}
		else if (temporalElement instanceof DwMultiTemporalIntervalElement) {
			DwMultiTemporalIntervalElement multiTemporalElement = (DwMultiTemporalIntervalElement) temporalElement;
			
			for (DwTemporalInterval interval : multiTemporalElement.getValidities()) {
				if (isInInterval(date, interval)) {
					return true;
				}
			}
			
			return false;
		}
		
		return false;
	}
	
	public static boolean isInInterval(Date date, DwTemporalInterval interval) {
		if (date == null) {
			if (interval.getFrom() == null/* && interval.getTo() == null*/) {
				return true;
			} else {
				return false;
			}
		}

		if ((interval.getFrom().before(date) || interval.getFrom().equals(date)) && (interval.getTo().after(date))) {
			return true;
		}
		
		return false;
	}

	/**
	 * Checks if the validity of @elementToCheck is within the validity
	 * of @elementToCheckAgainst Usable, e.g., to check if a constraint's
	 * ( @elementToCheck ) validity is no longer than it's referencing features'
	 * validity ( @elementToCheckAgainst )
	 * 
	 * @param elementToCheck
	 * @param elementToCheckAgainst
	 * @return
	 */
	public static boolean isWithinValidityOf(DwTemporalElement elementToCheck,
			DwTemporalElement elementToCheckAgainst) {
		List<DwTemporalInterval> intervalsOfElementToCheck = new ArrayList<>();
		List<DwTemporalInterval> intervalsOfElementToCheckAgainst = new ArrayList<>();
		
		if (elementToCheck instanceof DwSingleTemporalIntervalElement) {
			intervalsOfElementToCheck.add(((DwSingleTemporalIntervalElement) elementToCheck).getValidity());
		}
		else if (elementToCheck instanceof DwMultiTemporalIntervalElement) {
			intervalsOfElementToCheck.addAll(((DwMultiTemporalIntervalElement) elementToCheck).getValidities());
		}

		if (elementToCheckAgainst instanceof DwSingleTemporalIntervalElement) {
			intervalsOfElementToCheckAgainst.add(((DwSingleTemporalIntervalElement) elementToCheckAgainst).getValidity());
		}
		else if (elementToCheckAgainst instanceof DwMultiTemporalIntervalElement) {
			intervalsOfElementToCheckAgainst.addAll(((DwMultiTemporalIntervalElement) elementToCheckAgainst).getValidities());
		}
		
		for (DwTemporalInterval intervalOfElementToCheck : intervalsOfElementToCheck) {
			for (DwTemporalInterval intervalOfElementToCheckAgainst : intervalsOfElementToCheckAgainst) {
				if (isWithinValidityOf(intervalOfElementToCheck, intervalOfElementToCheckAgainst)) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	public static boolean isWithinValidityOf(DwTemporalInterval intervalToCheck,
			DwTemporalInterval intervalToCheckAgainst) {
		if (intervalToCheck.getFrom() == null && intervalToCheckAgainst.getFrom() != null) {
			return false;
		} else if (intervalToCheck.getFrom() != null && intervalToCheckAgainst.getFrom() != null
				&& intervalToCheck.getFrom().before(intervalToCheckAgainst.getFrom())) {
			return false;
		} else if (intervalToCheck.getTo() == null && intervalToCheckAgainst.getTo() != null) {
			return false;
		} else if (intervalToCheck.getTo() != null && intervalToCheckAgainst.getTo() != null
				&& intervalToCheck.getTo().after(intervalToCheckAgainst.getTo())) {
			return false;
		}
		return true;
	}
	
	

	/**
	 * Checks if the validity of @elementToCheck is within the validity of each
	 * of @elementsToCheckAgainst Usable, e.g., to check if a constraint's
	 * ( @elementToCheck ) validity is no longer than it's referencing features'
	 * validity ( @elementsToCheckAgainst )
	 * 
	 * @param elementToCheck
	 * @param elementsToCheckAgainst
	 * @return
	 */
	public static boolean isWithinValidityOf(DwTemporalElement elementToCheck,
			List<DwTemporalElement> elementsToCheckAgainst) {
		for (DwTemporalElement elementToCheckAgainst : elementsToCheckAgainst) {
			if (!isWithinValidityOf(elementToCheck, elementToCheckAgainst)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks if the each validity of @elementsToCheck is within the validity
	 * of @elementToCheckAgainst Usable, e.g., to check if a constraint's
	 * ( @elementsToCheck ) validity is no longer than it's referencing features'
	 * validity ( @elementToCheckAgainst )
	 * 
	 * @param elementsToCheck
	 * @param elementToCheckAgainst
	 * @return
	 */
	public static boolean areWithinValidityOf(List<DwTemporalElement> elementsToCheck,
			DwTemporalElement elementToCheckAgainst) {
		for (DwTemporalElement elementToCheck : elementsToCheck) {
			if (!isWithinValidityOf(elementToCheck, elementToCheckAgainst)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks if the each validity of @elementsToCheck is within the validity of
	 * each @elementsToCheckAgainst Usable, e.g., to check if a constraint's
	 * ( @elementsToCheck ) validity is no longer than it's referencing features'
	 * validity ( @elementsToCheckAgainst )
	 * 
	 * @param elementsToCheck
	 * @param elementsToCheckAgainst
	 * @return
	 */
	public static boolean areWithinValitiyOf(List<DwTemporalElement> elementsToCheck,
			List<DwTemporalElement> elementsToCheckAgainst) {
		for (DwTemporalElement elementToCheck : elementsToCheck) {
			if (!isWithinValidityOf(elementToCheck, elementsToCheckAgainst)) {
				return false;
			}
		}
		return true;
	}

	// TODO assumption only one valid temporal element at a time, check that?
	public static <T extends DwTemporalElement> T getValidTemporalElement(List<T> elements, Date date) {
		if (elements == null || elements.isEmpty()) {
			return null;
		}

//		if (date == null) {
//			if (elements.size() == 1) {
//				return elements.get(0);
//			} else {
//				DwLogger.logWarning("Something bad happened. Date given to getValidTemporalElement(...) in DwEvolutionUtil.java was null and more than one element was present.");
//				return null;
//			}
//		}

		for (T element : elements) {
			if (isValid(element, date)) {
				return element;
			}
		}
		
		return null;
	}

	/**
	 * Non copy
	 * 
	 * @param elements
	 * @param date
	 * @return
	 */
	public static <T extends DwTemporalElement> List<T> getValidTemporalElements(List<T> elements, Date date) {
		List<T> validElements = new LinkedList<T>();

		if (elements == null || elements.isEmpty()) {
			return validElements;
		}

		if (date == null) {
//			System.out.println(
//					"Date given to getValidTemporalElements(...) in HyEvolutionUtil.java was null. Returning all elements!");
			validElements.addAll(elements);
			return validElements;
		}

		for (T element : elements) {
			if (isValid(element, date)) {
				validElements.add(element);
			}
		}

		return validElements;
	}

	public static <T extends DwTemporalElement> List<T> getInvalidTemporalElements(List<T> elements, Date date) {
		List<T> invalidElements = new LinkedList<T>(elements);

		if (date != null) {
			invalidElements.removeAll(getValidTemporalElements(elements, date));
		}

		return invalidElements;
	}
	
	/**
	 * Returns a list that contains all elements of @param elements that are valid within the interval between @param lowerBound and @param upperBound
	 * @param elements
	 * @param lowerBound
	 * @param upperBound
	 * @return
	 */
	public static <T extends DwTemporalElement> List<T> getValidTemporalElements(List<T> elements, Date lowerBound, Date upperBound) {
		Set<Date> datesToCheck = new HashSet<Date>();
		
		if (lowerBound != null) {
			datesToCheck.add(lowerBound);
		}
		
		List<Date> datesOfElements = collectDates(elements);
		for (Date dateOfElement : datesOfElements) {
			if ((upperBound == null || dateOfElement.before(upperBound)) && (lowerBound == null || !lowerBound.after(dateOfElement))) {
				datesToCheck.add(dateOfElement);
			}
		}
		
		List<T> validElements = new LinkedList<T>();
		
		for (T element : elements) {
			for (Date dateToCheck : datesToCheck) {
				if (isValid(element, dateToCheck)) {
					validElements.add(element);
				}
			}
		}
		
		return validElements;
	}
	
	/**
	 * Returns the temporal elements that end on a given @param date or null if none contained in the given list of @param elements.
	 * @param elements
	 * @param date
	 * @return
	 */
	public static <T extends DwTemporalElement> List<DwTemporalInterval> getTemporalElementsForEndDate(List<T> elements, Date date) {
		List<DwTemporalInterval> matchingIntervals = new ArrayList<>();
		
		for(DwTemporalElement element : elements) {
			DwTemporalInterval matchingInterval = getIntervalForEndDate(element, date);
			if(matchingInterval != null)
				matchingIntervals.add(matchingInterval);
		}
		
		return matchingIntervals;
	}

	/**
	 * Returns a sorted list of all available dates of DwTemporalElements in the
	 * given model
	 * 
	 * @param model
	 * @return
	 */
	public static List<Date> collectDates(EObject model) {
		if (model == null)
			return new LinkedList<Date>();

		List<DwTemporalElement> temporalElements = getTemporalElementChildren(model);
		return collectDates(temporalElements);
	}
	
	public static List<Date> collectIntermediateDates(EObject model) {
		List<Date> dates = collectDates(model);
		dates.remove(dates.size()-1);
		return dates;
	}

	private static List<DwTemporalElement> getTemporalElementChildren(EObject model) {
		List<DwTemporalElement> result = new LinkedList<>();
		
		for(EObject child : model.eContents()) {
			if (child instanceof DwTemporalElement)
				result.add((DwTemporalElement) child);
			
			result.addAll(getTemporalElementChildren(child));
		}
		
		return result;
	}

	/**
	 * Collects a sorted list of unique dates of valid until and valid since of the
	 * given elements
	 * 
	 * @param elements
	 * @return sorted list of unique dates (created using a set)
	 */
	public static List<Date> collectDates(List<? extends DwTemporalElement> elements) {
		// NOTE: Check if equals and identity work for hash set.
		Set<Date> rawDates = new HashSet<Date>();

		for (DwTemporalElement temporalElement : elements) {
			List<DwTemporalInterval> intervals = new ArrayList<>();
			
			if (temporalElement instanceof DwSingleTemporalIntervalElement) {
				DwTemporalInterval validity = ((DwSingleTemporalIntervalElement) temporalElement).getValidity();
				if(validity != null)
					intervals.add(validity);
			}
			else if (temporalElement instanceof DwMultiTemporalIntervalElement) {
				intervals = ((DwMultiTemporalIntervalElement) temporalElement).getValidities();
			}
			
			for (DwTemporalInterval interval : intervals) {
				Date validSince = interval.getFrom();

				if (!(validSince.equals(DwDateResolverUtil.INITIAL_STATE_DATE) || validSince.equals(DwDateResolverUtil.ETERNITY_DATE))) {
					rawDates.add(validSince);
				}

				Date validUntil = interval.getTo();

				if (!(validUntil.equals(DwDateResolverUtil.INITIAL_STATE_DATE) || validUntil.equals(DwDateResolverUtil.ETERNITY_DATE))) {
					rawDates.add(validUntil);
				}
			}
		}
		List<Date> dates = new ArrayList<Date>(rawDates);

		Collections.sort(dates);

		return dates;
	}

	public static boolean modelHasValiditySinceNull(EObject model) {
		Iterator<EObject> iterator = model.eAllContents();

		while (iterator.hasNext()) {
			EObject eObject = iterator.next();

			if (eObject instanceof DwSingleTemporalIntervalElement) {
				if (((DwSingleTemporalIntervalElement) eObject).getValidity().getFrom() == null) {
					return true;
				}
			}
			else if (eObject instanceof DwMultiTemporalIntervalElement) {
				for (DwTemporalInterval interval : ((DwMultiTemporalIntervalElement) eObject).getValidities()) {
					if (interval.getFrom() == null) {
						return true;
					}
				}
			}
		}

		return false;
	}

	public static boolean modelHasValidityUntilNull(EObject model) {
		Iterator<EObject> iterator = model.eAllContents();

		while (iterator.hasNext()) {
			EObject eObject = iterator.next();

			if (eObject instanceof DwSingleTemporalIntervalElement) {
				if (((DwSingleTemporalIntervalElement) eObject).getValidity().getTo() == null) {
					return true;
				}
			}
			else if (eObject instanceof DwMultiTemporalIntervalElement) {
				for (DwTemporalInterval interval : ((DwMultiTemporalIntervalElement) eObject).getValidities()) {
					if (interval.getTo() == null) {
						return true;
					}
				}
			}
		}

		return false;
	}

	/**
	 * Collects all dates from parameter temporalModel and selects the date that is
	 * closest to the parameter date. If parameter date == null, the earliest date
	 * will be selected. If dates from parameter temporalModel are empty, null is
	 * returned.
	 * 
	 * @param dates
	 * @param date
	 * @return May be null if parameter temporalModel has no evolution steps.
	 */
	public static Date getClosestEvolutionDate(EObject temporalModel, Date date) {
		List<Date> dates = collectDates(temporalModel);

		return getClosestEvolutionDate(dates, date);
	}

	/**
	 * Selects the date of parameter dates that is closest to parameter date. If
	 * parameter date == null, the earliest date will be selected. If parameter are
	 * empty, null is returned.
	 * 
	 * @param dates
	 * @param date
	 * @return May be null if parameter temporalModel has no evolution steps.
	 */
	public static Date getClosestEvolutionDate(Collection<Date> dates, Date date) {
		if (dates == null || dates.isEmpty()) {
			return DwDateResolverUtil.INITIAL_STATE_DATE;
		}

		Date closestDate = null;

		for (Date iterator : dates) {
			if (iterator.equals(date)) {
				return iterator;
			} else if (closestDate == null) {
				closestDate = iterator;
			} else {
				long distanceToOldClosestDate = (Math.abs(date.getTime() - closestDate.getTime()));
				long distanceToDate = (Math.abs(date.getTime() - iterator.getTime()));

				if (distanceToOldClosestDate > distanceToDate) {
					closestDate = iterator;
				}
			}
		}

		return closestDate;
	}

	public static <T extends DwTemporalElement> boolean isEverValid(T element) {
		if (element == null) {
			return false;
		}

		List<DwTemporalInterval> intervals = new ArrayList<>();
		
		if (element instanceof DwSingleTemporalIntervalElement) {
			intervals.add(((DwSingleTemporalIntervalElement) element).getValidity());
		}
		else if (element instanceof DwMultiTemporalIntervalElement) {
			intervals = ((DwMultiTemporalIntervalElement) element).getValidities();
		}
		
		for (DwTemporalInterval interval : intervals) {
			if(!isEverValid(interval))
				return false;
		}

		
		return true;
	}

	public static boolean isEverValid(DwTemporalInterval interval) {
//		return !( (interval.getFrom() != null && interval.getTo() != null && !interval.getTo().after(interval.getFrom()))
//				|| (interval.getFrom() == null && interval.getTo() != null && interval.getTo().getTime() == Long.MIN_VALUE) );
		
		return interval.getTo().after( interval.getFrom() );
	}
	
	
	/**
	 * Returns the given temporal element's valid interval for a given date.
	 * If no interval exists that encloses the given date, null is returned.
	 * 
	 * @param element a temporal element
	 * @param date a date for which a temporal interval is searched
	 * @return An interval that encloses the given date. Null if none exists
	 */
	public static DwTemporalInterval getValidIntervalForDate(DwTemporalElement element, Date date) {
		if(element == null)
			throw new NullPointerException("Given element must not be null");
		
		if(element instanceof DwSingleTemporalIntervalElement) {
			DwTemporalInterval singleInterval = ((DwSingleTemporalIntervalElement) element).getValidity();
			if(isInInterval(date, singleInterval))
				return singleInterval;
			else
				return null;
		}
		else if(element instanceof DwMultiTemporalIntervalElement) {
			for(DwTemporalInterval multiInterval : ((DwMultiTemporalIntervalElement) element).getValidities()) {
				if(isInInterval(date, multiInterval))
					return multiInterval;
			}
			return null;
		}
		
		throw new RuntimeException("Something weird happened. How is it possible for the given element to be neither of type DwSingleTemporalIntervalElement nor of type DwSingleTemporalIntervalElement?");
	}

	
	/**
	 * Returns the given temporal element's valid interval for a given <strong>end</strong> date.
	 * If no interval exists that ends on the given date, null is returned.
	 * 
	 * @param element a temporal element
	 * @param date an end date for which a temporal interval is searched
	 * @return An interval that ends on the given date. Null if none exists
	 */
	public static DwTemporalInterval getIntervalForEndDate(DwTemporalElement element, Date date) {
		if(element instanceof DwSingleTemporalIntervalElement) {
			DwTemporalInterval singleInterval = ((DwSingleTemporalIntervalElement) element).getValidity();
			if(date == null)
				if(singleInterval.getTo() == null)
					return singleInterval;
				else
					return null;
			
			if(date.equals(singleInterval.getTo()))
				return singleInterval;
			else
				return null;
		}
		else if(element instanceof DwMultiTemporalIntervalElement) {
			for(DwTemporalInterval multiInterval : ((DwMultiTemporalIntervalElement) element).getValidities()) {
				if(date == null)
					if(multiInterval.getTo() == null)
						return multiInterval;
				if(date.equals(multiInterval.getTo()))
					return multiInterval;
			}
			return null;
		}
		
		throw new RuntimeException("Something weird happened. How is it possible for the given element to be neither of type DwSingleTemporalIntervalElement nor of type DwSingleTemporalIntervalElement?");
	}
	
	
	
	public static DwTemporalInterval getIntervalForStartDate(DwTemporalElement element, Date date) {
		if(element instanceof DwSingleTemporalIntervalElement) {
			DwTemporalInterval singleInterval = ((DwSingleTemporalIntervalElement) element).getValidity();
			if(date == null)
				if(singleInterval.getFrom() == null)
					return singleInterval;
				else
					return null;
			
			if(date.equals(singleInterval.getFrom()))
				return singleInterval;
			else
				return null;
		}
		else if(element instanceof DwMultiTemporalIntervalElement) {
			for(DwTemporalInterval multiInterval : ((DwMultiTemporalIntervalElement) element).getValidities()) {
				if(date == null)
					if(multiInterval.getFrom() == null)
						return multiInterval;
				if(date.equals(multiInterval.getFrom()))
					return multiInterval;
			}
			return null;
		}
		
		throw new RuntimeException("Something weird happened. How is it possible for the given element to be neither of type DwSingleTemporalIntervalElement nor of type DwSingleTemporalIntervalElement?");
	}
	
	

	public static <T extends DwTemporalElement> T getTemporalElementForEndDate(EList<T> elements, Date date) {
		T matchingElement = null;
		
		for(T element : elements) {
			if(getIntervalForEndDate(element, date) != null) {
				if(matchingElement == null)
					matchingElement = element;
				else
					throw new UnsupportedOperationException("getTemporalElementForEndDate found multiple intervals for an end date");
			}
		}
		
		return matchingElement;
	}
	
	

	public static <T extends DwTemporalElement> T getTemporalElementForStartDate(EList<T> elements, Date date) {
		T matchingElement = null;
		
		for(T element : elements) {
			if(getIntervalForStartDate(element, date) != null) {
				if(matchingElement == null)
					matchingElement = element;
				else
					throw new UnsupportedOperationException("getTemporalElementForEndDate found multiple intervals for an end date");
			}
		}
		
		return matchingElement;
	}
	
	

	public static <T extends DwSingleTemporalIntervalElement> List<T> getElementsThatStartAtOrAfterDate(EList<T> elements, Date date) {
		List<T> matchingElement = new ArrayList<>();
		
		for(T element : elements) {
			if(element.getFrom().compareTo(date) >= 0) {
				matchingElement.add(element);
			}
		}
		
		return matchingElement;
	}
	
	
	/**
	 * Sets a new interval to the given element's temporal validity
	 * 
	 * @param element a temporal element with a single temporal validity
	 * @param from a start date for the new temporal validity
	 * @param to a stop date for the new temporal validity
	 */
	public static void setTemporalValidity(DwSingleTemporalIntervalElement element, Date from, Date to) {
		DwTemporalInterval newValidity = DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
		setValidity(newValidity, from, to);
		element.setValidity(newValidity);
	}
	
	
	/**
	 * Change the currently valid interval (withr espect to given @param date) of a multi interval element
	 * 
	 * @param element a temporal element with multiple temporal validities
	 * @param date the date that is the current time
	 * @param from new start date of found interval
	 * @param to new end date of found interval
	 */
	public static void setCurrentTemporalValidity(DwMultiTemporalIntervalElement element, Date date, Date from, Date to) {
		DwTemporalInterval existingValidity = getValidIntervalForDate(element, date);
		if(existingValidity != null) {
			setValidity(existingValidity, from, to);
		}
	}
	
	
	/**
	 * Adds a new interval to the given element's list of temporal validities
	 * 
	 * @param element a temporal element with multiple temporal validities
	 * @param from a start date for the new temporal validity
	 * @param to a stop date for the new temporal validity
	 */
	public static void addNewTemporalValidity(DwMultiTemporalIntervalElement element, Date from, Date to) {
		DwTemporalInterval newValidity = DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
		setValidity(newValidity, from, to);
		element.getValidities().add(newValidity);
	}
	
	
	
	public static void setValidity(DwTemporalInterval interval, Date from, Date to) {
		if(interval == null)
			return;
		
		if(from != null) {
			if(from.equals(DwDateResolverUtil.ETERNITY_DATE))
				throw new RuntimeException("Tried to set an interval start to eternity");
			else
				interval.setFrom(from);
		}

		if(to != null) {
			if(to.equals(DwDateResolverUtil.INITIAL_STATE_DATE))
				throw new RuntimeException("Tried to set an interval end to the initial state");
			else
				interval.setTo(to);
		}

		if(interval.getFrom() == null)
			interval.setFrom(DwDateResolverUtil.INITIAL_STATE_DATE);
		if(interval.getTo() == null)
			interval.setTo(DwDateResolverUtil.ETERNITY_DATE);
	}
	
}






















