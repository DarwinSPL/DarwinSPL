package de.darwinspl.temporal.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;

import de.darwinspl.temporal.DwTemporalElement;

public class DwTemporalModelCopier extends Copier {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8170521632121647245L;

	
	@Override
    public EObject copy(EObject eObject) {
		EObject copiedObject = super.copy(eObject);
		
		if(eObject instanceof DwTemporalElement) {
			DwTemporalElement copiedElement = (DwTemporalElement) copiedObject;
			DwTemporalElement element = (DwTemporalElement) eObject;
			copiedElement.setId(element.getId());
		}
		
		return copiedObject;
	}
}
