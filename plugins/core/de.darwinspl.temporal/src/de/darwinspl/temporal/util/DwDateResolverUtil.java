package de.darwinspl.temporal.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.darwinspl.temporal.DwTemporalInterval;

public class DwDateResolverUtil {

	protected static final String DEFAULT_DATE_FORMAT_STRING = "yyyy-MM-dd";
	protected static final String ALTERNATIVE_DATE_FORMAT_STRING = "yyyy/MM/dd";
	
	protected static final String DEFAULT_DATE_FORMAT_TIME_STRING = DEFAULT_DATE_FORMAT_STRING + "'T'HH:mm:ss";
	protected static final String ALTERNATIVE_DATE_FORMAT_TIME_STRING = ALTERNATIVE_DATE_FORMAT_STRING + "'T'HH:mm:ss";
	
	protected static final SimpleDateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat(DEFAULT_DATE_FORMAT_STRING);
	protected static final SimpleDateFormat DEFAULT_DATE_FORMAT_TIME = new SimpleDateFormat(DEFAULT_DATE_FORMAT_TIME_STRING);
	
	protected static final SimpleDateFormat ALTERNATIVE_DATE_FORMAT = new SimpleDateFormat(ALTERNATIVE_DATE_FORMAT_STRING);
	protected static final SimpleDateFormat ALTERNATIVE_DATE_FORMAT_TIME = new SimpleDateFormat(ALTERNATIVE_DATE_FORMAT_TIME_STRING);

	public static final String INITIAL_STATE_DATE_STRING = "initial state";
	public static final String ETERNITY_DATE_STRING = "eternity";

	public static final long INITIAL_STATE_DATE_LONG = 0L;
	public static final long ETERNITY_DATE_LONG = Long.MAX_VALUE;

	public static final Date INITIAL_STATE_DATE = new Date(INITIAL_STATE_DATE_LONG);
	public static final Date ETERNITY_DATE = new Date(ETERNITY_DATE_LONG);
	
	
	
	public static Date resolveDate(String dateString) {
		if(dateString == null || dateString.equals("")) {
			return null;
		}
		
		String[] splits = dateString.split(":");
		
		SimpleDateFormat dateFormat;
		
		switch(splits.length) {
		case 1:
			dateFormat = DEFAULT_DATE_FORMAT;
			break;
		case 2:
			dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT_STRING+"'T'HH:mm");
			break;
		case 3:
			dateFormat = DEFAULT_DATE_FORMAT_TIME;
			break;
		default:
			dateFormat = DEFAULT_DATE_FORMAT;
			break;
		}
		
		return resolveDate(dateString, dateFormat);
	}
	
	public static Date resolveDateAlternative(String dateString) {
		DateFormat dateFormat = ALTERNATIVE_DATE_FORMAT_TIME;
		
		if (!dateString.contains("T")) {
			dateFormat = ALTERNATIVE_DATE_FORMAT;
		}
		return resolveDate(dateString, dateFormat);
	}
	
	public static Date resolveDate(String dateString, DateFormat customDateFormat) {
		if(dateString.equalsIgnoreCase(INITIAL_STATE_DATE_STRING))
			return INITIAL_STATE_DATE;
		if(dateString.equalsIgnoreCase(ETERNITY_DATE_STRING))
			return ETERNITY_DATE;
		
		try {
			return customDateFormat.parse(dateString);
		} catch (ParseException e) {
//			e.printStackTrace();
			return null;
		}
	}
	
	public static String deresolveDate(Date date) {
		return deresolveDate(date, DEFAULT_DATE_FORMAT_TIME);
	}
	
	public static String deresolveDateAlternative(Date date) {
		return deresolveDate(date, ALTERNATIVE_DATE_FORMAT_TIME);
	}
	
	public static String deresolveDate(Date date, DateFormat customDateFormat) {
		if(date.equals(INITIAL_STATE_DATE))
			return INITIAL_STATE_DATE_STRING;
		if(date.equals(ETERNITY_DATE))
			return ETERNITY_DATE_STRING;

		return customDateFormat.format(date);
	}
	
	public static String deresolveInterval(DwTemporalInterval interval) {
		String startDate = deresolveDate(interval.getFrom());
		String endDate = deresolveDate(interval.getTo());
		
		return startDate + "  ->  " + endDate;
	}

	public static String getTimeAsString(long millis, boolean displayMillis) {
		int seconds = (int) Math.ceil((double) millis / 1000.0d);
		int minutes = (int) Math.floor(seconds / 60.0d);
		int hours = (int) Math.floor(minutes / 60.0d);
		
		seconds = seconds  % 60;
		minutes = minutes  % 60;
		
		String readableTime = seconds + "s";
		if(minutes > 0)
			readableTime = minutes + "m " + readableTime;
		if(hours > 0)
			readableTime = hours + "h " + readableTime;
		
		if(displayMillis)
			return millis + "ms (" + readableTime + ")";
		else
			return readableTime;
	}
}
