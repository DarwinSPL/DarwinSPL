package de.darwinspl.temporal;

import de.darwinspl.temporal.impl.DwTemporalIntervalImpl;

public class DwTemporalIntervalCustom extends DwTemporalIntervalImpl {
	
	@Override
	public String toString() {
		return "[" + this.from + ", " + this.to + ")";
	}

}
