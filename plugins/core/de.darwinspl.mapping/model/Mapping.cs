SYNTAXDEF dwmapping
FOR <http://www.darwinspl.de/mapping/2.0>
START DwMappingModel

IMPORTS {
	dwexpression : <http://www.darwinspl.de/expression/2.0> WITH SYNTAX expression <../../de.darwinspl.expression/model/Expression.cs>
}

OPTIONS {	
	reloadGeneratorModel = "false";
	usePredefinedTokens = "false";
	
	defaultTokenName = "IDENTIFIER_TOKEN";
	
	editorName = "DarwinSPL Mapping Editor";
	newFileWizardCategory = "de.darwinspl.newwizards.Category"; // TODO
	newFileWizardName = "DarwinSPL Mapping (*.dwmapping)";
	
	disableLaunchSupport = "true";
	disableDebugSupport = "true";
	disableBuilder = "true";
	disableNewProjectWizard = "true";
	
	overrideUIManifest = "false";
}

TOKENS {
	DEFINE PATH_TO_FILE $'<' ('A'..'Z'|'a'..'z'|'0'..'9'|'_'|' '|'/')+ '.' ('A'..'Z'|'a'..'z'|'0'..'9'|'_'|' '|'/')+ '>'$;
	DEFINE IDENTIFIER_TOKEN $('A'..'Z'|'a'..'z'|'_')('A'..'Z'|'a'..'z'|'_'|'0'..'9')*$;
	
	DEFINE SL_COMMENT $'//'(~('\n'|'\r'|'\uffff'))*$;
	DEFINE ML_COMMENT $'/*'.*'*/'$;
	
	DEFINE LINEBREAK $('\r\n'|'\r'|'\n')$;
	DEFINE WHITESPACE $(' '|'\t'|'\f')$;
}

TOKENSTYLES  {
	"SL_COMMENT", "ML_COMMENT" COLOR #008000;
		
	"<->", "->", "||", "&&", "!" COLOR #800040, BOLD;
	
	"PATH_TO_FILE" COLOR #800040, BOLD;
	
	//"^" COLOR #800040, BOLD;
	// "?" COLOR #800040, BOLD;
	
	// "<", ">" COLOR #800040, BOLD;
	"[", "]", "(", ")" COLOR #0000CC;
}

RULES {
	//Explicitly allow that there are no mappings, e.g., for generated empty mapping files.
	DwMappingModel ::= 
	"feature" "model" featureModel[PATH_TO_FILE] !0!0
	(mappings (!0!0 mappings)*)?;
	
	DwMapping ::= condition #1 ":" !1 fileReferences ("," !0 fileReferences)* (validity)?;
	
	DwFileReference ::= pathToFile[PATH_TO_FILE];
	
	@SuppressWarnings(explicitSyntaxChoice)
	DwTemporalInterval ::= "[" (
									from[DATE] "-" to[DATE] |
									from[DATE] "-" "eternity" |
									"initial state" "-" to[DATE] |
									"initial state" "-" "eternity"
								)
							"]";
}
