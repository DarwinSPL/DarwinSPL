package de.darwinspl.mapping.validation.constraints;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;

import de.darwinspl.mapping.DwFileReference;
import de.darwinspl.mapping.util.DwMappingUtil;

public class DwMappingFileReferenceExistsConstraint extends AbstractModelConstraint {

	public DwMappingFileReferenceExistsConstraint() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public IStatus validate(IValidationContext ctx) {
		EObject eObj = ctx.getTarget();
		
		if (eObj != null && eObj instanceof DwFileReference) {
			DwFileReference fileReference = (DwFileReference) eObj;
			
			IFile file = DwMappingUtil.resolveFileFromMappingPath(fileReference);
			if(file == null || !file.exists()) {
				return ctx.createFailureStatus(new Object[] {fileReference.getPathToFile()});
			}
		}
		
		return ctx.createSuccessStatus();
	}

}
