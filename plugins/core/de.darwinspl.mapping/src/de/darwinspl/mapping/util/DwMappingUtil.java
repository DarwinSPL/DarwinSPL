package de.darwinspl.mapping.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.emf.ecore.resource.Resource;

import de.christophseidl.util.ecore.EcoreIOUtil;
import de.darwinspl.mapping.DwFileReference;
import de.darwinspl.mapping.DwMapping;

public class DwMappingUtil {

	private static final String FILE_EXTENSION_FOR_CONCRETE_SYNTAX = "dwmapping";
	private static final String FILE_EXTENSION_FOR_XMI = "dwmapping_xmi";
	
	public static IFile resolveFileFromMappingPath(DwFileReference fileReference) {
		Resource containerResource = fileReference.eResource();
		
		IFile containingIFile = EcoreIOUtil.getFile(containerResource);
		
		String pathToFile = fileReference.getPathToFile();
		
		IResource referencedFile = containingIFile.getParent().findMember(pathToFile.substring(1, pathToFile.length()-1));
		if(referencedFile == null || !referencedFile.exists()) {
			return null;
		}
		
		if (referencedFile instanceof IFile) {
			return (IFile) referencedFile;
		}
		
		return null;
	}
	
	public static List<IFile> resolveFilesFromMapping(DwMapping mapping) throws DwNonExistentFileReferenceException {
		List<DwFileReference> mappingFileReferences = mapping.getFileReferences();
		
		List<IFile> referencedFiles = new ArrayList<>(mappingFileReferences.size());
		
		for (DwFileReference fileReference : mappingFileReferences) {
			IFile resolvedFile = resolveFileFromMappingPath(fileReference);
			
			if (resolvedFile == null) {
				throw new DwNonExistentFileReferenceException("Referenced file "+fileReference.getPathToFile()+" does not exist, is no file, or is not in the same project.");
			}
			
			referencedFiles.add(resolvedFile);
		}
		
		return referencedFiles;
	}

	public static String getFILE_EXTENSION_FOR_CONCRETE_SYNTAX() {
		return FILE_EXTENSION_FOR_CONCRETE_SYNTAX;
	}

	public static String getFILE_EXTENSION_FOR_XMI() {
		return FILE_EXTENSION_FOR_XMI;
	}

	
}
