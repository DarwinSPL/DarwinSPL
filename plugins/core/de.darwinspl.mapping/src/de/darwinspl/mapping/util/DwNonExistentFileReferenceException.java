package de.darwinspl.mapping.util;

public class DwNonExistentFileReferenceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5846780327926495840L;

	public DwNonExistentFileReferenceException(String message) {
		super(message);
	}
	
}
