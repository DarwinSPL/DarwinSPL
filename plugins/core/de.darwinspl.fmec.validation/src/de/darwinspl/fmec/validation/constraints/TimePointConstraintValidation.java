package de.darwinspl.fmec.validation.constraints;

import static de.darwinspl.fmec.validation.constraints.ConstraintValidationUtil.isEvent;
import static de.darwinspl.fmec.validation.constraints.ConstraintValidationUtil.isExplicitTimePoint;
import static de.darwinspl.fmec.validation.constraints.ConstraintValidationUtil.isProperty;
import static de.darwinspl.fmec.validation.constraints.ConstraintValidationUtil.isTimePoint;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;

import de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock;
import de.darwinspl.fmec.constraints.TimePointConstraint;
import de.darwinspl.fmec.keywords.timepoint.KeywordAfter;
import de.darwinspl.fmec.keywords.timepoint.KeywordAt;
import de.darwinspl.fmec.keywords.timepoint.KeywordBefore;
import de.darwinspl.fmec.keywords.timepoint.KeywordUntil;
import de.darwinspl.fmec.keywords.timepoint.KeywordWhen;
import de.darwinspl.fmec.keywords.timepoint.TimePointKeyword;

public class TimePointConstraintValidation extends AbstractModelConstraint {

	@Override
	public IStatus validate(IValidationContext ctx) {
		EObject target = ctx.getTarget();
		
		if(target instanceof TimePointConstraint) {
			TimePointConstraint constraint = ((TimePointConstraint) target);
			
			EvolutionaryAbstractBuidingBlock preArgument = constraint.getPreArgument();
			TimePointKeyword keyword = constraint.getKeyword();
			EvolutionaryAbstractBuidingBlock postArgument = constraint.getPostArgument();
			
			if(keyword instanceof KeywordBefore) {
				if(!isEvent(preArgument) || !isTimePoint(postArgument)) {
					return ctx.createFailureStatus("before", "<Event> before <Event/ExplicitTimePoint>");
				}
			}
			else if(keyword instanceof KeywordUntil) {
				if(!isProperty(preArgument) || !isTimePoint(postArgument)) {
					return ctx.createFailureStatus("until", "<Property> until <Event/ExplicitTimePoint>");
				}
			}
			else if(keyword instanceof KeywordAfter) {
				if(!(isEvent(preArgument) || isProperty(preArgument)) || !isTimePoint(postArgument)) {
					return ctx.createFailureStatus("after", "<Event/Property> until <Event/ExplicitTimePoint>");
				}
			}
			else if(keyword instanceof KeywordWhen) {
				boolean isCorrect = false;
				if(isEvent(preArgument) && (isEvent(postArgument) || isProperty(postArgument)))
					isCorrect = true;
				if(isProperty(preArgument) && isProperty(postArgument))
					isCorrect = true;
				
				if(!isCorrect) {
					return ctx.createFailureStatus("when", "<Event> when <Event/Property>\" or \"<Property> when <Property>");
				}
			}
			else if(keyword instanceof KeywordAt) {
				if(!(isEvent(preArgument) || isProperty(preArgument)) || !isExplicitTimePoint(postArgument)) {
					return ctx.createFailureStatus("at", "<Event/Property> at <ExplicitTimePoint>");
				}
			}
		}
		
		return ctx.createSuccessStatus();
	}

}
