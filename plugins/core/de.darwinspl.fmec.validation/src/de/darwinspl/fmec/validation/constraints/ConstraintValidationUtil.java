package de.darwinspl.fmec.validation.constraints;

import de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock;
import de.darwinspl.fmec.property.EvolutionaryProperty;
import de.darwinspl.fmec.timepoint.EvolutionaryEvent;
import de.darwinspl.fmec.timepoint.ExplicitTimePoint;
import de.darwinspl.fmec.timepoint.TimePoint;

public class ConstraintValidationUtil {
	
	public static boolean isProperty(EvolutionaryAbstractBuidingBlock block) {
		return block instanceof EvolutionaryProperty;
	}
	
	public static boolean isTimePoint(EvolutionaryAbstractBuidingBlock block) {
		return block instanceof TimePoint;
	}
	
	public static boolean isEvent(EvolutionaryAbstractBuidingBlock block) {
		if(block instanceof TimePoint)
			return ((TimePoint) block).getTimePoint() instanceof EvolutionaryEvent;
		return false;
	}
	
	public static boolean isExplicitTimePoint(EvolutionaryAbstractBuidingBlock block) {
		if(block instanceof TimePoint)
			return ((TimePoint) block).getTimePoint() instanceof ExplicitTimePoint;
		return false;
	}
	
}
