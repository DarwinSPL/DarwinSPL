package de.darwinspl.fmec.validation.constraints;

import static de.darwinspl.fmec.validation.constraints.ConstraintValidationUtil.isEvent;
import static de.darwinspl.fmec.validation.constraints.ConstraintValidationUtil.isProperty;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;

import de.darwinspl.fmec.constraints.EvolutionaryAbstractBuidingBlock;
import de.darwinspl.fmec.constraints.TimeIntervalConstraint;
import de.darwinspl.fmec.keywords.interval.IntervalKeyword;
import de.darwinspl.fmec.keywords.interval.KeywordDuring;

public class TimeIntervalConstraintValidation extends AbstractModelConstraint {

	@Override
	public IStatus validate(IValidationContext ctx) {
		EObject target = ctx.getTarget();
		
		if(target instanceof TimeIntervalConstraint) {
			TimeIntervalConstraint constraint = ((TimeIntervalConstraint) target);
			
			EvolutionaryAbstractBuidingBlock preArgument = constraint.getPreArgument();
			IntervalKeyword keyword = constraint.getKeyword();
			
			if(keyword instanceof KeywordDuring) {
				if(!(isEvent(preArgument) || isProperty(preArgument))) {
					return ctx.createFailureStatus("during", "<Event/Property> during [ <Event/ExplicitTimePoint> , <Event/ExplicitTimePoint> )");
				}
			}
		}
		
		return ctx.createSuccessStatus();
	}

}
