/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.analysis;

import java.util.Map;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

public class Tfm_ctcPATH_TO_FILETokenResolver implements de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver {
	
	private de.darwinspl.constraint.resource.tfm_ctc.analysis.Tfm_ctcDefaultTokenResolver defaultTokenResolver = new de.darwinspl.constraint.resource.tfm_ctc.analysis.Tfm_ctcDefaultTokenResolver(true);
	
	public String deResolve(Object value, EStructuralFeature feature, EObject container) {
		// By default token de-resolving is delegated to the DefaultTokenResolver.
		String result = defaultTokenResolver.deResolve(value, feature, container, null, null, null);
		return result;
	}
	
	public void resolve(String lexem, EStructuralFeature feature, de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolveResult result) {
		// By default token resolving is delegated to the DefaultTokenResolver.
		defaultTokenResolver.resolve(lexem, feature, result, null, null, null);
	}
	
	public void setOptions(Map<?,?> options) {
		defaultTokenResolver.setOptions(options);
	}
	
}
