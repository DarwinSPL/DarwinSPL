/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.analysis;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

public class DwFeatureReferenceExpressionFeatureReferenceResolver implements de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceResolver<de.darwinspl.expression.DwFeatureReferenceExpression, de.darwinspl.feature.DwFeature> {
	
	private de.darwinspl.expression.resource.dwexpression.analysis.DwFeatureReferenceExpressionFeatureReferenceResolver delegate = new de.darwinspl.expression.resource.dwexpression.analysis.DwFeatureReferenceExpressionFeatureReferenceResolver();
	
	public void resolve(String identifier, de.darwinspl.expression.DwFeatureReferenceExpression container, EReference reference, int position, boolean resolveFuzzy, final de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceResolveResult<de.darwinspl.feature.DwFeature> result) {
		delegate.resolve(identifier, container, reference, position, resolveFuzzy, new de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceResolveResult<de.darwinspl.feature.DwFeature>() {
			
			public boolean wasResolvedUniquely() {
				return result.wasResolvedUniquely();
			}
			
			public boolean wasResolvedMultiple() {
				return result.wasResolvedMultiple();
			}
			
			public boolean wasResolved() {
				return result.wasResolved();
			}
			
			public void setErrorMessage(String message) {
				result.setErrorMessage(message);
			}
			
			public Collection<de.darwinspl.expression.resource.dwexpression.IDwexpressionReferenceMapping<de.darwinspl.feature.DwFeature>> getMappings() {
				throw new UnsupportedOperationException();
			}
			
			public String getErrorMessage() {
				return result.getErrorMessage();
			}
			
			public void addMapping(String identifier, URI newIdentifier) {
				result.addMapping(identifier, newIdentifier);
			}
			
			public void addMapping(String identifier, URI newIdentifier, String warning) {
				result.addMapping(identifier, newIdentifier, warning);
			}
			
			public void addMapping(String identifier, de.darwinspl.feature.DwFeature target) {
				result.addMapping(identifier, target);
			}
			
			public void addMapping(String identifier, de.darwinspl.feature.DwFeature target, String warning) {
				result.addMapping(identifier, target, warning);
			}
			
			public Collection<de.darwinspl.expression.resource.dwexpression.IDwexpressionQuickFix> getQuickFixes() {
				return Collections.emptySet();
			}
			
			public void addQuickFix(final de.darwinspl.expression.resource.dwexpression.IDwexpressionQuickFix quickFix) {
				result.addQuickFix(new de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcQuickFix() {
					
					public String getImageKey() {
						return quickFix.getImageKey();
					}
					
					public String getDisplayString() {
						return quickFix.getDisplayString();
					}
					
					public Collection<EObject> getContextObjects() {
						return quickFix.getContextObjects();
					}
					
					public String getContextAsString() {
						return quickFix.getContextAsString();
					}
					
					public String apply(String currentText) {
						return quickFix.apply(currentText);
					}
				});
			}
		});
		
	}
	
	public String deResolve(de.darwinspl.feature.DwFeature element, de.darwinspl.expression.DwFeatureReferenceExpression container, EReference reference) {
		return delegate.deResolve(element, container, reference);
	}
	
	public void setOptions(Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
