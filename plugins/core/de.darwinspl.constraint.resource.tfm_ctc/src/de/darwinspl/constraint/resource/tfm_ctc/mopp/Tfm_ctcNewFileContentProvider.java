/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.mopp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

public class Tfm_ctcNewFileContentProvider {
	
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcMetaInformation getMetaInformation() {
		return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcMetaInformation();
	}
	
	public String getNewFileContent(String newFileName) {
		return "feature model <" + newFileName + ".tfm>";
//		return getExampleContent(new EClass[] {
//			de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(),
//		}, getMetaInformation().getClassesWithSyntax(), newFileName);
	}
	
	protected String getExampleContent(EClass[] startClasses, EClass[] allClassesWithSyntax, String newFileName) {
		String content = "";
		for (EClass next : startClasses) {
			content = getExampleContent(next, allClassesWithSyntax, newFileName);
			if (content.trim().length() > 0) {
				break;
			}
		}
		return content;
	}
	
	protected String getExampleContent(EClass eClass, EClass[] allClassesWithSyntax, String newFileName) {
		// create a minimal model
		EObject root = new de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcMinimalModelHelper().getMinimalModel(eClass, allClassesWithSyntax, newFileName);
		if (root == null) {
			// could not create a minimal model. returning an empty document is the best we
			// can do.
			return "";
		}
		// use printer to get text for model
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextPrinter printer = getPrinter(buffer);
		try {
			printer.print(root);
		} catch (IOException e) {
			new de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcRuntimeUtil().logError("Exception while generating example content.", e);
		}
		return buffer.toString();
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextPrinter getPrinter(OutputStream outputStream) {
		return getMetaInformation().createPrinter(outputStream, new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcResource());
	}
	
}
