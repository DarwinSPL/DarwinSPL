/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.analysis;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

import de.darwinspl.common.ecore.util.DwEcoreUtil;
import de.darwinspl.feature.DwTemporalFeatureModel;

public class DwConstraintModelFeatureModelReferenceResolver implements de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceResolver<de.darwinspl.constraint.DwConstraintModel, de.darwinspl.feature.DwTemporalFeatureModel> {
	
	public void resolve(String identifier, de.darwinspl.constraint.DwConstraintModel container, EReference reference, int position, boolean resolveFuzzy, final de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceResolveResult<de.darwinspl.feature.DwTemporalFeatureModel> result) {
		try {
			EObject root = DwEcoreUtil.resolveFeatureModelReference(identifier, container);
			
			if(root instanceof DwTemporalFeatureModel)
				result.addMapping(identifier, (DwTemporalFeatureModel) root);
		} catch(Exception e) {
			result.setErrorMessage(e.getMessage());
		}
	}
	
	public String deResolve(de.darwinspl.feature.DwTemporalFeatureModel element, de.darwinspl.constraint.DwConstraintModel container, EReference reference) {
		return DwEcoreUtil.deresolveFeatureModelReference(element, container);
	}
	
	public void setOptions(Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
