/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.grammar;


public class Tfm_ctcWhiteSpace extends de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcFormattingElement {
	
	private final int amount;
	
	public Tfm_ctcWhiteSpace(int amount, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality cardinality) {
		super(cardinality);
		this.amount = amount;
	}
	
	public int getAmount() {
		return amount;
	}
	
	public String toString() {
		return "#" + getAmount();
	}
	
}
