/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.mopp;

import java.util.ArrayList;
import java.util.Collection;
import org.eclipse.emf.ecore.EObject;

public class Tfm_ctcParseResult implements de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcParseResult {
	
	private EObject root;
	
	private de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcLocationMap locationMap;
	
	private Collection<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcCommand<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource>> commands = new ArrayList<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcCommand<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource>>();
	
	public Tfm_ctcParseResult() {
		super();
	}
	
	public EObject getRoot() {
		return root;
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcLocationMap getLocationMap() {
		return locationMap;
	}
	
	public void setRoot(EObject root) {
		this.root = root;
	}
	
	public void setLocationMap(de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcLocationMap locationMap) {
		this.locationMap = locationMap;
	}
	
	public Collection<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcCommand<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource>> getPostParseCommands() {
		return commands;
	}
	
}
