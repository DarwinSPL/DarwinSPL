/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.grammar;


public enum Tfm_ctcCardinality {
	
	ONE, PLUS, QUESTIONMARK, STAR;
	
}
