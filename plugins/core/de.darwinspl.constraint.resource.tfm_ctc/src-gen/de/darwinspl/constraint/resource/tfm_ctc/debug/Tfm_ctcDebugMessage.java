/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.debug;


/**
 * DebugMessages are exchanged between the debug server (the Eclipse debug
 * framework) and the debug client (a running process or interpreter). To exchange
 * messages they are serialized and sent over sockets.
 */
public class Tfm_ctcDebugMessage {
	// The generator for this class is currently disabled by option
	// 'disableDebugSupport' in the .cs file.
}
