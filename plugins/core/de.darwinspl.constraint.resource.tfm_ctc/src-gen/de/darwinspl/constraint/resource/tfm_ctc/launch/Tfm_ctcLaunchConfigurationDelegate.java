/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.launch;


/**
 * A class that handles launch configurations.
 */
public class Tfm_ctcLaunchConfigurationDelegate {
	// The generator for this class is currently disabled by option
	// 'disableLaunchSupport' in the .cs file.
}
