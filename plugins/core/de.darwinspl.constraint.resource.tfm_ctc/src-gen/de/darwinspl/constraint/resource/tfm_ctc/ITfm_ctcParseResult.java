/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc;

import java.util.Collection;
import org.eclipse.emf.ecore.EObject;

/**
 * An interface used to access the result of parsing a document.
 */
public interface ITfm_ctcParseResult {
	
	/**
	 * Returns the root object of the document.
	 */
	public EObject getRoot();
	
	/**
	 * Returns a list of commands that must be executed after parsing the document.
	 */
	public Collection<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcCommand<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource>> getPostParseCommands();
	
	/**
	 * Returns a map that can be used to retrieve the position of objects in the
	 * parsed text.
	 */
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcLocationMap getLocationMap();
	
}
