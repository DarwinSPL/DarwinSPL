/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.mopp;

import java.util.ArrayList;
import java.util.Collection;

/**
 * This class provides information about how brackets must be handled in the
 * editor (e.g., whether they must be closed automatically).
 */
public class Tfm_ctcBracketInformationProvider {
	
	public Collection<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcBracketPair> getBracketPairs() {
		Collection<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcBracketPair> result = new ArrayList<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcBracketPair>();
		result.add(new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcBracketPair("[", "]", true, false));
		result.add(new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcBracketPair("(", ")", true, false));
		result.add(new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcBracketPair("\"", "\"", false, false));
		return result;
	}
	
}
