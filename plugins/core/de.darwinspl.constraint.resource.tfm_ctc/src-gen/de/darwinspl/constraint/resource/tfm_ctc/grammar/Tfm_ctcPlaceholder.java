/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.grammar;

import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * A class to represent placeholders in a grammar.
 */
public class Tfm_ctcPlaceholder extends de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcTerminal {
	
	private final String tokenName;
	
	public Tfm_ctcPlaceholder(EStructuralFeature feature, String tokenName, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality cardinality, int mandatoryOccurencesAfter) {
		super(feature, cardinality, mandatoryOccurencesAfter);
		this.tokenName = tokenName;
	}
	
	public String getTokenName() {
		return tokenName;
	}
	
}
