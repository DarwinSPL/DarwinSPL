/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.grammar;


/**
 * A class to represent a keyword in the grammar.
 */
public class Tfm_ctcKeyword extends de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSyntaxElement {
	
	private final String value;
	
	public Tfm_ctcKeyword(String value, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality cardinality) {
		super(cardinality, null);
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	public String toString() {
		return "\"" + value + "\"";
	}
	
}
