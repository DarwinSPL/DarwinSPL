/**
 * <copyright>
 * </copyright>
 *
 * 
 */
/**
 * This class can be used to configure options that must be used when resources
 * are loaded. This is similar to using the extension point
 * 'de.darwinspl.constraint.resource.tfm_ctc.default_load_options' with the
 * difference that the options defined in this class are used even if no Eclipse
 * platform is running.
 */
package de.darwinspl.constraint.resource.tfm_ctc.mopp;

import java.util.Collections;
import java.util.Map;

public class Tfm_ctcOptionProvider implements de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcOptionProvider {
	
	public Map<?,?> getOptions() {
		// create a map with static option providers here
		return Collections.emptyMap();
	}
	
}
