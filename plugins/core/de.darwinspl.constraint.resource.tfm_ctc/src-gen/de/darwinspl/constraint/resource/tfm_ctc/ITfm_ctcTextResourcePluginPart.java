/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc;


/**
 * This interface is extended by some other generated classes. It provides access
 * to the plug-in meta information.
 */
public interface ITfm_ctcTextResourcePluginPart {
	
	/**
	 * Returns a meta information object for the language plug-in that contains this
	 * part.
	 */
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcMetaInformation getMetaInformation();
	
}
