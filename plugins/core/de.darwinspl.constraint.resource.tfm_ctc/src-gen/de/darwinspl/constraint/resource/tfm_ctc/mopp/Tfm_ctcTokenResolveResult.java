/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.mopp;


/**
 * A basic implementation of the ITokenResolveResult interface.
 */
public class Tfm_ctcTokenResolveResult implements de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolveResult {
	
	private String errorMessage;
	private Object resolvedToken;
	
	public Tfm_ctcTokenResolveResult() {
		super();
		clear();
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public Object getResolvedToken() {
		return resolvedToken;
	}
	
	public void setErrorMessage(String message) {
		errorMessage = message;
	}
	
	public void setResolvedToken(Object resolvedToken) {
		this.resolvedToken = resolvedToken;
	}
	
	public void clear() {
		errorMessage = "Can't resolve token.";
		resolvedToken = null;
	}
	
}
