/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc;


/**
 * A simple interface for commands that can be executed and that return
 * information about the success of their execution.
 */
public interface ITfm_ctcCommand<ContextType> {
	
	public boolean execute(ContextType context);
}
