/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.mopp;

import java.util.LinkedHashSet;
import java.util.Set;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * A representation for a range in a document where a boolean attribute is
 * expected.
 */
public class Tfm_ctcExpectedBooleanTerminal extends de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcAbstractExpectedElement {
	
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcBooleanTerminal booleanTerminal;
	
	public Tfm_ctcExpectedBooleanTerminal(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcBooleanTerminal booleanTerminal) {
		super(booleanTerminal.getMetaclass());
		this.booleanTerminal = booleanTerminal;
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcBooleanTerminal getBooleanTerminal() {
		return booleanTerminal;
	}
	
	/**
	 * Returns the expected boolean terminal.
	 */
	public de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSyntaxElement getSyntaxElement() {
		return booleanTerminal;
	}
	
	private EStructuralFeature getFeature() {
		return booleanTerminal.getFeature();
	}
	
	public String toString() {
		return "EFeature " + getFeature().getEContainingClass().getName() + "." + getFeature().getName();
	}
	
	public boolean equals(Object o) {
		if (o instanceof Tfm_ctcExpectedBooleanTerminal) {
			return getFeature().equals(((Tfm_ctcExpectedBooleanTerminal) o).getFeature());
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return getFeature().hashCode();
	}
	
	public Set<String> getTokenNames() {
		// BooleanTerminals are associated with two or one token(s)
		Set<String> tokenNames = new LinkedHashSet<String>(2);
		String trueLiteral = booleanTerminal.getTrueLiteral();
		if (!"".equals(trueLiteral)) {
			tokenNames.add("'" + trueLiteral + "'");
		}
		String falseLiteral = booleanTerminal.getFalseLiteral();
		if (!"".equals(falseLiteral)) {
			tokenNames.add("'" + falseLiteral + "'");
		}
		return tokenNames;
	}
	
}
