/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.mopp;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used when computing code completion proposals hold groups of
 * expected elements which belong to the same follow set.
 */
public class Tfm_ctcFollowSetGroupList {
	
	private int lastFollowSetID = -1;
	private List<de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcFollowSetGroup> followSetGroups = new ArrayList<de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcFollowSetGroup>();
	
	public Tfm_ctcFollowSetGroupList(List<de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal> expectedTerminals) {
		for (de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal expectedTerminal : expectedTerminals) {
			addExpectedTerminal(expectedTerminal);
		}
	}
	
	private void addExpectedTerminal(de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal expectedTerminal) {
		de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcFollowSetGroup group;
		
		int followSetID = expectedTerminal.getFollowSetID();
		if (followSetID == lastFollowSetID) {
			if (followSetGroups.isEmpty()) {
				group = addNewGroup();
			} else {
				group = followSetGroups.get(followSetGroups.size() - 1);
			}
		} else {
			group = addNewGroup();
			lastFollowSetID = followSetID;
		}
		
		group.add(expectedTerminal);
	}
	
	public List<de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcFollowSetGroup> getFollowSetGroups() {
		return followSetGroups;
	}
	
	private de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcFollowSetGroup addNewGroup() {
		de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcFollowSetGroup group = new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcFollowSetGroup();
		followSetGroups.add(group);
		return group;
	}
	
}
