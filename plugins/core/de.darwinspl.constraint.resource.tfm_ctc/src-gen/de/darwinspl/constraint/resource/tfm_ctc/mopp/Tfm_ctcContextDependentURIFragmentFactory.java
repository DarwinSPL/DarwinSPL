/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.mopp;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

/**
 * <p>
 * A factory for ContextDependentURIFragments. Given a feasible reference
 * resolver, this factory returns a matching fragment that used the resolver to
 * resolver proxy objects.
 * </p>
 * 
 * @param <ContainerType> the type of the class containing the reference to be
 * resolved
 * @param <ReferenceType> the type of the reference to be resolved
 */
public class Tfm_ctcContextDependentURIFragmentFactory<ContainerType extends EObject, ReferenceType extends EObject>  implements de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcContextDependentURIFragmentFactory<ContainerType, ReferenceType> {
	
	private final de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceResolver<ContainerType, ReferenceType> resolver;
	
	public Tfm_ctcContextDependentURIFragmentFactory(de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceResolver<ContainerType, ReferenceType> resolver) {
		this.resolver = resolver;
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcContextDependentURIFragment<?> create(String identifier, ContainerType container, EReference reference, int positionInReference, EObject proxy) {
		
		return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcContextDependentURIFragment<ContainerType, ReferenceType>(identifier, container, reference, positionInReference, proxy) {
			public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceResolver<ContainerType, ReferenceType> getResolver() {
				return resolver;
			}
		};
	}
}
