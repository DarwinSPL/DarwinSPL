/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.launch;


/**
 * A class that provides common methods that are required by launch configuration
 * delegates.
 */
public class Tfm_ctcLaunchConfigurationHelper {
	// The generator for this class is currently disabled by option
	// 'disableLaunchSupport' in the .cs file.
}
