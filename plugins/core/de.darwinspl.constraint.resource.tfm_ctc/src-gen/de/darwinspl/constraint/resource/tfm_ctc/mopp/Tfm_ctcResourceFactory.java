/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.mopp;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;

public class Tfm_ctcResourceFactory implements Resource.Factory {
	
	public Tfm_ctcResourceFactory() {
		super();
	}
	
	public Resource createResource(URI uri) {
		return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcResource(uri);
	}
	
}
