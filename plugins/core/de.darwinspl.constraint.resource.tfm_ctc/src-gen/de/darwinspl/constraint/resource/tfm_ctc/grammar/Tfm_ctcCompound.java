/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.grammar;


public class Tfm_ctcCompound extends de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSyntaxElement {
	
	public Tfm_ctcCompound(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice choice, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality cardinality) {
		super(cardinality, new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSyntaxElement[] {choice});
	}
	
	public String toString() {
		return "(" + getChildren()[0] + ")";
	}
	
}
