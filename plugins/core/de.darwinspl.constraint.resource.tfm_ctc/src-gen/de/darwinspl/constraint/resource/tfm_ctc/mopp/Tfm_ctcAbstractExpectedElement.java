/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.mopp;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import org.eclipse.emf.ecore.EClass;

/**
 * Abstract super class for all expected elements. Provides methods to add
 * followers.
 */
public abstract class Tfm_ctcAbstractExpectedElement implements de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcExpectedElement {
	
	private EClass ruleMetaclass;
	
	private Set<de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcPair<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcExpectedElement, de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcContainedFeature[]>> followers = new LinkedHashSet<de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcPair<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcExpectedElement, de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcContainedFeature[]>>();
	
	public Tfm_ctcAbstractExpectedElement(EClass ruleMetaclass) {
		super();
		this.ruleMetaclass = ruleMetaclass;
	}
	
	public EClass getRuleMetaclass() {
		return ruleMetaclass;
	}
	
	public void addFollower(de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcExpectedElement follower, de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcContainedFeature[] path) {
		followers.add(new de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcPair<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcExpectedElement, de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcContainedFeature[]>(follower, path));
	}
	
	public Collection<de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcPair<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcExpectedElement, de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcContainedFeature[]>> getFollowers() {
		return followers;
	}
	
}
