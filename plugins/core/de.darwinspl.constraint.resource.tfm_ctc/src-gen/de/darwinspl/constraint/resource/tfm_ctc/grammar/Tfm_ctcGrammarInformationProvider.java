/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.grammar;

import java.lang.reflect.Field;
import java.util.LinkedHashSet;
import java.util.Set;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcoreFactory;

public class Tfm_ctcGrammarInformationProvider {
	
	public final static EStructuralFeature ANONYMOUS_FEATURE = EcoreFactory.eINSTANCE.createEAttribute();
	static {
		ANONYMOUS_FEATURE.setName("_");
	}
	
	public final static Tfm_ctcGrammarInformationProvider INSTANCE = new Tfm_ctcGrammarInformationProvider();
	
	private Set<String> keywords;
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword TFM_CTC_0_0_0_0 = INSTANCE.getTFM_CTC_0_0_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword getTFM_CTC_0_0_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword("feature", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword TFM_CTC_0_0_0_1 = INSTANCE.getTFM_CTC_0_0_0_1();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword getTFM_CTC_0_0_0_1() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword("model", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcPlaceholder TFM_CTC_0_0_0_2 = INSTANCE.getTFM_CTC_0_0_0_2();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcPlaceholder getTFM_CTC_0_0_0_2() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcPlaceholder(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel().getEStructuralFeature(de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT_MODEL__FEATURE_MODEL), "PATH_TO_FILE", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcLineBreak TFM_CTC_0_0_0_3 = INSTANCE.getTFM_CTC_0_0_0_3();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcLineBreak getTFM_CTC_0_0_0_3() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcLineBreak(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcLineBreak TFM_CTC_0_0_0_4 = INSTANCE.getTFM_CTC_0_0_0_4();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcLineBreak getTFM_CTC_0_0_0_4() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcLineBreak(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment TFM_CTC_0_0_0_5_0_0_0 = INSTANCE.getTFM_CTC_0_0_0_5_0_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment getTFM_CTC_0_0_0_5_0_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel().getEStructuralFeature(de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT_MODEL__CONSTRAINTS), de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, new EClass[] {de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraint(), }, 0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcLineBreak TFM_CTC_0_0_0_5_0_0_1 = INSTANCE.getTFM_CTC_0_0_0_5_0_0_1();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcLineBreak getTFM_CTC_0_0_0_5_0_0_1() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcLineBreak(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence TFM_CTC_0_0_0_5_0_0 = INSTANCE.getTFM_CTC_0_0_0_5_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence getTFM_CTC_0_0_0_5_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, TFM_CTC_0_0_0_5_0_0_0, TFM_CTC_0_0_0_5_0_0_1);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice TFM_CTC_0_0_0_5_0 = INSTANCE.getTFM_CTC_0_0_0_5_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice getTFM_CTC_0_0_0_5_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, TFM_CTC_0_0_0_5_0_0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCompound TFM_CTC_0_0_0_5 = INSTANCE.getTFM_CTC_0_0_0_5();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCompound getTFM_CTC_0_0_0_5() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCompound(TFM_CTC_0_0_0_5_0, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.STAR);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence TFM_CTC_0_0_0 = INSTANCE.getTFM_CTC_0_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence getTFM_CTC_0_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, TFM_CTC_0_0_0_0, TFM_CTC_0_0_0_1, TFM_CTC_0_0_0_2, TFM_CTC_0_0_0_3, TFM_CTC_0_0_0_4, TFM_CTC_0_0_0_5);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice TFM_CTC_0_0 = INSTANCE.getTFM_CTC_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice getTFM_CTC_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, TFM_CTC_0_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * DwConstraintModel
	 */
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule TFM_CTC_0 = INSTANCE.getTFM_CTC_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule getTFM_CTC_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(), TFM_CTC_0_0, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment TFM_CTC_1_0_0_0 = INSTANCE.getTFM_CTC_1_0_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment getTFM_CTC_1_0_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraint().getEStructuralFeature(de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT__ROOT_EXPRESSION), de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, new EClass[] {de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwExpression(), }, 0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment TFM_CTC_1_0_0_1_0_0_0 = INSTANCE.getTFM_CTC_1_0_0_1_0_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment getTFM_CTC_1_0_0_1_0_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraint().getEStructuralFeature(de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT__VALIDITY), de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, new EClass[] {de.darwinspl.temporal.DwTemporalModelPackage.eINSTANCE.getDwTemporalInterval(), }, 0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence TFM_CTC_1_0_0_1_0_0 = INSTANCE.getTFM_CTC_1_0_0_1_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence getTFM_CTC_1_0_0_1_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, TFM_CTC_1_0_0_1_0_0_0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice TFM_CTC_1_0_0_1_0 = INSTANCE.getTFM_CTC_1_0_0_1_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice getTFM_CTC_1_0_0_1_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, TFM_CTC_1_0_0_1_0_0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCompound TFM_CTC_1_0_0_1 = INSTANCE.getTFM_CTC_1_0_0_1();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCompound getTFM_CTC_1_0_0_1() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCompound(TFM_CTC_1_0_0_1_0, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.QUESTIONMARK);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence TFM_CTC_1_0_0 = INSTANCE.getTFM_CTC_1_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence getTFM_CTC_1_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, TFM_CTC_1_0_0_0, TFM_CTC_1_0_0_1);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice TFM_CTC_1_0 = INSTANCE.getTFM_CTC_1_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice getTFM_CTC_1_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, TFM_CTC_1_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class DwConstraint
	 */
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule TFM_CTC_1 = INSTANCE.getTFM_CTC_1();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule getTFM_CTC_1() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraint(), TFM_CTC_1_0, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword TFM_CTC_2_0_0_0 = INSTANCE.getTFM_CTC_2_0_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword getTFM_CTC_2_0_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword("[", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcPlaceholder TFM_CTC_2_0_0_1_0_0_0 = INSTANCE.getTFM_CTC_2_0_0_1_0_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcPlaceholder getTFM_CTC_2_0_0_1_0_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcPlaceholder(de.darwinspl.temporal.DwTemporalModelPackage.eINSTANCE.getDwTemporalInterval().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__FROM), "DATE", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword TFM_CTC_2_0_0_1_0_0_1 = INSTANCE.getTFM_CTC_2_0_0_1_0_0_1();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword getTFM_CTC_2_0_0_1_0_0_1() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword("-", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcPlaceholder TFM_CTC_2_0_0_1_0_0_2 = INSTANCE.getTFM_CTC_2_0_0_1_0_0_2();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcPlaceholder getTFM_CTC_2_0_0_1_0_0_2() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcPlaceholder(de.darwinspl.temporal.DwTemporalModelPackage.eINSTANCE.getDwTemporalInterval().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__TO), "DATE", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence TFM_CTC_2_0_0_1_0_0 = INSTANCE.getTFM_CTC_2_0_0_1_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence getTFM_CTC_2_0_0_1_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, TFM_CTC_2_0_0_1_0_0_0, TFM_CTC_2_0_0_1_0_0_1, TFM_CTC_2_0_0_1_0_0_2);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcPlaceholder TFM_CTC_2_0_0_1_0_1_0 = INSTANCE.getTFM_CTC_2_0_0_1_0_1_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcPlaceholder getTFM_CTC_2_0_0_1_0_1_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcPlaceholder(de.darwinspl.temporal.DwTemporalModelPackage.eINSTANCE.getDwTemporalInterval().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__FROM), "DATE", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword TFM_CTC_2_0_0_1_0_1_1 = INSTANCE.getTFM_CTC_2_0_0_1_0_1_1();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword getTFM_CTC_2_0_0_1_0_1_1() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword("-", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword TFM_CTC_2_0_0_1_0_1_2 = INSTANCE.getTFM_CTC_2_0_0_1_0_1_2();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword getTFM_CTC_2_0_0_1_0_1_2() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword("eternity", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence TFM_CTC_2_0_0_1_0_1 = INSTANCE.getTFM_CTC_2_0_0_1_0_1();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence getTFM_CTC_2_0_0_1_0_1() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, TFM_CTC_2_0_0_1_0_1_0, TFM_CTC_2_0_0_1_0_1_1, TFM_CTC_2_0_0_1_0_1_2);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword TFM_CTC_2_0_0_1_0_2_0 = INSTANCE.getTFM_CTC_2_0_0_1_0_2_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword getTFM_CTC_2_0_0_1_0_2_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword("initial state", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword TFM_CTC_2_0_0_1_0_2_1 = INSTANCE.getTFM_CTC_2_0_0_1_0_2_1();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword getTFM_CTC_2_0_0_1_0_2_1() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword("-", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcPlaceholder TFM_CTC_2_0_0_1_0_2_2 = INSTANCE.getTFM_CTC_2_0_0_1_0_2_2();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcPlaceholder getTFM_CTC_2_0_0_1_0_2_2() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcPlaceholder(de.darwinspl.temporal.DwTemporalModelPackage.eINSTANCE.getDwTemporalInterval().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__TO), "DATE", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence TFM_CTC_2_0_0_1_0_2 = INSTANCE.getTFM_CTC_2_0_0_1_0_2();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence getTFM_CTC_2_0_0_1_0_2() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, TFM_CTC_2_0_0_1_0_2_0, TFM_CTC_2_0_0_1_0_2_1, TFM_CTC_2_0_0_1_0_2_2);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword TFM_CTC_2_0_0_1_0_3_0 = INSTANCE.getTFM_CTC_2_0_0_1_0_3_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword getTFM_CTC_2_0_0_1_0_3_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword("initial state", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword TFM_CTC_2_0_0_1_0_3_1 = INSTANCE.getTFM_CTC_2_0_0_1_0_3_1();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword getTFM_CTC_2_0_0_1_0_3_1() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword("-", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword TFM_CTC_2_0_0_1_0_3_2 = INSTANCE.getTFM_CTC_2_0_0_1_0_3_2();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword getTFM_CTC_2_0_0_1_0_3_2() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword("eternity", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence TFM_CTC_2_0_0_1_0_3 = INSTANCE.getTFM_CTC_2_0_0_1_0_3();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence getTFM_CTC_2_0_0_1_0_3() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, TFM_CTC_2_0_0_1_0_3_0, TFM_CTC_2_0_0_1_0_3_1, TFM_CTC_2_0_0_1_0_3_2);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice TFM_CTC_2_0_0_1_0 = INSTANCE.getTFM_CTC_2_0_0_1_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice getTFM_CTC_2_0_0_1_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, TFM_CTC_2_0_0_1_0_0, TFM_CTC_2_0_0_1_0_1, TFM_CTC_2_0_0_1_0_2, TFM_CTC_2_0_0_1_0_3);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCompound TFM_CTC_2_0_0_1 = INSTANCE.getTFM_CTC_2_0_0_1();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCompound getTFM_CTC_2_0_0_1() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCompound(TFM_CTC_2_0_0_1_0, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword TFM_CTC_2_0_0_2 = INSTANCE.getTFM_CTC_2_0_0_2();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword getTFM_CTC_2_0_0_2() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword("]", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence TFM_CTC_2_0_0 = INSTANCE.getTFM_CTC_2_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence getTFM_CTC_2_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, TFM_CTC_2_0_0_0, TFM_CTC_2_0_0_1, TFM_CTC_2_0_0_2);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice TFM_CTC_2_0 = INSTANCE.getTFM_CTC_2_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice getTFM_CTC_2_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, TFM_CTC_2_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * DwTemporalInterval
	 */
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule TFM_CTC_2 = INSTANCE.getTFM_CTC_2();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule getTFM_CTC_2() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule(de.darwinspl.temporal.DwTemporalModelPackage.eINSTANCE.getDwTemporalInterval(), TFM_CTC_2_0, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment DWEXPRESSION_0_0_0_0 = INSTANCE.getDWEXPRESSION_0_0_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment getDWEXPRESSION_0_0_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwEquivalenceExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_EQUIVALENCE_EXPRESSION__OPERAND1), de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, new EClass[] {de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwExpression(), }, 0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword DWEXPRESSION_0_0_0_1 = INSTANCE.getDWEXPRESSION_0_0_0_1();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword getDWEXPRESSION_0_0_0_1() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword("<->", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment DWEXPRESSION_0_0_0_2 = INSTANCE.getDWEXPRESSION_0_0_0_2();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment getDWEXPRESSION_0_0_0_2() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwEquivalenceExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_EQUIVALENCE_EXPRESSION__OPERAND2), de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, new EClass[] {de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwExpression(), }, 0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence DWEXPRESSION_0_0_0 = INSTANCE.getDWEXPRESSION_0_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence getDWEXPRESSION_0_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, DWEXPRESSION_0_0_0_0, DWEXPRESSION_0_0_0_1, DWEXPRESSION_0_0_0_2);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice DWEXPRESSION_0_0 = INSTANCE.getDWEXPRESSION_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice getDWEXPRESSION_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, DWEXPRESSION_0_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * DwEquivalenceExpression
	 */
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule DWEXPRESSION_0 = INSTANCE.getDWEXPRESSION_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule getDWEXPRESSION_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwEquivalenceExpression(), DWEXPRESSION_0_0, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment DWEXPRESSION_1_0_0_0 = INSTANCE.getDWEXPRESSION_1_0_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment getDWEXPRESSION_1_0_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwImpliesExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_IMPLIES_EXPRESSION__OPERAND1), de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, new EClass[] {de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwExpression(), }, 0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword DWEXPRESSION_1_0_0_1 = INSTANCE.getDWEXPRESSION_1_0_0_1();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword getDWEXPRESSION_1_0_0_1() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword("->", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment DWEXPRESSION_1_0_0_2 = INSTANCE.getDWEXPRESSION_1_0_0_2();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment getDWEXPRESSION_1_0_0_2() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwImpliesExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_IMPLIES_EXPRESSION__OPERAND2), de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, new EClass[] {de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwExpression(), }, 0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence DWEXPRESSION_1_0_0 = INSTANCE.getDWEXPRESSION_1_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence getDWEXPRESSION_1_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, DWEXPRESSION_1_0_0_0, DWEXPRESSION_1_0_0_1, DWEXPRESSION_1_0_0_2);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice DWEXPRESSION_1_0 = INSTANCE.getDWEXPRESSION_1_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice getDWEXPRESSION_1_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, DWEXPRESSION_1_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * DwImpliesExpression
	 */
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule DWEXPRESSION_1 = INSTANCE.getDWEXPRESSION_1();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule getDWEXPRESSION_1() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwImpliesExpression(), DWEXPRESSION_1_0, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment DWEXPRESSION_2_0_0_0 = INSTANCE.getDWEXPRESSION_2_0_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment getDWEXPRESSION_2_0_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwOrExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_OR_EXPRESSION__OPERAND1), de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, new EClass[] {de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwExpression(), }, 0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword DWEXPRESSION_2_0_0_1 = INSTANCE.getDWEXPRESSION_2_0_0_1();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword getDWEXPRESSION_2_0_0_1() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword("||", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment DWEXPRESSION_2_0_0_2 = INSTANCE.getDWEXPRESSION_2_0_0_2();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment getDWEXPRESSION_2_0_0_2() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwOrExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_OR_EXPRESSION__OPERAND2), de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, new EClass[] {de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwExpression(), }, 0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence DWEXPRESSION_2_0_0 = INSTANCE.getDWEXPRESSION_2_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence getDWEXPRESSION_2_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, DWEXPRESSION_2_0_0_0, DWEXPRESSION_2_0_0_1, DWEXPRESSION_2_0_0_2);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice DWEXPRESSION_2_0 = INSTANCE.getDWEXPRESSION_2_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice getDWEXPRESSION_2_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, DWEXPRESSION_2_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * DwOrExpression
	 */
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule DWEXPRESSION_2 = INSTANCE.getDWEXPRESSION_2();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule getDWEXPRESSION_2() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwOrExpression(), DWEXPRESSION_2_0, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment DWEXPRESSION_3_0_0_0 = INSTANCE.getDWEXPRESSION_3_0_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment getDWEXPRESSION_3_0_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwAndExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_AND_EXPRESSION__OPERAND1), de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, new EClass[] {de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwExpression(), }, 0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword DWEXPRESSION_3_0_0_1 = INSTANCE.getDWEXPRESSION_3_0_0_1();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword getDWEXPRESSION_3_0_0_1() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword("&&", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment DWEXPRESSION_3_0_0_2 = INSTANCE.getDWEXPRESSION_3_0_0_2();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment getDWEXPRESSION_3_0_0_2() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwAndExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_AND_EXPRESSION__OPERAND2), de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, new EClass[] {de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwExpression(), }, 0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence DWEXPRESSION_3_0_0 = INSTANCE.getDWEXPRESSION_3_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence getDWEXPRESSION_3_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, DWEXPRESSION_3_0_0_0, DWEXPRESSION_3_0_0_1, DWEXPRESSION_3_0_0_2);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice DWEXPRESSION_3_0 = INSTANCE.getDWEXPRESSION_3_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice getDWEXPRESSION_3_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, DWEXPRESSION_3_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * DwAndExpression
	 */
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule DWEXPRESSION_3 = INSTANCE.getDWEXPRESSION_3();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule getDWEXPRESSION_3() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwAndExpression(), DWEXPRESSION_3_0, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword DWEXPRESSION_4_0_0_0 = INSTANCE.getDWEXPRESSION_4_0_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword getDWEXPRESSION_4_0_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword("!", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment DWEXPRESSION_4_0_0_1 = INSTANCE.getDWEXPRESSION_4_0_0_1();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment getDWEXPRESSION_4_0_0_1() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwNotExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_NOT_EXPRESSION__OPERAND), de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, new EClass[] {de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwExpression(), }, 0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence DWEXPRESSION_4_0_0 = INSTANCE.getDWEXPRESSION_4_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence getDWEXPRESSION_4_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, DWEXPRESSION_4_0_0_0, DWEXPRESSION_4_0_0_1);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice DWEXPRESSION_4_0 = INSTANCE.getDWEXPRESSION_4_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice getDWEXPRESSION_4_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, DWEXPRESSION_4_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * DwNotExpression
	 */
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule DWEXPRESSION_4 = INSTANCE.getDWEXPRESSION_4();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule getDWEXPRESSION_4() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwNotExpression(), DWEXPRESSION_4_0, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword DWEXPRESSION_5_0_0_0 = INSTANCE.getDWEXPRESSION_5_0_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword getDWEXPRESSION_5_0_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword("(", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment DWEXPRESSION_5_0_0_1 = INSTANCE.getDWEXPRESSION_5_0_0_1();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment getDWEXPRESSION_5_0_0_1() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainment(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwNestedExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_NESTED_EXPRESSION__OPERAND), de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, new EClass[] {de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwExpression(), }, 0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword DWEXPRESSION_5_0_0_2 = INSTANCE.getDWEXPRESSION_5_0_0_2();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword getDWEXPRESSION_5_0_0_2() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword(")", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence DWEXPRESSION_5_0_0 = INSTANCE.getDWEXPRESSION_5_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence getDWEXPRESSION_5_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, DWEXPRESSION_5_0_0_0, DWEXPRESSION_5_0_0_1, DWEXPRESSION_5_0_0_2);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice DWEXPRESSION_5_0 = INSTANCE.getDWEXPRESSION_5_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice getDWEXPRESSION_5_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, DWEXPRESSION_5_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * DwNestedExpression
	 */
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule DWEXPRESSION_5 = INSTANCE.getDWEXPRESSION_5();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule getDWEXPRESSION_5() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwNestedExpression(), DWEXPRESSION_5_0, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcPlaceholder DWEXPRESSION_6_0_0_0_0_0_0 = INSTANCE.getDWEXPRESSION_6_0_0_0_0_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcPlaceholder getDWEXPRESSION_6_0_0_0_0_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcPlaceholder(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwFeatureReferenceExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), "QUOTED_34_34", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence DWEXPRESSION_6_0_0_0_0_0 = INSTANCE.getDWEXPRESSION_6_0_0_0_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence getDWEXPRESSION_6_0_0_0_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, DWEXPRESSION_6_0_0_0_0_0_0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcPlaceholder DWEXPRESSION_6_0_0_0_0_1_0 = INSTANCE.getDWEXPRESSION_6_0_0_0_0_1_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcPlaceholder getDWEXPRESSION_6_0_0_0_0_1_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcPlaceholder(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwFeatureReferenceExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), "IDENTIFIER_TOKEN", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence DWEXPRESSION_6_0_0_0_0_1 = INSTANCE.getDWEXPRESSION_6_0_0_0_0_1();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence getDWEXPRESSION_6_0_0_0_0_1() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, DWEXPRESSION_6_0_0_0_0_1_0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice DWEXPRESSION_6_0_0_0_0 = INSTANCE.getDWEXPRESSION_6_0_0_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice getDWEXPRESSION_6_0_0_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, DWEXPRESSION_6_0_0_0_0_0, DWEXPRESSION_6_0_0_0_0_1);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCompound DWEXPRESSION_6_0_0_0 = INSTANCE.getDWEXPRESSION_6_0_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCompound getDWEXPRESSION_6_0_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCompound(DWEXPRESSION_6_0_0_0_0, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence DWEXPRESSION_6_0_0 = INSTANCE.getDWEXPRESSION_6_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence getDWEXPRESSION_6_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, DWEXPRESSION_6_0_0_0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice DWEXPRESSION_6_0 = INSTANCE.getDWEXPRESSION_6_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice getDWEXPRESSION_6_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, DWEXPRESSION_6_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * DwFeatureReferenceExpression
	 */
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule DWEXPRESSION_6 = INSTANCE.getDWEXPRESSION_6();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule getDWEXPRESSION_6() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwFeatureReferenceExpression(), DWEXPRESSION_6_0, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcBooleanTerminal DWEXPRESSION_7_0_0_0 = INSTANCE.getDWEXPRESSION_7_0_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcBooleanTerminal getDWEXPRESSION_7_0_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcBooleanTerminal(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwBooleanValueExpression().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_BOOLEAN_VALUE_EXPRESSION__VALUE), "true", "false", de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, 0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence DWEXPRESSION_7_0_0 = INSTANCE.getDWEXPRESSION_7_0_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence getDWEXPRESSION_7_0_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSequence(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, DWEXPRESSION_7_0_0_0);
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice DWEXPRESSION_7_0 = INSTANCE.getDWEXPRESSION_7_0();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice getDWEXPRESSION_7_0() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE, DWEXPRESSION_7_0_0);
	}
	
	/**
	 * This constant refers to the definition of the syntax for meta class
	 * DwBooleanValueExpression
	 */
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule DWEXPRESSION_7 = INSTANCE.getDWEXPRESSION_7();
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule getDWEXPRESSION_7() {
		return new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwBooleanValueExpression(), DWEXPRESSION_7_0, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality.ONE);
	}
	
	
	public static String getSyntaxElementID(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSyntaxElement syntaxElement) {
		if (syntaxElement == null) {
			// null indicates EOF
			return "<EOF>";
		}
		for (Field field : de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.class.getFields()) {
			Object fieldValue;
			try {
				fieldValue = field.get(null);
				if (fieldValue == syntaxElement) {
					String id = field.getName();
					return id;
				}
			} catch (Exception e) { }
		}
		return null;
	}
	
	public static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSyntaxElement getSyntaxElementByID(String syntaxElementID) {
		try {
			return (de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSyntaxElement) de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.class.getField(syntaxElementID).get(null);
		} catch (Exception e) {
			return null;
		}
	}
	
	public final static de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule[] RULES = new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule[] {
		TFM_CTC_0,
		TFM_CTC_1,
		TFM_CTC_2,
		DWEXPRESSION_0,
		DWEXPRESSION_1,
		DWEXPRESSION_2,
		DWEXPRESSION_3,
		DWEXPRESSION_4,
		DWEXPRESSION_5,
		DWEXPRESSION_6,
		DWEXPRESSION_7,
	};
	
	/**
	 * Returns all keywords of the grammar. This includes all literals for boolean and
	 * enumeration terminals.
	 */
	public Set<String> getKeywords() {
		if (this.keywords == null) {
			this.keywords = new LinkedHashSet<String>();
			for (de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule rule : RULES) {
				findKeywords(rule, this.keywords);
			}
		}
		return keywords;
	}
	
	/**
	 * Finds all keywords in the given element and its children and adds them to the
	 * set. This includes all literals for boolean and enumeration terminals.
	 */
	private void findKeywords(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSyntaxElement element, Set<String> keywords) {
		if (element instanceof de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword) {
			keywords.add(((de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword) element).getValue());
		} else if (element instanceof de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcBooleanTerminal) {
			keywords.add(((de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcBooleanTerminal) element).getTrueLiteral());
			keywords.add(((de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcBooleanTerminal) element).getFalseLiteral());
		} else if (element instanceof de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcEnumerationTerminal) {
			de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcEnumerationTerminal terminal = (de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcEnumerationTerminal) element;
			for (String key : terminal.getLiteralMapping().keySet()) {
				keywords.add(key);
			}
		}
		for (de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSyntaxElement child : element.getChildren()) {
			findKeywords(child, this.keywords);
		}
	}
	
}
