/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.mopp;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.resource.Resource.Factory;

public class Tfm_ctcMetaInformation implements de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcMetaInformation {
	
	public String getSyntaxName() {
		return "tfm_ctc";
	}
	
	public String getURI() {
		return "http://www.darwinspl.de/constraint/2.0";
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextScanner createLexer() {
		return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcAntlrScanner(new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcLexer());
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextParser createParser(InputStream inputStream, String encoding) {
		return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcParser().createInstance(inputStream, encoding);
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextPrinter createPrinter(OutputStream outputStream, de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource resource) {
		return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcPrinter(outputStream, resource);
	}
	
	public EClass[] getClassesWithSyntax() {
		return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcSyntaxCoverageInformationProvider().getClassesWithSyntax();
	}
	
	public EClass[] getStartSymbols() {
		return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcSyntaxCoverageInformationProvider().getStartSymbols();
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceResolverSwitch getReferenceResolverSwitch() {
		return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcReferenceResolverSwitch();
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolverFactory getTokenResolverFactory() {
		return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenResolverFactory();
	}
	
	public String getPathToCSDefinition() {
		return "de.darwinspl.constraint/model/Constraint.cs";
	}
	
	public String[] getTokenNames() {
		return de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcParser.tokenNames;
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenStyle getDefaultTokenStyle(String tokenName) {
		return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyleInformationProvider().getDefaultTokenStyle(tokenName);
	}
	
	public Collection<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcBracketPair> getBracketPairs() {
		return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcBracketInformationProvider().getBracketPairs();
	}
	
	public EClass[] getFoldableClasses() {
		return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcFoldingInformationProvider().getFoldableClasses();
	}
	
	public Factory createResourceFactory() {
		return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcResourceFactory();
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcNewFileContentProvider getNewFileContentProvider() {
		return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcNewFileContentProvider();
	}
	
	public void registerResourceFactory() {
		// if no resource factory registered, register delegator
		if (Factory.Registry.INSTANCE.getExtensionToFactoryMap().get(getSyntaxName()) == null) {
			Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(getSyntaxName(), new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcResourceFactoryDelegator());
		}
	}
	
	/**
	 * Returns the key of the option that can be used to register a preprocessor that
	 * is used as a pipe when loading resources. This key is language-specific. To
	 * register one preprocessor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getInputStreamPreprocessorProviderOptionKey() {
		return getSyntaxName() + "_" + "INPUT_STREAM_PREPROCESSOR_PROVIDER";
	}
	
	/**
	 * Returns the key of the option that can be used to register a post-processors
	 * that are invoked after loading resources. This key is language-specific. To
	 * register one post-processor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getResourcePostProcessorProviderOptionKey() {
		return getSyntaxName() + "_" + "RESOURCE_POSTPROCESSOR_PROVIDER";
	}
	
	public String getLaunchConfigurationType() {
		return "de.darwinspl.constraint.resource.tfm_ctc.ui.launchConfigurationType";
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcNameProvider createNameProvider() {
		return new de.darwinspl.constraint.resource.tfm_ctc.analysis.Tfm_ctcDefaultNameProvider();
	}
	
	public String[] getSyntaxHighlightableTokenNames() {
		de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcAntlrTokenHelper tokenHelper = new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcAntlrTokenHelper();
		List<String> highlightableTokens = new ArrayList<String>();
		String[] parserTokenNames = getTokenNames();
		for (int i = 0; i < parserTokenNames.length; i++) {
			// If ANTLR is used we need to normalize the token names
			if (!tokenHelper.canBeUsedForSyntaxHighlighting(i)) {
				continue;
			}
			String tokenName = tokenHelper.getTokenName(parserTokenNames, i);
			if (tokenName == null) {
				continue;
			}
			highlightableTokens.add(tokenName);
		}
		highlightableTokens.add(de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyleInformationProvider.TASK_ITEM_TOKEN_NAME);
		return highlightableTokens.toArray(new String[highlightableTokens.size()]);
	}
	
}
