/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.util;

import de.darwinspl.constraint.DwConstraint;
import de.darwinspl.constraint.DwConstraintModel;
import de.darwinspl.expression.DwAndExpression;
import de.darwinspl.expression.DwAtomicExpression;
import de.darwinspl.expression.DwBinaryExpression;
import de.darwinspl.expression.DwBooleanValueExpression;
import de.darwinspl.expression.DwEquivalenceExpression;
import de.darwinspl.expression.DwExpression;
import de.darwinspl.expression.DwFeatureReferenceExpression;
import de.darwinspl.expression.DwImpliesExpression;
import de.darwinspl.expression.DwNestedExpression;
import de.darwinspl.expression.DwNotExpression;
import de.darwinspl.expression.DwOrExpression;
import de.darwinspl.expression.DwSetExpression;
import de.darwinspl.expression.DwUnaryExpression;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EmptyStackException;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;
import org.eclipse.emf.ecore.EObject;

/**
 * This class provides basic infrastructure to interpret models. To implement
 * concrete interpreters, subclass this abstract interpreter and override the
 * interprete_* methods. The interpretation can be customized by binding the two
 * type parameters (ResultType, ContextType). The former is returned by all
 * interprete_* methods, while the latter is passed from method to method while
 * traversing the model. The concrete traversal strategy can also be exchanged.
 * One can use a static traversal strategy by pushing all objects to interpret on
 * the interpretation stack (using addObjectToInterprete()) before calling
 * interprete(). Alternatively, the traversal strategy can be dynamic by pushing
 * objects on the interpretation stack during interpretation.
 */
public class AbstractTfm_ctcInterpreter<ResultType, ContextType> {
	
	private Stack<EObject> interpretationStack = new Stack<EObject>();
	private List<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcInterpreterListener> listeners = new ArrayList<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcInterpreterListener>();
	private EObject nextObjectToInterprete;
	private ContextType currentContext;
	
	public ResultType interprete(ContextType context) {
		ResultType result = null;
		EObject next = null;
		currentContext = context;
		while (!interpretationStack.empty()) {
			try {
				next = interpretationStack.pop();
			} catch (EmptyStackException ese) {
				// this can happen when the interpreter was terminated between the call to empty()
				// and pop()
				break;
			}
			nextObjectToInterprete = next;
			notifyListeners(next);
			result = interprete(next, context);
			if (!continueInterpretation(context, result)) {
				break;
			}
		}
		currentContext = null;
		return result;
	}
	
	/**
	 * Override this method to stop the overall interpretation depending on the result
	 * of the interpretation of a single model elements.
	 */
	public boolean continueInterpretation(ContextType context, ResultType result) {
		return true;
	}
	
	public ResultType interprete(EObject object, ContextType context) {
		ResultType result = null;
		if (object instanceof de.darwinspl.constraint.DwConstraintModel) {
			result = interprete_de_darwinspl_constraint_DwConstraintModel((de.darwinspl.constraint.DwConstraintModel) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.constraint.DwConstraint) {
			result = interprete_de_darwinspl_constraint_DwConstraint((de.darwinspl.constraint.DwConstraint) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.expression.DwFeatureReferenceExpression) {
			result = interprete_de_darwinspl_expression_DwFeatureReferenceExpression((de.darwinspl.expression.DwFeatureReferenceExpression) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.expression.DwBooleanValueExpression) {
			result = interprete_de_darwinspl_expression_DwBooleanValueExpression((de.darwinspl.expression.DwBooleanValueExpression) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.expression.DwAtomicExpression) {
			result = interprete_de_darwinspl_expression_DwAtomicExpression((de.darwinspl.expression.DwAtomicExpression) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.expression.DwNestedExpression) {
			result = interprete_de_darwinspl_expression_DwNestedExpression((de.darwinspl.expression.DwNestedExpression) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.expression.DwNotExpression) {
			result = interprete_de_darwinspl_expression_DwNotExpression((de.darwinspl.expression.DwNotExpression) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.expression.DwUnaryExpression) {
			result = interprete_de_darwinspl_expression_DwUnaryExpression((de.darwinspl.expression.DwUnaryExpression) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.expression.DwAndExpression) {
			result = interprete_de_darwinspl_expression_DwAndExpression((de.darwinspl.expression.DwAndExpression) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.expression.DwOrExpression) {
			result = interprete_de_darwinspl_expression_DwOrExpression((de.darwinspl.expression.DwOrExpression) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.expression.DwImpliesExpression) {
			result = interprete_de_darwinspl_expression_DwImpliesExpression((de.darwinspl.expression.DwImpliesExpression) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.expression.DwEquivalenceExpression) {
			result = interprete_de_darwinspl_expression_DwEquivalenceExpression((de.darwinspl.expression.DwEquivalenceExpression) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.expression.DwBinaryExpression) {
			result = interprete_de_darwinspl_expression_DwBinaryExpression((de.darwinspl.expression.DwBinaryExpression) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.expression.DwSetExpression) {
			result = interprete_de_darwinspl_expression_DwSetExpression((de.darwinspl.expression.DwSetExpression) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.darwinspl.expression.DwExpression) {
			result = interprete_de_darwinspl_expression_DwExpression((de.darwinspl.expression.DwExpression) object, context);
		}
		if (result != null) {
			return result;
		}
		return result;
	}
	
	public ResultType interprete_de_darwinspl_constraint_DwConstraintModel(DwConstraintModel dwConstraintModel, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_constraint_DwConstraint(DwConstraint dwConstraint, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_expression_DwExpression(DwExpression dwExpression, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_expression_DwAtomicExpression(DwAtomicExpression dwAtomicExpression, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_expression_DwFeatureReferenceExpression(DwFeatureReferenceExpression dwFeatureReferenceExpression, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_expression_DwBooleanValueExpression(DwBooleanValueExpression dwBooleanValueExpression, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_expression_DwUnaryExpression(DwUnaryExpression dwUnaryExpression, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_expression_DwNestedExpression(DwNestedExpression dwNestedExpression, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_expression_DwNotExpression(DwNotExpression dwNotExpression, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_expression_DwBinaryExpression(DwBinaryExpression dwBinaryExpression, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_expression_DwAndExpression(DwAndExpression dwAndExpression, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_expression_DwOrExpression(DwOrExpression dwOrExpression, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_expression_DwImpliesExpression(DwImpliesExpression dwImpliesExpression, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_expression_DwEquivalenceExpression(DwEquivalenceExpression dwEquivalenceExpression, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_darwinspl_expression_DwSetExpression(DwSetExpression dwSetExpression, ContextType context) {
		return null;
	}
	
	private void notifyListeners(EObject element) {
		for (de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcInterpreterListener listener : listeners) {
			listener.handleInterpreteObject(element);
		}
	}
	
	/**
	 * Adds the given object to the interpretation stack. Attention: Objects that are
	 * added first, are interpret last.
	 */
	public void addObjectToInterprete(EObject object) {
		interpretationStack.push(object);
	}
	
	/**
	 * Adds the given collection of objects to the interpretation stack. Attention:
	 * Collections that are added first, are interpret last.
	 */
	public void addObjectsToInterprete(Collection<? extends EObject> objects) {
		for (EObject object : objects) {
			addObjectToInterprete(object);
		}
	}
	
	/**
	 * Adds the given collection of objects in reverse order to the interpretation
	 * stack.
	 */
	public void addObjectsToInterpreteInReverseOrder(Collection<? extends EObject> objects) {
		List<EObject> reverse = new ArrayList<EObject>(objects.size());
		reverse.addAll(objects);
		Collections.reverse(reverse);
		addObjectsToInterprete(reverse);
	}
	
	/**
	 * Adds the given object and all its children to the interpretation stack such
	 * that they are interpret in top down order.
	 */
	public void addObjectTreeToInterpreteTopDown(EObject root) {
		List<EObject> objects = new ArrayList<EObject>();
		objects.add(root);
		Iterator<EObject> it = root.eAllContents();
		while (it.hasNext()) {
			EObject eObject = (EObject) it.next();
			objects.add(eObject);
		}
		addObjectsToInterpreteInReverseOrder(objects);
	}
	
	public void addListener(de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcInterpreterListener newListener) {
		listeners.add(newListener);
	}
	
	public boolean removeListener(de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcInterpreterListener listener) {
		return listeners.remove(listener);
	}
	
	public EObject getNextObjectToInterprete() {
		return nextObjectToInterprete;
	}
	
	public Stack<EObject> getInterpretationStack() {
		return interpretationStack;
	}
	
	public void terminate() {
		interpretationStack.clear();
	}
	
	public ContextType getCurrentContext() {
		return currentContext;
	}
	
}
