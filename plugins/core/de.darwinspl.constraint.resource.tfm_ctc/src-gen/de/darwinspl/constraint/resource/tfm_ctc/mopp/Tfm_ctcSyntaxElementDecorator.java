/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.mopp;

import java.util.ArrayList;
import java.util.List;

public class Tfm_ctcSyntaxElementDecorator {
	
	/**
	 * the syntax element to be decorated
	 */
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSyntaxElement decoratedElement;
	
	/**
	 * an array of child decorators (one decorator per child of the decorated syntax
	 * element
	 */
	private Tfm_ctcSyntaxElementDecorator[] childDecorators;
	
	/**
	 * a list of the indices that must be printed
	 */
	private List<Integer> indicesToPrint = new ArrayList<Integer>();
	
	public Tfm_ctcSyntaxElementDecorator(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSyntaxElement decoratedElement, Tfm_ctcSyntaxElementDecorator[] childDecorators) {
		super();
		this.decoratedElement = decoratedElement;
		this.childDecorators = childDecorators;
	}
	
	public void addIndexToPrint(Integer index) {
		indicesToPrint.add(index);
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSyntaxElement getDecoratedElement() {
		return decoratedElement;
	}
	
	public Tfm_ctcSyntaxElementDecorator[] getChildDecorators() {
		return childDecorators;
	}
	
	public Integer getNextIndexToPrint() {
		if (indicesToPrint.size() == 0) {
			return null;
		}
		return indicesToPrint.remove(0);
	}
	
	public String toString() {
		return "" + getDecoratedElement();
	}
	
}
