/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.mopp;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * The Tfm_ctcTokenResolverFactory class provides access to all generated token
 * resolvers. By giving the name of a defined token, the corresponding resolve can
 * be obtained. Despite the fact that this class is called TokenResolverFactory is
 * does NOT create new token resolvers whenever a client calls methods to obtain a
 * resolver. Rather, this class maintains a map of all resolvers and creates each
 * resolver at most once.
 */
public class Tfm_ctcTokenResolverFactory implements de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolverFactory {
	
	private Map<String, de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver> tokenName2TokenResolver;
	private Map<String, de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver> featureName2CollectInTokenResolver;
	private static de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver defaultResolver = new de.darwinspl.constraint.resource.tfm_ctc.analysis.Tfm_ctcDefaultTokenResolver();
	
	public Tfm_ctcTokenResolverFactory() {
		tokenName2TokenResolver = new LinkedHashMap<String, de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver>();
		featureName2CollectInTokenResolver = new LinkedHashMap<String, de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver>();
		registerTokenResolver("PATH_TO_FILE", new de.darwinspl.constraint.resource.tfm_ctc.analysis.Tfm_ctcPATH_TO_FILETokenResolver());
		registerTokenResolver("DATE", new de.darwinspl.constraint.resource.tfm_ctc.analysis.DwexpressionDATETokenResolver());
		registerTokenResolver("IDENTIFIER_TOKEN", new de.darwinspl.constraint.resource.tfm_ctc.analysis.DwexpressionIDENTIFIER_TOKENTokenResolver());
		registerTokenResolver("QUOTED_34_34", new de.darwinspl.constraint.resource.tfm_ctc.analysis.DwexpressionQUOTED_34_34TokenResolver());
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver createTokenResolver(String tokenName) {
		return internalCreateResolver(tokenName2TokenResolver, tokenName);
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver createCollectInTokenResolver(String featureName) {
		return internalCreateResolver(featureName2CollectInTokenResolver, featureName);
	}
	
	protected boolean registerTokenResolver(String tokenName, de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver resolver){
		return internalRegisterTokenResolver(tokenName2TokenResolver, tokenName, resolver);
	}
	
	protected boolean registerCollectInTokenResolver(String featureName, de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver resolver){
		return internalRegisterTokenResolver(featureName2CollectInTokenResolver, featureName, resolver);
	}
	
	protected de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver deRegisterTokenResolver(String tokenName){
		return tokenName2TokenResolver.remove(tokenName);
	}
	
	private de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver internalCreateResolver(Map<String, de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver> resolverMap, String key) {
		if (resolverMap.containsKey(key)){
			return resolverMap.get(key);
		} else {
			return defaultResolver;
		}
	}
	
	private boolean internalRegisterTokenResolver(Map<String, de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver> resolverMap, String key, de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver resolver) {
		if (!resolverMap.containsKey(key)) {
			resolverMap.put(key,resolver);
			return true;
		}
		return false;
	}
	
}
