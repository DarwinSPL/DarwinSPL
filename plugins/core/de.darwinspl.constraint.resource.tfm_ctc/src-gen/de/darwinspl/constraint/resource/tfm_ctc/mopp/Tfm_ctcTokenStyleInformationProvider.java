/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.mopp;


public class Tfm_ctcTokenStyleInformationProvider {
	
	public static String TASK_ITEM_TOKEN_NAME = "TASK_ITEM";
	
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenStyle getDefaultTokenStyle(String tokenName) {
		if ("PATH_TO_FILE".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x32, 0x59, 0x85}, null, false, true, false, false);
		}
		if ("SL_COMMENT".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x00, 0x80, 0x00}, null, false, false, false, false);
		}
		if ("ML_COMMENT".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x00, 0x80, 0x00}, null, false, false, false, false);
		}
		if ("<->".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x80, 0x00, 0x40}, null, true, false, false, false);
		}
		if ("->".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x80, 0x00, 0x40}, null, true, false, false, false);
		}
		if ("||".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x80, 0x00, 0x40}, null, true, false, false, false);
		}
		if ("&&".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x80, 0x00, 0x40}, null, true, false, false, false);
		}
		if ("!".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x80, 0x00, 0x40}, null, true, false, false, false);
		}
		if ("/".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x80, 0x00, 0x40}, null, true, false, false, false);
		}
		if ("*".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x80, 0x00, 0x40}, null, true, false, false, false);
		}
		if ("\\u0025".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x80, 0x00, 0x40}, null, true, false, false, false);
		}
		if ("+".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x80, 0x00, 0x40}, null, true, false, false, false);
		}
		if ("-".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x80, 0x00, 0x40}, null, true, false, false, false);
		}
		if ("?".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x80, 0x00, 0x40}, null, true, false, false, false);
		}
		if ("<".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x80, 0x00, 0x40}, null, true, false, false, false);
		}
		if ("<=".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x80, 0x00, 0x40}, null, true, false, false, false);
		}
		if ("=".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x80, 0x00, 0x40}, null, true, false, false, false);
		}
		if (">".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x80, 0x00, 0x40}, null, true, false, false, false);
		}
		if (">=".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x80, 0x00, 0x40}, null, true, false, false, false);
		}
		if ("[".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x00, 0x00, 0xCC}, null, false, false, false, false);
		}
		if ("]".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x00, 0x00, 0xCC}, null, false, false, false, false);
		}
		if ("(".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x00, 0x00, 0xCC}, null, false, false, false, false);
		}
		if (")".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x00, 0x00, 0xCC}, null, false, false, false, false);
		}
		if ("true".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("false".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("QUOTED_34_34".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x2A, 0x00, 0xFF}, null, false, false, false, false);
		}
		if ("TASK_ITEM".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x7F, 0x9F, 0xBF}, null, true, false, false, false);
		}
		if ("feature".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("model".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("eternity".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("initial state".equals(tokenName)) {
			return new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		return null;
	}
	
}
