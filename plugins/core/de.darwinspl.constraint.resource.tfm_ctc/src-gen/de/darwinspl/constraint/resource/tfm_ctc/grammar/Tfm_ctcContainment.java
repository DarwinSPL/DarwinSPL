/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.grammar;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;

public class Tfm_ctcContainment extends de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcTerminal {
	
	private final EClass[] allowedTypes;
	
	public Tfm_ctcContainment(EStructuralFeature feature, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality cardinality, EClass[] allowedTypes, int mandatoryOccurencesAfter) {
		super(feature, cardinality, mandatoryOccurencesAfter);
		this.allowedTypes = allowedTypes;
	}
	
	public EClass[] getAllowedTypes() {
		return allowedTypes;
	}
	
	@Override
	public boolean hasContainment(EClass metaclass) {
		for (EClass allowedType : allowedTypes) {
			if (allowedType == metaclass) {
				return true;
			}
		}
		return false;
	}
	
	public String toString() {
		String typeRestrictions = null;
		if (allowedTypes != null && allowedTypes.length > 0) {
			typeRestrictions = de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcStringUtil.explode(allowedTypes, ", ", new de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcFunction1<String, EClass>() {
				public String execute(EClass eClass) {
					return eClass.getName();
				}
			});
		}
		return getFeature().getName() + (typeRestrictions == null ? "" : "[" + typeRestrictions + "]");
	}
	
}
