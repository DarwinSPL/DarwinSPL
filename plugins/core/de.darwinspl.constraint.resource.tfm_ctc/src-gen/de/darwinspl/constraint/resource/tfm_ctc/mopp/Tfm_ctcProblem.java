/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.mopp;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;

public class Tfm_ctcProblem implements de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcProblem {
	
	private String message;
	private de.darwinspl.constraint.resource.tfm_ctc.Tfm_ctcEProblemType type;
	private de.darwinspl.constraint.resource.tfm_ctc.Tfm_ctcEProblemSeverity severity;
	private Collection<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcQuickFix> quickFixes;
	
	public Tfm_ctcProblem(String message, de.darwinspl.constraint.resource.tfm_ctc.Tfm_ctcEProblemType type, de.darwinspl.constraint.resource.tfm_ctc.Tfm_ctcEProblemSeverity severity) {
		this(message, type, severity, Collections.<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcQuickFix>emptySet());
	}
	
	public Tfm_ctcProblem(String message, de.darwinspl.constraint.resource.tfm_ctc.Tfm_ctcEProblemType type, de.darwinspl.constraint.resource.tfm_ctc.Tfm_ctcEProblemSeverity severity, de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcQuickFix quickFix) {
		this(message, type, severity, Collections.singleton(quickFix));
	}
	
	public Tfm_ctcProblem(String message, de.darwinspl.constraint.resource.tfm_ctc.Tfm_ctcEProblemType type, de.darwinspl.constraint.resource.tfm_ctc.Tfm_ctcEProblemSeverity severity, Collection<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcQuickFix> quickFixes) {
		super();
		this.message = message;
		this.type = type;
		this.severity = severity;
		this.quickFixes = new LinkedHashSet<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcQuickFix>();
		this.quickFixes.addAll(quickFixes);
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.Tfm_ctcEProblemType getType() {
		return type;
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.Tfm_ctcEProblemSeverity getSeverity() {
		return severity;
	}
	
	public String getMessage() {
		return message;
	}
	
	public Collection<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcQuickFix> getQuickFixes() {
		return quickFixes;
	}
	
}
