/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.mopp;

import org.eclipse.emf.common.util.URI;

/**
 * <p>
 * A basic implementation of the
 * de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcURIMapping interface that can
 * map identifiers to URIs.
 * </p>
 * 
 * @param <ReferenceType> unused type parameter which is needed to implement
 * de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcURIMapping.
 */
public class Tfm_ctcURIMapping<ReferenceType> implements de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcURIMapping<ReferenceType> {
	
	private URI uri;
	private String identifier;
	private String warning;
	
	public Tfm_ctcURIMapping(String identifier, URI newIdentifier, String warning) {
		super();
		this.uri = newIdentifier;
		this.identifier = identifier;
		this.warning = warning;
	}
	
	public URI getTargetIdentifier() {
		return uri;
	}
	
	public String getIdentifier() {
		return identifier;
	}
	
	public String getWarning() {
		return warning;
	}
	
}
