/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.debug;


/**
 * A DebuggableInterpreter is a facade for interpreters that adds debug support.
 */
public class Tfm_ctcDebuggableInterpreter {
	// The generator for this class is currently disabled by option
	// 'disableDebugSupport' in the .cs file.
}
