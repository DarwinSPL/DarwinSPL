/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.debug;


/**
 * The DebugProxy allows to communicate between the interpreter, which runs in a
 * separate thread or process and the Eclipse Debug framework (i.e., the
 * DebugTarget class).
 */
public class Tfm_ctcDebugProxy {
	// The generator for this class is currently disabled by option
	// 'disableDebugSupport' in the .cs file.
}
