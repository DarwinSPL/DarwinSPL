/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.mopp;

import org.antlr.runtime3_4_0.ANTLRStringStream;
import org.antlr.runtime3_4_0.Lexer;
import org.antlr.runtime3_4_0.Token;

public class Tfm_ctcAntlrScanner implements de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextScanner {
	
	private Lexer antlrLexer;
	
	public Tfm_ctcAntlrScanner(Lexer antlrLexer) {
		this.antlrLexer = antlrLexer;
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextToken getNextToken() {
		if (antlrLexer.getCharStream() == null) {
			return null;
		}
		final Token current = antlrLexer.nextToken();
		if (current == null || current.getType() < 0) {
			return null;
		}
		de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextToken result = new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcANTLRTextToken(current);
		return result;
	}
	
	public void setText(String text) {
		antlrLexer.setCharStream(new ANTLRStringStream(text));
	}
	
}
