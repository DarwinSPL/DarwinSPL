/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc;

import java.util.Collection;
import java.util.Set;
import org.eclipse.emf.ecore.EClass;

/**
 * An element that is expected at a given position in a resource stream.
 */
public interface ITfm_ctcExpectedElement {
	
	/**
	 * Returns the names of all tokens that are expected at the given position.
	 */
	public Set<String> getTokenNames();
	
	/**
	 * Returns the metaclass of the rule that contains the expected element.
	 */
	public EClass getRuleMetaclass();
	
	/**
	 * Returns the syntax element that is expected.
	 */
	public de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSyntaxElement getSyntaxElement();
	
	/**
	 * Adds an element that is a valid follower for this element.
	 */
	public void addFollower(de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcExpectedElement follower, de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcContainedFeature[] path);
	
	/**
	 * Returns all valid followers for this element. Each follower is represented by a
	 * pair of an expected elements and the containment trace that leads from the
	 * current element to the follower.
	 */
	public Collection<de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcPair<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcExpectedElement, de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcContainedFeature[]>> getFollowers();
	
}
