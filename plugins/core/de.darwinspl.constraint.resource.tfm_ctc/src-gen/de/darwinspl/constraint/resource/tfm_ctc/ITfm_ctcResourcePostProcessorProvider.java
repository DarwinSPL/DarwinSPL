/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc;


/**
 * Implementors of this interface can provide a post-processor for text resources.
 */
public interface ITfm_ctcResourcePostProcessorProvider {
	
	/**
	 * Returns the processor that shall be called after text resource are successfully
	 * parsed.
	 */
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcResourcePostProcessor getResourcePostProcessor();
	
}
