/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.mopp;

import java.util.Collection;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;

/**
 * <p>
 * A FuzzyResolveResult is an implementation of the ITfm_ctcReferenceResolveResult
 * interface that delegates all method calls to a given
 * ITfm_ctcReferenceResolveResult with ReferenceType EObject. It is used by
 * reference resolver switches to collect results from different reference
 * resolvers in a type safe manner.
 * </p>
 * 
 * @param <ReferenceType> the type of the reference that is resolved
 */
public class Tfm_ctcFuzzyResolveResult<ReferenceType extends EObject> implements de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceResolveResult<ReferenceType> {
	
	private de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceResolveResult<EObject> delegate;
	
	public Tfm_ctcFuzzyResolveResult(de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceResolveResult<EObject> delegate) {
		this.delegate = delegate;
	}
	
	public String getErrorMessage() {
		return delegate.getErrorMessage();
	}
	
	public Collection<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceMapping<ReferenceType>> getMappings() {
		return null;
	}
	
	public boolean wasResolved() {
		return delegate.wasResolved();
	}
	
	public boolean wasResolvedMultiple() {
		return delegate.wasResolvedMultiple();
	}
	
	public boolean wasResolvedUniquely() {
		return delegate.wasResolvedUniquely();
	}
	
	public void setErrorMessage(String message) {
		delegate.setErrorMessage(message);
	}
	
	public void addMapping(String identifier, ReferenceType target) {
		delegate.addMapping(identifier, (EObject) target);
	}
	
	public void addMapping(String identifier, URI uri) {
		delegate.addMapping(identifier, uri);
	}
	
	public void addMapping(String identifier, ReferenceType target, String warning) {
		delegate.addMapping(identifier, (EObject) target, warning);
	}
	
	public void addMapping(String identifier, URI uri, String warning) {
		delegate.addMapping(identifier, uri, warning);
	}
	
	public Collection<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcQuickFix> getQuickFixes() {
		return delegate.getQuickFixes();
	}
	
	public void addQuickFix(de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcQuickFix quickFix) {
		delegate.addQuickFix(quickFix);
	}
	
}
