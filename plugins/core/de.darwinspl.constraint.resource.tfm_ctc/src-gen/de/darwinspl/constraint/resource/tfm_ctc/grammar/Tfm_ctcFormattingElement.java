/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.grammar;


public abstract class Tfm_ctcFormattingElement extends de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSyntaxElement {
	
	public Tfm_ctcFormattingElement(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality cardinality) {
		super(cardinality, null);
	}
	
}
