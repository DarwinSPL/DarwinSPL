// $ANTLR 3.4

	package de.darwinspl.constraint.resource.tfm_ctc.mopp;
	
	import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.antlr.runtime3_4_0.ANTLRInputStream;
import org.antlr.runtime3_4_0.BitSet;
import org.antlr.runtime3_4_0.CommonToken;
import org.antlr.runtime3_4_0.CommonTokenStream;
import org.antlr.runtime3_4_0.IntStream;
import org.antlr.runtime3_4_0.Lexer;
import org.antlr.runtime3_4_0.RecognitionException;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;


import org.antlr.runtime3_4_0.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class Tfm_ctcParser extends Tfm_ctcANTLRParserBase {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "DATE", "IDENTIFIER_TOKEN", "INTEGER_LITERAL", "LINEBREAK", "ML_COMMENT", "PATH_TO_FILE", "QUOTED_34_34", "SL_COMMENT", "WHITESPACE", "'!'", "'&&'", "'('", "')'", "'-'", "'->'", "'<->'", "'['", "']'", "'eternity'", "'false'", "'feature'", "'initial state'", "'model'", "'true'", "'||'"
    };

    public static final int EOF=-1;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__19=19;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int DATE=4;
    public static final int IDENTIFIER_TOKEN=5;
    public static final int INTEGER_LITERAL=6;
    public static final int LINEBREAK=7;
    public static final int ML_COMMENT=8;
    public static final int PATH_TO_FILE=9;
    public static final int QUOTED_34_34=10;
    public static final int SL_COMMENT=11;
    public static final int WHITESPACE=12;

    // delegates
    public Tfm_ctcANTLRParserBase[] getDelegates() {
        return new Tfm_ctcANTLRParserBase[] {};
    }

    // delegators


    public Tfm_ctcParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public Tfm_ctcParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
        this.state.initializeRuleMemo(32 + 1);
         

    }

    public String[] getTokenNames() { return Tfm_ctcParser.tokenNames; }
    public String getGrammarFileName() { return "Tfm_ctc.g"; }


    	private de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolverFactory tokenResolverFactory = new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenResolverFactory();
    	
    	/**
    	 * the index of the last token that was handled by collectHiddenTokens()
    	 */
    	private int lastPosition;
    	
    	/**
    	 * A flag that indicates whether the parser should remember all expected elements.
    	 * This flag is set to true when using the parse for code completion. Otherwise it
    	 * is set to false.
    	 */
    	private boolean rememberExpectedElements = false;
    	
    	private Object parseToIndexTypeObject;
    	private int lastTokenIndex = 0;
    	
    	/**
    	 * A list of expected elements the were collected while parsing the input stream.
    	 * This list is only filled if <code>rememberExpectedElements</code> is set to
    	 * true.
    	 */
    	private List<de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal> expectedElements = new ArrayList<de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal>();
    	
    	private int mismatchedTokenRecoveryTries = 0;
    	/**
    	 * A helper list to allow a lexer to pass errors to its parser
    	 */
    	protected List<RecognitionException> lexerExceptions = Collections.synchronizedList(new ArrayList<RecognitionException>());
    	
    	/**
    	 * Another helper list to allow a lexer to pass positions of errors to its parser
    	 */
    	protected List<Integer> lexerExceptionPositions = Collections.synchronizedList(new ArrayList<Integer>());
    	
    	/**
    	 * A stack for incomplete objects. This stack is used filled when the parser is
    	 * used for code completion. Whenever the parser starts to read an object it is
    	 * pushed on the stack. Once the element was parser completely it is popped from
    	 * the stack.
    	 */
    	List<EObject> incompleteObjects = new ArrayList<EObject>();
    	
    	private int stopIncludingHiddenTokens;
    	private int stopExcludingHiddenTokens;
    	private int tokenIndexOfLastCompleteElement;
    	
    	private int expectedElementsIndexOfLastCompleteElement;
    	
    	/**
    	 * The offset indicating the cursor position when the parser is used for code
    	 * completion by calling parseToExpectedElements().
    	 */
    	private int cursorOffset;
    	
    	/**
    	 * The offset of the first hidden token of the last expected element. This offset
    	 * is used to discard expected elements, which are not needed for code completion.
    	 */
    	private int lastStartIncludingHidden;
    	
    	private de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcLocationMap locationMap;
    	
    	private de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcSyntaxErrorMessageConverter syntaxErrorMessageConverter = new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcSyntaxErrorMessageConverter(tokenNames);
    	
    	@Override
    	public void reportError(RecognitionException re) {
    		addErrorToResource(syntaxErrorMessageConverter.translateParseError(re));
    	}
    	
    	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
    		postParseCommands.add(new de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcCommand<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource>() {
    			public boolean execute(de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource resource) {
    				if (resource == null) {
    					// the resource can be null if the parser is used for code completion
    					return true;
    				}
    				resource.addProblem(new de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcProblem() {
    					public de.darwinspl.constraint.resource.tfm_ctc.Tfm_ctcEProblemSeverity getSeverity() {
    						return de.darwinspl.constraint.resource.tfm_ctc.Tfm_ctcEProblemSeverity.ERROR;
    					}
    					public de.darwinspl.constraint.resource.tfm_ctc.Tfm_ctcEProblemType getType() {
    						return de.darwinspl.constraint.resource.tfm_ctc.Tfm_ctcEProblemType.SYNTAX_ERROR;
    					}
    					public String getMessage() {
    						return errorMessage;
    					}
    					public Collection<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcQuickFix> getQuickFixes() {
    						return null;
    					}
    				}, column, line, startIndex, stopIndex);
    				return true;
    			}
    		});
    	}
    	
    	protected void addErrorToResource(de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcLocalizedMessage message) {
    		if (message == null) {
    			return;
    		}
    		addErrorToResource(message.getMessage(), message.getColumn(), message.getLine(), message.getCharStart(), message.getCharEnd());
    	}
    	
    	public void addExpectedElement(EClass eClass, int expectationStartIndex, int expectationEndIndex) {
    		for (int expectationIndex = expectationStartIndex; expectationIndex <= expectationEndIndex; expectationIndex++) {
    			addExpectedElement(eClass, de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectationConstants.EXPECTATIONS[expectationIndex]);
    		}
    	}
    	
    	public void addExpectedElement(EClass eClass, int expectationIndex) {
    		addExpectedElement(eClass, de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectationConstants.EXPECTATIONS[expectationIndex]);
    	}
    	
    	public void addExpectedElement(EClass eClass, int[] ids) {
    		if (!this.rememberExpectedElements) {
    			return;
    		}
    		int terminalID = ids[0];
    		int followSetID = ids[1];
    		de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcExpectedElement terminal = de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcFollowSetProvider.TERMINALS[terminalID];
    		de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcContainedFeature[] containmentFeatures = new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcContainedFeature[ids.length - 2];
    		for (int i = 2; i < ids.length; i++) {
    			containmentFeatures[i - 2] = de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcFollowSetProvider.LINKS[ids[i]];
    		}
    		de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainmentTrace containmentTrace = new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainmentTrace(eClass, containmentFeatures);
    		EObject container = getLastIncompleteElement();
    		de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal expectedElement = new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal(container, terminal, followSetID, containmentTrace);
    		setPosition(expectedElement, input.index());
    		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
    		lastStartIncludingHidden = startIncludingHiddenTokens;
    		this.expectedElements.add(expectedElement);
    	}
    	
    	protected void collectHiddenTokens(EObject element) {
    	}
    	
    	protected void copyLocalizationInfos(final EObject source, final EObject target) {
    		if (disableLocationMap) {
    			return;
    		}
    		final de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcLocationMap locationMap = this.locationMap;
    		if (locationMap == null) {
    			// the locationMap can be null if the parser is used for code completion
    			return;
    		}
    		postParseCommands.add(new de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcCommand<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource>() {
    			public boolean execute(de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource resource) {
    				locationMap.setCharStart(target, locationMap.getCharStart(source));
    				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
    				locationMap.setColumn(target, locationMap.getColumn(source));
    				locationMap.setLine(target, locationMap.getLine(source));
    				return true;
    			}
    		});
    	}
    	
    	protected void copyLocalizationInfos(final CommonToken source, final EObject target) {
    		if (disableLocationMap) {
    			return;
    		}
    		final de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcLocationMap locationMap = this.locationMap;
    		if (locationMap == null) {
    			// the locationMap can be null if the parser is used for code completion
    			return;
    		}
    		postParseCommands.add(new de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcCommand<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource>() {
    			public boolean execute(de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource resource) {
    				if (source == null) {
    					return true;
    				}
    				locationMap.setCharStart(target, source.getStartIndex());
    				locationMap.setCharEnd(target, source.getStopIndex());
    				locationMap.setColumn(target, source.getCharPositionInLine());
    				locationMap.setLine(target, source.getLine());
    				return true;
    			}
    		});
    	}
    	
    	/**
    	 * Sets the end character index and the last line for the given object in the
    	 * location map.
    	 */
    	protected void setLocalizationEnd(Collection<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcCommand<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource>> postParseCommands , final EObject object, final int endChar, final int endLine) {
    		if (disableLocationMap) {
    			return;
    		}
    		final de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcLocationMap locationMap = this.locationMap;
    		if (locationMap == null) {
    			// the locationMap can be null if the parser is used for code completion
    			return;
    		}
    		postParseCommands.add(new de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcCommand<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource>() {
    			public boolean execute(de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource resource) {
    				locationMap.setCharEnd(object, endChar);
    				locationMap.setLine(object, endLine);
    				return true;
    			}
    		});
    	}
    	
    	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextParser createInstance(InputStream actualInputStream, String encoding) {
    		try {
    			if (encoding == null) {
    				return new Tfm_ctcParser(new CommonTokenStream(new Tfm_ctcLexer(new ANTLRInputStream(actualInputStream))));
    			} else {
    				return new Tfm_ctcParser(new CommonTokenStream(new Tfm_ctcLexer(new ANTLRInputStream(actualInputStream, encoding))));
    			}
    		} catch (IOException e) {
    			new de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcRuntimeUtil().logError("Error while creating parser.", e);
    			return null;
    		}
    	}
    	
    	/**
    	 * This default constructor is only used to call createInstance() on it.
    	 */
    	public Tfm_ctcParser() {
    		super(null);
    	}
    	
    	protected EObject doParse() throws RecognitionException {
    		this.lastPosition = 0;
    		// required because the lexer class can not be subclassed
    		((Tfm_ctcLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
    		((Tfm_ctcLexer) getTokenStream().getTokenSource()).lexerExceptionPositions = lexerExceptionPositions;
    		Object typeObject = getTypeObject();
    		if (typeObject == null) {
    			return start();
    		} else if (typeObject instanceof EClass) {
    			EClass type = (EClass) typeObject;
    			if (type.getInstanceClass() == de.darwinspl.constraint.DwConstraintModel.class) {
    				return parse_de_darwinspl_constraint_DwConstraintModel();
    			}
    			if (type.getInstanceClass() == de.darwinspl.constraint.DwConstraint.class) {
    				return parse_de_darwinspl_constraint_DwConstraint();
    			}
    			if (type.getInstanceClass() == de.darwinspl.temporal.DwTemporalInterval.class) {
    				return parse_de_darwinspl_temporal_DwTemporalInterval();
    			}
    		}
    		throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcUnexpectedContentTypeException(typeObject);
    	}
    	
    	public int getMismatchedTokenRecoveryTries() {
    		return mismatchedTokenRecoveryTries;
    	}
    	
    	public Object getMissingSymbol(IntStream arg0, RecognitionException arg1, int arg2, BitSet arg3) {
    		mismatchedTokenRecoveryTries++;
    		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
    	}
    	
    	public Object getParseToIndexTypeObject() {
    		return parseToIndexTypeObject;
    	}
    	
    	protected Object getTypeObject() {
    		Object typeObject = getParseToIndexTypeObject();
    		if (typeObject != null) {
    			return typeObject;
    		}
    		Map<?,?> options = getOptions();
    		if (options != null) {
    			typeObject = options.get(de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcOptions.RESOURCE_CONTENT_TYPE);
    		}
    		return typeObject;
    	}
    	
    	/**
    	 * Implementation that calls {@link #doParse()} and handles the thrown
    	 * RecognitionExceptions.
    	 */
    	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcParseResult parse() {
    		// Reset parser state
    		terminateParsing = false;
    		postParseCommands = new ArrayList<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcCommand<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource>>();
    		de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcParseResult parseResult = new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcParseResult();
    		if (disableLocationMap) {
    			locationMap = new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcDevNullLocationMap();
    		} else {
    			locationMap = new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcLocationMap();
    		}
    		// Run parser
    		try {
    			EObject result =  doParse();
    			if (lexerExceptions.isEmpty()) {
    				parseResult.setRoot(result);
    				parseResult.setLocationMap(locationMap);
    			}
    		} catch (RecognitionException re) {
    			addErrorToResource(syntaxErrorMessageConverter.translateParseError(re));
    		} catch (IllegalArgumentException iae) {
    			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
    				// can be caused if a null is set on EMF models where not allowed. this will just
    				// happen if other errors occurred before
    			} else {
    				iae.printStackTrace();
    			}
    		}
    		for (RecognitionException re : lexerExceptions) {
    			addErrorToResource(syntaxErrorMessageConverter.translateLexicalError(re, lexerExceptions, lexerExceptionPositions));
    		}
    		parseResult.getPostParseCommands().addAll(postParseCommands);
    		return parseResult;
    	}
    	
    	public List<de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal> parseToExpectedElements(EClass type, de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource dummyResource, int cursorOffset) {
    		this.rememberExpectedElements = true;
    		this.parseToIndexTypeObject = type;
    		this.cursorOffset = cursorOffset;
    		this.lastStartIncludingHidden = -1;
    		final CommonTokenStream tokenStream = (CommonTokenStream) getTokenStream();
    		de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcParseResult result = parse();
    		for (EObject incompleteObject : incompleteObjects) {
    			Lexer lexer = (Lexer) tokenStream.getTokenSource();
    			int endChar = lexer.getCharIndex();
    			int endLine = lexer.getLine();
    			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
    		}
    		if (result != null) {
    			EObject root = result.getRoot();
    			if (root != null) {
    				dummyResource.getContentsInternal().add(root);
    			}
    			for (de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcCommand<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource> command : result.getPostParseCommands()) {
    				command.execute(dummyResource);
    			}
    		}
    		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
    		Set<de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal> currentFollowSet = new LinkedHashSet<de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal>();
    		List<de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal> newFollowSet = new ArrayList<de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal>();
    		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
    			de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal expectedElementI = expectedElements.get(i);
    			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
    				currentFollowSet.add(expectedElementI);
    			} else {
    				break;
    			}
    		}
    		int followSetID = 36;
    		int i;
    		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
    			CommonToken nextToken = (CommonToken) tokenStream.get(i);
    			if (nextToken.getType() < 0) {
    				break;
    			}
    			if (nextToken.getChannel() == 99) {
    				// hidden tokens do not reduce the follow set
    			} else {
    				// now that we have found the next visible token the position for that expected
    				// terminals can be set
    				for (de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal nextFollow : newFollowSet) {
    					lastTokenIndex = 0;
    					setPosition(nextFollow, i);
    				}
    				newFollowSet.clear();
    				// normal tokens do reduce the follow set - only elements that match the token are
    				// kept
    				for (de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal nextFollow : currentFollowSet) {
    					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
    						// keep this one - it matches
    						Collection<de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcPair<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcExpectedElement, de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcContainedFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
    						for (de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcPair<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcExpectedElement, de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcContainedFeature[]> newFollowerPair : newFollowers) {
    							de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcExpectedElement newFollower = newFollowerPair.getLeft();
    							EObject container = getLastIncompleteElement();
    							de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainmentTrace containmentTrace = new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainmentTrace(null, newFollowerPair.getRight());
    							de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal newFollowTerminal = new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal(container, newFollower, followSetID, containmentTrace);
    							newFollowSet.add(newFollowTerminal);
    							expectedElements.add(newFollowTerminal);
    						}
    					}
    				}
    				currentFollowSet.clear();
    				currentFollowSet.addAll(newFollowSet);
    			}
    			followSetID++;
    		}
    		// after the last token in the stream we must set the position for the elements
    		// that were added during the last iteration of the loop
    		for (de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal nextFollow : newFollowSet) {
    			lastTokenIndex = 0;
    			setPosition(nextFollow, i);
    		}
    		return this.expectedElements;
    	}
    	
    	public void setPosition(de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal expectedElement, int tokenIndex) {
    		int currentIndex = Math.max(0, tokenIndex);
    		for (int index = lastTokenIndex; index < currentIndex; index++) {
    			if (index >= input.size()) {
    				break;
    			}
    			CommonToken tokenAtIndex = (CommonToken) input.get(index);
    			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			if (tokenAtIndex.getChannel() != 99 && !anonymousTokens.contains(tokenAtIndex)) {
    				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			}
    		}
    		lastTokenIndex = Math.max(0, currentIndex);
    		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
    	}
    	
    	public Object recoverFromMismatchedToken(IntStream input, int ttype, BitSet follow) throws RecognitionException {
    		if (!rememberExpectedElements) {
    			return super.recoverFromMismatchedToken(input, ttype, follow);
    		} else {
    			return null;
    		}
    	}
    	
    	private void startIncompleteElement(Object object) {
    		if (object instanceof EObject) {
    			this.incompleteObjects.add((EObject) object);
    		}
    	}
    	
    	private void completedElement(Object object, boolean isContainment) {
    		if (isContainment && !this.incompleteObjects.isEmpty()) {
    			this.incompleteObjects.remove(object);
    		}
    		if (object instanceof EObject) {
    			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
    			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
    		}
    	}
    	
    	private EObject getLastIncompleteElement() {
    		if (incompleteObjects.isEmpty()) {
    			return null;
    		}
    		return incompleteObjects.get(incompleteObjects.size() - 1);
    	}
    	



    // $ANTLR start "start"
    // Tfm_ctc.g:488:1: start returns [ EObject element = null] : (c0= parse_de_darwinspl_constraint_DwConstraintModel ) EOF ;
    public final EObject start() throws RecognitionException {
        EObject element =  null;

        int start_StartIndex = input.index();

        de.darwinspl.constraint.DwConstraintModel c0 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 1) ) { return element; }

            // Tfm_ctc.g:489:2: ( (c0= parse_de_darwinspl_constraint_DwConstraintModel ) EOF )
            // Tfm_ctc.g:490:2: (c0= parse_de_darwinspl_constraint_DwConstraintModel ) EOF
            {
            if ( state.backtracking==0 ) {
            		// follow set for start rule(s)
            		addExpectedElement(null, 0);
            		expectedElementsIndexOfLastCompleteElement = 0;
            	}

            // Tfm_ctc.g:495:2: (c0= parse_de_darwinspl_constraint_DwConstraintModel )
            // Tfm_ctc.g:496:3: c0= parse_de_darwinspl_constraint_DwConstraintModel
            {
            pushFollow(FOLLOW_parse_de_darwinspl_constraint_DwConstraintModel_in_start82);
            c0=parse_de_darwinspl_constraint_DwConstraintModel();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) { element = c0; }

            }


            match(input,EOF,FOLLOW_EOF_in_start89); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		retrieveLayoutInformation(element, null, null, false);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 1, start_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "start"



    // $ANTLR start "parse_de_darwinspl_constraint_DwConstraintModel"
    // Tfm_ctc.g:504:1: parse_de_darwinspl_constraint_DwConstraintModel returns [de.darwinspl.constraint.DwConstraintModel element = null] : a0= 'feature' a1= 'model' (a2= PATH_TO_FILE ) ( ( (a3_0= parse_de_darwinspl_constraint_DwConstraint ) ) )* ;
    public final de.darwinspl.constraint.DwConstraintModel parse_de_darwinspl_constraint_DwConstraintModel() throws RecognitionException {
        de.darwinspl.constraint.DwConstraintModel element =  null;

        int parse_de_darwinspl_constraint_DwConstraintModel_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        de.darwinspl.constraint.DwConstraint a3_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 2) ) { return element; }

            // Tfm_ctc.g:507:2: (a0= 'feature' a1= 'model' (a2= PATH_TO_FILE ) ( ( (a3_0= parse_de_darwinspl_constraint_DwConstraint ) ) )* )
            // Tfm_ctc.g:508:2: a0= 'feature' a1= 'model' (a2= PATH_TO_FILE ) ( ( (a3_0= parse_de_darwinspl_constraint_DwConstraint ) ) )*
            {
            a0=(Token)match(input,24,FOLLOW_24_in_parse_de_darwinspl_constraint_DwConstraintModel115); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.constraint.DwConstraintFactory.eINSTANCE.createDwConstraintModel();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_0_0_0_0, null, true);
            		copyLocalizationInfos((CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 1);
            	}

            a1=(Token)match(input,26,FOLLOW_26_in_parse_de_darwinspl_constraint_DwConstraintModel129); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.constraint.DwConstraintFactory.eINSTANCE.createDwConstraintModel();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_0_0_0_1, null, true);
            		copyLocalizationInfos((CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 2);
            	}

            // Tfm_ctc.g:536:2: (a2= PATH_TO_FILE )
            // Tfm_ctc.g:537:3: a2= PATH_TO_FILE
            {
            a2=(Token)match(input,PATH_TO_FILE,FOLLOW_PATH_TO_FILE_in_parse_de_darwinspl_constraint_DwConstraintModel147); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.constraint.DwConstraintFactory.eINSTANCE.createDwConstraintModel();
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("PATH_TO_FILE");
            				tokenResolver.setOptions(getOptions());
            				de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT_MODEL__FEATURE_MODEL), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((CommonToken) a2).getLine(), ((CommonToken) a2).getCharPositionInLine(), ((CommonToken) a2).getStartIndex(), ((CommonToken) a2).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				de.darwinspl.feature.DwTemporalFeatureModel proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwTemporalFeatureModel();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcContextDependentURIFragmentFactory<de.darwinspl.constraint.DwConstraintModel, de.darwinspl.feature.DwTemporalFeatureModel>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getDwConstraintModelFeatureModelReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT_MODEL__FEATURE_MODEL), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT_MODEL__FEATURE_MODEL), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_0_0_0_2, proxy, true);
            				copyLocalizationInfos((CommonToken) a2, element);
            				copyLocalizationInfos((CommonToken) a2, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(), 3, 327);
            	}

            // Tfm_ctc.g:576:2: ( ( (a3_0= parse_de_darwinspl_constraint_DwConstraint ) ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==IDENTIFIER_TOKEN||LA1_0==QUOTED_34_34||LA1_0==13||LA1_0==15||LA1_0==23||LA1_0==27) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // Tfm_ctc.g:577:3: ( (a3_0= parse_de_darwinspl_constraint_DwConstraint ) )
            	    {
            	    // Tfm_ctc.g:577:3: ( (a3_0= parse_de_darwinspl_constraint_DwConstraint ) )
            	    // Tfm_ctc.g:578:4: (a3_0= parse_de_darwinspl_constraint_DwConstraint )
            	    {
            	    // Tfm_ctc.g:578:4: (a3_0= parse_de_darwinspl_constraint_DwConstraint )
            	    // Tfm_ctc.g:579:5: a3_0= parse_de_darwinspl_constraint_DwConstraint
            	    {
            	    pushFollow(FOLLOW_parse_de_darwinspl_constraint_DwConstraint_in_parse_de_darwinspl_constraint_DwConstraintModel183);
            	    a3_0=parse_de_darwinspl_constraint_DwConstraint();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = de.darwinspl.constraint.DwConstraintFactory.eINSTANCE.createDwConstraintModel();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a3_0 != null) {
            	    						if (a3_0 != null) {
            	    							Object value = a3_0;
            	    							addObjectToList(element, de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT_MODEL__CONSTRAINTS, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_0_0_0_5_0_0_0, a3_0, true);
            	    						copyLocalizationInfos(a3_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				// We've found the last token for this rule. The constructed EObject is now
            	    				// complete.
            	    				completedElement(element, true);
            	    				addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(), 328, 652);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(), 653, 977);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 2, parse_de_darwinspl_constraint_DwConstraintModel_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_constraint_DwConstraintModel"



    // $ANTLR start "parse_de_darwinspl_constraint_DwConstraint"
    // Tfm_ctc.g:619:1: parse_de_darwinspl_constraint_DwConstraint returns [de.darwinspl.constraint.DwConstraint element = null] : (a0_0= parse_de_darwinspl_expression_DwExpression ) ( ( (a1_0= parse_de_darwinspl_temporal_DwTemporalInterval ) ) )? ;
    public final de.darwinspl.constraint.DwConstraint parse_de_darwinspl_constraint_DwConstraint() throws RecognitionException {
        de.darwinspl.constraint.DwConstraint element =  null;

        int parse_de_darwinspl_constraint_DwConstraint_StartIndex = input.index();

        de.darwinspl.expression.DwExpression a0_0 =null;

        de.darwinspl.temporal.DwTemporalInterval a1_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 3) ) { return element; }

            // Tfm_ctc.g:622:2: ( (a0_0= parse_de_darwinspl_expression_DwExpression ) ( ( (a1_0= parse_de_darwinspl_temporal_DwTemporalInterval ) ) )? )
            // Tfm_ctc.g:623:2: (a0_0= parse_de_darwinspl_expression_DwExpression ) ( ( (a1_0= parse_de_darwinspl_temporal_DwTemporalInterval ) ) )?
            {
            // Tfm_ctc.g:623:2: (a0_0= parse_de_darwinspl_expression_DwExpression )
            // Tfm_ctc.g:624:3: a0_0= parse_de_darwinspl_expression_DwExpression
            {
            pushFollow(FOLLOW_parse_de_darwinspl_expression_DwExpression_in_parse_de_darwinspl_constraint_DwConstraint243);
            a0_0=parse_de_darwinspl_expression_DwExpression();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.darwinspl.constraint.DwConstraintFactory.eINSTANCE.createDwConstraint();
            				startIncompleteElement(element);
            			}
            			if (a0_0 != null) {
            				if (a0_0 != null) {
            					Object value = a0_0;
            					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT__ROOT_EXPRESSION), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_1_0_0_0, a0_0, true);
            				copyLocalizationInfos(a0_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraint(), 978);
            		addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(), 979, 1303);
            	}

            // Tfm_ctc.g:650:2: ( ( (a1_0= parse_de_darwinspl_temporal_DwTemporalInterval ) ) )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==20) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // Tfm_ctc.g:651:3: ( (a1_0= parse_de_darwinspl_temporal_DwTemporalInterval ) )
                    {
                    // Tfm_ctc.g:651:3: ( (a1_0= parse_de_darwinspl_temporal_DwTemporalInterval ) )
                    // Tfm_ctc.g:652:4: (a1_0= parse_de_darwinspl_temporal_DwTemporalInterval )
                    {
                    // Tfm_ctc.g:652:4: (a1_0= parse_de_darwinspl_temporal_DwTemporalInterval )
                    // Tfm_ctc.g:653:5: a1_0= parse_de_darwinspl_temporal_DwTemporalInterval
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_temporal_DwTemporalInterval_in_parse_de_darwinspl_constraint_DwConstraint276);
                    a1_0=parse_de_darwinspl_temporal_DwTemporalInterval();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = de.darwinspl.constraint.DwConstraintFactory.eINSTANCE.createDwConstraint();
                    						startIncompleteElement(element);
                    					}
                    					if (a1_0 != null) {
                    						if (a1_0 != null) {
                    							Object value = a1_0;
                    							element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT__VALIDITY), value);
                    							completedElement(value, true);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_1_0_0_1_0_0_0, a1_0, true);
                    						copyLocalizationInfos(a1_0, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				// We've found the last token for this rule. The constructed EObject is now
                    				// complete.
                    				completedElement(element, true);
                    				addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(), 1304, 1628);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(), 1629, 1953);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 3, parse_de_darwinspl_constraint_DwConstraint_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_constraint_DwConstraint"



    // $ANTLR start "parse_de_darwinspl_temporal_DwTemporalInterval"
    // Tfm_ctc.g:693:1: parse_de_darwinspl_temporal_DwTemporalInterval returns [de.darwinspl.temporal.DwTemporalInterval element = null] : a0= '[' ( (a1= DATE ) a2= '-' (a3= DATE ) | (a4= DATE ) a5= '-' a6= 'eternity' |a7= 'initial state' a8= '-' (a9= DATE ) |a10= 'initial state' a11= '-' a12= 'eternity' ) a13= ']' ;
    public final de.darwinspl.temporal.DwTemporalInterval parse_de_darwinspl_temporal_DwTemporalInterval() throws RecognitionException {
        de.darwinspl.temporal.DwTemporalInterval element =  null;

        int parse_de_darwinspl_temporal_DwTemporalInterval_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a8=null;
        Token a9=null;
        Token a10=null;
        Token a11=null;
        Token a12=null;
        Token a13=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 4) ) { return element; }

            // Tfm_ctc.g:696:2: (a0= '[' ( (a1= DATE ) a2= '-' (a3= DATE ) | (a4= DATE ) a5= '-' a6= 'eternity' |a7= 'initial state' a8= '-' (a9= DATE ) |a10= 'initial state' a11= '-' a12= 'eternity' ) a13= ']' )
            // Tfm_ctc.g:697:2: a0= '[' ( (a1= DATE ) a2= '-' (a3= DATE ) | (a4= DATE ) a5= '-' a6= 'eternity' |a7= 'initial state' a8= '-' (a9= DATE ) |a10= 'initial state' a11= '-' a12= 'eternity' ) a13= ']'
            {
            a0=(Token)match(input,20,FOLLOW_20_in_parse_de_darwinspl_temporal_DwTemporalInterval332); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_0, null, true);
            		copyLocalizationInfos((CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 1954, 1957);
            	}

            // Tfm_ctc.g:711:2: ( (a1= DATE ) a2= '-' (a3= DATE ) | (a4= DATE ) a5= '-' a6= 'eternity' |a7= 'initial state' a8= '-' (a9= DATE ) |a10= 'initial state' a11= '-' a12= 'eternity' )
            int alt3=4;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==DATE) ) {
                int LA3_1 = input.LA(2);

                if ( (LA3_1==17) ) {
                    int LA3_3 = input.LA(3);

                    if ( (LA3_3==DATE) ) {
                        alt3=1;
                    }
                    else if ( (LA3_3==22) ) {
                        alt3=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return element;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 3, 3, input);

                        throw nvae;

                    }
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 3, 1, input);

                    throw nvae;

                }
            }
            else if ( (LA3_0==25) ) {
                int LA3_2 = input.LA(2);

                if ( (LA3_2==17) ) {
                    int LA3_4 = input.LA(3);

                    if ( (LA3_4==DATE) ) {
                        alt3=3;
                    }
                    else if ( (LA3_4==22) ) {
                        alt3=4;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return element;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 3, 4, input);

                        throw nvae;

                    }
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 3, 2, input);

                    throw nvae;

                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;

            }
            switch (alt3) {
                case 1 :
                    // Tfm_ctc.g:712:3: (a1= DATE ) a2= '-' (a3= DATE )
                    {
                    // Tfm_ctc.g:712:3: (a1= DATE )
                    // Tfm_ctc.g:713:4: a1= DATE
                    {
                    a1=(Token)match(input,DATE,FOLLOW_DATE_in_parse_de_darwinspl_temporal_DwTemporalInterval355); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (terminateParsing) {
                    					throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
                    				}
                    				if (element == null) {
                    					element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
                    					startIncompleteElement(element);
                    				}
                    				if (a1 != null) {
                    					de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("DATE");
                    					tokenResolver.setOptions(getOptions());
                    					de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolveResult result = getFreshTokenResolveResult();
                    					tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__FROM), result);
                    					Object resolvedObject = result.getResolvedToken();
                    					if (resolvedObject == null) {
                    						addErrorToResource(result.getErrorMessage(), ((CommonToken) a1).getLine(), ((CommonToken) a1).getCharPositionInLine(), ((CommonToken) a1).getStartIndex(), ((CommonToken) a1).getStopIndex());
                    					}
                    					java.util.Date resolved = (java.util.Date) resolvedObject;
                    					if (resolved != null) {
                    						Object value = resolved;
                    						element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__FROM), value);
                    						completedElement(value, false);
                    					}
                    					collectHiddenTokens(element);
                    					retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_1_0_0_0, resolved, true);
                    					copyLocalizationInfos((CommonToken) a1, element);
                    				}
                    			}

                    }


                    if ( state.backtracking==0 ) {
                    			// expected elements (follow set)
                    			addExpectedElement(null, 1958);
                    		}

                    a2=(Token)match(input,17,FOLLOW_17_in_parse_de_darwinspl_temporal_DwTemporalInterval382); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    			if (element == null) {
                    				element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
                    				startIncompleteElement(element);
                    			}
                    			collectHiddenTokens(element);
                    			retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_1_0_0_1, null, true);
                    			copyLocalizationInfos((CommonToken)a2, element);
                    		}

                    if ( state.backtracking==0 ) {
                    			// expected elements (follow set)
                    			addExpectedElement(null, 1959);
                    		}

                    // Tfm_ctc.g:762:3: (a3= DATE )
                    // Tfm_ctc.g:763:4: a3= DATE
                    {
                    a3=(Token)match(input,DATE,FOLLOW_DATE_in_parse_de_darwinspl_temporal_DwTemporalInterval404); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (terminateParsing) {
                    					throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
                    				}
                    				if (element == null) {
                    					element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
                    					startIncompleteElement(element);
                    				}
                    				if (a3 != null) {
                    					de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("DATE");
                    					tokenResolver.setOptions(getOptions());
                    					de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolveResult result = getFreshTokenResolveResult();
                    					tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__TO), result);
                    					Object resolvedObject = result.getResolvedToken();
                    					if (resolvedObject == null) {
                    						addErrorToResource(result.getErrorMessage(), ((CommonToken) a3).getLine(), ((CommonToken) a3).getCharPositionInLine(), ((CommonToken) a3).getStartIndex(), ((CommonToken) a3).getStopIndex());
                    					}
                    					java.util.Date resolved = (java.util.Date) resolvedObject;
                    					if (resolved != null) {
                    						Object value = resolved;
                    						element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__TO), value);
                    						completedElement(value, false);
                    					}
                    					collectHiddenTokens(element);
                    					retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_1_0_0_2, resolved, true);
                    					copyLocalizationInfos((CommonToken) a3, element);
                    				}
                    			}

                    }


                    if ( state.backtracking==0 ) {
                    			// expected elements (follow set)
                    			addExpectedElement(null, 1960);
                    		}

                    }
                    break;
                case 2 :
                    // Tfm_ctc.g:799:6: (a4= DATE ) a5= '-' a6= 'eternity'
                    {
                    // Tfm_ctc.g:799:6: (a4= DATE )
                    // Tfm_ctc.g:800:4: a4= DATE
                    {
                    a4=(Token)match(input,DATE,FOLLOW_DATE_in_parse_de_darwinspl_temporal_DwTemporalInterval442); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (terminateParsing) {
                    					throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
                    				}
                    				if (element == null) {
                    					element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
                    					startIncompleteElement(element);
                    				}
                    				if (a4 != null) {
                    					de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("DATE");
                    					tokenResolver.setOptions(getOptions());
                    					de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolveResult result = getFreshTokenResolveResult();
                    					tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__FROM), result);
                    					Object resolvedObject = result.getResolvedToken();
                    					if (resolvedObject == null) {
                    						addErrorToResource(result.getErrorMessage(), ((CommonToken) a4).getLine(), ((CommonToken) a4).getCharPositionInLine(), ((CommonToken) a4).getStartIndex(), ((CommonToken) a4).getStopIndex());
                    					}
                    					java.util.Date resolved = (java.util.Date) resolvedObject;
                    					if (resolved != null) {
                    						Object value = resolved;
                    						element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__FROM), value);
                    						completedElement(value, false);
                    					}
                    					collectHiddenTokens(element);
                    					retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_1_0_1_0, resolved, true);
                    					copyLocalizationInfos((CommonToken) a4, element);
                    				}
                    			}

                    }


                    if ( state.backtracking==0 ) {
                    			// expected elements (follow set)
                    			addExpectedElement(null, 1961);
                    		}

                    a5=(Token)match(input,17,FOLLOW_17_in_parse_de_darwinspl_temporal_DwTemporalInterval469); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    			if (element == null) {
                    				element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
                    				startIncompleteElement(element);
                    			}
                    			collectHiddenTokens(element);
                    			retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_1_0_1_1, null, true);
                    			copyLocalizationInfos((CommonToken)a5, element);
                    		}

                    if ( state.backtracking==0 ) {
                    			// expected elements (follow set)
                    			addExpectedElement(null, 1962);
                    		}

                    a6=(Token)match(input,22,FOLLOW_22_in_parse_de_darwinspl_temporal_DwTemporalInterval486); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    			if (element == null) {
                    				element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
                    				startIncompleteElement(element);
                    			}
                    			collectHiddenTokens(element);
                    			retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_1_0_1_2, null, true);
                    			copyLocalizationInfos((CommonToken)a6, element);
                    		}

                    if ( state.backtracking==0 ) {
                    			// expected elements (follow set)
                    			addExpectedElement(null, 1963);
                    		}

                    }
                    break;
                case 3 :
                    // Tfm_ctc.g:864:6: a7= 'initial state' a8= '-' (a9= DATE )
                    {
                    a7=(Token)match(input,25,FOLLOW_25_in_parse_de_darwinspl_temporal_DwTemporalInterval509); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    			if (element == null) {
                    				element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
                    				startIncompleteElement(element);
                    			}
                    			collectHiddenTokens(element);
                    			retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_1_0_2_0, null, true);
                    			copyLocalizationInfos((CommonToken)a7, element);
                    		}

                    if ( state.backtracking==0 ) {
                    			// expected elements (follow set)
                    			addExpectedElement(null, 1964);
                    		}

                    a8=(Token)match(input,17,FOLLOW_17_in_parse_de_darwinspl_temporal_DwTemporalInterval526); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    			if (element == null) {
                    				element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
                    				startIncompleteElement(element);
                    			}
                    			collectHiddenTokens(element);
                    			retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_1_0_2_1, null, true);
                    			copyLocalizationInfos((CommonToken)a8, element);
                    		}

                    if ( state.backtracking==0 ) {
                    			// expected elements (follow set)
                    			addExpectedElement(null, 1965);
                    		}

                    // Tfm_ctc.g:892:3: (a9= DATE )
                    // Tfm_ctc.g:893:4: a9= DATE
                    {
                    a9=(Token)match(input,DATE,FOLLOW_DATE_in_parse_de_darwinspl_temporal_DwTemporalInterval548); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (terminateParsing) {
                    					throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
                    				}
                    				if (element == null) {
                    					element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
                    					startIncompleteElement(element);
                    				}
                    				if (a9 != null) {
                    					de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("DATE");
                    					tokenResolver.setOptions(getOptions());
                    					de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolveResult result = getFreshTokenResolveResult();
                    					tokenResolver.resolve(a9.getText(), element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__TO), result);
                    					Object resolvedObject = result.getResolvedToken();
                    					if (resolvedObject == null) {
                    						addErrorToResource(result.getErrorMessage(), ((CommonToken) a9).getLine(), ((CommonToken) a9).getCharPositionInLine(), ((CommonToken) a9).getStartIndex(), ((CommonToken) a9).getStopIndex());
                    					}
                    					java.util.Date resolved = (java.util.Date) resolvedObject;
                    					if (resolved != null) {
                    						Object value = resolved;
                    						element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__TO), value);
                    						completedElement(value, false);
                    					}
                    					collectHiddenTokens(element);
                    					retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_1_0_2_2, resolved, true);
                    					copyLocalizationInfos((CommonToken) a9, element);
                    				}
                    			}

                    }


                    if ( state.backtracking==0 ) {
                    			// expected elements (follow set)
                    			addExpectedElement(null, 1966);
                    		}

                    }
                    break;
                case 4 :
                    // Tfm_ctc.g:929:6: a10= 'initial state' a11= '-' a12= 'eternity'
                    {
                    a10=(Token)match(input,25,FOLLOW_25_in_parse_de_darwinspl_temporal_DwTemporalInterval581); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    			if (element == null) {
                    				element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
                    				startIncompleteElement(element);
                    			}
                    			collectHiddenTokens(element);
                    			retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_1_0_3_0, null, true);
                    			copyLocalizationInfos((CommonToken)a10, element);
                    		}

                    if ( state.backtracking==0 ) {
                    			// expected elements (follow set)
                    			addExpectedElement(null, 1967);
                    		}

                    a11=(Token)match(input,17,FOLLOW_17_in_parse_de_darwinspl_temporal_DwTemporalInterval598); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    			if (element == null) {
                    				element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
                    				startIncompleteElement(element);
                    			}
                    			collectHiddenTokens(element);
                    			retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_1_0_3_1, null, true);
                    			copyLocalizationInfos((CommonToken)a11, element);
                    		}

                    if ( state.backtracking==0 ) {
                    			// expected elements (follow set)
                    			addExpectedElement(null, 1968);
                    		}

                    a12=(Token)match(input,22,FOLLOW_22_in_parse_de_darwinspl_temporal_DwTemporalInterval615); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    			if (element == null) {
                    				element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
                    				startIncompleteElement(element);
                    			}
                    			collectHiddenTokens(element);
                    			retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_1_0_3_2, null, true);
                    			copyLocalizationInfos((CommonToken)a12, element);
                    		}

                    if ( state.backtracking==0 ) {
                    			// expected elements (follow set)
                    			addExpectedElement(null, 1969);
                    		}

                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, 1970);
            	}

            a13=(Token)match(input,21,FOLLOW_21_in_parse_de_darwinspl_temporal_DwTemporalInterval639); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_2, null, true);
            		copyLocalizationInfos((CommonToken)a13, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		// We've found the last token for this rule. The constructed EObject is now
            		// complete.
            		completedElement(element, true);
            		addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(), 1971, 2295);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 4, parse_de_darwinspl_temporal_DwTemporalInterval_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_temporal_DwTemporalInterval"



    // $ANTLR start "parseop_DwExpression_level_0"
    // Tfm_ctc.g:996:1: parseop_DwExpression_level_0 returns [de.darwinspl.expression.DwExpression element = null] : leftArg= parseop_DwExpression_level_1 ( ( () a0= '<->' rightArg= parseop_DwExpression_level_1 )+ |) ;
    public final de.darwinspl.expression.DwExpression parseop_DwExpression_level_0() throws RecognitionException {
        de.darwinspl.expression.DwExpression element =  null;

        int parseop_DwExpression_level_0_StartIndex = input.index();

        Token a0=null;
        de.darwinspl.expression.DwExpression leftArg =null;

        de.darwinspl.expression.DwExpression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 5) ) { return element; }

            // Tfm_ctc.g:999:2: (leftArg= parseop_DwExpression_level_1 ( ( () a0= '<->' rightArg= parseop_DwExpression_level_1 )+ |) )
            // Tfm_ctc.g:1000:2: leftArg= parseop_DwExpression_level_1 ( ( () a0= '<->' rightArg= parseop_DwExpression_level_1 )+ |)
            {
            pushFollow(FOLLOW_parseop_DwExpression_level_1_in_parseop_DwExpression_level_0668);
            leftArg=parseop_DwExpression_level_1();

            state._fsp--;
            if (state.failed) return element;

            // Tfm_ctc.g:1000:41: ( ( () a0= '<->' rightArg= parseop_DwExpression_level_1 )+ |)
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==19) ) {
                alt5=1;
            }
            else if ( (LA5_0==EOF||LA5_0==IDENTIFIER_TOKEN||LA5_0==QUOTED_34_34||LA5_0==13||(LA5_0 >= 15 && LA5_0 <= 16)||LA5_0==20||LA5_0==23||LA5_0==27) ) {
                alt5=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;

            }
            switch (alt5) {
                case 1 :
                    // Tfm_ctc.g:1000:42: ( () a0= '<->' rightArg= parseop_DwExpression_level_1 )+
                    {
                    // Tfm_ctc.g:1000:42: ( () a0= '<->' rightArg= parseop_DwExpression_level_1 )+
                    int cnt4=0;
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0==19) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // Tfm_ctc.g:1001:3: () a0= '<->' rightArg= parseop_DwExpression_level_1
                    	    {
                    	    // Tfm_ctc.g:1001:3: ()
                    	    // Tfm_ctc.g:1001:4: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,19,FOLLOW_19_in_parseop_DwExpression_level_0688); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    			if (element == null) {
                    	    				element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwEquivalenceExpression();
                    	    				startIncompleteElement(element);
                    	    			}
                    	    			collectHiddenTokens(element);
                    	    			retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_0_0_0_1, null, true);
                    	    			copyLocalizationInfos((CommonToken)a0, element);
                    	    		}

                    	    if ( state.backtracking==0 ) {
                    	    			// expected elements (follow set)
                    	    			addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwEquivalenceExpression(), 2296, 2620);
                    	    		}

                    	    pushFollow(FOLLOW_parseop_DwExpression_level_1_in_parseop_DwExpression_level_0705);
                    	    rightArg=parseop_DwExpression_level_1();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    			if (terminateParsing) {
                    	    				throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
                    	    			}
                    	    			if (element == null) {
                    	    				element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwEquivalenceExpression();
                    	    				startIncompleteElement(element);
                    	    			}
                    	    			if (leftArg != null) {
                    	    				if (leftArg != null) {
                    	    					Object value = leftArg;
                    	    					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_EQUIVALENCE_EXPRESSION__OPERAND1), value);
                    	    					completedElement(value, true);
                    	    				}
                    	    				collectHiddenTokens(element);
                    	    				retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_0_0_0_0, leftArg, true);
                    	    				copyLocalizationInfos(leftArg, element);
                    	    			}
                    	    		}

                    	    if ( state.backtracking==0 ) {
                    	    			if (terminateParsing) {
                    	    				throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
                    	    			}
                    	    			if (element == null) {
                    	    				element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwEquivalenceExpression();
                    	    				startIncompleteElement(element);
                    	    			}
                    	    			if (rightArg != null) {
                    	    				if (rightArg != null) {
                    	    					Object value = rightArg;
                    	    					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_EQUIVALENCE_EXPRESSION__OPERAND2), value);
                    	    					completedElement(value, true);
                    	    				}
                    	    				collectHiddenTokens(element);
                    	    				retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_0_0_0_2, rightArg, true);
                    	    				copyLocalizationInfos(rightArg, element);
                    	    			}
                    	    		}

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt4 >= 1 ) break loop4;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(4, input);
                                throw eee;
                        }
                        cnt4++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Tfm_ctc.g:1056:21: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 5, parseop_DwExpression_level_0_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_DwExpression_level_0"



    // $ANTLR start "parseop_DwExpression_level_1"
    // Tfm_ctc.g:1061:1: parseop_DwExpression_level_1 returns [de.darwinspl.expression.DwExpression element = null] : leftArg= parseop_DwExpression_level_2 ( ( () a0= '->' rightArg= parseop_DwExpression_level_2 )+ |) ;
    public final de.darwinspl.expression.DwExpression parseop_DwExpression_level_1() throws RecognitionException {
        de.darwinspl.expression.DwExpression element =  null;

        int parseop_DwExpression_level_1_StartIndex = input.index();

        Token a0=null;
        de.darwinspl.expression.DwExpression leftArg =null;

        de.darwinspl.expression.DwExpression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 6) ) { return element; }

            // Tfm_ctc.g:1064:9: (leftArg= parseop_DwExpression_level_2 ( ( () a0= '->' rightArg= parseop_DwExpression_level_2 )+ |) )
            // Tfm_ctc.g:1065:9: leftArg= parseop_DwExpression_level_2 ( ( () a0= '->' rightArg= parseop_DwExpression_level_2 )+ |)
            {
            pushFollow(FOLLOW_parseop_DwExpression_level_2_in_parseop_DwExpression_level_1751);
            leftArg=parseop_DwExpression_level_2();

            state._fsp--;
            if (state.failed) return element;

            // Tfm_ctc.g:1065:39: ( ( () a0= '->' rightArg= parseop_DwExpression_level_2 )+ |)
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==18) ) {
                alt7=1;
            }
            else if ( (LA7_0==EOF||LA7_0==IDENTIFIER_TOKEN||LA7_0==QUOTED_34_34||LA7_0==13||(LA7_0 >= 15 && LA7_0 <= 16)||(LA7_0 >= 19 && LA7_0 <= 20)||LA7_0==23||LA7_0==27) ) {
                alt7=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;

            }
            switch (alt7) {
                case 1 :
                    // Tfm_ctc.g:1065:40: ( () a0= '->' rightArg= parseop_DwExpression_level_2 )+
                    {
                    // Tfm_ctc.g:1065:40: ( () a0= '->' rightArg= parseop_DwExpression_level_2 )+
                    int cnt6=0;
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( (LA6_0==18) ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // Tfm_ctc.g:1066:2: () a0= '->' rightArg= parseop_DwExpression_level_2
                    	    {
                    	    // Tfm_ctc.g:1066:2: ()
                    	    // Tfm_ctc.g:1066:3: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,18,FOLLOW_18_in_parseop_DwExpression_level_1767); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    		if (element == null) {
                    	    			element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwImpliesExpression();
                    	    			startIncompleteElement(element);
                    	    		}
                    	    		collectHiddenTokens(element);
                    	    		retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_1_0_0_1, null, true);
                    	    		copyLocalizationInfos((CommonToken)a0, element);
                    	    	}

                    	    if ( state.backtracking==0 ) {
                    	    		// expected elements (follow set)
                    	    		addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwImpliesExpression(), 2621, 2945);
                    	    	}

                    	    pushFollow(FOLLOW_parseop_DwExpression_level_2_in_parseop_DwExpression_level_1781);
                    	    rightArg=parseop_DwExpression_level_2();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    		if (terminateParsing) {
                    	    			throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
                    	    		}
                    	    		if (element == null) {
                    	    			element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwImpliesExpression();
                    	    			startIncompleteElement(element);
                    	    		}
                    	    		if (leftArg != null) {
                    	    			if (leftArg != null) {
                    	    				Object value = leftArg;
                    	    				element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_IMPLIES_EXPRESSION__OPERAND1), value);
                    	    				completedElement(value, true);
                    	    			}
                    	    			collectHiddenTokens(element);
                    	    			retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_1_0_0_0, leftArg, true);
                    	    			copyLocalizationInfos(leftArg, element);
                    	    		}
                    	    	}

                    	    if ( state.backtracking==0 ) {
                    	    		if (terminateParsing) {
                    	    			throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
                    	    		}
                    	    		if (element == null) {
                    	    			element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwImpliesExpression();
                    	    			startIncompleteElement(element);
                    	    		}
                    	    		if (rightArg != null) {
                    	    			if (rightArg != null) {
                    	    				Object value = rightArg;
                    	    				element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_IMPLIES_EXPRESSION__OPERAND2), value);
                    	    				completedElement(value, true);
                    	    			}
                    	    			collectHiddenTokens(element);
                    	    			retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_1_0_0_2, rightArg, true);
                    	    			copyLocalizationInfos(rightArg, element);
                    	    		}
                    	    	}

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt6 >= 1 ) break loop6;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(6, input);
                                throw eee;
                        }
                        cnt6++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Tfm_ctc.g:1121:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 6, parseop_DwExpression_level_1_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_DwExpression_level_1"



    // $ANTLR start "parseop_DwExpression_level_2"
    // Tfm_ctc.g:1126:1: parseop_DwExpression_level_2 returns [de.darwinspl.expression.DwExpression element = null] : leftArg= parseop_DwExpression_level_3 ( ( () a0= '||' rightArg= parseop_DwExpression_level_3 )+ |) ;
    public final de.darwinspl.expression.DwExpression parseop_DwExpression_level_2() throws RecognitionException {
        de.darwinspl.expression.DwExpression element =  null;

        int parseop_DwExpression_level_2_StartIndex = input.index();

        Token a0=null;
        de.darwinspl.expression.DwExpression leftArg =null;

        de.darwinspl.expression.DwExpression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 7) ) { return element; }

            // Tfm_ctc.g:1129:9: (leftArg= parseop_DwExpression_level_3 ( ( () a0= '||' rightArg= parseop_DwExpression_level_3 )+ |) )
            // Tfm_ctc.g:1130:9: leftArg= parseop_DwExpression_level_3 ( ( () a0= '||' rightArg= parseop_DwExpression_level_3 )+ |)
            {
            pushFollow(FOLLOW_parseop_DwExpression_level_3_in_parseop_DwExpression_level_2822);
            leftArg=parseop_DwExpression_level_3();

            state._fsp--;
            if (state.failed) return element;

            // Tfm_ctc.g:1130:39: ( ( () a0= '||' rightArg= parseop_DwExpression_level_3 )+ |)
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==28) ) {
                alt9=1;
            }
            else if ( (LA9_0==EOF||LA9_0==IDENTIFIER_TOKEN||LA9_0==QUOTED_34_34||LA9_0==13||(LA9_0 >= 15 && LA9_0 <= 16)||(LA9_0 >= 18 && LA9_0 <= 20)||LA9_0==23||LA9_0==27) ) {
                alt9=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;

            }
            switch (alt9) {
                case 1 :
                    // Tfm_ctc.g:1130:40: ( () a0= '||' rightArg= parseop_DwExpression_level_3 )+
                    {
                    // Tfm_ctc.g:1130:40: ( () a0= '||' rightArg= parseop_DwExpression_level_3 )+
                    int cnt8=0;
                    loop8:
                    do {
                        int alt8=2;
                        int LA8_0 = input.LA(1);

                        if ( (LA8_0==28) ) {
                            alt8=1;
                        }


                        switch (alt8) {
                    	case 1 :
                    	    // Tfm_ctc.g:1131:0: () a0= '||' rightArg= parseop_DwExpression_level_3
                    	    {
                    	    // Tfm_ctc.g:1131:2: ()
                    	    // Tfm_ctc.g:1131:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,28,FOLLOW_28_in_parseop_DwExpression_level_2835); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    	if (element == null) {
                    	    		element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwOrExpression();
                    	    		startIncompleteElement(element);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_2_0_0_1, null, true);
                    	    	copyLocalizationInfos((CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    	// expected elements (follow set)
                    	    	addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwOrExpression(), 2946, 3270);
                    	    }

                    	    pushFollow(FOLLOW_parseop_DwExpression_level_3_in_parseop_DwExpression_level_2846);
                    	    rightArg=parseop_DwExpression_level_3();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    	if (terminateParsing) {
                    	    		throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
                    	    	}
                    	    	if (element == null) {
                    	    		element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwOrExpression();
                    	    		startIncompleteElement(element);
                    	    	}
                    	    	if (leftArg != null) {
                    	    		if (leftArg != null) {
                    	    			Object value = leftArg;
                    	    			element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_OR_EXPRESSION__OPERAND1), value);
                    	    			completedElement(value, true);
                    	    		}
                    	    		collectHiddenTokens(element);
                    	    		retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_2_0_0_0, leftArg, true);
                    	    		copyLocalizationInfos(leftArg, element);
                    	    	}
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    	if (terminateParsing) {
                    	    		throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
                    	    	}
                    	    	if (element == null) {
                    	    		element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwOrExpression();
                    	    		startIncompleteElement(element);
                    	    	}
                    	    	if (rightArg != null) {
                    	    		if (rightArg != null) {
                    	    			Object value = rightArg;
                    	    			element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_OR_EXPRESSION__OPERAND2), value);
                    	    			completedElement(value, true);
                    	    		}
                    	    		collectHiddenTokens(element);
                    	    		retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_2_0_0_2, rightArg, true);
                    	    		copyLocalizationInfos(rightArg, element);
                    	    	}
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt8 >= 1 ) break loop8;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(8, input);
                                throw eee;
                        }
                        cnt8++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Tfm_ctc.g:1186:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 7, parseop_DwExpression_level_2_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_DwExpression_level_2"



    // $ANTLR start "parseop_DwExpression_level_3"
    // Tfm_ctc.g:1191:1: parseop_DwExpression_level_3 returns [de.darwinspl.expression.DwExpression element = null] : leftArg= parseop_DwExpression_level_14 ( ( () a0= '&&' rightArg= parseop_DwExpression_level_14 )+ |) ;
    public final de.darwinspl.expression.DwExpression parseop_DwExpression_level_3() throws RecognitionException {
        de.darwinspl.expression.DwExpression element =  null;

        int parseop_DwExpression_level_3_StartIndex = input.index();

        Token a0=null;
        de.darwinspl.expression.DwExpression leftArg =null;

        de.darwinspl.expression.DwExpression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 8) ) { return element; }

            // Tfm_ctc.g:1194:9: (leftArg= parseop_DwExpression_level_14 ( ( () a0= '&&' rightArg= parseop_DwExpression_level_14 )+ |) )
            // Tfm_ctc.g:1195:9: leftArg= parseop_DwExpression_level_14 ( ( () a0= '&&' rightArg= parseop_DwExpression_level_14 )+ |)
            {
            pushFollow(FOLLOW_parseop_DwExpression_level_14_in_parseop_DwExpression_level_3884);
            leftArg=parseop_DwExpression_level_14();

            state._fsp--;
            if (state.failed) return element;

            // Tfm_ctc.g:1195:40: ( ( () a0= '&&' rightArg= parseop_DwExpression_level_14 )+ |)
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==14) ) {
                alt11=1;
            }
            else if ( (LA11_0==EOF||LA11_0==IDENTIFIER_TOKEN||LA11_0==QUOTED_34_34||LA11_0==13||(LA11_0 >= 15 && LA11_0 <= 16)||(LA11_0 >= 18 && LA11_0 <= 20)||LA11_0==23||(LA11_0 >= 27 && LA11_0 <= 28)) ) {
                alt11=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;

            }
            switch (alt11) {
                case 1 :
                    // Tfm_ctc.g:1195:41: ( () a0= '&&' rightArg= parseop_DwExpression_level_14 )+
                    {
                    // Tfm_ctc.g:1195:41: ( () a0= '&&' rightArg= parseop_DwExpression_level_14 )+
                    int cnt10=0;
                    loop10:
                    do {
                        int alt10=2;
                        int LA10_0 = input.LA(1);

                        if ( (LA10_0==14) ) {
                            alt10=1;
                        }


                        switch (alt10) {
                    	case 1 :
                    	    // Tfm_ctc.g:1196:0: () a0= '&&' rightArg= parseop_DwExpression_level_14
                    	    {
                    	    // Tfm_ctc.g:1196:2: ()
                    	    // Tfm_ctc.g:1196:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,14,FOLLOW_14_in_parseop_DwExpression_level_3897); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwAndExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_3_0_0_1, null, true);
                    	    copyLocalizationInfos((CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwAndExpression(), 3271, 3595);
                    	    }

                    	    pushFollow(FOLLOW_parseop_DwExpression_level_14_in_parseop_DwExpression_level_3908);
                    	    rightArg=parseop_DwExpression_level_14();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    	throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwAndExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    	if (leftArg != null) {
                    	    		Object value = leftArg;
                    	    		element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_AND_EXPRESSION__OPERAND1), value);
                    	    		completedElement(value, true);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_3_0_0_0, leftArg, true);
                    	    	copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    	throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwAndExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    	if (rightArg != null) {
                    	    		Object value = rightArg;
                    	    		element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_AND_EXPRESSION__OPERAND2), value);
                    	    		completedElement(value, true);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_3_0_0_2, rightArg, true);
                    	    	copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt10 >= 1 ) break loop10;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(10, input);
                                throw eee;
                        }
                        cnt10++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Tfm_ctc.g:1251:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 8, parseop_DwExpression_level_3_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_DwExpression_level_3"



    // $ANTLR start "parseop_DwExpression_level_14"
    // Tfm_ctc.g:1256:1: parseop_DwExpression_level_14 returns [de.darwinspl.expression.DwExpression element = null] : (a0= '!' arg= parseop_DwExpression_level_15 |arg= parseop_DwExpression_level_15 );
    public final de.darwinspl.expression.DwExpression parseop_DwExpression_level_14() throws RecognitionException {
        de.darwinspl.expression.DwExpression element =  null;

        int parseop_DwExpression_level_14_StartIndex = input.index();

        Token a0=null;
        de.darwinspl.expression.DwExpression arg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 9) ) { return element; }

            // Tfm_ctc.g:1259:0: (a0= '!' arg= parseop_DwExpression_level_15 |arg= parseop_DwExpression_level_15 )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==13) ) {
                alt12=1;
            }
            else if ( (LA12_0==IDENTIFIER_TOKEN||LA12_0==QUOTED_34_34||LA12_0==15||LA12_0==23||LA12_0==27) ) {
                alt12=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;

            }
            switch (alt12) {
                case 1 :
                    // Tfm_ctc.g:1260:0: a0= '!' arg= parseop_DwExpression_level_15
                    {
                    a0=(Token)match(input,13,FOLLOW_13_in_parseop_DwExpression_level_14946); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwNotExpression();
                    startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_4_0_0_0, null, true);
                    copyLocalizationInfos((CommonToken)a0, element);
                    }

                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwNotExpression(), 3596, 3920);
                    }

                    pushFollow(FOLLOW_parseop_DwExpression_level_15_in_parseop_DwExpression_level_14957);
                    arg=parseop_DwExpression_level_15();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
                    }
                    if (element == null) {
                    element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwNotExpression();
                    startIncompleteElement(element);
                    }
                    if (arg != null) {
                    if (arg != null) {
                    Object value = arg;
                    element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_NOT_EXPRESSION__OPERAND), value);
                    completedElement(value, true);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_4_0_0_1, arg, true);
                    copyLocalizationInfos(arg, element);
                    }
                    }

                    }
                    break;
                case 2 :
                    // Tfm_ctc.g:1295:5: arg= parseop_DwExpression_level_15
                    {
                    pushFollow(FOLLOW_parseop_DwExpression_level_15_in_parseop_DwExpression_level_14967);
                    arg=parseop_DwExpression_level_15();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = arg; }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 9, parseop_DwExpression_level_14_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_DwExpression_level_14"



    // $ANTLR start "parseop_DwExpression_level_15"
    // Tfm_ctc.g:1298:1: parseop_DwExpression_level_15 returns [de.darwinspl.expression.DwExpression element = null] : (c0= parse_de_darwinspl_expression_DwNestedExpression |c1= parse_de_darwinspl_expression_DwFeatureReferenceExpression |c2= parse_de_darwinspl_expression_DwBooleanValueExpression );
    public final de.darwinspl.expression.DwExpression parseop_DwExpression_level_15() throws RecognitionException {
        de.darwinspl.expression.DwExpression element =  null;

        int parseop_DwExpression_level_15_StartIndex = input.index();

        de.darwinspl.expression.DwNestedExpression c0 =null;

        de.darwinspl.expression.DwFeatureReferenceExpression c1 =null;

        de.darwinspl.expression.DwBooleanValueExpression c2 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 10) ) { return element; }

            // Tfm_ctc.g:1301:0: (c0= parse_de_darwinspl_expression_DwNestedExpression |c1= parse_de_darwinspl_expression_DwFeatureReferenceExpression |c2= parse_de_darwinspl_expression_DwBooleanValueExpression )
            int alt13=3;
            switch ( input.LA(1) ) {
            case 15:
                {
                alt13=1;
                }
                break;
            case IDENTIFIER_TOKEN:
            case QUOTED_34_34:
                {
                alt13=2;
                }
                break;
            case 23:
            case 27:
                {
                alt13=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;

            }

            switch (alt13) {
                case 1 :
                    // Tfm_ctc.g:1302:0: c0= parse_de_darwinspl_expression_DwNestedExpression
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_expression_DwNestedExpression_in_parseop_DwExpression_level_15989);
                    c0=parse_de_darwinspl_expression_DwNestedExpression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Tfm_ctc.g:1303:2: c1= parse_de_darwinspl_expression_DwFeatureReferenceExpression
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_expression_DwFeatureReferenceExpression_in_parseop_DwExpression_level_15997);
                    c1=parse_de_darwinspl_expression_DwFeatureReferenceExpression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 3 :
                    // Tfm_ctc.g:1304:2: c2= parse_de_darwinspl_expression_DwBooleanValueExpression
                    {
                    pushFollow(FOLLOW_parse_de_darwinspl_expression_DwBooleanValueExpression_in_parseop_DwExpression_level_151005);
                    c2=parse_de_darwinspl_expression_DwBooleanValueExpression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c2; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 10, parseop_DwExpression_level_15_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_DwExpression_level_15"



    // $ANTLR start "parse_de_darwinspl_expression_DwNestedExpression"
    // Tfm_ctc.g:1307:1: parse_de_darwinspl_expression_DwNestedExpression returns [de.darwinspl.expression.DwNestedExpression element = null] : a0= '(' (a1_0= parse_de_darwinspl_expression_DwExpression ) a2= ')' ;
    public final de.darwinspl.expression.DwNestedExpression parse_de_darwinspl_expression_DwNestedExpression() throws RecognitionException {
        de.darwinspl.expression.DwNestedExpression element =  null;

        int parse_de_darwinspl_expression_DwNestedExpression_StartIndex = input.index();

        Token a0=null;
        Token a2=null;
        de.darwinspl.expression.DwExpression a1_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 11) ) { return element; }

            // Tfm_ctc.g:1310:4: (a0= '(' (a1_0= parse_de_darwinspl_expression_DwExpression ) a2= ')' )
            // Tfm_ctc.g:1311:4: a0= '(' (a1_0= parse_de_darwinspl_expression_DwExpression ) a2= ')'
            {
            a0=(Token)match(input,15,FOLLOW_15_in_parse_de_darwinspl_expression_DwNestedExpression1027); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (element == null) {
            element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwNestedExpression();
            startIncompleteElement(element);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_5_0_0_0, null, true);
            copyLocalizationInfos((CommonToken)a0, element);
            }

            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwNestedExpression(), 3921, 4245);
            }

            // Tfm_ctc.g:1325:6: (a1_0= parse_de_darwinspl_expression_DwExpression )
            // Tfm_ctc.g:1326:6: a1_0= parse_de_darwinspl_expression_DwExpression
            {
            pushFollow(FOLLOW_parse_de_darwinspl_expression_DwExpression_in_parse_de_darwinspl_expression_DwNestedExpression1040);
            a1_0=parse_de_darwinspl_expression_DwExpression();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (terminateParsing) {
            throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
            }
            if (element == null) {
            element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwNestedExpression();
            startIncompleteElement(element);
            }
            if (a1_0 != null) {
            if (a1_0 != null) {
            	Object value = a1_0;
            	element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_NESTED_EXPRESSION__OPERAND), value);
            	completedElement(value, true);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_5_0_0_1, a1_0, true);
            copyLocalizationInfos(a1_0, element);
            }
            }

            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(null, 4246);
            }

            a2=(Token)match(input,16,FOLLOW_16_in_parse_de_darwinspl_expression_DwNestedExpression1052); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (element == null) {
            element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwNestedExpression();
            startIncompleteElement(element);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_5_0_0_2, null, true);
            copyLocalizationInfos((CommonToken)a2, element);
            }

            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            // We've found the last token for this rule. The constructed EObject is now
            // complete.
            completedElement(element, true);
            addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraint(), 4247);
            addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(), 4248, 4572);
            addExpectedElement(null, 4573, 4577);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 11, parse_de_darwinspl_expression_DwNestedExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_expression_DwNestedExpression"



    // $ANTLR start "parse_de_darwinspl_expression_DwFeatureReferenceExpression"
    // Tfm_ctc.g:1372:1: parse_de_darwinspl_expression_DwFeatureReferenceExpression returns [de.darwinspl.expression.DwFeatureReferenceExpression element = null] : ( (a0= QUOTED_34_34 ) | (a1= IDENTIFIER_TOKEN ) ) ;
    public final de.darwinspl.expression.DwFeatureReferenceExpression parse_de_darwinspl_expression_DwFeatureReferenceExpression() throws RecognitionException {
        de.darwinspl.expression.DwFeatureReferenceExpression element =  null;

        int parse_de_darwinspl_expression_DwFeatureReferenceExpression_StartIndex = input.index();

        Token a0=null;
        Token a1=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 12) ) { return element; }

            // Tfm_ctc.g:1375:0: ( ( (a0= QUOTED_34_34 ) | (a1= IDENTIFIER_TOKEN ) ) )
            // Tfm_ctc.g:1376:0: ( (a0= QUOTED_34_34 ) | (a1= IDENTIFIER_TOKEN ) )
            {
            // Tfm_ctc.g:1376:0: ( (a0= QUOTED_34_34 ) | (a1= IDENTIFIER_TOKEN ) )
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==QUOTED_34_34) ) {
                alt14=1;
            }
            else if ( (LA14_0==IDENTIFIER_TOKEN) ) {
                alt14=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;

            }
            switch (alt14) {
                case 1 :
                    // Tfm_ctc.g:1377:0: (a0= QUOTED_34_34 )
                    {
                    // Tfm_ctc.g:1377:4: (a0= QUOTED_34_34 )
                    // Tfm_ctc.g:1378:4: a0= QUOTED_34_34
                    {
                    a0=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_de_darwinspl_expression_DwFeatureReferenceExpression1082); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    	throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
                    }
                    if (element == null) {
                    	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwFeatureReferenceExpression();
                    	startIncompleteElement(element);
                    }
                    if (a0 != null) {
                    	de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
                    	tokenResolver.setOptions(getOptions());
                    	de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolveResult result = getFreshTokenResolveResult();
                    	tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), result);
                    	Object resolvedObject = result.getResolvedToken();
                    	if (resolvedObject == null) {
                    		addErrorToResource(result.getErrorMessage(), ((CommonToken) a0).getLine(), ((CommonToken) a0).getCharPositionInLine(), ((CommonToken) a0).getStartIndex(), ((CommonToken) a0).getStopIndex());
                    	}
                    	String resolved = (String) resolvedObject;
                    	de.darwinspl.feature.DwFeature proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwFeature();
                    	collectHiddenTokens(element);
                    	registerContextDependentProxy(new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcContextDependentURIFragmentFactory<de.darwinspl.expression.DwFeatureReferenceExpression, de.darwinspl.feature.DwFeature>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getDwFeatureReferenceExpressionFeatureReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), resolved, proxy);
                    	if (proxy != null) {
                    		Object value = proxy;
                    		element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), value);
                    		completedElement(value, false);
                    	}
                    	collectHiddenTokens(element);
                    	retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_6_0_0_0_0_0_0, proxy, true);
                    	copyLocalizationInfos((CommonToken) a0, element);
                    	copyLocalizationInfos((CommonToken) a0, proxy);
                    }
                    }

                    }


                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraint(), 4578);
                    addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(), 4579, 4903);
                    addExpectedElement(null, 4904, 4908);
                    }

                    }
                    break;
                case 2 :
                    // Tfm_ctc.g:1420:2: (a1= IDENTIFIER_TOKEN )
                    {
                    // Tfm_ctc.g:1420:2: (a1= IDENTIFIER_TOKEN )
                    // Tfm_ctc.g:1421:4: a1= IDENTIFIER_TOKEN
                    {
                    a1=(Token)match(input,IDENTIFIER_TOKEN,FOLLOW_IDENTIFIER_TOKEN_in_parse_de_darwinspl_expression_DwFeatureReferenceExpression1099); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    	throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
                    }
                    if (element == null) {
                    	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwFeatureReferenceExpression();
                    	startIncompleteElement(element);
                    }
                    if (a1 != null) {
                    	de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER_TOKEN");
                    	tokenResolver.setOptions(getOptions());
                    	de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolveResult result = getFreshTokenResolveResult();
                    	tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), result);
                    	Object resolvedObject = result.getResolvedToken();
                    	if (resolvedObject == null) {
                    		addErrorToResource(result.getErrorMessage(), ((CommonToken) a1).getLine(), ((CommonToken) a1).getCharPositionInLine(), ((CommonToken) a1).getStartIndex(), ((CommonToken) a1).getStopIndex());
                    	}
                    	String resolved = (String) resolvedObject;
                    	de.darwinspl.feature.DwFeature proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwFeature();
                    	collectHiddenTokens(element);
                    	registerContextDependentProxy(new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcContextDependentURIFragmentFactory<de.darwinspl.expression.DwFeatureReferenceExpression, de.darwinspl.feature.DwFeature>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getDwFeatureReferenceExpressionFeatureReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), resolved, proxy);
                    	if (proxy != null) {
                    		Object value = proxy;
                    		element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), value);
                    		completedElement(value, false);
                    	}
                    	collectHiddenTokens(element);
                    	retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_6_0_0_0_0_1_0, proxy, true);
                    	copyLocalizationInfos((CommonToken) a1, element);
                    	copyLocalizationInfos((CommonToken) a1, proxy);
                    }
                    }

                    }


                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    // We've found the last token for this rule. The constructed EObject is now
                    // complete.
                    completedElement(element, true);
                    addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraint(), 4909);
                    addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(), 4910, 5234);
                    addExpectedElement(null, 5235, 5239);
                    }

                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            // We've found the last token for this rule. The constructed EObject is now
            // complete.
            completedElement(element, true);
            addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraint(), 5240);
            addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(), 5241, 5565);
            addExpectedElement(null, 5566, 5570);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 12, parse_de_darwinspl_expression_DwFeatureReferenceExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_expression_DwFeatureReferenceExpression"



    // $ANTLR start "parse_de_darwinspl_expression_DwBooleanValueExpression"
    // Tfm_ctc.g:1478:1: parse_de_darwinspl_expression_DwBooleanValueExpression returns [de.darwinspl.expression.DwBooleanValueExpression element = null] : ( (a0= 'true' |a1= 'false' ) ) ;
    public final de.darwinspl.expression.DwBooleanValueExpression parse_de_darwinspl_expression_DwBooleanValueExpression() throws RecognitionException {
        de.darwinspl.expression.DwBooleanValueExpression element =  null;

        int parse_de_darwinspl_expression_DwBooleanValueExpression_StartIndex = input.index();

        Token a0=null;
        Token a1=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 13) ) { return element; }

            // Tfm_ctc.g:1481:0: ( ( (a0= 'true' |a1= 'false' ) ) )
            // Tfm_ctc.g:1482:0: ( (a0= 'true' |a1= 'false' ) )
            {
            // Tfm_ctc.g:1482:0: ( (a0= 'true' |a1= 'false' ) )
            // Tfm_ctc.g:1483:0: (a0= 'true' |a1= 'false' )
            {
            // Tfm_ctc.g:1483:0: (a0= 'true' |a1= 'false' )
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==27) ) {
                alt15=1;
            }
            else if ( (LA15_0==23) ) {
                alt15=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;

            }
            switch (alt15) {
                case 1 :
                    // Tfm_ctc.g:1484:0: a0= 'true'
                    {
                    a0=(Token)match(input,27,FOLLOW_27_in_parse_de_darwinspl_expression_DwBooleanValueExpression1136); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwBooleanValueExpression();
                    	startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_7_0_0_0, true, true);
                    copyLocalizationInfos((CommonToken)a0, element);
                    // set value of boolean attribute
                    Object value = true;
                    element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_BOOLEAN_VALUE_EXPRESSION__VALUE), value);
                    completedElement(value, false);
                    }

                    }
                    break;
                case 2 :
                    // Tfm_ctc.g:1497:2: a1= 'false'
                    {
                    a1=(Token)match(input,23,FOLLOW_23_in_parse_de_darwinspl_expression_DwBooleanValueExpression1145); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwBooleanValueExpression();
                    	startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_7_0_0_0, false, true);
                    copyLocalizationInfos((CommonToken)a1, element);
                    // set value of boolean attribute
                    Object value = false;
                    element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_BOOLEAN_VALUE_EXPRESSION__VALUE), value);
                    completedElement(value, false);
                    }

                    }
                    break;

            }


            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            // We've found the last token for this rule. The constructed EObject is now
            // complete.
            completedElement(element, true);
            addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraint(), 5571);
            addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(), 5572, 5896);
            addExpectedElement(null, 5897, 5901);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 13, parse_de_darwinspl_expression_DwBooleanValueExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_expression_DwBooleanValueExpression"



    // $ANTLR start "parse_de_darwinspl_expression_DwExpression"
    // Tfm_ctc.g:1524:1: parse_de_darwinspl_expression_DwExpression returns [de.darwinspl.expression.DwExpression element = null] : c= parseop_DwExpression_level_0 ;
    public final de.darwinspl.expression.DwExpression parse_de_darwinspl_expression_DwExpression() throws RecognitionException {
        de.darwinspl.expression.DwExpression element =  null;

        int parse_de_darwinspl_expression_DwExpression_StartIndex = input.index();

        de.darwinspl.expression.DwExpression c =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 14) ) { return element; }

            // Tfm_ctc.g:1525:3: (c= parseop_DwExpression_level_0 )
            // Tfm_ctc.g:1526:3: c= parseop_DwExpression_level_0
            {
            pushFollow(FOLLOW_parseop_DwExpression_level_0_in_parse_de_darwinspl_expression_DwExpression1171);
            c=parseop_DwExpression_level_0();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) { element = c; /* this rule is an expression root */ }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 14, parse_de_darwinspl_expression_DwExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_darwinspl_expression_DwExpression"

    // Delegated rules


 

    public static final BitSet FOLLOW_parse_de_darwinspl_constraint_DwConstraintModel_in_start82 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_start89 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_parse_de_darwinspl_constraint_DwConstraintModel115 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_parse_de_darwinspl_constraint_DwConstraintModel129 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_PATH_TO_FILE_in_parse_de_darwinspl_constraint_DwConstraintModel147 = new BitSet(new long[]{0x000000000880A422L});
    public static final BitSet FOLLOW_parse_de_darwinspl_constraint_DwConstraint_in_parse_de_darwinspl_constraint_DwConstraintModel183 = new BitSet(new long[]{0x000000000880A422L});
    public static final BitSet FOLLOW_parse_de_darwinspl_expression_DwExpression_in_parse_de_darwinspl_constraint_DwConstraint243 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_temporal_DwTemporalInterval_in_parse_de_darwinspl_constraint_DwConstraint276 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_parse_de_darwinspl_temporal_DwTemporalInterval332 = new BitSet(new long[]{0x0000000002000010L});
    public static final BitSet FOLLOW_DATE_in_parse_de_darwinspl_temporal_DwTemporalInterval355 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_parse_de_darwinspl_temporal_DwTemporalInterval382 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_DATE_in_parse_de_darwinspl_temporal_DwTemporalInterval404 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_DATE_in_parse_de_darwinspl_temporal_DwTemporalInterval442 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_parse_de_darwinspl_temporal_DwTemporalInterval469 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_22_in_parse_de_darwinspl_temporal_DwTemporalInterval486 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_25_in_parse_de_darwinspl_temporal_DwTemporalInterval509 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_parse_de_darwinspl_temporal_DwTemporalInterval526 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_DATE_in_parse_de_darwinspl_temporal_DwTemporalInterval548 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_25_in_parse_de_darwinspl_temporal_DwTemporalInterval581 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_parse_de_darwinspl_temporal_DwTemporalInterval598 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_22_in_parse_de_darwinspl_temporal_DwTemporalInterval615 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_parse_de_darwinspl_temporal_DwTemporalInterval639 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_DwExpression_level_1_in_parseop_DwExpression_level_0668 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_19_in_parseop_DwExpression_level_0688 = new BitSet(new long[]{0x000000000880A420L});
    public static final BitSet FOLLOW_parseop_DwExpression_level_1_in_parseop_DwExpression_level_0705 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_parseop_DwExpression_level_2_in_parseop_DwExpression_level_1751 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_18_in_parseop_DwExpression_level_1767 = new BitSet(new long[]{0x000000000880A420L});
    public static final BitSet FOLLOW_parseop_DwExpression_level_2_in_parseop_DwExpression_level_1781 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_parseop_DwExpression_level_3_in_parseop_DwExpression_level_2822 = new BitSet(new long[]{0x0000000010000002L});
    public static final BitSet FOLLOW_28_in_parseop_DwExpression_level_2835 = new BitSet(new long[]{0x000000000880A420L});
    public static final BitSet FOLLOW_parseop_DwExpression_level_3_in_parseop_DwExpression_level_2846 = new BitSet(new long[]{0x0000000010000002L});
    public static final BitSet FOLLOW_parseop_DwExpression_level_14_in_parseop_DwExpression_level_3884 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_14_in_parseop_DwExpression_level_3897 = new BitSet(new long[]{0x000000000880A420L});
    public static final BitSet FOLLOW_parseop_DwExpression_level_14_in_parseop_DwExpression_level_3908 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_13_in_parseop_DwExpression_level_14946 = new BitSet(new long[]{0x0000000008808420L});
    public static final BitSet FOLLOW_parseop_DwExpression_level_15_in_parseop_DwExpression_level_14957 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_DwExpression_level_15_in_parseop_DwExpression_level_14967 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_expression_DwNestedExpression_in_parseop_DwExpression_level_15989 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_expression_DwFeatureReferenceExpression_in_parseop_DwExpression_level_15997 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_darwinspl_expression_DwBooleanValueExpression_in_parseop_DwExpression_level_151005 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_parse_de_darwinspl_expression_DwNestedExpression1027 = new BitSet(new long[]{0x000000000880A420L});
    public static final BitSet FOLLOW_parse_de_darwinspl_expression_DwExpression_in_parse_de_darwinspl_expression_DwNestedExpression1040 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_parse_de_darwinspl_expression_DwNestedExpression1052 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_de_darwinspl_expression_DwFeatureReferenceExpression1082 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTIFIER_TOKEN_in_parse_de_darwinspl_expression_DwFeatureReferenceExpression1099 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_parse_de_darwinspl_expression_DwBooleanValueExpression1136 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_parse_de_darwinspl_expression_DwBooleanValueExpression1145 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_DwExpression_level_0_in_parse_de_darwinspl_expression_DwExpression1171 = new BitSet(new long[]{0x0000000000000002L});

}