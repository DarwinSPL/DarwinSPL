grammar Tfm_ctc;

options {
	superClass = Tfm_ctcANTLRParserBase;
	backtrack = true;
	memoize = true;
}

@lexer::header {
	package de.darwinspl.constraint.resource.tfm_ctc.mopp;
	
	import java.util.ArrayList;
import java.util.List;
import org.antlr.runtime3_4_0.ANTLRStringStream;
import org.antlr.runtime3_4_0.RecognitionException;
}

@lexer::members {
	public List<RecognitionException> lexerExceptions  = new ArrayList<RecognitionException>();
	public List<Integer> lexerExceptionPositions = new ArrayList<Integer>();
	
	public void reportError(RecognitionException e) {
		lexerExceptions.add(e);
		lexerExceptionPositions.add(((ANTLRStringStream) input).index());
	}
}
@header{
	package de.darwinspl.constraint.resource.tfm_ctc.mopp;
	
	import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.antlr.runtime3_4_0.ANTLRInputStream;
import org.antlr.runtime3_4_0.BitSet;
import org.antlr.runtime3_4_0.CommonToken;
import org.antlr.runtime3_4_0.CommonTokenStream;
import org.antlr.runtime3_4_0.IntStream;
import org.antlr.runtime3_4_0.Lexer;
import org.antlr.runtime3_4_0.RecognitionException;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
}

@members{
	private de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolverFactory tokenResolverFactory = new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenResolverFactory();
	
	/**
	 * the index of the last token that was handled by collectHiddenTokens()
	 */
	private int lastPosition;
	
	/**
	 * A flag that indicates whether the parser should remember all expected elements.
	 * This flag is set to true when using the parse for code completion. Otherwise it
	 * is set to false.
	 */
	private boolean rememberExpectedElements = false;
	
	private Object parseToIndexTypeObject;
	private int lastTokenIndex = 0;
	
	/**
	 * A list of expected elements the were collected while parsing the input stream.
	 * This list is only filled if <code>rememberExpectedElements</code> is set to
	 * true.
	 */
	private List<de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal> expectedElements = new ArrayList<de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal>();
	
	private int mismatchedTokenRecoveryTries = 0;
	/**
	 * A helper list to allow a lexer to pass errors to its parser
	 */
	protected List<RecognitionException> lexerExceptions = Collections.synchronizedList(new ArrayList<RecognitionException>());
	
	/**
	 * Another helper list to allow a lexer to pass positions of errors to its parser
	 */
	protected List<Integer> lexerExceptionPositions = Collections.synchronizedList(new ArrayList<Integer>());
	
	/**
	 * A stack for incomplete objects. This stack is used filled when the parser is
	 * used for code completion. Whenever the parser starts to read an object it is
	 * pushed on the stack. Once the element was parser completely it is popped from
	 * the stack.
	 */
	List<EObject> incompleteObjects = new ArrayList<EObject>();
	
	private int stopIncludingHiddenTokens;
	private int stopExcludingHiddenTokens;
	private int tokenIndexOfLastCompleteElement;
	
	private int expectedElementsIndexOfLastCompleteElement;
	
	/**
	 * The offset indicating the cursor position when the parser is used for code
	 * completion by calling parseToExpectedElements().
	 */
	private int cursorOffset;
	
	/**
	 * The offset of the first hidden token of the last expected element. This offset
	 * is used to discard expected elements, which are not needed for code completion.
	 */
	private int lastStartIncludingHidden;
	
	private de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcLocationMap locationMap;
	
	private de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcSyntaxErrorMessageConverter syntaxErrorMessageConverter = new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcSyntaxErrorMessageConverter(tokenNames);
	
	@Override
	public void reportError(RecognitionException re) {
		addErrorToResource(syntaxErrorMessageConverter.translateParseError(re));
	}
	
	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
		postParseCommands.add(new de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcCommand<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource>() {
			public boolean execute(de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource resource) {
				if (resource == null) {
					// the resource can be null if the parser is used for code completion
					return true;
				}
				resource.addProblem(new de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcProblem() {
					public de.darwinspl.constraint.resource.tfm_ctc.Tfm_ctcEProblemSeverity getSeverity() {
						return de.darwinspl.constraint.resource.tfm_ctc.Tfm_ctcEProblemSeverity.ERROR;
					}
					public de.darwinspl.constraint.resource.tfm_ctc.Tfm_ctcEProblemType getType() {
						return de.darwinspl.constraint.resource.tfm_ctc.Tfm_ctcEProblemType.SYNTAX_ERROR;
					}
					public String getMessage() {
						return errorMessage;
					}
					public Collection<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcQuickFix> getQuickFixes() {
						return null;
					}
				}, column, line, startIndex, stopIndex);
				return true;
			}
		});
	}
	
	protected void addErrorToResource(de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcLocalizedMessage message) {
		if (message == null) {
			return;
		}
		addErrorToResource(message.getMessage(), message.getColumn(), message.getLine(), message.getCharStart(), message.getCharEnd());
	}
	
	public void addExpectedElement(EClass eClass, int expectationStartIndex, int expectationEndIndex) {
		for (int expectationIndex = expectationStartIndex; expectationIndex <= expectationEndIndex; expectationIndex++) {
			addExpectedElement(eClass, de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectationConstants.EXPECTATIONS[expectationIndex]);
		}
	}
	
	public void addExpectedElement(EClass eClass, int expectationIndex) {
		addExpectedElement(eClass, de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectationConstants.EXPECTATIONS[expectationIndex]);
	}
	
	public void addExpectedElement(EClass eClass, int[] ids) {
		if (!this.rememberExpectedElements) {
			return;
		}
		int terminalID = ids[0];
		int followSetID = ids[1];
		de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcExpectedElement terminal = de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcFollowSetProvider.TERMINALS[terminalID];
		de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcContainedFeature[] containmentFeatures = new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcContainedFeature[ids.length - 2];
		for (int i = 2; i < ids.length; i++) {
			containmentFeatures[i - 2] = de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcFollowSetProvider.LINKS[ids[i]];
		}
		de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainmentTrace containmentTrace = new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainmentTrace(eClass, containmentFeatures);
		EObject container = getLastIncompleteElement();
		de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal expectedElement = new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal(container, terminal, followSetID, containmentTrace);
		setPosition(expectedElement, input.index());
		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
		lastStartIncludingHidden = startIncludingHiddenTokens;
		this.expectedElements.add(expectedElement);
	}
	
	protected void collectHiddenTokens(EObject element) {
	}
	
	protected void copyLocalizationInfos(final EObject source, final EObject target) {
		if (disableLocationMap) {
			return;
		}
		final de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcLocationMap locationMap = this.locationMap;
		if (locationMap == null) {
			// the locationMap can be null if the parser is used for code completion
			return;
		}
		postParseCommands.add(new de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcCommand<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource>() {
			public boolean execute(de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource resource) {
				locationMap.setCharStart(target, locationMap.getCharStart(source));
				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
				locationMap.setColumn(target, locationMap.getColumn(source));
				locationMap.setLine(target, locationMap.getLine(source));
				return true;
			}
		});
	}
	
	protected void copyLocalizationInfos(final CommonToken source, final EObject target) {
		if (disableLocationMap) {
			return;
		}
		final de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcLocationMap locationMap = this.locationMap;
		if (locationMap == null) {
			// the locationMap can be null if the parser is used for code completion
			return;
		}
		postParseCommands.add(new de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcCommand<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource>() {
			public boolean execute(de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource resource) {
				if (source == null) {
					return true;
				}
				locationMap.setCharStart(target, source.getStartIndex());
				locationMap.setCharEnd(target, source.getStopIndex());
				locationMap.setColumn(target, source.getCharPositionInLine());
				locationMap.setLine(target, source.getLine());
				return true;
			}
		});
	}
	
	/**
	 * Sets the end character index and the last line for the given object in the
	 * location map.
	 */
	protected void setLocalizationEnd(Collection<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcCommand<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource>> postParseCommands , final EObject object, final int endChar, final int endLine) {
		if (disableLocationMap) {
			return;
		}
		final de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcLocationMap locationMap = this.locationMap;
		if (locationMap == null) {
			// the locationMap can be null if the parser is used for code completion
			return;
		}
		postParseCommands.add(new de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcCommand<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource>() {
			public boolean execute(de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource resource) {
				locationMap.setCharEnd(object, endChar);
				locationMap.setLine(object, endLine);
				return true;
			}
		});
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextParser createInstance(InputStream actualInputStream, String encoding) {
		try {
			if (encoding == null) {
				return new Tfm_ctcParser(new CommonTokenStream(new Tfm_ctcLexer(new ANTLRInputStream(actualInputStream))));
			} else {
				return new Tfm_ctcParser(new CommonTokenStream(new Tfm_ctcLexer(new ANTLRInputStream(actualInputStream, encoding))));
			}
		} catch (IOException e) {
			new de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcRuntimeUtil().logError("Error while creating parser.", e);
			return null;
		}
	}
	
	/**
	 * This default constructor is only used to call createInstance() on it.
	 */
	public Tfm_ctcParser() {
		super(null);
	}
	
	protected EObject doParse() throws RecognitionException {
		this.lastPosition = 0;
		// required because the lexer class can not be subclassed
		((Tfm_ctcLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
		((Tfm_ctcLexer) getTokenStream().getTokenSource()).lexerExceptionPositions = lexerExceptionPositions;
		Object typeObject = getTypeObject();
		if (typeObject == null) {
			return start();
		} else if (typeObject instanceof EClass) {
			EClass type = (EClass) typeObject;
			if (type.getInstanceClass() == de.darwinspl.constraint.DwConstraintModel.class) {
				return parse_de_darwinspl_constraint_DwConstraintModel();
			}
			if (type.getInstanceClass() == de.darwinspl.constraint.DwConstraint.class) {
				return parse_de_darwinspl_constraint_DwConstraint();
			}
			if (type.getInstanceClass() == de.darwinspl.temporal.DwTemporalInterval.class) {
				return parse_de_darwinspl_temporal_DwTemporalInterval();
			}
		}
		throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcUnexpectedContentTypeException(typeObject);
	}
	
	public int getMismatchedTokenRecoveryTries() {
		return mismatchedTokenRecoveryTries;
	}
	
	public Object getMissingSymbol(IntStream arg0, RecognitionException arg1, int arg2, BitSet arg3) {
		mismatchedTokenRecoveryTries++;
		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
	}
	
	public Object getParseToIndexTypeObject() {
		return parseToIndexTypeObject;
	}
	
	protected Object getTypeObject() {
		Object typeObject = getParseToIndexTypeObject();
		if (typeObject != null) {
			return typeObject;
		}
		Map<?,?> options = getOptions();
		if (options != null) {
			typeObject = options.get(de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcOptions.RESOURCE_CONTENT_TYPE);
		}
		return typeObject;
	}
	
	/**
	 * Implementation that calls {@link #doParse()} and handles the thrown
	 * RecognitionExceptions.
	 */
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcParseResult parse() {
		// Reset parser state
		terminateParsing = false;
		postParseCommands = new ArrayList<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcCommand<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource>>();
		de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcParseResult parseResult = new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcParseResult();
		if (disableLocationMap) {
			locationMap = new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcDevNullLocationMap();
		} else {
			locationMap = new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcLocationMap();
		}
		// Run parser
		try {
			EObject result =  doParse();
			if (lexerExceptions.isEmpty()) {
				parseResult.setRoot(result);
				parseResult.setLocationMap(locationMap);
			}
		} catch (RecognitionException re) {
			addErrorToResource(syntaxErrorMessageConverter.translateParseError(re));
		} catch (IllegalArgumentException iae) {
			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
				// can be caused if a null is set on EMF models where not allowed. this will just
				// happen if other errors occurred before
			} else {
				iae.printStackTrace();
			}
		}
		for (RecognitionException re : lexerExceptions) {
			addErrorToResource(syntaxErrorMessageConverter.translateLexicalError(re, lexerExceptions, lexerExceptionPositions));
		}
		parseResult.getPostParseCommands().addAll(postParseCommands);
		return parseResult;
	}
	
	public List<de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal> parseToExpectedElements(EClass type, de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource dummyResource, int cursorOffset) {
		this.rememberExpectedElements = true;
		this.parseToIndexTypeObject = type;
		this.cursorOffset = cursorOffset;
		this.lastStartIncludingHidden = -1;
		final CommonTokenStream tokenStream = (CommonTokenStream) getTokenStream();
		de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcParseResult result = parse();
		for (EObject incompleteObject : incompleteObjects) {
			Lexer lexer = (Lexer) tokenStream.getTokenSource();
			int endChar = lexer.getCharIndex();
			int endLine = lexer.getLine();
			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
		}
		if (result != null) {
			EObject root = result.getRoot();
			if (root != null) {
				dummyResource.getContentsInternal().add(root);
			}
			for (de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcCommand<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource> command : result.getPostParseCommands()) {
				command.execute(dummyResource);
			}
		}
		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
		Set<de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal> currentFollowSet = new LinkedHashSet<de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal>();
		List<de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal> newFollowSet = new ArrayList<de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal>();
		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
			de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal expectedElementI = expectedElements.get(i);
			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
				currentFollowSet.add(expectedElementI);
			} else {
				break;
			}
		}
		int followSetID = 36;
		int i;
		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
			CommonToken nextToken = (CommonToken) tokenStream.get(i);
			if (nextToken.getType() < 0) {
				break;
			}
			if (nextToken.getChannel() == 99) {
				// hidden tokens do not reduce the follow set
			} else {
				// now that we have found the next visible token the position for that expected
				// terminals can be set
				for (de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal nextFollow : newFollowSet) {
					lastTokenIndex = 0;
					setPosition(nextFollow, i);
				}
				newFollowSet.clear();
				// normal tokens do reduce the follow set - only elements that match the token are
				// kept
				for (de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal nextFollow : currentFollowSet) {
					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
						// keep this one - it matches
						Collection<de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcPair<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcExpectedElement, de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcContainedFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
						for (de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcPair<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcExpectedElement, de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcContainedFeature[]> newFollowerPair : newFollowers) {
							de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcExpectedElement newFollower = newFollowerPair.getLeft();
							EObject container = getLastIncompleteElement();
							de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainmentTrace containmentTrace = new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcContainmentTrace(null, newFollowerPair.getRight());
							de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal newFollowTerminal = new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal(container, newFollower, followSetID, containmentTrace);
							newFollowSet.add(newFollowTerminal);
							expectedElements.add(newFollowTerminal);
						}
					}
				}
				currentFollowSet.clear();
				currentFollowSet.addAll(newFollowSet);
			}
			followSetID++;
		}
		// after the last token in the stream we must set the position for the elements
		// that were added during the last iteration of the loop
		for (de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal nextFollow : newFollowSet) {
			lastTokenIndex = 0;
			setPosition(nextFollow, i);
		}
		return this.expectedElements;
	}
	
	public void setPosition(de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcExpectedTerminal expectedElement, int tokenIndex) {
		int currentIndex = Math.max(0, tokenIndex);
		for (int index = lastTokenIndex; index < currentIndex; index++) {
			if (index >= input.size()) {
				break;
			}
			CommonToken tokenAtIndex = (CommonToken) input.get(index);
			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			if (tokenAtIndex.getChannel() != 99 && !anonymousTokens.contains(tokenAtIndex)) {
				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			}
		}
		lastTokenIndex = Math.max(0, currentIndex);
		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
	}
	
	public Object recoverFromMismatchedToken(IntStream input, int ttype, BitSet follow) throws RecognitionException {
		if (!rememberExpectedElements) {
			return super.recoverFromMismatchedToken(input, ttype, follow);
		} else {
			return null;
		}
	}
	
	private void startIncompleteElement(Object object) {
		if (object instanceof EObject) {
			this.incompleteObjects.add((EObject) object);
		}
	}
	
	private void completedElement(Object object, boolean isContainment) {
		if (isContainment && !this.incompleteObjects.isEmpty()) {
			this.incompleteObjects.remove(object);
		}
		if (object instanceof EObject) {
			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
		}
	}
	
	private EObject getLastIncompleteElement() {
		if (incompleteObjects.isEmpty()) {
			return null;
		}
		return incompleteObjects.get(incompleteObjects.size() - 1);
	}
	
}

start returns [ EObject element = null]
:
	{
		// follow set for start rule(s)
		addExpectedElement(null, 0);
		expectedElementsIndexOfLastCompleteElement = 0;
	}
	(
		c0 = parse_de_darwinspl_constraint_DwConstraintModel{ element = c0; }
	)
	EOF	{
		retrieveLayoutInformation(element, null, null, false);
	}
	
;

parse_de_darwinspl_constraint_DwConstraintModel returns [de.darwinspl.constraint.DwConstraintModel element = null]
@init{
}
:
	a0 = 'feature' {
		if (element == null) {
			element = de.darwinspl.constraint.DwConstraintFactory.eINSTANCE.createDwConstraintModel();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_0_0_0_0, null, true);
		copyLocalizationInfos((CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, 1);
	}
	
	a1 = 'model' {
		if (element == null) {
			element = de.darwinspl.constraint.DwConstraintFactory.eINSTANCE.createDwConstraintModel();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_0_0_0_1, null, true);
		copyLocalizationInfos((CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, 2);
	}
	
	(
		a2 = PATH_TO_FILE		
		{
			if (terminateParsing) {
				throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.constraint.DwConstraintFactory.eINSTANCE.createDwConstraintModel();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("PATH_TO_FILE");
				tokenResolver.setOptions(getOptions());
				de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT_MODEL__FEATURE_MODEL), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((CommonToken) a2).getLine(), ((CommonToken) a2).getCharPositionInLine(), ((CommonToken) a2).getStartIndex(), ((CommonToken) a2).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				de.darwinspl.feature.DwTemporalFeatureModel proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwTemporalFeatureModel();
				collectHiddenTokens(element);
				registerContextDependentProxy(new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcContextDependentURIFragmentFactory<de.darwinspl.constraint.DwConstraintModel, de.darwinspl.feature.DwTemporalFeatureModel>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getDwConstraintModelFeatureModelReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT_MODEL__FEATURE_MODEL), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT_MODEL__FEATURE_MODEL), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_0_0_0_2, proxy, true);
				copyLocalizationInfos((CommonToken) a2, element);
				copyLocalizationInfos((CommonToken) a2, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(), 3, 327);
	}
	
	(
		(
			(
				a3_0 = parse_de_darwinspl_constraint_DwConstraint				{
					if (terminateParsing) {
						throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
					}
					if (element == null) {
						element = de.darwinspl.constraint.DwConstraintFactory.eINSTANCE.createDwConstraintModel();
						startIncompleteElement(element);
					}
					if (a3_0 != null) {
						if (a3_0 != null) {
							Object value = a3_0;
							addObjectToList(element, de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT_MODEL__CONSTRAINTS, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_0_0_0_5_0_0_0, a3_0, true);
						copyLocalizationInfos(a3_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				// We've found the last token for this rule. The constructed EObject is now
				// complete.
				completedElement(element, true);
				addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(), 328, 652);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(), 653, 977);
	}
	
;

parse_de_darwinspl_constraint_DwConstraint returns [de.darwinspl.constraint.DwConstraint element = null]
@init{
}
:
	(
		a0_0 = parse_de_darwinspl_expression_DwExpression		{
			if (terminateParsing) {
				throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.constraint.DwConstraintFactory.eINSTANCE.createDwConstraint();
				startIncompleteElement(element);
			}
			if (a0_0 != null) {
				if (a0_0 != null) {
					Object value = a0_0;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT__ROOT_EXPRESSION), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_1_0_0_0, a0_0, true);
				copyLocalizationInfos(a0_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraint(), 978);
		addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(), 979, 1303);
	}
	
	(
		(
			(
				a1_0 = parse_de_darwinspl_temporal_DwTemporalInterval				{
					if (terminateParsing) {
						throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
					}
					if (element == null) {
						element = de.darwinspl.constraint.DwConstraintFactory.eINSTANCE.createDwConstraint();
						startIncompleteElement(element);
					}
					if (a1_0 != null) {
						if (a1_0 != null) {
							Object value = a1_0;
							element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT__VALIDITY), value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_1_0_0_1_0_0_0, a1_0, true);
						copyLocalizationInfos(a1_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				// We've found the last token for this rule. The constructed EObject is now
				// complete.
				completedElement(element, true);
				addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(), 1304, 1628);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(), 1629, 1953);
	}
	
;

parse_de_darwinspl_temporal_DwTemporalInterval returns [de.darwinspl.temporal.DwTemporalInterval element = null]
@init{
}
:
	a0 = '[' {
		if (element == null) {
			element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_0, null, true);
		copyLocalizationInfos((CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, 1954, 1957);
	}
	
	(
		(
			a1 = DATE			
			{
				if (terminateParsing) {
					throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
				}
				if (element == null) {
					element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
					startIncompleteElement(element);
				}
				if (a1 != null) {
					de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("DATE");
					tokenResolver.setOptions(getOptions());
					de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolveResult result = getFreshTokenResolveResult();
					tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__FROM), result);
					Object resolvedObject = result.getResolvedToken();
					if (resolvedObject == null) {
						addErrorToResource(result.getErrorMessage(), ((CommonToken) a1).getLine(), ((CommonToken) a1).getCharPositionInLine(), ((CommonToken) a1).getStartIndex(), ((CommonToken) a1).getStopIndex());
					}
					java.util.Date resolved = (java.util.Date) resolvedObject;
					if (resolved != null) {
						Object value = resolved;
						element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__FROM), value);
						completedElement(value, false);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_1_0_0_0, resolved, true);
					copyLocalizationInfos((CommonToken) a1, element);
				}
			}
		)
		{
			// expected elements (follow set)
			addExpectedElement(null, 1958);
		}
		
		a2 = '-' {
			if (element == null) {
				element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
				startIncompleteElement(element);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_1_0_0_1, null, true);
			copyLocalizationInfos((CommonToken)a2, element);
		}
		{
			// expected elements (follow set)
			addExpectedElement(null, 1959);
		}
		
		(
			a3 = DATE			
			{
				if (terminateParsing) {
					throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
				}
				if (element == null) {
					element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
					startIncompleteElement(element);
				}
				if (a3 != null) {
					de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("DATE");
					tokenResolver.setOptions(getOptions());
					de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolveResult result = getFreshTokenResolveResult();
					tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__TO), result);
					Object resolvedObject = result.getResolvedToken();
					if (resolvedObject == null) {
						addErrorToResource(result.getErrorMessage(), ((CommonToken) a3).getLine(), ((CommonToken) a3).getCharPositionInLine(), ((CommonToken) a3).getStartIndex(), ((CommonToken) a3).getStopIndex());
					}
					java.util.Date resolved = (java.util.Date) resolvedObject;
					if (resolved != null) {
						Object value = resolved;
						element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__TO), value);
						completedElement(value, false);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_1_0_0_2, resolved, true);
					copyLocalizationInfos((CommonToken) a3, element);
				}
			}
		)
		{
			// expected elements (follow set)
			addExpectedElement(null, 1960);
		}
		
		
		|		(
			a4 = DATE			
			{
				if (terminateParsing) {
					throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
				}
				if (element == null) {
					element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
					startIncompleteElement(element);
				}
				if (a4 != null) {
					de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("DATE");
					tokenResolver.setOptions(getOptions());
					de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolveResult result = getFreshTokenResolveResult();
					tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__FROM), result);
					Object resolvedObject = result.getResolvedToken();
					if (resolvedObject == null) {
						addErrorToResource(result.getErrorMessage(), ((CommonToken) a4).getLine(), ((CommonToken) a4).getCharPositionInLine(), ((CommonToken) a4).getStartIndex(), ((CommonToken) a4).getStopIndex());
					}
					java.util.Date resolved = (java.util.Date) resolvedObject;
					if (resolved != null) {
						Object value = resolved;
						element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__FROM), value);
						completedElement(value, false);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_1_0_1_0, resolved, true);
					copyLocalizationInfos((CommonToken) a4, element);
				}
			}
		)
		{
			// expected elements (follow set)
			addExpectedElement(null, 1961);
		}
		
		a5 = '-' {
			if (element == null) {
				element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
				startIncompleteElement(element);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_1_0_1_1, null, true);
			copyLocalizationInfos((CommonToken)a5, element);
		}
		{
			// expected elements (follow set)
			addExpectedElement(null, 1962);
		}
		
		a6 = 'eternity' {
			if (element == null) {
				element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
				startIncompleteElement(element);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_1_0_1_2, null, true);
			copyLocalizationInfos((CommonToken)a6, element);
		}
		{
			// expected elements (follow set)
			addExpectedElement(null, 1963);
		}
		
		
		|		a7 = 'initial state' {
			if (element == null) {
				element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
				startIncompleteElement(element);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_1_0_2_0, null, true);
			copyLocalizationInfos((CommonToken)a7, element);
		}
		{
			// expected elements (follow set)
			addExpectedElement(null, 1964);
		}
		
		a8 = '-' {
			if (element == null) {
				element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
				startIncompleteElement(element);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_1_0_2_1, null, true);
			copyLocalizationInfos((CommonToken)a8, element);
		}
		{
			// expected elements (follow set)
			addExpectedElement(null, 1965);
		}
		
		(
			a9 = DATE			
			{
				if (terminateParsing) {
					throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
				}
				if (element == null) {
					element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
					startIncompleteElement(element);
				}
				if (a9 != null) {
					de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("DATE");
					tokenResolver.setOptions(getOptions());
					de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolveResult result = getFreshTokenResolveResult();
					tokenResolver.resolve(a9.getText(), element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__TO), result);
					Object resolvedObject = result.getResolvedToken();
					if (resolvedObject == null) {
						addErrorToResource(result.getErrorMessage(), ((CommonToken) a9).getLine(), ((CommonToken) a9).getCharPositionInLine(), ((CommonToken) a9).getStartIndex(), ((CommonToken) a9).getStopIndex());
					}
					java.util.Date resolved = (java.util.Date) resolvedObject;
					if (resolved != null) {
						Object value = resolved;
						element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__TO), value);
						completedElement(value, false);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_1_0_2_2, resolved, true);
					copyLocalizationInfos((CommonToken) a9, element);
				}
			}
		)
		{
			// expected elements (follow set)
			addExpectedElement(null, 1966);
		}
		
		
		|		a10 = 'initial state' {
			if (element == null) {
				element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
				startIncompleteElement(element);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_1_0_3_0, null, true);
			copyLocalizationInfos((CommonToken)a10, element);
		}
		{
			// expected elements (follow set)
			addExpectedElement(null, 1967);
		}
		
		a11 = '-' {
			if (element == null) {
				element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
				startIncompleteElement(element);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_1_0_3_1, null, true);
			copyLocalizationInfos((CommonToken)a11, element);
		}
		{
			// expected elements (follow set)
			addExpectedElement(null, 1968);
		}
		
		a12 = 'eternity' {
			if (element == null) {
				element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
				startIncompleteElement(element);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_1_0_3_2, null, true);
			copyLocalizationInfos((CommonToken)a12, element);
		}
		{
			// expected elements (follow set)
			addExpectedElement(null, 1969);
		}
		
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, 1970);
	}
	
	a13 = ']' {
		if (element == null) {
			element = de.darwinspl.temporal.DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.TFM_CTC_2_0_0_2, null, true);
		copyLocalizationInfos((CommonToken)a13, element);
	}
	{
		// expected elements (follow set)
		// We've found the last token for this rule. The constructed EObject is now
		// complete.
		completedElement(element, true);
		addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(), 1971, 2295);
	}
	
;

parseop_DwExpression_level_0 returns [de.darwinspl.expression.DwExpression element = null]
@init{
}
:
	leftArg = parseop_DwExpression_level_1	((
		()
		{ element = null; }
		a0 = '<->' {
			if (element == null) {
				element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwEquivalenceExpression();
				startIncompleteElement(element);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_0_0_0_1, null, true);
			copyLocalizationInfos((CommonToken)a0, element);
		}
		{
			// expected elements (follow set)
			addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwEquivalenceExpression(), 2296, 2620);
		}
		
		rightArg = parseop_DwExpression_level_1		{
			if (terminateParsing) {
				throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwEquivalenceExpression();
				startIncompleteElement(element);
			}
			if (leftArg != null) {
				if (leftArg != null) {
					Object value = leftArg;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_EQUIVALENCE_EXPRESSION__OPERAND1), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_0_0_0_0, leftArg, true);
				copyLocalizationInfos(leftArg, element);
			}
		}
		{
			if (terminateParsing) {
				throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
			}
			if (element == null) {
				element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwEquivalenceExpression();
				startIncompleteElement(element);
			}
			if (rightArg != null) {
				if (rightArg != null) {
					Object value = rightArg;
					element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_EQUIVALENCE_EXPRESSION__OPERAND2), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_0_0_0_2, rightArg, true);
				copyLocalizationInfos(rightArg, element);
			}
		}
		{ leftArg = element; /* this may become an argument in the next iteration */ }
	)+ | /* epsilon */ { element = leftArg; }
	
)
;

parseop_DwExpression_level_1 returns [de.darwinspl.expression.DwExpression element = null]
@init{
}
:
leftArg = parseop_DwExpression_level_2((
	()
	{ element = null; }
	a0 = '->' {
		if (element == null) {
			element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwImpliesExpression();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_1_0_0_1, null, true);
		copyLocalizationInfos((CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwImpliesExpression(), 2621, 2945);
	}
	
	rightArg = parseop_DwExpression_level_2	{
		if (terminateParsing) {
			throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
		}
		if (element == null) {
			element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwImpliesExpression();
			startIncompleteElement(element);
		}
		if (leftArg != null) {
			if (leftArg != null) {
				Object value = leftArg;
				element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_IMPLIES_EXPRESSION__OPERAND1), value);
				completedElement(value, true);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_1_0_0_0, leftArg, true);
			copyLocalizationInfos(leftArg, element);
		}
	}
	{
		if (terminateParsing) {
			throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
		}
		if (element == null) {
			element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwImpliesExpression();
			startIncompleteElement(element);
		}
		if (rightArg != null) {
			if (rightArg != null) {
				Object value = rightArg;
				element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_IMPLIES_EXPRESSION__OPERAND2), value);
				completedElement(value, true);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_1_0_0_2, rightArg, true);
			copyLocalizationInfos(rightArg, element);
		}
	}
	{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_DwExpression_level_2 returns [de.darwinspl.expression.DwExpression element = null]
@init{
}
:
leftArg = parseop_DwExpression_level_3((
()
{ element = null; }
a0 = '||' {
	if (element == null) {
		element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwOrExpression();
		startIncompleteElement(element);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_2_0_0_1, null, true);
	copyLocalizationInfos((CommonToken)a0, element);
}
{
	// expected elements (follow set)
	addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwOrExpression(), 2946, 3270);
}

rightArg = parseop_DwExpression_level_3{
	if (terminateParsing) {
		throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
	}
	if (element == null) {
		element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwOrExpression();
		startIncompleteElement(element);
	}
	if (leftArg != null) {
		if (leftArg != null) {
			Object value = leftArg;
			element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_OR_EXPRESSION__OPERAND1), value);
			completedElement(value, true);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_2_0_0_0, leftArg, true);
		copyLocalizationInfos(leftArg, element);
	}
}
{
	if (terminateParsing) {
		throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
	}
	if (element == null) {
		element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwOrExpression();
		startIncompleteElement(element);
	}
	if (rightArg != null) {
		if (rightArg != null) {
			Object value = rightArg;
			element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_OR_EXPRESSION__OPERAND2), value);
			completedElement(value, true);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_2_0_0_2, rightArg, true);
		copyLocalizationInfos(rightArg, element);
	}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_DwExpression_level_3 returns [de.darwinspl.expression.DwExpression element = null]
@init{
}
:
leftArg = parseop_DwExpression_level_14((
()
{ element = null; }
a0 = '&&' {
if (element == null) {
	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwAndExpression();
	startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_3_0_0_1, null, true);
copyLocalizationInfos((CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwAndExpression(), 3271, 3595);
}

rightArg = parseop_DwExpression_level_14{
if (terminateParsing) {
	throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
}
if (element == null) {
	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwAndExpression();
	startIncompleteElement(element);
}
if (leftArg != null) {
	if (leftArg != null) {
		Object value = leftArg;
		element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_AND_EXPRESSION__OPERAND1), value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_3_0_0_0, leftArg, true);
	copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
	throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
}
if (element == null) {
	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwAndExpression();
	startIncompleteElement(element);
}
if (rightArg != null) {
	if (rightArg != null) {
		Object value = rightArg;
		element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_AND_EXPRESSION__OPERAND2), value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_3_0_0_2, rightArg, true);
	copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_DwExpression_level_14 returns [de.darwinspl.expression.DwExpression element = null]
@init{
}
:
a0 = '!' {
if (element == null) {
element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwNotExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_4_0_0_0, null, true);
copyLocalizationInfos((CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwNotExpression(), 3596, 3920);
}

arg = parseop_DwExpression_level_15{
if (terminateParsing) {
throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
}
if (element == null) {
element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwNotExpression();
startIncompleteElement(element);
}
if (arg != null) {
if (arg != null) {
Object value = arg;
element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_NOT_EXPRESSION__OPERAND), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_4_0_0_1, arg, true);
copyLocalizationInfos(arg, element);
}
}
|

arg = parseop_DwExpression_level_15{ element = arg; }
;

parseop_DwExpression_level_15 returns [de.darwinspl.expression.DwExpression element = null]
@init{
}
:
c0 = parse_de_darwinspl_expression_DwNestedExpression{ element = c0; /* this is a subclass or primitive expression choice */ }
|c1 = parse_de_darwinspl_expression_DwFeatureReferenceExpression{ element = c1; /* this is a subclass or primitive expression choice */ }
|c2 = parse_de_darwinspl_expression_DwBooleanValueExpression{ element = c2; /* this is a subclass or primitive expression choice */ }
;

parse_de_darwinspl_expression_DwNestedExpression returns [de.darwinspl.expression.DwNestedExpression element = null]
@init{
}
:
a0 = '(' {
if (element == null) {
element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwNestedExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_5_0_0_0, null, true);
copyLocalizationInfos((CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwNestedExpression(), 3921, 4245);
}

(
a1_0 = parse_de_darwinspl_expression_DwExpression{
if (terminateParsing) {
throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
}
if (element == null) {
element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwNestedExpression();
startIncompleteElement(element);
}
if (a1_0 != null) {
if (a1_0 != null) {
	Object value = a1_0;
	element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_NESTED_EXPRESSION__OPERAND), value);
	completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_5_0_0_1, a1_0, true);
copyLocalizationInfos(a1_0, element);
}
}
)
{
// expected elements (follow set)
addExpectedElement(null, 4246);
}

a2 = ')' {
if (element == null) {
element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwNestedExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_5_0_0_2, null, true);
copyLocalizationInfos((CommonToken)a2, element);
}
{
// expected elements (follow set)
// We've found the last token for this rule. The constructed EObject is now
// complete.
completedElement(element, true);
addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraint(), 4247);
addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(), 4248, 4572);
addExpectedElement(null, 4573, 4577);
}

;

parse_de_darwinspl_expression_DwFeatureReferenceExpression returns [de.darwinspl.expression.DwFeatureReferenceExpression element = null]
@init{
}
:
(
(
a0 = QUOTED_34_34
{
if (terminateParsing) {
	throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
}
if (element == null) {
	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwFeatureReferenceExpression();
	startIncompleteElement(element);
}
if (a0 != null) {
	de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
	tokenResolver.setOptions(getOptions());
	de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolveResult result = getFreshTokenResolveResult();
	tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), result);
	Object resolvedObject = result.getResolvedToken();
	if (resolvedObject == null) {
		addErrorToResource(result.getErrorMessage(), ((CommonToken) a0).getLine(), ((CommonToken) a0).getCharPositionInLine(), ((CommonToken) a0).getStartIndex(), ((CommonToken) a0).getStopIndex());
	}
	String resolved = (String) resolvedObject;
	de.darwinspl.feature.DwFeature proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwFeature();
	collectHiddenTokens(element);
	registerContextDependentProxy(new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcContextDependentURIFragmentFactory<de.darwinspl.expression.DwFeatureReferenceExpression, de.darwinspl.feature.DwFeature>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getDwFeatureReferenceExpressionFeatureReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), resolved, proxy);
	if (proxy != null) {
		Object value = proxy;
		element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), value);
		completedElement(value, false);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_6_0_0_0_0_0_0, proxy, true);
	copyLocalizationInfos((CommonToken) a0, element);
	copyLocalizationInfos((CommonToken) a0, proxy);
}
}
)
{
// expected elements (follow set)
addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraint(), 4578);
addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(), 4579, 4903);
addExpectedElement(null, 4904, 4908);
}


|(
a1 = IDENTIFIER_TOKEN
{
if (terminateParsing) {
	throw new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTerminateParsingException();
}
if (element == null) {
	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwFeatureReferenceExpression();
	startIncompleteElement(element);
}
if (a1 != null) {
	de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER_TOKEN");
	tokenResolver.setOptions(getOptions());
	de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolveResult result = getFreshTokenResolveResult();
	tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), result);
	Object resolvedObject = result.getResolvedToken();
	if (resolvedObject == null) {
		addErrorToResource(result.getErrorMessage(), ((CommonToken) a1).getLine(), ((CommonToken) a1).getCharPositionInLine(), ((CommonToken) a1).getStartIndex(), ((CommonToken) a1).getStopIndex());
	}
	String resolved = (String) resolvedObject;
	de.darwinspl.feature.DwFeature proxy = de.darwinspl.feature.DwFeatureFactory.eINSTANCE.createDwFeature();
	collectHiddenTokens(element);
	registerContextDependentProxy(new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcContextDependentURIFragmentFactory<de.darwinspl.expression.DwFeatureReferenceExpression, de.darwinspl.feature.DwFeature>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getDwFeatureReferenceExpressionFeatureReferenceResolver()), element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), resolved, proxy);
	if (proxy != null) {
		Object value = proxy;
		element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), value);
		completedElement(value, false);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_6_0_0_0_0_1_0, proxy, true);
	copyLocalizationInfos((CommonToken) a1, element);
	copyLocalizationInfos((CommonToken) a1, proxy);
}
}
)
{
// expected elements (follow set)
// We've found the last token for this rule. The constructed EObject is now
// complete.
completedElement(element, true);
addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraint(), 4909);
addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(), 4910, 5234);
addExpectedElement(null, 5235, 5239);
}

)
{
// expected elements (follow set)
// We've found the last token for this rule. The constructed EObject is now
// complete.
completedElement(element, true);
addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraint(), 5240);
addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(), 5241, 5565);
addExpectedElement(null, 5566, 5570);
}

;

parse_de_darwinspl_expression_DwBooleanValueExpression returns [de.darwinspl.expression.DwBooleanValueExpression element = null]
@init{
}
:
(
(
a0 = 'true' {
if (element == null) {
	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwBooleanValueExpression();
	startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_7_0_0_0, true, true);
copyLocalizationInfos((CommonToken)a0, element);
// set value of boolean attribute
Object value = true;
element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_BOOLEAN_VALUE_EXPRESSION__VALUE), value);
completedElement(value, false);
}
|a1 = 'false' {
if (element == null) {
	element = de.darwinspl.expression.DwExpressionFactory.eINSTANCE.createDwBooleanValueExpression();
	startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcGrammarInformationProvider.DWEXPRESSION_7_0_0_0, false, true);
copyLocalizationInfos((CommonToken)a1, element);
// set value of boolean attribute
Object value = false;
element.eSet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_BOOLEAN_VALUE_EXPRESSION__VALUE), value);
completedElement(value, false);
}
)
)
{
// expected elements (follow set)
// We've found the last token for this rule. The constructed EObject is now
// complete.
completedElement(element, true);
addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraint(), 5571);
addExpectedElement(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(), 5572, 5896);
addExpectedElement(null, 5897, 5901);
}

;

parse_de_darwinspl_expression_DwExpression returns [de.darwinspl.expression.DwExpression element = null]
:
c = parseop_DwExpression_level_0{ element = c; /* this rule is an expression root */ }

;

PATH_TO_FILE:
('<' ('A'..'Z'|'a'..'z'|'0'..'9'|'_'|' '|'/')+ '.tfm>')
;
DATE:
((('0'..'9')+ '/'('0'..'9')+ '/'('0'..'9')+ ( 'T'('0'..'9')+ ':'('0'..'9')+ (':' ('0'..'9')+ )?)?))
;
INTEGER_LITERAL:
(('0'..'9')+ )
{ _channel = 99; }
;
IDENTIFIER_TOKEN:
(('A'..'Z'|'a'..'z'|'_')('A'..'Z'|'a'..'z'|'_'|'0'..'9')*)
;
SL_COMMENT:
('//'(~('\n'|'\r'|'\uffff'))*)
{ _channel = 99; }
;
ML_COMMENT:
('/*'.*'*/')
{ _channel = 99; }
;
LINEBREAK:
(('\r\n'|'\r'|'\n'))
{ _channel = 99; }
;
WHITESPACE:
((' '|'\t'|'\f'))
{ _channel = 99; }
;
QUOTED_34_34:
(('"')(~('"'))*('"'))
;

