/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.util;

import java.io.File;
import java.util.Map;
import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;

/**
 * Class Tfm_ctcTextResourceUtil can be used to perform common tasks on text
 * resources, such as loading and saving resources, as well as, checking them for
 * errors. This class is deprecated and has been replaced by
 * de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcResourceUtil.
 */
public class Tfm_ctcTextResourceUtil {
	
	/**
	 * Use
	 * de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcResourceUtil.getResource()
	 * instead.
	 */
	@Deprecated
	public static de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcResource getResource(IFile file) {
		return new de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcEclipseProxy().getResource(file);
	}
	
	/**
	 * Use
	 * de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcResourceUtil.getResource()
	 * instead.
	 */
	@Deprecated
	public static de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcResource getResource(File file, Map<?,?> options) {
		return de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcResourceUtil.getResource(file, options);
	}
	
	/**
	 * Use
	 * de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcResourceUtil.getResource()
	 * instead.
	 */
	@Deprecated
	public static de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcResource getResource(URI uri) {
		return de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcResourceUtil.getResource(uri);
	}
	
	/**
	 * Use
	 * de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcResourceUtil.getResource()
	 * instead.
	 */
	@Deprecated
	public static de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcResource getResource(URI uri, Map<?,?> options) {
		return de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcResourceUtil.getResource(uri, options);
	}
	
}
