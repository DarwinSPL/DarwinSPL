/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc;

import org.eclipse.emf.ecore.EObject;

public interface ITfm_ctcInterpreterListener {
	
	public void handleInterpreteObject(EObject element);
}
