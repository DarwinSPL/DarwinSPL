/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.mopp;


public class Tfm_ctcResourcePostProcessor implements de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcResourcePostProcessor {
	
	public void process(de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcResource resource) {
		// Set the overrideResourcePostProcessor option to false to customize resource
		// post processing.
	}
	
	public void terminate() {
		// To signal termination to the process() method, setting a boolean field is
		// recommended. Depending on the value of this field process() can stop its
		// computation. However, this is only required for computation intensive
		// post-processors.
	}
	
}
