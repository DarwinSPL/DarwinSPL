/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.mopp;

import org.eclipse.emf.ecore.EClass;

public class Tfm_ctcSyntaxCoverageInformationProvider {
	
	public EClass[] getClassesWithSyntax() {
		return new EClass[] {
			de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(),
			de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraint(),
			de.darwinspl.temporal.DwTemporalModelPackage.eINSTANCE.getDwTemporalInterval(),
			de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwEquivalenceExpression(),
			de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwImpliesExpression(),
			de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwOrExpression(),
			de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwAndExpression(),
			de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwNotExpression(),
			de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwNestedExpression(),
			de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwFeatureReferenceExpression(),
			de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwBooleanValueExpression(),
		};
	}
	
	public EClass[] getStartSymbols() {
		return new EClass[] {
			de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel(),
		};
	}
	
}
