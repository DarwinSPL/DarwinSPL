/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.mopp;

import java.util.Collections;
import java.util.Set;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * A representation for a range in a document where a structural feature (e.g., a
 * reference) is expected.
 */
public class Tfm_ctcExpectedStructuralFeature extends de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcAbstractExpectedElement {
	
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcPlaceholder placeholder;
	
	public Tfm_ctcExpectedStructuralFeature(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcPlaceholder placeholder) {
		super(placeholder.getMetaclass());
		this.placeholder = placeholder;
	}
	
	public EStructuralFeature getFeature() {
		return placeholder.getFeature();
	}
	
	/**
	 * Returns the expected placeholder.
	 */
	public de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSyntaxElement getSyntaxElement() {
		return placeholder;
	}
	
	public String getTokenName() {
		return placeholder.getTokenName();
	}
	
	public Set<String> getTokenNames() {
		return Collections.singleton(getTokenName());
	}
	
	public String toString() {
		return "EFeature " + getFeature().getEContainingClass().getName() + "." + getFeature().getName();
	}
	
	public boolean equals(Object o) {
		if (o instanceof Tfm_ctcExpectedStructuralFeature) {
			return getFeature().equals(((Tfm_ctcExpectedStructuralFeature) o).getFeature());
		}
		return false;
	}
	@Override
	public int hashCode() {
		return getFeature().hashCode();
	}
	
}
