/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.mopp;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;

public class Tfm_ctcReferenceResolverSwitch implements de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceResolverSwitch {
	
	/**
	 * This map stores a copy of the options the were set for loading the resource.
	 */
	private Map<Object, Object> options;
	
	protected de.darwinspl.constraint.resource.tfm_ctc.analysis.DwConstraintModelFeatureModelReferenceResolver dwConstraintModelFeatureModelReferenceResolver = new de.darwinspl.constraint.resource.tfm_ctc.analysis.DwConstraintModelFeatureModelReferenceResolver();
	protected de.darwinspl.constraint.resource.tfm_ctc.analysis.DwFeatureReferenceExpressionFeatureReferenceResolver dwFeatureReferenceExpressionFeatureReferenceResolver = new de.darwinspl.constraint.resource.tfm_ctc.analysis.DwFeatureReferenceExpressionFeatureReferenceResolver();
	
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceResolver<de.darwinspl.constraint.DwConstraintModel, de.darwinspl.feature.DwTemporalFeatureModel> getDwConstraintModelFeatureModelReferenceResolver() {
		return getResolverChain(de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel_FeatureModel(), dwConstraintModelFeatureModelReferenceResolver);
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceResolver<de.darwinspl.expression.DwFeatureReferenceExpression, de.darwinspl.feature.DwFeature> getDwFeatureReferenceExpressionFeatureReferenceResolver() {
		return getResolverChain(de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwFeatureReferenceExpression_Feature(), dwFeatureReferenceExpressionFeatureReferenceResolver);
	}
	
	public void setOptions(Map<?, ?> options) {
		if (options != null) {
			this.options = new LinkedHashMap<Object, Object>();
			this.options.putAll(options);
		}
		dwConstraintModelFeatureModelReferenceResolver.setOptions(options);
		dwFeatureReferenceExpressionFeatureReferenceResolver.setOptions(options);
	}
	
	public void resolveFuzzy(String identifier, EObject container, EReference reference, int position, de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceResolveResult<EObject> result) {
		if (container == null) {
			return;
		}
		if (de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel().isInstance(container)) {
			Tfm_ctcFuzzyResolveResult<de.darwinspl.feature.DwTemporalFeatureModel> frr = new Tfm_ctcFuzzyResolveResult<de.darwinspl.feature.DwTemporalFeatureModel>(result);
			String referenceName = reference.getName();
			EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof EReference && referenceName != null && referenceName.equals("featureModel")) {
				dwConstraintModelFeatureModelReferenceResolver.resolve(identifier, (de.darwinspl.constraint.DwConstraintModel) container, (EReference) feature, position, true, frr);
			}
		}
		if (de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwFeatureReferenceExpression().isInstance(container)) {
			Tfm_ctcFuzzyResolveResult<de.darwinspl.feature.DwFeature> frr = new Tfm_ctcFuzzyResolveResult<de.darwinspl.feature.DwFeature>(result);
			String referenceName = reference.getName();
			EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof EReference && referenceName != null && referenceName.equals("feature")) {
				dwFeatureReferenceExpressionFeatureReferenceResolver.resolve(identifier, (de.darwinspl.expression.DwFeatureReferenceExpression) container, (EReference) feature, position, true, frr);
			}
		}
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceResolver<? extends EObject, ? extends EObject> getResolver(EStructuralFeature reference) {
		if (reference == de.darwinspl.constraint.DwConstraintPackage.eINSTANCE.getDwConstraintModel_FeatureModel()) {
			return getResolverChain(reference, dwConstraintModelFeatureModelReferenceResolver);
		}
		if (reference == de.darwinspl.expression.DwExpressionPackage.eINSTANCE.getDwFeatureReferenceExpression_Feature()) {
			return getResolverChain(reference, dwFeatureReferenceExpressionFeatureReferenceResolver);
		}
		return null;
	}
	
	@SuppressWarnings({"rawtypes", "unchecked"})
	public <ContainerType extends EObject, ReferenceType extends EObject> de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceResolver<ContainerType, ReferenceType> getResolverChain(EStructuralFeature reference, de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceResolver<ContainerType, ReferenceType> originalResolver) {
		if (options == null) {
			return originalResolver;
		}
		Object value = options.get(de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcOptions.ADDITIONAL_REFERENCE_RESOLVERS);
		if (value == null) {
			return originalResolver;
		}
		if (!(value instanceof Map)) {
			// send this to the error log
			new de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcRuntimeUtil().logWarning("Found value with invalid type for option " + de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + Map.class.getName() + ", but was " + value.getClass().getName() + ")", null);
			return originalResolver;
		}
		Map<?,?> resolverMap = (Map<?,?>) value;
		Object resolverValue = resolverMap.get(reference);
		if (resolverValue == null) {
			return originalResolver;
		}
		if (resolverValue instanceof de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceResolver) {
			de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceResolver replacingResolver = (de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceResolver) resolverValue;
			if (replacingResolver instanceof de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcDelegatingReferenceResolver) {
				// pass original resolver to the replacing one
				((de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcDelegatingReferenceResolver) replacingResolver).setDelegate(originalResolver);
			}
			return replacingResolver;
		} else if (resolverValue instanceof Collection) {
			Collection replacingResolvers = (Collection) resolverValue;
			de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceResolver replacingResolver = originalResolver;
			for (Object next : replacingResolvers) {
				if (next instanceof de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceCache) {
					de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceResolver nextResolver = (de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcReferenceResolver) next;
					if (nextResolver instanceof de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcDelegatingReferenceResolver) {
						// pass original resolver to the replacing one
						((de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcDelegatingReferenceResolver) nextResolver).setDelegate(replacingResolver);
					}
					replacingResolver = nextResolver;
				} else {
					// The collection contains a non-resolver. Send a warning to the error log.
					new de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcRuntimeUtil().logWarning("Found value with invalid type in value map for option " + de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcDelegatingReferenceResolver.class.getName() + ", but was " + next.getClass().getName() + ")", null);
				}
			}
			return replacingResolver;
		} else {
			// The value for the option ADDITIONAL_REFERENCE_RESOLVERS has an unknown type.
			new de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcRuntimeUtil().logWarning("Found value with invalid type in value map for option " + de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcDelegatingReferenceResolver.class.getName() + ", but was " + resolverValue.getClass().getName() + ")", null);
			return originalResolver;
		}
	}
	
}
