/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc;


public enum Tfm_ctcEProblemSeverity {
	WARNING, ERROR;
}
