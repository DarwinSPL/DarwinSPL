/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc;


/**
 * Implementors of this interface provide an EMF resource.
 */
public interface ITfm_ctcResourceProvider {
	
	/**
	 * Returns the resource.
	 */
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource getResource();
	
}
