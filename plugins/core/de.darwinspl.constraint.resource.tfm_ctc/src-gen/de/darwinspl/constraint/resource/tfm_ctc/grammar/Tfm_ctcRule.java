/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.grammar;

import org.eclipse.emf.ecore.EClass;

/**
 * A class to represent a rules in the grammar.
 */
public class Tfm_ctcRule extends de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSyntaxElement {
	
	private final EClass metaclass;
	
	public Tfm_ctcRule(EClass metaclass, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice choice, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality cardinality) {
		super(cardinality, new de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSyntaxElement[] {choice});
		this.metaclass = metaclass;
	}
	
	public EClass getMetaclass() {
		return metaclass;
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice getDefinition() {
		return (de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcChoice) getChildren()[0];
	}
	
	@Deprecated
	public String toString() {
		return metaclass == null ? "null" : metaclass.getName() + " ::= " + getDefinition().toString();
	}
	
}

