/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.grammar;


public class Tfm_ctcSequence extends de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSyntaxElement {
	
	public Tfm_ctcSequence(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality cardinality, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSyntaxElement... elements) {
		super(cardinality, elements);
	}
	
	public String toString() {
		return de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcStringUtil.explode(getChildren(), " ");
	}
	
}
