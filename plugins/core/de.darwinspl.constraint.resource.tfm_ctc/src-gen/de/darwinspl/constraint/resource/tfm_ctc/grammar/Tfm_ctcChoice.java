/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.grammar;


public class Tfm_ctcChoice extends de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSyntaxElement {
	
	public Tfm_ctcChoice(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality cardinality, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSyntaxElement... choices) {
		super(cardinality, choices);
	}
	
	public String toString() {
		return de.darwinspl.constraint.resource.tfm_ctc.util.Tfm_ctcStringUtil.explode(getChildren(), "|");
	}
	
}
