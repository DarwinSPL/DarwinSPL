/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.mopp;

import java.util.Collections;
import java.util.Set;

/**
 * A representation for a range in a document where a keyword (i.e., a static
 * string) is expected.
 */
public class Tfm_ctcExpectedCsString extends de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcAbstractExpectedElement {
	
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword keyword;
	
	public Tfm_ctcExpectedCsString(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcKeyword keyword) {
		super(keyword.getMetaclass());
		this.keyword = keyword;
	}
	
	public String getValue() {
		return keyword.getValue();
	}
	
	/**
	 * Returns the expected keyword.
	 */
	public de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSyntaxElement getSyntaxElement() {
		return keyword;
	}
	
	public Set<String> getTokenNames() {
		return Collections.singleton("'" + getValue() + "'");
	}
	
	public String toString() {
		return "CsString \"" + getValue() + "\"";
	}
	
	public boolean equals(Object o) {
		if (o instanceof Tfm_ctcExpectedCsString) {
			return getValue().equals(((Tfm_ctcExpectedCsString) o).getValue());
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return getValue().hashCode();
	}
	
}
