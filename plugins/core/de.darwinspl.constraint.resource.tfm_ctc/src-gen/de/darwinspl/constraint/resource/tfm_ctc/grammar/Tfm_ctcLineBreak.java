/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.grammar;


public class Tfm_ctcLineBreak extends de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcFormattingElement {
	
	private final int tabs;
	
	public Tfm_ctcLineBreak(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality cardinality, int tabs) {
		super(cardinality);
		this.tabs = tabs;
	}
	
	public int getTabs() {
		return tabs;
	}
	
	public String toString() {
		return "!" + getTabs();
	}
	
}
