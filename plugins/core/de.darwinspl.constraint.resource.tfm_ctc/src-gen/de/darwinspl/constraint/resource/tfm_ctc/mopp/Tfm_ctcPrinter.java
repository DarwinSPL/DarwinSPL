/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.mopp;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

/**
 * This class provides an implementation of the
 * de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextDiagnostic interface.
 * However, it is recommended to use the
 * de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcPrinter2 instead, because
 * it provides advanced printing features. There are even some features (e.g.,
 * printing enumeration terminals) which are only supported by that class.
 */
public class Tfm_ctcPrinter implements de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextPrinter {
	
	protected de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolverFactory tokenResolverFactory = new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcTokenResolverFactory();
	
	protected OutputStream outputStream;
	
	/**
	 * Holds the resource that is associated with this printer. This may be null if
	 * the printer is used stand alone.
	 */
	private de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource resource;
	
	private Map<?, ?> options;
	private String encoding = System.getProperty("file.encoding");
	
	public Tfm_ctcPrinter(OutputStream outputStream, de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource resource) {
		super();
		this.outputStream = outputStream;
		this.resource = resource;
	}
	
	protected int matchCount(Map<String, Integer> featureCounter, Collection<String> needed) {
		int pos = 0;
		int neg = 0;
		
		for (String featureName : featureCounter.keySet()) {
			if (needed.contains(featureName)) {
				int value = featureCounter.get(featureName);
				if (value == 0) {
					neg += 1;
				} else {
					pos += 1;
				}
			}
		}
		return neg > 0 ? -neg : pos;
	}
	
	protected void doPrint(EObject element, PrintWriter out, String globaltab) {
		if (element == null) {
			throw new IllegalArgumentException("Nothing to write.");
		}
		if (out == null) {
			throw new IllegalArgumentException("Nothing to write on.");
		}
		
		if (element instanceof de.darwinspl.constraint.DwConstraintModel) {
			print_de_darwinspl_constraint_DwConstraintModel((de.darwinspl.constraint.DwConstraintModel) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.constraint.DwConstraint) {
			print_de_darwinspl_constraint_DwConstraint((de.darwinspl.constraint.DwConstraint) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.temporal.DwTemporalInterval) {
			print_de_darwinspl_temporal_DwTemporalInterval((de.darwinspl.temporal.DwTemporalInterval) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.expression.DwEquivalenceExpression) {
			print_de_darwinspl_expression_DwEquivalenceExpression((de.darwinspl.expression.DwEquivalenceExpression) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.expression.DwImpliesExpression) {
			print_de_darwinspl_expression_DwImpliesExpression((de.darwinspl.expression.DwImpliesExpression) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.expression.DwOrExpression) {
			print_de_darwinspl_expression_DwOrExpression((de.darwinspl.expression.DwOrExpression) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.expression.DwAndExpression) {
			print_de_darwinspl_expression_DwAndExpression((de.darwinspl.expression.DwAndExpression) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.expression.DwNotExpression) {
			print_de_darwinspl_expression_DwNotExpression((de.darwinspl.expression.DwNotExpression) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.expression.DwNestedExpression) {
			print_de_darwinspl_expression_DwNestedExpression((de.darwinspl.expression.DwNestedExpression) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.expression.DwFeatureReferenceExpression) {
			print_de_darwinspl_expression_DwFeatureReferenceExpression((de.darwinspl.expression.DwFeatureReferenceExpression) element, globaltab, out);
			return;
		}
		if (element instanceof de.darwinspl.expression.DwBooleanValueExpression) {
			print_de_darwinspl_expression_DwBooleanValueExpression((de.darwinspl.expression.DwBooleanValueExpression) element, globaltab, out);
			return;
		}
		
		addWarningToResource("The printer can not handle " + element.eClass().getName() + " elements", element);
	}
	
	protected de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcReferenceResolverSwitch getReferenceResolverSwitch() {
		return (de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcReferenceResolverSwitch) new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcMetaInformation().getReferenceResolverSwitch();
	}
	
	protected void addWarningToResource(final String errorMessage, EObject cause) {
		de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource resource = getResource();
		if (resource == null) {
			// the resource can be null if the printer is used stand alone
			return;
		}
		resource.addProblem(new de.darwinspl.constraint.resource.tfm_ctc.mopp.Tfm_ctcProblem(errorMessage, de.darwinspl.constraint.resource.tfm_ctc.Tfm_ctcEProblemType.PRINT_PROBLEM, de.darwinspl.constraint.resource.tfm_ctc.Tfm_ctcEProblemSeverity.WARNING), cause);
	}
	
	public void setOptions(Map<?,?> options) {
		this.options = options;
	}
	
	public Map<?,?> getOptions() {
		return options;
	}
	
	public void setEncoding(String encoding) {
		if (encoding != null) {
			this.encoding = encoding;
		}
	}
	
	public String getEncoding() {
		return encoding;
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTextResource getResource() {
		return resource;
	}
	
	/**
	 * Calls {@link #doPrint(EObject, PrintWriter, String)} and writes the result to
	 * the underlying output stream.
	 */
	public void print(EObject element) throws java.io.IOException {
		PrintWriter out = new PrintWriter(new OutputStreamWriter(new BufferedOutputStream(outputStream), encoding));
		doPrint(element, out, "");
		out.flush();
	}
	
	public void print_de_darwinspl_constraint_DwConstraintModel(de.darwinspl.constraint.DwConstraintModel element, String outertab, PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT_MODEL__FEATURE_MODEL));
		printCountingMap.put("featureModel", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT_MODEL__CONSTRAINTS));
		printCountingMap.put("constraints", temp == null ? 0 : ((Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		PrintWriter out1 = null;
		Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("feature");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("model");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("featureModel");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT_MODEL__FEATURE_MODEL));
			if (o != null) {
				de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver resolver = tokenResolverFactory.createTokenResolver("PATH_TO_FILE");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getDwConstraintModelFeatureModelReferenceResolver().deResolve((de.darwinspl.feature.DwTemporalFeatureModel) o, element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT_MODEL__FEATURE_MODEL)), element.eClass().getEStructuralFeature(de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT_MODEL__FEATURE_MODEL), element));
				out.print(" ");
			}
			printCountingMap.put("featureModel", count - 1);
		}
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new StringWriter();
			out1 = new PrintWriter(sWriter);
			printCountingMap1 = new LinkedHashMap<String, Integer>(printCountingMap);
			print_de_darwinspl_constraint_DwConstraintModel_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
	}
	
	public void print_de_darwinspl_constraint_DwConstraintModel_0(de.darwinspl.constraint.DwConstraintModel element, String outertab, PrintWriter out, Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("constraints");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT_MODEL__CONSTRAINTS));
			List<?> list = (List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("constraints", count - 1);
		}
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
	}
	
	
	public void print_de_darwinspl_constraint_DwConstraint(de.darwinspl.constraint.DwConstraint element, String outertab, PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT__ID));
		printCountingMap.put("id", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT__VALIDITY));
		printCountingMap.put("validity", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT__ROOT_EXPRESSION));
		printCountingMap.put("rootExpression", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		java.io.StringWriter sWriter = null;
		PrintWriter out1 = null;
		Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("rootExpression");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT__ROOT_EXPRESSION));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("rootExpression", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new StringWriter();
		out1 = new PrintWriter(sWriter);
		printCountingMap1 = new LinkedHashMap<String, Integer>(printCountingMap);
		print_de_darwinspl_constraint_DwConstraint_0(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
	}
	
	public void print_de_darwinspl_constraint_DwConstraint_0(de.darwinspl.constraint.DwConstraint element, String outertab, PrintWriter out, Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("validity");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.constraint.DwConstraintPackage.DW_CONSTRAINT__VALIDITY));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("validity", count - 1);
		}
	}
	
	
	public void print_de_darwinspl_temporal_DwTemporalInterval(de.darwinspl.temporal.DwTemporalInterval element, String outertab, PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__FROM));
		printCountingMap.put("from", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__TO));
		printCountingMap.put("to", temp == null ? 0 : 1);
		// print collected hidden tokens
		// DEFINITION PART BEGINS (CsString)
		out.print("[");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		print_de_darwinspl_temporal_DwTemporalInterval_0(element, localtab, out, printCountingMap);
		// DEFINITION PART BEGINS (CsString)
		out.print("]");
		out.print(" ");
	}
	
	public void print_de_darwinspl_temporal_DwTemporalInterval_0(de.darwinspl.temporal.DwTemporalInterval element, String outertab, PrintWriter out, Map<String, Integer> printCountingMap) {
		int count;
		int alt = -1;
		alt = 0;
		int matches = 		matchCount(printCountingMap, Arrays.asList(		"from"		,
		"to"		));
		int tempMatchCount;
		tempMatchCount = 		matchCount(printCountingMap, Arrays.asList(		"from"		));
		if (tempMatchCount > matches) {
			alt = 1;
			matches = tempMatchCount;
		}
		tempMatchCount = 		matchCount(printCountingMap, Arrays.asList(		"to"		));
		if (tempMatchCount > matches) {
			alt = 2;
			matches = tempMatchCount;
		}
		tempMatchCount = 		0;
		if (tempMatchCount > matches) {
			alt = 3;
			matches = tempMatchCount;
		}
		switch(alt) {
			case 1:			{
				// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
				count = printCountingMap.get("from");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__FROM));
					if (o != null) {
						de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver resolver = tokenResolverFactory.createTokenResolver("DATE");
						resolver.setOptions(getOptions());
						out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__FROM), element));
						out.print(" ");
					}
					printCountingMap.put("from", count - 1);
				}
				// DEFINITION PART BEGINS (CsString)
				out.print("-");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print("eternity");
				out.print(" ");
			}
			break;
			case 2:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("initial state");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print("-");
				out.print(" ");
				// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
				count = printCountingMap.get("to");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__TO));
					if (o != null) {
						de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver resolver = tokenResolverFactory.createTokenResolver("DATE");
						resolver.setOptions(getOptions());
						out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__TO), element));
						out.print(" ");
					}
					printCountingMap.put("to", count - 1);
				}
			}
			break;
			case 3:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("initial state");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print("-");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print("eternity");
				out.print(" ");
			}
			break;
			default:			// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
			count = printCountingMap.get("from");
			if (count > 0) {
				Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__FROM));
				if (o != null) {
					de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver resolver = tokenResolverFactory.createTokenResolver("DATE");
					resolver.setOptions(getOptions());
					out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__FROM), element));
					out.print(" ");
				}
				printCountingMap.put("from", count - 1);
			}
			// DEFINITION PART BEGINS (CsString)
			out.print("-");
			out.print(" ");
			// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
			count = printCountingMap.get("to");
			if (count > 0) {
				Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__TO));
				if (o != null) {
					de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver resolver = tokenResolverFactory.createTokenResolver("DATE");
					resolver.setOptions(getOptions());
					out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(de.darwinspl.temporal.DwTemporalModelPackage.DW_TEMPORAL_INTERVAL__TO), element));
					out.print(" ");
				}
				printCountingMap.put("to", count - 1);
			}
		}
	}
	
	
	public void print_de_darwinspl_expression_DwEquivalenceExpression(de.darwinspl.expression.DwEquivalenceExpression element, String outertab, PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_EQUIVALENCE_EXPRESSION__OPERAND1));
		printCountingMap.put("operand1", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_EQUIVALENCE_EXPRESSION__OPERAND2));
		printCountingMap.put("operand2", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("operand1");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_EQUIVALENCE_EXPRESSION__OPERAND1));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("operand1", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("<->");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("operand2");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_EQUIVALENCE_EXPRESSION__OPERAND2));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("operand2", count - 1);
		}
	}
	
	
	public void print_de_darwinspl_expression_DwImpliesExpression(de.darwinspl.expression.DwImpliesExpression element, String outertab, PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_IMPLIES_EXPRESSION__OPERAND1));
		printCountingMap.put("operand1", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_IMPLIES_EXPRESSION__OPERAND2));
		printCountingMap.put("operand2", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("operand1");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_IMPLIES_EXPRESSION__OPERAND1));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("operand1", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("->");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("operand2");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_IMPLIES_EXPRESSION__OPERAND2));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("operand2", count - 1);
		}
	}
	
	
	public void print_de_darwinspl_expression_DwOrExpression(de.darwinspl.expression.DwOrExpression element, String outertab, PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_OR_EXPRESSION__OPERAND1));
		printCountingMap.put("operand1", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_OR_EXPRESSION__OPERAND2));
		printCountingMap.put("operand2", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("operand1");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_OR_EXPRESSION__OPERAND1));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("operand1", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("||");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("operand2");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_OR_EXPRESSION__OPERAND2));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("operand2", count - 1);
		}
	}
	
	
	public void print_de_darwinspl_expression_DwAndExpression(de.darwinspl.expression.DwAndExpression element, String outertab, PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_AND_EXPRESSION__OPERAND1));
		printCountingMap.put("operand1", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_AND_EXPRESSION__OPERAND2));
		printCountingMap.put("operand2", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("operand1");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_AND_EXPRESSION__OPERAND1));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("operand1", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("&&");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("operand2");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_AND_EXPRESSION__OPERAND2));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("operand2", count - 1);
		}
	}
	
	
	public void print_de_darwinspl_expression_DwNotExpression(de.darwinspl.expression.DwNotExpression element, String outertab, PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_NOT_EXPRESSION__OPERAND));
		printCountingMap.put("operand", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("!");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("operand");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_NOT_EXPRESSION__OPERAND));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("operand", count - 1);
		}
	}
	
	
	public void print_de_darwinspl_expression_DwNestedExpression(de.darwinspl.expression.DwNestedExpression element, String outertab, PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_NESTED_EXPRESSION__OPERAND));
		printCountingMap.put("operand", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("(");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("operand");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_NESTED_EXPRESSION__OPERAND));
			if (o != null) {
				doPrint((EObject) o, out, localtab);
			}
			printCountingMap.put("operand", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(")");
		out.print(" ");
	}
	
	
	public void print_de_darwinspl_expression_DwFeatureReferenceExpression(de.darwinspl.expression.DwFeatureReferenceExpression element, String outertab, PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE));
		printCountingMap.put("feature", temp == null ? 0 : 1);
		// print collected hidden tokens
		// DEFINITION PART BEGINS (CompoundDefinition)
		print_de_darwinspl_expression_DwFeatureReferenceExpression_0(element, localtab, out, printCountingMap);
	}
	
	public void print_de_darwinspl_expression_DwFeatureReferenceExpression_0(de.darwinspl.expression.DwFeatureReferenceExpression element, String outertab, PrintWriter out, Map<String, Integer> printCountingMap) {
		int count;
		int alt = -1;
		alt = 0;
		int matches = 		matchCount(printCountingMap, Arrays.asList(		"feature"		));
		int tempMatchCount;
		tempMatchCount = 		matchCount(printCountingMap, Arrays.asList(		"feature"		));
		if (tempMatchCount > matches) {
			alt = 1;
			matches = tempMatchCount;
		}
		switch(alt) {
			case 1:			{
				// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
				count = printCountingMap.get("feature");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE));
					if (o != null) {
						de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER_TOKEN");
						resolver.setOptions(getOptions());
						out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getDwFeatureReferenceExpressionFeatureReferenceResolver().deResolve((de.darwinspl.feature.DwFeature) o, element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE)), element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), element));
						out.print(" ");
					}
					printCountingMap.put("feature", count - 1);
				}
			}
			break;
			default:			// DEFINITION PART BEGINS (PlaceholderInQuotes)
			count = printCountingMap.get("feature");
			if (count > 0) {
				Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE));
				if (o != null) {
					de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
					resolver.setOptions(getOptions());
					out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getDwFeatureReferenceExpressionFeatureReferenceResolver().deResolve((de.darwinspl.feature.DwFeature) o, element, (EReference) element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE)), element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION__FEATURE), element));
					out.print(" ");
				}
				printCountingMap.put("feature", count - 1);
			}
		}
	}
	
	
	public void print_de_darwinspl_expression_DwBooleanValueExpression(de.darwinspl.expression.DwBooleanValueExpression element, String outertab, PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		Map<String, Integer> printCountingMap = new LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_BOOLEAN_VALUE_EXPRESSION__VALUE));
		printCountingMap.put("value", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (BooleanTerminal)
		count = printCountingMap.get("value");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(de.darwinspl.expression.DwExpressionPackage.DW_BOOLEAN_VALUE_EXPRESSION__VALUE));
			if (o != null) {
			}
			printCountingMap.put("value", count - 1);
		}
	}
	
	
}
