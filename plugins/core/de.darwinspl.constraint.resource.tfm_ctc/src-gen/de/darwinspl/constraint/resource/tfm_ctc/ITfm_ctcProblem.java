/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc;

import java.util.Collection;

public interface ITfm_ctcProblem {
	public String getMessage();
	public de.darwinspl.constraint.resource.tfm_ctc.Tfm_ctcEProblemSeverity getSeverity();
	public de.darwinspl.constraint.resource.tfm_ctc.Tfm_ctcEProblemType getType();
	public Collection<de.darwinspl.constraint.resource.tfm_ctc.ITfm_ctcQuickFix> getQuickFixes();
}
