/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.grammar;

import org.eclipse.emf.ecore.EClass;

/**
 * The abstract super class for all elements of a grammar. This class provides
 * methods to traverse the grammar rules.
 */
public abstract class Tfm_ctcSyntaxElement {
	
	private Tfm_ctcSyntaxElement[] children;
	private Tfm_ctcSyntaxElement parent;
	private de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality cardinality;
	
	public Tfm_ctcSyntaxElement(de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality cardinality, Tfm_ctcSyntaxElement[] children) {
		this.cardinality = cardinality;
		this.children = children;
		if (this.children != null) {
			for (Tfm_ctcSyntaxElement child : this.children) {
				child.setParent(this);
			}
		}
	}
	
	/**
	 * Sets the parent of this syntax element. This method must be invoked at most
	 * once.
	 */
	public void setParent(Tfm_ctcSyntaxElement parent) {
		assert this.parent == null;
		this.parent = parent;
	}
	
	/**
	 * Returns the parent of this syntax element. This parent is determined by the
	 * containment hierarchy in the CS model.
	 */
	public Tfm_ctcSyntaxElement getParent() {
		return parent;
	}
	
	/**
	 * Returns the rule of this syntax element. The rule is determined by the
	 * containment hierarchy in the CS model.
	 */
	public de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule getRule() {
		if (this instanceof de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule) {
			return (de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcRule) this;
		}
		return parent.getRule();
	}
	
	public Tfm_ctcSyntaxElement[] getChildren() {
		if (children == null) {
			return new Tfm_ctcSyntaxElement[0];
		}
		return children;
	}
	
	public EClass getMetaclass() {
		return parent.getMetaclass();
	}
	
	public de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality getCardinality() {
		return cardinality;
	}
	
	public boolean hasContainment(EClass metaclass) {
		de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSyntaxElement[] children = getChildren();
		for (de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcSyntaxElement child : children) {
			if (child.hasContainment(metaclass)) {
				return true;
			}
		}
		return false;
	}
	
}
