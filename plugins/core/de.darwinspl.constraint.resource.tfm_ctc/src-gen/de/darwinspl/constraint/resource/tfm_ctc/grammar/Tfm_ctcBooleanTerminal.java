/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.darwinspl.constraint.resource.tfm_ctc.grammar;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * A class to represent boolean terminals in a grammar.
 */
public class Tfm_ctcBooleanTerminal extends de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcTerminal {
	
	private String trueLiteral;
	private String falseLiteral;
	
	public Tfm_ctcBooleanTerminal(EStructuralFeature attribute, String trueLiteral, String falseLiteral, de.darwinspl.constraint.resource.tfm_ctc.grammar.Tfm_ctcCardinality cardinality, int mandatoryOccurrencesAfter) {
		super(attribute, cardinality, mandatoryOccurrencesAfter);
		assert attribute instanceof EAttribute;
		this.trueLiteral = trueLiteral;
		this.falseLiteral = falseLiteral;
	}
	
	public String getTrueLiteral() {
		return trueLiteral;
	}
	
	public String getFalseLiteral() {
		return falseLiteral;
	}
	
	public EAttribute getAttribute() {
		return (EAttribute) getFeature();
	}
	
}
