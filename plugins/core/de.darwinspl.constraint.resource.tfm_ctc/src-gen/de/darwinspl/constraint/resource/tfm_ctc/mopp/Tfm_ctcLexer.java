// $ANTLR 3.4

	package de.darwinspl.constraint.resource.tfm_ctc.mopp;
	
	import java.util.ArrayList;
import java.util.List;
import org.antlr.runtime3_4_0.ANTLRStringStream;
import org.antlr.runtime3_4_0.RecognitionException;


import org.antlr.runtime3_4_0.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class Tfm_ctcLexer extends Lexer {
    public static final int EOF=-1;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__19=19;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int DATE=4;
    public static final int IDENTIFIER_TOKEN=5;
    public static final int INTEGER_LITERAL=6;
    public static final int LINEBREAK=7;
    public static final int ML_COMMENT=8;
    public static final int PATH_TO_FILE=9;
    public static final int QUOTED_34_34=10;
    public static final int SL_COMMENT=11;
    public static final int WHITESPACE=12;

    	public List<RecognitionException> lexerExceptions  = new ArrayList<RecognitionException>();
    	public List<Integer> lexerExceptionPositions = new ArrayList<Integer>();
    	
    	public void reportError(RecognitionException e) {
    		lexerExceptions.add(e);
    		lexerExceptionPositions.add(((ANTLRStringStream) input).index());
    	}


    // delegates
    // delegators
    public Lexer[] getDelegates() {
        return new Lexer[] {};
    }

    public Tfm_ctcLexer() {} 
    public Tfm_ctcLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public Tfm_ctcLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);
    }
    public String getGrammarFileName() { return "Tfm_ctc.g"; }

    // $ANTLR start "T__13"
    public final void mT__13() throws RecognitionException {
        try {
            int _type = T__13;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Tfm_ctc.g:20:7: ( '!' )
            // Tfm_ctc.g:20:9: '!'
            {
            match('!'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__13"

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Tfm_ctc.g:21:7: ( '&&' )
            // Tfm_ctc.g:21:9: '&&'
            {
            match("&&"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Tfm_ctc.g:22:7: ( '(' )
            // Tfm_ctc.g:22:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Tfm_ctc.g:23:7: ( ')' )
            // Tfm_ctc.g:23:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Tfm_ctc.g:24:7: ( '-' )
            // Tfm_ctc.g:24:9: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Tfm_ctc.g:25:7: ( '->' )
            // Tfm_ctc.g:25:9: '->'
            {
            match("->"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Tfm_ctc.g:26:7: ( '<->' )
            // Tfm_ctc.g:26:9: '<->'
            {
            match("<->"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Tfm_ctc.g:27:7: ( '[' )
            // Tfm_ctc.g:27:9: '['
            {
            match('['); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Tfm_ctc.g:28:7: ( ']' )
            // Tfm_ctc.g:28:9: ']'
            {
            match(']'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Tfm_ctc.g:29:7: ( 'eternity' )
            // Tfm_ctc.g:29:9: 'eternity'
            {
            match("eternity"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Tfm_ctc.g:30:7: ( 'false' )
            // Tfm_ctc.g:30:9: 'false'
            {
            match("false"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Tfm_ctc.g:31:7: ( 'feature' )
            // Tfm_ctc.g:31:9: 'feature'
            {
            match("feature"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Tfm_ctc.g:32:7: ( 'initial state' )
            // Tfm_ctc.g:32:9: 'initial state'
            {
            match("initial state"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Tfm_ctc.g:33:7: ( 'model' )
            // Tfm_ctc.g:33:9: 'model'
            {
            match("model"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Tfm_ctc.g:34:7: ( 'true' )
            // Tfm_ctc.g:34:9: 'true'
            {
            match("true"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Tfm_ctc.g:35:7: ( '||' )
            // Tfm_ctc.g:35:9: '||'
            {
            match("||"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "PATH_TO_FILE"
    public final void mPATH_TO_FILE() throws RecognitionException {
        try {
            int _type = PATH_TO_FILE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Tfm_ctc.g:1530:13: ( ( '<' ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | ' ' | '/' )+ '.tfm>' ) )
            // Tfm_ctc.g:1531:2: ( '<' ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | ' ' | '/' )+ '.tfm>' )
            {
            // Tfm_ctc.g:1531:2: ( '<' ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | ' ' | '/' )+ '.tfm>' )
            // Tfm_ctc.g:1531:2: '<' ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | ' ' | '/' )+ '.tfm>'
            {
            match('<'); 

            // Tfm_ctc.g:1531:6: ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | ' ' | '/' )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==' '||(LA1_0 >= '/' && LA1_0 <= '9')||(LA1_0 >= 'A' && LA1_0 <= 'Z')||LA1_0=='_'||(LA1_0 >= 'a' && LA1_0 <= 'z')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // Tfm_ctc.g:
            	    {
            	    if ( input.LA(1)==' '||(input.LA(1) >= '/' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            match(".tfm>"); 



            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "PATH_TO_FILE"

    // $ANTLR start "DATE"
    public final void mDATE() throws RecognitionException {
        try {
            int _type = DATE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Tfm_ctc.g:1533:5: ( ( ( ( '0' .. '9' )+ '/' ( '0' .. '9' )+ '/' ( '0' .. '9' )+ ( 'T' ( '0' .. '9' )+ ':' ( '0' .. '9' )+ ( ':' ( '0' .. '9' )+ )? )? ) ) )
            // Tfm_ctc.g:1534:2: ( ( ( '0' .. '9' )+ '/' ( '0' .. '9' )+ '/' ( '0' .. '9' )+ ( 'T' ( '0' .. '9' )+ ':' ( '0' .. '9' )+ ( ':' ( '0' .. '9' )+ )? )? ) )
            {
            // Tfm_ctc.g:1534:2: ( ( ( '0' .. '9' )+ '/' ( '0' .. '9' )+ '/' ( '0' .. '9' )+ ( 'T' ( '0' .. '9' )+ ':' ( '0' .. '9' )+ ( ':' ( '0' .. '9' )+ )? )? ) )
            // Tfm_ctc.g:1534:2: ( ( '0' .. '9' )+ '/' ( '0' .. '9' )+ '/' ( '0' .. '9' )+ ( 'T' ( '0' .. '9' )+ ':' ( '0' .. '9' )+ ( ':' ( '0' .. '9' )+ )? )? )
            {
            // Tfm_ctc.g:1534:2: ( ( '0' .. '9' )+ '/' ( '0' .. '9' )+ '/' ( '0' .. '9' )+ ( 'T' ( '0' .. '9' )+ ':' ( '0' .. '9' )+ ( ':' ( '0' .. '9' )+ )? )? )
            // Tfm_ctc.g:1534:3: ( '0' .. '9' )+ '/' ( '0' .. '9' )+ '/' ( '0' .. '9' )+ ( 'T' ( '0' .. '9' )+ ':' ( '0' .. '9' )+ ( ':' ( '0' .. '9' )+ )? )?
            {
            // Tfm_ctc.g:1534:3: ( '0' .. '9' )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0 >= '0' && LA2_0 <= '9')) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // Tfm_ctc.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);


            match('/'); 

            // Tfm_ctc.g:1534:18: ( '0' .. '9' )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0 >= '0' && LA3_0 <= '9')) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // Tfm_ctc.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);


            match('/'); 

            // Tfm_ctc.g:1534:33: ( '0' .. '9' )+
            int cnt4=0;
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( ((LA4_0 >= '0' && LA4_0 <= '9')) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // Tfm_ctc.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt4 >= 1 ) break loop4;
                        EarlyExitException eee =
                            new EarlyExitException(4, input);
                        throw eee;
                }
                cnt4++;
            } while (true);


            // Tfm_ctc.g:1534:45: ( 'T' ( '0' .. '9' )+ ':' ( '0' .. '9' )+ ( ':' ( '0' .. '9' )+ )? )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0=='T') ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // Tfm_ctc.g:1534:47: 'T' ( '0' .. '9' )+ ':' ( '0' .. '9' )+ ( ':' ( '0' .. '9' )+ )?
                    {
                    match('T'); 

                    // Tfm_ctc.g:1534:50: ( '0' .. '9' )+
                    int cnt5=0;
                    loop5:
                    do {
                        int alt5=2;
                        int LA5_0 = input.LA(1);

                        if ( ((LA5_0 >= '0' && LA5_0 <= '9')) ) {
                            alt5=1;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // Tfm_ctc.g:
                    	    {
                    	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                    	        input.consume();
                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt5 >= 1 ) break loop5;
                                EarlyExitException eee =
                                    new EarlyExitException(5, input);
                                throw eee;
                        }
                        cnt5++;
                    } while (true);


                    match(':'); 

                    // Tfm_ctc.g:1534:65: ( '0' .. '9' )+
                    int cnt6=0;
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( ((LA6_0 >= '0' && LA6_0 <= '9')) ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // Tfm_ctc.g:
                    	    {
                    	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                    	        input.consume();
                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt6 >= 1 ) break loop6;
                                EarlyExitException eee =
                                    new EarlyExitException(6, input);
                                throw eee;
                        }
                        cnt6++;
                    } while (true);


                    // Tfm_ctc.g:1534:77: ( ':' ( '0' .. '9' )+ )?
                    int alt8=2;
                    int LA8_0 = input.LA(1);

                    if ( (LA8_0==':') ) {
                        alt8=1;
                    }
                    switch (alt8) {
                        case 1 :
                            // Tfm_ctc.g:1534:78: ':' ( '0' .. '9' )+
                            {
                            match(':'); 

                            // Tfm_ctc.g:1534:82: ( '0' .. '9' )+
                            int cnt7=0;
                            loop7:
                            do {
                                int alt7=2;
                                int LA7_0 = input.LA(1);

                                if ( ((LA7_0 >= '0' && LA7_0 <= '9')) ) {
                                    alt7=1;
                                }


                                switch (alt7) {
                            	case 1 :
                            	    // Tfm_ctc.g:
                            	    {
                            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                            	        input.consume();
                            	    }
                            	    else {
                            	        MismatchedSetException mse = new MismatchedSetException(null,input);
                            	        recover(mse);
                            	        throw mse;
                            	    }


                            	    }
                            	    break;

                            	default :
                            	    if ( cnt7 >= 1 ) break loop7;
                                        EarlyExitException eee =
                                            new EarlyExitException(7, input);
                                        throw eee;
                                }
                                cnt7++;
                            } while (true);


                            }
                            break;

                    }


                    }
                    break;

            }


            }


            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DATE"

    // $ANTLR start "INTEGER_LITERAL"
    public final void mINTEGER_LITERAL() throws RecognitionException {
        try {
            int _type = INTEGER_LITERAL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Tfm_ctc.g:1536:16: ( ( ( '0' .. '9' )+ ) )
            // Tfm_ctc.g:1537:2: ( ( '0' .. '9' )+ )
            {
            // Tfm_ctc.g:1537:2: ( ( '0' .. '9' )+ )
            // Tfm_ctc.g:1537:2: ( '0' .. '9' )+
            {
            // Tfm_ctc.g:1537:2: ( '0' .. '9' )+
            int cnt10=0;
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( ((LA10_0 >= '0' && LA10_0 <= '9')) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // Tfm_ctc.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt10 >= 1 ) break loop10;
                        EarlyExitException eee =
                            new EarlyExitException(10, input);
                        throw eee;
                }
                cnt10++;
            } while (true);


            }


             _channel = 99; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "INTEGER_LITERAL"

    // $ANTLR start "IDENTIFIER_TOKEN"
    public final void mIDENTIFIER_TOKEN() throws RecognitionException {
        try {
            int _type = IDENTIFIER_TOKEN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Tfm_ctc.g:1540:17: ( ( ( 'A' .. 'Z' | 'a' .. 'z' | '_' ) ( 'A' .. 'Z' | 'a' .. 'z' | '_' | '0' .. '9' )* ) )
            // Tfm_ctc.g:1541:2: ( ( 'A' .. 'Z' | 'a' .. 'z' | '_' ) ( 'A' .. 'Z' | 'a' .. 'z' | '_' | '0' .. '9' )* )
            {
            // Tfm_ctc.g:1541:2: ( ( 'A' .. 'Z' | 'a' .. 'z' | '_' ) ( 'A' .. 'Z' | 'a' .. 'z' | '_' | '0' .. '9' )* )
            // Tfm_ctc.g:1541:2: ( 'A' .. 'Z' | 'a' .. 'z' | '_' ) ( 'A' .. 'Z' | 'a' .. 'z' | '_' | '0' .. '9' )*
            {
            if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            // Tfm_ctc.g:1541:25: ( 'A' .. 'Z' | 'a' .. 'z' | '_' | '0' .. '9' )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( ((LA11_0 >= '0' && LA11_0 <= '9')||(LA11_0 >= 'A' && LA11_0 <= 'Z')||LA11_0=='_'||(LA11_0 >= 'a' && LA11_0 <= 'z')) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // Tfm_ctc.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);


            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "IDENTIFIER_TOKEN"

    // $ANTLR start "SL_COMMENT"
    public final void mSL_COMMENT() throws RecognitionException {
        try {
            int _type = SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Tfm_ctc.g:1543:11: ( ( '//' (~ ( '\\n' | '\\r' | '\\uffff' ) )* ) )
            // Tfm_ctc.g:1544:2: ( '//' (~ ( '\\n' | '\\r' | '\\uffff' ) )* )
            {
            // Tfm_ctc.g:1544:2: ( '//' (~ ( '\\n' | '\\r' | '\\uffff' ) )* )
            // Tfm_ctc.g:1544:2: '//' (~ ( '\\n' | '\\r' | '\\uffff' ) )*
            {
            match("//"); 



            // Tfm_ctc.g:1544:6: (~ ( '\\n' | '\\r' | '\\uffff' ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( ((LA12_0 >= '\u0000' && LA12_0 <= '\t')||(LA12_0 >= '\u000B' && LA12_0 <= '\f')||(LA12_0 >= '\u000E' && LA12_0 <= '\uFFFE')) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // Tfm_ctc.g:
            	    {
            	    if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFE') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);


            }


             _channel = 99; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SL_COMMENT"

    // $ANTLR start "ML_COMMENT"
    public final void mML_COMMENT() throws RecognitionException {
        try {
            int _type = ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Tfm_ctc.g:1547:11: ( ( '/*' ( . )* '*/' ) )
            // Tfm_ctc.g:1548:2: ( '/*' ( . )* '*/' )
            {
            // Tfm_ctc.g:1548:2: ( '/*' ( . )* '*/' )
            // Tfm_ctc.g:1548:2: '/*' ( . )* '*/'
            {
            match("/*"); 



            // Tfm_ctc.g:1548:6: ( . )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0=='*') ) {
                    int LA13_1 = input.LA(2);

                    if ( (LA13_1=='/') ) {
                        alt13=2;
                    }
                    else if ( ((LA13_1 >= '\u0000' && LA13_1 <= '.')||(LA13_1 >= '0' && LA13_1 <= '\uFFFF')) ) {
                        alt13=1;
                    }


                }
                else if ( ((LA13_0 >= '\u0000' && LA13_0 <= ')')||(LA13_0 >= '+' && LA13_0 <= '\uFFFF')) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // Tfm_ctc.g:1548:6: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);


            match("*/"); 



            }


             _channel = 99; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ML_COMMENT"

    // $ANTLR start "LINEBREAK"
    public final void mLINEBREAK() throws RecognitionException {
        try {
            int _type = LINEBREAK;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Tfm_ctc.g:1551:10: ( ( ( '\\r\\n' | '\\r' | '\\n' ) ) )
            // Tfm_ctc.g:1552:2: ( ( '\\r\\n' | '\\r' | '\\n' ) )
            {
            // Tfm_ctc.g:1552:2: ( ( '\\r\\n' | '\\r' | '\\n' ) )
            // Tfm_ctc.g:1552:2: ( '\\r\\n' | '\\r' | '\\n' )
            {
            // Tfm_ctc.g:1552:2: ( '\\r\\n' | '\\r' | '\\n' )
            int alt14=3;
            int LA14_0 = input.LA(1);

            if ( (LA14_0=='\r') ) {
                int LA14_1 = input.LA(2);

                if ( (LA14_1=='\n') ) {
                    alt14=1;
                }
                else {
                    alt14=2;
                }
            }
            else if ( (LA14_0=='\n') ) {
                alt14=3;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;

            }
            switch (alt14) {
                case 1 :
                    // Tfm_ctc.g:1552:3: '\\r\\n'
                    {
                    match("\r\n"); 



                    }
                    break;
                case 2 :
                    // Tfm_ctc.g:1552:10: '\\r'
                    {
                    match('\r'); 

                    }
                    break;
                case 3 :
                    // Tfm_ctc.g:1552:15: '\\n'
                    {
                    match('\n'); 

                    }
                    break;

            }


            }


             _channel = 99; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LINEBREAK"

    // $ANTLR start "WHITESPACE"
    public final void mWHITESPACE() throws RecognitionException {
        try {
            int _type = WHITESPACE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Tfm_ctc.g:1555:11: ( ( ( ' ' | '\\t' | '\\f' ) ) )
            // Tfm_ctc.g:1556:2: ( ( ' ' | '\\t' | '\\f' ) )
            {
            if ( input.LA(1)=='\t'||input.LA(1)=='\f'||input.LA(1)==' ' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


             _channel = 99; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WHITESPACE"

    // $ANTLR start "QUOTED_34_34"
    public final void mQUOTED_34_34() throws RecognitionException {
        try {
            int _type = QUOTED_34_34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Tfm_ctc.g:1559:13: ( ( ( '\"' ) (~ ( '\"' ) )* ( '\"' ) ) )
            // Tfm_ctc.g:1560:2: ( ( '\"' ) (~ ( '\"' ) )* ( '\"' ) )
            {
            // Tfm_ctc.g:1560:2: ( ( '\"' ) (~ ( '\"' ) )* ( '\"' ) )
            // Tfm_ctc.g:1560:2: ( '\"' ) (~ ( '\"' ) )* ( '\"' )
            {
            // Tfm_ctc.g:1560:2: ( '\"' )
            // Tfm_ctc.g:1560:3: '\"'
            {
            match('\"'); 

            }


            // Tfm_ctc.g:1560:7: (~ ( '\"' ) )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( ((LA15_0 >= '\u0000' && LA15_0 <= '!')||(LA15_0 >= '#' && LA15_0 <= '\uFFFF')) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // Tfm_ctc.g:
            	    {
            	    if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '!')||(input.LA(1) >= '#' && input.LA(1) <= '\uFFFF') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);


            // Tfm_ctc.g:1560:16: ( '\"' )
            // Tfm_ctc.g:1560:17: '\"'
            {
            match('\"'); 

            }


            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "QUOTED_34_34"

    public void mTokens() throws RecognitionException {
        // Tfm_ctc.g:1:8: ( T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | PATH_TO_FILE | DATE | INTEGER_LITERAL | IDENTIFIER_TOKEN | SL_COMMENT | ML_COMMENT | LINEBREAK | WHITESPACE | QUOTED_34_34 )
        int alt16=25;
        alt16 = dfa16.predict(input);
        switch (alt16) {
            case 1 :
                // Tfm_ctc.g:1:10: T__13
                {
                mT__13(); 


                }
                break;
            case 2 :
                // Tfm_ctc.g:1:16: T__14
                {
                mT__14(); 


                }
                break;
            case 3 :
                // Tfm_ctc.g:1:22: T__15
                {
                mT__15(); 


                }
                break;
            case 4 :
                // Tfm_ctc.g:1:28: T__16
                {
                mT__16(); 


                }
                break;
            case 5 :
                // Tfm_ctc.g:1:34: T__17
                {
                mT__17(); 


                }
                break;
            case 6 :
                // Tfm_ctc.g:1:40: T__18
                {
                mT__18(); 


                }
                break;
            case 7 :
                // Tfm_ctc.g:1:46: T__19
                {
                mT__19(); 


                }
                break;
            case 8 :
                // Tfm_ctc.g:1:52: T__20
                {
                mT__20(); 


                }
                break;
            case 9 :
                // Tfm_ctc.g:1:58: T__21
                {
                mT__21(); 


                }
                break;
            case 10 :
                // Tfm_ctc.g:1:64: T__22
                {
                mT__22(); 


                }
                break;
            case 11 :
                // Tfm_ctc.g:1:70: T__23
                {
                mT__23(); 


                }
                break;
            case 12 :
                // Tfm_ctc.g:1:76: T__24
                {
                mT__24(); 


                }
                break;
            case 13 :
                // Tfm_ctc.g:1:82: T__25
                {
                mT__25(); 


                }
                break;
            case 14 :
                // Tfm_ctc.g:1:88: T__26
                {
                mT__26(); 


                }
                break;
            case 15 :
                // Tfm_ctc.g:1:94: T__27
                {
                mT__27(); 


                }
                break;
            case 16 :
                // Tfm_ctc.g:1:100: T__28
                {
                mT__28(); 


                }
                break;
            case 17 :
                // Tfm_ctc.g:1:106: PATH_TO_FILE
                {
                mPATH_TO_FILE(); 


                }
                break;
            case 18 :
                // Tfm_ctc.g:1:119: DATE
                {
                mDATE(); 


                }
                break;
            case 19 :
                // Tfm_ctc.g:1:124: INTEGER_LITERAL
                {
                mINTEGER_LITERAL(); 


                }
                break;
            case 20 :
                // Tfm_ctc.g:1:140: IDENTIFIER_TOKEN
                {
                mIDENTIFIER_TOKEN(); 


                }
                break;
            case 21 :
                // Tfm_ctc.g:1:157: SL_COMMENT
                {
                mSL_COMMENT(); 


                }
                break;
            case 22 :
                // Tfm_ctc.g:1:168: ML_COMMENT
                {
                mML_COMMENT(); 


                }
                break;
            case 23 :
                // Tfm_ctc.g:1:179: LINEBREAK
                {
                mLINEBREAK(); 


                }
                break;
            case 24 :
                // Tfm_ctc.g:1:189: WHITESPACE
                {
                mWHITESPACE(); 


                }
                break;
            case 25 :
                // Tfm_ctc.g:1:200: QUOTED_34_34
                {
                mQUOTED_34_34(); 


                }
                break;

        }

    }


    protected DFA16 dfa16 = new DFA16(this);
    static final String DFA16_eotS =
        "\5\uffff\1\26\3\uffff\5\20\1\uffff\1\40\11\uffff\6\20\4\uffff\13"+
        "\20\1\64\1\20\1\66\2\20\1\71\1\uffff\1\20\1\uffff\2\20\1\uffff\1"+
        "\20\1\76\1\20\1\100\3\uffff";
    static final String DFA16_eofS =
        "\101\uffff";
    static final String DFA16_minS =
        "\1\11\4\uffff\1\76\1\40\2\uffff\1\164\1\141\1\156\1\157\1\162\1"+
        "\uffff\1\57\1\uffff\1\52\7\uffff\1\145\1\154\1\141\1\151\1\144\1"+
        "\165\4\uffff\1\162\1\163\2\164\2\145\1\156\1\145\1\165\1\151\1\154"+
        "\1\60\1\151\1\60\1\162\1\141\1\60\1\uffff\1\164\1\uffff\1\145\1"+
        "\154\1\uffff\1\171\1\60\1\40\1\60\3\uffff";
    static final String DFA16_maxS =
        "\1\174\4\uffff\1\76\1\172\2\uffff\1\164\1\145\1\156\1\157\1\162"+
        "\1\uffff\1\71\1\uffff\1\57\7\uffff\1\145\1\154\1\141\1\151\1\144"+
        "\1\165\4\uffff\1\162\1\163\2\164\2\145\1\156\1\145\1\165\1\151\1"+
        "\154\1\172\1\151\1\172\1\162\1\141\1\172\1\uffff\1\164\1\uffff\1"+
        "\145\1\154\1\uffff\1\171\1\172\1\40\1\172\3\uffff";
    static final String DFA16_acceptS =
        "\1\uffff\1\1\1\2\1\3\1\4\2\uffff\1\10\1\11\5\uffff\1\20\1\uffff"+
        "\1\24\1\uffff\1\27\1\30\1\31\1\6\1\5\1\7\1\21\6\uffff\1\22\1\23"+
        "\1\25\1\26\21\uffff\1\17\1\uffff\1\13\2\uffff\1\16\4\uffff\1\14"+
        "\1\15\1\12";
    static final String DFA16_specialS =
        "\101\uffff}>";
    static final String[] DFA16_transitionS = {
            "\1\23\1\22\1\uffff\1\23\1\22\22\uffff\1\23\1\1\1\24\3\uffff"+
            "\1\2\1\uffff\1\3\1\4\3\uffff\1\5\1\uffff\1\21\12\17\2\uffff"+
            "\1\6\4\uffff\32\20\1\7\1\uffff\1\10\1\uffff\1\20\1\uffff\4\20"+
            "\1\11\1\12\2\20\1\13\3\20\1\14\6\20\1\15\6\20\1\uffff\1\16",
            "",
            "",
            "",
            "",
            "\1\25",
            "\1\30\14\uffff\1\27\1\uffff\13\30\7\uffff\32\30\4\uffff\1\30"+
            "\1\uffff\32\30",
            "",
            "",
            "\1\31",
            "\1\32\3\uffff\1\33",
            "\1\34",
            "\1\35",
            "\1\36",
            "",
            "\1\37\12\17",
            "",
            "\1\42\4\uffff\1\41",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\43",
            "\1\44",
            "\1\45",
            "\1\46",
            "\1\47",
            "\1\50",
            "",
            "",
            "",
            "",
            "\1\51",
            "\1\52",
            "\1\53",
            "\1\54",
            "\1\55",
            "\1\56",
            "\1\57",
            "\1\60",
            "\1\61",
            "\1\62",
            "\1\63",
            "\12\20\7\uffff\32\20\4\uffff\1\20\1\uffff\32\20",
            "\1\65",
            "\12\20\7\uffff\32\20\4\uffff\1\20\1\uffff\32\20",
            "\1\67",
            "\1\70",
            "\12\20\7\uffff\32\20\4\uffff\1\20\1\uffff\32\20",
            "",
            "\1\72",
            "",
            "\1\73",
            "\1\74",
            "",
            "\1\75",
            "\12\20\7\uffff\32\20\4\uffff\1\20\1\uffff\32\20",
            "\1\77",
            "\12\20\7\uffff\32\20\4\uffff\1\20\1\uffff\32\20",
            "",
            "",
            ""
    };

    static final short[] DFA16_eot = DFA.unpackEncodedString(DFA16_eotS);
    static final short[] DFA16_eof = DFA.unpackEncodedString(DFA16_eofS);
    static final char[] DFA16_min = DFA.unpackEncodedStringToUnsignedChars(DFA16_minS);
    static final char[] DFA16_max = DFA.unpackEncodedStringToUnsignedChars(DFA16_maxS);
    static final short[] DFA16_accept = DFA.unpackEncodedString(DFA16_acceptS);
    static final short[] DFA16_special = DFA.unpackEncodedString(DFA16_specialS);
    static final short[][] DFA16_transition;

    static {
        int numStates = DFA16_transitionS.length;
        DFA16_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA16_transition[i] = DFA.unpackEncodedString(DFA16_transitionS[i]);
        }
    }

    class DFA16 extends DFA {

        public DFA16(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 16;
            this.eot = DFA16_eot;
            this.eof = DFA16_eof;
            this.min = DFA16_min;
            this.max = DFA16_max;
            this.accept = DFA16_accept;
            this.special = DFA16_special;
            this.transition = DFA16_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | PATH_TO_FILE | DATE | INTEGER_LITERAL | IDENTIFIER_TOKEN | SL_COMMENT | ML_COMMENT | LINEBREAK | WHITESPACE | QUOTED_34_34 );";
        }
    }
 

}