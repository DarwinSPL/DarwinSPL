package de.darwinspl.fmec.verification.ui;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.internal.ObjectPluginAction;

public class DwVerifyFMECsFileActionDelegate implements IObjectActionDelegate {
	
	private Shell shell;

	public DwVerifyFMECsFileActionDelegate() {
		
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	@Override
	public void run(IAction action) {
		if(action instanceof ObjectPluginAction) {
			ISelection selection = ((ObjectPluginAction) action).getSelection();
			
			if(selection instanceof TreeSelection) {
				TreePath treePath = ((TreeSelection) selection).getPaths()[0];
				Object file = treePath.getSegment(treePath.getSegmentCount()-1);

				if(file instanceof IFile) {
					DwVerifyFMECsHandler fmecHandler = new DwVerifyFMECsHandler();
					fmecHandler.handleFmecModuleFile((IFile) file);
				}
			}
		}
	}
	
	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		
	}

}









