package de.darwinspl.fmec.verification.ui;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.ICoreRunnable;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;

import de.christophseidl.util.ecore.EcoreIOUtil;
import de.darwinspl.common.eclipse.ui.DwDialogUtil;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.evolution.operation.resource.DwEvolutionOperationModelResourceUtil;
import de.darwinspl.fmec.FeatureModelEvolutionConstraintModel;
import de.darwinspl.temporal.maude.analysis.DwEvolutionChecker;
import de.darwinspl.temporal.maude.analysis.DwEvolutionCheckingResult;

public class DwVerifyFMECsHandler {

	public void handleFmecModuleFile(IFile file) {
		EObject model = EcoreIOUtil.loadModel((IFile) file);
		
		if(model instanceof FeatureModelEvolutionConstraintModel) {
			FeatureModelEvolutionConstraintModel fmecModel = (FeatureModelEvolutionConstraintModel) model;
			DwTemporalFeatureModel tfm = fmecModel.getFeatureModel();
			
			DwEvolutionOperationModelResourceUtil.loadAndCacheOperationsModel(tfm);
			
			Job evalJob = Job.create("Evaluating \"" + ((IFile) file).getName() + "\"...", (ICoreRunnable) monitor -> {
				monitor.beginTask("Verifying FMECs...", 1);
				
				String result = DwEvolutionChecker.getInstance().checkFmecModule(tfm, fmecModel);
				if(result.startsWith("#"))
					result = DwEvolutionCheckingResult.getBetterLookingFormat(result);
				else
					result = "DarwinSPL was not able to establish a connection to the Maude model checking service.\nPlease check your DarwinSPL settings.";
				
				final String niceResult = result;
				
				monitor.worked(1);
				monitor.done();

				Display.getDefault().asyncExec(() -> {
					DwDialogUtil.openDialog("Verification of FMECs", niceResult, MessageDialog.INFORMATION, "Close");
				});
			});
			
			evalJob.setPriority(Job.INTERACTIVE);
			evalJob.schedule();
			
			DwEvolutionOperationModelResourceUtil.removeOperationModelFromCache(tfm);
		}
	}
	
}
