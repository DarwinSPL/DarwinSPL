package de.darwinspl.fmec.verification.ui;


import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.text.source.IVerticalRulerInfo;
import org.eclipse.swt.widgets.Event;
import org.eclipse.ui.IActionDelegate2;
import org.eclipse.ui.IPersistableElement;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.texteditor.AbstractRulerActionDelegate;
import org.eclipse.ui.texteditor.ITextEditor;

public class DwVerifyFMECsEditorActionDelegate extends AbstractRulerActionDelegate implements IActionDelegate2 {
	
	private ITextEditor editor;

	public DwVerifyFMECsEditorActionDelegate() {
	}

	@Override
	protected IAction createAction(ITextEditor editor, IVerticalRulerInfo rulerInfo) {
		this.editor = editor;
		return null;
	}
	
	@Override
	public void runWithEvent(IAction action, Event event) {
		IPersistableElement editorInput = this.editor.getEditorInput().getPersistable();
		if(editorInput instanceof FileEditorInput) {
			IFile triggerFile = ((FileEditorInput) editorInput).getFile();
			
			DwVerifyFMECsHandler fmecHandler = new DwVerifyFMECsHandler();
			fmecHandler.handleFmecModuleFile(triggerFile);
		}
	}

}
