/**
 */
package de.darwinspl.feature;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Named Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.DwNamedElement#getNames <em>Names</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.DwFeaturePackage#getDwNamedElement()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface DwNamedElement extends EObject {
	/**
	 * Returns the value of the '<em><b>Names</b></em>' containment reference list.
	 * The list contents are of type {@link de.darwinspl.feature.DwName}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Names</em>' containment reference list.
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwNamedElement_Names()
	 * @model containment="true"
	 * @generated
	 */
	EList<DwName> getNames();

} // DwNamedElement
