/**
 */
package de.darwinspl.feature;

import de.darwinspl.temporal.DwSingleTemporalIntervalElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Group Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.DwGroupType#getType <em>Type</em>}</li>
 *   <li>{@link de.darwinspl.feature.DwGroupType#getGoup <em>Goup</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.DwFeaturePackage#getDwGroupType()
 * @model
 * @generated
 */
public interface DwGroupType extends DwSingleTemporalIntervalElement {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link de.darwinspl.feature.DwGroupTypeEnum}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see de.darwinspl.feature.DwGroupTypeEnum
	 * @see #setType(DwGroupTypeEnum)
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwGroupType_Type()
	 * @model required="true"
	 * @generated
	 */
	DwGroupTypeEnum getType();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.DwGroupType#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see de.darwinspl.feature.DwGroupTypeEnum
	 * @see #getType()
	 * @generated
	 */
	void setType(DwGroupTypeEnum value);

	/**
	 * Returns the value of the '<em><b>Goup</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.darwinspl.feature.DwGroup#getTypes <em>Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Goup</em>' container reference.
	 * @see #setGoup(DwGroup)
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwGroupType_Goup()
	 * @see de.darwinspl.feature.DwGroup#getTypes
	 * @model opposite="types" required="true" transient="false"
	 * @generated
	 */
	DwGroup getGoup();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.DwGroupType#getGoup <em>Goup</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Goup</em>' container reference.
	 * @see #getGoup()
	 * @generated
	 */
	void setGoup(DwGroup value);

} // DwGroupType
