/**
 */
package de.darwinspl.feature;

import de.darwinspl.temporal.DwSingleTemporalIntervalElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Root Feature Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.DwRootFeatureContainer#getFeature <em>Feature</em>}</li>
 *   <li>{@link de.darwinspl.feature.DwRootFeatureContainer#getFeatureModel <em>Feature Model</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.DwFeaturePackage#getDwRootFeatureContainer()
 * @model
 * @generated
 */
public interface DwRootFeatureContainer extends DwSingleTemporalIntervalElement {
	/**
	 * Returns the value of the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature</em>' reference.
	 * @see #setFeature(DwFeature)
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwRootFeatureContainer_Feature()
	 * @model required="true"
	 * @generated
	 */
	DwFeature getFeature();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.DwRootFeatureContainer#getFeature <em>Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature</em>' reference.
	 * @see #getFeature()
	 * @generated
	 */
	void setFeature(DwFeature value);

	/**
	 * Returns the value of the '<em><b>Feature Model</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.darwinspl.feature.DwTemporalFeatureModel#getRootFeatures <em>Root Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Model</em>' container reference.
	 * @see #setFeatureModel(DwTemporalFeatureModel)
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwRootFeatureContainer_FeatureModel()
	 * @see de.darwinspl.feature.DwTemporalFeatureModel#getRootFeatures
	 * @model opposite="rootFeatures" required="true" transient="false"
	 * @generated
	 */
	DwTemporalFeatureModel getFeatureModel();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.DwRootFeatureContainer#getFeatureModel <em>Feature Model</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature Model</em>' container reference.
	 * @see #getFeatureModel()
	 * @generated
	 */
	void setFeatureModel(DwTemporalFeatureModel value);

} // DwRootFeatureContainer
