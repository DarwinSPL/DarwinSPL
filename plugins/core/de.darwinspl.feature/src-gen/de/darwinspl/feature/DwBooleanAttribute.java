/**
 */
package de.darwinspl.feature;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Boolean Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.DwBooleanAttribute#isDefaultValue <em>Default Value</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.DwFeaturePackage#getDwBooleanAttribute()
 * @model
 * @generated
 */
public interface DwBooleanAttribute extends DwFeatureAttribute {
	/**
	 * Returns the value of the '<em><b>Default Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Value</em>' attribute.
	 * @see #setDefaultValue(boolean)
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwBooleanAttribute_DefaultValue()
	 * @model required="true"
	 * @generated
	 */
	boolean isDefaultValue();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.DwBooleanAttribute#isDefaultValue <em>Default Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Value</em>' attribute.
	 * @see #isDefaultValue()
	 * @generated
	 */
	void setDefaultValue(boolean value);

} // DwBooleanAttribute
