/**
 */
package de.darwinspl.feature;

import de.darwinspl.temporal.DwTemporalModelPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.darwinspl.feature.DwFeatureFactory
 * @model kind="package"
 * @generated
 */
public interface DwFeaturePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "feature";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.darwinspl.de/feature/2.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "feature";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DwFeaturePackage eINSTANCE = de.darwinspl.feature.impl.DwFeaturePackageImpl.init();

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.impl.DwTemporalFeatureModelImpl <em>Dw Temporal Feature Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.impl.DwTemporalFeatureModelImpl
	 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwTemporalFeatureModel()
	 * @generated
	 */
	int DW_TEMPORAL_FEATURE_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_TEMPORAL_FEATURE_MODEL__ID = DwTemporalModelPackage.DW_ELEMENT_WITH_ID__ID;

	/**
	 * The feature id for the '<em><b>Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_TEMPORAL_FEATURE_MODEL__FEATURES = DwTemporalModelPackage.DW_ELEMENT_WITH_ID_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Groups</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_TEMPORAL_FEATURE_MODEL__GROUPS = DwTemporalModelPackage.DW_ELEMENT_WITH_ID_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Enums</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_TEMPORAL_FEATURE_MODEL__ENUMS = DwTemporalModelPackage.DW_ELEMENT_WITH_ID_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Root Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_TEMPORAL_FEATURE_MODEL__ROOT_FEATURES = DwTemporalModelPackage.DW_ELEMENT_WITH_ID_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Dw Temporal Feature Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_TEMPORAL_FEATURE_MODEL_FEATURE_COUNT = DwTemporalModelPackage.DW_ELEMENT_WITH_ID_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_TEMPORAL_FEATURE_MODEL___CREATE_ID = DwTemporalModelPackage.DW_ELEMENT_WITH_ID___CREATE_ID;

	/**
	 * The number of operations of the '<em>Dw Temporal Feature Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_TEMPORAL_FEATURE_MODEL_OPERATION_COUNT = DwTemporalModelPackage.DW_ELEMENT_WITH_ID_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.DwNamedElement <em>Dw Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.DwNamedElement
	 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwNamedElement()
	 * @generated
	 */
	int DW_NAMED_ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>Names</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NAMED_ELEMENT__NAMES = 0;

	/**
	 * The number of structural features of the '<em>Dw Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Dw Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NAMED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.impl.DwNameImpl <em>Dw Name</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.impl.DwNameImpl
	 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwName()
	 * @generated
	 */
	int DW_NAME = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NAME__ID = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Validity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NAME__VALIDITY = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__VALIDITY;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NAME__NAME = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Dw Name</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NAME_FEATURE_COUNT = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NAME___CREATE_ID = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NAME___IS_VALID__DATE = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___IS_VALID__DATE;

	/**
	 * The operation id for the '<em>Get From</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NAME___GET_FROM = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_FROM;

	/**
	 * The operation id for the '<em>Get To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NAME___GET_TO = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_TO;

	/**
	 * The number of operations of the '<em>Dw Name</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NAME_OPERATION_COUNT = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.impl.DwFeatureImpl <em>Dw Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.impl.DwFeatureImpl
	 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwFeature()
	 * @generated
	 */
	int DW_FEATURE = 3;

	/**
	 * The feature id for the '<em><b>Names</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE__NAMES = DW_NAMED_ELEMENT__NAMES;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE__ID = DW_NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Validities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE__VALIDITIES = DW_NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE__FEATURE_MODEL = DW_NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE__TYPES = DW_NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Versions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE__VERSIONS = DW_NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE__ATTRIBUTES = DW_NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Parent Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE__PARENT_OF = DW_NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Group Membership</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE__GROUP_MEMBERSHIP = DW_NAMED_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Dw Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_FEATURE_COUNT = DW_NAMED_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE___CREATE_ID = DW_NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE___IS_VALID__DATE = DW_NAMED_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Optional</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE___IS_OPTIONAL__DATE = DW_NAMED_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Mandatory</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE___IS_MANDATORY__DATE = DW_NAMED_ELEMENT_OPERATION_COUNT + 3;

	/**
	 * The number of operations of the '<em>Dw Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_OPERATION_COUNT = DW_NAMED_ELEMENT_OPERATION_COUNT + 4;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.impl.DwFeatureTypeImpl <em>Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.impl.DwFeatureTypeImpl
	 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwFeatureType()
	 * @generated
	 */
	int DW_FEATURE_TYPE = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_TYPE__ID = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Validity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_TYPE__VALIDITY = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__VALIDITY;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_TYPE__TYPE = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_TYPE__FEATURE = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_TYPE_FEATURE_COUNT = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_TYPE___CREATE_ID = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_TYPE___IS_VALID__DATE = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___IS_VALID__DATE;

	/**
	 * The operation id for the '<em>Get From</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_TYPE___GET_FROM = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_FROM;

	/**
	 * The operation id for the '<em>Get To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_TYPE___GET_TO = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_TO;

	/**
	 * The number of operations of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_TYPE_OPERATION_COUNT = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.impl.DwFeatureVersionImpl <em>Version</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.impl.DwFeatureVersionImpl
	 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwFeatureVersion()
	 * @generated
	 */
	int DW_FEATURE_VERSION = 5;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION__ID = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Validities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION__VALIDITIES = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT__VALIDITIES;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION__NUMBER = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Superseded Version</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION__SUPERSEDED_VERSION = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Superseding Versions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION__SUPERSEDING_VERSIONS = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION__FEATURE = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Version</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_FEATURE_COUNT = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION___CREATE_ID = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION___IS_VALID__DATE = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT___IS_VALID__DATE;

	/**
	 * The number of operations of the '<em>Version</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_OPERATION_COUNT = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.impl.DwFeatureAttributeImpl <em>Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.impl.DwFeatureAttributeImpl
	 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwFeatureAttribute()
	 * @generated
	 */
	int DW_FEATURE_ATTRIBUTE = 6;

	/**
	 * The feature id for the '<em><b>Names</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE__NAMES = DW_NAMED_ELEMENT__NAMES;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE__ID = DW_NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Validities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE__VALIDITIES = DW_NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE__FEATURE = DW_NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Recursive</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE__RECURSIVE = DW_NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Configurable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE__CONFIGURABLE = DW_NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_FEATURE_COUNT = DW_NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE___CREATE_ID = DW_NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE___IS_VALID__DATE = DW_NAMED_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_ATTRIBUTE_OPERATION_COUNT = DW_NAMED_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.impl.DwNumberAttributeImpl <em>Dw Number Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.impl.DwNumberAttributeImpl
	 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwNumberAttribute()
	 * @generated
	 */
	int DW_NUMBER_ATTRIBUTE = 7;

	/**
	 * The feature id for the '<em><b>Names</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NUMBER_ATTRIBUTE__NAMES = DW_FEATURE_ATTRIBUTE__NAMES;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NUMBER_ATTRIBUTE__ID = DW_FEATURE_ATTRIBUTE__ID;

	/**
	 * The feature id for the '<em><b>Validities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NUMBER_ATTRIBUTE__VALIDITIES = DW_FEATURE_ATTRIBUTE__VALIDITIES;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NUMBER_ATTRIBUTE__FEATURE = DW_FEATURE_ATTRIBUTE__FEATURE;

	/**
	 * The feature id for the '<em><b>Recursive</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NUMBER_ATTRIBUTE__RECURSIVE = DW_FEATURE_ATTRIBUTE__RECURSIVE;

	/**
	 * The feature id for the '<em><b>Configurable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NUMBER_ATTRIBUTE__CONFIGURABLE = DW_FEATURE_ATTRIBUTE__CONFIGURABLE;

	/**
	 * The feature id for the '<em><b>Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NUMBER_ATTRIBUTE__MIN = DW_FEATURE_ATTRIBUTE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NUMBER_ATTRIBUTE__MAX = DW_FEATURE_ATTRIBUTE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Default Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NUMBER_ATTRIBUTE__DEFAULT_VALUE = DW_FEATURE_ATTRIBUTE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Dw Number Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NUMBER_ATTRIBUTE_FEATURE_COUNT = DW_FEATURE_ATTRIBUTE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NUMBER_ATTRIBUTE___CREATE_ID = DW_FEATURE_ATTRIBUTE___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NUMBER_ATTRIBUTE___IS_VALID__DATE = DW_FEATURE_ATTRIBUTE___IS_VALID__DATE;

	/**
	 * The number of operations of the '<em>Dw Number Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NUMBER_ATTRIBUTE_OPERATION_COUNT = DW_FEATURE_ATTRIBUTE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.impl.DwBooleanAttributeImpl <em>Dw Boolean Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.impl.DwBooleanAttributeImpl
	 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwBooleanAttribute()
	 * @generated
	 */
	int DW_BOOLEAN_ATTRIBUTE = 8;

	/**
	 * The feature id for the '<em><b>Names</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_BOOLEAN_ATTRIBUTE__NAMES = DW_FEATURE_ATTRIBUTE__NAMES;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_BOOLEAN_ATTRIBUTE__ID = DW_FEATURE_ATTRIBUTE__ID;

	/**
	 * The feature id for the '<em><b>Validities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_BOOLEAN_ATTRIBUTE__VALIDITIES = DW_FEATURE_ATTRIBUTE__VALIDITIES;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_BOOLEAN_ATTRIBUTE__FEATURE = DW_FEATURE_ATTRIBUTE__FEATURE;

	/**
	 * The feature id for the '<em><b>Recursive</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_BOOLEAN_ATTRIBUTE__RECURSIVE = DW_FEATURE_ATTRIBUTE__RECURSIVE;

	/**
	 * The feature id for the '<em><b>Configurable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_BOOLEAN_ATTRIBUTE__CONFIGURABLE = DW_FEATURE_ATTRIBUTE__CONFIGURABLE;

	/**
	 * The feature id for the '<em><b>Default Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_BOOLEAN_ATTRIBUTE__DEFAULT_VALUE = DW_FEATURE_ATTRIBUTE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Dw Boolean Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_BOOLEAN_ATTRIBUTE_FEATURE_COUNT = DW_FEATURE_ATTRIBUTE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_BOOLEAN_ATTRIBUTE___CREATE_ID = DW_FEATURE_ATTRIBUTE___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_BOOLEAN_ATTRIBUTE___IS_VALID__DATE = DW_FEATURE_ATTRIBUTE___IS_VALID__DATE;

	/**
	 * The number of operations of the '<em>Dw Boolean Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_BOOLEAN_ATTRIBUTE_OPERATION_COUNT = DW_FEATURE_ATTRIBUTE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.impl.DwStringAttributeImpl <em>Dw String Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.impl.DwStringAttributeImpl
	 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwStringAttribute()
	 * @generated
	 */
	int DW_STRING_ATTRIBUTE = 9;

	/**
	 * The feature id for the '<em><b>Names</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_STRING_ATTRIBUTE__NAMES = DW_FEATURE_ATTRIBUTE__NAMES;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_STRING_ATTRIBUTE__ID = DW_FEATURE_ATTRIBUTE__ID;

	/**
	 * The feature id for the '<em><b>Validities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_STRING_ATTRIBUTE__VALIDITIES = DW_FEATURE_ATTRIBUTE__VALIDITIES;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_STRING_ATTRIBUTE__FEATURE = DW_FEATURE_ATTRIBUTE__FEATURE;

	/**
	 * The feature id for the '<em><b>Recursive</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_STRING_ATTRIBUTE__RECURSIVE = DW_FEATURE_ATTRIBUTE__RECURSIVE;

	/**
	 * The feature id for the '<em><b>Configurable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_STRING_ATTRIBUTE__CONFIGURABLE = DW_FEATURE_ATTRIBUTE__CONFIGURABLE;

	/**
	 * The feature id for the '<em><b>Default Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_STRING_ATTRIBUTE__DEFAULT_VALUE = DW_FEATURE_ATTRIBUTE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Dw String Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_STRING_ATTRIBUTE_FEATURE_COUNT = DW_FEATURE_ATTRIBUTE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_STRING_ATTRIBUTE___CREATE_ID = DW_FEATURE_ATTRIBUTE___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_STRING_ATTRIBUTE___IS_VALID__DATE = DW_FEATURE_ATTRIBUTE___IS_VALID__DATE;

	/**
	 * The number of operations of the '<em>Dw String Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_STRING_ATTRIBUTE_OPERATION_COUNT = DW_FEATURE_ATTRIBUTE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.impl.DwEnumAttributeImpl <em>Dw Enum Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.impl.DwEnumAttributeImpl
	 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwEnumAttribute()
	 * @generated
	 */
	int DW_ENUM_ATTRIBUTE = 10;

	/**
	 * The feature id for the '<em><b>Names</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_ATTRIBUTE__NAMES = DW_FEATURE_ATTRIBUTE__NAMES;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_ATTRIBUTE__ID = DW_FEATURE_ATTRIBUTE__ID;

	/**
	 * The feature id for the '<em><b>Validities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_ATTRIBUTE__VALIDITIES = DW_FEATURE_ATTRIBUTE__VALIDITIES;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_ATTRIBUTE__FEATURE = DW_FEATURE_ATTRIBUTE__FEATURE;

	/**
	 * The feature id for the '<em><b>Recursive</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_ATTRIBUTE__RECURSIVE = DW_FEATURE_ATTRIBUTE__RECURSIVE;

	/**
	 * The feature id for the '<em><b>Configurable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_ATTRIBUTE__CONFIGURABLE = DW_FEATURE_ATTRIBUTE__CONFIGURABLE;

	/**
	 * The feature id for the '<em><b>Enum</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_ATTRIBUTE__ENUM = DW_FEATURE_ATTRIBUTE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Default Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_ATTRIBUTE__DEFAULT_VALUE = DW_FEATURE_ATTRIBUTE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Dw Enum Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_ATTRIBUTE_FEATURE_COUNT = DW_FEATURE_ATTRIBUTE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_ATTRIBUTE___CREATE_ID = DW_FEATURE_ATTRIBUTE___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_ATTRIBUTE___IS_VALID__DATE = DW_FEATURE_ATTRIBUTE___IS_VALID__DATE;

	/**
	 * The number of operations of the '<em>Dw Enum Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ENUM_ATTRIBUTE_OPERATION_COUNT = DW_FEATURE_ATTRIBUTE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.impl.DwGroupImpl <em>Dw Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.impl.DwGroupImpl
	 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwGroup()
	 * @generated
	 */
	int DW_GROUP = 11;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP__ID = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Validities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP__VALIDITIES = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT__VALIDITIES;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP__FEATURE_MODEL = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP__TYPES = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Child Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP__CHILD_OF = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Parent Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP__PARENT_OF = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Dw Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_FEATURE_COUNT = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP___CREATE_ID = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP___IS_VALID__DATE = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT___IS_VALID__DATE;

	/**
	 * The operation id for the '<em>Is Alternative</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP___IS_ALTERNATIVE__DATE = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Or</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP___IS_OR__DATE = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is And</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP___IS_AND__DATE = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>Dw Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_OPERATION_COUNT = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.impl.DwGroupTypeImpl <em>Dw Group Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.impl.DwGroupTypeImpl
	 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwGroupType()
	 * @generated
	 */
	int DW_GROUP_TYPE = 12;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_TYPE__ID = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Validity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_TYPE__VALIDITY = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__VALIDITY;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_TYPE__TYPE = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Goup</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_TYPE__GOUP = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Dw Group Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_TYPE_FEATURE_COUNT = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_TYPE___CREATE_ID = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_TYPE___IS_VALID__DATE = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___IS_VALID__DATE;

	/**
	 * The operation id for the '<em>Get From</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_TYPE___GET_FROM = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_FROM;

	/**
	 * The operation id for the '<em>Get To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_TYPE___GET_TO = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_TO;

	/**
	 * The number of operations of the '<em>Dw Group Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_TYPE_OPERATION_COUNT = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.impl.DwRootFeatureContainerImpl <em>Dw Root Feature Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.impl.DwRootFeatureContainerImpl
	 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwRootFeatureContainer()
	 * @generated
	 */
	int DW_ROOT_FEATURE_CONTAINER = 13;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ROOT_FEATURE_CONTAINER__ID = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Validity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ROOT_FEATURE_CONTAINER__VALIDITY = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__VALIDITY;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ROOT_FEATURE_CONTAINER__FEATURE = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ROOT_FEATURE_CONTAINER__FEATURE_MODEL = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Dw Root Feature Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ROOT_FEATURE_CONTAINER_FEATURE_COUNT = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ROOT_FEATURE_CONTAINER___CREATE_ID = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ROOT_FEATURE_CONTAINER___IS_VALID__DATE = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___IS_VALID__DATE;

	/**
	 * The operation id for the '<em>Get From</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ROOT_FEATURE_CONTAINER___GET_FROM = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_FROM;

	/**
	 * The operation id for the '<em>Get To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ROOT_FEATURE_CONTAINER___GET_TO = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_TO;

	/**
	 * The number of operations of the '<em>Dw Root Feature Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ROOT_FEATURE_CONTAINER_OPERATION_COUNT = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.impl.DwParentFeatureChildGroupContainerImpl <em>Dw Parent Feature Child Group Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.impl.DwParentFeatureChildGroupContainerImpl
	 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwParentFeatureChildGroupContainer()
	 * @generated
	 */
	int DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER = 14;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__ID = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Validity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__VALIDITY = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__VALIDITY;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__PARENT = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Child Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__CHILD_GROUP = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Dw Parent Feature Child Group Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER_FEATURE_COUNT = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER___CREATE_ID = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER___IS_VALID__DATE = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___IS_VALID__DATE;

	/**
	 * The operation id for the '<em>Get From</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER___GET_FROM = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_FROM;

	/**
	 * The operation id for the '<em>Get To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER___GET_TO = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_TO;

	/**
	 * The number of operations of the '<em>Dw Parent Feature Child Group Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER_OPERATION_COUNT = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.impl.DwGroupCompositionImpl <em>Dw Group Composition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.impl.DwGroupCompositionImpl
	 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwGroupComposition()
	 * @generated
	 */
	int DW_GROUP_COMPOSITION = 15;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_COMPOSITION__ID = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Validity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_COMPOSITION__VALIDITY = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__VALIDITY;

	/**
	 * The feature id for the '<em><b>Composition Of</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_COMPOSITION__COMPOSITION_OF = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Child Features</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_COMPOSITION__CHILD_FEATURES = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Dw Group Composition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_COMPOSITION_FEATURE_COUNT = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_COMPOSITION___CREATE_ID = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_COMPOSITION___IS_VALID__DATE = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___IS_VALID__DATE;

	/**
	 * The operation id for the '<em>Get From</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_COMPOSITION___GET_FROM = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_FROM;

	/**
	 * The operation id for the '<em>Get To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_COMPOSITION___GET_TO = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_TO;

	/**
	 * The number of operations of the '<em>Dw Group Composition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_GROUP_COMPOSITION_OPERATION_COUNT = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.DwFeatureTypeEnum <em>Type Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.DwFeatureTypeEnum
	 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwFeatureTypeEnum()
	 * @generated
	 */
	int DW_FEATURE_TYPE_ENUM = 16;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.DwGroupTypeEnum <em>Dw Group Type Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.DwGroupTypeEnum
	 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwGroupTypeEnum()
	 * @generated
	 */
	int DW_GROUP_TYPE_ENUM = 17;


	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.DwTemporalFeatureModel <em>Dw Temporal Feature Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Temporal Feature Model</em>'.
	 * @see de.darwinspl.feature.DwTemporalFeatureModel
	 * @generated
	 */
	EClass getDwTemporalFeatureModel();

	/**
	 * Returns the meta object for the containment reference list '{@link de.darwinspl.feature.DwTemporalFeatureModel#getFeatures <em>Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Features</em>'.
	 * @see de.darwinspl.feature.DwTemporalFeatureModel#getFeatures()
	 * @see #getDwTemporalFeatureModel()
	 * @generated
	 */
	EReference getDwTemporalFeatureModel_Features();

	/**
	 * Returns the meta object for the containment reference list '{@link de.darwinspl.feature.DwTemporalFeatureModel#getGroups <em>Groups</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Groups</em>'.
	 * @see de.darwinspl.feature.DwTemporalFeatureModel#getGroups()
	 * @see #getDwTemporalFeatureModel()
	 * @generated
	 */
	EReference getDwTemporalFeatureModel_Groups();

	/**
	 * Returns the meta object for the containment reference list '{@link de.darwinspl.feature.DwTemporalFeatureModel#getEnums <em>Enums</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Enums</em>'.
	 * @see de.darwinspl.feature.DwTemporalFeatureModel#getEnums()
	 * @see #getDwTemporalFeatureModel()
	 * @generated
	 */
	EReference getDwTemporalFeatureModel_Enums();

	/**
	 * Returns the meta object for the containment reference list '{@link de.darwinspl.feature.DwTemporalFeatureModel#getRootFeatures <em>Root Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Root Features</em>'.
	 * @see de.darwinspl.feature.DwTemporalFeatureModel#getRootFeatures()
	 * @see #getDwTemporalFeatureModel()
	 * @generated
	 */
	EReference getDwTemporalFeatureModel_RootFeatures();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.DwNamedElement <em>Dw Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Named Element</em>'.
	 * @see de.darwinspl.feature.DwNamedElement
	 * @generated
	 */
	EClass getDwNamedElement();

	/**
	 * Returns the meta object for the containment reference list '{@link de.darwinspl.feature.DwNamedElement#getNames <em>Names</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Names</em>'.
	 * @see de.darwinspl.feature.DwNamedElement#getNames()
	 * @see #getDwNamedElement()
	 * @generated
	 */
	EReference getDwNamedElement_Names();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.DwName <em>Dw Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Name</em>'.
	 * @see de.darwinspl.feature.DwName
	 * @generated
	 */
	EClass getDwName();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.DwName#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.darwinspl.feature.DwName#getName()
	 * @see #getDwName()
	 * @generated
	 */
	EAttribute getDwName_Name();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.DwFeature <em>Dw Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Feature</em>'.
	 * @see de.darwinspl.feature.DwFeature
	 * @generated
	 */
	EClass getDwFeature();

	/**
	 * Returns the meta object for the container reference '{@link de.darwinspl.feature.DwFeature#getFeatureModel <em>Feature Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Feature Model</em>'.
	 * @see de.darwinspl.feature.DwFeature#getFeatureModel()
	 * @see #getDwFeature()
	 * @generated
	 */
	EReference getDwFeature_FeatureModel();

	/**
	 * Returns the meta object for the containment reference list '{@link de.darwinspl.feature.DwFeature#getTypes <em>Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Types</em>'.
	 * @see de.darwinspl.feature.DwFeature#getTypes()
	 * @see #getDwFeature()
	 * @generated
	 */
	EReference getDwFeature_Types();

	/**
	 * Returns the meta object for the containment reference list '{@link de.darwinspl.feature.DwFeature#getVersions <em>Versions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Versions</em>'.
	 * @see de.darwinspl.feature.DwFeature#getVersions()
	 * @see #getDwFeature()
	 * @generated
	 */
	EReference getDwFeature_Versions();

	/**
	 * Returns the meta object for the containment reference list '{@link de.darwinspl.feature.DwFeature#getAttributes <em>Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Attributes</em>'.
	 * @see de.darwinspl.feature.DwFeature#getAttributes()
	 * @see #getDwFeature()
	 * @generated
	 */
	EReference getDwFeature_Attributes();

	/**
	 * Returns the meta object for the containment reference list '{@link de.darwinspl.feature.DwFeature#getParentOf <em>Parent Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parent Of</em>'.
	 * @see de.darwinspl.feature.DwFeature#getParentOf()
	 * @see #getDwFeature()
	 * @generated
	 */
	EReference getDwFeature_ParentOf();

	/**
	 * Returns the meta object for the reference list '{@link de.darwinspl.feature.DwFeature#getGroupMembership <em>Group Membership</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Group Membership</em>'.
	 * @see de.darwinspl.feature.DwFeature#getGroupMembership()
	 * @see #getDwFeature()
	 * @generated
	 */
	EReference getDwFeature_GroupMembership();

	/**
	 * Returns the meta object for the '{@link de.darwinspl.feature.DwFeature#isOptional(java.util.Date) <em>Is Optional</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Optional</em>' operation.
	 * @see de.darwinspl.feature.DwFeature#isOptional(java.util.Date)
	 * @generated
	 */
	EOperation getDwFeature__IsOptional__Date();

	/**
	 * Returns the meta object for the '{@link de.darwinspl.feature.DwFeature#isMandatory(java.util.Date) <em>Is Mandatory</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Mandatory</em>' operation.
	 * @see de.darwinspl.feature.DwFeature#isMandatory(java.util.Date)
	 * @generated
	 */
	EOperation getDwFeature__IsMandatory__Date();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.DwFeatureType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type</em>'.
	 * @see de.darwinspl.feature.DwFeatureType
	 * @generated
	 */
	EClass getDwFeatureType();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.DwFeatureType#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see de.darwinspl.feature.DwFeatureType#getType()
	 * @see #getDwFeatureType()
	 * @generated
	 */
	EAttribute getDwFeatureType_Type();

	/**
	 * Returns the meta object for the container reference '{@link de.darwinspl.feature.DwFeatureType#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Feature</em>'.
	 * @see de.darwinspl.feature.DwFeatureType#getFeature()
	 * @see #getDwFeatureType()
	 * @generated
	 */
	EReference getDwFeatureType_Feature();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.DwFeatureVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Version</em>'.
	 * @see de.darwinspl.feature.DwFeatureVersion
	 * @generated
	 */
	EClass getDwFeatureVersion();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.DwFeatureVersion#getNumber <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number</em>'.
	 * @see de.darwinspl.feature.DwFeatureVersion#getNumber()
	 * @see #getDwFeatureVersion()
	 * @generated
	 */
	EAttribute getDwFeatureVersion_Number();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.DwFeatureVersion#getSupersededVersion <em>Superseded Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Superseded Version</em>'.
	 * @see de.darwinspl.feature.DwFeatureVersion#getSupersededVersion()
	 * @see #getDwFeatureVersion()
	 * @generated
	 */
	EReference getDwFeatureVersion_SupersededVersion();

	/**
	 * Returns the meta object for the reference list '{@link de.darwinspl.feature.DwFeatureVersion#getSupersedingVersions <em>Superseding Versions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Superseding Versions</em>'.
	 * @see de.darwinspl.feature.DwFeatureVersion#getSupersedingVersions()
	 * @see #getDwFeatureVersion()
	 * @generated
	 */
	EReference getDwFeatureVersion_SupersedingVersions();

	/**
	 * Returns the meta object for the container reference '{@link de.darwinspl.feature.DwFeatureVersion#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Feature</em>'.
	 * @see de.darwinspl.feature.DwFeatureVersion#getFeature()
	 * @see #getDwFeatureVersion()
	 * @generated
	 */
	EReference getDwFeatureVersion_Feature();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.DwFeatureAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute</em>'.
	 * @see de.darwinspl.feature.DwFeatureAttribute
	 * @generated
	 */
	EClass getDwFeatureAttribute();

	/**
	 * Returns the meta object for the container reference '{@link de.darwinspl.feature.DwFeatureAttribute#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Feature</em>'.
	 * @see de.darwinspl.feature.DwFeatureAttribute#getFeature()
	 * @see #getDwFeatureAttribute()
	 * @generated
	 */
	EReference getDwFeatureAttribute_Feature();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.DwFeatureAttribute#isRecursive <em>Recursive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Recursive</em>'.
	 * @see de.darwinspl.feature.DwFeatureAttribute#isRecursive()
	 * @see #getDwFeatureAttribute()
	 * @generated
	 */
	EAttribute getDwFeatureAttribute_Recursive();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.DwFeatureAttribute#isConfigurable <em>Configurable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Configurable</em>'.
	 * @see de.darwinspl.feature.DwFeatureAttribute#isConfigurable()
	 * @see #getDwFeatureAttribute()
	 * @generated
	 */
	EAttribute getDwFeatureAttribute_Configurable();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.DwNumberAttribute <em>Dw Number Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Number Attribute</em>'.
	 * @see de.darwinspl.feature.DwNumberAttribute
	 * @generated
	 */
	EClass getDwNumberAttribute();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.DwNumberAttribute#getMin <em>Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min</em>'.
	 * @see de.darwinspl.feature.DwNumberAttribute#getMin()
	 * @see #getDwNumberAttribute()
	 * @generated
	 */
	EAttribute getDwNumberAttribute_Min();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.DwNumberAttribute#getMax <em>Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max</em>'.
	 * @see de.darwinspl.feature.DwNumberAttribute#getMax()
	 * @see #getDwNumberAttribute()
	 * @generated
	 */
	EAttribute getDwNumberAttribute_Max();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.DwNumberAttribute#getDefaultValue <em>Default Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Default Value</em>'.
	 * @see de.darwinspl.feature.DwNumberAttribute#getDefaultValue()
	 * @see #getDwNumberAttribute()
	 * @generated
	 */
	EAttribute getDwNumberAttribute_DefaultValue();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.DwBooleanAttribute <em>Dw Boolean Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Boolean Attribute</em>'.
	 * @see de.darwinspl.feature.DwBooleanAttribute
	 * @generated
	 */
	EClass getDwBooleanAttribute();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.DwBooleanAttribute#isDefaultValue <em>Default Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Default Value</em>'.
	 * @see de.darwinspl.feature.DwBooleanAttribute#isDefaultValue()
	 * @see #getDwBooleanAttribute()
	 * @generated
	 */
	EAttribute getDwBooleanAttribute_DefaultValue();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.DwStringAttribute <em>Dw String Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw String Attribute</em>'.
	 * @see de.darwinspl.feature.DwStringAttribute
	 * @generated
	 */
	EClass getDwStringAttribute();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.DwStringAttribute#getDefaultValue <em>Default Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Default Value</em>'.
	 * @see de.darwinspl.feature.DwStringAttribute#getDefaultValue()
	 * @see #getDwStringAttribute()
	 * @generated
	 */
	EAttribute getDwStringAttribute_DefaultValue();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.DwEnumAttribute <em>Dw Enum Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Enum Attribute</em>'.
	 * @see de.darwinspl.feature.DwEnumAttribute
	 * @generated
	 */
	EClass getDwEnumAttribute();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.DwEnumAttribute#getEnum <em>Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Enum</em>'.
	 * @see de.darwinspl.feature.DwEnumAttribute#getEnum()
	 * @see #getDwEnumAttribute()
	 * @generated
	 */
	EReference getDwEnumAttribute_Enum();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.DwEnumAttribute#getDefaultValue <em>Default Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Default Value</em>'.
	 * @see de.darwinspl.feature.DwEnumAttribute#getDefaultValue()
	 * @see #getDwEnumAttribute()
	 * @generated
	 */
	EReference getDwEnumAttribute_DefaultValue();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.DwGroup <em>Dw Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Group</em>'.
	 * @see de.darwinspl.feature.DwGroup
	 * @generated
	 */
	EClass getDwGroup();

	/**
	 * Returns the meta object for the container reference '{@link de.darwinspl.feature.DwGroup#getFeatureModel <em>Feature Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Feature Model</em>'.
	 * @see de.darwinspl.feature.DwGroup#getFeatureModel()
	 * @see #getDwGroup()
	 * @generated
	 */
	EReference getDwGroup_FeatureModel();

	/**
	 * Returns the meta object for the containment reference list '{@link de.darwinspl.feature.DwGroup#getTypes <em>Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Types</em>'.
	 * @see de.darwinspl.feature.DwGroup#getTypes()
	 * @see #getDwGroup()
	 * @generated
	 */
	EReference getDwGroup_Types();

	/**
	 * Returns the meta object for the reference list '{@link de.darwinspl.feature.DwGroup#getChildOf <em>Child Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Child Of</em>'.
	 * @see de.darwinspl.feature.DwGroup#getChildOf()
	 * @see #getDwGroup()
	 * @generated
	 */
	EReference getDwGroup_ChildOf();

	/**
	 * Returns the meta object for the containment reference list '{@link de.darwinspl.feature.DwGroup#getParentOf <em>Parent Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parent Of</em>'.
	 * @see de.darwinspl.feature.DwGroup#getParentOf()
	 * @see #getDwGroup()
	 * @generated
	 */
	EReference getDwGroup_ParentOf();

	/**
	 * Returns the meta object for the '{@link de.darwinspl.feature.DwGroup#isAlternative(java.util.Date) <em>Is Alternative</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Alternative</em>' operation.
	 * @see de.darwinspl.feature.DwGroup#isAlternative(java.util.Date)
	 * @generated
	 */
	EOperation getDwGroup__IsAlternative__Date();

	/**
	 * Returns the meta object for the '{@link de.darwinspl.feature.DwGroup#isOr(java.util.Date) <em>Is Or</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Or</em>' operation.
	 * @see de.darwinspl.feature.DwGroup#isOr(java.util.Date)
	 * @generated
	 */
	EOperation getDwGroup__IsOr__Date();

	/**
	 * Returns the meta object for the '{@link de.darwinspl.feature.DwGroup#isAnd(java.util.Date) <em>Is And</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is And</em>' operation.
	 * @see de.darwinspl.feature.DwGroup#isAnd(java.util.Date)
	 * @generated
	 */
	EOperation getDwGroup__IsAnd__Date();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.DwGroupType <em>Dw Group Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Group Type</em>'.
	 * @see de.darwinspl.feature.DwGroupType
	 * @generated
	 */
	EClass getDwGroupType();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.feature.DwGroupType#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see de.darwinspl.feature.DwGroupType#getType()
	 * @see #getDwGroupType()
	 * @generated
	 */
	EAttribute getDwGroupType_Type();

	/**
	 * Returns the meta object for the container reference '{@link de.darwinspl.feature.DwGroupType#getGoup <em>Goup</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Goup</em>'.
	 * @see de.darwinspl.feature.DwGroupType#getGoup()
	 * @see #getDwGroupType()
	 * @generated
	 */
	EReference getDwGroupType_Goup();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.DwRootFeatureContainer <em>Dw Root Feature Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Root Feature Container</em>'.
	 * @see de.darwinspl.feature.DwRootFeatureContainer
	 * @generated
	 */
	EClass getDwRootFeatureContainer();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.DwRootFeatureContainer#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Feature</em>'.
	 * @see de.darwinspl.feature.DwRootFeatureContainer#getFeature()
	 * @see #getDwRootFeatureContainer()
	 * @generated
	 */
	EReference getDwRootFeatureContainer_Feature();

	/**
	 * Returns the meta object for the container reference '{@link de.darwinspl.feature.DwRootFeatureContainer#getFeatureModel <em>Feature Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Feature Model</em>'.
	 * @see de.darwinspl.feature.DwRootFeatureContainer#getFeatureModel()
	 * @see #getDwRootFeatureContainer()
	 * @generated
	 */
	EReference getDwRootFeatureContainer_FeatureModel();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.DwParentFeatureChildGroupContainer <em>Dw Parent Feature Child Group Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Parent Feature Child Group Container</em>'.
	 * @see de.darwinspl.feature.DwParentFeatureChildGroupContainer
	 * @generated
	 */
	EClass getDwParentFeatureChildGroupContainer();

	/**
	 * Returns the meta object for the container reference '{@link de.darwinspl.feature.DwParentFeatureChildGroupContainer#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent</em>'.
	 * @see de.darwinspl.feature.DwParentFeatureChildGroupContainer#getParent()
	 * @see #getDwParentFeatureChildGroupContainer()
	 * @generated
	 */
	EReference getDwParentFeatureChildGroupContainer_Parent();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.DwParentFeatureChildGroupContainer#getChildGroup <em>Child Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Child Group</em>'.
	 * @see de.darwinspl.feature.DwParentFeatureChildGroupContainer#getChildGroup()
	 * @see #getDwParentFeatureChildGroupContainer()
	 * @generated
	 */
	EReference getDwParentFeatureChildGroupContainer_ChildGroup();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.DwGroupComposition <em>Dw Group Composition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Group Composition</em>'.
	 * @see de.darwinspl.feature.DwGroupComposition
	 * @generated
	 */
	EClass getDwGroupComposition();

	/**
	 * Returns the meta object for the container reference '{@link de.darwinspl.feature.DwGroupComposition#getCompositionOf <em>Composition Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Composition Of</em>'.
	 * @see de.darwinspl.feature.DwGroupComposition#getCompositionOf()
	 * @see #getDwGroupComposition()
	 * @generated
	 */
	EReference getDwGroupComposition_CompositionOf();

	/**
	 * Returns the meta object for the reference list '{@link de.darwinspl.feature.DwGroupComposition#getChildFeatures <em>Child Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Child Features</em>'.
	 * @see de.darwinspl.feature.DwGroupComposition#getChildFeatures()
	 * @see #getDwGroupComposition()
	 * @generated
	 */
	EReference getDwGroupComposition_ChildFeatures();

	/**
	 * Returns the meta object for enum '{@link de.darwinspl.feature.DwFeatureTypeEnum <em>Type Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Type Enum</em>'.
	 * @see de.darwinspl.feature.DwFeatureTypeEnum
	 * @generated
	 */
	EEnum getDwFeatureTypeEnum();

	/**
	 * Returns the meta object for enum '{@link de.darwinspl.feature.DwGroupTypeEnum <em>Dw Group Type Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Dw Group Type Enum</em>'.
	 * @see de.darwinspl.feature.DwGroupTypeEnum
	 * @generated
	 */
	EEnum getDwGroupTypeEnum();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DwFeatureFactory getDwFeatureFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.impl.DwTemporalFeatureModelImpl <em>Dw Temporal Feature Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.impl.DwTemporalFeatureModelImpl
		 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwTemporalFeatureModel()
		 * @generated
		 */
		EClass DW_TEMPORAL_FEATURE_MODEL = eINSTANCE.getDwTemporalFeatureModel();

		/**
		 * The meta object literal for the '<em><b>Features</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_TEMPORAL_FEATURE_MODEL__FEATURES = eINSTANCE.getDwTemporalFeatureModel_Features();

		/**
		 * The meta object literal for the '<em><b>Groups</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_TEMPORAL_FEATURE_MODEL__GROUPS = eINSTANCE.getDwTemporalFeatureModel_Groups();

		/**
		 * The meta object literal for the '<em><b>Enums</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_TEMPORAL_FEATURE_MODEL__ENUMS = eINSTANCE.getDwTemporalFeatureModel_Enums();

		/**
		 * The meta object literal for the '<em><b>Root Features</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_TEMPORAL_FEATURE_MODEL__ROOT_FEATURES = eINSTANCE.getDwTemporalFeatureModel_RootFeatures();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.DwNamedElement <em>Dw Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.DwNamedElement
		 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwNamedElement()
		 * @generated
		 */
		EClass DW_NAMED_ELEMENT = eINSTANCE.getDwNamedElement();

		/**
		 * The meta object literal for the '<em><b>Names</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_NAMED_ELEMENT__NAMES = eINSTANCE.getDwNamedElement_Names();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.impl.DwNameImpl <em>Dw Name</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.impl.DwNameImpl
		 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwName()
		 * @generated
		 */
		EClass DW_NAME = eINSTANCE.getDwName();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_NAME__NAME = eINSTANCE.getDwName_Name();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.impl.DwFeatureImpl <em>Dw Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.impl.DwFeatureImpl
		 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwFeature()
		 * @generated
		 */
		EClass DW_FEATURE = eINSTANCE.getDwFeature();

		/**
		 * The meta object literal for the '<em><b>Feature Model</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE__FEATURE_MODEL = eINSTANCE.getDwFeature_FeatureModel();

		/**
		 * The meta object literal for the '<em><b>Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE__TYPES = eINSTANCE.getDwFeature_Types();

		/**
		 * The meta object literal for the '<em><b>Versions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE__VERSIONS = eINSTANCE.getDwFeature_Versions();

		/**
		 * The meta object literal for the '<em><b>Attributes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE__ATTRIBUTES = eINSTANCE.getDwFeature_Attributes();

		/**
		 * The meta object literal for the '<em><b>Parent Of</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE__PARENT_OF = eINSTANCE.getDwFeature_ParentOf();

		/**
		 * The meta object literal for the '<em><b>Group Membership</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE__GROUP_MEMBERSHIP = eINSTANCE.getDwFeature_GroupMembership();

		/**
		 * The meta object literal for the '<em><b>Is Optional</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DW_FEATURE___IS_OPTIONAL__DATE = eINSTANCE.getDwFeature__IsOptional__Date();

		/**
		 * The meta object literal for the '<em><b>Is Mandatory</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DW_FEATURE___IS_MANDATORY__DATE = eINSTANCE.getDwFeature__IsMandatory__Date();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.impl.DwFeatureTypeImpl <em>Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.impl.DwFeatureTypeImpl
		 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwFeatureType()
		 * @generated
		 */
		EClass DW_FEATURE_TYPE = eINSTANCE.getDwFeatureType();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_FEATURE_TYPE__TYPE = eINSTANCE.getDwFeatureType_Type();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_TYPE__FEATURE = eINSTANCE.getDwFeatureType_Feature();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.impl.DwFeatureVersionImpl <em>Version</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.impl.DwFeatureVersionImpl
		 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwFeatureVersion()
		 * @generated
		 */
		EClass DW_FEATURE_VERSION = eINSTANCE.getDwFeatureVersion();

		/**
		 * The meta object literal for the '<em><b>Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_FEATURE_VERSION__NUMBER = eINSTANCE.getDwFeatureVersion_Number();

		/**
		 * The meta object literal for the '<em><b>Superseded Version</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_VERSION__SUPERSEDED_VERSION = eINSTANCE.getDwFeatureVersion_SupersededVersion();

		/**
		 * The meta object literal for the '<em><b>Superseding Versions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_VERSION__SUPERSEDING_VERSIONS = eINSTANCE.getDwFeatureVersion_SupersedingVersions();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_VERSION__FEATURE = eINSTANCE.getDwFeatureVersion_Feature();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.impl.DwFeatureAttributeImpl <em>Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.impl.DwFeatureAttributeImpl
		 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwFeatureAttribute()
		 * @generated
		 */
		EClass DW_FEATURE_ATTRIBUTE = eINSTANCE.getDwFeatureAttribute();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_ATTRIBUTE__FEATURE = eINSTANCE.getDwFeatureAttribute_Feature();

		/**
		 * The meta object literal for the '<em><b>Recursive</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_FEATURE_ATTRIBUTE__RECURSIVE = eINSTANCE.getDwFeatureAttribute_Recursive();

		/**
		 * The meta object literal for the '<em><b>Configurable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_FEATURE_ATTRIBUTE__CONFIGURABLE = eINSTANCE.getDwFeatureAttribute_Configurable();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.impl.DwNumberAttributeImpl <em>Dw Number Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.impl.DwNumberAttributeImpl
		 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwNumberAttribute()
		 * @generated
		 */
		EClass DW_NUMBER_ATTRIBUTE = eINSTANCE.getDwNumberAttribute();

		/**
		 * The meta object literal for the '<em><b>Min</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_NUMBER_ATTRIBUTE__MIN = eINSTANCE.getDwNumberAttribute_Min();

		/**
		 * The meta object literal for the '<em><b>Max</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_NUMBER_ATTRIBUTE__MAX = eINSTANCE.getDwNumberAttribute_Max();

		/**
		 * The meta object literal for the '<em><b>Default Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_NUMBER_ATTRIBUTE__DEFAULT_VALUE = eINSTANCE.getDwNumberAttribute_DefaultValue();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.impl.DwBooleanAttributeImpl <em>Dw Boolean Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.impl.DwBooleanAttributeImpl
		 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwBooleanAttribute()
		 * @generated
		 */
		EClass DW_BOOLEAN_ATTRIBUTE = eINSTANCE.getDwBooleanAttribute();

		/**
		 * The meta object literal for the '<em><b>Default Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_BOOLEAN_ATTRIBUTE__DEFAULT_VALUE = eINSTANCE.getDwBooleanAttribute_DefaultValue();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.impl.DwStringAttributeImpl <em>Dw String Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.impl.DwStringAttributeImpl
		 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwStringAttribute()
		 * @generated
		 */
		EClass DW_STRING_ATTRIBUTE = eINSTANCE.getDwStringAttribute();

		/**
		 * The meta object literal for the '<em><b>Default Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_STRING_ATTRIBUTE__DEFAULT_VALUE = eINSTANCE.getDwStringAttribute_DefaultValue();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.impl.DwEnumAttributeImpl <em>Dw Enum Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.impl.DwEnumAttributeImpl
		 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwEnumAttribute()
		 * @generated
		 */
		EClass DW_ENUM_ATTRIBUTE = eINSTANCE.getDwEnumAttribute();

		/**
		 * The meta object literal for the '<em><b>Enum</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_ENUM_ATTRIBUTE__ENUM = eINSTANCE.getDwEnumAttribute_Enum();

		/**
		 * The meta object literal for the '<em><b>Default Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_ENUM_ATTRIBUTE__DEFAULT_VALUE = eINSTANCE.getDwEnumAttribute_DefaultValue();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.impl.DwGroupImpl <em>Dw Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.impl.DwGroupImpl
		 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwGroup()
		 * @generated
		 */
		EClass DW_GROUP = eINSTANCE.getDwGroup();

		/**
		 * The meta object literal for the '<em><b>Feature Model</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_GROUP__FEATURE_MODEL = eINSTANCE.getDwGroup_FeatureModel();

		/**
		 * The meta object literal for the '<em><b>Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_GROUP__TYPES = eINSTANCE.getDwGroup_Types();

		/**
		 * The meta object literal for the '<em><b>Child Of</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_GROUP__CHILD_OF = eINSTANCE.getDwGroup_ChildOf();

		/**
		 * The meta object literal for the '<em><b>Parent Of</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_GROUP__PARENT_OF = eINSTANCE.getDwGroup_ParentOf();

		/**
		 * The meta object literal for the '<em><b>Is Alternative</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DW_GROUP___IS_ALTERNATIVE__DATE = eINSTANCE.getDwGroup__IsAlternative__Date();

		/**
		 * The meta object literal for the '<em><b>Is Or</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DW_GROUP___IS_OR__DATE = eINSTANCE.getDwGroup__IsOr__Date();

		/**
		 * The meta object literal for the '<em><b>Is And</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DW_GROUP___IS_AND__DATE = eINSTANCE.getDwGroup__IsAnd__Date();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.impl.DwGroupTypeImpl <em>Dw Group Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.impl.DwGroupTypeImpl
		 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwGroupType()
		 * @generated
		 */
		EClass DW_GROUP_TYPE = eINSTANCE.getDwGroupType();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_GROUP_TYPE__TYPE = eINSTANCE.getDwGroupType_Type();

		/**
		 * The meta object literal for the '<em><b>Goup</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_GROUP_TYPE__GOUP = eINSTANCE.getDwGroupType_Goup();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.impl.DwRootFeatureContainerImpl <em>Dw Root Feature Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.impl.DwRootFeatureContainerImpl
		 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwRootFeatureContainer()
		 * @generated
		 */
		EClass DW_ROOT_FEATURE_CONTAINER = eINSTANCE.getDwRootFeatureContainer();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_ROOT_FEATURE_CONTAINER__FEATURE = eINSTANCE.getDwRootFeatureContainer_Feature();

		/**
		 * The meta object literal for the '<em><b>Feature Model</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_ROOT_FEATURE_CONTAINER__FEATURE_MODEL = eINSTANCE.getDwRootFeatureContainer_FeatureModel();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.impl.DwParentFeatureChildGroupContainerImpl <em>Dw Parent Feature Child Group Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.impl.DwParentFeatureChildGroupContainerImpl
		 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwParentFeatureChildGroupContainer()
		 * @generated
		 */
		EClass DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER = eINSTANCE.getDwParentFeatureChildGroupContainer();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__PARENT = eINSTANCE.getDwParentFeatureChildGroupContainer_Parent();

		/**
		 * The meta object literal for the '<em><b>Child Group</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__CHILD_GROUP = eINSTANCE.getDwParentFeatureChildGroupContainer_ChildGroup();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.impl.DwGroupCompositionImpl <em>Dw Group Composition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.impl.DwGroupCompositionImpl
		 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwGroupComposition()
		 * @generated
		 */
		EClass DW_GROUP_COMPOSITION = eINSTANCE.getDwGroupComposition();

		/**
		 * The meta object literal for the '<em><b>Composition Of</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_GROUP_COMPOSITION__COMPOSITION_OF = eINSTANCE.getDwGroupComposition_CompositionOf();

		/**
		 * The meta object literal for the '<em><b>Child Features</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_GROUP_COMPOSITION__CHILD_FEATURES = eINSTANCE.getDwGroupComposition_ChildFeatures();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.DwFeatureTypeEnum <em>Type Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.DwFeatureTypeEnum
		 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwFeatureTypeEnum()
		 * @generated
		 */
		EEnum DW_FEATURE_TYPE_ENUM = eINSTANCE.getDwFeatureTypeEnum();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.DwGroupTypeEnum <em>Dw Group Type Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.DwGroupTypeEnum
		 * @see de.darwinspl.feature.impl.DwFeaturePackageImpl#getDwGroupTypeEnum()
		 * @generated
		 */
		EEnum DW_GROUP_TYPE_ENUM = eINSTANCE.getDwGroupTypeEnum();

	}

} //DwFeaturePackage
