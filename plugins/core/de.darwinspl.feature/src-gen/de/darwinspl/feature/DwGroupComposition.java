/**
 */
package de.darwinspl.feature;

import de.darwinspl.temporal.DwSingleTemporalIntervalElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Group Composition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.DwGroupComposition#getCompositionOf <em>Composition Of</em>}</li>
 *   <li>{@link de.darwinspl.feature.DwGroupComposition#getChildFeatures <em>Child Features</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.DwFeaturePackage#getDwGroupComposition()
 * @model
 * @generated
 */
public interface DwGroupComposition extends DwSingleTemporalIntervalElement {
	/**
	 * Returns the value of the '<em><b>Composition Of</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.darwinspl.feature.DwGroup#getParentOf <em>Parent Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Composition Of</em>' container reference.
	 * @see #setCompositionOf(DwGroup)
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwGroupComposition_CompositionOf()
	 * @see de.darwinspl.feature.DwGroup#getParentOf
	 * @model opposite="parentOf" required="true" transient="false"
	 * @generated
	 */
	DwGroup getCompositionOf();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.DwGroupComposition#getCompositionOf <em>Composition Of</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Composition Of</em>' container reference.
	 * @see #getCompositionOf()
	 * @generated
	 */
	void setCompositionOf(DwGroup value);

	/**
	 * Returns the value of the '<em><b>Child Features</b></em>' reference list.
	 * The list contents are of type {@link de.darwinspl.feature.DwFeature}.
	 * It is bidirectional and its opposite is '{@link de.darwinspl.feature.DwFeature#getGroupMembership <em>Group Membership</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Child Features</em>' reference list.
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwGroupComposition_ChildFeatures()
	 * @see de.darwinspl.feature.DwFeature#getGroupMembership
	 * @model opposite="groupMembership" required="true"
	 * @generated
	 */
	EList<DwFeature> getChildFeatures();

} // DwGroupComposition
