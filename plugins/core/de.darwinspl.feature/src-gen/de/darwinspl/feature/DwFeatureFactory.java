/**
 */
package de.darwinspl.feature;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.darwinspl.feature.DwFeaturePackage
 * @generated
 */
public interface DwFeatureFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DwFeatureFactory eINSTANCE = de.darwinspl.feature.impl.DwFeatureFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Dw Temporal Feature Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Temporal Feature Model</em>'.
	 * @generated
	 */
	DwTemporalFeatureModel createDwTemporalFeatureModel();

	/**
	 * Returns a new object of class '<em>Dw Name</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Name</em>'.
	 * @generated
	 */
	DwName createDwName();

	/**
	 * Returns a new object of class '<em>Dw Feature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Feature</em>'.
	 * @generated
	 */
	DwFeature createDwFeature();

	/**
	 * Returns a new object of class '<em>Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Type</em>'.
	 * @generated
	 */
	DwFeatureType createDwFeatureType();

	/**
	 * Returns a new object of class '<em>Version</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Version</em>'.
	 * @generated
	 */
	DwFeatureVersion createDwFeatureVersion();

	/**
	 * Returns a new object of class '<em>Dw Number Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Number Attribute</em>'.
	 * @generated
	 */
	DwNumberAttribute createDwNumberAttribute();

	/**
	 * Returns a new object of class '<em>Dw Boolean Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Boolean Attribute</em>'.
	 * @generated
	 */
	DwBooleanAttribute createDwBooleanAttribute();

	/**
	 * Returns a new object of class '<em>Dw String Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw String Attribute</em>'.
	 * @generated
	 */
	DwStringAttribute createDwStringAttribute();

	/**
	 * Returns a new object of class '<em>Dw Enum Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Enum Attribute</em>'.
	 * @generated
	 */
	DwEnumAttribute createDwEnumAttribute();

	/**
	 * Returns a new object of class '<em>Dw Group</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Group</em>'.
	 * @generated
	 */
	DwGroup createDwGroup();

	/**
	 * Returns a new object of class '<em>Dw Group Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Group Type</em>'.
	 * @generated
	 */
	DwGroupType createDwGroupType();

	/**
	 * Returns a new object of class '<em>Dw Root Feature Container</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Root Feature Container</em>'.
	 * @generated
	 */
	DwRootFeatureContainer createDwRootFeatureContainer();

	/**
	 * Returns a new object of class '<em>Dw Parent Feature Child Group Container</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Parent Feature Child Group Container</em>'.
	 * @generated
	 */
	DwParentFeatureChildGroupContainer createDwParentFeatureChildGroupContainer();

	/**
	 * Returns a new object of class '<em>Dw Group Composition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Group Composition</em>'.
	 * @generated
	 */
	DwGroupComposition createDwGroupComposition();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	DwFeaturePackage getDwFeaturePackage();

} //DwFeatureFactory
