/**
 */
package de.darwinspl.feature;

import de.darwinspl.temporal.DwSingleTemporalIntervalElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Parent Feature Child Group Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.DwParentFeatureChildGroupContainer#getParent <em>Parent</em>}</li>
 *   <li>{@link de.darwinspl.feature.DwParentFeatureChildGroupContainer#getChildGroup <em>Child Group</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.DwFeaturePackage#getDwParentFeatureChildGroupContainer()
 * @model
 * @generated
 */
public interface DwParentFeatureChildGroupContainer extends DwSingleTemporalIntervalElement {
	/**
	 * Returns the value of the '<em><b>Parent</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.darwinspl.feature.DwFeature#getParentOf <em>Parent Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' container reference.
	 * @see #setParent(DwFeature)
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwParentFeatureChildGroupContainer_Parent()
	 * @see de.darwinspl.feature.DwFeature#getParentOf
	 * @model opposite="parentOf" required="true" transient="false"
	 * @generated
	 */
	DwFeature getParent();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.DwParentFeatureChildGroupContainer#getParent <em>Parent</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' container reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(DwFeature value);

	/**
	 * Returns the value of the '<em><b>Child Group</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.darwinspl.feature.DwGroup#getChildOf <em>Child Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Child Group</em>' reference.
	 * @see #setChildGroup(DwGroup)
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwParentFeatureChildGroupContainer_ChildGroup()
	 * @see de.darwinspl.feature.DwGroup#getChildOf
	 * @model opposite="childOf" required="true"
	 * @generated
	 */
	DwGroup getChildGroup();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.DwParentFeatureChildGroupContainer#getChildGroup <em>Child Group</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Child Group</em>' reference.
	 * @see #getChildGroup()
	 * @generated
	 */
	void setChildGroup(DwGroup value);

} // DwParentFeatureChildGroupContainer
