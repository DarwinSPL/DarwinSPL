/**
 */
package de.darwinspl.feature;

import de.darwinspl.temporal.DwMultiTemporalIntervalElement;

import java.util.Date;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.DwFeature#getFeatureModel <em>Feature Model</em>}</li>
 *   <li>{@link de.darwinspl.feature.DwFeature#getTypes <em>Types</em>}</li>
 *   <li>{@link de.darwinspl.feature.DwFeature#getVersions <em>Versions</em>}</li>
 *   <li>{@link de.darwinspl.feature.DwFeature#getAttributes <em>Attributes</em>}</li>
 *   <li>{@link de.darwinspl.feature.DwFeature#getParentOf <em>Parent Of</em>}</li>
 *   <li>{@link de.darwinspl.feature.DwFeature#getGroupMembership <em>Group Membership</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.DwFeaturePackage#getDwFeature()
 * @model
 * @generated
 */
public interface DwFeature extends DwNamedElement, DwMultiTemporalIntervalElement {
	/**
	 * Returns the value of the '<em><b>Feature Model</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.darwinspl.feature.DwTemporalFeatureModel#getFeatures <em>Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Model</em>' container reference.
	 * @see #setFeatureModel(DwTemporalFeatureModel)
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwFeature_FeatureModel()
	 * @see de.darwinspl.feature.DwTemporalFeatureModel#getFeatures
	 * @model opposite="features" required="true" transient="false"
	 * @generated
	 */
	DwTemporalFeatureModel getFeatureModel();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.DwFeature#getFeatureModel <em>Feature Model</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature Model</em>' container reference.
	 * @see #getFeatureModel()
	 * @generated
	 */
	void setFeatureModel(DwTemporalFeatureModel value);

	/**
	 * Returns the value of the '<em><b>Types</b></em>' containment reference list.
	 * The list contents are of type {@link de.darwinspl.feature.DwFeatureType}.
	 * It is bidirectional and its opposite is '{@link de.darwinspl.feature.DwFeatureType#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Types</em>' containment reference list.
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwFeature_Types()
	 * @see de.darwinspl.feature.DwFeatureType#getFeature
	 * @model opposite="feature" containment="true" required="true"
	 * @generated
	 */
	EList<DwFeatureType> getTypes();

	/**
	 * Returns the value of the '<em><b>Versions</b></em>' containment reference list.
	 * The list contents are of type {@link de.darwinspl.feature.DwFeatureVersion}.
	 * It is bidirectional and its opposite is '{@link de.darwinspl.feature.DwFeatureVersion#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Versions</em>' containment reference list.
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwFeature_Versions()
	 * @see de.darwinspl.feature.DwFeatureVersion#getFeature
	 * @model opposite="feature" containment="true"
	 * @generated
	 */
	EList<DwFeatureVersion> getVersions();

	/**
	 * Returns the value of the '<em><b>Attributes</b></em>' containment reference list.
	 * The list contents are of type {@link de.darwinspl.feature.DwFeatureAttribute}.
	 * It is bidirectional and its opposite is '{@link de.darwinspl.feature.DwFeatureAttribute#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attributes</em>' containment reference list.
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwFeature_Attributes()
	 * @see de.darwinspl.feature.DwFeatureAttribute#getFeature
	 * @model opposite="feature" containment="true"
	 * @generated
	 */
	EList<DwFeatureAttribute> getAttributes();

	/**
	 * Returns the value of the '<em><b>Parent Of</b></em>' containment reference list.
	 * The list contents are of type {@link de.darwinspl.feature.DwParentFeatureChildGroupContainer}.
	 * It is bidirectional and its opposite is '{@link de.darwinspl.feature.DwParentFeatureChildGroupContainer#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Of</em>' containment reference list.
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwFeature_ParentOf()
	 * @see de.darwinspl.feature.DwParentFeatureChildGroupContainer#getParent
	 * @model opposite="parent" containment="true"
	 * @generated
	 */
	EList<DwParentFeatureChildGroupContainer> getParentOf();

	/**
	 * Returns the value of the '<em><b>Group Membership</b></em>' reference list.
	 * The list contents are of type {@link de.darwinspl.feature.DwGroupComposition}.
	 * It is bidirectional and its opposite is '{@link de.darwinspl.feature.DwGroupComposition#getChildFeatures <em>Child Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group Membership</em>' reference list.
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwFeature_GroupMembership()
	 * @see de.darwinspl.feature.DwGroupComposition#getChildFeatures
	 * @model opposite="childFeatures"
	 * @generated
	 */
	EList<DwGroupComposition> getGroupMembership();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" dateRequired="true"
	 * @generated
	 */
	boolean isOptional(Date date);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" dateRequired="true"
	 * @generated
	 */
	boolean isMandatory(Date date);

} // DwFeature
