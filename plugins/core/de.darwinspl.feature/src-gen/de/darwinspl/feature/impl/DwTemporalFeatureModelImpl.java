/**
 */
package de.darwinspl.feature.impl;

import de.darwinspl.datatypes.DwEnum;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeaturePackage;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwRootFeatureContainer;
import de.darwinspl.feature.DwTemporalFeatureModel;

import de.darwinspl.temporal.impl.DwElementWithIdImpl;
import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Temporal Feature Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.impl.DwTemporalFeatureModelImpl#getFeatures <em>Features</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwTemporalFeatureModelImpl#getGroups <em>Groups</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwTemporalFeatureModelImpl#getEnums <em>Enums</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwTemporalFeatureModelImpl#getRootFeatures <em>Root Features</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwTemporalFeatureModelImpl extends DwElementWithIdImpl implements DwTemporalFeatureModel {
	/**
	 * The cached value of the '{@link #getFeatures() <em>Features</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatures()
	 * @generated
	 * @ordered
	 */
	protected EList<DwFeature> features;

	/**
	 * The cached value of the '{@link #getGroups() <em>Groups</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroups()
	 * @generated
	 * @ordered
	 */
	protected EList<DwGroup> groups;

	/**
	 * The cached value of the '{@link #getEnums() <em>Enums</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnums()
	 * @generated
	 * @ordered
	 */
	protected EList<DwEnum> enums;

	/**
	 * The cached value of the '{@link #getRootFeatures() <em>Root Features</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootFeatures()
	 * @generated
	 * @ordered
	 */
	protected EList<DwRootFeatureContainer> rootFeatures;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwTemporalFeatureModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwFeaturePackage.Literals.DW_TEMPORAL_FEATURE_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwFeature> getFeatures() {
		if (features == null) {
			features = new EObjectContainmentWithInverseEList<DwFeature>(DwFeature.class, this, DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__FEATURES, DwFeaturePackage.DW_FEATURE__FEATURE_MODEL);
		}
		return features;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwGroup> getGroups() {
		if (groups == null) {
			groups = new EObjectContainmentWithInverseEList<DwGroup>(DwGroup.class, this, DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__GROUPS, DwFeaturePackage.DW_GROUP__FEATURE_MODEL);
		}
		return groups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwEnum> getEnums() {
		if (enums == null) {
			enums = new EObjectContainmentEList<DwEnum>(DwEnum.class, this, DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__ENUMS);
		}
		return enums;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwRootFeatureContainer> getRootFeatures() {
		if (rootFeatures == null) {
			rootFeatures = new EObjectContainmentWithInverseEList<DwRootFeatureContainer>(DwRootFeatureContainer.class, this, DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__ROOT_FEATURES, DwFeaturePackage.DW_ROOT_FEATURE_CONTAINER__FEATURE_MODEL);
		}
		return rootFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__FEATURES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getFeatures()).basicAdd(otherEnd, msgs);
			case DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__GROUPS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getGroups()).basicAdd(otherEnd, msgs);
			case DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__ROOT_FEATURES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getRootFeatures()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__FEATURES:
				return ((InternalEList<?>)getFeatures()).basicRemove(otherEnd, msgs);
			case DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__GROUPS:
				return ((InternalEList<?>)getGroups()).basicRemove(otherEnd, msgs);
			case DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__ENUMS:
				return ((InternalEList<?>)getEnums()).basicRemove(otherEnd, msgs);
			case DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__ROOT_FEATURES:
				return ((InternalEList<?>)getRootFeatures()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__FEATURES:
				return getFeatures();
			case DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__GROUPS:
				return getGroups();
			case DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__ENUMS:
				return getEnums();
			case DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__ROOT_FEATURES:
				return getRootFeatures();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__FEATURES:
				getFeatures().clear();
				getFeatures().addAll((Collection<? extends DwFeature>)newValue);
				return;
			case DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__GROUPS:
				getGroups().clear();
				getGroups().addAll((Collection<? extends DwGroup>)newValue);
				return;
			case DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__ENUMS:
				getEnums().clear();
				getEnums().addAll((Collection<? extends DwEnum>)newValue);
				return;
			case DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__ROOT_FEATURES:
				getRootFeatures().clear();
				getRootFeatures().addAll((Collection<? extends DwRootFeatureContainer>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__FEATURES:
				getFeatures().clear();
				return;
			case DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__GROUPS:
				getGroups().clear();
				return;
			case DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__ENUMS:
				getEnums().clear();
				return;
			case DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__ROOT_FEATURES:
				getRootFeatures().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__FEATURES:
				return features != null && !features.isEmpty();
			case DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__GROUPS:
				return groups != null && !groups.isEmpty();
			case DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__ENUMS:
				return enums != null && !enums.isEmpty();
			case DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__ROOT_FEATURES:
				return rootFeatures != null && !rootFeatures.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DwTemporalFeatureModelImpl
