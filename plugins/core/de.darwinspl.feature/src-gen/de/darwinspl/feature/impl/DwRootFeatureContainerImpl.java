/**
 */
package de.darwinspl.feature.impl;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeaturePackage;
import de.darwinspl.feature.DwRootFeatureContainer;
import de.darwinspl.feature.DwTemporalFeatureModel;

import de.darwinspl.temporal.impl.DwSingleTemporalIntervalElementImpl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Root Feature Container</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.impl.DwRootFeatureContainerImpl#getFeature <em>Feature</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwRootFeatureContainerImpl#getFeatureModel <em>Feature Model</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwRootFeatureContainerImpl extends DwSingleTemporalIntervalElementImpl implements DwRootFeatureContainer {
	/**
	 * The cached value of the '{@link #getFeature() <em>Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeature()
	 * @generated
	 * @ordered
	 */
	protected DwFeature feature;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwRootFeatureContainerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwFeaturePackage.Literals.DW_ROOT_FEATURE_CONTAINER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeature getFeature() {
		if (feature != null && feature.eIsProxy()) {
			InternalEObject oldFeature = (InternalEObject)feature;
			feature = (DwFeature)eResolveProxy(oldFeature);
			if (feature != oldFeature) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwFeaturePackage.DW_ROOT_FEATURE_CONTAINER__FEATURE, oldFeature, feature));
			}
		}
		return feature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwFeature basicGetFeature() {
		return feature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFeature(DwFeature newFeature) {
		DwFeature oldFeature = feature;
		feature = newFeature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwFeaturePackage.DW_ROOT_FEATURE_CONTAINER__FEATURE, oldFeature, feature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwTemporalFeatureModel getFeatureModel() {
		if (eContainerFeatureID() != DwFeaturePackage.DW_ROOT_FEATURE_CONTAINER__FEATURE_MODEL) return null;
		return (DwTemporalFeatureModel)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFeatureModel(DwTemporalFeatureModel newFeatureModel, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newFeatureModel, DwFeaturePackage.DW_ROOT_FEATURE_CONTAINER__FEATURE_MODEL, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFeatureModel(DwTemporalFeatureModel newFeatureModel) {
		if (newFeatureModel != eInternalContainer() || (eContainerFeatureID() != DwFeaturePackage.DW_ROOT_FEATURE_CONTAINER__FEATURE_MODEL && newFeatureModel != null)) {
			if (EcoreUtil.isAncestor(this, newFeatureModel))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newFeatureModel != null)
				msgs = ((InternalEObject)newFeatureModel).eInverseAdd(this, DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__ROOT_FEATURES, DwTemporalFeatureModel.class, msgs);
			msgs = basicSetFeatureModel(newFeatureModel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwFeaturePackage.DW_ROOT_FEATURE_CONTAINER__FEATURE_MODEL, newFeatureModel, newFeatureModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwFeaturePackage.DW_ROOT_FEATURE_CONTAINER__FEATURE_MODEL:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetFeatureModel((DwTemporalFeatureModel)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwFeaturePackage.DW_ROOT_FEATURE_CONTAINER__FEATURE_MODEL:
				return basicSetFeatureModel(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DwFeaturePackage.DW_ROOT_FEATURE_CONTAINER__FEATURE_MODEL:
				return eInternalContainer().eInverseRemove(this, DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__ROOT_FEATURES, DwTemporalFeatureModel.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwFeaturePackage.DW_ROOT_FEATURE_CONTAINER__FEATURE:
				if (resolve) return getFeature();
				return basicGetFeature();
			case DwFeaturePackage.DW_ROOT_FEATURE_CONTAINER__FEATURE_MODEL:
				return getFeatureModel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwFeaturePackage.DW_ROOT_FEATURE_CONTAINER__FEATURE:
				setFeature((DwFeature)newValue);
				return;
			case DwFeaturePackage.DW_ROOT_FEATURE_CONTAINER__FEATURE_MODEL:
				setFeatureModel((DwTemporalFeatureModel)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwFeaturePackage.DW_ROOT_FEATURE_CONTAINER__FEATURE:
				setFeature((DwFeature)null);
				return;
			case DwFeaturePackage.DW_ROOT_FEATURE_CONTAINER__FEATURE_MODEL:
				setFeatureModel((DwTemporalFeatureModel)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwFeaturePackage.DW_ROOT_FEATURE_CONTAINER__FEATURE:
				return feature != null;
			case DwFeaturePackage.DW_ROOT_FEATURE_CONTAINER__FEATURE_MODEL:
				return getFeatureModel() != null;
		}
		return super.eIsSet(featureID);
	}

} //DwRootFeatureContainerImpl
