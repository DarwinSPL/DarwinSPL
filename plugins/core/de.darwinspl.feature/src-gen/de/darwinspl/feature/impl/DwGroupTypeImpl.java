/**
 */
package de.darwinspl.feature.impl;

import de.darwinspl.feature.DwFeaturePackage;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupType;
import de.darwinspl.feature.DwGroupTypeEnum;

import de.darwinspl.temporal.impl.DwSingleTemporalIntervalElementImpl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Group Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.impl.DwGroupTypeImpl#getType <em>Type</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwGroupTypeImpl#getGoup <em>Goup</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwGroupTypeImpl extends DwSingleTemporalIntervalElementImpl implements DwGroupType {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final DwGroupTypeEnum TYPE_EDEFAULT = DwGroupTypeEnum.AND;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected DwGroupTypeEnum type = TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwGroupTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwFeaturePackage.Literals.DW_GROUP_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroupTypeEnum getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setType(DwGroupTypeEnum newType) {
		DwGroupTypeEnum oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwFeaturePackage.DW_GROUP_TYPE__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroup getGoup() {
		if (eContainerFeatureID() != DwFeaturePackage.DW_GROUP_TYPE__GOUP) return null;
		return (DwGroup)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGoup(DwGroup newGoup, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newGoup, DwFeaturePackage.DW_GROUP_TYPE__GOUP, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setGoup(DwGroup newGoup) {
		if (newGoup != eInternalContainer() || (eContainerFeatureID() != DwFeaturePackage.DW_GROUP_TYPE__GOUP && newGoup != null)) {
			if (EcoreUtil.isAncestor(this, newGoup))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newGoup != null)
				msgs = ((InternalEObject)newGoup).eInverseAdd(this, DwFeaturePackage.DW_GROUP__TYPES, DwGroup.class, msgs);
			msgs = basicSetGoup(newGoup, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwFeaturePackage.DW_GROUP_TYPE__GOUP, newGoup, newGoup));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwFeaturePackage.DW_GROUP_TYPE__GOUP:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetGoup((DwGroup)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwFeaturePackage.DW_GROUP_TYPE__GOUP:
				return basicSetGoup(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DwFeaturePackage.DW_GROUP_TYPE__GOUP:
				return eInternalContainer().eInverseRemove(this, DwFeaturePackage.DW_GROUP__TYPES, DwGroup.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwFeaturePackage.DW_GROUP_TYPE__TYPE:
				return getType();
			case DwFeaturePackage.DW_GROUP_TYPE__GOUP:
				return getGoup();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwFeaturePackage.DW_GROUP_TYPE__TYPE:
				setType((DwGroupTypeEnum)newValue);
				return;
			case DwFeaturePackage.DW_GROUP_TYPE__GOUP:
				setGoup((DwGroup)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwFeaturePackage.DW_GROUP_TYPE__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case DwFeaturePackage.DW_GROUP_TYPE__GOUP:
				setGoup((DwGroup)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwFeaturePackage.DW_GROUP_TYPE__TYPE:
				return type != TYPE_EDEFAULT;
			case DwFeaturePackage.DW_GROUP_TYPE__GOUP:
				return getGoup() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //DwGroupTypeImpl
