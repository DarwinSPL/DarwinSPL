/**
 */
package de.darwinspl.feature.impl;

import de.darwinspl.datatypes.DwDatatypesPackage;

import de.darwinspl.feature.DwBooleanAttribute;
import de.darwinspl.feature.DwEnumAttribute;
import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureAttribute;
import de.darwinspl.feature.DwFeatureFactory;
import de.darwinspl.feature.DwFeaturePackage;
import de.darwinspl.feature.DwFeatureType;
import de.darwinspl.feature.DwFeatureTypeEnum;
import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupComposition;
import de.darwinspl.feature.DwGroupType;
import de.darwinspl.feature.DwGroupTypeEnum;
import de.darwinspl.feature.DwName;
import de.darwinspl.feature.DwNamedElement;
import de.darwinspl.feature.DwNumberAttribute;
import de.darwinspl.feature.DwParentFeatureChildGroupContainer;
import de.darwinspl.feature.DwRootFeatureContainer;
import de.darwinspl.feature.DwStringAttribute;
import de.darwinspl.feature.DwTemporalFeatureModel;

import de.darwinspl.temporal.DwTemporalModelPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DwFeaturePackageImpl extends EPackageImpl implements DwFeaturePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwTemporalFeatureModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwNamedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwNameEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureVersionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwNumberAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwBooleanAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwStringAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwEnumAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwGroupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwGroupTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwRootFeatureContainerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwParentFeatureChildGroupContainerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwGroupCompositionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dwFeatureTypeEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dwGroupTypeEnumEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.darwinspl.feature.DwFeaturePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private DwFeaturePackageImpl() {
		super(eNS_URI, DwFeatureFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link DwFeaturePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static DwFeaturePackage init() {
		if (isInited) return (DwFeaturePackage)EPackage.Registry.INSTANCE.getEPackage(DwFeaturePackage.eNS_URI);

		// Obtain or create and register package
		Object registeredDwFeaturePackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		DwFeaturePackageImpl theDwFeaturePackage = registeredDwFeaturePackage instanceof DwFeaturePackageImpl ? (DwFeaturePackageImpl)registeredDwFeaturePackage : new DwFeaturePackageImpl();

		isInited = true;

		// Initialize simple dependencies
		DwDatatypesPackage.eINSTANCE.eClass();
		DwTemporalModelPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theDwFeaturePackage.createPackageContents();

		// Initialize created meta-data
		theDwFeaturePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theDwFeaturePackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(DwFeaturePackage.eNS_URI, theDwFeaturePackage);
		return theDwFeaturePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwTemporalFeatureModel() {
		return dwTemporalFeatureModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwTemporalFeatureModel_Features() {
		return (EReference)dwTemporalFeatureModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwTemporalFeatureModel_Groups() {
		return (EReference)dwTemporalFeatureModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwTemporalFeatureModel_Enums() {
		return (EReference)dwTemporalFeatureModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwTemporalFeatureModel_RootFeatures() {
		return (EReference)dwTemporalFeatureModelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwNamedElement() {
		return dwNamedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwNamedElement_Names() {
		return (EReference)dwNamedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwName() {
		return dwNameEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwName_Name() {
		return (EAttribute)dwNameEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeature() {
		return dwFeatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeature_FeatureModel() {
		return (EReference)dwFeatureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeature_Types() {
		return (EReference)dwFeatureEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeature_Versions() {
		return (EReference)dwFeatureEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeature_Attributes() {
		return (EReference)dwFeatureEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeature_ParentOf() {
		return (EReference)dwFeatureEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeature_GroupMembership() {
		return (EReference)dwFeatureEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getDwFeature__IsOptional__Date() {
		return dwFeatureEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getDwFeature__IsMandatory__Date() {
		return dwFeatureEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureType() {
		return dwFeatureTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwFeatureType_Type() {
		return (EAttribute)dwFeatureTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureType_Feature() {
		return (EReference)dwFeatureTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureVersion() {
		return dwFeatureVersionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwFeatureVersion_Number() {
		return (EAttribute)dwFeatureVersionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureVersion_SupersededVersion() {
		return (EReference)dwFeatureVersionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureVersion_SupersedingVersions() {
		return (EReference)dwFeatureVersionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureVersion_Feature() {
		return (EReference)dwFeatureVersionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureAttribute() {
		return dwFeatureAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureAttribute_Feature() {
		return (EReference)dwFeatureAttributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwFeatureAttribute_Recursive() {
		return (EAttribute)dwFeatureAttributeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwFeatureAttribute_Configurable() {
		return (EAttribute)dwFeatureAttributeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwNumberAttribute() {
		return dwNumberAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwNumberAttribute_Min() {
		return (EAttribute)dwNumberAttributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwNumberAttribute_Max() {
		return (EAttribute)dwNumberAttributeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwNumberAttribute_DefaultValue() {
		return (EAttribute)dwNumberAttributeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwBooleanAttribute() {
		return dwBooleanAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwBooleanAttribute_DefaultValue() {
		return (EAttribute)dwBooleanAttributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwStringAttribute() {
		return dwStringAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwStringAttribute_DefaultValue() {
		return (EAttribute)dwStringAttributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwEnumAttribute() {
		return dwEnumAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwEnumAttribute_Enum() {
		return (EReference)dwEnumAttributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwEnumAttribute_DefaultValue() {
		return (EReference)dwEnumAttributeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwGroup() {
		return dwGroupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwGroup_FeatureModel() {
		return (EReference)dwGroupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwGroup_Types() {
		return (EReference)dwGroupEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwGroup_ChildOf() {
		return (EReference)dwGroupEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwGroup_ParentOf() {
		return (EReference)dwGroupEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getDwGroup__IsAlternative__Date() {
		return dwGroupEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getDwGroup__IsOr__Date() {
		return dwGroupEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getDwGroup__IsAnd__Date() {
		return dwGroupEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwGroupType() {
		return dwGroupTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwGroupType_Type() {
		return (EAttribute)dwGroupTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwGroupType_Goup() {
		return (EReference)dwGroupTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwRootFeatureContainer() {
		return dwRootFeatureContainerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwRootFeatureContainer_Feature() {
		return (EReference)dwRootFeatureContainerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwRootFeatureContainer_FeatureModel() {
		return (EReference)dwRootFeatureContainerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwParentFeatureChildGroupContainer() {
		return dwParentFeatureChildGroupContainerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwParentFeatureChildGroupContainer_Parent() {
		return (EReference)dwParentFeatureChildGroupContainerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwParentFeatureChildGroupContainer_ChildGroup() {
		return (EReference)dwParentFeatureChildGroupContainerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwGroupComposition() {
		return dwGroupCompositionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwGroupComposition_CompositionOf() {
		return (EReference)dwGroupCompositionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwGroupComposition_ChildFeatures() {
		return (EReference)dwGroupCompositionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getDwFeatureTypeEnum() {
		return dwFeatureTypeEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getDwGroupTypeEnum() {
		return dwGroupTypeEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureFactory getDwFeatureFactory() {
		return (DwFeatureFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		dwTemporalFeatureModelEClass = createEClass(DW_TEMPORAL_FEATURE_MODEL);
		createEReference(dwTemporalFeatureModelEClass, DW_TEMPORAL_FEATURE_MODEL__FEATURES);
		createEReference(dwTemporalFeatureModelEClass, DW_TEMPORAL_FEATURE_MODEL__GROUPS);
		createEReference(dwTemporalFeatureModelEClass, DW_TEMPORAL_FEATURE_MODEL__ENUMS);
		createEReference(dwTemporalFeatureModelEClass, DW_TEMPORAL_FEATURE_MODEL__ROOT_FEATURES);

		dwNamedElementEClass = createEClass(DW_NAMED_ELEMENT);
		createEReference(dwNamedElementEClass, DW_NAMED_ELEMENT__NAMES);

		dwNameEClass = createEClass(DW_NAME);
		createEAttribute(dwNameEClass, DW_NAME__NAME);

		dwFeatureEClass = createEClass(DW_FEATURE);
		createEReference(dwFeatureEClass, DW_FEATURE__FEATURE_MODEL);
		createEReference(dwFeatureEClass, DW_FEATURE__TYPES);
		createEReference(dwFeatureEClass, DW_FEATURE__VERSIONS);
		createEReference(dwFeatureEClass, DW_FEATURE__ATTRIBUTES);
		createEReference(dwFeatureEClass, DW_FEATURE__PARENT_OF);
		createEReference(dwFeatureEClass, DW_FEATURE__GROUP_MEMBERSHIP);
		createEOperation(dwFeatureEClass, DW_FEATURE___IS_OPTIONAL__DATE);
		createEOperation(dwFeatureEClass, DW_FEATURE___IS_MANDATORY__DATE);

		dwFeatureTypeEClass = createEClass(DW_FEATURE_TYPE);
		createEAttribute(dwFeatureTypeEClass, DW_FEATURE_TYPE__TYPE);
		createEReference(dwFeatureTypeEClass, DW_FEATURE_TYPE__FEATURE);

		dwFeatureVersionEClass = createEClass(DW_FEATURE_VERSION);
		createEAttribute(dwFeatureVersionEClass, DW_FEATURE_VERSION__NUMBER);
		createEReference(dwFeatureVersionEClass, DW_FEATURE_VERSION__SUPERSEDED_VERSION);
		createEReference(dwFeatureVersionEClass, DW_FEATURE_VERSION__SUPERSEDING_VERSIONS);
		createEReference(dwFeatureVersionEClass, DW_FEATURE_VERSION__FEATURE);

		dwFeatureAttributeEClass = createEClass(DW_FEATURE_ATTRIBUTE);
		createEReference(dwFeatureAttributeEClass, DW_FEATURE_ATTRIBUTE__FEATURE);
		createEAttribute(dwFeatureAttributeEClass, DW_FEATURE_ATTRIBUTE__RECURSIVE);
		createEAttribute(dwFeatureAttributeEClass, DW_FEATURE_ATTRIBUTE__CONFIGURABLE);

		dwNumberAttributeEClass = createEClass(DW_NUMBER_ATTRIBUTE);
		createEAttribute(dwNumberAttributeEClass, DW_NUMBER_ATTRIBUTE__MIN);
		createEAttribute(dwNumberAttributeEClass, DW_NUMBER_ATTRIBUTE__MAX);
		createEAttribute(dwNumberAttributeEClass, DW_NUMBER_ATTRIBUTE__DEFAULT_VALUE);

		dwBooleanAttributeEClass = createEClass(DW_BOOLEAN_ATTRIBUTE);
		createEAttribute(dwBooleanAttributeEClass, DW_BOOLEAN_ATTRIBUTE__DEFAULT_VALUE);

		dwStringAttributeEClass = createEClass(DW_STRING_ATTRIBUTE);
		createEAttribute(dwStringAttributeEClass, DW_STRING_ATTRIBUTE__DEFAULT_VALUE);

		dwEnumAttributeEClass = createEClass(DW_ENUM_ATTRIBUTE);
		createEReference(dwEnumAttributeEClass, DW_ENUM_ATTRIBUTE__ENUM);
		createEReference(dwEnumAttributeEClass, DW_ENUM_ATTRIBUTE__DEFAULT_VALUE);

		dwGroupEClass = createEClass(DW_GROUP);
		createEReference(dwGroupEClass, DW_GROUP__FEATURE_MODEL);
		createEReference(dwGroupEClass, DW_GROUP__TYPES);
		createEReference(dwGroupEClass, DW_GROUP__CHILD_OF);
		createEReference(dwGroupEClass, DW_GROUP__PARENT_OF);
		createEOperation(dwGroupEClass, DW_GROUP___IS_ALTERNATIVE__DATE);
		createEOperation(dwGroupEClass, DW_GROUP___IS_OR__DATE);
		createEOperation(dwGroupEClass, DW_GROUP___IS_AND__DATE);

		dwGroupTypeEClass = createEClass(DW_GROUP_TYPE);
		createEAttribute(dwGroupTypeEClass, DW_GROUP_TYPE__TYPE);
		createEReference(dwGroupTypeEClass, DW_GROUP_TYPE__GOUP);

		dwRootFeatureContainerEClass = createEClass(DW_ROOT_FEATURE_CONTAINER);
		createEReference(dwRootFeatureContainerEClass, DW_ROOT_FEATURE_CONTAINER__FEATURE);
		createEReference(dwRootFeatureContainerEClass, DW_ROOT_FEATURE_CONTAINER__FEATURE_MODEL);

		dwParentFeatureChildGroupContainerEClass = createEClass(DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER);
		createEReference(dwParentFeatureChildGroupContainerEClass, DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__PARENT);
		createEReference(dwParentFeatureChildGroupContainerEClass, DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__CHILD_GROUP);

		dwGroupCompositionEClass = createEClass(DW_GROUP_COMPOSITION);
		createEReference(dwGroupCompositionEClass, DW_GROUP_COMPOSITION__COMPOSITION_OF);
		createEReference(dwGroupCompositionEClass, DW_GROUP_COMPOSITION__CHILD_FEATURES);

		// Create enums
		dwFeatureTypeEnumEEnum = createEEnum(DW_FEATURE_TYPE_ENUM);
		dwGroupTypeEnumEEnum = createEEnum(DW_GROUP_TYPE_ENUM);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		DwTemporalModelPackage theDwTemporalModelPackage = (DwTemporalModelPackage)EPackage.Registry.INSTANCE.getEPackage(DwTemporalModelPackage.eNS_URI);
		DwDatatypesPackage theDwDatatypesPackage = (DwDatatypesPackage)EPackage.Registry.INSTANCE.getEPackage(DwDatatypesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		dwTemporalFeatureModelEClass.getESuperTypes().add(theDwTemporalModelPackage.getDwElementWithId());
		dwNameEClass.getESuperTypes().add(theDwTemporalModelPackage.getDwSingleTemporalIntervalElement());
		dwFeatureEClass.getESuperTypes().add(this.getDwNamedElement());
		dwFeatureEClass.getESuperTypes().add(theDwTemporalModelPackage.getDwMultiTemporalIntervalElement());
		dwFeatureTypeEClass.getESuperTypes().add(theDwTemporalModelPackage.getDwSingleTemporalIntervalElement());
		dwFeatureVersionEClass.getESuperTypes().add(theDwTemporalModelPackage.getDwMultiTemporalIntervalElement());
		dwFeatureAttributeEClass.getESuperTypes().add(this.getDwNamedElement());
		dwFeatureAttributeEClass.getESuperTypes().add(theDwTemporalModelPackage.getDwMultiTemporalIntervalElement());
		dwNumberAttributeEClass.getESuperTypes().add(this.getDwFeatureAttribute());
		dwBooleanAttributeEClass.getESuperTypes().add(this.getDwFeatureAttribute());
		dwStringAttributeEClass.getESuperTypes().add(this.getDwFeatureAttribute());
		dwEnumAttributeEClass.getESuperTypes().add(this.getDwFeatureAttribute());
		dwGroupEClass.getESuperTypes().add(theDwTemporalModelPackage.getDwMultiTemporalIntervalElement());
		dwGroupTypeEClass.getESuperTypes().add(theDwTemporalModelPackage.getDwSingleTemporalIntervalElement());
		dwRootFeatureContainerEClass.getESuperTypes().add(theDwTemporalModelPackage.getDwSingleTemporalIntervalElement());
		dwParentFeatureChildGroupContainerEClass.getESuperTypes().add(theDwTemporalModelPackage.getDwSingleTemporalIntervalElement());
		dwGroupCompositionEClass.getESuperTypes().add(theDwTemporalModelPackage.getDwSingleTemporalIntervalElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(dwTemporalFeatureModelEClass, DwTemporalFeatureModel.class, "DwTemporalFeatureModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwTemporalFeatureModel_Features(), this.getDwFeature(), this.getDwFeature_FeatureModel(), "features", null, 0, -1, DwTemporalFeatureModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwTemporalFeatureModel_Groups(), this.getDwGroup(), this.getDwGroup_FeatureModel(), "groups", null, 0, -1, DwTemporalFeatureModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwTemporalFeatureModel_Enums(), theDwDatatypesPackage.getDwEnum(), null, "enums", null, 0, -1, DwTemporalFeatureModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwTemporalFeatureModel_RootFeatures(), this.getDwRootFeatureContainer(), this.getDwRootFeatureContainer_FeatureModel(), "rootFeatures", null, 0, -1, DwTemporalFeatureModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwNamedElementEClass, DwNamedElement.class, "DwNamedElement", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwNamedElement_Names(), this.getDwName(), null, "names", null, 0, -1, DwNamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwNameEClass, DwName.class, "DwName", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDwName_Name(), ecorePackage.getEString(), "name", null, 1, 1, DwName.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwFeatureEClass, DwFeature.class, "DwFeature", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwFeature_FeatureModel(), this.getDwTemporalFeatureModel(), this.getDwTemporalFeatureModel_Features(), "featureModel", null, 1, 1, DwFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwFeature_Types(), this.getDwFeatureType(), this.getDwFeatureType_Feature(), "types", null, 1, -1, DwFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwFeature_Versions(), this.getDwFeatureVersion(), this.getDwFeatureVersion_Feature(), "versions", null, 0, -1, DwFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwFeature_Attributes(), this.getDwFeatureAttribute(), this.getDwFeatureAttribute_Feature(), "attributes", null, 0, -1, DwFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwFeature_ParentOf(), this.getDwParentFeatureChildGroupContainer(), this.getDwParentFeatureChildGroupContainer_Parent(), "parentOf", null, 0, -1, DwFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwFeature_GroupMembership(), this.getDwGroupComposition(), this.getDwGroupComposition_ChildFeatures(), "groupMembership", null, 0, -1, DwFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getDwFeature__IsOptional__Date(), ecorePackage.getEBoolean(), "isOptional", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "date", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getDwFeature__IsMandatory__Date(), ecorePackage.getEBoolean(), "isMandatory", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "date", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(dwFeatureTypeEClass, DwFeatureType.class, "DwFeatureType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDwFeatureType_Type(), this.getDwFeatureTypeEnum(), "type", null, 1, 1, DwFeatureType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwFeatureType_Feature(), this.getDwFeature(), this.getDwFeature_Types(), "feature", null, 1, 1, DwFeatureType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwFeatureVersionEClass, DwFeatureVersion.class, "DwFeatureVersion", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDwFeatureVersion_Number(), ecorePackage.getEString(), "number", null, 1, 1, DwFeatureVersion.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwFeatureVersion_SupersededVersion(), this.getDwFeatureVersion(), null, "supersededVersion", null, 0, 1, DwFeatureVersion.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwFeatureVersion_SupersedingVersions(), this.getDwFeatureVersion(), null, "supersedingVersions", null, 0, -1, DwFeatureVersion.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwFeatureVersion_Feature(), this.getDwFeature(), this.getDwFeature_Versions(), "feature", null, 1, 1, DwFeatureVersion.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwFeatureAttributeEClass, DwFeatureAttribute.class, "DwFeatureAttribute", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwFeatureAttribute_Feature(), this.getDwFeature(), this.getDwFeature_Attributes(), "feature", null, 1, 1, DwFeatureAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDwFeatureAttribute_Recursive(), ecorePackage.getEBoolean(), "recursive", null, 1, 1, DwFeatureAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDwFeatureAttribute_Configurable(), ecorePackage.getEBoolean(), "configurable", null, 1, 1, DwFeatureAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwNumberAttributeEClass, DwNumberAttribute.class, "DwNumberAttribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDwNumberAttribute_Min(), ecorePackage.getEDouble(), "min", null, 1, 1, DwNumberAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDwNumberAttribute_Max(), ecorePackage.getEDouble(), "max", null, 1, 1, DwNumberAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDwNumberAttribute_DefaultValue(), ecorePackage.getEDouble(), "defaultValue", null, 1, 1, DwNumberAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwBooleanAttributeEClass, DwBooleanAttribute.class, "DwBooleanAttribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDwBooleanAttribute_DefaultValue(), ecorePackage.getEBoolean(), "defaultValue", null, 1, 1, DwBooleanAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwStringAttributeEClass, DwStringAttribute.class, "DwStringAttribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDwStringAttribute_DefaultValue(), ecorePackage.getEString(), "defaultValue", null, 1, 1, DwStringAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwEnumAttributeEClass, DwEnumAttribute.class, "DwEnumAttribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwEnumAttribute_Enum(), theDwDatatypesPackage.getDwEnum(), null, "enum", null, 1, 1, DwEnumAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwEnumAttribute_DefaultValue(), theDwDatatypesPackage.getDwEnumLiteral(), null, "defaultValue", null, 0, 1, DwEnumAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwGroupEClass, DwGroup.class, "DwGroup", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwGroup_FeatureModel(), this.getDwTemporalFeatureModel(), this.getDwTemporalFeatureModel_Groups(), "featureModel", null, 1, 1, DwGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwGroup_Types(), this.getDwGroupType(), this.getDwGroupType_Goup(), "types", null, 1, -1, DwGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwGroup_ChildOf(), this.getDwParentFeatureChildGroupContainer(), this.getDwParentFeatureChildGroupContainer_ChildGroup(), "childOf", null, 1, -1, DwGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwGroup_ParentOf(), this.getDwGroupComposition(), this.getDwGroupComposition_CompositionOf(), "parentOf", null, 1, -1, DwGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getDwGroup__IsAlternative__Date(), ecorePackage.getEBoolean(), "isAlternative", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "date", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getDwGroup__IsOr__Date(), ecorePackage.getEBoolean(), "isOr", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "date", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getDwGroup__IsAnd__Date(), ecorePackage.getEBoolean(), "isAnd", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "date", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(dwGroupTypeEClass, DwGroupType.class, "DwGroupType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDwGroupType_Type(), this.getDwGroupTypeEnum(), "type", null, 1, 1, DwGroupType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwGroupType_Goup(), this.getDwGroup(), this.getDwGroup_Types(), "goup", null, 1, 1, DwGroupType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwRootFeatureContainerEClass, DwRootFeatureContainer.class, "DwRootFeatureContainer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwRootFeatureContainer_Feature(), this.getDwFeature(), null, "feature", null, 1, 1, DwRootFeatureContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwRootFeatureContainer_FeatureModel(), this.getDwTemporalFeatureModel(), this.getDwTemporalFeatureModel_RootFeatures(), "featureModel", null, 1, 1, DwRootFeatureContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwParentFeatureChildGroupContainerEClass, DwParentFeatureChildGroupContainer.class, "DwParentFeatureChildGroupContainer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwParentFeatureChildGroupContainer_Parent(), this.getDwFeature(), this.getDwFeature_ParentOf(), "parent", null, 1, 1, DwParentFeatureChildGroupContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwParentFeatureChildGroupContainer_ChildGroup(), this.getDwGroup(), this.getDwGroup_ChildOf(), "childGroup", null, 1, 1, DwParentFeatureChildGroupContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwGroupCompositionEClass, DwGroupComposition.class, "DwGroupComposition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwGroupComposition_CompositionOf(), this.getDwGroup(), this.getDwGroup_ParentOf(), "compositionOf", null, 1, 1, DwGroupComposition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwGroupComposition_ChildFeatures(), this.getDwFeature(), this.getDwFeature_GroupMembership(), "childFeatures", null, 1, -1, DwGroupComposition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(dwFeatureTypeEnumEEnum, DwFeatureTypeEnum.class, "DwFeatureTypeEnum");
		addEEnumLiteral(dwFeatureTypeEnumEEnum, DwFeatureTypeEnum.OPTIONAL);
		addEEnumLiteral(dwFeatureTypeEnumEEnum, DwFeatureTypeEnum.MANDATORY);

		initEEnum(dwGroupTypeEnumEEnum, DwGroupTypeEnum.class, "DwGroupTypeEnum");
		addEEnumLiteral(dwGroupTypeEnumEEnum, DwGroupTypeEnum.AND);
		addEEnumLiteral(dwGroupTypeEnumEEnum, DwGroupTypeEnum.ALTERNATIVE);
		addEEnumLiteral(dwGroupTypeEnumEEnum, DwGroupTypeEnum.OR);

		// Create resource
		createResource(eNS_URI);
	}

} //DwFeaturePackageImpl
