/**
 */
package de.darwinspl.feature.impl;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeaturePackage;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwParentFeatureChildGroupContainer;

import de.darwinspl.temporal.impl.DwSingleTemporalIntervalElementImpl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Parent Feature Child Group Container</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.impl.DwParentFeatureChildGroupContainerImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwParentFeatureChildGroupContainerImpl#getChildGroup <em>Child Group</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwParentFeatureChildGroupContainerImpl extends DwSingleTemporalIntervalElementImpl implements DwParentFeatureChildGroupContainer {
	/**
	 * The cached value of the '{@link #getChildGroup() <em>Child Group</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChildGroup()
	 * @generated
	 * @ordered
	 */
	protected DwGroup childGroup;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwParentFeatureChildGroupContainerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwFeaturePackage.Literals.DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeature getParent() {
		if (eContainerFeatureID() != DwFeaturePackage.DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__PARENT) return null;
		return (DwFeature)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParent(DwFeature newParent, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newParent, DwFeaturePackage.DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__PARENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setParent(DwFeature newParent) {
		if (newParent != eInternalContainer() || (eContainerFeatureID() != DwFeaturePackage.DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__PARENT && newParent != null)) {
			if (EcoreUtil.isAncestor(this, newParent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParent != null)
				msgs = ((InternalEObject)newParent).eInverseAdd(this, DwFeaturePackage.DW_FEATURE__PARENT_OF, DwFeature.class, msgs);
			msgs = basicSetParent(newParent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwFeaturePackage.DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__PARENT, newParent, newParent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroup getChildGroup() {
		if (childGroup != null && childGroup.eIsProxy()) {
			InternalEObject oldChildGroup = (InternalEObject)childGroup;
			childGroup = (DwGroup)eResolveProxy(oldChildGroup);
			if (childGroup != oldChildGroup) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwFeaturePackage.DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__CHILD_GROUP, oldChildGroup, childGroup));
			}
		}
		return childGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwGroup basicGetChildGroup() {
		return childGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChildGroup(DwGroup newChildGroup, NotificationChain msgs) {
		DwGroup oldChildGroup = childGroup;
		childGroup = newChildGroup;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DwFeaturePackage.DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__CHILD_GROUP, oldChildGroup, newChildGroup);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setChildGroup(DwGroup newChildGroup) {
		if (newChildGroup != childGroup) {
			NotificationChain msgs = null;
			if (childGroup != null)
				msgs = ((InternalEObject)childGroup).eInverseRemove(this, DwFeaturePackage.DW_GROUP__CHILD_OF, DwGroup.class, msgs);
			if (newChildGroup != null)
				msgs = ((InternalEObject)newChildGroup).eInverseAdd(this, DwFeaturePackage.DW_GROUP__CHILD_OF, DwGroup.class, msgs);
			msgs = basicSetChildGroup(newChildGroup, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwFeaturePackage.DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__CHILD_GROUP, newChildGroup, newChildGroup));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwFeaturePackage.DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__PARENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetParent((DwFeature)otherEnd, msgs);
			case DwFeaturePackage.DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__CHILD_GROUP:
				if (childGroup != null)
					msgs = ((InternalEObject)childGroup).eInverseRemove(this, DwFeaturePackage.DW_GROUP__CHILD_OF, DwGroup.class, msgs);
				return basicSetChildGroup((DwGroup)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwFeaturePackage.DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__PARENT:
				return basicSetParent(null, msgs);
			case DwFeaturePackage.DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__CHILD_GROUP:
				return basicSetChildGroup(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DwFeaturePackage.DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__PARENT:
				return eInternalContainer().eInverseRemove(this, DwFeaturePackage.DW_FEATURE__PARENT_OF, DwFeature.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwFeaturePackage.DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__PARENT:
				return getParent();
			case DwFeaturePackage.DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__CHILD_GROUP:
				if (resolve) return getChildGroup();
				return basicGetChildGroup();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwFeaturePackage.DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__PARENT:
				setParent((DwFeature)newValue);
				return;
			case DwFeaturePackage.DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__CHILD_GROUP:
				setChildGroup((DwGroup)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwFeaturePackage.DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__PARENT:
				setParent((DwFeature)null);
				return;
			case DwFeaturePackage.DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__CHILD_GROUP:
				setChildGroup((DwGroup)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwFeaturePackage.DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__PARENT:
				return getParent() != null;
			case DwFeaturePackage.DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__CHILD_GROUP:
				return childGroup != null;
		}
		return super.eIsSet(featureID);
	}

} //DwParentFeatureChildGroupContainerImpl
