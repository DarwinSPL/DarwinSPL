/**
 */
package de.darwinspl.feature.impl;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureAttribute;
import de.darwinspl.feature.DwFeaturePackage;
import de.darwinspl.feature.DwName;

import de.darwinspl.temporal.DwElementWithId;
import de.darwinspl.temporal.DwMultiTemporalIntervalElement;
import de.darwinspl.temporal.DwTemporalElement;
import de.darwinspl.temporal.DwTemporalInterval;
import de.darwinspl.temporal.DwTemporalModelPackage;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attribute</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.impl.DwFeatureAttributeImpl#getNames <em>Names</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwFeatureAttributeImpl#getId <em>Id</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwFeatureAttributeImpl#getValidities <em>Validities</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwFeatureAttributeImpl#getFeature <em>Feature</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwFeatureAttributeImpl#isRecursive <em>Recursive</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwFeatureAttributeImpl#isConfigurable <em>Configurable</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class DwFeatureAttributeImpl extends MinimalEObjectImpl.Container implements DwFeatureAttribute {
	/**
	 * The cached value of the '{@link #getNames() <em>Names</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNames()
	 * @generated
	 * @ordered
	 */
	protected EList<DwName> names;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getValidities() <em>Validities</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValidities()
	 * @generated
	 * @ordered
	 */
	protected EList<DwTemporalInterval> validities;

	/**
	 * The default value of the '{@link #isRecursive() <em>Recursive</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRecursive()
	 * @generated
	 * @ordered
	 */
	protected static final boolean RECURSIVE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isRecursive() <em>Recursive</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRecursive()
	 * @generated
	 * @ordered
	 */
	protected boolean recursive = RECURSIVE_EDEFAULT;

	/**
	 * The default value of the '{@link #isConfigurable() <em>Configurable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isConfigurable()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CONFIGURABLE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isConfigurable() <em>Configurable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isConfigurable()
	 * @generated
	 * @ordered
	 */
	protected boolean configurable = CONFIGURABLE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwFeatureAttributeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwFeaturePackage.Literals.DW_FEATURE_ATTRIBUTE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwName> getNames() {
		if (names == null) {
			names = new EObjectContainmentEList<DwName>(DwName.class, this, DwFeaturePackage.DW_FEATURE_ATTRIBUTE__NAMES);
		}
		return names;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwFeaturePackage.DW_FEATURE_ATTRIBUTE__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwTemporalInterval> getValidities() {
		if (validities == null) {
			validities = new EObjectContainmentEList<DwTemporalInterval>(DwTemporalInterval.class, this, DwFeaturePackage.DW_FEATURE_ATTRIBUTE__VALIDITIES);
		}
		return validities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeature getFeature() {
		if (eContainerFeatureID() != DwFeaturePackage.DW_FEATURE_ATTRIBUTE__FEATURE) return null;
		return (DwFeature)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFeature(DwFeature newFeature, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newFeature, DwFeaturePackage.DW_FEATURE_ATTRIBUTE__FEATURE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFeature(DwFeature newFeature) {
		if (newFeature != eInternalContainer() || (eContainerFeatureID() != DwFeaturePackage.DW_FEATURE_ATTRIBUTE__FEATURE && newFeature != null)) {
			if (EcoreUtil.isAncestor(this, newFeature))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newFeature != null)
				msgs = ((InternalEObject)newFeature).eInverseAdd(this, DwFeaturePackage.DW_FEATURE__ATTRIBUTES, DwFeature.class, msgs);
			msgs = basicSetFeature(newFeature, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwFeaturePackage.DW_FEATURE_ATTRIBUTE__FEATURE, newFeature, newFeature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isRecursive() {
		return recursive;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRecursive(boolean newRecursive) {
		boolean oldRecursive = recursive;
		recursive = newRecursive;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwFeaturePackage.DW_FEATURE_ATTRIBUTE__RECURSIVE, oldRecursive, recursive));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isConfigurable() {
		return configurable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setConfigurable(boolean newConfigurable) {
		boolean oldConfigurable = configurable;
		configurable = newConfigurable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwFeaturePackage.DW_FEATURE_ATTRIBUTE__CONFIGURABLE, oldConfigurable, configurable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String createId() {
		if(this.id == null || this.id.equals("")) {
		   String newIdentifier = "_"+java.util.UUID.randomUUID().toString();
		   setId(newIdentifier);
		}
		return getId();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid(final Date date) {
		return de.darwinspl.temporal.util.DwEvolutionUtil.isValid(this, date);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__FEATURE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetFeature((DwFeature)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__NAMES:
				return ((InternalEList<?>)getNames()).basicRemove(otherEnd, msgs);
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__VALIDITIES:
				return ((InternalEList<?>)getValidities()).basicRemove(otherEnd, msgs);
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__FEATURE:
				return basicSetFeature(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__FEATURE:
				return eInternalContainer().eInverseRemove(this, DwFeaturePackage.DW_FEATURE__ATTRIBUTES, DwFeature.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__NAMES:
				return getNames();
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__ID:
				return getId();
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__VALIDITIES:
				return getValidities();
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__FEATURE:
				return getFeature();
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__RECURSIVE:
				return isRecursive();
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__CONFIGURABLE:
				return isConfigurable();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__NAMES:
				getNames().clear();
				getNames().addAll((Collection<? extends DwName>)newValue);
				return;
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__ID:
				setId((String)newValue);
				return;
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__VALIDITIES:
				getValidities().clear();
				getValidities().addAll((Collection<? extends DwTemporalInterval>)newValue);
				return;
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__FEATURE:
				setFeature((DwFeature)newValue);
				return;
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__RECURSIVE:
				setRecursive((Boolean)newValue);
				return;
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__CONFIGURABLE:
				setConfigurable((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__NAMES:
				getNames().clear();
				return;
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__ID:
				setId(ID_EDEFAULT);
				return;
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__VALIDITIES:
				getValidities().clear();
				return;
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__FEATURE:
				setFeature((DwFeature)null);
				return;
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__RECURSIVE:
				setRecursive(RECURSIVE_EDEFAULT);
				return;
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__CONFIGURABLE:
				setConfigurable(CONFIGURABLE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__NAMES:
				return names != null && !names.isEmpty();
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__VALIDITIES:
				return validities != null && !validities.isEmpty();
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__FEATURE:
				return getFeature() != null;
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__RECURSIVE:
				return recursive != RECURSIVE_EDEFAULT;
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__CONFIGURABLE:
				return configurable != CONFIGURABLE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == DwElementWithId.class) {
			switch (derivedFeatureID) {
				case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__ID: return DwTemporalModelPackage.DW_ELEMENT_WITH_ID__ID;
				default: return -1;
			}
		}
		if (baseClass == DwTemporalElement.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == DwMultiTemporalIntervalElement.class) {
			switch (derivedFeatureID) {
				case DwFeaturePackage.DW_FEATURE_ATTRIBUTE__VALIDITIES: return DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT__VALIDITIES;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == DwElementWithId.class) {
			switch (baseFeatureID) {
				case DwTemporalModelPackage.DW_ELEMENT_WITH_ID__ID: return DwFeaturePackage.DW_FEATURE_ATTRIBUTE__ID;
				default: return -1;
			}
		}
		if (baseClass == DwTemporalElement.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == DwMultiTemporalIntervalElement.class) {
			switch (baseFeatureID) {
				case DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT__VALIDITIES: return DwFeaturePackage.DW_FEATURE_ATTRIBUTE__VALIDITIES;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == DwElementWithId.class) {
			switch (baseOperationID) {
				case DwTemporalModelPackage.DW_ELEMENT_WITH_ID___CREATE_ID: return DwFeaturePackage.DW_FEATURE_ATTRIBUTE___CREATE_ID;
				default: return -1;
			}
		}
		if (baseClass == DwTemporalElement.class) {
			switch (baseOperationID) {
				case DwTemporalModelPackage.DW_TEMPORAL_ELEMENT___IS_VALID__DATE: return DwFeaturePackage.DW_FEATURE_ATTRIBUTE___IS_VALID__DATE;
				default: return -1;
			}
		}
		if (baseClass == DwMultiTemporalIntervalElement.class) {
			switch (baseOperationID) {
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE___IS_VALID__DATE:
				return isValid((Date)arguments.get(0));
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE___CREATE_ID:
				return createId();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", recursive: ");
		result.append(recursive);
		result.append(", configurable: ");
		result.append(configurable);
		result.append(')');
		return result.toString();
	}

} //DwFeatureAttributeImpl
