/**
 */
package de.darwinspl.feature.impl;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeaturePackage;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupComposition;

import de.darwinspl.temporal.impl.DwSingleTemporalIntervalElementImpl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Group Composition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.impl.DwGroupCompositionImpl#getCompositionOf <em>Composition Of</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwGroupCompositionImpl#getChildFeatures <em>Child Features</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwGroupCompositionImpl extends DwSingleTemporalIntervalElementImpl implements DwGroupComposition {
	/**
	 * The cached value of the '{@link #getChildFeatures() <em>Child Features</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChildFeatures()
	 * @generated
	 * @ordered
	 */
	protected EList<DwFeature> childFeatures;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwGroupCompositionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwFeaturePackage.Literals.DW_GROUP_COMPOSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroup getCompositionOf() {
		if (eContainerFeatureID() != DwFeaturePackage.DW_GROUP_COMPOSITION__COMPOSITION_OF) return null;
		return (DwGroup)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCompositionOf(DwGroup newCompositionOf, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newCompositionOf, DwFeaturePackage.DW_GROUP_COMPOSITION__COMPOSITION_OF, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCompositionOf(DwGroup newCompositionOf) {
		if (newCompositionOf != eInternalContainer() || (eContainerFeatureID() != DwFeaturePackage.DW_GROUP_COMPOSITION__COMPOSITION_OF && newCompositionOf != null)) {
			if (EcoreUtil.isAncestor(this, newCompositionOf))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCompositionOf != null)
				msgs = ((InternalEObject)newCompositionOf).eInverseAdd(this, DwFeaturePackage.DW_GROUP__PARENT_OF, DwGroup.class, msgs);
			msgs = basicSetCompositionOf(newCompositionOf, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwFeaturePackage.DW_GROUP_COMPOSITION__COMPOSITION_OF, newCompositionOf, newCompositionOf));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwFeature> getChildFeatures() {
		if (childFeatures == null) {
			childFeatures = new EObjectWithInverseResolvingEList.ManyInverse<DwFeature>(DwFeature.class, this, DwFeaturePackage.DW_GROUP_COMPOSITION__CHILD_FEATURES, DwFeaturePackage.DW_FEATURE__GROUP_MEMBERSHIP);
		}
		return childFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwFeaturePackage.DW_GROUP_COMPOSITION__COMPOSITION_OF:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetCompositionOf((DwGroup)otherEnd, msgs);
			case DwFeaturePackage.DW_GROUP_COMPOSITION__CHILD_FEATURES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getChildFeatures()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwFeaturePackage.DW_GROUP_COMPOSITION__COMPOSITION_OF:
				return basicSetCompositionOf(null, msgs);
			case DwFeaturePackage.DW_GROUP_COMPOSITION__CHILD_FEATURES:
				return ((InternalEList<?>)getChildFeatures()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DwFeaturePackage.DW_GROUP_COMPOSITION__COMPOSITION_OF:
				return eInternalContainer().eInverseRemove(this, DwFeaturePackage.DW_GROUP__PARENT_OF, DwGroup.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwFeaturePackage.DW_GROUP_COMPOSITION__COMPOSITION_OF:
				return getCompositionOf();
			case DwFeaturePackage.DW_GROUP_COMPOSITION__CHILD_FEATURES:
				return getChildFeatures();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwFeaturePackage.DW_GROUP_COMPOSITION__COMPOSITION_OF:
				setCompositionOf((DwGroup)newValue);
				return;
			case DwFeaturePackage.DW_GROUP_COMPOSITION__CHILD_FEATURES:
				getChildFeatures().clear();
				getChildFeatures().addAll((Collection<? extends DwFeature>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwFeaturePackage.DW_GROUP_COMPOSITION__COMPOSITION_OF:
				setCompositionOf((DwGroup)null);
				return;
			case DwFeaturePackage.DW_GROUP_COMPOSITION__CHILD_FEATURES:
				getChildFeatures().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwFeaturePackage.DW_GROUP_COMPOSITION__COMPOSITION_OF:
				return getCompositionOf() != null;
			case DwFeaturePackage.DW_GROUP_COMPOSITION__CHILD_FEATURES:
				return childFeatures != null && !childFeatures.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DwGroupCompositionImpl
