/**
 */
package de.darwinspl.feature.impl;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureAttribute;
import de.darwinspl.feature.DwFeaturePackage;
import de.darwinspl.feature.DwFeatureType;
import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.DwGroupComposition;
import de.darwinspl.feature.DwName;
import de.darwinspl.feature.DwParentFeatureChildGroupContainer;
import de.darwinspl.feature.DwTemporalFeatureModel;

import de.darwinspl.temporal.DwElementWithId;
import de.darwinspl.temporal.DwMultiTemporalIntervalElement;
import de.darwinspl.temporal.DwTemporalElement;
import de.darwinspl.temporal.DwTemporalInterval;
import de.darwinspl.temporal.DwTemporalModelPackage;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Feature</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.impl.DwFeatureImpl#getNames <em>Names</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwFeatureImpl#getId <em>Id</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwFeatureImpl#getValidities <em>Validities</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwFeatureImpl#getFeatureModel <em>Feature Model</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwFeatureImpl#getTypes <em>Types</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwFeatureImpl#getVersions <em>Versions</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwFeatureImpl#getAttributes <em>Attributes</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwFeatureImpl#getParentOf <em>Parent Of</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwFeatureImpl#getGroupMembership <em>Group Membership</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwFeatureImpl extends MinimalEObjectImpl.Container implements DwFeature {
	/**
	 * The cached value of the '{@link #getNames() <em>Names</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNames()
	 * @generated
	 * @ordered
	 */
	protected EList<DwName> names;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getValidities() <em>Validities</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValidities()
	 * @generated
	 * @ordered
	 */
	protected EList<DwTemporalInterval> validities;

	/**
	 * The cached value of the '{@link #getTypes() <em>Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<DwFeatureType> types;

	/**
	 * The cached value of the '{@link #getVersions() <em>Versions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersions()
	 * @generated
	 * @ordered
	 */
	protected EList<DwFeatureVersion> versions;

	/**
	 * The cached value of the '{@link #getAttributes() <em>Attributes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributes()
	 * @generated
	 * @ordered
	 */
	protected EList<DwFeatureAttribute> attributes;

	/**
	 * The cached value of the '{@link #getParentOf() <em>Parent Of</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParentOf()
	 * @generated
	 * @ordered
	 */
	protected EList<DwParentFeatureChildGroupContainer> parentOf;

	/**
	 * The cached value of the '{@link #getGroupMembership() <em>Group Membership</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroupMembership()
	 * @generated
	 * @ordered
	 */
	protected EList<DwGroupComposition> groupMembership;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwFeatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwFeaturePackage.Literals.DW_FEATURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwName> getNames() {
		if (names == null) {
			names = new EObjectContainmentEList<DwName>(DwName.class, this, DwFeaturePackage.DW_FEATURE__NAMES);
		}
		return names;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwFeaturePackage.DW_FEATURE__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwTemporalInterval> getValidities() {
		if (validities == null) {
			validities = new EObjectContainmentEList<DwTemporalInterval>(DwTemporalInterval.class, this, DwFeaturePackage.DW_FEATURE__VALIDITIES);
		}
		return validities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwTemporalFeatureModel getFeatureModel() {
		if (eContainerFeatureID() != DwFeaturePackage.DW_FEATURE__FEATURE_MODEL) return null;
		return (DwTemporalFeatureModel)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFeatureModel(DwTemporalFeatureModel newFeatureModel, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newFeatureModel, DwFeaturePackage.DW_FEATURE__FEATURE_MODEL, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFeatureModel(DwTemporalFeatureModel newFeatureModel) {
		if (newFeatureModel != eInternalContainer() || (eContainerFeatureID() != DwFeaturePackage.DW_FEATURE__FEATURE_MODEL && newFeatureModel != null)) {
			if (EcoreUtil.isAncestor(this, newFeatureModel))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newFeatureModel != null)
				msgs = ((InternalEObject)newFeatureModel).eInverseAdd(this, DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__FEATURES, DwTemporalFeatureModel.class, msgs);
			msgs = basicSetFeatureModel(newFeatureModel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwFeaturePackage.DW_FEATURE__FEATURE_MODEL, newFeatureModel, newFeatureModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwFeatureType> getTypes() {
		if (types == null) {
			types = new EObjectContainmentWithInverseEList<DwFeatureType>(DwFeatureType.class, this, DwFeaturePackage.DW_FEATURE__TYPES, DwFeaturePackage.DW_FEATURE_TYPE__FEATURE);
		}
		return types;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwFeatureVersion> getVersions() {
		if (versions == null) {
			versions = new EObjectContainmentWithInverseEList<DwFeatureVersion>(DwFeatureVersion.class, this, DwFeaturePackage.DW_FEATURE__VERSIONS, DwFeaturePackage.DW_FEATURE_VERSION__FEATURE);
		}
		return versions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwFeatureAttribute> getAttributes() {
		if (attributes == null) {
			attributes = new EObjectContainmentWithInverseEList<DwFeatureAttribute>(DwFeatureAttribute.class, this, DwFeaturePackage.DW_FEATURE__ATTRIBUTES, DwFeaturePackage.DW_FEATURE_ATTRIBUTE__FEATURE);
		}
		return attributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwParentFeatureChildGroupContainer> getParentOf() {
		if (parentOf == null) {
			parentOf = new EObjectContainmentWithInverseEList<DwParentFeatureChildGroupContainer>(DwParentFeatureChildGroupContainer.class, this, DwFeaturePackage.DW_FEATURE__PARENT_OF, DwFeaturePackage.DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__PARENT);
		}
		return parentOf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwGroupComposition> getGroupMembership() {
		if (groupMembership == null) {
			groupMembership = new EObjectWithInverseResolvingEList.ManyInverse<DwGroupComposition>(DwGroupComposition.class, this, DwFeaturePackage.DW_FEATURE__GROUP_MEMBERSHIP, DwFeaturePackage.DW_GROUP_COMPOSITION__CHILD_FEATURES);
		}
		return groupMembership;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isOptional(Date date) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isMandatory(Date date) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String createId() {
		if(this.id == null || this.id.equals("")) {
		   String newIdentifier = "_"+java.util.UUID.randomUUID().toString();
		   setId(newIdentifier);
		}
		return getId();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid(final Date date) {
		return de.darwinspl.temporal.util.DwEvolutionUtil.isValid(this, date);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwFeaturePackage.DW_FEATURE__FEATURE_MODEL:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetFeatureModel((DwTemporalFeatureModel)otherEnd, msgs);
			case DwFeaturePackage.DW_FEATURE__TYPES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getTypes()).basicAdd(otherEnd, msgs);
			case DwFeaturePackage.DW_FEATURE__VERSIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getVersions()).basicAdd(otherEnd, msgs);
			case DwFeaturePackage.DW_FEATURE__ATTRIBUTES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAttributes()).basicAdd(otherEnd, msgs);
			case DwFeaturePackage.DW_FEATURE__PARENT_OF:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getParentOf()).basicAdd(otherEnd, msgs);
			case DwFeaturePackage.DW_FEATURE__GROUP_MEMBERSHIP:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getGroupMembership()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwFeaturePackage.DW_FEATURE__NAMES:
				return ((InternalEList<?>)getNames()).basicRemove(otherEnd, msgs);
			case DwFeaturePackage.DW_FEATURE__VALIDITIES:
				return ((InternalEList<?>)getValidities()).basicRemove(otherEnd, msgs);
			case DwFeaturePackage.DW_FEATURE__FEATURE_MODEL:
				return basicSetFeatureModel(null, msgs);
			case DwFeaturePackage.DW_FEATURE__TYPES:
				return ((InternalEList<?>)getTypes()).basicRemove(otherEnd, msgs);
			case DwFeaturePackage.DW_FEATURE__VERSIONS:
				return ((InternalEList<?>)getVersions()).basicRemove(otherEnd, msgs);
			case DwFeaturePackage.DW_FEATURE__ATTRIBUTES:
				return ((InternalEList<?>)getAttributes()).basicRemove(otherEnd, msgs);
			case DwFeaturePackage.DW_FEATURE__PARENT_OF:
				return ((InternalEList<?>)getParentOf()).basicRemove(otherEnd, msgs);
			case DwFeaturePackage.DW_FEATURE__GROUP_MEMBERSHIP:
				return ((InternalEList<?>)getGroupMembership()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DwFeaturePackage.DW_FEATURE__FEATURE_MODEL:
				return eInternalContainer().eInverseRemove(this, DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__FEATURES, DwTemporalFeatureModel.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwFeaturePackage.DW_FEATURE__NAMES:
				return getNames();
			case DwFeaturePackage.DW_FEATURE__ID:
				return getId();
			case DwFeaturePackage.DW_FEATURE__VALIDITIES:
				return getValidities();
			case DwFeaturePackage.DW_FEATURE__FEATURE_MODEL:
				return getFeatureModel();
			case DwFeaturePackage.DW_FEATURE__TYPES:
				return getTypes();
			case DwFeaturePackage.DW_FEATURE__VERSIONS:
				return getVersions();
			case DwFeaturePackage.DW_FEATURE__ATTRIBUTES:
				return getAttributes();
			case DwFeaturePackage.DW_FEATURE__PARENT_OF:
				return getParentOf();
			case DwFeaturePackage.DW_FEATURE__GROUP_MEMBERSHIP:
				return getGroupMembership();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwFeaturePackage.DW_FEATURE__NAMES:
				getNames().clear();
				getNames().addAll((Collection<? extends DwName>)newValue);
				return;
			case DwFeaturePackage.DW_FEATURE__ID:
				setId((String)newValue);
				return;
			case DwFeaturePackage.DW_FEATURE__VALIDITIES:
				getValidities().clear();
				getValidities().addAll((Collection<? extends DwTemporalInterval>)newValue);
				return;
			case DwFeaturePackage.DW_FEATURE__FEATURE_MODEL:
				setFeatureModel((DwTemporalFeatureModel)newValue);
				return;
			case DwFeaturePackage.DW_FEATURE__TYPES:
				getTypes().clear();
				getTypes().addAll((Collection<? extends DwFeatureType>)newValue);
				return;
			case DwFeaturePackage.DW_FEATURE__VERSIONS:
				getVersions().clear();
				getVersions().addAll((Collection<? extends DwFeatureVersion>)newValue);
				return;
			case DwFeaturePackage.DW_FEATURE__ATTRIBUTES:
				getAttributes().clear();
				getAttributes().addAll((Collection<? extends DwFeatureAttribute>)newValue);
				return;
			case DwFeaturePackage.DW_FEATURE__PARENT_OF:
				getParentOf().clear();
				getParentOf().addAll((Collection<? extends DwParentFeatureChildGroupContainer>)newValue);
				return;
			case DwFeaturePackage.DW_FEATURE__GROUP_MEMBERSHIP:
				getGroupMembership().clear();
				getGroupMembership().addAll((Collection<? extends DwGroupComposition>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwFeaturePackage.DW_FEATURE__NAMES:
				getNames().clear();
				return;
			case DwFeaturePackage.DW_FEATURE__ID:
				setId(ID_EDEFAULT);
				return;
			case DwFeaturePackage.DW_FEATURE__VALIDITIES:
				getValidities().clear();
				return;
			case DwFeaturePackage.DW_FEATURE__FEATURE_MODEL:
				setFeatureModel((DwTemporalFeatureModel)null);
				return;
			case DwFeaturePackage.DW_FEATURE__TYPES:
				getTypes().clear();
				return;
			case DwFeaturePackage.DW_FEATURE__VERSIONS:
				getVersions().clear();
				return;
			case DwFeaturePackage.DW_FEATURE__ATTRIBUTES:
				getAttributes().clear();
				return;
			case DwFeaturePackage.DW_FEATURE__PARENT_OF:
				getParentOf().clear();
				return;
			case DwFeaturePackage.DW_FEATURE__GROUP_MEMBERSHIP:
				getGroupMembership().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwFeaturePackage.DW_FEATURE__NAMES:
				return names != null && !names.isEmpty();
			case DwFeaturePackage.DW_FEATURE__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case DwFeaturePackage.DW_FEATURE__VALIDITIES:
				return validities != null && !validities.isEmpty();
			case DwFeaturePackage.DW_FEATURE__FEATURE_MODEL:
				return getFeatureModel() != null;
			case DwFeaturePackage.DW_FEATURE__TYPES:
				return types != null && !types.isEmpty();
			case DwFeaturePackage.DW_FEATURE__VERSIONS:
				return versions != null && !versions.isEmpty();
			case DwFeaturePackage.DW_FEATURE__ATTRIBUTES:
				return attributes != null && !attributes.isEmpty();
			case DwFeaturePackage.DW_FEATURE__PARENT_OF:
				return parentOf != null && !parentOf.isEmpty();
			case DwFeaturePackage.DW_FEATURE__GROUP_MEMBERSHIP:
				return groupMembership != null && !groupMembership.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == DwElementWithId.class) {
			switch (derivedFeatureID) {
				case DwFeaturePackage.DW_FEATURE__ID: return DwTemporalModelPackage.DW_ELEMENT_WITH_ID__ID;
				default: return -1;
			}
		}
		if (baseClass == DwTemporalElement.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == DwMultiTemporalIntervalElement.class) {
			switch (derivedFeatureID) {
				case DwFeaturePackage.DW_FEATURE__VALIDITIES: return DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT__VALIDITIES;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == DwElementWithId.class) {
			switch (baseFeatureID) {
				case DwTemporalModelPackage.DW_ELEMENT_WITH_ID__ID: return DwFeaturePackage.DW_FEATURE__ID;
				default: return -1;
			}
		}
		if (baseClass == DwTemporalElement.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == DwMultiTemporalIntervalElement.class) {
			switch (baseFeatureID) {
				case DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT__VALIDITIES: return DwFeaturePackage.DW_FEATURE__VALIDITIES;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == DwElementWithId.class) {
			switch (baseOperationID) {
				case DwTemporalModelPackage.DW_ELEMENT_WITH_ID___CREATE_ID: return DwFeaturePackage.DW_FEATURE___CREATE_ID;
				default: return -1;
			}
		}
		if (baseClass == DwTemporalElement.class) {
			switch (baseOperationID) {
				case DwTemporalModelPackage.DW_TEMPORAL_ELEMENT___IS_VALID__DATE: return DwFeaturePackage.DW_FEATURE___IS_VALID__DATE;
				default: return -1;
			}
		}
		if (baseClass == DwMultiTemporalIntervalElement.class) {
			switch (baseOperationID) {
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case DwFeaturePackage.DW_FEATURE___IS_OPTIONAL__DATE:
				return isOptional((Date)arguments.get(0));
			case DwFeaturePackage.DW_FEATURE___IS_MANDATORY__DATE:
				return isMandatory((Date)arguments.get(0));
			case DwFeaturePackage.DW_FEATURE___IS_VALID__DATE:
				return isValid((Date)arguments.get(0));
			case DwFeaturePackage.DW_FEATURE___CREATE_ID:
				return createId();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(')');
		return result.toString();
	}

} //DwFeatureImpl
