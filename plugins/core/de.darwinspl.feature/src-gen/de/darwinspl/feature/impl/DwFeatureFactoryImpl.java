/**
 */
package de.darwinspl.feature.impl;

import de.darwinspl.feature.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DwFeatureFactoryImpl extends EFactoryImpl implements DwFeatureFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DwFeatureFactory init() {
		try {
			DwFeatureFactory theDwFeatureFactory = (DwFeatureFactory)EPackage.Registry.INSTANCE.getEFactory(DwFeaturePackage.eNS_URI);
			if (theDwFeatureFactory != null) {
				return theDwFeatureFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DwFeatureFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwFeatureFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL: return createDwTemporalFeatureModel();
			case DwFeaturePackage.DW_NAME: return createDwName();
			case DwFeaturePackage.DW_FEATURE: return createDwFeature();
			case DwFeaturePackage.DW_FEATURE_TYPE: return createDwFeatureType();
			case DwFeaturePackage.DW_FEATURE_VERSION: return createDwFeatureVersion();
			case DwFeaturePackage.DW_NUMBER_ATTRIBUTE: return createDwNumberAttribute();
			case DwFeaturePackage.DW_BOOLEAN_ATTRIBUTE: return createDwBooleanAttribute();
			case DwFeaturePackage.DW_STRING_ATTRIBUTE: return createDwStringAttribute();
			case DwFeaturePackage.DW_ENUM_ATTRIBUTE: return createDwEnumAttribute();
			case DwFeaturePackage.DW_GROUP: return createDwGroup();
			case DwFeaturePackage.DW_GROUP_TYPE: return createDwGroupType();
			case DwFeaturePackage.DW_ROOT_FEATURE_CONTAINER: return createDwRootFeatureContainer();
			case DwFeaturePackage.DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER: return createDwParentFeatureChildGroupContainer();
			case DwFeaturePackage.DW_GROUP_COMPOSITION: return createDwGroupComposition();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case DwFeaturePackage.DW_FEATURE_TYPE_ENUM:
				return createDwFeatureTypeEnumFromString(eDataType, initialValue);
			case DwFeaturePackage.DW_GROUP_TYPE_ENUM:
				return createDwGroupTypeEnumFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case DwFeaturePackage.DW_FEATURE_TYPE_ENUM:
				return convertDwFeatureTypeEnumToString(eDataType, instanceValue);
			case DwFeaturePackage.DW_GROUP_TYPE_ENUM:
				return convertDwGroupTypeEnumToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwTemporalFeatureModel createDwTemporalFeatureModel() {
		DwTemporalFeatureModelImpl dwTemporalFeatureModel = new DwTemporalFeatureModelImpl();
		return dwTemporalFeatureModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwName createDwName() {
		DwNameImpl dwName = new DwNameImpl();
		return dwName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeature createDwFeature() {
		DwFeatureImpl dwFeature = new DwFeatureImpl();
		return dwFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureType createDwFeatureType() {
		DwFeatureTypeImpl dwFeatureType = new DwFeatureTypeImpl();
		return dwFeatureType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureVersion createDwFeatureVersion() {
		DwFeatureVersionImpl dwFeatureVersion = new DwFeatureVersionImpl();
		return dwFeatureVersion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwNumberAttribute createDwNumberAttribute() {
		DwNumberAttributeImpl dwNumberAttribute = new DwNumberAttributeImpl();
		return dwNumberAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwBooleanAttribute createDwBooleanAttribute() {
		DwBooleanAttributeImpl dwBooleanAttribute = new DwBooleanAttributeImpl();
		return dwBooleanAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwStringAttribute createDwStringAttribute() {
		DwStringAttributeImpl dwStringAttribute = new DwStringAttributeImpl();
		return dwStringAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwEnumAttribute createDwEnumAttribute() {
		DwEnumAttributeImpl dwEnumAttribute = new DwEnumAttributeImpl();
		return dwEnumAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroup createDwGroup() {
		DwGroupImpl dwGroup = new DwGroupImpl();
		return dwGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroupType createDwGroupType() {
		DwGroupTypeImpl dwGroupType = new DwGroupTypeImpl();
		return dwGroupType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwRootFeatureContainer createDwRootFeatureContainer() {
		DwRootFeatureContainerImpl dwRootFeatureContainer = new DwRootFeatureContainerImpl();
		return dwRootFeatureContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwParentFeatureChildGroupContainer createDwParentFeatureChildGroupContainer() {
		DwParentFeatureChildGroupContainerImpl dwParentFeatureChildGroupContainer = new DwParentFeatureChildGroupContainerImpl();
		return dwParentFeatureChildGroupContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwGroupComposition createDwGroupComposition() {
		DwGroupCompositionImpl dwGroupComposition = new DwGroupCompositionImpl();
		return dwGroupComposition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwFeatureTypeEnum createDwFeatureTypeEnumFromString(EDataType eDataType, String initialValue) {
		DwFeatureTypeEnum result = DwFeatureTypeEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDwFeatureTypeEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwGroupTypeEnum createDwGroupTypeEnumFromString(EDataType eDataType, String initialValue) {
		DwGroupTypeEnum result = DwGroupTypeEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDwGroupTypeEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeaturePackage getDwFeaturePackage() {
		return (DwFeaturePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DwFeaturePackage getPackage() {
		return DwFeaturePackage.eINSTANCE;
	}

} //DwFeatureFactoryImpl
