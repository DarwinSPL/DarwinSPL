/**
 */
package de.darwinspl.feature.impl;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeaturePackage;
import de.darwinspl.feature.DwFeatureVersion;

import de.darwinspl.temporal.impl.DwMultiTemporalIntervalElementImpl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Version</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.impl.DwFeatureVersionImpl#getNumber <em>Number</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwFeatureVersionImpl#getSupersededVersion <em>Superseded Version</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwFeatureVersionImpl#getSupersedingVersions <em>Superseding Versions</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwFeatureVersionImpl#getFeature <em>Feature</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwFeatureVersionImpl extends DwMultiTemporalIntervalElementImpl implements DwFeatureVersion {
	/**
	 * The default value of the '{@link #getNumber() <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber()
	 * @generated
	 * @ordered
	 */
	protected static final String NUMBER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNumber() <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber()
	 * @generated
	 * @ordered
	 */
	protected String number = NUMBER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSupersededVersion() <em>Superseded Version</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSupersededVersion()
	 * @generated
	 * @ordered
	 */
	protected DwFeatureVersion supersededVersion;

	/**
	 * The cached value of the '{@link #getSupersedingVersions() <em>Superseding Versions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSupersedingVersions()
	 * @generated
	 * @ordered
	 */
	protected EList<DwFeatureVersion> supersedingVersions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwFeatureVersionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwFeaturePackage.Literals.DW_FEATURE_VERSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getNumber() {
		return number;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNumber(String newNumber) {
		String oldNumber = number;
		number = newNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwFeaturePackage.DW_FEATURE_VERSION__NUMBER, oldNumber, number));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureVersion getSupersededVersion() {
		if (supersededVersion != null && supersededVersion.eIsProxy()) {
			InternalEObject oldSupersededVersion = (InternalEObject)supersededVersion;
			supersededVersion = (DwFeatureVersion)eResolveProxy(oldSupersededVersion);
			if (supersededVersion != oldSupersededVersion) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwFeaturePackage.DW_FEATURE_VERSION__SUPERSEDED_VERSION, oldSupersededVersion, supersededVersion));
			}
		}
		return supersededVersion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwFeatureVersion basicGetSupersededVersion() {
		return supersededVersion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSupersededVersion(DwFeatureVersion newSupersededVersion) {
		DwFeatureVersion oldSupersededVersion = supersededVersion;
		supersededVersion = newSupersededVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwFeaturePackage.DW_FEATURE_VERSION__SUPERSEDED_VERSION, oldSupersededVersion, supersededVersion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwFeatureVersion> getSupersedingVersions() {
		if (supersedingVersions == null) {
			supersedingVersions = new EObjectResolvingEList<DwFeatureVersion>(DwFeatureVersion.class, this, DwFeaturePackage.DW_FEATURE_VERSION__SUPERSEDING_VERSIONS);
		}
		return supersedingVersions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeature getFeature() {
		if (eContainerFeatureID() != DwFeaturePackage.DW_FEATURE_VERSION__FEATURE) return null;
		return (DwFeature)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFeature(DwFeature newFeature, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newFeature, DwFeaturePackage.DW_FEATURE_VERSION__FEATURE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFeature(DwFeature newFeature) {
		if (newFeature != eInternalContainer() || (eContainerFeatureID() != DwFeaturePackage.DW_FEATURE_VERSION__FEATURE && newFeature != null)) {
			if (EcoreUtil.isAncestor(this, newFeature))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newFeature != null)
				msgs = ((InternalEObject)newFeature).eInverseAdd(this, DwFeaturePackage.DW_FEATURE__VERSIONS, DwFeature.class, msgs);
			msgs = basicSetFeature(newFeature, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwFeaturePackage.DW_FEATURE_VERSION__FEATURE, newFeature, newFeature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwFeaturePackage.DW_FEATURE_VERSION__FEATURE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetFeature((DwFeature)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwFeaturePackage.DW_FEATURE_VERSION__FEATURE:
				return basicSetFeature(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DwFeaturePackage.DW_FEATURE_VERSION__FEATURE:
				return eInternalContainer().eInverseRemove(this, DwFeaturePackage.DW_FEATURE__VERSIONS, DwFeature.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwFeaturePackage.DW_FEATURE_VERSION__NUMBER:
				return getNumber();
			case DwFeaturePackage.DW_FEATURE_VERSION__SUPERSEDED_VERSION:
				if (resolve) return getSupersededVersion();
				return basicGetSupersededVersion();
			case DwFeaturePackage.DW_FEATURE_VERSION__SUPERSEDING_VERSIONS:
				return getSupersedingVersions();
			case DwFeaturePackage.DW_FEATURE_VERSION__FEATURE:
				return getFeature();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwFeaturePackage.DW_FEATURE_VERSION__NUMBER:
				setNumber((String)newValue);
				return;
			case DwFeaturePackage.DW_FEATURE_VERSION__SUPERSEDED_VERSION:
				setSupersededVersion((DwFeatureVersion)newValue);
				return;
			case DwFeaturePackage.DW_FEATURE_VERSION__SUPERSEDING_VERSIONS:
				getSupersedingVersions().clear();
				getSupersedingVersions().addAll((Collection<? extends DwFeatureVersion>)newValue);
				return;
			case DwFeaturePackage.DW_FEATURE_VERSION__FEATURE:
				setFeature((DwFeature)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwFeaturePackage.DW_FEATURE_VERSION__NUMBER:
				setNumber(NUMBER_EDEFAULT);
				return;
			case DwFeaturePackage.DW_FEATURE_VERSION__SUPERSEDED_VERSION:
				setSupersededVersion((DwFeatureVersion)null);
				return;
			case DwFeaturePackage.DW_FEATURE_VERSION__SUPERSEDING_VERSIONS:
				getSupersedingVersions().clear();
				return;
			case DwFeaturePackage.DW_FEATURE_VERSION__FEATURE:
				setFeature((DwFeature)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwFeaturePackage.DW_FEATURE_VERSION__NUMBER:
				return NUMBER_EDEFAULT == null ? number != null : !NUMBER_EDEFAULT.equals(number);
			case DwFeaturePackage.DW_FEATURE_VERSION__SUPERSEDED_VERSION:
				return supersededVersion != null;
			case DwFeaturePackage.DW_FEATURE_VERSION__SUPERSEDING_VERSIONS:
				return supersedingVersions != null && !supersedingVersions.isEmpty();
			case DwFeaturePackage.DW_FEATURE_VERSION__FEATURE:
				return getFeature() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (number: ");
		result.append(number);
		result.append(')');
		return result.toString();
	}

} //DwFeatureVersionImpl
