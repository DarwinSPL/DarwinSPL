/**
 */
package de.darwinspl.feature.impl;

import de.darwinspl.feature.DwFeaturePackage;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupComposition;
import de.darwinspl.feature.DwGroupType;
import de.darwinspl.feature.DwParentFeatureChildGroupContainer;
import de.darwinspl.feature.DwTemporalFeatureModel;

import de.darwinspl.temporal.impl.DwMultiTemporalIntervalElementImpl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Group</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.impl.DwGroupImpl#getFeatureModel <em>Feature Model</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwGroupImpl#getTypes <em>Types</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwGroupImpl#getChildOf <em>Child Of</em>}</li>
 *   <li>{@link de.darwinspl.feature.impl.DwGroupImpl#getParentOf <em>Parent Of</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwGroupImpl extends DwMultiTemporalIntervalElementImpl implements DwGroup {
	/**
	 * The cached value of the '{@link #getTypes() <em>Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<DwGroupType> types;

	/**
	 * The cached value of the '{@link #getChildOf() <em>Child Of</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChildOf()
	 * @generated
	 * @ordered
	 */
	protected EList<DwParentFeatureChildGroupContainer> childOf;

	/**
	 * The cached value of the '{@link #getParentOf() <em>Parent Of</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParentOf()
	 * @generated
	 * @ordered
	 */
	protected EList<DwGroupComposition> parentOf;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwGroupImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwFeaturePackage.Literals.DW_GROUP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwTemporalFeatureModel getFeatureModel() {
		if (eContainerFeatureID() != DwFeaturePackage.DW_GROUP__FEATURE_MODEL) return null;
		return (DwTemporalFeatureModel)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFeatureModel(DwTemporalFeatureModel newFeatureModel, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newFeatureModel, DwFeaturePackage.DW_GROUP__FEATURE_MODEL, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFeatureModel(DwTemporalFeatureModel newFeatureModel) {
		if (newFeatureModel != eInternalContainer() || (eContainerFeatureID() != DwFeaturePackage.DW_GROUP__FEATURE_MODEL && newFeatureModel != null)) {
			if (EcoreUtil.isAncestor(this, newFeatureModel))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newFeatureModel != null)
				msgs = ((InternalEObject)newFeatureModel).eInverseAdd(this, DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__GROUPS, DwTemporalFeatureModel.class, msgs);
			msgs = basicSetFeatureModel(newFeatureModel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwFeaturePackage.DW_GROUP__FEATURE_MODEL, newFeatureModel, newFeatureModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwGroupType> getTypes() {
		if (types == null) {
			types = new EObjectContainmentWithInverseEList<DwGroupType>(DwGroupType.class, this, DwFeaturePackage.DW_GROUP__TYPES, DwFeaturePackage.DW_GROUP_TYPE__GOUP);
		}
		return types;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwParentFeatureChildGroupContainer> getChildOf() {
		if (childOf == null) {
			childOf = new EObjectWithInverseResolvingEList<DwParentFeatureChildGroupContainer>(DwParentFeatureChildGroupContainer.class, this, DwFeaturePackage.DW_GROUP__CHILD_OF, DwFeaturePackage.DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER__CHILD_GROUP);
		}
		return childOf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwGroupComposition> getParentOf() {
		if (parentOf == null) {
			parentOf = new EObjectContainmentWithInverseEList<DwGroupComposition>(DwGroupComposition.class, this, DwFeaturePackage.DW_GROUP__PARENT_OF, DwFeaturePackage.DW_GROUP_COMPOSITION__COMPOSITION_OF);
		}
		return parentOf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isAlternative(Date date) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isOr(Date date) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isAnd(Date date) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwFeaturePackage.DW_GROUP__FEATURE_MODEL:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetFeatureModel((DwTemporalFeatureModel)otherEnd, msgs);
			case DwFeaturePackage.DW_GROUP__TYPES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getTypes()).basicAdd(otherEnd, msgs);
			case DwFeaturePackage.DW_GROUP__CHILD_OF:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getChildOf()).basicAdd(otherEnd, msgs);
			case DwFeaturePackage.DW_GROUP__PARENT_OF:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getParentOf()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwFeaturePackage.DW_GROUP__FEATURE_MODEL:
				return basicSetFeatureModel(null, msgs);
			case DwFeaturePackage.DW_GROUP__TYPES:
				return ((InternalEList<?>)getTypes()).basicRemove(otherEnd, msgs);
			case DwFeaturePackage.DW_GROUP__CHILD_OF:
				return ((InternalEList<?>)getChildOf()).basicRemove(otherEnd, msgs);
			case DwFeaturePackage.DW_GROUP__PARENT_OF:
				return ((InternalEList<?>)getParentOf()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DwFeaturePackage.DW_GROUP__FEATURE_MODEL:
				return eInternalContainer().eInverseRemove(this, DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL__GROUPS, DwTemporalFeatureModel.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwFeaturePackage.DW_GROUP__FEATURE_MODEL:
				return getFeatureModel();
			case DwFeaturePackage.DW_GROUP__TYPES:
				return getTypes();
			case DwFeaturePackage.DW_GROUP__CHILD_OF:
				return getChildOf();
			case DwFeaturePackage.DW_GROUP__PARENT_OF:
				return getParentOf();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwFeaturePackage.DW_GROUP__FEATURE_MODEL:
				setFeatureModel((DwTemporalFeatureModel)newValue);
				return;
			case DwFeaturePackage.DW_GROUP__TYPES:
				getTypes().clear();
				getTypes().addAll((Collection<? extends DwGroupType>)newValue);
				return;
			case DwFeaturePackage.DW_GROUP__CHILD_OF:
				getChildOf().clear();
				getChildOf().addAll((Collection<? extends DwParentFeatureChildGroupContainer>)newValue);
				return;
			case DwFeaturePackage.DW_GROUP__PARENT_OF:
				getParentOf().clear();
				getParentOf().addAll((Collection<? extends DwGroupComposition>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwFeaturePackage.DW_GROUP__FEATURE_MODEL:
				setFeatureModel((DwTemporalFeatureModel)null);
				return;
			case DwFeaturePackage.DW_GROUP__TYPES:
				getTypes().clear();
				return;
			case DwFeaturePackage.DW_GROUP__CHILD_OF:
				getChildOf().clear();
				return;
			case DwFeaturePackage.DW_GROUP__PARENT_OF:
				getParentOf().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwFeaturePackage.DW_GROUP__FEATURE_MODEL:
				return getFeatureModel() != null;
			case DwFeaturePackage.DW_GROUP__TYPES:
				return types != null && !types.isEmpty();
			case DwFeaturePackage.DW_GROUP__CHILD_OF:
				return childOf != null && !childOf.isEmpty();
			case DwFeaturePackage.DW_GROUP__PARENT_OF:
				return parentOf != null && !parentOf.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case DwFeaturePackage.DW_GROUP___IS_ALTERNATIVE__DATE:
				return isAlternative((Date)arguments.get(0));
			case DwFeaturePackage.DW_GROUP___IS_OR__DATE:
				return isOr((Date)arguments.get(0));
			case DwFeaturePackage.DW_GROUP___IS_AND__DATE:
				return isAnd((Date)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //DwGroupImpl
