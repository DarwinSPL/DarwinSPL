/**
 */
package de.darwinspl.feature;

import de.darwinspl.temporal.DwMultiTemporalIntervalElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.DwFeatureAttribute#getFeature <em>Feature</em>}</li>
 *   <li>{@link de.darwinspl.feature.DwFeatureAttribute#isRecursive <em>Recursive</em>}</li>
 *   <li>{@link de.darwinspl.feature.DwFeatureAttribute#isConfigurable <em>Configurable</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.DwFeaturePackage#getDwFeatureAttribute()
 * @model abstract="true"
 * @generated
 */
public interface DwFeatureAttribute extends DwNamedElement, DwMultiTemporalIntervalElement {
	/**
	 * Returns the value of the '<em><b>Feature</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.darwinspl.feature.DwFeature#getAttributes <em>Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature</em>' container reference.
	 * @see #setFeature(DwFeature)
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwFeatureAttribute_Feature()
	 * @see de.darwinspl.feature.DwFeature#getAttributes
	 * @model opposite="attributes" required="true" transient="false"
	 * @generated
	 */
	DwFeature getFeature();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.DwFeatureAttribute#getFeature <em>Feature</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature</em>' container reference.
	 * @see #getFeature()
	 * @generated
	 */
	void setFeature(DwFeature value);

	/**
	 * Returns the value of the '<em><b>Recursive</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Recursive</em>' attribute.
	 * @see #setRecursive(boolean)
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwFeatureAttribute_Recursive()
	 * @model required="true"
	 * @generated
	 */
	boolean isRecursive();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.DwFeatureAttribute#isRecursive <em>Recursive</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Recursive</em>' attribute.
	 * @see #isRecursive()
	 * @generated
	 */
	void setRecursive(boolean value);

	/**
	 * Returns the value of the '<em><b>Configurable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Configurable</em>' attribute.
	 * @see #setConfigurable(boolean)
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwFeatureAttribute_Configurable()
	 * @model required="true"
	 * @generated
	 */
	boolean isConfigurable();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.DwFeatureAttribute#isConfigurable <em>Configurable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Configurable</em>' attribute.
	 * @see #isConfigurable()
	 * @generated
	 */
	void setConfigurable(boolean value);

} // DwFeatureAttribute
