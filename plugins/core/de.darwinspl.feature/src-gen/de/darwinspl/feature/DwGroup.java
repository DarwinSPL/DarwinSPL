/**
 */
package de.darwinspl.feature;

import de.darwinspl.temporal.DwMultiTemporalIntervalElement;

import java.util.Date;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.DwGroup#getFeatureModel <em>Feature Model</em>}</li>
 *   <li>{@link de.darwinspl.feature.DwGroup#getTypes <em>Types</em>}</li>
 *   <li>{@link de.darwinspl.feature.DwGroup#getChildOf <em>Child Of</em>}</li>
 *   <li>{@link de.darwinspl.feature.DwGroup#getParentOf <em>Parent Of</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.DwFeaturePackage#getDwGroup()
 * @model
 * @generated
 */
public interface DwGroup extends DwMultiTemporalIntervalElement {
	/**
	 * Returns the value of the '<em><b>Feature Model</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.darwinspl.feature.DwTemporalFeatureModel#getGroups <em>Groups</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Model</em>' container reference.
	 * @see #setFeatureModel(DwTemporalFeatureModel)
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwGroup_FeatureModel()
	 * @see de.darwinspl.feature.DwTemporalFeatureModel#getGroups
	 * @model opposite="groups" required="true" transient="false"
	 * @generated
	 */
	DwTemporalFeatureModel getFeatureModel();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.DwGroup#getFeatureModel <em>Feature Model</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature Model</em>' container reference.
	 * @see #getFeatureModel()
	 * @generated
	 */
	void setFeatureModel(DwTemporalFeatureModel value);

	/**
	 * Returns the value of the '<em><b>Types</b></em>' containment reference list.
	 * The list contents are of type {@link de.darwinspl.feature.DwGroupType}.
	 * It is bidirectional and its opposite is '{@link de.darwinspl.feature.DwGroupType#getGoup <em>Goup</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Types</em>' containment reference list.
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwGroup_Types()
	 * @see de.darwinspl.feature.DwGroupType#getGoup
	 * @model opposite="goup" containment="true" required="true"
	 * @generated
	 */
	EList<DwGroupType> getTypes();

	/**
	 * Returns the value of the '<em><b>Child Of</b></em>' reference list.
	 * The list contents are of type {@link de.darwinspl.feature.DwParentFeatureChildGroupContainer}.
	 * It is bidirectional and its opposite is '{@link de.darwinspl.feature.DwParentFeatureChildGroupContainer#getChildGroup <em>Child Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Child Of</em>' reference list.
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwGroup_ChildOf()
	 * @see de.darwinspl.feature.DwParentFeatureChildGroupContainer#getChildGroup
	 * @model opposite="childGroup" required="true"
	 * @generated
	 */
	EList<DwParentFeatureChildGroupContainer> getChildOf();

	/**
	 * Returns the value of the '<em><b>Parent Of</b></em>' containment reference list.
	 * The list contents are of type {@link de.darwinspl.feature.DwGroupComposition}.
	 * It is bidirectional and its opposite is '{@link de.darwinspl.feature.DwGroupComposition#getCompositionOf <em>Composition Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Of</em>' containment reference list.
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwGroup_ParentOf()
	 * @see de.darwinspl.feature.DwGroupComposition#getCompositionOf
	 * @model opposite="compositionOf" containment="true" required="true"
	 * @generated
	 */
	EList<DwGroupComposition> getParentOf();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" dateRequired="true"
	 * @generated
	 */
	boolean isAlternative(Date date);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" dateRequired="true"
	 * @generated
	 */
	boolean isOr(Date date);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" dateRequired="true"
	 * @generated
	 */
	boolean isAnd(Date date);

} // DwGroup
