/**
 */
package de.darwinspl.feature;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Number Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.DwNumberAttribute#getMin <em>Min</em>}</li>
 *   <li>{@link de.darwinspl.feature.DwNumberAttribute#getMax <em>Max</em>}</li>
 *   <li>{@link de.darwinspl.feature.DwNumberAttribute#getDefaultValue <em>Default Value</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.DwFeaturePackage#getDwNumberAttribute()
 * @model
 * @generated
 */
public interface DwNumberAttribute extends DwFeatureAttribute {
	/**
	 * Returns the value of the '<em><b>Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min</em>' attribute.
	 * @see #setMin(double)
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwNumberAttribute_Min()
	 * @model required="true"
	 * @generated
	 */
	double getMin();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.DwNumberAttribute#getMin <em>Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min</em>' attribute.
	 * @see #getMin()
	 * @generated
	 */
	void setMin(double value);

	/**
	 * Returns the value of the '<em><b>Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max</em>' attribute.
	 * @see #setMax(double)
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwNumberAttribute_Max()
	 * @model required="true"
	 * @generated
	 */
	double getMax();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.DwNumberAttribute#getMax <em>Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max</em>' attribute.
	 * @see #getMax()
	 * @generated
	 */
	void setMax(double value);

	/**
	 * Returns the value of the '<em><b>Default Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Value</em>' attribute.
	 * @see #setDefaultValue(double)
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwNumberAttribute_DefaultValue()
	 * @model required="true"
	 * @generated
	 */
	double getDefaultValue();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.DwNumberAttribute#getDefaultValue <em>Default Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Value</em>' attribute.
	 * @see #getDefaultValue()
	 * @generated
	 */
	void setDefaultValue(double value);

} // DwNumberAttribute
