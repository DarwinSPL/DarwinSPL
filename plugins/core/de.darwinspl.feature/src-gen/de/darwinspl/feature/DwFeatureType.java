/**
 */
package de.darwinspl.feature;

import de.darwinspl.temporal.DwSingleTemporalIntervalElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.DwFeatureType#getType <em>Type</em>}</li>
 *   <li>{@link de.darwinspl.feature.DwFeatureType#getFeature <em>Feature</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.DwFeaturePackage#getDwFeatureType()
 * @model
 * @generated
 */
public interface DwFeatureType extends DwSingleTemporalIntervalElement {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link de.darwinspl.feature.DwFeatureTypeEnum}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see de.darwinspl.feature.DwFeatureTypeEnum
	 * @see #setType(DwFeatureTypeEnum)
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwFeatureType_Type()
	 * @model required="true"
	 * @generated
	 */
	DwFeatureTypeEnum getType();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.DwFeatureType#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see de.darwinspl.feature.DwFeatureTypeEnum
	 * @see #getType()
	 * @generated
	 */
	void setType(DwFeatureTypeEnum value);

	/**
	 * Returns the value of the '<em><b>Feature</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.darwinspl.feature.DwFeature#getTypes <em>Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature</em>' container reference.
	 * @see #setFeature(DwFeature)
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwFeatureType_Feature()
	 * @see de.darwinspl.feature.DwFeature#getTypes
	 * @model opposite="types" required="true" transient="false"
	 * @generated
	 */
	DwFeature getFeature();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.DwFeatureType#getFeature <em>Feature</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature</em>' container reference.
	 * @see #getFeature()
	 * @generated
	 */
	void setFeature(DwFeature value);

} // DwFeatureType
