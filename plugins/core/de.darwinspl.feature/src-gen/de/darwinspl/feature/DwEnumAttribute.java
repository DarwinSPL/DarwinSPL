/**
 */
package de.darwinspl.feature;

import de.darwinspl.datatypes.DwEnum;
import de.darwinspl.datatypes.DwEnumLiteral;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Enum Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.DwEnumAttribute#getEnum <em>Enum</em>}</li>
 *   <li>{@link de.darwinspl.feature.DwEnumAttribute#getDefaultValue <em>Default Value</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.DwFeaturePackage#getDwEnumAttribute()
 * @model
 * @generated
 */
public interface DwEnumAttribute extends DwFeatureAttribute {
	/**
	 * Returns the value of the '<em><b>Enum</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enum</em>' reference.
	 * @see #setEnum(DwEnum)
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwEnumAttribute_Enum()
	 * @model required="true"
	 * @generated
	 */
	DwEnum getEnum();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.DwEnumAttribute#getEnum <em>Enum</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enum</em>' reference.
	 * @see #getEnum()
	 * @generated
	 */
	void setEnum(DwEnum value);

	/**
	 * Returns the value of the '<em><b>Default Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Value</em>' reference.
	 * @see #setDefaultValue(DwEnumLiteral)
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwEnumAttribute_DefaultValue()
	 * @model
	 * @generated
	 */
	DwEnumLiteral getDefaultValue();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.DwEnumAttribute#getDefaultValue <em>Default Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Value</em>' reference.
	 * @see #getDefaultValue()
	 * @generated
	 */
	void setDefaultValue(DwEnumLiteral value);

} // DwEnumAttribute
