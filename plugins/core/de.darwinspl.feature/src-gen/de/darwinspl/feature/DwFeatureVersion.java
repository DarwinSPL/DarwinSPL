/**
 */
package de.darwinspl.feature;

import de.darwinspl.temporal.DwMultiTemporalIntervalElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Version</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.DwFeatureVersion#getNumber <em>Number</em>}</li>
 *   <li>{@link de.darwinspl.feature.DwFeatureVersion#getSupersededVersion <em>Superseded Version</em>}</li>
 *   <li>{@link de.darwinspl.feature.DwFeatureVersion#getSupersedingVersions <em>Superseding Versions</em>}</li>
 *   <li>{@link de.darwinspl.feature.DwFeatureVersion#getFeature <em>Feature</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.DwFeaturePackage#getDwFeatureVersion()
 * @model
 * @generated
 */
public interface DwFeatureVersion extends DwMultiTemporalIntervalElement {
	/**
	 * Returns the value of the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number</em>' attribute.
	 * @see #setNumber(String)
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwFeatureVersion_Number()
	 * @model required="true"
	 * @generated
	 */
	String getNumber();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.DwFeatureVersion#getNumber <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number</em>' attribute.
	 * @see #getNumber()
	 * @generated
	 */
	void setNumber(String value);

	/**
	 * Returns the value of the '<em><b>Superseded Version</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Superseded Version</em>' reference.
	 * @see #setSupersededVersion(DwFeatureVersion)
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwFeatureVersion_SupersededVersion()
	 * @model
	 * @generated
	 */
	DwFeatureVersion getSupersededVersion();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.DwFeatureVersion#getSupersededVersion <em>Superseded Version</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Superseded Version</em>' reference.
	 * @see #getSupersededVersion()
	 * @generated
	 */
	void setSupersededVersion(DwFeatureVersion value);

	/**
	 * Returns the value of the '<em><b>Superseding Versions</b></em>' reference list.
	 * The list contents are of type {@link de.darwinspl.feature.DwFeatureVersion}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Superseding Versions</em>' reference list.
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwFeatureVersion_SupersedingVersions()
	 * @model
	 * @generated
	 */
	EList<DwFeatureVersion> getSupersedingVersions();

	/**
	 * Returns the value of the '<em><b>Feature</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.darwinspl.feature.DwFeature#getVersions <em>Versions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature</em>' container reference.
	 * @see #setFeature(DwFeature)
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwFeatureVersion_Feature()
	 * @see de.darwinspl.feature.DwFeature#getVersions
	 * @model opposite="versions" required="true" transient="false"
	 * @generated
	 */
	DwFeature getFeature();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.DwFeatureVersion#getFeature <em>Feature</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature</em>' container reference.
	 * @see #getFeature()
	 * @generated
	 */
	void setFeature(DwFeature value);

} // DwFeatureVersion
