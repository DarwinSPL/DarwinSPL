/**
 */
package de.darwinspl.feature.util;

import de.darwinspl.feature.*;

import de.darwinspl.temporal.DwElementWithId;
import de.darwinspl.temporal.DwMultiTemporalIntervalElement;
import de.darwinspl.temporal.DwSingleTemporalIntervalElement;
import de.darwinspl.temporal.DwTemporalElement;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see de.darwinspl.feature.DwFeaturePackage
 * @generated
 */
public class DwFeatureSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DwFeaturePackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwFeatureSwitch() {
		if (modelPackage == null) {
			modelPackage = DwFeaturePackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case DwFeaturePackage.DW_TEMPORAL_FEATURE_MODEL: {
				DwTemporalFeatureModel dwTemporalFeatureModel = (DwTemporalFeatureModel)theEObject;
				T result = caseDwTemporalFeatureModel(dwTemporalFeatureModel);
				if (result == null) result = caseDwElementWithId(dwTemporalFeatureModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwFeaturePackage.DW_NAMED_ELEMENT: {
				DwNamedElement dwNamedElement = (DwNamedElement)theEObject;
				T result = caseDwNamedElement(dwNamedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwFeaturePackage.DW_NAME: {
				DwName dwName = (DwName)theEObject;
				T result = caseDwName(dwName);
				if (result == null) result = caseDwSingleTemporalIntervalElement(dwName);
				if (result == null) result = caseDwTemporalElement(dwName);
				if (result == null) result = caseDwElementWithId(dwName);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwFeaturePackage.DW_FEATURE: {
				DwFeature dwFeature = (DwFeature)theEObject;
				T result = caseDwFeature(dwFeature);
				if (result == null) result = caseDwNamedElement(dwFeature);
				if (result == null) result = caseDwMultiTemporalIntervalElement(dwFeature);
				if (result == null) result = caseDwTemporalElement(dwFeature);
				if (result == null) result = caseDwElementWithId(dwFeature);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwFeaturePackage.DW_FEATURE_TYPE: {
				DwFeatureType dwFeatureType = (DwFeatureType)theEObject;
				T result = caseDwFeatureType(dwFeatureType);
				if (result == null) result = caseDwSingleTemporalIntervalElement(dwFeatureType);
				if (result == null) result = caseDwTemporalElement(dwFeatureType);
				if (result == null) result = caseDwElementWithId(dwFeatureType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwFeaturePackage.DW_FEATURE_VERSION: {
				DwFeatureVersion dwFeatureVersion = (DwFeatureVersion)theEObject;
				T result = caseDwFeatureVersion(dwFeatureVersion);
				if (result == null) result = caseDwMultiTemporalIntervalElement(dwFeatureVersion);
				if (result == null) result = caseDwTemporalElement(dwFeatureVersion);
				if (result == null) result = caseDwElementWithId(dwFeatureVersion);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwFeaturePackage.DW_FEATURE_ATTRIBUTE: {
				DwFeatureAttribute dwFeatureAttribute = (DwFeatureAttribute)theEObject;
				T result = caseDwFeatureAttribute(dwFeatureAttribute);
				if (result == null) result = caseDwNamedElement(dwFeatureAttribute);
				if (result == null) result = caseDwMultiTemporalIntervalElement(dwFeatureAttribute);
				if (result == null) result = caseDwTemporalElement(dwFeatureAttribute);
				if (result == null) result = caseDwElementWithId(dwFeatureAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwFeaturePackage.DW_NUMBER_ATTRIBUTE: {
				DwNumberAttribute dwNumberAttribute = (DwNumberAttribute)theEObject;
				T result = caseDwNumberAttribute(dwNumberAttribute);
				if (result == null) result = caseDwFeatureAttribute(dwNumberAttribute);
				if (result == null) result = caseDwNamedElement(dwNumberAttribute);
				if (result == null) result = caseDwMultiTemporalIntervalElement(dwNumberAttribute);
				if (result == null) result = caseDwTemporalElement(dwNumberAttribute);
				if (result == null) result = caseDwElementWithId(dwNumberAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwFeaturePackage.DW_BOOLEAN_ATTRIBUTE: {
				DwBooleanAttribute dwBooleanAttribute = (DwBooleanAttribute)theEObject;
				T result = caseDwBooleanAttribute(dwBooleanAttribute);
				if (result == null) result = caseDwFeatureAttribute(dwBooleanAttribute);
				if (result == null) result = caseDwNamedElement(dwBooleanAttribute);
				if (result == null) result = caseDwMultiTemporalIntervalElement(dwBooleanAttribute);
				if (result == null) result = caseDwTemporalElement(dwBooleanAttribute);
				if (result == null) result = caseDwElementWithId(dwBooleanAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwFeaturePackage.DW_STRING_ATTRIBUTE: {
				DwStringAttribute dwStringAttribute = (DwStringAttribute)theEObject;
				T result = caseDwStringAttribute(dwStringAttribute);
				if (result == null) result = caseDwFeatureAttribute(dwStringAttribute);
				if (result == null) result = caseDwNamedElement(dwStringAttribute);
				if (result == null) result = caseDwMultiTemporalIntervalElement(dwStringAttribute);
				if (result == null) result = caseDwTemporalElement(dwStringAttribute);
				if (result == null) result = caseDwElementWithId(dwStringAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwFeaturePackage.DW_ENUM_ATTRIBUTE: {
				DwEnumAttribute dwEnumAttribute = (DwEnumAttribute)theEObject;
				T result = caseDwEnumAttribute(dwEnumAttribute);
				if (result == null) result = caseDwFeatureAttribute(dwEnumAttribute);
				if (result == null) result = caseDwNamedElement(dwEnumAttribute);
				if (result == null) result = caseDwMultiTemporalIntervalElement(dwEnumAttribute);
				if (result == null) result = caseDwTemporalElement(dwEnumAttribute);
				if (result == null) result = caseDwElementWithId(dwEnumAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwFeaturePackage.DW_GROUP: {
				DwGroup dwGroup = (DwGroup)theEObject;
				T result = caseDwGroup(dwGroup);
				if (result == null) result = caseDwMultiTemporalIntervalElement(dwGroup);
				if (result == null) result = caseDwTemporalElement(dwGroup);
				if (result == null) result = caseDwElementWithId(dwGroup);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwFeaturePackage.DW_GROUP_TYPE: {
				DwGroupType dwGroupType = (DwGroupType)theEObject;
				T result = caseDwGroupType(dwGroupType);
				if (result == null) result = caseDwSingleTemporalIntervalElement(dwGroupType);
				if (result == null) result = caseDwTemporalElement(dwGroupType);
				if (result == null) result = caseDwElementWithId(dwGroupType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwFeaturePackage.DW_ROOT_FEATURE_CONTAINER: {
				DwRootFeatureContainer dwRootFeatureContainer = (DwRootFeatureContainer)theEObject;
				T result = caseDwRootFeatureContainer(dwRootFeatureContainer);
				if (result == null) result = caseDwSingleTemporalIntervalElement(dwRootFeatureContainer);
				if (result == null) result = caseDwTemporalElement(dwRootFeatureContainer);
				if (result == null) result = caseDwElementWithId(dwRootFeatureContainer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwFeaturePackage.DW_PARENT_FEATURE_CHILD_GROUP_CONTAINER: {
				DwParentFeatureChildGroupContainer dwParentFeatureChildGroupContainer = (DwParentFeatureChildGroupContainer)theEObject;
				T result = caseDwParentFeatureChildGroupContainer(dwParentFeatureChildGroupContainer);
				if (result == null) result = caseDwSingleTemporalIntervalElement(dwParentFeatureChildGroupContainer);
				if (result == null) result = caseDwTemporalElement(dwParentFeatureChildGroupContainer);
				if (result == null) result = caseDwElementWithId(dwParentFeatureChildGroupContainer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwFeaturePackage.DW_GROUP_COMPOSITION: {
				DwGroupComposition dwGroupComposition = (DwGroupComposition)theEObject;
				T result = caseDwGroupComposition(dwGroupComposition);
				if (result == null) result = caseDwSingleTemporalIntervalElement(dwGroupComposition);
				if (result == null) result = caseDwTemporalElement(dwGroupComposition);
				if (result == null) result = caseDwElementWithId(dwGroupComposition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Temporal Feature Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Temporal Feature Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwTemporalFeatureModel(DwTemporalFeatureModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwNamedElement(DwNamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Name</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Name</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwName(DwName object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Feature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Feature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeature(DwFeature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureType(DwFeatureType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Version</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Version</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureVersion(DwFeatureVersion object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureAttribute(DwFeatureAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Number Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Number Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwNumberAttribute(DwNumberAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Boolean Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Boolean Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwBooleanAttribute(DwBooleanAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw String Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw String Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwStringAttribute(DwStringAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Enum Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Enum Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwEnumAttribute(DwEnumAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Group</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwGroup(DwGroup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Group Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Group Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwGroupType(DwGroupType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Root Feature Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Root Feature Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwRootFeatureContainer(DwRootFeatureContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Parent Feature Child Group Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Parent Feature Child Group Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwParentFeatureChildGroupContainer(DwParentFeatureChildGroupContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Group Composition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Group Composition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwGroupComposition(DwGroupComposition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Element With Id</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Element With Id</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwElementWithId(DwElementWithId object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Temporal Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Temporal Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwTemporalElement(DwTemporalElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Single Temporal Interval Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Single Temporal Interval Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwSingleTemporalIntervalElement(DwSingleTemporalIntervalElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Multi Temporal Interval Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Multi Temporal Interval Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwMultiTemporalIntervalElement(DwMultiTemporalIntervalElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //DwFeatureSwitch
