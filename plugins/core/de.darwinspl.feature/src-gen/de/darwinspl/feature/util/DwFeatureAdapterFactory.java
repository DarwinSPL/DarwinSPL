/**
 */
package de.darwinspl.feature.util;

import de.darwinspl.feature.*;

import de.darwinspl.temporal.DwElementWithId;
import de.darwinspl.temporal.DwMultiTemporalIntervalElement;
import de.darwinspl.temporal.DwSingleTemporalIntervalElement;
import de.darwinspl.temporal.DwTemporalElement;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see de.darwinspl.feature.DwFeaturePackage
 * @generated
 */
public class DwFeatureAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DwFeaturePackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwFeatureAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = DwFeaturePackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwFeatureSwitch<Adapter> modelSwitch =
		new DwFeatureSwitch<Adapter>() {
			@Override
			public Adapter caseDwTemporalFeatureModel(DwTemporalFeatureModel object) {
				return createDwTemporalFeatureModelAdapter();
			}
			@Override
			public Adapter caseDwNamedElement(DwNamedElement object) {
				return createDwNamedElementAdapter();
			}
			@Override
			public Adapter caseDwName(DwName object) {
				return createDwNameAdapter();
			}
			@Override
			public Adapter caseDwFeature(DwFeature object) {
				return createDwFeatureAdapter();
			}
			@Override
			public Adapter caseDwFeatureType(DwFeatureType object) {
				return createDwFeatureTypeAdapter();
			}
			@Override
			public Adapter caseDwFeatureVersion(DwFeatureVersion object) {
				return createDwFeatureVersionAdapter();
			}
			@Override
			public Adapter caseDwFeatureAttribute(DwFeatureAttribute object) {
				return createDwFeatureAttributeAdapter();
			}
			@Override
			public Adapter caseDwNumberAttribute(DwNumberAttribute object) {
				return createDwNumberAttributeAdapter();
			}
			@Override
			public Adapter caseDwBooleanAttribute(DwBooleanAttribute object) {
				return createDwBooleanAttributeAdapter();
			}
			@Override
			public Adapter caseDwStringAttribute(DwStringAttribute object) {
				return createDwStringAttributeAdapter();
			}
			@Override
			public Adapter caseDwEnumAttribute(DwEnumAttribute object) {
				return createDwEnumAttributeAdapter();
			}
			@Override
			public Adapter caseDwGroup(DwGroup object) {
				return createDwGroupAdapter();
			}
			@Override
			public Adapter caseDwGroupType(DwGroupType object) {
				return createDwGroupTypeAdapter();
			}
			@Override
			public Adapter caseDwRootFeatureContainer(DwRootFeatureContainer object) {
				return createDwRootFeatureContainerAdapter();
			}
			@Override
			public Adapter caseDwParentFeatureChildGroupContainer(DwParentFeatureChildGroupContainer object) {
				return createDwParentFeatureChildGroupContainerAdapter();
			}
			@Override
			public Adapter caseDwGroupComposition(DwGroupComposition object) {
				return createDwGroupCompositionAdapter();
			}
			@Override
			public Adapter caseDwElementWithId(DwElementWithId object) {
				return createDwElementWithIdAdapter();
			}
			@Override
			public Adapter caseDwTemporalElement(DwTemporalElement object) {
				return createDwTemporalElementAdapter();
			}
			@Override
			public Adapter caseDwSingleTemporalIntervalElement(DwSingleTemporalIntervalElement object) {
				return createDwSingleTemporalIntervalElementAdapter();
			}
			@Override
			public Adapter caseDwMultiTemporalIntervalElement(DwMultiTemporalIntervalElement object) {
				return createDwMultiTemporalIntervalElementAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.DwTemporalFeatureModel <em>Dw Temporal Feature Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.DwTemporalFeatureModel
	 * @generated
	 */
	public Adapter createDwTemporalFeatureModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.DwNamedElement <em>Dw Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.DwNamedElement
	 * @generated
	 */
	public Adapter createDwNamedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.DwName <em>Dw Name</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.DwName
	 * @generated
	 */
	public Adapter createDwNameAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.DwFeature <em>Dw Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.DwFeature
	 * @generated
	 */
	public Adapter createDwFeatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.DwFeatureType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.DwFeatureType
	 * @generated
	 */
	public Adapter createDwFeatureTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.DwFeatureVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.DwFeatureVersion
	 * @generated
	 */
	public Adapter createDwFeatureVersionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.DwFeatureAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.DwFeatureAttribute
	 * @generated
	 */
	public Adapter createDwFeatureAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.DwNumberAttribute <em>Dw Number Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.DwNumberAttribute
	 * @generated
	 */
	public Adapter createDwNumberAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.DwBooleanAttribute <em>Dw Boolean Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.DwBooleanAttribute
	 * @generated
	 */
	public Adapter createDwBooleanAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.DwStringAttribute <em>Dw String Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.DwStringAttribute
	 * @generated
	 */
	public Adapter createDwStringAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.DwEnumAttribute <em>Dw Enum Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.DwEnumAttribute
	 * @generated
	 */
	public Adapter createDwEnumAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.DwGroup <em>Dw Group</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.DwGroup
	 * @generated
	 */
	public Adapter createDwGroupAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.DwGroupType <em>Dw Group Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.DwGroupType
	 * @generated
	 */
	public Adapter createDwGroupTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.DwRootFeatureContainer <em>Dw Root Feature Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.DwRootFeatureContainer
	 * @generated
	 */
	public Adapter createDwRootFeatureContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.DwParentFeatureChildGroupContainer <em>Dw Parent Feature Child Group Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.DwParentFeatureChildGroupContainer
	 * @generated
	 */
	public Adapter createDwParentFeatureChildGroupContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.DwGroupComposition <em>Dw Group Composition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.DwGroupComposition
	 * @generated
	 */
	public Adapter createDwGroupCompositionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.temporal.DwElementWithId <em>Dw Element With Id</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.temporal.DwElementWithId
	 * @generated
	 */
	public Adapter createDwElementWithIdAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.temporal.DwTemporalElement <em>Dw Temporal Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.temporal.DwTemporalElement
	 * @generated
	 */
	public Adapter createDwTemporalElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.temporal.DwSingleTemporalIntervalElement <em>Dw Single Temporal Interval Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.temporal.DwSingleTemporalIntervalElement
	 * @generated
	 */
	public Adapter createDwSingleTemporalIntervalElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.temporal.DwMultiTemporalIntervalElement <em>Dw Multi Temporal Interval Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.temporal.DwMultiTemporalIntervalElement
	 * @generated
	 */
	public Adapter createDwMultiTemporalIntervalElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //DwFeatureAdapterFactory
