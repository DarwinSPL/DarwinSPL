/**
 */
package de.darwinspl.feature;

import de.darwinspl.datatypes.DwEnum;

import de.darwinspl.temporal.DwElementWithId;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Temporal Feature Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.DwTemporalFeatureModel#getFeatures <em>Features</em>}</li>
 *   <li>{@link de.darwinspl.feature.DwTemporalFeatureModel#getGroups <em>Groups</em>}</li>
 *   <li>{@link de.darwinspl.feature.DwTemporalFeatureModel#getEnums <em>Enums</em>}</li>
 *   <li>{@link de.darwinspl.feature.DwTemporalFeatureModel#getRootFeatures <em>Root Features</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.DwFeaturePackage#getDwTemporalFeatureModel()
 * @model
 * @generated
 */
public interface DwTemporalFeatureModel extends DwElementWithId {
	/**
	 * Returns the value of the '<em><b>Features</b></em>' containment reference list.
	 * The list contents are of type {@link de.darwinspl.feature.DwFeature}.
	 * It is bidirectional and its opposite is '{@link de.darwinspl.feature.DwFeature#getFeatureModel <em>Feature Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Features</em>' containment reference list.
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwTemporalFeatureModel_Features()
	 * @see de.darwinspl.feature.DwFeature#getFeatureModel
	 * @model opposite="featureModel" containment="true"
	 * @generated
	 */
	EList<DwFeature> getFeatures();

	/**
	 * Returns the value of the '<em><b>Groups</b></em>' containment reference list.
	 * The list contents are of type {@link de.darwinspl.feature.DwGroup}.
	 * It is bidirectional and its opposite is '{@link de.darwinspl.feature.DwGroup#getFeatureModel <em>Feature Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Groups</em>' containment reference list.
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwTemporalFeatureModel_Groups()
	 * @see de.darwinspl.feature.DwGroup#getFeatureModel
	 * @model opposite="featureModel" containment="true"
	 * @generated
	 */
	EList<DwGroup> getGroups();

	/**
	 * Returns the value of the '<em><b>Enums</b></em>' containment reference list.
	 * The list contents are of type {@link de.darwinspl.datatypes.DwEnum}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enums</em>' containment reference list.
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwTemporalFeatureModel_Enums()
	 * @model containment="true"
	 * @generated
	 */
	EList<DwEnum> getEnums();

	/**
	 * Returns the value of the '<em><b>Root Features</b></em>' containment reference list.
	 * The list contents are of type {@link de.darwinspl.feature.DwRootFeatureContainer}.
	 * It is bidirectional and its opposite is '{@link de.darwinspl.feature.DwRootFeatureContainer#getFeatureModel <em>Feature Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Features</em>' containment reference list.
	 * @see de.darwinspl.feature.DwFeaturePackage#getDwTemporalFeatureModel_RootFeatures()
	 * @see de.darwinspl.feature.DwRootFeatureContainer#getFeatureModel
	 * @model opposite="featureModel" containment="true"
	 * @generated
	 */
	EList<DwRootFeatureContainer> getRootFeatures();

} // DwTemporalFeatureModel
