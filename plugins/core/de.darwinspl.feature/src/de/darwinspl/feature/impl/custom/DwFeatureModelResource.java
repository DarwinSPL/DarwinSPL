package de.darwinspl.feature.impl.custom;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

public class DwFeatureModelResource extends XMIResourceImpl {

	private static final String EXTENSION_POINT_ID = "de.darwinspl.feature.feature_model_resource_save_listener";
	Set<DwFeatureModelResourceListener> listeners;

	public DwFeatureModelResource(URI uri) {
		super(uri);
	}
	
	@Override
	public void save(Map<?, ?> options) throws IOException {
		super.save(options);
		
		if(listeners == null)
			initListeners();
		
		for(DwFeatureModelResourceListener listener : listeners)
			listener.resourceSaved(this);
	}
	
	public void discard() {
		if(listeners == null)
			initListeners();
		
		for(DwFeatureModelResourceListener listener : listeners)
			listener.resourceChangesDiscarded(this);
	}

	private void initListeners() {
		listeners = new HashSet<>();
		IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor(EXTENSION_POINT_ID);
		
        for (IConfigurationElement e : config) {
			try {
				Object o = e.createExecutableExtension("implementation");
	            if (o instanceof DwFeatureModelResourceListener) {
	            	listeners.add((DwFeatureModelResourceListener) o);
	            }
			} catch (CoreException e1) {
				e1.printStackTrace();
			}
        }
	}
	
}
