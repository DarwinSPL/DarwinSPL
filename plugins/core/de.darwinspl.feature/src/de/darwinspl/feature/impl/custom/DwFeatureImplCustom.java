package de.darwinspl.feature.impl.custom;

import java.util.Date;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.impl.DwFeatureImpl;
import de.darwinspl.feature.util.custom.DwFeatureUtil;

public class DwFeatureImplCustom extends DwFeatureImpl {

	protected DwFeatureImplCustom() {
		super();
	}
	
	@Override
	public boolean isMandatory(final Date date) {
		return DwFeatureUtil.isMandatory(this, date);
	}
	
	@Override
	public boolean isOptional(final Date date) {
		return DwFeatureUtil.isOptional(this, date);
	}

	@Override
	public String toString() {
		return "DwFeatureImplCustom (names=" + names + ", id=" + id + ")";
	}

	@Override
	public boolean equals(Object obj) {
		if(super.equals(obj))
			return true;
		
		if(obj instanceof DwFeature) {
			return ((DwFeature) obj).getId().equals(this.getId());
		}
		
		return false;
	}
	
}
