package de.darwinspl.feature.impl.custom;

import de.darwinspl.feature.DwBooleanAttribute;
import de.darwinspl.feature.DwEnumAttribute;
import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureFactory;
import de.darwinspl.feature.DwFeatureType;
import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupComposition;
import de.darwinspl.feature.DwGroupType;
import de.darwinspl.feature.DwName;
import de.darwinspl.feature.DwNumberAttribute;
import de.darwinspl.feature.DwParentFeatureChildGroupContainer;
import de.darwinspl.feature.DwRootFeatureContainer;
import de.darwinspl.feature.DwStringAttribute;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.impl.DwFeatureFactoryImpl;

public class DwFeatureFactoryWithIds extends DwFeatureFactoryImpl {

	public static DwFeatureFactory eINSTANCE = DwFeatureFactoryWithIds.init();
	
	public static DwFeatureFactory init() {
		DwFeatureFactory featureFactory = DwFeatureFactoryImpl.init();
		if(!(featureFactory instanceof DwFeatureFactoryWithIds)) {
			featureFactory = new DwFeatureFactoryWithIds();
		}
		return featureFactory;
	}
	
	@Override
	public DwTemporalFeatureModel createDwTemporalFeatureModel() {
		DwTemporalFeatureModel tfm = super.createDwTemporalFeatureModel();
		tfm.createId();
		return tfm;
	}
	
	@Override
	public DwBooleanAttribute createDwBooleanAttribute() {
		DwBooleanAttribute DwBooleanAttribute = super.createDwBooleanAttribute();
		DwBooleanAttribute.createId();
		return DwBooleanAttribute;
	}
	
	@Override
	public DwFeatureType createDwFeatureType() {
		DwFeatureType DwFeatureType = super.createDwFeatureType();
		DwFeatureType.createId();
		return DwFeatureType;
	}
	

	@Override
	public DwGroupType createDwGroupType() {
		DwGroupType DwGroupType = super.createDwGroupType();
		DwGroupType.createId();
		return DwGroupType;
	}
	
	@Override
	public DwEnumAttribute createDwEnumAttribute() {
		DwEnumAttribute DwEnumAttribute = super.createDwEnumAttribute();
		DwEnumAttribute.createId();
		return DwEnumAttribute;
	}
	
	@Override
	public DwFeature createDwFeature() {
		DwFeature DwFeature = new DwFeatureImplCustom();
		DwFeature.createId();
		return DwFeature;
	}
	
	@Override
	public DwParentFeatureChildGroupContainer createDwParentFeatureChildGroupContainer() {
		DwParentFeatureChildGroupContainer DwParentFeatureChildGroupContainer = super.createDwParentFeatureChildGroupContainer();
		DwParentFeatureChildGroupContainer.createId();
		return DwParentFeatureChildGroupContainer;
	}
	
	@Override
	public DwGroup createDwGroup() {
		DwGroup DwGroup = new DwGroupImplCustom();
		DwGroup.createId();
		return DwGroup;
	}
	
	@Override
	public DwGroupComposition createDwGroupComposition() {
		DwGroupComposition DwGroupComposition = super.createDwGroupComposition();
		DwGroupComposition.createId();
		return DwGroupComposition;
	}
	
	@Override
	public DwNumberAttribute createDwNumberAttribute() {
		DwNumberAttribute DwNumberAttribute = super.createDwNumberAttribute();
		DwNumberAttribute.createId();
		return DwNumberAttribute;
	}
	
	@Override
	public DwRootFeatureContainer createDwRootFeatureContainer() {
		DwRootFeatureContainer DwRootFeatureContainer = super.createDwRootFeatureContainer();
		DwRootFeatureContainer.createId();
		return DwRootFeatureContainer;
	}
	
	@Override
	public DwStringAttribute createDwStringAttribute() {
		DwStringAttribute DwStringAttribute = super.createDwStringAttribute();
		DwStringAttribute.createId();
		return DwStringAttribute;
	}
	
	@Override
	public DwFeatureVersion createDwFeatureVersion() {
		DwFeatureVersion DwFeatureVersion = super.createDwFeatureVersion();
		DwFeatureVersion.createId();
		return DwFeatureVersion;
	}
	
	@Override
	public DwName createDwName() {
		DwName name = new DwNameImplCustom();
		name.createId();
		return name;
	}
}
