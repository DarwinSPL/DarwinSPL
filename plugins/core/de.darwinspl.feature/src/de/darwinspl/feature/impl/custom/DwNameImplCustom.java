package de.darwinspl.feature.impl.custom;

import de.darwinspl.feature.impl.DwNameImpl;

public class DwNameImplCustom extends DwNameImpl {

	protected DwNameImplCustom() {
		super();
	}

	@Override
	public String toString() {
		return "DwNameImplCustom (name=" + name + ")";
	}
	
}
