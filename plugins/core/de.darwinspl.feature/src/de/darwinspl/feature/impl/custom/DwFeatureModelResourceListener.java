package de.darwinspl.feature.impl.custom;

public interface DwFeatureModelResourceListener {

	public void resourceSaved(DwFeatureModelResource res);
	
	public void resourceChangesDiscarded(DwFeatureModelResource res);

}
