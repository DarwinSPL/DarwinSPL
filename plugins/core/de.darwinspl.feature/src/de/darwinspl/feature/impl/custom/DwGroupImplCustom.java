package de.darwinspl.feature.impl.custom;

import java.util.Date;

import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.impl.DwGroupImpl;
import de.darwinspl.feature.util.custom.DwFeatureUtil;

public class DwGroupImplCustom extends DwGroupImpl {
	
	protected DwGroupImplCustom() {
		super();
	}
	
	@Override
	public boolean isAlternative(final Date date) {
		return DwFeatureUtil.isAlternative(this, date);
	}

	@Override
	public boolean isOr(final Date date) {
		return DwFeatureUtil.isOr(this, date);
	}

	@Override
	public boolean isAnd(final Date date) {
		return DwFeatureUtil.isAnd(this, date);
	}

	@Override
	public boolean equals(Object obj) {
		if(super.equals(obj))
			return true;
		
		if(obj instanceof DwGroup) {
			return ((DwGroup) obj).getId().equals(this.getId());
		}
		
		return false;
	}
}
