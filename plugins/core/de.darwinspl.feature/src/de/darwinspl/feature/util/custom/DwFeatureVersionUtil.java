package de.darwinspl.feature.util.custom;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.temporal.util.DwEvolutionUtil;

public class DwFeatureVersionUtil {

	/**
	 * Retrieves all versions that are directly (next level) superseding @version at the given date. If a child version is invalid, its superseding versions are added (recursively) 
	 * @param version
	 * @param date
	 * @return
	 */
	public static List<DwFeatureVersion> getValidSupersedingVersions(DwFeatureVersion version, Date date) {
		List<DwFeatureVersion> validSupersedingVersions = new ArrayList<DwFeatureVersion>();
		
		for(DwFeatureVersion supersedingVersion: version.getSupersedingVersions()) {
			if(DwEvolutionUtil.isValid(supersedingVersion, date)) {
				validSupersedingVersions.add(supersedingVersion);
			}
			else {
				validSupersedingVersions.addAll(getValidSupersedingVersions(supersedingVersion, date));
			}
		}
		
		return validSupersedingVersions;
	}
	
	
	/**
	 * Retrieves the version that a @version supersedes. If the superseded version is not valid @date, its superseded version is returned instaed (recursively)
	 * @param version
	 * @param date
	 * @return
	 */
	public static DwFeatureVersion getValidSupersededVersion(DwFeatureVersion version, Date date) {
		if(version.getSupersededVersion() == null) {
			return null;
		}
		else if(DwEvolutionUtil.isValid(version.getSupersededVersion(), date)) {
			return version.getSupersededVersion();
		}
		else {
			return getValidSupersededVersion(version.getSupersededVersion(), date);
		}
	}
	
	/**
	 * Retrieves the current 
	 * @param feature
	 * @param date
	 * @return
	 */
	public static DwFeatureVersion getRootVersion(DwFeature feature) {
		for(DwFeatureVersion version: feature.getVersions()) {
			if(isRootVersion(version)) {
				return version;
			}
		}
		
		return null;
	}
	
	public static boolean isRootVersion(DwFeatureVersion version) {
		if(version.getSupersededVersion() == null) {
			return true;
		}
		return false;
	}
	
	public static DwFeatureVersion getLastVersionOnMostRecentBranch(DwFeature feature, Date date) {
		List<DwFeatureVersion> versions = feature.getVersions();
		DwFeatureVersion lastVersion = null;
		
		for (DwFeatureVersion version : versions) {
			if (isLastVersionOnBranch(version, date)) {
				lastVersion = version;
			}
		}
		
		return lastVersion;
	}

	public static void wireAndAddVersionAsRoot(DwFeatureVersion version, DwFeature parentFeature) {
		if (version == null || parentFeature == null) {
			throw new InvalidParameterException();
		}
		
		//NOTE: Order mandatory for possible notifications.
		wireVersionAsRoot(version, parentFeature);
		addVersion(version, parentFeature);
	}
	
	public static void wireAndAddVersionAfter(DwFeatureVersion version, DwFeatureVersion addAfterVersion) {
		if (version == null || addAfterVersion == null) {
			throw new InvalidParameterException();
		}
		
		//NOTE: Order mandatory for possible notifications.
		wireVersionAfter(version, addAfterVersion);
		addVersionAfter(version, addAfterVersion);
	}
	
	public static void wireAndAddVersionAfterOnNewBranch(DwFeatureVersion version, DwFeatureVersion addAfterVersion) {
		if (version == null || addAfterVersion == null) {
			throw new InvalidParameterException();
		}
		
		//NOTE: Order mandatory for possible notifications.
		wireVersionAfterOnNewBranch(version, addAfterVersion);
		addVersionAfter(version, addAfterVersion);
	}
	
	public static void wireAndAddVersionBefore(DwFeatureVersion version, DwFeatureVersion addBeforeVersion) {
		if (version == null || addBeforeVersion == null) {
			throw new InvalidParameterException();
		}
		
		//NOTE: Order mandatory for possible notifications.
		wireVersionBefore(version, addBeforeVersion);
		addVersionBefore(version, addBeforeVersion);
	}
	
	
	public static void addVersionAfter(DwFeatureVersion version, DwFeatureVersion addAfterVersion) {
		if (version == null || addAfterVersion == null) {
			throw new InvalidParameterException();
		}
		
		DwFeature parentFeature = addAfterVersion.getFeature();
		List<DwFeatureVersion> versions = parentFeature.getVersions();
		int index = versions.indexOf(addAfterVersion) + 1;
		addVersion(version, parentFeature, index);
	}
	
	public static void addVersionBefore(DwFeatureVersion version, DwFeatureVersion addBeforeVersion) {
		if (version == null || addBeforeVersion == null) {
			throw new InvalidParameterException();
		}
		
		DwFeature parentFeature = addBeforeVersion.getFeature();
		List<DwFeatureVersion> versions = parentFeature.getVersions();
		int index = versions.indexOf(addBeforeVersion);
		addVersion(version, parentFeature, index);
	}
	
	public static void addVersion(DwFeatureVersion version, DwFeature parentFeature) {
		addVersion(version, parentFeature, -1);
	}
	
	public static void addVersion(DwFeatureVersion version, DwFeature parentFeature, int index) {
		List<DwFeatureVersion> versions = parentFeature.getVersions();
		
		if (index == -1) {
			versions.add(version);
		} else if (index < 0) {
			versions.add(0, version);
		} else if (index >= versions.size()) {
			versions.add(version);
		} else {
			versions.add(index, version);
		}
	}
	
	public static int removeVersion(DwFeatureVersion version) {
		DwFeature parentFeature = version.getFeature();
		
		if (parentFeature == null) {
			return -1;
		}
		
		List<DwFeatureVersion> versions = parentFeature.getVersions();
		int index = versions.indexOf(version);
		versions.remove(version);
		return index;
	}
	
	public static void wireVersionAsRoot(DwFeatureVersion version, DwFeature parentFeature) {
		if (version == null || parentFeature == null) {
			throw new InvalidParameterException();
		}
		
		DwFeatureVersion oldRootVersion = getRootVersion(parentFeature);
		
		if (oldRootVersion != null) {
			oldRootVersion.setSupersededVersion(version);
		}
	}
	
	public static void wireVersionAfter(DwFeatureVersion version, DwFeatureVersion insertAfterVersion) {
		if (version == null || insertAfterVersion == null) {
			throw new InvalidParameterException();
		}
		
		DwFeatureVersion predecessor = insertAfterVersion;
		List<DwFeatureVersion> successors = insertAfterVersion.getSupersedingVersions();
		
		rewireVersion(version, predecessor, successors);
	}
	
	public static void wireVersionAfterOnNewBranch(DwFeatureVersion version, DwFeatureVersion insertAfterVersion) {
		if (version == null || insertAfterVersion == null) {
			throw new InvalidParameterException();
		}
		
		DwFeatureVersion predecessor = insertAfterVersion;
		rewireVersion(version, predecessor, null);
	}
	
	public static void wireVersionBefore(DwFeatureVersion version, DwFeatureVersion insertBeforeVersion) {
		wireVersionBefore(version, insertBeforeVersion, false);
	}
	
	public static void wireVersionBefore(DwFeatureVersion version, DwFeatureVersion insertBeforeVersion, boolean insertBeforeBranching) {
		if (version == null || insertBeforeVersion == null) {
			throw new InvalidParameterException();
		}
		
		DwFeatureVersion predecessor = insertBeforeVersion.getSupersededVersion();
		
		if (predecessor == null) {
			DwFeature parentFeature = insertBeforeVersion.getFeature();
			wireVersionAsRoot(version, parentFeature);
		} else {
			List<DwFeatureVersion> successors = insertBeforeBranching ? predecessor.getSupersedingVersions() : Collections.singletonList(insertBeforeVersion);
			rewireVersion(version, predecessor, successors);
		}
	}
	
	public static void rewireVersion(DwFeatureVersion version, DwFeatureVersion newPredecessor, List<DwFeatureVersion> newSuccessors) {
		if (version == null) {
			return;
		}
		
		//Defensive copy as the list may change due to inverse references!
		List<DwFeatureVersion> newSuccessorsCopy = new ArrayList<DwFeatureVersion>();
		
		if (newSuccessors != null) {
			newSuccessorsCopy.addAll(newSuccessors);
		}
		
		version.setSupersededVersion(newPredecessor);
		
		for (DwFeatureVersion newSuccessor : newSuccessorsCopy) {
			newSuccessor.setSupersededVersion(version);
		}
	}
	
	public static void unwireAndRemoveVersion(DwFeatureVersion version) {
		DwFeatureVersionUtil.unwireVersion(version);
		DwFeatureVersionUtil.removeVersion(version);
	}
	
	public static void unwireVersion(DwFeatureVersion version) {
		List<DwFeatureVersion> successors = version.getSupersedingVersions();
		//Defensive copy as the list may change due to inverse references!
		List<DwFeatureVersion> oldSuccessors = new ArrayList<DwFeatureVersion>(successors);
		
		DwFeatureVersion oldPredecessor = version.getSupersededVersion();
		version.setSupersededVersion(null);
		
		for (DwFeatureVersion oldSuccessor : oldSuccessors) {
			oldSuccessor.setSupersededVersion(oldPredecessor);
		}
	}
	
	public static boolean isLastVersionOnBranch(DwFeatureVersion version, Date date) {
		List<DwFeatureVersion> successors = DwEvolutionUtil.getValidTemporalElements(version.getSupersedingVersions(), date);
		return successors.isEmpty();
	}
	
}
