package de.darwinspl.feature.util.exporter;

import java.util.Date;
import java.util.List;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureAttribute;
import de.darwinspl.feature.DwFeatureType;
import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupComposition;
import de.darwinspl.feature.DwGroupType;
import de.darwinspl.feature.DwName;
import de.darwinspl.feature.DwParentFeatureChildGroupContainer;
import de.darwinspl.feature.DwRootFeatureContainer;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.util.custom.DwTemporalFeatureModelWellFormednessException;
import de.darwinspl.temporal.util.DwEvolutionUtil;


public abstract class DwTemporalFeatureModelExporter<T> {	

	// TODO validity checks. -> new Util for a certain date
	
	/**
	 * Only containing valid elements of one point in time.
	 * @param constraintModel
	 */
	public T exportFeatureModel(DwTemporalFeatureModel featureModel, Date date) throws DwTemporalFeatureModelWellFormednessException {		
		
		DwRootFeatureContainer rootFeature = DwEvolutionUtil.getValidTemporalElement(featureModel.getRootFeatures(), date);
		
		DwFeature rootFeatureFeature = rootFeature.getFeature();
		
		DwName rootFeatureName = DwEvolutionUtil.getValidTemporalElement(rootFeatureFeature.getNames(), date);
		
		DwFeatureType rootFeatureType = DwEvolutionUtil.getValidTemporalElement(rootFeatureFeature.getTypes(), date);
		
		List<DwFeatureAttribute> validFeatureAttributes = DwEvolutionUtil.getValidTemporalElements(rootFeatureFeature.getAttributes(), date);
		
		List<DwFeatureVersion> validVersions = DwEvolutionUtil.getValidTemporalElements(rootFeatureFeature.getVersions(), date);
		
		visitFeature(rootFeatureFeature, rootFeatureName, rootFeatureType, validFeatureAttributes, validVersions, true, null, date);
		
		processSubTree(rootFeatureFeature, date);
		
		return finish();
		
	}
	
	private void processSubTree(DwFeature feature, Date date) throws DwTemporalFeatureModelWellFormednessException {
		List<DwParentFeatureChildGroupContainer> featureChildren = feature.getParentOf();
		for(DwParentFeatureChildGroupContainer featureChild: featureChildren) {
			DwGroup group = featureChild.getChildGroup();
			
			DwGroupType groupType = DwEvolutionUtil.getValidTemporalElement(group.getTypes(), date);
			visitGroup(group, groupType, feature, date);
			
			DwGroupComposition groupComposition = DwEvolutionUtil.getValidTemporalElement(group.getParentOf(), date);
			
			for(DwFeature subFeature: groupComposition.getChildFeatures()) {
				DwName subFeatureName = DwEvolutionUtil.getValidTemporalElement(subFeature.getNames(), date);
				
				DwFeatureType subFeatureType = DwEvolutionUtil.getValidTemporalElement(subFeature.getTypes(), date);
				
				List<DwFeatureAttribute> validFeatureAttributes = DwEvolutionUtil.getValidTemporalElements(subFeature.getAttributes(), date);
				
				List<DwFeatureVersion> validVersions = DwEvolutionUtil.getValidTemporalElements(subFeature.getVersions(), date);
				
				visitFeature(subFeature, subFeatureName, subFeatureType, validFeatureAttributes, validVersions, false, group, date);
			}
			
			for(DwFeature subFeature: groupComposition.getChildFeatures()) {
				processSubTree(subFeature, date);
			}
		}
	}
	
//	/**
//	 * sinceNull prioritized, then untilNull, then date
//	 * @param featureModel
//	 * @param date
//	 * @param sinceNull
//	 * @param untilNull
//	 * @return
//	 * @throws DwTemporalFeatureModelWellFormednessException
//	 */
//	public T exportFeatureModel(DwTemporalFeatureModel featureModel, Date date) throws DwTemporalFeatureModelWellFormednessException {	
//		// If date is null and no dates are available, take the current date
//		// if dates is not empty, take a date one day before the first available date
////		if(date == null) {
////			Calendar calendar = Calendar.getInstance();
////			
////			List<Date> dates = DwEvolutionUtil.collectDates(featureModel);
////			if(dates.isEmpty()) {
////				date = calendar.getTime();
////			} else {
////				calendar.setTime(dates.get(0));
////				calendar.add(Calendar.DAY_OF_MONTH, -1);
////				date = calendar.getTime();
////			}
////		}
////		
////		
////		DwTemporalFeatureModel featureModelCopy = (DwTemporalFeatureModel) DwEvolutionUtil.getCopyOfValidModel(featureModel, date);
//		return exportValidFeatureModel(featureModel, date);
//	}
	
	
	/**
	 * Group possibly null if root feature
	 * @param feature
	 * @param name
	 * @param cardinality
	 * @param attributes only valid ones at @date
	 * @param versions only valid ones at @date
	 * @param group
	 * @param date
	 */
	protected abstract void visitFeature(DwFeature feature, DwName name, DwFeatureType type, List<? extends DwFeatureAttribute> validAttributes, List<DwFeatureVersion> validVersions, boolean isRoot, DwGroup group, Date date) throws DwTemporalFeatureModelWellFormednessException;
	protected abstract void visitGroup(DwGroup group, DwGroupType type, DwFeature parent, Date date) throws DwTemporalFeatureModelWellFormednessException;
	protected abstract T finish();
}
