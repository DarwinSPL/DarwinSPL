package de.darwinspl.feature.util.custom;

import java.util.Date;
import java.util.List;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureFactory;
import de.darwinspl.feature.DwFeatureType;
import de.darwinspl.feature.DwFeatureTypeEnum;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupComposition;
import de.darwinspl.feature.DwGroupType;
import de.darwinspl.feature.DwGroupTypeEnum;
import de.darwinspl.feature.DwName;
import de.darwinspl.feature.DwParentFeatureChildGroupContainer;
import de.darwinspl.feature.DwRootFeatureContainer;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.impl.custom.DwFeatureFactoryWithIds;
import de.darwinspl.temporal.DwTemporalModelFactory;
import de.darwinspl.temporal.DwTemporalModelFactoryImplCustom;
import de.darwinspl.temporal.util.DwDateResolverUtil;
import de.darwinspl.temporal.util.DwEvolutionUtil;

/**
 * 
 * PLEASE use this class to create features, groups and friends! All required fields will be set appropriately.
 *  
 */

public class DwSimpleFeatureFactoryCustom {

	public static final String DEFAULT_FEATURE_NAME = "NewFeature";
	public static final DwFeatureTypeEnum DEFAULT_FEATURE_TYPE = DwFeatureTypeEnum.OPTIONAL;
	public static final DwGroupTypeEnum DEFAULT_GROUP_TYPE = DwGroupTypeEnum.AND;
	
	protected static final DwFeatureFactory FEATURE_FACTORY = DwFeatureFactoryWithIds.eINSTANCE;
	protected static final DwTemporalModelFactory TEMPORAL_FACTORY = DwTemporalModelFactoryImplCustom.eINSTANCE;
	
	
	
	// Feature Model
	
	public static DwTemporalFeatureModel createNewFeatureModel(DwFeature rootFeature) {
		DwRootFeatureContainer rootContainer = createNewRootFeatureContainer(rootFeature);
		DwTemporalFeatureModel model = createNewFeatureModel(rootContainer);
		
		return model;
	}
	
	public static DwTemporalFeatureModel createNewFeatureModel(DwRootFeatureContainer rootFeatureContainer) {
		DwTemporalFeatureModel model = createNewFeatureModel();
		
		model.getRootFeatures().add(rootFeatureContainer);
		model.getFeatures().add(rootFeatureContainer.getFeature());
		
		return model;
	}
	
	public static DwTemporalFeatureModel createNewFeatureModel() {
		return FEATURE_FACTORY.createDwTemporalFeatureModel();
	}
	
	
	
	// Names
	
	public static DwName createNewName(String name, Date creationDate) {
		DwName nameContainer = createNewName(name);
		DwEvolutionUtil.setTemporalValidity(nameContainer, creationDate, DwDateResolverUtil.ETERNITY_DATE);
		return nameContainer;
	}
	
	public static DwName createNewName(String name) {
		DwName nameContainer = createNewName();
		nameContainer.setName(name);
		return nameContainer;
	}
	
	public static DwName createNewName() {
		DwName nameContainer = FEATURE_FACTORY.createDwName();
		nameContainer.setValidity(TEMPORAL_FACTORY.createDwTemporalInterval());
		return nameContainer;
	}

	
	
	// Features
	
	public static DwFeature createNewFeatureWithDefaultName() {
		return createNewFeature(DEFAULT_FEATURE_NAME);
	}
	
	public static DwFeature createNewFeature(String name) {
		return createNewFeature(name, DEFAULT_FEATURE_TYPE);
	}

	public static DwFeature createNewFeatureWithDefaultName(Date creationDate) {
		return createNewFeature(DEFAULT_FEATURE_NAME, creationDate);
	}

	public static DwFeature createNewFeatureWithUnconflictingName(DwTemporalFeatureModel tfm, Date date) {
		List<DwName> allNames = DwFeatureUtil.getAllFeatureNames(tfm);
		
		int i=1;
		outer: while(true) {
			String name = DEFAULT_FEATURE_NAME + i;
			
			for(DwName existingName : allNames) {
				if(existingName.getName().equals(name)) {
					i++;
					continue outer;
				}
			}
			
			return createNewFeature(name, date);
		}
	}
	
	public static DwFeature createNewFeature(String name, Date creationDate) {
		return createNewFeature(name, DEFAULT_FEATURE_TYPE, creationDate);
	}
	
	public static DwFeature createNewFeature(String name, DwFeatureTypeEnum variationType) {
		return createNewFeature(name, variationType, DwDateResolverUtil.INITIAL_STATE_DATE);
	}
	
	public static DwFeature createNewFeature(String name, DwFeatureTypeEnum variationType, Date creationDate) {
		DwFeature feature = FEATURE_FACTORY.createDwFeature();
		DwEvolutionUtil.addNewTemporalValidity(feature, creationDate, DwDateResolverUtil.ETERNITY_DATE);
		
		feature.getTypes().add(createNewFeatureType(variationType, creationDate));
		feature.getNames().add(createNewName(name, creationDate));
		
		return feature;
	}

	
	
	// Root Feature Container
	
	public static DwRootFeatureContainer createNewRootFeatureContainer(DwFeature featureToBeAdded, Date creationDate) {
		DwRootFeatureContainer container = createNewRootFeatureContainer(creationDate);
		container.setFeature(featureToBeAdded);
		return container;
	}
	
	public static DwRootFeatureContainer createNewRootFeatureContainer(DwFeature featureToBeAdded) {
		DwRootFeatureContainer container = createNewRootFeatureContainer();
		container.setFeature(featureToBeAdded);
		return container;
	}
	
	public static DwRootFeatureContainer createNewRootFeatureContainer(Date creationDate) {
		DwRootFeatureContainer container = createNewRootFeatureContainer();
		DwEvolutionUtil.setTemporalValidity(container, creationDate, DwDateResolverUtil.ETERNITY_DATE);
		return container;
	}
	
	public static DwRootFeatureContainer createNewRootFeatureContainer() {
		DwRootFeatureContainer container = FEATURE_FACTORY.createDwRootFeatureContainer();
		container.setValidity(TEMPORAL_FACTORY.createDwTemporalInterval());
		return container;
	}
	
	
	
	// Feature Types
	
	public static DwFeatureType createNewFeatureType(DwFeatureTypeEnum type, Date creationDate) {
		DwFeatureType typeContainer = createNewFeatureType(type);
		DwEvolutionUtil.setTemporalValidity(typeContainer, creationDate, DwDateResolverUtil.ETERNITY_DATE);
		return typeContainer;
	}
	
	public static DwFeatureType createNewFeatureType(Date creationDate) {
		DwFeatureType typeContainer = createNewFeatureType();
		DwEvolutionUtil.setTemporalValidity(typeContainer, creationDate, DwDateResolverUtil.ETERNITY_DATE);
		return typeContainer;
	}
	
	public static DwFeatureType createNewFeatureType(DwFeatureTypeEnum type) {
		DwFeatureType typeContainer = createNewFeatureType();
		typeContainer.setType(type);
		return typeContainer;
	}
	
	public static DwFeatureType createNewFeatureType() {
		DwFeatureType typeContainer = FEATURE_FACTORY.createDwFeatureType();
		typeContainer.setValidity(TEMPORAL_FACTORY.createDwTemporalInterval());
		return typeContainer;
	}
	
	
	
	// Relations that Features hold to their child groups
	
	public static DwParentFeatureChildGroupContainer createNewParentFeatureChildGroupContainer(DwFeature parentFeature, DwGroup childGroup, Date creationDate) {
		DwParentFeatureChildGroupContainer relation = createNewParentFeatureChildGroupContainer(childGroup, creationDate);
		relation.setParent(parentFeature);
		return relation;
	}
	
	public static DwParentFeatureChildGroupContainer createNewParentFeatureChildGroupContainer(DwGroup childGroup, Date creationDate) {
		DwParentFeatureChildGroupContainer relation = createNewParentFeatureChildGroupContainer(creationDate);
		relation.setChildGroup(childGroup);
		return relation;
	}
	
	public static DwParentFeatureChildGroupContainer createNewParentFeatureChildGroupContainer(DwFeature parentFeature, Date creationDate) {
		DwParentFeatureChildGroupContainer relation = createNewParentFeatureChildGroupContainer(creationDate);
		relation.setParent(parentFeature);
		return relation;
	}
	
	public static DwParentFeatureChildGroupContainer createNewParentFeatureChildGroupContainer(Date creationDate) {
		DwParentFeatureChildGroupContainer relation = createNewParentFeatureChildGroupContainer();
		DwEvolutionUtil.setTemporalValidity(relation, creationDate, DwDateResolverUtil.ETERNITY_DATE);
		
		return relation;
	}
	
	public static DwParentFeatureChildGroupContainer createNewParentFeatureChildGroupContainer(DwFeature parentFeature) {
		DwParentFeatureChildGroupContainer relation = createNewParentFeatureChildGroupContainer();
		relation.setParent(parentFeature);
		
		return relation;
	}
	
	public static DwParentFeatureChildGroupContainer createNewParentFeatureChildGroupContainer(DwGroup childGroup) {
		DwParentFeatureChildGroupContainer relation = createNewParentFeatureChildGroupContainer();
		relation.setChildGroup(childGroup);
		
		return relation;
	}
	
	public static DwParentFeatureChildGroupContainer createNewParentFeatureChildGroupContainer() {
		DwParentFeatureChildGroupContainer relation = FEATURE_FACTORY.createDwParentFeatureChildGroupContainer();
		relation.setValidity(TEMPORAL_FACTORY.createDwTemporalInterval());
		
		return relation;
	}
	
	
	
	// Groups
	
	public static DwGroup createNewGroup() {
		return createNewGroup(DEFAULT_GROUP_TYPE, DwDateResolverUtil.INITIAL_STATE_DATE);
	}
	
	public static DwGroup createNewGroup(Date creationDate) {
		return createNewGroup(DEFAULT_GROUP_TYPE, creationDate);
	}
	
	public static DwGroup createNewGroup(DwGroupTypeEnum variationType) {
		return createNewGroup(variationType, DwDateResolverUtil.INITIAL_STATE_DATE);
	}

	public static DwGroup createNewGroup(DwGroupTypeEnum variationType, Date creationDate) {
		DwGroup group = FEATURE_FACTORY.createDwGroup();
		DwEvolutionUtil.addNewTemporalValidity(group, creationDate, DwDateResolverUtil.ETERNITY_DATE);
		
		group.getTypes().add(createNewGroupType(variationType, creationDate));
		
		return group;
	}
	
	
	
	// Group Types

	public static DwGroupType createNewGroupType(DwGroupTypeEnum type, Date creationDate) {
		DwGroupType typeContainer = createNewGroupType(type);
		DwEvolutionUtil.setTemporalValidity(typeContainer, creationDate, DwDateResolverUtil.ETERNITY_DATE);
		
		return typeContainer;
	}
	
	public static DwGroupType createNewGroupType(Date creationDate) {
		DwGroupType typeContainer = createNewGroupType();
		DwEvolutionUtil.setTemporalValidity(typeContainer, creationDate, DwDateResolverUtil.ETERNITY_DATE);
		
		return typeContainer;
	}
	
	public static DwGroupType createNewGroupType(DwGroupTypeEnum type) {
		DwGroupType typeContainer = createNewGroupType();
		typeContainer.setType(type);
		
		return typeContainer;
	}
	
	public static DwGroupType createNewGroupType() {
		DwGroupType typeContainer = FEATURE_FACTORY.createDwGroupType();
		typeContainer.setValidity(TEMPORAL_FACTORY.createDwTemporalInterval());
		
		return typeContainer;
	}
	
	
	
	// Relations that Groups hold to their child features
	
	public static DwGroupComposition createNewGroupComposition(DwGroup parentGroup, Date creationDate) {
		DwGroupComposition relation = createNewGroupComposition(creationDate);
		relation.setCompositionOf(parentGroup);
		
		return relation;
	}
	
	public static DwGroupComposition createNewGroupComposition(Date creationDate) {
		DwGroupComposition relation = createNewGroupComposition();
		DwEvolutionUtil.setTemporalValidity(relation, creationDate, DwDateResolverUtil.ETERNITY_DATE);
		
		return relation;
	}
	
	public static DwGroupComposition createNewGroupComposition(DwGroup parentGroup) {
		DwGroupComposition relation = createNewGroupComposition();
		relation.setCompositionOf(parentGroup);
		
		return relation;
	}
	
	public static DwGroupComposition createNewGroupComposition() {
		DwGroupComposition relation = FEATURE_FACTORY.createDwGroupComposition();
		relation.setValidity(TEMPORAL_FACTORY.createDwTemporalInterval());
		
		return relation;
	}
	
	
	
	// Tests
	// @Rahel: Shouldn't this be somewhere within the test project? And also, most of the functionality beneath was already implemented above, why not reuse it?
	
	public static DwTemporalFeatureModel createFeatureModelWithChild(Date creationDate) {
//		DwTemporalFeatureModel model = createNewFeatureModel(createNewFeature("Test Model"));		
//		DwFeature rootFeature = DwFeatureUtil.getRootFeature(model, creationDate);
//		model = addFeatureChildUnderFeatureModel(model, creationDate, rootFeature);
//		
//		return model;
		throw new UnsupportedOperationException();
	}
	
	public static DwTemporalFeatureModel addFeatureChildUnderFeatureModel(DwTemporalFeatureModel model, Date creationDate, DwFeature parentFeature) {
//		DwFeature newFeature = createNewFeatureWithDefaultName(creationDate);
//		model.getFeatures().add(newFeature);
//		
//		DwGroup newGroup = createNewGroup(DwGroupTypeEnum.AND, creationDate);
//		model.getGroups().add(newGroup);
//		
//		DwParentFeatureChildGroupContainer featureToGroupRelation = createNewParentFeatureChildGroupContainer(parentFeature, creationDate);
//		featureToGroupRelation.setChildGroup(newGroup);
//		
//		DwGroupComposition groupToFeatureRelation = createNewGroupComposition(newGroup, creationDate);
//		groupToFeatureRelation.getChildFeatures().add(newFeature);
//		
//		return model;
		throw new UnsupportedOperationException();
	}

}
