package de.darwinspl.feature.util.custom;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.darwinspl.common.eclipse.util.DwLogger;
import de.darwinspl.datatypes.DwEnum;
import de.darwinspl.datatypes.DwEnumValue;
import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureAttribute;
import de.darwinspl.feature.DwFeatureType;
import de.darwinspl.feature.DwFeatureTypeEnum;
import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupComposition;
import de.darwinspl.feature.DwGroupType;
import de.darwinspl.feature.DwGroupTypeEnum;
import de.darwinspl.feature.DwName;
import de.darwinspl.feature.DwNamedElement;
import de.darwinspl.feature.DwParentFeatureChildGroupContainer;
import de.darwinspl.feature.DwRootFeatureContainer;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.temporal.DwTemporalElement;
import de.darwinspl.temporal.util.DwEvolutionUtil;

public class DwFeatureUtil {

	private static final String FEATURE_MODEL_FILE_EXTENSION_FOR_XMI = "tfm";
	private static final String CONSTRAINT_MODEL_FILE_EXTENSION_FOR_XMI = FEATURE_MODEL_FILE_EXTENSION_FOR_XMI + "_ctc";
	private static final String TEMPORAL_CONSTRAINTS_MODEL_FILE_EXTENSION_FOR_XMI = "fmec";

	public static String getFeatureModelFileExtensionForXmi() {
		return FEATURE_MODEL_FILE_EXTENSION_FOR_XMI;
	}

	public static String getConstraintModelFileExtensionForXmi() {
		return CONSTRAINT_MODEL_FILE_EXTENSION_FOR_XMI;
	}

	public static String getTemporalConstraintModelFileExtensionForXmi() {
		return TEMPORAL_CONSTRAINTS_MODEL_FILE_EXTENSION_FOR_XMI;
	}

	// TODO make suitable for evolution
	public static DwFeatureVersion getInitialVersion(DwFeature feature) {
		// TODO: Make sure that it is the initial version not just the one declared
		// first in the model
		List<DwFeatureVersion> versions = feature.getVersions();

		if (!versions.isEmpty()) {
			return versions.get(0);
		}

		return null;
	}

	public static List<DwFeature> findFeaturesOnSameTreeLevel(DwFeature referenceFeature, Date date) {
		int referenceTreeLevel = getTreeLevelOfFeature(referenceFeature, date);

		DwTemporalFeatureModel featureModel = (DwTemporalFeatureModel) EcoreUtil.getRootContainer(referenceFeature);
		List<DwFeature> featuresOnSameTreeLevel = new LinkedList<DwFeature>();

		Iterator<EObject> iterator = featureModel.eAllContents();

		while (iterator.hasNext()) {
			EObject element = iterator.next();

			if (element instanceof DwFeature) {
				DwFeature feature = (DwFeature) element;

				if (feature != referenceFeature) {
					int featureTreeLevel = getTreeLevelOfFeature(feature, date);

					if (referenceTreeLevel == featureTreeLevel) {
						featuresOnSameTreeLevel.add(feature);
					}
				}
			}
		}

		return featuresOnSameTreeLevel;
	}

	private static int getTreeLevelOfFeature(DwFeature feature, Date date) {
		DwGroup parentGroup = DwFeatureUtil.getParentGroupOfFeature(feature, date);

		if (parentGroup == null) {
			// Root feature is considered level 0.
			return 0;
		}

		DwFeature parentFeature = DwFeatureUtil.getParentFeatureOfGroup(parentGroup, date);
		return getTreeLevelOfFeature(parentFeature, date) + 1;
	}

	public static int getNumberOfMandatoryFeatures(List<DwFeature> features, Date date) {
		int numberOfMandatoryFeatures = 0;

		for (DwFeature feature : features) {
			if (feature.isMandatory(date)) {
				numberOfMandatoryFeatures++;
			}
		}

		return numberOfMandatoryFeatures;
	}

//	public static List<DwFeature> getFeaturesOfGroup(DwGroup group, DwFeatureTypeEnum featureType, Date date) {
//		List<DwFeature> childFeatures = DwFeatureUtil.getChildsOfGroup(group, date);
//		return filterFeatures(childFeatures, featureType, date);
//	}

	public static List<DwFeature> filterFeatures(List<DwFeature> features, DwFeatureTypeEnum featureType, Date date) {
		List<DwFeature> filteredFeatures = new LinkedList<DwFeature>();

		if (featureType == null) {
			filteredFeatures.addAll(features);
		} else {
			for (DwFeature feature : features) {
				DwFeatureType featureTypeOfFeature = DwFeatureUtil.getType(feature, date);
				if (featureTypeOfFeature != null) {
					if (featureTypeOfFeature.getType().equals(featureType)) {
						filteredFeatures.add(feature);
					}
				}
			}
		}

		return filteredFeatures;
	}

	/**
	 * 
	 * @param features
	 * @param date
	 * @return First list is mandatory list, second list is optional list
	 */
	public static List<List<DwFeature>> splitFeaturesIntoMandatoryAndOptional(List<DwFeature> features, Date date) {
		List<DwFeature> mandatoryFeatures = new LinkedList<DwFeature>();
		List<DwFeature> optionalFeatures = new LinkedList<DwFeature>();
		List<List<DwFeature>> splittedFeatures = new LinkedList<List<DwFeature>>();
		splittedFeatures.add(mandatoryFeatures);
		splittedFeatures.add(optionalFeatures);

		for (DwFeature feature : features) {
			if (DwFeatureUtil.isMandatory(feature, date)) {
				mandatoryFeatures.add(feature);
			} else {
				optionalFeatures.add(feature);
			}
		}

		return splittedFeatures;
	}

	/**
	 * Only for FMs valid at one point in time!
	 * 
	 * @param group
	 * @param cardinality
	 * @param groupChildren
	 * @return
	 */
	public static boolean isAnd(DwGroup group, Date date) {
		if (group == null) {
			DwLogger.logError("Something bad happened. Group was null during isAnd check");
			return false;
		}

		DwGroupType groupType = DwEvolutionUtil.getValidTemporalElement(group.getTypes(), date);

		if(groupType == null || groupType.getType() == null) return true;
		return (groupType.getType().equals(DwGroupTypeEnum.AND));
	}

	/**
	 * Only for FMs valid at one point in time!
	 * 
	 * @param group
	 * @param date
	 * @return
	 * @throws DwTemporalFeatureModelWellFormednessException
	 */
	public static boolean isOr(DwGroup group, Date date) {
		if (group == null) {
			DwLogger.logError("Something bad happened. Group was null during isOr check");
			return false;
		}

		DwGroupType groupType = DwEvolutionUtil.getValidTemporalElement(group.getTypes(), date);

		if(groupType == null || groupType.getType() == null) return false;
		return (groupType.getType().equals(DwGroupTypeEnum.OR));
	}

	public static boolean isAlternative(DwGroup group, Date date) {
		if (group == null) {
			DwLogger.logError("Something bad happened. Group was null during isAlternative check");
			return false;
		}

		DwGroupType groupType = DwEvolutionUtil.getValidTemporalElement(group.getTypes(), date);

		if(groupType == null || groupType.getType() == null) return false;
		return (groupType.getType().equals(DwGroupTypeEnum.ALTERNATIVE));
	}

	/**
	 * Only for FMs valid at one point in time!
	 * 
	 * @param feature
	 * @return
	 * @throws DwTemporalFeatureModelWellFormednessException
	 */
	public static boolean isOptional(DwFeature feature, Date date) {
		if (feature == null) {
			DwLogger.logError("Something bad happened. Feature was null during isOptional check");
			return false;
		}

		DwFeatureType featureType = DwEvolutionUtil.getValidTemporalElement(feature.getTypes(), date);
		
		//TODO what is feature has no type? catch
		if (featureType != null && featureType.getType() != null){
			return (featureType.getType().equals(DwFeatureTypeEnum.OPTIONAL));
		} else {
			DwLogger.logError("Something bad happened. The feature " + feature.toString() + " did not contain any type object or the valid type object did not contain a type.");
			return false;
		}
		
	}

	public static boolean isRootFeature(DwFeature feature, Date date) {
		if (feature == null) {
			DwLogger.logError("Something bad happened. Feature was null during isRoot check");
			return false;
		}
		if(feature.getFeatureModel() == null) {
			DwLogger.logError("Something bad happened. Feature was not contained inside a TFM during isRoot check");
			return false;
		}

		DwRootFeatureContainer rootFeature = DwEvolutionUtil.getValidTemporalElement(feature.getFeatureModel().getRootFeatures(), date);
		if (rootFeature == null) {
			DwLogger.logError("Something bad happened. Root Feature was null during isRoot check");
			return false;
		}

		if (rootFeature.getFeature() == feature) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param group
	 * @param featureType null collects all features
	 * @return
	 */
	public static int getAmountOfFeaturesOfGroup(DwGroup group, DwFeatureTypeEnum featureType, Date date) {
		DwGroupComposition validGroupCompotisition = DwEvolutionUtil.getValidTemporalElement(group.getParentOf(), date);

		if (featureType == null) {
			return validGroupCompotisition.getChildFeatures().size();
		}

		int features = 0;

		for (DwFeature feature : validGroupCompotisition.getChildFeatures()) {
			DwFeatureType validFeatureType = DwEvolutionUtil.getValidTemporalElement(feature.getTypes(), date);
			if (validFeatureType.getType().equals(featureType)) {
				features++;
			}
		}

		return features;
	}

	/**
	 * This does not take constraints into account.
	 */
//	public static int calculateNumberOfPossibleConfigurations(DEFeatureModel featureModel) {
//		DEFeature rootFeature = featureModel.getRootFeature();
//		
//		return doCalculateNumberOfPossibleConfigurations(rootFeature);
//	}
//	
//	private static int doCalculateNumberOfPossibleConfigurations(DECardinalityBasedElement cardinalityBasedElement) {
//		boolean includeVersions = true;
//		
//		if (cardinalityBasedElement instanceof DEGroup) {
//			DEGroup group = (DEGroup) cardinalityBasedElement;
//			
//			int minCardinality = group.getMaxCardinality();
//			int maxCardinality = group.getMaxCardinality();
//			
//			List<DEFeature> features = group.getFeatures();
//			int numberOfFeatures = features.size();
//			int numberOfMandatoryFeatures = getNumberOfMandatoryFeatures(features);
//			
//			int effectiveMinCardinality = Math.min(minCardinality, numberOfMandatoryFeatures);
//			int effectiveMaxCardinality = Math.min(maxCardinality, numberOfFeatures);
//			
//			if (effectiveMinCardinality > effectiveMaxCardinality) {
//				return 0;
//			}
//			
//			if (effectiveMinCardinality == effectiveMaxCardinality) {
//				return effectiveMinCardinality;
//			}
//			
//			
//			int k = numberOfFeatures - numberOfMandatoryFeatures;
//			int numberOfPossibilities = 0;
//			
//			for (int n = effectiveMinCardinality; n <= effectiveMaxCardinality; n++) {
//				int subBranchPossibilities = 1;
//				//TODO: The concrete combination has to consider how many options the sub branch has
//				numberOfPossibilities += binomialCoefficient(n, k);
//			}
//			
//			return numberOfPossibilities;
//		}
//		
//		
//		if (cardinalityBasedElement instanceof DEFeature) {
//			DEFeature feature = (DEFeature) cardinalityBasedElement;
//			
//			int numberOfPossibilities = 1;
//			
//			if (includeVersions) {
//				List<DEVersion> versions = feature.getVersions();
//				
//				if (!versions.isEmpty()) {
//					//Add one less than all versions as selecting the plain
//					//feature (without any version) is no longer possible.
//					numberOfPossibilities += versions.size() - 1;
//				}
//			}
//			
//			List<DEGroup> groups = feature.getGroups();
//			
//			for (DEGroup group : groups) {
//				numberOfPossibilities *= doCalculateNumberOfPossibleConfigurations(group);
//			}
//
//			//Has to be last as deselecting the feature does not
//			//entail any selection of child groups/features.
//			if (feature.isOptional()) {
//				numberOfPossibilities += 1;
//			}
//			
//			return numberOfPossibilities;
//		}
//		
//		throw new UnsupportedOperationException();
//	}
//	
//	private static long binomialCoefficient(int n, int k) {
//		long coefficient = 1;
//		
//		for (int i = n - k + 1; i <= n; i++) {
//			coefficient *= i;
//		}
//		
//		for (int i = 1; i <= k; i++) {
//			coefficient /= i;
//		}
//		
//		return coefficient;
//	}

//	public static void printConfiguration(List<? extends DEConfigurationArtifact> configuration) {
//		DwLogger.logWarning(formatConfiguration(configuration));
//	}
//	
//	public static String formatConfiguration(List<? extends DEConfigurationArtifact> configuration) {
//		String output = "";
//		boolean isFirst = true;
//		
//		for (DEConfigurationArtifact configurationArtifact : configuration) {
//			if (!isFirst) {
//				output += ", ";
//			}
//			
//			if (configurationArtifact instanceof DEFeature) {
//				DEFeature feature = (DEFeature) configurationArtifact;
//				
//				output += feature.getName();
//			}
//			
//			if (configurationArtifact instanceof DEVersion) {
//				
//				DEVersion version = (DEVersion) configurationArtifact;
//				DEFeature feature = version.getFeature();
//				
//				output += feature.getName() + " @ " + version.getNumber();
//			}
//			
//			isFirst = false;
//		}
//		
//		return output;
//	}

//	//DEBUG
//	public static void checkConfigurationValidity(List<? extends DEConfigurationArtifact> configuration) {
//		//All features have to have exactly one version.
//		Map<DEFeature, DEVersion> featureToVersionMap =  new HashMap<DEFeature, DEVersion>();
//		List<DEFeature> features = new LinkedList<DEFeature>();
//		
//		for (DEConfigurationArtifact artifact : configuration) {
//			if (artifact instanceof DEFeature) {
//				DEFeature feature = (DEFeature) artifact;
//				features.add(feature);
//			}
//			
//			if (artifact instanceof DEVersion) {
//				DEVersion version = (DEVersion) artifact;
//				DEFeature feature = version.getFeature();
//				
//				if (featureToVersionMap.containsKey(feature)) {
//					DEVersion existingVersion = featureToVersionMap.get(feature);
//					DwLogger.logError("Configuration " + formatConfiguration(configuration) + " is invalid!");
//					DwLogger.logError("Configuration already contains a version (" + existingVersion.getNumber() + " for feature " + feature.getName() + " besides version " + version.getNumber());
//				} else {
//					featureToVersionMap.put(feature, version);
//				}
//			}
//		}
//		
//		for (DEFeature feature : features) {
//			if (!featureToVersionMap.containsKey(feature)) {
//				DwLogger.logError("Configuration " + formatConfiguration(configuration) + " is invalid!");
//				DwLogger.logError("Configuration does not contain a version for " + feature.getName());
//			}
//		}
//	}
	
	
	public static boolean isRootFeature(DwTemporalFeatureModel featureModel, DwFeature feature, Date date) {
		if(featureModel == null) {
			DwLogger.logWarning("DwFeatureUtil:isRootFeature() was given an empty feature model...");
			return false;
		}
		
		DwRootFeatureContainer rootFeature = DwEvolutionUtil.getValidTemporalElement(featureModel.getRootFeatures(), date);
		if (rootFeature.getFeature() == feature) {
			return true;
		}
		return false;
	}

	public static List<DwGroup> getChildGroupsOfFeature(DwFeature feature, Date date) {
		List<DwGroup> validGroups = new LinkedList<DwGroup>();

		List<DwParentFeatureChildGroupContainer> validFeatureChilds = DwEvolutionUtil
				.getValidTemporalElements(feature.getParentOf(), date);
		for (DwParentFeatureChildGroupContainer featureChild : validFeatureChilds) {
			DwGroup childGroup = featureChild.getChildGroup();
			if (DwEvolutionUtil.isValid(childGroup, date)) {
				validGroups.add(featureChild.getChildGroup());
			}
		}

		return validGroups;
	}

	public static List<DwFeature> getChildFeaturesOfFeature(DwFeature feature, Date date) {
		if (feature == null) {
			return null;
		}

		List<DwFeature> childFeatures = new LinkedList<DwFeature>();

		for (DwGroup childGroup : getChildGroupsOfFeature(feature, date)) {
			childFeatures.addAll(getChildFeaturesOfGroup(childGroup, date));
		}

		return childFeatures;
	}

//	public static List<DwFeature> getChildsOfGroup(DwGroup group, Date date) {
//		// TODO Assumption: Feature validity >= GroupComposition validity
//		return getGroupComposition(group, date).getFeatures();
//	}

	public static DwName getName(DwNamedElement namedElement, Date date) {
		return (DwName) DwEvolutionUtil.getValidTemporalElement(namedElement.getNames(), date);
	}

	@SuppressWarnings("unchecked")
	public static List<DwFeatureVersion> getFeatureVersions(DwFeature feature, Date date) {
		return (List<DwFeatureVersion>) (List<?>) DwEvolutionUtil.getValidTemporalElements(feature.getVersions(), date);
	}

	public static DwFeature getRootFeature(DwTemporalFeatureModel featureModel, Date date) {
		if(featureModel == null)
			// Editor is loading
			return null;
		
		DwRootFeatureContainer rootFeature = DwEvolutionUtil.getValidTemporalElement(featureModel.getRootFeatures(), date);
		if(rootFeature == null)
			return null;
		else
			return rootFeature.getFeature();
	}

	public static DwGroupComposition getGroupComposition(DwGroup group, Date date) {
		// TODO assumption: only one valid GroupComposition at a time
		return (DwGroupComposition) DwEvolutionUtil.getValidTemporalElement(group.getParentOf(), date);
	}

	public static DwGroupComposition getGroupMembership(DwFeature feature, Date date) {
		// TODO assumption: only one valid GroupComposition at a time
		return (DwGroupComposition) DwEvolutionUtil.getValidTemporalElement(feature.getGroupMembership(), date);
	}

	public static DwParentFeatureChildGroupContainer getParentFeature(DwGroup group, Date date) {
		// TODO assumption: only one valid FeatureChild of a group at a time
		return (DwParentFeatureChildGroupContainer) DwEvolutionUtil.getValidTemporalElement(group.getChildOf(), date);
	}

	@SuppressWarnings("unchecked")
	public static List<DwEnum> getEnums(DwTemporalFeatureModel featureModel, Date date) {
		return (List<DwEnum>) (List<?>) DwEvolutionUtil.getValidTemporalElements(featureModel.getEnums(), date);
	}

	@SuppressWarnings({ "unchecked" })
	public static List<DwFeatureAttribute> getAttributes(DwFeature feature, Date date) {
		return (List<DwFeatureAttribute>) (List<?>) DwEvolutionUtil.getValidTemporalElements(feature.getAttributes(),
				date);
	}

	@SuppressWarnings("unchecked")
	public static List<DwEnumValue> getEnumLiterals(DwEnum DwEnum, Date date) {
		return (List<DwEnumValue>) (List<?>) DwEvolutionUtil.getValidTemporalElements(DwEnum.getLiterals(), date);
	}

	// public static boolean isDeprecated(DwFeature feature, Date date) {
	// if (feature == null) {
	// DwLogger.logError(
	// "Something bad happened. Could not determine deprecation status of
	// feature as feature given to isDeprecated(...) was null");
	// return false;
	// }
	//
	// if (feature.getDeprecatedSince() != null &&
	// (feature.getDeprecatedSince().compareTo(date) <= 0)) {
	// return true;
	// }
	//
	// return false;
	// }

	// public static HyLinearTemporalElement
	// getMostRecentLinearTemporalElement(EList<?> elements) {
	// if(elements == null || elements.isEmpty()) {
	// DwLogger.logError("Something bad happened. get most recent element of a
	// list of versions. EList<?> given to
	// getMostRecentLinearTemporalElement(...) was null or empty");
	// return null;
	// }
	//
	// for(Object o: elements) {
	// if(o instanceof HyLinearTemporalElement) {
	// HyLinearTemporalElement element = (HyLinearTemporalElement) o;
	// if (element.getSupersedingElement() == null) {
	// return element;
	// }
	// }
	// }
	//
	// return null;
	// }

	public static DwName getMostRecentFeatureName(DwFeature feature) {
		if (feature == null) {
			DwLogger.logError(
					"Something bad happened. Could not get most recent feature name as DEFeature given to getMostRecentFeatureName(...) was null");
			return null;
		}
		DwName recentName = null;
		if (feature.getNames().size() == 1) {
			return feature.getNames().get(0);
		}

		for (DwName name : feature.getNames()) {
			if (recentName == null) {
				recentName = name;
			} else {
				if (name.getValidity().getFrom() != null) {
					if (recentName.getValidity().getFrom() == null) {
						recentName = name;
					} else {
						if (recentName.getValidity().getFrom().compareTo(name.getValidity().getFrom()) < 0) {
							recentName = name;
						}
					}
				}
			}
		}

		return recentName;
	}

	public static List<DwFeature> getChildFeaturesOfGroup(DwGroup group, Date date) {
		if (group == null) {
			return new LinkedList<DwFeature>();
		}

		DwGroupComposition validGroupComposition = DwEvolutionUtil.getValidTemporalElement(group.getParentOf(), date);

		if (validGroupComposition == null) {
			return new LinkedList<DwFeature>();
		}

		return validGroupComposition.getChildFeatures();
	}

//	public static boolean isAlternative(DwGroup group, Date date) {
//		DwGroupType type = DwEvolutionUtil.getValidTemporalElement(group.getTypes(), date);
//		if (type.getType() == DwGroupTypeEnum.ALTERNATIVE) {
//			return true;
//		} else {
//			return false;
//		}
//	}
//
//	public static boolean isOr(DwGroup group, Date date) {
//		DwGroupType type = DwEvolutionUtil.getValidTemporalElement(group.getTypes(), date);
//		if (type.getType() == DwGroupTypeEnum.OR) {
//			return true;
//		} else {
//			return false;
//		}
//	}
////
////	public static boolean isAnd(DwGroup group, Date date) {
////		DwGroupType type = DwEvolutionUtil.getValidTemporalElement(group.getTypes(), date);
////		if (type.getType() == DwGroupTypeEnum.AND) {
////			return true;
////		} else {
////			return false;
////		}
////	}

	public static DwGroup getParentGroupOfFeature(DwFeature feature, Date date) {
		if (feature == null) {
			return null;
		}

		DwTemporalElement validGroupComposition = DwEvolutionUtil.getValidTemporalElement(feature.getGroupMembership(), date);

		if (validGroupComposition == null) {
			if (DwFeatureUtil.isRootFeature(feature, date)) {
//				DwLogger.logWarning("Tried to get parent group of root feature");
			} else {
				DwLogger.logError("Something bad happened. Feature " + feature
						+ " is not member of any group or member of multiple groups at date " + date);
			}
			return null;

		}

		if (validGroupComposition instanceof DwGroupComposition) {
			return ((DwGroupComposition) validGroupComposition).getCompositionOf();
		} else {
			DwLogger.logError("Something bad happened. DwGroupComposition expected but not received at date " + date
					+ " in method getParentGroupOfFeature");
			return null;
		}
	}

	public static DwFeature getParentFeatureOfFeature(DwFeature feature, Date date) {
		return getParentFeatureOfGroup(getParentGroupOfFeature(feature, date), date);
	}

	public static DwFeature getParentFeatureOfGroup(DwGroup group, Date date) {
		if (group == null) {
			return null;
		}

		DwTemporalElement validFeatureChild = DwEvolutionUtil.getValidTemporalElement(group.getChildOf(), date);

		if (validFeatureChild == null) {
			DwLogger.logError("Something bad happened. Group " + group + " has no or multiple parent at date " + date);
			return null;
		}

		if (validFeatureChild instanceof DwParentFeatureChildGroupContainer) {
			return ((DwParentFeatureChildGroupContainer) validFeatureChild).getParent();
		} else {
			DwLogger.logError(
					"Something bad happened. DwParentFeatureChildGroupContainer expected but not received at date "
							+ date + " in method getParentOfGroup");
			return null;
		}
	}

	public static DwFeatureType getType(DwFeature feature, Date date) {
		return DwEvolutionUtil.getValidTemporalElement(feature.getTypes(), date);
	}

	public static DwGroupType getType(DwGroup group, Date date) {
		return DwEvolutionUtil.getValidTemporalElement(group.getTypes(), date);
	}

	public static List<DwFeature> getFeatures(DwTemporalFeatureModel featureModel, Date date) {
		return DwEvolutionUtil.getValidTemporalElements(featureModel.getFeatures(), date);
	}

	public static List<DwFeature> getFeatures(DwTemporalFeatureModel featureModel, Date date, DwFeatureTypeEnum type) {
		List<DwFeature> validFeatures = getFeatures(featureModel, date);

		if (validFeatures == null || type == null) {
			return validFeatures;
		}

		List<DwFeature> featuresWithCorrectType = new LinkedList<DwFeature>();

		for (DwFeature feature : validFeatures) {
			DwFeatureType featureType = getType(feature, date);
			if (featureType != null) {
				if (featureType.getType().equals(type)) {
					featuresWithCorrectType.add(feature);
				}
			}
		}

		return featuresWithCorrectType;
	}

	public static List<DwGroup> getGroups(DwTemporalFeatureModel featureModel, Date date) {
		return DwEvolutionUtil.getValidTemporalElements(featureModel.getGroups(), date);
	}

	public static boolean isMandatory(DwFeature feature, Date date) {
//		DwFeatureType type = DwEvolutionUtil.getValidTemporalElement(feature.getTypes(), date);
//		if (type.getType() == DwFeatureTypeEnum.MANDATORY) {
//			return true;
//		} else {
//			return false;
//		}
		
		return !isOptional(feature, date);
	}

//	public static boolean isOptional(DwFeature feature, Date date) {
//		DwFeatureType type = DwEvolutionUtil.getValidTemporalElement(feature.getTypes(), date);
//		if (type.getType() == DwFeatureTypeEnum.OPTIONAL) {
//			return true;
//		} else {
//			return false;
//		}
//	}

	public static boolean isLeaf(DwFeature feature, Date date) {
		List<DwParentFeatureChildGroupContainer> children = feature.getParentOf();

		if (children == null || DwEvolutionUtil.getValidTemporalElements(children, date).isEmpty()) {
			return true;
		}
		return false;
	}

	public static boolean isInGroup(DwFeature feature, DwGroup group, Date date) {
		return getChildFeaturesOfGroup(group, date).contains(feature);
	}

	/**
	 * Get all features in the subtree under @feature
	 * 
	 * @param feature
	 * @param date
	 * @return
	 */
	public static List<DwFeature> getFeaturesOfSubtree(DwFeature feature, Date date, int depth) {
		depth++;
		List<DwFeature> features = new LinkedList<DwFeature>();

		for (DwGroup group : getChildGroupsOfFeature(feature, date)) {
			List<DwFeature> subFeatures = getChildFeaturesOfGroup(group, date);
			features.addAll(subFeatures);

			for (DwFeature subFeature : subFeatures) {
				features.addAll(getFeaturesOfSubtree(subFeature, date, depth));
			}
		}

		return features;
	}

	public static List<DwFeature> getFeaturesOfSubtree(DwGroup group, Date date, int depth) {
		depth++;
		List<DwFeature> features = new LinkedList<DwFeature>();

		for (DwFeature groupFeature : getChildFeaturesOfGroup(group, date)) {
			features.add(groupFeature);

			features.addAll(getFeaturesOfSubtree(groupFeature, date, depth));
		}

		return features;
	}

	public static DwGroup getRandomGroup(DwTemporalFeatureModel featureModel, Date date) {
		List<DwGroup> validGroups = DwFeatureUtil.getGroups(featureModel, date);
		if (validGroups.size() < 1) {
			return null;
		}
		Random rand = new Random();
		int groupIndex = rand.nextInt(validGroups.size());
		return validGroups.get(groupIndex);
	}

	public static DwGroup getRandomGroupWithType(DwTemporalFeatureModel featureModel, DwGroupTypeEnum allowedGroupType,
			Date date) {
		List<DwGroupTypeEnum> groupTypes = new LinkedList<DwGroupTypeEnum>();
		groupTypes.add(allowedGroupType);

		return getRandomGroupWithType(featureModel, groupTypes, date);
	}

	public static DwGroup getRandomGroupWithType(DwTemporalFeatureModel featureModel,
			List<DwGroupTypeEnum> allowedGroupTypes, Date date) {
		List<DwGroup> potentialGroups = new LinkedList<DwGroup>();

		outerFor: for (DwGroup group : DwFeatureUtil.getGroups(featureModel, date)) {
			DwGroupTypeEnum groupType = DwFeatureUtil.getType(group, date).getType();

			for (DwGroupTypeEnum allowedType : allowedGroupTypes) {
				if (groupType.equals(allowedType)) {
					potentialGroups.add(group);
					continue outerFor;
				}
			}
		}

		Random rand = new Random();
		int groupIndex = rand.nextInt(potentialGroups.size());
		return potentialGroups.get(groupIndex);
	}

	public static DwGroup getRandomGroup(DwTemporalFeatureModel featureModel, List<DwGroup> excludedGroups, Date date) {
		List<DwGroup> validGroups = DwFeatureUtil.getGroups(featureModel, date);
		validGroups.removeAll(excludedGroups);

		if (validGroups.size() < 1) {
			return null;
		}
		Random rand = new Random();
		int groupIndex = rand.nextInt(validGroups.size());
		return validGroups.get(groupIndex);
	}

	public static DwGroup getRandomGroup(DwTemporalFeatureModel featureModel, DwGroup excludedGroup, Date date) {
		List<DwGroup> validGroups = DwFeatureUtil.getGroups(featureModel, date);
		validGroups.remove(excludedGroup);

		if (validGroups.size() < 1) {
			return null;
		}
		Random rand = new Random();
		int groupIndex = rand.nextInt(validGroups.size());
		return validGroups.get(groupIndex);
	}

	public static DwFeature getRandomFeature(DwTemporalFeatureModel featureModel, Date date) {
		List<DwFeature> validFeatures = DwFeatureUtil.getFeatures(featureModel, date);
		if (validFeatures.size() < 1) {
			return null;
		}
		Random rand = new Random();
		int index = rand.nextInt(validFeatures.size());
		return validFeatures.get(index);
	}

	public static DwFeature getRandomFeature(DwTemporalFeatureModel featureModel, List<DwFeature> excludedFeatures,
			Date date) {
		List<DwFeature> validFeatures = DwFeatureUtil.getFeatures(featureModel, date);
		validFeatures.removeAll(excludedFeatures);
		if (validFeatures.size() < 1) {
			return null;
		}
		Random rand = new Random();
		int index = rand.nextInt(validFeatures.size());
		if (validFeatures.get(index) == null) {
			DwLogger.logWarning("WTDF?");
		}
		return validFeatures.get(index);
	}

	public static DwFeature getRandomFeature(DwTemporalFeatureModel featureModel, DwFeature excludedFeature,
			Date date) {
		List<DwFeature> validFeatures = DwFeatureUtil.getFeatures(featureModel, date);
		validFeatures.remove(excludedFeature);
		if (validFeatures.size() < 1) {
			return null;
		}
		Random rand = new Random();
		int index = rand.nextInt(validFeatures.size());
		if (validFeatures.get(index) == null) {
			DwLogger.logWarning("???");
		}
		return validFeatures.get(index);
	}

	public static DwFeature getRandomNonRootFeature(DwTemporalFeatureModel featureModel, Date date) {
		return getRandomFeature(featureModel, DwFeatureUtil.getRootFeature(featureModel, date), date);
	}

	public static List<DwFeatureVersion> getVersionsOfFeature(DwFeature feature, Date date) {
		List<DwFeatureVersion> versions = new LinkedList<DwFeatureVersion>();

		versions.addAll(DwEvolutionUtil.getValidTemporalElements(feature.getVersions(), date));

		return versions;
	}

	public static DwName getLastValidName(DwNamedElement namedElement) {

		Map<Date, DwName> nameDateMap = new HashMap<Date, DwName>();

		for (DwName DwName : namedElement.getNames()) {
			if (DwName.getValidity().getTo() == null) {
				return DwName;
			} else {
				nameDateMap.put(DwName.getValidity().getTo(), DwName);
			}
		}

		List<Date> dateList = new LinkedList<Date>(nameDateMap.keySet());
		Collections.sort(dateList);
		Date lastDate = dateList.get(dateList.size() - 1);
		return nameDateMap.get(lastDate);
	}
	
	public static List<DwName> getAllFeatureNames(DwTemporalFeatureModel tfm) {
		List<DwName> result = new ArrayList<>();
		for(DwFeature feature : tfm.getFeatures()) {
			result.addAll(feature.getNames());
		}
		
		return result;
	}
	
	public static List<DwName> getAllFeatureNames(DwTemporalFeatureModel tfm, Date date) {
		List<DwName> allNames = getAllFeatureNames(tfm);
		
		List<DwName> result = new ArrayList<>();
		for(DwName name : allNames) {
			try {
				if(DwEvolutionUtil.isValid((DwTemporalElement) name.eContainer(), date)) {
					if(DwEvolutionUtil.isValid(name, date)) {
						result.add(name);
					}
				}
			} catch(ClassCastException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}

	public static boolean hasVersions(DwFeature feature, Date date) {
		if (feature == null) {
			return false;
		}

		List<DwFeatureVersion> versions = DwEvolutionUtil.getValidTemporalElements(feature.getVersions(), date);

		return !versions.isEmpty();
	}

//	public static List<DwFeature> findFeaturesOnSameTreeLevel(DwFeature referenceFeature, Date date) {
//		if(referenceFeature == null) {
//			return null;
//		}
//		
//		if(!DwEvolutionUtil.isValid(referenceFeature, date)) {
//			return null;
//		}
//		
//		int referenceTreeLevel = getTreeLevelOfFeature(referenceFeature, date);
//		List<DwFeature> featuresOnSameTreeLevel = new LinkedList<DwFeature>();
//		
//		EObject rootContainer = EcoreUtil.getRootContainer(referenceFeature);
//		
//		//NOTE: If a feature is detached, the determined root container is the feature itself.
//		if (rootContainer instanceof DwTemporalFeatureModel) {
//			DwTemporalFeatureModel featureModel = (DwTemporalFeatureModel) rootContainer;
//			
//			Iterator<EObject> iterator = featureModel.eAllContents();
//			
//			while(iterator.hasNext()) {
//				EObject element = iterator.next();
//				
//				if (element instanceof DwFeature) {
//					DwFeature feature = (DwFeature) element;
//					
//					if (feature != referenceFeature) {
//						int featureTreeLevel = getTreeLevelOfFeature(feature, date);
//						
//						if (referenceTreeLevel == featureTreeLevel) {
//							featuresOnSameTreeLevel.add(feature);
//						}
//					}
//				}
//			}
//		}
//		
//		return featuresOnSameTreeLevel;
//	}

//	private static int getTreeLevelOfFeature(DwFeature feature, Date date) {
//		if (feature == null) {
//			return -1;
//		}
//		if(!DwEvolutionUtil.isValid(feature, date)) {
//			return -1;
//		}
//		
//		DwGroup parentGroup = DwFeatureUtil.getParentGroupOfFeature(feature, date);
//		
//		if (parentGroup == null) {
//			//Root feature is considered level 0.
//			return 0;
//		}
//		
//		
//		DwFeature parentFeature = DwFeatureUtil.getParentFeatureOfGroup(parentGroup, date);
//		return getTreeLevelOfFeature(parentFeature, date) + 1;
//	}
	
	
	
	public static Set<DwTemporalElement> collectAllChildFeaturesAndGroupsAcrossTime(DwTemporalElement rootElement) {
		Set<DwTemporalElement> result = new HashSet<>();
		collectAllChildFeaturesAndGroupsAcrossTime(result, rootElement);
		return result;
	}
	
	public static void collectAllChildFeaturesAndGroupsAcrossTime(
			Set<DwTemporalElement> result,
			DwTemporalElement rootElement) {
		
		result.add(rootElement);
		
		if(rootElement instanceof DwGroup) {
			DwGroup group = (DwGroup) rootElement;
			for(DwGroupComposition relationToChildFeatures : group.getParentOf()) {
				for(DwFeature childFeature : relationToChildFeatures.getChildFeatures()) {
					collectAllChildFeaturesAndGroupsAcrossTime(result, childFeature);
				}
			}
		}
		else if(rootElement instanceof DwFeature) {
			DwFeature feature = (DwFeature) rootElement;
			for(DwParentFeatureChildGroupContainer relationToChildGroup: feature.getParentOf()) {
				collectAllChildFeaturesAndGroupsAcrossTime(result, relationToChildGroup.getChildGroup());
			}
		}
	}
	




	public static void collectFeaturesAndGroupsOfSubtree(Set<DwTemporalElement> result, DwFeature feature, Date date) {
		result.add(feature);
		for(DwGroup group : DwFeatureUtil.getChildGroupsOfFeature(feature, date)) {
			collectFeaturesAndGroupsOfSubtree(result, group, date);
		}
	}

	public static void collectFeaturesAndGroupsOfSubtree(Set<DwTemporalElement> result, DwGroup group, Date date) {
		result.add(group);
		for(DwFeature feature : DwFeatureUtil.getChildFeaturesOfGroup(group, date)) {
			collectFeaturesAndGroupsOfSubtree(result, feature, date);
		}
	}

	public static void collectFeaturesAndGroupsOfSupertree(Set<DwTemporalElement> superElements, DwTemporalElement element, Date date) {
		DwTemporalElement parent;
		
		if(element instanceof DwFeature) {
			parent = DwFeatureUtil.getParentGroupOfFeature(((DwFeature) element), date);
		}
		else if(element instanceof DwGroup) {
			parent = DwFeatureUtil.getParentFeatureOfGroup(((DwGroup) element), date);
		}
		else return;
		
		if(parent != null) {
			superElements.add(parent);
			collectFeaturesAndGroupsOfSupertree(superElements, parent, date);
		}
	}



	public static DwFeature findFeatureById(DwTemporalFeatureModel tfm, String id) {
		for(DwFeature feature : tfm.getFeatures()) {
			if(feature.getId().equals(id))
				return feature;
		}
		
		return null;
	}
	
	public static DwGroup findGroupById(DwTemporalFeatureModel tfm, String id) {
		for(DwGroup group : tfm.getGroups()) {
			if(group.getId().equals(id))
				return group;
		}
		
		return null;
	}
	
	public static EObject findElementById(DwTemporalFeatureModel tfm, String id) {
		DwFeature feature = findFeatureById(tfm, id);
		if(feature != null)
			return feature;
		
		DwGroup group = findGroupById(tfm, id);
		return group;
	}
}
