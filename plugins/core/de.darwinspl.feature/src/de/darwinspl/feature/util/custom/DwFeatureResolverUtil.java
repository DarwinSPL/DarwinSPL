package de.darwinspl.feature.util.custom;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import de.darwinspl.common.ecore.util.DwEcoreUtil;
import de.darwinspl.datatypes.DwEnum;
import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureAttribute;
import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.DwName;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.temporal.util.DwEvolutionUtil;

public class DwFeatureResolverUtil {
//	public static final String[] FILE_EXTENSIONS = { "DwFeature", "DwTemporalFeatureModel" };

	// TODO incorporate evolution

	public static DwTemporalFeatureModel getAccompanyingFeatureModel(EObject elementInOriginalResource) {
		
		EObject model = DwEcoreUtil.loadAccompanyingModelInSameProject(elementInOriginalResource, DwFeatureUtil.getFeatureModelFileExtensionForXmi());
		
		if(model != null && model instanceof DwTemporalFeatureModel) {
			return (DwTemporalFeatureModel)model;
		}
		
		return null;
		
		
//		DwTemporalFeatureModel featureModel = DEIOUtil.doLoadAccompanyingModel(elementInOriginalResource, new String[] {DwFeatureUtil.getFeatureModelFileExtensionForXmi()});
//		
//		if(featureModel == null) {
//			// search for first feature model in the same folder and then all the way up to the project
//			
//			 IFile fileOfOriginalResource = EcoreResolverUtil.resolveRelativeFileFromEObject(elementInOriginalResource);
//			 if(fileOfOriginalResource != null) {
//				 IContainer container = fileOfOriginalResource.getParent();
//				 
//				 while(container != null) {
//					 IFile featureModelFile = ResourceUtil.findFirstFileWithExtension(container, DwFeatureUtil.getFeatureModelFileExtensionForXmi());
//					 
//					 featureModel = EcoreIOUtil.loadModel(featureModelFile, elementInOriginalResource.eResource().getResourceSet());
//					 
//					 if(featureModel != null) {
//						 break;
//					 }
//					 
//					 // Repeat only if container is not a project
//					 if(container instanceof IProject) {
//						 break;
//					 }
//					 
//					 container = container.getParent();						 
//				 }
//			 }
//		}
//		
//		return featureModel;
	}

	public static DwFeature resolveFeature(String identifier, DwTemporalFeatureModel featureModel, Date date)
			throws DwTemporalFeatureModelWellFormednessException {
		
		if(identifier == null || featureModel == null) {
			return null;
		}
		
		List<DwFeature> validFeatures = new ArrayList<DwFeature>();
		
		// try if name is unique over time
		for (DwFeature feature : featureModel.getFeatures()) {
			for (DwName name : feature.getNames()) {
				String nameString = name.getName();
				if (nameString != null && nameString.equals(identifier)) {
					validFeatures.add(feature);
				}
			}
		}

		if(validFeatures.size() > 1) {
			// name is not unique
			List<DwFeature> validFeaturesWithEvolution = new ArrayList<DwFeature>(1);
			for(DwFeature feature: validFeatures) {
				
				if(DwEvolutionUtil.isValid(feature, date)) {
					DwName name = DwFeatureUtil.getName(feature, date);
					if((name != null) && name.getName().equals(identifier)) {
						validFeaturesWithEvolution.add(feature);
					}					
				}
			}
			validFeatures = validFeaturesWithEvolution;
		}

		if (validFeatures.size() > 1) {
			// More than one feature with that name at date date
			throw new DwTemporalFeatureModelWellFormednessException();
		} else if (validFeatures.size() == 1) {
			return validFeatures.get(0);
		}

		// Iterator<EObject> iterator = featureModel.eAllContents();
		//
		// while(iterator.hasNext()) {
		// EObject element = iterator.next();
		//
		// if (element instanceof DwFeature) {
		// DwFeature feature = (DwFeature) element;
		// String name =
		// DwFeatureUtil.getMostRecentFeatureName(feature).getName();
		//
		// if (identifier.equals(name)) {
		// return feature;
		// }
		// }
		// }

		
		
		return null;
	}

	public static DwEnum resolveEnum(String identifier, DwTemporalFeatureModel featureModel, Date date)  {
		if (identifier == null || featureModel == null) {
			return null;
		}

		List<DwEnum> matchingEnums = new ArrayList<DwEnum>();
		// try if name is unique
		for (DwEnum DwEnum : featureModel.getEnums()) {
			if (DwEnum.getName().equals(identifier)) {
				matchingEnums.add(DwEnum);
			}
		}

		
		if(matchingEnums.size() > 1) {
			// name is not unique
			List<DwEnum> validEnumsWithEvolution = new ArrayList<DwEnum>(1);
			for(DwEnum DwEnum: matchingEnums) {
				if(DwEvolutionUtil.isValid(DwEnum, date)) {
					validEnumsWithEvolution.add(DwEnum);
				}				
			}
			matchingEnums = validEnumsWithEvolution;
		}

		if (matchingEnums.size() > 1) {
			// TODO error: more than one valid element
		} else if (matchingEnums.size() == 1) {
			return matchingEnums.get(0);
		}

		return null;
	}

	public static String deresolveFeature(DwFeature feature, Date date) {
		// TODO incorporate evolution! For every element!
		DwName featureName = DwFeatureUtil.getName(feature, date);
		if(featureName == null) {
			System.out.println("Bad!");
		}
		String identifier = featureName.getName();

		// Standard TEXT token regular expression
		String textTokenRegularExpression = "[A-Za-z_][A-Za-z0-9_]*";

		if (!identifier.matches(textTokenRegularExpression)) {
//			identifier = "\"" + identifier + "\"";
		}

		return identifier;
	}

	public static DwFeatureVersion resolveVersion(String identifier, DwFeature feature, Date date) throws DwTemporalFeatureModelWellFormednessException {
		if (identifier == null) {
			return null;
		}

		List<DwFeatureVersion> validVersions = DwFeatureUtil.getVersionsOfFeature(feature, date);
		
		List<DwFeatureVersion> resolvedVersions = new ArrayList<DwFeatureVersion>(1);

		for (DwFeatureVersion version : validVersions) {
			String number = version.getNumber();

			if (identifier.equals(number)) {
				resolvedVersions.add(version);
			}
		}

		if(resolvedVersions.size()>1) {
			throw new DwTemporalFeatureModelWellFormednessException("Identifier "+identifier+" is ambiguous and lead to more than one resolved version.");
		}
		
		if(resolvedVersions.isEmpty()) {
			return null;
		} 
		else {
			return resolvedVersions.get(0);
		}
	}

	public static String deresolveVersion(DwFeatureVersion version, Date date) {
		return version.getNumber();
	}
	
	public static DwFeatureAttribute resolveFeatureAttribute(String identifier, DwFeature containingFeature, Date date) throws DwTemporalFeatureModelWellFormednessException {		
		List<DwFeatureAttribute> validAttributes = new ArrayList<DwFeatureAttribute>();
		// try if name is unique
		for (DwFeatureAttribute attribute : containingFeature.getAttributes()) {
			for(DwName name: attribute.getNames()) {
				String nameString = name.getName();
				if(nameString.equals(identifier)) {
					validAttributes.add(attribute);					
				}
			}
		}
		
		if(validAttributes.size() > 1) {
			// name is not unique -> incorporate evolution
			List<DwFeatureAttribute> validAttributesWithEvolution = new ArrayList<DwFeatureAttribute>(1);
			for(DwFeatureAttribute attribute: validAttributes) {
				if(DwEvolutionUtil.isValid(containingFeature, date) && DwEvolutionUtil.isValid(attribute, date)) {
					DwName name = DwFeatureUtil.getName(attribute, date);
					if((name != null) && name.getName().equals(identifier)) {
						validAttributesWithEvolution.add(attribute);
					}
				}
				
			}
			validAttributes = validAttributesWithEvolution;
		}

		if (validAttributes.size() > 1) {
			// More than one attribute with that name at date date
			throw new DwTemporalFeatureModelWellFormednessException();
		} else if (validAttributes.size() == 1) {
			return validAttributes.get(0);
		}

		// String featureString;
		// String attributeString;
		//
		// String[] splits = identifier.split(".");
		// if(splits.length > 2) {
		// System.out.println("Warning: in the feature attribute referenced by
		// "+identifier+", there are some dots in the name of the feature or the
		// attribute. Please fix this");
		// return null;
		// } else if(splits.length < 2) {
		// System.out.println("Warning: feature attributes are referenced as
		// follows: featureName.attributeName");
		// return null;
		// }
		//
		// featureString = splits[0];
		// attributeString = splits[1];
		//
		// List<DwFeature> validFeatures = new ArrayList<DwFeature>();
		// DwFeature resolvedFeature;
		// for(DwFeature feature: featureModel.getFeatures()) {
		// String name = DwFeatureUtil.getValidName(feature.getNames(),
		// date).getName();
		// if(name.equals(featureString)) {
		// validFeatures.add(feature);
		// }
		// }
		//
		// if(validFeatures.size() > 1) {
		// // More than one feature with that name at date date
		// throw new DwTemporalFeatureModelWellFormednessException();
		// }
		// else if(validFeatures.size() == 1) {
		// resolvedFeature = validFeatures.get(0);
		//
		//
		// for(DwFeatureAttribute attribute: resolvedFeature.getAttributes()) {
		// String name =
		// DwFeatureUtil.getValidName(attribute.getNames(),
		// date).getName();
		// if(name.equals(attributeString)) {
		// validAttributes.add(attribute);
		// }
		// }
		//
		// if(validAttributes.size() > 1) {
		// // More than one attribute with that name at date date
		// throw new DwTemporalFeatureModelWellFormednessException();
		// }
		// else if(validAttributes.size() == 1) {
		// return validAttributes.get(0);
		// }
		// }

		
		return null;
	}

	public static String deresolveFeatureAttribute(DwFeatureAttribute attribute, Date date) {
		String name = "";
		DwFeature feature = attribute.getFeature();
		name += DwFeatureUtil.getName(feature, date).getName();
		name += ".";
		name += DwFeatureUtil.getName(attribute, date).getName();
		return name;
	}
}
