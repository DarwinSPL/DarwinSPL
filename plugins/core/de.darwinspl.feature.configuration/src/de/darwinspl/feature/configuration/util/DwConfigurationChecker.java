package de.darwinspl.feature.configuration.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;

import de.darwinspl.datatypes.DwBooleanValue;
import de.darwinspl.datatypes.DwEnumValue;
import de.darwinspl.datatypes.DwNumberValue;
import de.darwinspl.datatypes.DwStringValue;
import de.darwinspl.datatypes.DwValue;
import de.darwinspl.feature.DwBooleanAttribute;
import de.darwinspl.feature.DwEnumAttribute;
import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureAttribute;
import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupComposition;
import de.darwinspl.feature.DwNumberAttribute;
import de.darwinspl.feature.DwParentFeatureChildGroupContainer;
import de.darwinspl.feature.DwStringAttribute;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.configuration.DwAttributeValueConfiguration;
import de.darwinspl.feature.configuration.DwConfiguration;
import de.darwinspl.feature.util.custom.DwFeatureUtil;
import de.darwinspl.temporal.util.DwEvolutionUtil;

public class DwConfigurationChecker {

	private Map<EObject, String> errorMessages;

	public DwConfigurationChecker() {
		this.errorMessages = new HashMap<EObject, String>();
	}

	/**
	 * For one point in time
	 * IMPORTANT! CONSTRAINTS ARE NOT CHECKED AS CONTEXTS IN THEM CURRENTLY BREAK EVERYTHING #ContextCTC
	 * 
	 * @param configuration
	 * @return
	 */
	public boolean isValid(DwConfiguration configuration, Date date) {
		// TODO only valid features and versions. EMFValidation?-> Student?
		boolean isValid = true;

		List<DwFeature> selectedFeatures = DwConfigurationUtil.getSelectedFeatures(configuration, date);
		List<DwFeatureVersion> selectedVersions = DwConfigurationUtil.getSelectedVersions(configuration, date);

		// Check if root feature is selected
		DwTemporalFeatureModel featureModel = configuration.getFeatureModel();
		
		DwFeature rootFeature = DwFeatureUtil.getRootFeature(featureModel, date);
		
		if (!selectedFeatures.contains(rootFeature)) {
			isValid = false;
			addErrorMessage(rootFeature, "RootFeature for Date " + date + " must be selected");
		}

		for (DwFeature selectedFeature : selectedFeatures) {

			// check if parent of feature is selected
			if (!checkParentSelected(selectedFeature, date, featureModel, selectedFeatures)) {
				isValid = false;
			}

			// check if at least one feature of each alternative and or group is
			// selected
			if (!checkSubGroupsSelection(selectedFeature, date, selectedFeatures)) {
				isValid = false;
			}
		}

		List<DwFeature> selectedViaVersion = new ArrayList<DwFeature>();

		// Check if feature is selected in two different versions
		for (DwFeatureVersion version : selectedVersions) {
			if (selectedViaVersion.contains(version.getFeature())) {
				isValid = false;
				addErrorMessage(version.getFeature(), "Feature selected with multiple versions");
			} else {
				selectedViaVersion.add(version.getFeature());
			}
		}

		return isValid;
		
//		#ContextCTC
//		
//		// check cross-tree constraints
//		HyConstraintModel constraintModel = HyConstraintIOUtil.loadAccompanyingConstraintModel(featureModel);
//		if (constraintModel != null) {
//			HyConstraintEvaluator constraintEvaluator = new HyConstraintEvaluator();
//			boolean allConstraintsSatisfied = constraintEvaluator.evaluate(constraintModel, selectedFeatures,
//					selectedVersions, date);
//
//			if (!allConstraintsSatisfied) {
//
//				HyConstraint violatedConstraint = constraintEvaluator.getViolatedConstraint();
//				String formattedConstraint = HyConstraintFormatter.formatConstraint(violatedConstraint);
//				addErrorMessage(violatedConstraint, "Constraint \"" + formattedConstraint + "\" is violated.");
//
//				isValid = false;
//			}
//		}
//		
//
//		
//		// check attribute domains and right type of values
//		List<DwAttributeValueConfiguration> attributeValueAssignments = DwConfigurationUtil.getAttributeValueAssignments(validConfigurationElements);
//		for(DwAttributeValueConfiguration attributeValueAssignment: attributeValueAssignments) {
//			if(!checkAttributeValueAssignment(attributeValueAssignment, date)) {
//				isValid = false;
//			}
//		}
//
//		return isValid;
	}

	private boolean checkAttributeValueAssignment(DwAttributeValueConfiguration attributeValueAssignment, Date date) {
		// TODO type match -> EMF Validation
		DwFeatureAttribute attribute = attributeValueAssignment.getAttribute();
		DwValue value = attributeValueAssignment.getValue();
		
		if(attribute instanceof DwNumberAttribute) {
			if(value instanceof DwNumberValue) {
				DwNumberAttribute numberAttribute = (DwNumberAttribute) attribute;
				DwNumberValue numberValue = (DwNumberValue) value;
				if(numberValue.getValue()<numberAttribute.getMin() || numberValue.getValue()>numberAttribute.getMax()) {
					addAttributeDomainViolationError(attribute, date);
					return false;
				}
			}
			else {
				addValueTypeMismatchError(attribute, date);
				return false;
			}
		} 
		else if(attribute instanceof DwEnumAttribute) {
			if(value instanceof DwEnumValue) {
				
				DwEnumAttribute enumAttribute = (DwEnumAttribute) attribute;
				DwEnumValue enumValue = (DwEnumValue) value;
				
				if(enumValue.getValue().getEnum() != enumAttribute.getEnum()) {
					addAttributeDomainViolationError(attribute, date);
					return false;
				}
			}
			else {
				addValueTypeMismatchError(attribute, date);
				return false;
			}
		}
		else if(attribute instanceof DwBooleanAttribute) {
			if(! (value instanceof DwBooleanValue)) {
				addValueTypeMismatchError(attribute, date);
				return false;
			}
		}
		else if(attribute instanceof DwStringAttribute) {
			if(! (value instanceof DwStringValue)) {
				addValueTypeMismatchError(attribute, date);
				return false;
			}
		}
		
		return true;
	}
	
	private void addAttributeDomainViolationError(DwFeatureAttribute attribute, Date date) {
		addErrorMessage(attribute, "Value assigned to attribute "+DwEvolutionUtil.getValidTemporalElement(attribute.getNames(), date) + " does not match the attribute domain.");
	}
	
	private void addValueTypeMismatchError(DwFeatureAttribute attribute, Date date) {
		addErrorMessage(attribute, "Value assigned to attribute "+DwEvolutionUtil.getValidTemporalElement(attribute.getNames(), date) + " has the wrong type.");
	}
	
	private boolean checkParentSelected(DwFeature feature, Date date, DwTemporalFeatureModel featureModel,
			List<DwFeature> selectedFeatures) {

		if (!DwFeatureUtil.isRootFeature(featureModel, feature, date)) {
			DwFeature parentFeature = DwFeatureUtil.getParentFeatureOfFeature(feature, date);
			if (!selectedFeatures.contains(parentFeature)) {
				checkParentSelected(parentFeature, date, featureModel, selectedFeatures);
				addErrorMessage(parentFeature, "Feature has to be selected as feature in subtree is selected");
				return false;
			}
		}

		return true;
	}

	private boolean checkSubGroupsSelection(DwFeature feature, Date date, List<DwFeature> selectedFeatures) {
		boolean isValid = true;

		for (DwParentFeatureChildGroupContainer validFeatureChildren : DwEvolutionUtil.getValidTemporalElements(feature.getParentOf(),
				date)) {
			DwGroup childGroup = validFeatureChildren.getChildGroup();
			DwGroupComposition groupComposition = DwEvolutionUtil.getValidTemporalElement(childGroup.getParentOf(),
					date);
			if (childGroup.isAlternative(date) || childGroup.isOr(date)) {
				int selectedChildren = 0;
				for (DwFeature childFeature : groupComposition.getChildFeatures()) {
					if (selectedFeatures.contains(childFeature)) {
						selectedChildren++;
					}
				}

				if (selectedChildren == 0) {
					isValid = false;
					addErrorMessage(feature,
							"At least one Feature of each or and alternative subgroup has to be selected");
				} else if (selectedChildren > 1 && childGroup.isAlternative(date)) {
					isValid = false;
					addErrorMessage(feature, "Each alternative subgroup may only have exactly one feautre selected");
				}
			}
			// Check for selection of mandatory children
			else if (childGroup.isAnd(date)) {
				for (DwFeature childFeature : groupComposition.getChildFeatures()) {
					if (DwFeatureUtil.isMandatory(childFeature, date)) {
						if (!selectedFeatures.contains(childFeature)) {
							addErrorMessage(childFeature,
									"Mandatory features have to be selected if parent is selected");
							isValid = false;
						}
					}
				}
			}
		}

		return isValid;
	}

	private void addErrorMessage(EObject object, String message) {
		if (errorMessages.containsKey(object)) {
			String existingMessage = errorMessages.get(object);
			existingMessage += System.lineSeparator();
			existingMessage += message;
			errorMessages.put(object, existingMessage);
		} else {
			errorMessages.put(object, message);
		}
	}

	public Map<EObject, String> getErrorMessages() {
		return errorMessages;
	}
	

}
