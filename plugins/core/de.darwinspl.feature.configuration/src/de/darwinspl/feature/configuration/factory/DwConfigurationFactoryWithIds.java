package de.darwinspl.feature.configuration.factory;

import de.darwinspl.feature.configuration.DwAttributeValueConfiguration;
import de.darwinspl.feature.configuration.DwConfiguration;
import de.darwinspl.feature.configuration.DwFeatureDeselection;
import de.darwinspl.feature.configuration.DwFeatureSelection;
import de.darwinspl.feature.configuration.DwFeatureVersionDeselection;
import de.darwinspl.feature.configuration.DwFeatureVersionSelection;
import de.darwinspl.feature.configuration.impl.DwConfigurationFactoryImpl;

public class DwConfigurationFactoryWithIds extends DwConfigurationFactoryImpl {
	@Override
	public DwAttributeValueConfiguration createDwAttributeValueConfiguration() {
		DwAttributeValueConfiguration DwAttributeValueConfiguration = super.createDwAttributeValueConfiguration();
		DwAttributeValueConfiguration.createId();
		return DwAttributeValueConfiguration;
	}
	
	@Override
	public DwConfiguration createDwConfiguration() {
		DwConfiguration DwConfiguration = super.createDwConfiguration();
		DwConfiguration.createId();
		return DwConfiguration;
	}
	
	@Override
	public DwFeatureDeselection createDwFeatureDeselection() {
		DwFeatureDeselection DwFeatureDeselection = super.createDwFeatureDeselection();
		DwFeatureDeselection.createId();
		return DwFeatureDeselection;
	}
	
	@Override
	public DwFeatureSelection createDwFeatureSelection() {
		DwFeatureSelection DwFeatureSelection = super.createDwFeatureSelection();
		DwFeatureSelection.createId();
		return DwFeatureSelection;
	}
	
	@Override
	public DwFeatureVersionDeselection createDwFeatureVersionDeselection() {
		DwFeatureVersionDeselection DwFeatureVersionDeselection = super.createDwFeatureVersionDeselection();
		DwFeatureVersionDeselection.createId();
		return DwFeatureVersionDeselection;
	}
	
	@Override
	public DwFeatureVersionSelection createDwFeatureVersionSelection() {
		DwFeatureVersionSelection DwFeatureVersionSelection = super.createDwFeatureVersionSelection();
		DwFeatureVersionSelection.createId();
		return DwFeatureVersionSelection;
	}
}
