package de.darwinspl.feature.configuration.util;

public class DwConfigurationWellFormednessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -543379905356821898L;

	
	public DwConfigurationWellFormednessException(String message) {
		super(message);
	}
}
