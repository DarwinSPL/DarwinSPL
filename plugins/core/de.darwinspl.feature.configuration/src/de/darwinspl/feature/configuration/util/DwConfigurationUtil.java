package de.darwinspl.feature.configuration.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.emf.ecore.util.EcoreUtil;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureVersion;
import de.darwinspl.feature.configuration.DwConfiguration;
import de.darwinspl.feature.configuration.DwConfigurationFactory;
import de.darwinspl.feature.configuration.DwFeatureConfiguration;
import de.darwinspl.feature.configuration.DwFeatureDeselection;
import de.darwinspl.feature.configuration.DwFeatureSelection;
import de.darwinspl.feature.configuration.DwFeatureVersionConfiguration;
import de.darwinspl.feature.configuration.DwFeatureVersionSelection;
import de.darwinspl.temporal.DwTemporalInterval;
import de.darwinspl.temporal.DwTemporalModelFactory;
import de.darwinspl.temporal.util.DwEvolutionUtil;

public class DwConfigurationUtil {

	
	private static final String CONFIGURATION_MODEL_FILE_EXTENSION_FOR_XMI = "dwconfiguration";
//	private static final String CONFIGURATION_MODEL_FILE_EXTENSION_FOR_CONCRETE_SYNTAX = "Dwconfiguration";
	
	
	public static List<DwFeature> getSelectedFeatures(DwConfiguration configuration, Date date) {
		List<DwFeatureConfiguration> validConfigurationElements = DwEvolutionUtil.getValidTemporalElements(configuration.getFeatureConfigurations(), date);
		return getSelectedFeatures(validConfigurationElements);
	}
	
	public static List<DwFeature> getSelectedFeatures(List<DwFeatureConfiguration> validFeatureConfigurations) {
		List<DwFeature> selectionFeatures = new ArrayList<DwFeature>();
		
		for(DwFeatureConfiguration element: validFeatureConfigurations) {
			if(element instanceof DwFeatureSelection) {
				selectionFeatures.add(((DwFeatureSelection)element).getFeature());
			}
		}
		
		return selectionFeatures;
	}
	
	public static List<DwFeature> getSelectedFeatures(List<DwFeatureConfiguration> featureConfigurations, Date date){
		List<DwFeatureConfiguration> validConfigurationElements = DwEvolutionUtil.getValidTemporalElements(featureConfigurations, date);
		return getSelectedFeatures(validConfigurationElements);
	}
	
	public static List<DwFeatureVersion> getSelectedVersions(DwConfiguration configuration, Date date) {
		List<DwFeatureVersionConfiguration> validConfigurationElements = DwEvolutionUtil.getValidTemporalElements(configuration.getVersionConfigurations(), date);
		return getSelectedVersions(validConfigurationElements);
	}
	
	public static List<DwFeatureVersion> getSelectedVersions(List<DwFeatureVersionConfiguration> validVersionConfigurations) {
		List<DwFeatureVersion> selectionVersions = new ArrayList<DwFeatureVersion>();
		
		for(DwFeatureVersionConfiguration element: validVersionConfigurations) {
			if(element instanceof DwFeatureVersionSelection) {
				selectionVersions.add(((DwFeatureVersionSelection)element).getVersion());
			}
		}
		
		return selectionVersions;
	}
	
	public static List<DwFeatureVersion> getSelectedVersions(List<DwFeatureVersionConfiguration> versionConfigurations, Date date){
		List<DwFeatureVersionConfiguration> validConfigurationElements = DwEvolutionUtil.getValidTemporalElements(versionConfigurations, date);
		return getSelectedVersions(validConfigurationElements);
	}
	
	public static boolean doesConfigurationSelect(DwConfiguration configuration, DwFeature searchedFeature) {
		if(configuration==null || searchedFeature==null) {
			return false;
		}
		
		for(DwFeatureConfiguration configEle: configuration.getFeatureConfigurations()) {
			if(configEle instanceof DwFeatureSelection) {
				DwFeatureSelection featureSelection = (DwFeatureSelection) configEle;
				if(EcoreUtil.equals(featureSelection.getFeature(), searchedFeature)) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	public static boolean doesConfigurationDeselect(DwConfiguration configuration, DwFeature searchedFeature) {
		if(configuration==null || searchedFeature==null) {
			return false;
		}
		
		for(DwFeatureConfiguration configEle: configuration.getFeatureConfigurations()) {
			if(configEle instanceof DwFeatureDeselection) {
				DwFeatureDeselection featureSelection = (DwFeatureDeselection) configEle;
				if(EcoreUtil.equals(featureSelection.getFeature(), searchedFeature)) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	public static boolean doesConfigurationSelectVersion(DwConfiguration configuration, DwFeatureVersion DwFeatureVersion) {
		if(configuration==null || DwFeatureVersion==null) {
			return false;
		}
		
		for(DwFeatureVersionConfiguration configEle: configuration.getVersionConfigurations()) {
			if(configEle instanceof DwFeatureVersionSelection) {
				DwFeatureVersionSelection versionSelection = (DwFeatureVersionSelection) configEle;
				if(EcoreUtil.equals(versionSelection.getVersion(), DwFeatureVersion)) {
					return true;
				}
			}
		}
		
		return false;
	}

	public static String getConfigurationModelFileExtensionForXmi() {
		return CONFIGURATION_MODEL_FILE_EXTENSION_FOR_XMI;
	}

//	public static String getConfigurationModelFileExtensionForConcreteSyntax() {
//		return CONFIGURATION_MODEL_FILE_EXTENSION_FOR_CONCRETE_SYNTAX;
//	}
	
	

	/**
	 * Creates a DwFeatureDeselection element and adds it to the configuration.
	 * @param feature
	 * @param configuration
	 * @param date
	 * @return
	 */
	public static DwFeatureDeselection deselectFeature(DwFeature feature, DwConfiguration configuration, Date date) {
		DwFeatureDeselection featureDeselection = DwConfigurationFactory.eINSTANCE.createDwFeatureDeselection();
		
		addFeatureConfiguration(featureDeselection, feature, configuration, date);
		
		return featureDeselection;
	}
	
	/**
	 * Creates a DwFeatureSelection element and adds it to the configuration.
	 * @param feature
	 * @param configuration
	 * @param date
	 * @return
	 */
	public static DwFeatureSelection selectFeature(DwFeature feature, DwConfiguration configuration, Date date) {
		DwFeatureSelection featureSelection = DwConfigurationFactory.eINSTANCE.createDwFeatureSelection();
		
		addFeatureConfiguration(featureSelection, feature, configuration, date);
		
		return featureSelection;
	}
	
	public static void resetFeatureSelectionState(DwFeature feature, DwConfiguration configuration, Date date) {
		List<DwFeatureConfiguration> featureConfigurationsToRemove = new ArrayList<>();
		for(DwFeatureConfiguration featureConfiguration : DwEvolutionUtil.getValidTemporalElements(configuration.getFeatureConfigurations(), date)) {
			if (featureConfiguration.getFeature().equals(feature)) {
				featureConfigurationsToRemove.add(featureConfiguration);
			}
		}
		
		configuration.getFeatureConfigurations().removeAll(featureConfigurationsToRemove);
	}
	
	private static void addFeatureConfiguration(DwFeatureConfiguration featureConfiguration, DwFeature feature, DwConfiguration configuration, Date date) {
		// TODO sensible valid until?!
		DwEvolutionUtil.setTemporalValidity(featureConfiguration, date, null);
		featureConfiguration.setFeature(feature);
		configuration.getFeatureConfigurations().add(featureConfiguration);
	}
	
	public static DwConfiguration createNewConfiguration() {
		return DwConfigurationFactory.eINSTANCE.createDwConfiguration();
	}
	
	public static DwConfiguration createNewConfiguration(Date startOfValidity, Date endOfValidity) {
		DwConfiguration configuration = DwConfigurationFactory.eINSTANCE.createDwConfiguration();
		DwTemporalInterval interval = DwTemporalModelFactory.eINSTANCE.createDwTemporalInterval();
		DwEvolutionUtil.setValidity(interval, startOfValidity, endOfValidity);
		configuration.getValidities().add(interval);
		
		return configuration;
	}
	
}
