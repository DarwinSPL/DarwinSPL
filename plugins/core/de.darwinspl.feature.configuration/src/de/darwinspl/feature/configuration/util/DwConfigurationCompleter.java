package de.darwinspl.feature.configuration.util;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.configuration.DwConfiguration;
import de.darwinspl.feature.configuration.DwConfigurationFactory;
import de.darwinspl.feature.configuration.DwFeatureSelection;
import de.darwinspl.feature.configuration.DwFeatureVersionConfiguration;
import de.darwinspl.feature.configuration.DwFeatureVersionSelection;

public class DwConfigurationCompleter {

	
	/**
	 * Completes the configuration (versions selected, features not). No new configuration is created! Elements added!
	 * @param configuration
	 * @param date
	 * @return
	 * @throws DwConfigurationWellFormednessException
	 */
	public static DwConfiguration completeConfiguration(DwConfiguration configuration, Date date) throws DwConfigurationWellFormednessException {
		// TODO add default version for features?
		DwConfigurationChecker configurationChecker = new DwConfigurationChecker();
		
		if(configurationChecker.isValid(configuration, date)) {
			
			Set<DwFeature> selectedFeatures = new HashSet<DwFeature>();
			Set<DwFeature> requiredFeatures = new HashSet<DwFeature>();
			
			selectedFeatures.addAll(DwConfigurationUtil.getSelectedFeatures(configuration.getFeatureConfigurations(), date));
			
			for(DwFeatureVersionConfiguration versionConfig: configuration.getVersionConfigurations()) {
				if(versionConfig instanceof DwFeatureVersionSelection) {
					requiredFeatures.add(versionConfig.getVersion().getFeature());
				}
			}
			
			Set<DwFeature> missingFeatures = new HashSet<DwFeature>();
			missingFeatures.addAll(requiredFeatures);
			missingFeatures.removeAll(selectedFeatures);
			
//			DwConfiguration completedConfiguration = EcoreUtil.copy(configuration);
//			List<DwConfigurationElement> completedConfigurationArtifacts = completedConfiguration.getElements();
			
			for (DwFeature missingFeature : missingFeatures) {
				DwFeatureSelection featureSelection = DwConfigurationFactory.eINSTANCE.createDwFeatureSelection();
				featureSelection.setFeature(missingFeature);
				configuration.getFeatureConfigurations().add(featureSelection);
			}
			
			return configuration;
		} else {
			throw new DwConfigurationWellFormednessException("Configuration is not valid");
		}
		
		
		
	}
	
}
