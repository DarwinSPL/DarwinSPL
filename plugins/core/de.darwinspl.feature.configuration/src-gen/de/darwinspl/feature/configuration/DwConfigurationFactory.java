/**
 */
package de.darwinspl.feature.configuration;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.darwinspl.feature.configuration.DwConfigurationPackage
 * @generated
 */
public interface DwConfigurationFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DwConfigurationFactory eINSTANCE = de.darwinspl.feature.configuration.impl.DwConfigurationFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Dw Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Configuration</em>'.
	 * @generated
	 */
	DwConfiguration createDwConfiguration();

	/**
	 * Returns a new object of class '<em>Dw Feature Selection</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Feature Selection</em>'.
	 * @generated
	 */
	DwFeatureSelection createDwFeatureSelection();

	/**
	 * Returns a new object of class '<em>Dw Feature Deselection</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Feature Deselection</em>'.
	 * @generated
	 */
	DwFeatureDeselection createDwFeatureDeselection();

	/**
	 * Returns a new object of class '<em>Dw Attribute Value Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Attribute Value Configuration</em>'.
	 * @generated
	 */
	DwAttributeValueConfiguration createDwAttributeValueConfiguration();

	/**
	 * Returns a new object of class '<em>Dw Feature Version Selection</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Feature Version Selection</em>'.
	 * @generated
	 */
	DwFeatureVersionSelection createDwFeatureVersionSelection();

	/**
	 * Returns a new object of class '<em>Dw Feature Version Deselection</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Feature Version Deselection</em>'.
	 * @generated
	 */
	DwFeatureVersionDeselection createDwFeatureVersionDeselection();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	DwConfigurationPackage getDwConfigurationPackage();

} //DwConfigurationFactory
