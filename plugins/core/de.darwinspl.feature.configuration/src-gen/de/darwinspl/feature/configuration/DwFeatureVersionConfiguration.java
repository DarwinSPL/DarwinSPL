/**
 */
package de.darwinspl.feature.configuration;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureVersion;

import de.darwinspl.temporal.DwSingleTemporalIntervalElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Feature Version Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.configuration.DwFeatureVersionConfiguration#getVersion <em>Version</em>}</li>
 *   <li>{@link de.darwinspl.feature.configuration.DwFeatureVersionConfiguration#getFeature <em>Feature</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.configuration.DwConfigurationPackage#getDwFeatureVersionConfiguration()
 * @model abstract="true"
 * @generated
 */
public interface DwFeatureVersionConfiguration extends DwSingleTemporalIntervalElement {
	/**
	 * Returns the value of the '<em><b>Version</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' reference.
	 * @see #setVersion(DwFeatureVersion)
	 * @see de.darwinspl.feature.configuration.DwConfigurationPackage#getDwFeatureVersionConfiguration_Version()
	 * @model required="true"
	 * @generated
	 */
	DwFeatureVersion getVersion();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.configuration.DwFeatureVersionConfiguration#getVersion <em>Version</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' reference.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(DwFeatureVersion value);

	/**
	 * Returns the value of the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature</em>' reference.
	 * @see #setFeature(DwFeature)
	 * @see de.darwinspl.feature.configuration.DwConfigurationPackage#getDwFeatureVersionConfiguration_Feature()
	 * @model
	 * @generated
	 */
	DwFeature getFeature();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.configuration.DwFeatureVersionConfiguration#getFeature <em>Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature</em>' reference.
	 * @see #getFeature()
	 * @generated
	 */
	void setFeature(DwFeature value);

} // DwFeatureVersionConfiguration
