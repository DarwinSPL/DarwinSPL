/**
 */
package de.darwinspl.feature.configuration;

import de.darwinspl.feature.DwTemporalFeatureModel;

import de.darwinspl.temporal.DwMultiTemporalIntervalElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.configuration.DwConfiguration#getFeatureModel <em>Feature Model</em>}</li>
 *   <li>{@link de.darwinspl.feature.configuration.DwConfiguration#getFeatureConfigurations <em>Feature Configurations</em>}</li>
 *   <li>{@link de.darwinspl.feature.configuration.DwConfiguration#getVersionConfigurations <em>Version Configurations</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.configuration.DwConfigurationPackage#getDwConfiguration()
 * @model
 * @generated
 */
public interface DwConfiguration extends DwMultiTemporalIntervalElement {
	/**
	 * Returns the value of the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Model</em>' reference.
	 * @see #setFeatureModel(DwTemporalFeatureModel)
	 * @see de.darwinspl.feature.configuration.DwConfigurationPackage#getDwConfiguration_FeatureModel()
	 * @model required="true"
	 * @generated
	 */
	DwTemporalFeatureModel getFeatureModel();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.configuration.DwConfiguration#getFeatureModel <em>Feature Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature Model</em>' reference.
	 * @see #getFeatureModel()
	 * @generated
	 */
	void setFeatureModel(DwTemporalFeatureModel value);

	/**
	 * Returns the value of the '<em><b>Feature Configurations</b></em>' containment reference list.
	 * The list contents are of type {@link de.darwinspl.feature.configuration.DwFeatureConfiguration}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Configurations</em>' containment reference list.
	 * @see de.darwinspl.feature.configuration.DwConfigurationPackage#getDwConfiguration_FeatureConfigurations()
	 * @model containment="true"
	 * @generated
	 */
	EList<DwFeatureConfiguration> getFeatureConfigurations();

	/**
	 * Returns the value of the '<em><b>Version Configurations</b></em>' reference list.
	 * The list contents are of type {@link de.darwinspl.feature.configuration.DwFeatureVersionConfiguration}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version Configurations</em>' reference list.
	 * @see de.darwinspl.feature.configuration.DwConfigurationPackage#getDwConfiguration_VersionConfigurations()
	 * @model
	 * @generated
	 */
	EList<DwFeatureVersionConfiguration> getVersionConfigurations();

} // DwConfiguration
