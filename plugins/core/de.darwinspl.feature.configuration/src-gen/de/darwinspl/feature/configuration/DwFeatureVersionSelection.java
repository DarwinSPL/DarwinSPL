/**
 */
package de.darwinspl.feature.configuration;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Feature Version Selection</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.feature.configuration.DwConfigurationPackage#getDwFeatureVersionSelection()
 * @model
 * @generated
 */
public interface DwFeatureVersionSelection extends DwFeatureVersionConfiguration {
} // DwFeatureVersionSelection
