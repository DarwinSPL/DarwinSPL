/**
 */
package de.darwinspl.feature.configuration.util;

import de.darwinspl.feature.configuration.*;

import de.darwinspl.temporal.DwElementWithId;
import de.darwinspl.temporal.DwMultiTemporalIntervalElement;
import de.darwinspl.temporal.DwSingleTemporalIntervalElement;
import de.darwinspl.temporal.DwTemporalElement;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see de.darwinspl.feature.configuration.DwConfigurationPackage
 * @generated
 */
public class DwConfigurationAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DwConfigurationPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwConfigurationAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = DwConfigurationPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwConfigurationSwitch<Adapter> modelSwitch =
		new DwConfigurationSwitch<Adapter>() {
			@Override
			public Adapter caseDwConfiguration(DwConfiguration object) {
				return createDwConfigurationAdapter();
			}
			@Override
			public Adapter caseDwFeatureConfiguration(DwFeatureConfiguration object) {
				return createDwFeatureConfigurationAdapter();
			}
			@Override
			public Adapter caseDwFeatureSelection(DwFeatureSelection object) {
				return createDwFeatureSelectionAdapter();
			}
			@Override
			public Adapter caseDwFeatureDeselection(DwFeatureDeselection object) {
				return createDwFeatureDeselectionAdapter();
			}
			@Override
			public Adapter caseDwAttributeValueConfiguration(DwAttributeValueConfiguration object) {
				return createDwAttributeValueConfigurationAdapter();
			}
			@Override
			public Adapter caseDwFeatureVersionConfiguration(DwFeatureVersionConfiguration object) {
				return createDwFeatureVersionConfigurationAdapter();
			}
			@Override
			public Adapter caseDwFeatureVersionSelection(DwFeatureVersionSelection object) {
				return createDwFeatureVersionSelectionAdapter();
			}
			@Override
			public Adapter caseDwFeatureVersionDeselection(DwFeatureVersionDeselection object) {
				return createDwFeatureVersionDeselectionAdapter();
			}
			@Override
			public Adapter caseDwElementWithId(DwElementWithId object) {
				return createDwElementWithIdAdapter();
			}
			@Override
			public Adapter caseDwTemporalElement(DwTemporalElement object) {
				return createDwTemporalElementAdapter();
			}
			@Override
			public Adapter caseDwMultiTemporalIntervalElement(DwMultiTemporalIntervalElement object) {
				return createDwMultiTemporalIntervalElementAdapter();
			}
			@Override
			public Adapter caseDwSingleTemporalIntervalElement(DwSingleTemporalIntervalElement object) {
				return createDwSingleTemporalIntervalElementAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.configuration.DwConfiguration <em>Dw Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.configuration.DwConfiguration
	 * @generated
	 */
	public Adapter createDwConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.configuration.DwFeatureConfiguration <em>Dw Feature Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.configuration.DwFeatureConfiguration
	 * @generated
	 */
	public Adapter createDwFeatureConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.configuration.DwFeatureSelection <em>Dw Feature Selection</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.configuration.DwFeatureSelection
	 * @generated
	 */
	public Adapter createDwFeatureSelectionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.configuration.DwFeatureDeselection <em>Dw Feature Deselection</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.configuration.DwFeatureDeselection
	 * @generated
	 */
	public Adapter createDwFeatureDeselectionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.configuration.DwAttributeValueConfiguration <em>Dw Attribute Value Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.configuration.DwAttributeValueConfiguration
	 * @generated
	 */
	public Adapter createDwAttributeValueConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.configuration.DwFeatureVersionConfiguration <em>Dw Feature Version Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.configuration.DwFeatureVersionConfiguration
	 * @generated
	 */
	public Adapter createDwFeatureVersionConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.configuration.DwFeatureVersionSelection <em>Dw Feature Version Selection</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.configuration.DwFeatureVersionSelection
	 * @generated
	 */
	public Adapter createDwFeatureVersionSelectionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.feature.configuration.DwFeatureVersionDeselection <em>Dw Feature Version Deselection</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.feature.configuration.DwFeatureVersionDeselection
	 * @generated
	 */
	public Adapter createDwFeatureVersionDeselectionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.temporal.DwElementWithId <em>Dw Element With Id</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.temporal.DwElementWithId
	 * @generated
	 */
	public Adapter createDwElementWithIdAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.temporal.DwTemporalElement <em>Dw Temporal Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.temporal.DwTemporalElement
	 * @generated
	 */
	public Adapter createDwTemporalElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.temporal.DwMultiTemporalIntervalElement <em>Dw Multi Temporal Interval Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.temporal.DwMultiTemporalIntervalElement
	 * @generated
	 */
	public Adapter createDwMultiTemporalIntervalElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.temporal.DwSingleTemporalIntervalElement <em>Dw Single Temporal Interval Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.temporal.DwSingleTemporalIntervalElement
	 * @generated
	 */
	public Adapter createDwSingleTemporalIntervalElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //DwConfigurationAdapterFactory
