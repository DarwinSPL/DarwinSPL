/**
 */
package de.darwinspl.feature.configuration.impl;

import de.darwinspl.datatypes.DwDatatypesPackage;

import de.darwinspl.feature.DwFeaturePackage;

import de.darwinspl.feature.configuration.DwAttributeValueConfiguration;
import de.darwinspl.feature.configuration.DwConfiguration;
import de.darwinspl.feature.configuration.DwConfigurationFactory;
import de.darwinspl.feature.configuration.DwConfigurationPackage;
import de.darwinspl.feature.configuration.DwFeatureConfiguration;
import de.darwinspl.feature.configuration.DwFeatureDeselection;
import de.darwinspl.feature.configuration.DwFeatureSelection;
import de.darwinspl.feature.configuration.DwFeatureVersionConfiguration;
import de.darwinspl.feature.configuration.DwFeatureVersionDeselection;
import de.darwinspl.feature.configuration.DwFeatureVersionSelection;

import de.darwinspl.temporal.DwTemporalModelPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DwConfigurationPackageImpl extends EPackageImpl implements DwConfigurationPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureSelectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureDeselectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwAttributeValueConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureVersionConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureVersionSelectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureVersionDeselectionEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.darwinspl.feature.configuration.DwConfigurationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private DwConfigurationPackageImpl() {
		super(eNS_URI, DwConfigurationFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link DwConfigurationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static DwConfigurationPackage init() {
		if (isInited) return (DwConfigurationPackage)EPackage.Registry.INSTANCE.getEPackage(DwConfigurationPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredDwConfigurationPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		DwConfigurationPackageImpl theDwConfigurationPackage = registeredDwConfigurationPackage instanceof DwConfigurationPackageImpl ? (DwConfigurationPackageImpl)registeredDwConfigurationPackage : new DwConfigurationPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		DwDatatypesPackage.eINSTANCE.eClass();
		DwFeaturePackage.eINSTANCE.eClass();
		DwTemporalModelPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theDwConfigurationPackage.createPackageContents();

		// Initialize created meta-data
		theDwConfigurationPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theDwConfigurationPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(DwConfigurationPackage.eNS_URI, theDwConfigurationPackage);
		return theDwConfigurationPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwConfiguration() {
		return dwConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwConfiguration_FeatureModel() {
		return (EReference)dwConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwConfiguration_FeatureConfigurations() {
		return (EReference)dwConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwConfiguration_VersionConfigurations() {
		return (EReference)dwConfigurationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureConfiguration() {
		return dwFeatureConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureConfiguration_Feature() {
		return (EReference)dwFeatureConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureSelection() {
		return dwFeatureSelectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureDeselection() {
		return dwFeatureDeselectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwAttributeValueConfiguration() {
		return dwAttributeValueConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwAttributeValueConfiguration_Attribute() {
		return (EReference)dwAttributeValueConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwAttributeValueConfiguration_Value() {
		return (EReference)dwAttributeValueConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureVersionConfiguration() {
		return dwFeatureVersionConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureVersionConfiguration_Version() {
		return (EReference)dwFeatureVersionConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureVersionConfiguration_Feature() {
		return (EReference)dwFeatureVersionConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureVersionSelection() {
		return dwFeatureVersionSelectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureVersionDeselection() {
		return dwFeatureVersionDeselectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwConfigurationFactory getDwConfigurationFactory() {
		return (DwConfigurationFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		dwConfigurationEClass = createEClass(DW_CONFIGURATION);
		createEReference(dwConfigurationEClass, DW_CONFIGURATION__FEATURE_MODEL);
		createEReference(dwConfigurationEClass, DW_CONFIGURATION__FEATURE_CONFIGURATIONS);
		createEReference(dwConfigurationEClass, DW_CONFIGURATION__VERSION_CONFIGURATIONS);

		dwFeatureConfigurationEClass = createEClass(DW_FEATURE_CONFIGURATION);
		createEReference(dwFeatureConfigurationEClass, DW_FEATURE_CONFIGURATION__FEATURE);

		dwFeatureSelectionEClass = createEClass(DW_FEATURE_SELECTION);

		dwFeatureDeselectionEClass = createEClass(DW_FEATURE_DESELECTION);

		dwAttributeValueConfigurationEClass = createEClass(DW_ATTRIBUTE_VALUE_CONFIGURATION);
		createEReference(dwAttributeValueConfigurationEClass, DW_ATTRIBUTE_VALUE_CONFIGURATION__ATTRIBUTE);
		createEReference(dwAttributeValueConfigurationEClass, DW_ATTRIBUTE_VALUE_CONFIGURATION__VALUE);

		dwFeatureVersionConfigurationEClass = createEClass(DW_FEATURE_VERSION_CONFIGURATION);
		createEReference(dwFeatureVersionConfigurationEClass, DW_FEATURE_VERSION_CONFIGURATION__VERSION);
		createEReference(dwFeatureVersionConfigurationEClass, DW_FEATURE_VERSION_CONFIGURATION__FEATURE);

		dwFeatureVersionSelectionEClass = createEClass(DW_FEATURE_VERSION_SELECTION);

		dwFeatureVersionDeselectionEClass = createEClass(DW_FEATURE_VERSION_DESELECTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		DwTemporalModelPackage theDwTemporalModelPackage = (DwTemporalModelPackage)EPackage.Registry.INSTANCE.getEPackage(DwTemporalModelPackage.eNS_URI);
		DwFeaturePackage theDwFeaturePackage = (DwFeaturePackage)EPackage.Registry.INSTANCE.getEPackage(DwFeaturePackage.eNS_URI);
		DwDatatypesPackage theDwDatatypesPackage = (DwDatatypesPackage)EPackage.Registry.INSTANCE.getEPackage(DwDatatypesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		dwConfigurationEClass.getESuperTypes().add(theDwTemporalModelPackage.getDwMultiTemporalIntervalElement());
		dwFeatureConfigurationEClass.getESuperTypes().add(theDwTemporalModelPackage.getDwSingleTemporalIntervalElement());
		dwFeatureSelectionEClass.getESuperTypes().add(this.getDwFeatureConfiguration());
		dwFeatureDeselectionEClass.getESuperTypes().add(this.getDwFeatureConfiguration());
		dwAttributeValueConfigurationEClass.getESuperTypes().add(theDwTemporalModelPackage.getDwSingleTemporalIntervalElement());
		dwFeatureVersionConfigurationEClass.getESuperTypes().add(theDwTemporalModelPackage.getDwSingleTemporalIntervalElement());
		dwFeatureVersionSelectionEClass.getESuperTypes().add(this.getDwFeatureVersionConfiguration());
		dwFeatureVersionDeselectionEClass.getESuperTypes().add(this.getDwFeatureVersionConfiguration());

		// Initialize classes, features, and operations; add parameters
		initEClass(dwConfigurationEClass, DwConfiguration.class, "DwConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwConfiguration_FeatureModel(), theDwFeaturePackage.getDwTemporalFeatureModel(), null, "featureModel", null, 1, 1, DwConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwConfiguration_FeatureConfigurations(), this.getDwFeatureConfiguration(), null, "featureConfigurations", null, 0, -1, DwConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwConfiguration_VersionConfigurations(), this.getDwFeatureVersionConfiguration(), null, "versionConfigurations", null, 0, -1, DwConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwFeatureConfigurationEClass, DwFeatureConfiguration.class, "DwFeatureConfiguration", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwFeatureConfiguration_Feature(), theDwFeaturePackage.getDwFeature(), null, "feature", null, 1, 1, DwFeatureConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwFeatureSelectionEClass, DwFeatureSelection.class, "DwFeatureSelection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dwFeatureDeselectionEClass, DwFeatureDeselection.class, "DwFeatureDeselection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dwAttributeValueConfigurationEClass, DwAttributeValueConfiguration.class, "DwAttributeValueConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwAttributeValueConfiguration_Attribute(), theDwFeaturePackage.getDwFeatureAttribute(), null, "attribute", null, 1, 1, DwAttributeValueConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwAttributeValueConfiguration_Value(), theDwDatatypesPackage.getDwValue(), null, "value", null, 1, 1, DwAttributeValueConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwFeatureVersionConfigurationEClass, DwFeatureVersionConfiguration.class, "DwFeatureVersionConfiguration", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwFeatureVersionConfiguration_Version(), theDwFeaturePackage.getDwFeatureVersion(), null, "version", null, 1, 1, DwFeatureVersionConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwFeatureVersionConfiguration_Feature(), theDwFeaturePackage.getDwFeature(), null, "feature", null, 0, 1, DwFeatureVersionConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwFeatureVersionSelectionEClass, DwFeatureVersionSelection.class, "DwFeatureVersionSelection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dwFeatureVersionDeselectionEClass, DwFeatureVersionDeselection.class, "DwFeatureVersionDeselection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //DwConfigurationPackageImpl
