/**
 */
package de.darwinspl.feature.configuration;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Feature Deselection</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.feature.configuration.DwConfigurationPackage#getDwFeatureDeselection()
 * @model
 * @generated
 */
public interface DwFeatureDeselection extends DwFeatureConfiguration {
} // DwFeatureDeselection
