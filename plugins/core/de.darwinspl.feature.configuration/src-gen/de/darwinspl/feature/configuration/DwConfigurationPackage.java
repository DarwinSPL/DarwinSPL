/**
 */
package de.darwinspl.feature.configuration;

import de.darwinspl.temporal.DwTemporalModelPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.darwinspl.feature.configuration.DwConfigurationFactory
 * @model kind="package"
 * @generated
 */
public interface DwConfigurationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "configuration";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.darwinspl.de/configuration/2.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "configuration";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DwConfigurationPackage eINSTANCE = de.darwinspl.feature.configuration.impl.DwConfigurationPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.configuration.impl.DwConfigurationImpl <em>Dw Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.configuration.impl.DwConfigurationImpl
	 * @see de.darwinspl.feature.configuration.impl.DwConfigurationPackageImpl#getDwConfiguration()
	 * @generated
	 */
	int DW_CONFIGURATION = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONFIGURATION__ID = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Validities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONFIGURATION__VALIDITIES = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT__VALIDITIES;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONFIGURATION__FEATURE_MODEL = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Feature Configurations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONFIGURATION__FEATURE_CONFIGURATIONS = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Version Configurations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONFIGURATION__VERSION_CONFIGURATIONS = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Dw Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONFIGURATION_FEATURE_COUNT = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONFIGURATION___CREATE_ID = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONFIGURATION___IS_VALID__DATE = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT___IS_VALID__DATE;

	/**
	 * The number of operations of the '<em>Dw Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_CONFIGURATION_OPERATION_COUNT = DwTemporalModelPackage.DW_MULTI_TEMPORAL_INTERVAL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.configuration.impl.DwFeatureConfigurationImpl <em>Dw Feature Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.configuration.impl.DwFeatureConfigurationImpl
	 * @see de.darwinspl.feature.configuration.impl.DwConfigurationPackageImpl#getDwFeatureConfiguration()
	 * @generated
	 */
	int DW_FEATURE_CONFIGURATION = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_CONFIGURATION__ID = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Validity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_CONFIGURATION__VALIDITY = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__VALIDITY;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_CONFIGURATION__FEATURE = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Dw Feature Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_CONFIGURATION_FEATURE_COUNT = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_CONFIGURATION___CREATE_ID = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_CONFIGURATION___IS_VALID__DATE = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___IS_VALID__DATE;

	/**
	 * The operation id for the '<em>Get From</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_CONFIGURATION___GET_FROM = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_FROM;

	/**
	 * The operation id for the '<em>Get To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_CONFIGURATION___GET_TO = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_TO;

	/**
	 * The number of operations of the '<em>Dw Feature Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_CONFIGURATION_OPERATION_COUNT = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.configuration.impl.DwFeatureSelectionImpl <em>Dw Feature Selection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.configuration.impl.DwFeatureSelectionImpl
	 * @see de.darwinspl.feature.configuration.impl.DwConfigurationPackageImpl#getDwFeatureSelection()
	 * @generated
	 */
	int DW_FEATURE_SELECTION = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_SELECTION__ID = DW_FEATURE_CONFIGURATION__ID;

	/**
	 * The feature id for the '<em><b>Validity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_SELECTION__VALIDITY = DW_FEATURE_CONFIGURATION__VALIDITY;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_SELECTION__FEATURE = DW_FEATURE_CONFIGURATION__FEATURE;

	/**
	 * The number of structural features of the '<em>Dw Feature Selection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_SELECTION_FEATURE_COUNT = DW_FEATURE_CONFIGURATION_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_SELECTION___CREATE_ID = DW_FEATURE_CONFIGURATION___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_SELECTION___IS_VALID__DATE = DW_FEATURE_CONFIGURATION___IS_VALID__DATE;

	/**
	 * The operation id for the '<em>Get From</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_SELECTION___GET_FROM = DW_FEATURE_CONFIGURATION___GET_FROM;

	/**
	 * The operation id for the '<em>Get To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_SELECTION___GET_TO = DW_FEATURE_CONFIGURATION___GET_TO;

	/**
	 * The number of operations of the '<em>Dw Feature Selection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_SELECTION_OPERATION_COUNT = DW_FEATURE_CONFIGURATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.configuration.impl.DwFeatureDeselectionImpl <em>Dw Feature Deselection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.configuration.impl.DwFeatureDeselectionImpl
	 * @see de.darwinspl.feature.configuration.impl.DwConfigurationPackageImpl#getDwFeatureDeselection()
	 * @generated
	 */
	int DW_FEATURE_DESELECTION = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DESELECTION__ID = DW_FEATURE_CONFIGURATION__ID;

	/**
	 * The feature id for the '<em><b>Validity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DESELECTION__VALIDITY = DW_FEATURE_CONFIGURATION__VALIDITY;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DESELECTION__FEATURE = DW_FEATURE_CONFIGURATION__FEATURE;

	/**
	 * The number of structural features of the '<em>Dw Feature Deselection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DESELECTION_FEATURE_COUNT = DW_FEATURE_CONFIGURATION_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DESELECTION___CREATE_ID = DW_FEATURE_CONFIGURATION___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DESELECTION___IS_VALID__DATE = DW_FEATURE_CONFIGURATION___IS_VALID__DATE;

	/**
	 * The operation id for the '<em>Get From</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DESELECTION___GET_FROM = DW_FEATURE_CONFIGURATION___GET_FROM;

	/**
	 * The operation id for the '<em>Get To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DESELECTION___GET_TO = DW_FEATURE_CONFIGURATION___GET_TO;

	/**
	 * The number of operations of the '<em>Dw Feature Deselection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_DESELECTION_OPERATION_COUNT = DW_FEATURE_CONFIGURATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.configuration.impl.DwAttributeValueConfigurationImpl <em>Dw Attribute Value Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.configuration.impl.DwAttributeValueConfigurationImpl
	 * @see de.darwinspl.feature.configuration.impl.DwConfigurationPackageImpl#getDwAttributeValueConfiguration()
	 * @generated
	 */
	int DW_ATTRIBUTE_VALUE_CONFIGURATION = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ATTRIBUTE_VALUE_CONFIGURATION__ID = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Validity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ATTRIBUTE_VALUE_CONFIGURATION__VALIDITY = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__VALIDITY;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ATTRIBUTE_VALUE_CONFIGURATION__ATTRIBUTE = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ATTRIBUTE_VALUE_CONFIGURATION__VALUE = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Dw Attribute Value Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ATTRIBUTE_VALUE_CONFIGURATION_FEATURE_COUNT = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ATTRIBUTE_VALUE_CONFIGURATION___CREATE_ID = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ATTRIBUTE_VALUE_CONFIGURATION___IS_VALID__DATE = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___IS_VALID__DATE;

	/**
	 * The operation id for the '<em>Get From</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ATTRIBUTE_VALUE_CONFIGURATION___GET_FROM = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_FROM;

	/**
	 * The operation id for the '<em>Get To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ATTRIBUTE_VALUE_CONFIGURATION___GET_TO = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_TO;

	/**
	 * The number of operations of the '<em>Dw Attribute Value Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ATTRIBUTE_VALUE_CONFIGURATION_OPERATION_COUNT = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.configuration.impl.DwFeatureVersionConfigurationImpl <em>Dw Feature Version Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.configuration.impl.DwFeatureVersionConfigurationImpl
	 * @see de.darwinspl.feature.configuration.impl.DwConfigurationPackageImpl#getDwFeatureVersionConfiguration()
	 * @generated
	 */
	int DW_FEATURE_VERSION_CONFIGURATION = 5;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_CONFIGURATION__ID = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Validity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_CONFIGURATION__VALIDITY = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT__VALIDITY;

	/**
	 * The feature id for the '<em><b>Version</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_CONFIGURATION__VERSION = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_CONFIGURATION__FEATURE = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Dw Feature Version Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_CONFIGURATION_FEATURE_COUNT = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_CONFIGURATION___CREATE_ID = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_CONFIGURATION___IS_VALID__DATE = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___IS_VALID__DATE;

	/**
	 * The operation id for the '<em>Get From</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_CONFIGURATION___GET_FROM = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_FROM;

	/**
	 * The operation id for the '<em>Get To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_CONFIGURATION___GET_TO = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT___GET_TO;

	/**
	 * The number of operations of the '<em>Dw Feature Version Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_CONFIGURATION_OPERATION_COUNT = DwTemporalModelPackage.DW_SINGLE_TEMPORAL_INTERVAL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.configuration.impl.DwFeatureVersionSelectionImpl <em>Dw Feature Version Selection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.configuration.impl.DwFeatureVersionSelectionImpl
	 * @see de.darwinspl.feature.configuration.impl.DwConfigurationPackageImpl#getDwFeatureVersionSelection()
	 * @generated
	 */
	int DW_FEATURE_VERSION_SELECTION = 6;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_SELECTION__ID = DW_FEATURE_VERSION_CONFIGURATION__ID;

	/**
	 * The feature id for the '<em><b>Validity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_SELECTION__VALIDITY = DW_FEATURE_VERSION_CONFIGURATION__VALIDITY;

	/**
	 * The feature id for the '<em><b>Version</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_SELECTION__VERSION = DW_FEATURE_VERSION_CONFIGURATION__VERSION;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_SELECTION__FEATURE = DW_FEATURE_VERSION_CONFIGURATION__FEATURE;

	/**
	 * The number of structural features of the '<em>Dw Feature Version Selection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_SELECTION_FEATURE_COUNT = DW_FEATURE_VERSION_CONFIGURATION_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_SELECTION___CREATE_ID = DW_FEATURE_VERSION_CONFIGURATION___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_SELECTION___IS_VALID__DATE = DW_FEATURE_VERSION_CONFIGURATION___IS_VALID__DATE;

	/**
	 * The operation id for the '<em>Get From</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_SELECTION___GET_FROM = DW_FEATURE_VERSION_CONFIGURATION___GET_FROM;

	/**
	 * The operation id for the '<em>Get To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_SELECTION___GET_TO = DW_FEATURE_VERSION_CONFIGURATION___GET_TO;

	/**
	 * The number of operations of the '<em>Dw Feature Version Selection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_SELECTION_OPERATION_COUNT = DW_FEATURE_VERSION_CONFIGURATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.feature.configuration.impl.DwFeatureVersionDeselectionImpl <em>Dw Feature Version Deselection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.feature.configuration.impl.DwFeatureVersionDeselectionImpl
	 * @see de.darwinspl.feature.configuration.impl.DwConfigurationPackageImpl#getDwFeatureVersionDeselection()
	 * @generated
	 */
	int DW_FEATURE_VERSION_DESELECTION = 7;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_DESELECTION__ID = DW_FEATURE_VERSION_CONFIGURATION__ID;

	/**
	 * The feature id for the '<em><b>Validity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_DESELECTION__VALIDITY = DW_FEATURE_VERSION_CONFIGURATION__VALIDITY;

	/**
	 * The feature id for the '<em><b>Version</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_DESELECTION__VERSION = DW_FEATURE_VERSION_CONFIGURATION__VERSION;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_DESELECTION__FEATURE = DW_FEATURE_VERSION_CONFIGURATION__FEATURE;

	/**
	 * The number of structural features of the '<em>Dw Feature Version Deselection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_DESELECTION_FEATURE_COUNT = DW_FEATURE_VERSION_CONFIGURATION_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Create Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_DESELECTION___CREATE_ID = DW_FEATURE_VERSION_CONFIGURATION___CREATE_ID;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_DESELECTION___IS_VALID__DATE = DW_FEATURE_VERSION_CONFIGURATION___IS_VALID__DATE;

	/**
	 * The operation id for the '<em>Get From</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_DESELECTION___GET_FROM = DW_FEATURE_VERSION_CONFIGURATION___GET_FROM;

	/**
	 * The operation id for the '<em>Get To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_DESELECTION___GET_TO = DW_FEATURE_VERSION_CONFIGURATION___GET_TO;

	/**
	 * The number of operations of the '<em>Dw Feature Version Deselection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_VERSION_DESELECTION_OPERATION_COUNT = DW_FEATURE_VERSION_CONFIGURATION_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.configuration.DwConfiguration <em>Dw Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Configuration</em>'.
	 * @see de.darwinspl.feature.configuration.DwConfiguration
	 * @generated
	 */
	EClass getDwConfiguration();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.configuration.DwConfiguration#getFeatureModel <em>Feature Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Feature Model</em>'.
	 * @see de.darwinspl.feature.configuration.DwConfiguration#getFeatureModel()
	 * @see #getDwConfiguration()
	 * @generated
	 */
	EReference getDwConfiguration_FeatureModel();

	/**
	 * Returns the meta object for the containment reference list '{@link de.darwinspl.feature.configuration.DwConfiguration#getFeatureConfigurations <em>Feature Configurations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Feature Configurations</em>'.
	 * @see de.darwinspl.feature.configuration.DwConfiguration#getFeatureConfigurations()
	 * @see #getDwConfiguration()
	 * @generated
	 */
	EReference getDwConfiguration_FeatureConfigurations();

	/**
	 * Returns the meta object for the reference list '{@link de.darwinspl.feature.configuration.DwConfiguration#getVersionConfigurations <em>Version Configurations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Version Configurations</em>'.
	 * @see de.darwinspl.feature.configuration.DwConfiguration#getVersionConfigurations()
	 * @see #getDwConfiguration()
	 * @generated
	 */
	EReference getDwConfiguration_VersionConfigurations();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.configuration.DwFeatureConfiguration <em>Dw Feature Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Feature Configuration</em>'.
	 * @see de.darwinspl.feature.configuration.DwFeatureConfiguration
	 * @generated
	 */
	EClass getDwFeatureConfiguration();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.configuration.DwFeatureConfiguration#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Feature</em>'.
	 * @see de.darwinspl.feature.configuration.DwFeatureConfiguration#getFeature()
	 * @see #getDwFeatureConfiguration()
	 * @generated
	 */
	EReference getDwFeatureConfiguration_Feature();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.configuration.DwFeatureSelection <em>Dw Feature Selection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Feature Selection</em>'.
	 * @see de.darwinspl.feature.configuration.DwFeatureSelection
	 * @generated
	 */
	EClass getDwFeatureSelection();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.configuration.DwFeatureDeselection <em>Dw Feature Deselection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Feature Deselection</em>'.
	 * @see de.darwinspl.feature.configuration.DwFeatureDeselection
	 * @generated
	 */
	EClass getDwFeatureDeselection();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.configuration.DwAttributeValueConfiguration <em>Dw Attribute Value Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Attribute Value Configuration</em>'.
	 * @see de.darwinspl.feature.configuration.DwAttributeValueConfiguration
	 * @generated
	 */
	EClass getDwAttributeValueConfiguration();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.configuration.DwAttributeValueConfiguration#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute</em>'.
	 * @see de.darwinspl.feature.configuration.DwAttributeValueConfiguration#getAttribute()
	 * @see #getDwAttributeValueConfiguration()
	 * @generated
	 */
	EReference getDwAttributeValueConfiguration_Attribute();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.configuration.DwAttributeValueConfiguration#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see de.darwinspl.feature.configuration.DwAttributeValueConfiguration#getValue()
	 * @see #getDwAttributeValueConfiguration()
	 * @generated
	 */
	EReference getDwAttributeValueConfiguration_Value();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.configuration.DwFeatureVersionConfiguration <em>Dw Feature Version Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Feature Version Configuration</em>'.
	 * @see de.darwinspl.feature.configuration.DwFeatureVersionConfiguration
	 * @generated
	 */
	EClass getDwFeatureVersionConfiguration();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.configuration.DwFeatureVersionConfiguration#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Version</em>'.
	 * @see de.darwinspl.feature.configuration.DwFeatureVersionConfiguration#getVersion()
	 * @see #getDwFeatureVersionConfiguration()
	 * @generated
	 */
	EReference getDwFeatureVersionConfiguration_Version();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.feature.configuration.DwFeatureVersionConfiguration#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Feature</em>'.
	 * @see de.darwinspl.feature.configuration.DwFeatureVersionConfiguration#getFeature()
	 * @see #getDwFeatureVersionConfiguration()
	 * @generated
	 */
	EReference getDwFeatureVersionConfiguration_Feature();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.configuration.DwFeatureVersionSelection <em>Dw Feature Version Selection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Feature Version Selection</em>'.
	 * @see de.darwinspl.feature.configuration.DwFeatureVersionSelection
	 * @generated
	 */
	EClass getDwFeatureVersionSelection();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.feature.configuration.DwFeatureVersionDeselection <em>Dw Feature Version Deselection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Feature Version Deselection</em>'.
	 * @see de.darwinspl.feature.configuration.DwFeatureVersionDeselection
	 * @generated
	 */
	EClass getDwFeatureVersionDeselection();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DwConfigurationFactory getDwConfigurationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.configuration.impl.DwConfigurationImpl <em>Dw Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.configuration.impl.DwConfigurationImpl
		 * @see de.darwinspl.feature.configuration.impl.DwConfigurationPackageImpl#getDwConfiguration()
		 * @generated
		 */
		EClass DW_CONFIGURATION = eINSTANCE.getDwConfiguration();

		/**
		 * The meta object literal for the '<em><b>Feature Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_CONFIGURATION__FEATURE_MODEL = eINSTANCE.getDwConfiguration_FeatureModel();

		/**
		 * The meta object literal for the '<em><b>Feature Configurations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_CONFIGURATION__FEATURE_CONFIGURATIONS = eINSTANCE.getDwConfiguration_FeatureConfigurations();

		/**
		 * The meta object literal for the '<em><b>Version Configurations</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_CONFIGURATION__VERSION_CONFIGURATIONS = eINSTANCE.getDwConfiguration_VersionConfigurations();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.configuration.impl.DwFeatureConfigurationImpl <em>Dw Feature Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.configuration.impl.DwFeatureConfigurationImpl
		 * @see de.darwinspl.feature.configuration.impl.DwConfigurationPackageImpl#getDwFeatureConfiguration()
		 * @generated
		 */
		EClass DW_FEATURE_CONFIGURATION = eINSTANCE.getDwFeatureConfiguration();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_CONFIGURATION__FEATURE = eINSTANCE.getDwFeatureConfiguration_Feature();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.configuration.impl.DwFeatureSelectionImpl <em>Dw Feature Selection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.configuration.impl.DwFeatureSelectionImpl
		 * @see de.darwinspl.feature.configuration.impl.DwConfigurationPackageImpl#getDwFeatureSelection()
		 * @generated
		 */
		EClass DW_FEATURE_SELECTION = eINSTANCE.getDwFeatureSelection();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.configuration.impl.DwFeatureDeselectionImpl <em>Dw Feature Deselection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.configuration.impl.DwFeatureDeselectionImpl
		 * @see de.darwinspl.feature.configuration.impl.DwConfigurationPackageImpl#getDwFeatureDeselection()
		 * @generated
		 */
		EClass DW_FEATURE_DESELECTION = eINSTANCE.getDwFeatureDeselection();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.configuration.impl.DwAttributeValueConfigurationImpl <em>Dw Attribute Value Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.configuration.impl.DwAttributeValueConfigurationImpl
		 * @see de.darwinspl.feature.configuration.impl.DwConfigurationPackageImpl#getDwAttributeValueConfiguration()
		 * @generated
		 */
		EClass DW_ATTRIBUTE_VALUE_CONFIGURATION = eINSTANCE.getDwAttributeValueConfiguration();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_ATTRIBUTE_VALUE_CONFIGURATION__ATTRIBUTE = eINSTANCE.getDwAttributeValueConfiguration_Attribute();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_ATTRIBUTE_VALUE_CONFIGURATION__VALUE = eINSTANCE.getDwAttributeValueConfiguration_Value();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.configuration.impl.DwFeatureVersionConfigurationImpl <em>Dw Feature Version Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.configuration.impl.DwFeatureVersionConfigurationImpl
		 * @see de.darwinspl.feature.configuration.impl.DwConfigurationPackageImpl#getDwFeatureVersionConfiguration()
		 * @generated
		 */
		EClass DW_FEATURE_VERSION_CONFIGURATION = eINSTANCE.getDwFeatureVersionConfiguration();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_VERSION_CONFIGURATION__VERSION = eINSTANCE.getDwFeatureVersionConfiguration_Version();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_VERSION_CONFIGURATION__FEATURE = eINSTANCE.getDwFeatureVersionConfiguration_Feature();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.configuration.impl.DwFeatureVersionSelectionImpl <em>Dw Feature Version Selection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.configuration.impl.DwFeatureVersionSelectionImpl
		 * @see de.darwinspl.feature.configuration.impl.DwConfigurationPackageImpl#getDwFeatureVersionSelection()
		 * @generated
		 */
		EClass DW_FEATURE_VERSION_SELECTION = eINSTANCE.getDwFeatureVersionSelection();

		/**
		 * The meta object literal for the '{@link de.darwinspl.feature.configuration.impl.DwFeatureVersionDeselectionImpl <em>Dw Feature Version Deselection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.feature.configuration.impl.DwFeatureVersionDeselectionImpl
		 * @see de.darwinspl.feature.configuration.impl.DwConfigurationPackageImpl#getDwFeatureVersionDeselection()
		 * @generated
		 */
		EClass DW_FEATURE_VERSION_DESELECTION = eINSTANCE.getDwFeatureVersionDeselection();

	}

} //DwConfigurationPackage
