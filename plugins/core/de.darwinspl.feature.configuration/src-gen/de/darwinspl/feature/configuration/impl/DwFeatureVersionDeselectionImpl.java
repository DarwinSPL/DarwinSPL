/**
 */
package de.darwinspl.feature.configuration.impl;

import de.darwinspl.feature.configuration.DwConfigurationPackage;
import de.darwinspl.feature.configuration.DwFeatureVersionDeselection;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Feature Version Deselection</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DwFeatureVersionDeselectionImpl extends DwFeatureVersionConfigurationImpl implements DwFeatureVersionDeselection {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwFeatureVersionDeselectionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwConfigurationPackage.Literals.DW_FEATURE_VERSION_DESELECTION;
	}

} //DwFeatureVersionDeselectionImpl
