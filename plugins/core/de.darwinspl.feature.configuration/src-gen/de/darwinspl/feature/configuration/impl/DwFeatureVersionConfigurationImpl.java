/**
 */
package de.darwinspl.feature.configuration.impl;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureVersion;

import de.darwinspl.feature.configuration.DwConfigurationPackage;
import de.darwinspl.feature.configuration.DwFeatureVersionConfiguration;

import de.darwinspl.temporal.impl.DwSingleTemporalIntervalElementImpl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Feature Version Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.configuration.impl.DwFeatureVersionConfigurationImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link de.darwinspl.feature.configuration.impl.DwFeatureVersionConfigurationImpl#getFeature <em>Feature</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class DwFeatureVersionConfigurationImpl extends DwSingleTemporalIntervalElementImpl implements DwFeatureVersionConfiguration {
	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected DwFeatureVersion version;

	/**
	 * The cached value of the '{@link #getFeature() <em>Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeature()
	 * @generated
	 * @ordered
	 */
	protected DwFeature feature;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwFeatureVersionConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwConfigurationPackage.Literals.DW_FEATURE_VERSION_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureVersion getVersion() {
		if (version != null && version.eIsProxy()) {
			InternalEObject oldVersion = (InternalEObject)version;
			version = (DwFeatureVersion)eResolveProxy(oldVersion);
			if (version != oldVersion) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwConfigurationPackage.DW_FEATURE_VERSION_CONFIGURATION__VERSION, oldVersion, version));
			}
		}
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwFeatureVersion basicGetVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVersion(DwFeatureVersion newVersion) {
		DwFeatureVersion oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwConfigurationPackage.DW_FEATURE_VERSION_CONFIGURATION__VERSION, oldVersion, version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeature getFeature() {
		if (feature != null && feature.eIsProxy()) {
			InternalEObject oldFeature = (InternalEObject)feature;
			feature = (DwFeature)eResolveProxy(oldFeature);
			if (feature != oldFeature) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwConfigurationPackage.DW_FEATURE_VERSION_CONFIGURATION__FEATURE, oldFeature, feature));
			}
		}
		return feature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwFeature basicGetFeature() {
		return feature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFeature(DwFeature newFeature) {
		DwFeature oldFeature = feature;
		feature = newFeature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwConfigurationPackage.DW_FEATURE_VERSION_CONFIGURATION__FEATURE, oldFeature, feature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwConfigurationPackage.DW_FEATURE_VERSION_CONFIGURATION__VERSION:
				if (resolve) return getVersion();
				return basicGetVersion();
			case DwConfigurationPackage.DW_FEATURE_VERSION_CONFIGURATION__FEATURE:
				if (resolve) return getFeature();
				return basicGetFeature();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwConfigurationPackage.DW_FEATURE_VERSION_CONFIGURATION__VERSION:
				setVersion((DwFeatureVersion)newValue);
				return;
			case DwConfigurationPackage.DW_FEATURE_VERSION_CONFIGURATION__FEATURE:
				setFeature((DwFeature)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwConfigurationPackage.DW_FEATURE_VERSION_CONFIGURATION__VERSION:
				setVersion((DwFeatureVersion)null);
				return;
			case DwConfigurationPackage.DW_FEATURE_VERSION_CONFIGURATION__FEATURE:
				setFeature((DwFeature)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwConfigurationPackage.DW_FEATURE_VERSION_CONFIGURATION__VERSION:
				return version != null;
			case DwConfigurationPackage.DW_FEATURE_VERSION_CONFIGURATION__FEATURE:
				return feature != null;
		}
		return super.eIsSet(featureID);
	}

} //DwFeatureVersionConfigurationImpl
