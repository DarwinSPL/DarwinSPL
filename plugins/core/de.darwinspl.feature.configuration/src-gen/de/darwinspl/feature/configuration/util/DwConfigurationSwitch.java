/**
 */
package de.darwinspl.feature.configuration.util;

import de.darwinspl.feature.configuration.*;

import de.darwinspl.temporal.DwElementWithId;
import de.darwinspl.temporal.DwMultiTemporalIntervalElement;
import de.darwinspl.temporal.DwSingleTemporalIntervalElement;
import de.darwinspl.temporal.DwTemporalElement;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see de.darwinspl.feature.configuration.DwConfigurationPackage
 * @generated
 */
public class DwConfigurationSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DwConfigurationPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwConfigurationSwitch() {
		if (modelPackage == null) {
			modelPackage = DwConfigurationPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case DwConfigurationPackage.DW_CONFIGURATION: {
				DwConfiguration dwConfiguration = (DwConfiguration)theEObject;
				T result = caseDwConfiguration(dwConfiguration);
				if (result == null) result = caseDwMultiTemporalIntervalElement(dwConfiguration);
				if (result == null) result = caseDwTemporalElement(dwConfiguration);
				if (result == null) result = caseDwElementWithId(dwConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwConfigurationPackage.DW_FEATURE_CONFIGURATION: {
				DwFeatureConfiguration dwFeatureConfiguration = (DwFeatureConfiguration)theEObject;
				T result = caseDwFeatureConfiguration(dwFeatureConfiguration);
				if (result == null) result = caseDwSingleTemporalIntervalElement(dwFeatureConfiguration);
				if (result == null) result = caseDwTemporalElement(dwFeatureConfiguration);
				if (result == null) result = caseDwElementWithId(dwFeatureConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwConfigurationPackage.DW_FEATURE_SELECTION: {
				DwFeatureSelection dwFeatureSelection = (DwFeatureSelection)theEObject;
				T result = caseDwFeatureSelection(dwFeatureSelection);
				if (result == null) result = caseDwFeatureConfiguration(dwFeatureSelection);
				if (result == null) result = caseDwSingleTemporalIntervalElement(dwFeatureSelection);
				if (result == null) result = caseDwTemporalElement(dwFeatureSelection);
				if (result == null) result = caseDwElementWithId(dwFeatureSelection);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwConfigurationPackage.DW_FEATURE_DESELECTION: {
				DwFeatureDeselection dwFeatureDeselection = (DwFeatureDeselection)theEObject;
				T result = caseDwFeatureDeselection(dwFeatureDeselection);
				if (result == null) result = caseDwFeatureConfiguration(dwFeatureDeselection);
				if (result == null) result = caseDwSingleTemporalIntervalElement(dwFeatureDeselection);
				if (result == null) result = caseDwTemporalElement(dwFeatureDeselection);
				if (result == null) result = caseDwElementWithId(dwFeatureDeselection);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwConfigurationPackage.DW_ATTRIBUTE_VALUE_CONFIGURATION: {
				DwAttributeValueConfiguration dwAttributeValueConfiguration = (DwAttributeValueConfiguration)theEObject;
				T result = caseDwAttributeValueConfiguration(dwAttributeValueConfiguration);
				if (result == null) result = caseDwSingleTemporalIntervalElement(dwAttributeValueConfiguration);
				if (result == null) result = caseDwTemporalElement(dwAttributeValueConfiguration);
				if (result == null) result = caseDwElementWithId(dwAttributeValueConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwConfigurationPackage.DW_FEATURE_VERSION_CONFIGURATION: {
				DwFeatureVersionConfiguration dwFeatureVersionConfiguration = (DwFeatureVersionConfiguration)theEObject;
				T result = caseDwFeatureVersionConfiguration(dwFeatureVersionConfiguration);
				if (result == null) result = caseDwSingleTemporalIntervalElement(dwFeatureVersionConfiguration);
				if (result == null) result = caseDwTemporalElement(dwFeatureVersionConfiguration);
				if (result == null) result = caseDwElementWithId(dwFeatureVersionConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwConfigurationPackage.DW_FEATURE_VERSION_SELECTION: {
				DwFeatureVersionSelection dwFeatureVersionSelection = (DwFeatureVersionSelection)theEObject;
				T result = caseDwFeatureVersionSelection(dwFeatureVersionSelection);
				if (result == null) result = caseDwFeatureVersionConfiguration(dwFeatureVersionSelection);
				if (result == null) result = caseDwSingleTemporalIntervalElement(dwFeatureVersionSelection);
				if (result == null) result = caseDwTemporalElement(dwFeatureVersionSelection);
				if (result == null) result = caseDwElementWithId(dwFeatureVersionSelection);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwConfigurationPackage.DW_FEATURE_VERSION_DESELECTION: {
				DwFeatureVersionDeselection dwFeatureVersionDeselection = (DwFeatureVersionDeselection)theEObject;
				T result = caseDwFeatureVersionDeselection(dwFeatureVersionDeselection);
				if (result == null) result = caseDwFeatureVersionConfiguration(dwFeatureVersionDeselection);
				if (result == null) result = caseDwSingleTemporalIntervalElement(dwFeatureVersionDeselection);
				if (result == null) result = caseDwTemporalElement(dwFeatureVersionDeselection);
				if (result == null) result = caseDwElementWithId(dwFeatureVersionDeselection);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwConfiguration(DwConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Feature Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Feature Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureConfiguration(DwFeatureConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Feature Selection</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Feature Selection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureSelection(DwFeatureSelection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Feature Deselection</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Feature Deselection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureDeselection(DwFeatureDeselection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Attribute Value Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Attribute Value Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwAttributeValueConfiguration(DwAttributeValueConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Feature Version Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Feature Version Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureVersionConfiguration(DwFeatureVersionConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Feature Version Selection</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Feature Version Selection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureVersionSelection(DwFeatureVersionSelection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Feature Version Deselection</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Feature Version Deselection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureVersionDeselection(DwFeatureVersionDeselection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Element With Id</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Element With Id</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwElementWithId(DwElementWithId object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Temporal Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Temporal Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwTemporalElement(DwTemporalElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Multi Temporal Interval Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Multi Temporal Interval Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwMultiTemporalIntervalElement(DwMultiTemporalIntervalElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Single Temporal Interval Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Single Temporal Interval Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwSingleTemporalIntervalElement(DwSingleTemporalIntervalElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //DwConfigurationSwitch
