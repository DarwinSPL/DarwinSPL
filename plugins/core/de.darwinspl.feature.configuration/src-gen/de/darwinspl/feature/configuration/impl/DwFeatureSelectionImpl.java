/**
 */
package de.darwinspl.feature.configuration.impl;

import de.darwinspl.feature.configuration.DwConfigurationPackage;
import de.darwinspl.feature.configuration.DwFeatureSelection;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Feature Selection</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DwFeatureSelectionImpl extends DwFeatureConfigurationImpl implements DwFeatureSelection {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwFeatureSelectionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwConfigurationPackage.Literals.DW_FEATURE_SELECTION;
	}

} //DwFeatureSelectionImpl
