/**
 */
package de.darwinspl.feature.configuration.impl;

import de.darwinspl.feature.configuration.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DwConfigurationFactoryImpl extends EFactoryImpl implements DwConfigurationFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DwConfigurationFactory init() {
		try {
			DwConfigurationFactory theDwConfigurationFactory = (DwConfigurationFactory)EPackage.Registry.INSTANCE.getEFactory(DwConfigurationPackage.eNS_URI);
			if (theDwConfigurationFactory != null) {
				return theDwConfigurationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DwConfigurationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwConfigurationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case DwConfigurationPackage.DW_CONFIGURATION: return createDwConfiguration();
			case DwConfigurationPackage.DW_FEATURE_SELECTION: return createDwFeatureSelection();
			case DwConfigurationPackage.DW_FEATURE_DESELECTION: return createDwFeatureDeselection();
			case DwConfigurationPackage.DW_ATTRIBUTE_VALUE_CONFIGURATION: return createDwAttributeValueConfiguration();
			case DwConfigurationPackage.DW_FEATURE_VERSION_SELECTION: return createDwFeatureVersionSelection();
			case DwConfigurationPackage.DW_FEATURE_VERSION_DESELECTION: return createDwFeatureVersionDeselection();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwConfiguration createDwConfiguration() {
		DwConfigurationImpl dwConfiguration = new DwConfigurationImpl();
		return dwConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureSelection createDwFeatureSelection() {
		DwFeatureSelectionImpl dwFeatureSelection = new DwFeatureSelectionImpl();
		return dwFeatureSelection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureDeselection createDwFeatureDeselection() {
		DwFeatureDeselectionImpl dwFeatureDeselection = new DwFeatureDeselectionImpl();
		return dwFeatureDeselection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwAttributeValueConfiguration createDwAttributeValueConfiguration() {
		DwAttributeValueConfigurationImpl dwAttributeValueConfiguration = new DwAttributeValueConfigurationImpl();
		return dwAttributeValueConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureVersionSelection createDwFeatureVersionSelection() {
		DwFeatureVersionSelectionImpl dwFeatureVersionSelection = new DwFeatureVersionSelectionImpl();
		return dwFeatureVersionSelection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureVersionDeselection createDwFeatureVersionDeselection() {
		DwFeatureVersionDeselectionImpl dwFeatureVersionDeselection = new DwFeatureVersionDeselectionImpl();
		return dwFeatureVersionDeselection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwConfigurationPackage getDwConfigurationPackage() {
		return (DwConfigurationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DwConfigurationPackage getPackage() {
		return DwConfigurationPackage.eINSTANCE;
	}

} //DwConfigurationFactoryImpl
