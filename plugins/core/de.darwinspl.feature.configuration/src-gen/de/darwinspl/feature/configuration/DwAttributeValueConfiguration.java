/**
 */
package de.darwinspl.feature.configuration;

import de.darwinspl.datatypes.DwValue;

import de.darwinspl.feature.DwFeatureAttribute;

import de.darwinspl.temporal.DwSingleTemporalIntervalElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Attribute Value Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.configuration.DwAttributeValueConfiguration#getAttribute <em>Attribute</em>}</li>
 *   <li>{@link de.darwinspl.feature.configuration.DwAttributeValueConfiguration#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.configuration.DwConfigurationPackage#getDwAttributeValueConfiguration()
 * @model
 * @generated
 */
public interface DwAttributeValueConfiguration extends DwSingleTemporalIntervalElement {
	/**
	 * Returns the value of the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute</em>' reference.
	 * @see #setAttribute(DwFeatureAttribute)
	 * @see de.darwinspl.feature.configuration.DwConfigurationPackage#getDwAttributeValueConfiguration_Attribute()
	 * @model required="true"
	 * @generated
	 */
	DwFeatureAttribute getAttribute();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.configuration.DwAttributeValueConfiguration#getAttribute <em>Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute</em>' reference.
	 * @see #getAttribute()
	 * @generated
	 */
	void setAttribute(DwFeatureAttribute value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' reference.
	 * @see #setValue(DwValue)
	 * @see de.darwinspl.feature.configuration.DwConfigurationPackage#getDwAttributeValueConfiguration_Value()
	 * @model required="true"
	 * @generated
	 */
	DwValue getValue();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.configuration.DwAttributeValueConfiguration#getValue <em>Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(DwValue value);

} // DwAttributeValueConfiguration
