/**
 */
package de.darwinspl.feature.configuration.impl;

import de.darwinspl.feature.DwTemporalFeatureModel;

import de.darwinspl.feature.configuration.DwConfiguration;
import de.darwinspl.feature.configuration.DwConfigurationPackage;
import de.darwinspl.feature.configuration.DwFeatureConfiguration;
import de.darwinspl.feature.configuration.DwFeatureVersionConfiguration;

import de.darwinspl.temporal.impl.DwMultiTemporalIntervalElementImpl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.configuration.impl.DwConfigurationImpl#getFeatureModel <em>Feature Model</em>}</li>
 *   <li>{@link de.darwinspl.feature.configuration.impl.DwConfigurationImpl#getFeatureConfigurations <em>Feature Configurations</em>}</li>
 *   <li>{@link de.darwinspl.feature.configuration.impl.DwConfigurationImpl#getVersionConfigurations <em>Version Configurations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwConfigurationImpl extends DwMultiTemporalIntervalElementImpl implements DwConfiguration {
	/**
	 * The cached value of the '{@link #getFeatureModel() <em>Feature Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatureModel()
	 * @generated
	 * @ordered
	 */
	protected DwTemporalFeatureModel featureModel;

	/**
	 * The cached value of the '{@link #getFeatureConfigurations() <em>Feature Configurations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatureConfigurations()
	 * @generated
	 * @ordered
	 */
	protected EList<DwFeatureConfiguration> featureConfigurations;

	/**
	 * The cached value of the '{@link #getVersionConfigurations() <em>Version Configurations</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersionConfigurations()
	 * @generated
	 * @ordered
	 */
	protected EList<DwFeatureVersionConfiguration> versionConfigurations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwConfigurationPackage.Literals.DW_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwTemporalFeatureModel getFeatureModel() {
		if (featureModel != null && featureModel.eIsProxy()) {
			InternalEObject oldFeatureModel = (InternalEObject)featureModel;
			featureModel = (DwTemporalFeatureModel)eResolveProxy(oldFeatureModel);
			if (featureModel != oldFeatureModel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwConfigurationPackage.DW_CONFIGURATION__FEATURE_MODEL, oldFeatureModel, featureModel));
			}
		}
		return featureModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwTemporalFeatureModel basicGetFeatureModel() {
		return featureModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFeatureModel(DwTemporalFeatureModel newFeatureModel) {
		DwTemporalFeatureModel oldFeatureModel = featureModel;
		featureModel = newFeatureModel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwConfigurationPackage.DW_CONFIGURATION__FEATURE_MODEL, oldFeatureModel, featureModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwFeatureConfiguration> getFeatureConfigurations() {
		if (featureConfigurations == null) {
			featureConfigurations = new EObjectContainmentEList<DwFeatureConfiguration>(DwFeatureConfiguration.class, this, DwConfigurationPackage.DW_CONFIGURATION__FEATURE_CONFIGURATIONS);
		}
		return featureConfigurations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DwFeatureVersionConfiguration> getVersionConfigurations() {
		if (versionConfigurations == null) {
			versionConfigurations = new EObjectResolvingEList<DwFeatureVersionConfiguration>(DwFeatureVersionConfiguration.class, this, DwConfigurationPackage.DW_CONFIGURATION__VERSION_CONFIGURATIONS);
		}
		return versionConfigurations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DwConfigurationPackage.DW_CONFIGURATION__FEATURE_CONFIGURATIONS:
				return ((InternalEList<?>)getFeatureConfigurations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwConfigurationPackage.DW_CONFIGURATION__FEATURE_MODEL:
				if (resolve) return getFeatureModel();
				return basicGetFeatureModel();
			case DwConfigurationPackage.DW_CONFIGURATION__FEATURE_CONFIGURATIONS:
				return getFeatureConfigurations();
			case DwConfigurationPackage.DW_CONFIGURATION__VERSION_CONFIGURATIONS:
				return getVersionConfigurations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwConfigurationPackage.DW_CONFIGURATION__FEATURE_MODEL:
				setFeatureModel((DwTemporalFeatureModel)newValue);
				return;
			case DwConfigurationPackage.DW_CONFIGURATION__FEATURE_CONFIGURATIONS:
				getFeatureConfigurations().clear();
				getFeatureConfigurations().addAll((Collection<? extends DwFeatureConfiguration>)newValue);
				return;
			case DwConfigurationPackage.DW_CONFIGURATION__VERSION_CONFIGURATIONS:
				getVersionConfigurations().clear();
				getVersionConfigurations().addAll((Collection<? extends DwFeatureVersionConfiguration>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwConfigurationPackage.DW_CONFIGURATION__FEATURE_MODEL:
				setFeatureModel((DwTemporalFeatureModel)null);
				return;
			case DwConfigurationPackage.DW_CONFIGURATION__FEATURE_CONFIGURATIONS:
				getFeatureConfigurations().clear();
				return;
			case DwConfigurationPackage.DW_CONFIGURATION__VERSION_CONFIGURATIONS:
				getVersionConfigurations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwConfigurationPackage.DW_CONFIGURATION__FEATURE_MODEL:
				return featureModel != null;
			case DwConfigurationPackage.DW_CONFIGURATION__FEATURE_CONFIGURATIONS:
				return featureConfigurations != null && !featureConfigurations.isEmpty();
			case DwConfigurationPackage.DW_CONFIGURATION__VERSION_CONFIGURATIONS:
				return versionConfigurations != null && !versionConfigurations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DwConfigurationImpl
