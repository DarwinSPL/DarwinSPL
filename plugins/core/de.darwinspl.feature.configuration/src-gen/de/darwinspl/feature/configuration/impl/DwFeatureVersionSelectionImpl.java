/**
 */
package de.darwinspl.feature.configuration.impl;

import de.darwinspl.feature.configuration.DwConfigurationPackage;
import de.darwinspl.feature.configuration.DwFeatureVersionSelection;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Feature Version Selection</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DwFeatureVersionSelectionImpl extends DwFeatureVersionConfigurationImpl implements DwFeatureVersionSelection {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwFeatureVersionSelectionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwConfigurationPackage.Literals.DW_FEATURE_VERSION_SELECTION;
	}

} //DwFeatureVersionSelectionImpl
