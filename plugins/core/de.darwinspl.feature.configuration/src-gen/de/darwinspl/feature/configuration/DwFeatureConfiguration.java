/**
 */
package de.darwinspl.feature.configuration;

import de.darwinspl.feature.DwFeature;

import de.darwinspl.temporal.DwSingleTemporalIntervalElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Feature Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.configuration.DwFeatureConfiguration#getFeature <em>Feature</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.feature.configuration.DwConfigurationPackage#getDwFeatureConfiguration()
 * @model abstract="true"
 * @generated
 */
public interface DwFeatureConfiguration extends DwSingleTemporalIntervalElement {
	/**
	 * Returns the value of the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature</em>' reference.
	 * @see #setFeature(DwFeature)
	 * @see de.darwinspl.feature.configuration.DwConfigurationPackage#getDwFeatureConfiguration_Feature()
	 * @model required="true"
	 * @generated
	 */
	DwFeature getFeature();

	/**
	 * Sets the value of the '{@link de.darwinspl.feature.configuration.DwFeatureConfiguration#getFeature <em>Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature</em>' reference.
	 * @see #getFeature()
	 * @generated
	 */
	void setFeature(DwFeature value);

} // DwFeatureConfiguration
