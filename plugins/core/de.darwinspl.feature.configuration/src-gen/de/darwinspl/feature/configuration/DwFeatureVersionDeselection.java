/**
 */
package de.darwinspl.feature.configuration;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Feature Version Deselection</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.feature.configuration.DwConfigurationPackage#getDwFeatureVersionDeselection()
 * @model
 * @generated
 */
public interface DwFeatureVersionDeselection extends DwFeatureVersionConfiguration {
} // DwFeatureVersionDeselection
