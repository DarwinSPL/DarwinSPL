/**
 */
package de.darwinspl.feature.configuration.impl;

import de.darwinspl.feature.configuration.DwConfigurationPackage;
import de.darwinspl.feature.configuration.DwFeatureDeselection;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Feature Deselection</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DwFeatureDeselectionImpl extends DwFeatureConfigurationImpl implements DwFeatureDeselection {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwFeatureDeselectionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwConfigurationPackage.Literals.DW_FEATURE_DESELECTION;
	}

} //DwFeatureDeselectionImpl
