/**
 */
package de.darwinspl.feature.configuration.impl;

import de.darwinspl.datatypes.DwValue;

import de.darwinspl.feature.DwFeatureAttribute;

import de.darwinspl.feature.configuration.DwAttributeValueConfiguration;
import de.darwinspl.feature.configuration.DwConfigurationPackage;

import de.darwinspl.temporal.impl.DwSingleTemporalIntervalElementImpl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Attribute Value Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.feature.configuration.impl.DwAttributeValueConfigurationImpl#getAttribute <em>Attribute</em>}</li>
 *   <li>{@link de.darwinspl.feature.configuration.impl.DwAttributeValueConfigurationImpl#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DwAttributeValueConfigurationImpl extends DwSingleTemporalIntervalElementImpl implements DwAttributeValueConfiguration {
	/**
	 * The cached value of the '{@link #getAttribute() <em>Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttribute()
	 * @generated
	 * @ordered
	 */
	protected DwFeatureAttribute attribute;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected DwValue value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwAttributeValueConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwConfigurationPackage.Literals.DW_ATTRIBUTE_VALUE_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureAttribute getAttribute() {
		if (attribute != null && attribute.eIsProxy()) {
			InternalEObject oldAttribute = (InternalEObject)attribute;
			attribute = (DwFeatureAttribute)eResolveProxy(oldAttribute);
			if (attribute != oldAttribute) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwConfigurationPackage.DW_ATTRIBUTE_VALUE_CONFIGURATION__ATTRIBUTE, oldAttribute, attribute));
			}
		}
		return attribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwFeatureAttribute basicGetAttribute() {
		return attribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAttribute(DwFeatureAttribute newAttribute) {
		DwFeatureAttribute oldAttribute = attribute;
		attribute = newAttribute;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwConfigurationPackage.DW_ATTRIBUTE_VALUE_CONFIGURATION__ATTRIBUTE, oldAttribute, attribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwValue getValue() {
		if (value != null && value.eIsProxy()) {
			InternalEObject oldValue = (InternalEObject)value;
			value = (DwValue)eResolveProxy(oldValue);
			if (value != oldValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DwConfigurationPackage.DW_ATTRIBUTE_VALUE_CONFIGURATION__VALUE, oldValue, value));
			}
		}
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwValue basicGetValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setValue(DwValue newValue) {
		DwValue oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DwConfigurationPackage.DW_ATTRIBUTE_VALUE_CONFIGURATION__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DwConfigurationPackage.DW_ATTRIBUTE_VALUE_CONFIGURATION__ATTRIBUTE:
				if (resolve) return getAttribute();
				return basicGetAttribute();
			case DwConfigurationPackage.DW_ATTRIBUTE_VALUE_CONFIGURATION__VALUE:
				if (resolve) return getValue();
				return basicGetValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DwConfigurationPackage.DW_ATTRIBUTE_VALUE_CONFIGURATION__ATTRIBUTE:
				setAttribute((DwFeatureAttribute)newValue);
				return;
			case DwConfigurationPackage.DW_ATTRIBUTE_VALUE_CONFIGURATION__VALUE:
				setValue((DwValue)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DwConfigurationPackage.DW_ATTRIBUTE_VALUE_CONFIGURATION__ATTRIBUTE:
				setAttribute((DwFeatureAttribute)null);
				return;
			case DwConfigurationPackage.DW_ATTRIBUTE_VALUE_CONFIGURATION__VALUE:
				setValue((DwValue)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DwConfigurationPackage.DW_ATTRIBUTE_VALUE_CONFIGURATION__ATTRIBUTE:
				return attribute != null;
			case DwConfigurationPackage.DW_ATTRIBUTE_VALUE_CONFIGURATION__VALUE:
				return value != null;
		}
		return super.eIsSet(featureID);
	}

} //DwAttributeValueConfigurationImpl
