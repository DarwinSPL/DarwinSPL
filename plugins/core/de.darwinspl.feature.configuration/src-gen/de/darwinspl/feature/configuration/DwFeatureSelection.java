/**
 */
package de.darwinspl.feature.configuration;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Feature Selection</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.feature.configuration.DwConfigurationPackage#getDwFeatureSelection()
 * @model
 * @generated
 */
public interface DwFeatureSelection extends DwFeatureConfiguration {
} // DwFeatureSelection
