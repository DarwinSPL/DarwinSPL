package de.darwinspl.feature.graphical.treeviewer.treeviewer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Composite;

import de.darwinspl.properties.sources.DwTemporalElementPropertySource;
import de.darwinspl.temporal.DwTemporalElement;

public class DwGraphicalTreeViewer extends TreeViewer {

	DwGraphicalTreeViewerEditor editor;
	
	public DwGraphicalTreeViewer(Composite parent) {
		super(parent);
	}
	
	public DwGraphicalTreeViewer(Composite parent, DwGraphicalTreeViewerEditor editor) {
		super(parent);
		this.editor = editor;
	}
	
//	@Override
//	public ISelection getSelection() {
//		ISelection selection = super.getSelection();
//		if(!selection.isEmpty()) {
//			List<TreePath> newTreePaths = new ArrayList<>();
//			
//			Iterator<?> elementIterator = ((TreeSelection) selection).iterator();
//			while(elementIterator.hasNext()) {
//				Object selectedElementUncasted = elementIterator.next();
//				if(selectedElementUncasted instanceof DwTemporalElement) {
//					DwTemporalElement selectedElement = (DwTemporalElement) selectedElementUncasted;
//					DwTemporalElementPropertySource wrappedSelection = new DwTemporalElementPropertySource(selectedElement, this.editor.getCurrentSelectedDate(), this.editor.isLastDateSelected());
//					TreePath newPath = new TreePath(new DwTemporalElementPropertySource[] {wrappedSelection});
//					newTreePaths.add(newPath);
//				}
//			}
//			
//			selection = new TreeSelection(newTreePaths.toArray(new TreePath[]{}));
//		}
//		return selection;
//	}
	
	public DwGraphicalTreeViewerEditor getEditor() {
		return editor;
	}

}
