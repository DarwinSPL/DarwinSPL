package de.darwinspl.feature.graphical.treeviewer.provider;

import java.net.URL;

import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.internal.util.BundleUtility;
import org.osgi.framework.Bundle;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureTypeEnum;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupTypeEnum;
import de.darwinspl.feature.DwRootFeatureContainer;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.graphical.treeviewer.treeviewer.DwGraphicalTreeViewerEditor;
import de.darwinspl.temporal.util.DwEvolutionUtil;

public class DwGraphicalTreeViewerLabelProvider extends LabelProvider {
	DwGraphicalTreeViewerEditor editor;
	
	public DwGraphicalTreeViewerLabelProvider(DwGraphicalTreeViewerEditor editor) {
		this.editor = editor;
	}
	
	@Override
	public String getText(Object element) {
		String name;
		if(element instanceof DwFeature) {
			name = DwEvolutionUtil.getValidTemporalElement(((DwFeature) element).getNames(), editor.getCurrentSelectedDate()).getName();
			if(!name.isEmpty()) 
				return name;
			return "Feature";
		}
		else if(element instanceof DwRootFeatureContainer) {
			name = DwEvolutionUtil.getValidTemporalElement(((DwRootFeatureContainer) element).getFeature().getNames(), editor.getCurrentSelectedDate()).getName();
			if(!name.isEmpty()) 
				return name;
			return "RootFeature";
		}
		else if(element instanceof DwTemporalFeatureModel) {
			return "FeatureModel";
		} else if(element instanceof DwGroup) {
			return "Group";
		}
		return element.toString();
		
	}
	
	@SuppressWarnings("restriction")
	@Override
	public Image getImage(Object element) {
		String icon = "";
		if(element instanceof DwGroup) {
			DwGroupTypeEnum groupType = DwEvolutionUtil.getValidTemporalElement(((DwGroup) element).getTypes(), editor.getCurrentSelectedDate()).getType();
			if(groupType.getName().equals("AND")) {
				icon = "icons/ActionMakeGroupAnd.png";
			} else if(groupType.getName().equals("OR")) {
				icon = "icons/ActionMakeGroupOr.png";
			} else if(groupType.getName().equals("ALTERNATIVE")){
				icon = "icons/ActionMakeGroupAlternative.png";
			}
		} else if(element instanceof DwFeature || element instanceof DwRootFeatureContainer) {
			DwFeatureTypeEnum featureType;
			if(element instanceof DwFeature) {
				featureType = DwEvolutionUtil.getValidTemporalElement(((DwFeature) element).getTypes(), editor.getCurrentSelectedDate()).getType();
			} else {
				featureType = DwEvolutionUtil.getValidTemporalElement(((DwRootFeatureContainer) element).getFeature().getTypes(), editor.getCurrentSelectedDate()).getType();
			}
			if(featureType.getName().equals("MANDATORY")) {
				icon = "icons/ActionMakeFeatureMandatory.png";
			} else if(featureType.getName().equals("OPTIONAL")) {
				icon = "icons/ActionMakeFeatureOptional.png";	
			}
		}
		if (!(icon.equals(""))) {
			Bundle bundle = Platform.getBundle("de.darwinspl.feature.graphical.treeviewer");
			URL fullPathString = BundleUtility.find(bundle, icon);
			Image image = JFaceResources.getResources().createImage(ImageDescriptor.createFromURL(fullPathString));

			return image;
		}
		return null;
	}
}