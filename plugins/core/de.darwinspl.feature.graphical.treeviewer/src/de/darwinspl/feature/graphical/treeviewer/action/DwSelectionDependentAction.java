package de.darwinspl.feature.graphical.treeviewer.action;

import java.util.ArrayList;
import java.util.List;

import de.darwinspl.feature.DwRootFeatureContainer;
import de.darwinspl.feature.graphical.treeviewer.treeviewer.DwGraphicalTreeViewerEditor;
import de.darwinspl.properties.sources.DwTemporalElementPropertySource;

public abstract class DwSelectionDependentAction extends DwAction {
	
	private boolean allowMultipleSelection;
	DwGraphicalTreeViewerEditor editor;
	
	public DwSelectionDependentAction(DwGraphicalTreeViewerEditor editor) {
		this(editor, false);
	}
	
	public DwSelectionDependentAction(DwGraphicalTreeViewerEditor editor, boolean allowMultipleSelection) {
		super(editor);
		this.editor = editor;
		this.allowMultipleSelection = allowMultipleSelection;
		
		updateEnabledState();
	}
	
	@Override
	public void run() {
		List<Object> acceptedModels = getAcceptedModels();
		for (Object acceptedModel : acceptedModels) {
			execute(acceptedModel);
			
			if (!allowMultipleSelection) {
				//Only perform once
				return;
			}
		}
	}
	
	protected abstract void execute(Object acceptedModel);
		
	private List<Object> getAcceptedModels() {
		List<Object> acceptedModels = new ArrayList<Object>();
		if (((DwGraphicalTreeViewerEditor) editor).getSelection() != null) {
			Object selectedElement = ((DwGraphicalTreeViewerEditor) editor).getSelection().getFirstElement();
			if(selectedElement instanceof DwTemporalElementPropertySource) {
				selectedElement = ((DwTemporalElementPropertySource) selectedElement).getWrappedObject();
			}
			if(acceptsSelectedModel(selectedElement)) {
				if(selectedElement instanceof DwRootFeatureContainer) {
					acceptedModels.add(((DwRootFeatureContainer)selectedElement).getFeature());
				} else {
					acceptedModels.add(selectedElement);
				}
			}
		}
		return acceptedModels;
	}
	
	protected abstract boolean acceptsSelectedModel(Object selectedModel);

	@Override
	public void updateEnabledState() {
		boolean enabled = determineEnabledState();
		setEnabled(enabled);
	}
	
	protected boolean determineEnabledState() {
		List<Object> acceptedModels = getAcceptedModels();
		
		if (acceptedModels.isEmpty()) {
			return false;
		}
		
		return true;
	}

}
