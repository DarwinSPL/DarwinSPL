package de.darwinspl.feature.graphical.treeviewer.action;

import org.eclipse.emf.common.command.Command;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.graphical.treeviewer.commands.DwGroupMoveCommand;
import de.darwinspl.feature.graphical.treeviewer.treeviewer.DwGraphicalTreeViewerEditor;

public class DwGroupMoveAction extends DwCommandAction {
	
	DwFeature targetObject;
	DwGraphicalTreeViewerEditor editor;

	public DwGroupMoveAction(DwGraphicalTreeViewerEditor editor, DwFeature targetObject) {
		super(editor);
		this.targetObject = targetObject;
		this.editor = editor;
	}

	@Override
	protected Command createCommand(Object acceptedModel) {
		if(acceptedModel instanceof DwGroup) {
			return new DwGroupMoveCommand((DwGroup) acceptedModel, targetObject, editor);	
		}
		return null;
	}

	@Override
	protected boolean acceptsSelectedModel(Object selectedModel) {
		return true;
	}

	@Override
	protected String createText() {
		return null;
	}

	@Override
	protected String createID() {
		return null;
	}

}
