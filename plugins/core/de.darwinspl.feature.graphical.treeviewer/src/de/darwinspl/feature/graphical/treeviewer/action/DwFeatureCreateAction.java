package de.darwinspl.feature.graphical.treeviewer.action;

import java.util.Date;

import org.eclipse.emf.common.command.Command;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwRootFeatureContainer;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.graphical.treeviewer.commands.DwFeatureCreateCommand;
import de.darwinspl.feature.graphical.treeviewer.treeviewer.DwGraphicalTreeViewerEditor;
import de.darwinspl.feature.util.custom.DwFeatureUtil;
import de.darwinspl.properties.sources.DwTemporalElementPropertySource;
import de.darwinspl.temporal.DwTemporalElement;

public class DwFeatureCreateAction extends DwCommandAction {

public static final String ID = DwFeatureCreateAction.class.getCanonicalName();
	
	public DwFeatureCreateAction(DwGraphicalTreeViewerEditor editor) {
		super(editor);
	}

	@Override
	protected String createText() {
		return "Create Feature";
	}
	
	@Override
	protected String createID() {
		return ID;
	}
	
	@Override
	protected String createIconPath() {
		return "icons/ActionCreateFeature.png";
	}
	
	@Override
	protected Command createCommand(Object acceptedModel) {		
		if(acceptedModel instanceof DwTemporalElementPropertySource) {
			return new DwFeatureCreateCommand(((DwTemporalElementPropertySource) acceptedModel).getWrappedObject(), getEditor(), false);			
		}
		return new DwFeatureCreateCommand(acceptedModel, getEditor(), false);			
	}

	@Override
	protected boolean acceptsSelectedModel(Object selectedModel) {
		if (selectedModel instanceof DwFeature || selectedModel instanceof DwGroup || selectedModel instanceof DwRootFeatureContainer) {
			return true;
		}
		
		if (selectedModel instanceof DwTemporalFeatureModel) {
			DwTemporalFeatureModel featureModel = (DwTemporalFeatureModel) selectedModel;
			
			Date date = this.getDateFromEditor();
			DwFeature rootFeature = DwFeatureUtil.getRootFeature(featureModel, date);
			
			if (rootFeature == null) {
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public void updateEnabledState() {
		super.updateEnabledState();
	}

}
