package de.darwinspl.feature.graphical.treeviewer.provider;

import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emfforms.spi.swt.treemasterdetail.DNDProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerDropAdapter;
import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.DropTargetListener;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;

import de.darwinspl.feature.graphical.treeviewer.action.DwFeatureMoveAction;
import de.darwinspl.feature.graphical.treeviewer.action.DwGroupMoveAction;
import de.darwinspl.feature.graphical.treeviewer.treeviewer.DwGraphicalTreeViewer;
import de.darwinspl.feature.impl.DwFeatureImpl;
import de.darwinspl.feature.impl.DwGroupImpl;
import de.darwinspl.feature.util.custom.DwFeatureUtil;
import de.darwinspl.temporal.DwTemporalElement;

public class DwGraphicalTreeViewerDNDProvider implements DNDProvider {
	
	Transfer[] transferTypes;
	
	public DwGraphicalTreeViewerDNDProvider() {
		transferTypes = new Transfer[] { TextTransfer.getInstance() };
	}

	@Override
	public boolean hasDND() {
		return true;
	}

	@Override
	public int getDragOperations() {
		return 0;
	}

	@Override
	public Transfer[] getDragTransferTypes() {
		return transferTypes;
	}

	@Override
	public DragSourceListener getDragListener(TreeViewer treeViewer) {
		return new NodeDragListener(treeViewer);
	}

	@Override
	public int getDropOperations() {
		return 0;
	}

	@Override
	public Transfer[] getDropTransferTypes() {
		return transferTypes;
	}

	@Override
	public DropTargetListener getDropListener(EditingDomain editingDomain, TreeViewer treeViewer) {
		return new NodeDropListener(treeViewer);
	}

}

class NodeDropListener extends ViewerDropAdapter {
	
	private TreeViewer treeViewer;
	private DwTemporalElement target;
	private int operation;
	
	public NodeDropListener(TreeViewer treeViewer) {
		super(treeViewer);
		this.treeViewer = treeViewer;
	}

	@Override
	public void drop(DropTargetEvent event) {
		
		if (event.data instanceof String) {
			operation = this.determineLocation(event);
			DwTemporalElement dropedNode = (DwTemporalElement) ((TreeSelection) treeViewer.getSelection()).getFirstElement();
			if (dropedNode instanceof DwFeatureImpl) {
				switch (this.operation) {
				case ViewerDropAdapter.LOCATION_BEFORE:
					if(target instanceof DwGroupImpl) {
						break;
					}
					target = DwFeatureUtil.getParentGroupOfFeature((DwFeatureImpl) target, ((DwGraphicalTreeViewer) treeViewer).getEditor().getCurrentSelectedDate());
				case ViewerDropAdapter.LOCATION_AFTER:
				case ViewerDropAdapter.LOCATION_ON:
					if(target instanceof DwFeatureImpl) {
						break;
					}
					if(!DwFeatureUtil.getParentGroupOfFeature((DwFeatureImpl) dropedNode, ((DwGraphicalTreeViewer) treeViewer).getEditor().getCurrentSelectedDate()).equals(target)) {
						if(treeViewer instanceof DwGraphicalTreeViewer) {
							DwFeatureMoveAction moveAction = new DwFeatureMoveAction(((DwGraphicalTreeViewer) treeViewer).getEditor(), target);							
							moveAction.run();
						}
					}
					break;
				case ViewerDropAdapter.LOCATION_NONE:
					break;
				default:
					break;
				}
			} else if (dropedNode instanceof DwGroupImpl) {
				switch(this.operation) {
				case ViewerDropAdapter.LOCATION_BEFORE:
					if(target instanceof DwFeatureImpl) {
						break;
					}
					target = DwFeatureUtil.getParentFeatureOfGroup((DwGroupImpl) target, ((DwGraphicalTreeViewer) treeViewer).getEditor().getCurrentSelectedDate());
				case ViewerDropAdapter.LOCATION_AFTER:
				case ViewerDropAdapter.LOCATION_ON:
					if(target instanceof DwGroupImpl) {
						break;
					}
					if(treeViewer instanceof DwGraphicalTreeViewer) {
						if(!DwFeatureUtil.getChildGroupsOfFeature((DwFeatureImpl)target, ((DwGraphicalTreeViewer) treeViewer).getEditor().getCurrentSelectedDate()).contains(dropedNode)) {
							if(target instanceof DwFeatureImpl) {
								DwGroupMoveAction moveAction = new DwGroupMoveAction(((DwGraphicalTreeViewer) treeViewer).getEditor(), (DwFeatureImpl) target);								
								moveAction.run();
							}
						}						
					}
					break;
				case ViewerDropAdapter.LOCATION_NONE:
					break;
				default:
					break;
				}
			}
		}
		super.drop(event);
		((DwGraphicalTreeViewer) treeViewer).getEditor().updateTree();;
	}
	
	@Override
	public boolean performDrop(Object data) {
		return true;
	}

	@Override
	public boolean validateDrop(Object target, int operation, TransferData transferType) {
		if(target instanceof DwTemporalElement) {
			this.target = (DwTemporalElement) target;
			this.operation = operation;
			return true;
		}
		return false;
	}
}

class NodeDragListener extends DragSourceAdapter {
	
	private TreeViewer treeViewer;
	
	public NodeDragListener(TreeViewer treeViewer) {
		this.treeViewer = treeViewer;
	}
	
	@Override
	public void dragSetData(DragSourceEvent event) {
		ISelection selection= treeViewer.getSelection();
		
		if(selection instanceof TreeSelection) {
			TreeSelection treeSelection = (TreeSelection) selection;
			event.data = treeSelection.getFirstElement().toString();
		} else {
			event.doit = false;
		}
	}
}