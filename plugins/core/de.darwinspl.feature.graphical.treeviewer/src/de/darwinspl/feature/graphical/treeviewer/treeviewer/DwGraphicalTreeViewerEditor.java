package de.darwinspl.feature.graphical.treeviewer.treeviewer;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.Date;
import java.util.EventObject;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.command.BasicCommandStack;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.common.command.CommandStackListener;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecp.ui.view.swt.ECPSWTView;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory;
import org.eclipse.emf.edit.provider.resource.ResourceItemProviderAdapterFactory;
import org.eclipse.emf.edit.ui.action.EditingDomainActionBarContributor;
import org.eclipse.emfforms.spi.swt.treemasterdetail.TreeMasterDetailComposite;
import org.eclipse.emfforms.spi.swt.treemasterdetail.TreeMasterDetailSWTBuilder;
import org.eclipse.emfforms.spi.swt.treemasterdetail.TreeMasterDetailSWTFactory;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.dialogs.SaveAsDialog;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.part.FileEditorInput;

import de.christophseidl.util.ecore.EcoreIOUtil;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.graphical.base.editor.DwBasicGraphicalEditorBase;
import de.darwinspl.feature.graphical.base.editor.DwGraphicalViewer;
import de.darwinspl.feature.graphical.base.slider.DwDateChangeListener;
import de.darwinspl.feature.graphical.base.slider.DwGraphicalModelDateScale;
import de.darwinspl.feature.graphical.treeviewer.provider.DwGraphicalTreeViewerContentProvider;
import de.darwinspl.feature.graphical.treeviewer.provider.DwGraphicalTreeViewerContextMenuProvider;
import de.darwinspl.feature.graphical.treeviewer.provider.DwGraphicalTreeViewerDNDProvider;
import de.darwinspl.feature.graphical.treeviewer.provider.DwGraphicalTreeViewerLabelProvider;
import de.darwinspl.feature.impl.custom.DwFeatureModelResource;
import de.darwinspl.temporal.util.DwEvolutionUtil;

public class DwGraphicalTreeViewerEditor extends EditorPart implements DwDateChangeListener, PropertyChangeListener, IEditingDomainProvider, DwBasicGraphicalEditorBase {

	protected AdapterFactoryEditingDomain editingDomain;
	protected ComposedAdapterFactory adapterFactory;
	private DwTemporalFeatureModel tfm;
	private IStructuredSelection treeSelection;
	private DwTreeViewerBuilder tree;
	private TreeMasterDetailSWTBuilder builder;
	private CommandStack commandStack;
	
	List<Date> dates;
	Date currentSelectedDate;
	boolean lastDateSelected;

	DwGraphicalModelDateScale scale;
	TreeMasterDetailComposite renderedComposite;
	ComposedAdapterFactory composedAdapterFactory;
	ECPSWTView renderedViewComposite;
	ActionRegistry actionRegistry;
	
	public DwGraphicalTreeViewerEditor() {
		super();
	}
	
	public Date getCurrentSelectedDate() {
		if(scale == null) {
			return DwEvolutionUtil.getClosestEvolutionDate(tfm, new Date());
		}
		return scale.getCurrentSelectedDate();
	}

	public void executeCommand(Command command) {
		commandStack = editingDomain.getCommandStack();
		commandStack.execute(command);
	}
	
	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		setSite(site);
		setInput(input);
		setPartName(input.getName());
		
		initializeEditingDomain();
		
		if (input instanceof IFileEditorInput) {
			IFileEditorInput fileInput = (IFileEditorInput) input;
			IFile file = fileInput.getFile();
			try {
				loadContent(file);
			} catch (IOException e) {
				
			}
		}
	}
	
	public ActionRegistry getActionRegistry() {
		if (actionRegistry == null)
			actionRegistry = new ActionRegistry();
		return actionRegistry;	
	}

	private void loadContent(IFile file) throws IOException {
		tfm = EcoreIOUtil.loadModel(file);
	}
	
	
	private void initializeEditingDomain() {
		adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

		adapterFactory.addAdapterFactory(new ResourceItemProviderAdapterFactory());
		adapterFactory.addAdapterFactory(new ReflectiveItemProviderAdapterFactory());

		commandStack = new BasicCommandStack();

		commandStack.addCommandStackListener(new CommandStackListener() {
			public void commandStackChanged(final EventObject event) {
				getSite().getShell().getDisplay().asyncExec(new Runnable() {
					public void run() {
						firePropertyChange(IEditorPart.PROP_DIRTY);
					}
				});
			}
		});

		editingDomain = new AdapterFactoryEditingDomain(adapterFactory, commandStack);		
	}
	
	
	@Override
	public boolean isDirty() {
		return ((BasicCommandStack) editingDomain.getCommandStack()).isSaveNeeded();
	}

	@Override
	public boolean isSaveAsAllowed() {
		return true;
	}

	@Override
	public void createPartControl(Composite parent) {
		final Composite content = new Composite(parent, SWT.NONE);
		content.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		content.setLayout(new GridLayout(1, false));

		scale = new DwGraphicalModelDateScale(content);
		
		scale.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		scale.setModel(tfm);
		scale.addDateChangeListener(this);
		
		builder = TreeMasterDetailSWTFactory.fillDefaults(content, SWT.NONE,
				tfm.eResource());

		tree = new DwTreeViewerBuilder(this);
		
		builder.customizeTree(tree);
		
		builder.customizeMenu(new DwGraphicalTreeViewerContextMenuProvider(this));
		
		builder.customizeContentProvider(new DwGraphicalTreeViewerContentProvider(this));
		builder.customizeLabelProvider(new DwGraphicalTreeViewerLabelProvider(this));
		
		DwGraphicalTreeViewerDNDProvider dnd = new DwGraphicalTreeViewerDNDProvider();
		builder.customizeDragAndDrop(dnd);
		
		renderedComposite = builder.create();
		renderedComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		getSite().setSelectionProvider(renderedComposite.getSelectionProvider());

	}
	
	public TreeViewer getTree() {
		return tree.getViewer();
	}
	
	public String getValidNewFeatureName() {
		return "New Feature";
	}
	
	public void updateTree() {
		try {
			renderedComposite.getSelectionProvider().refresh();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setSelection(ISelection iSelection) {
		if(iSelection instanceof IStructuredSelection)
			this.treeSelection = (IStructuredSelection) iSelection;
	}
	
	public IStructuredSelection getSelection() {
		return treeSelection;
	}
	
	public DwGraphicalModelDateScale getScale() {
		return scale;
	}

	protected IResourceChangeListener resourceChangeListener = new IResourceChangeListener() {
		
		@Override
		public void resourceChanged(IResourceChangeEvent event) {
			Resource resource = editingDomain.getResourceSet().getResources().get(0);
			resource.unload();
			try {
				resource.load(null);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	};

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getNewValue() instanceof Date) {
			this.currentSelectedDate = (Date) evt.getNewValue();
			try {
				renderedComposite.getSelectionProvider().refresh();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void setFocus() {
		
	}
	@Override
	public EditingDomain getEditingDomain() {
		return editingDomain;
	}

	public DwTemporalFeatureModel getFeatureModel() {
		return tfm;
	}

	public boolean isLastDateSelected() {
		if (getCurrentSelectedDate() != null) {
			return scale.isLastDateSelected();
		}
		
		return false;
	}
	
	
	@Override
	public void doSave(IProgressMonitor monitor) {
		try {
			tfm.eResource().save(null);
			((BasicCommandStack) editingDomain.getCommandStack()).saveIsDone();
			firePropertyChange(IEditorPart.PROP_DIRTY);

			scale.doUpdate();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}

	@Override
	public void doSaveAs() {
		SaveAsDialog saveAsDialog = new SaveAsDialog(getSite().getShell());
		saveAsDialog.open();
		IPath path = saveAsDialog.getResult();
		if (path != null) {
			IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(path);
			if (file != null) {
				doSaveAs(URI.createPlatformResourceURI(file.getFullPath().toString(), true), new FileEditorInput(file));
			}
		}		
	}
	
	protected void doSaveAs(URI uri, IEditorInput editorInput) {
		(editingDomain.getResourceSet().getResources().get(0)).setURI(uri);
		setInputWithNotify(editorInput);
		setPartName(editorInput.getName());
		IProgressMonitor progressMonitor = getActionBars().getStatusLineManager() != null
				? getActionBars().getStatusLineManager().getProgressMonitor()
				: new NullProgressMonitor();
		doSave(progressMonitor);
	}
	
	public IActionBars getActionBars() {
		return getActionBarContributor().getActionBars();
	}

	public EditingDomainActionBarContributor getActionBarContributor() {
		return (EditingDomainActionBarContributor) getEditorSite().getActionBarContributor();
	}

	@Override
	public void dateChanged(Date newDate, Date oldDate, Object source) {
		updateTree();
	}

	@Override
	public DwGraphicalViewer getViewer() {
		return null;
	}
	
	@Override
	public void dispose() {
		super.dispose();
		DwFeatureModelResource res = (DwFeatureModelResource) this.getFeatureModel().eResource();
		if(res != null)
			res.discard();
	}

}
