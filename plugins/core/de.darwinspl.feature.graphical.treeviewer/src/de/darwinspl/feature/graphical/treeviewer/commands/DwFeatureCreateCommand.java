package de.darwinspl.feature.graphical.treeviewer.commands;

import org.eclipse.emf.ecore.EObject;

import de.darwinspl.feature.evolution.operation.factory.DwSimpleEvolutionOperationFactory;
import de.darwinspl.feature.evolution.operation.interpreter.DwEvolutionOperationInterpreter;
import de.darwinspl.feature.graphical.treeviewer.treeviewer.DwGraphicalTreeViewerEditor;
import de.darwinspl.feature.operation.DwFeatureCreateOperation;

public class DwFeatureCreateCommand extends DwCommand {
	
	DwFeatureCreateOperation createEvoOp;
	DwGraphicalTreeViewerEditor editor;

	public DwFeatureCreateCommand(Object parentObject, DwGraphicalTreeViewerEditor editor) {
		init(parentObject, editor, false);
		this.editor = editor;
	}
	
	public DwFeatureCreateCommand(Object parentObject, DwGraphicalTreeViewerEditor editor, boolean forceCreateParentGroup) {
		init(parentObject, editor, forceCreateParentGroup);
		this.editor = editor;
	}

	private void init(Object parentObject, DwGraphicalTreeViewerEditor editor, boolean forceCreateParentGroup) {
		DwSimpleEvolutionOperationFactory operationFactory = new DwSimpleEvolutionOperationFactory();
		createEvoOp = operationFactory.createDwFeatureCreateOperation(editor.getFeatureModel(), (EObject) parentObject, forceCreateParentGroup, editor.getCurrentSelectedDate(), editor.isLastDateSelected());
	}
	
	@Override
	public void execute() {
		DwEvolutionOperationInterpreter.execute(createEvoOp);
		editor.updateTree();
	}

	@Override
	public void undo() {
		DwEvolutionOperationInterpreter.undo(createEvoOp);
		editor.updateTree();
	}

}
