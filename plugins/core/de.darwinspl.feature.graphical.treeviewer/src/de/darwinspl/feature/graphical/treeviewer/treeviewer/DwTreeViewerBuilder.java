package de.darwinspl.feature.graphical.treeviewer.treeviewer;

import org.eclipse.emfforms.spi.swt.treemasterdetail.TreeViewerBuilder;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.widgets.Composite;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.impl.DwFeatureImpl;
import de.darwinspl.properties.sources.DwTemporalElementPropertySource;
import de.darwinspl.temporal.DwTemporalElement;


public class DwTreeViewerBuilder implements TreeViewerBuilder {

	private boolean canBeChanged;
	private DwGraphicalTreeViewerEditor editor;
	private TreeViewer viewer;
			
	public DwTreeViewerBuilder(DwGraphicalTreeViewerEditor editor) {
		this.editor = editor;
	}
	
	@Override
	public TreeViewer createTree(Composite parent) {
		viewer = new DwGraphicalTreeViewer(parent, editor);
		viewer.setComparator(new TreeViewerViewerComparator());
		ISelectionChangedListener selectionChangedListener =
				new ISelectionChangedListener() {
					public void selectionChanged(SelectionChangedEvent selectionChangedEvent) {
						editor.setSelection(selectionChangedEvent.getSelection());
					}

				};
		viewer.addSelectionChangedListener(selectionChangedListener);
		allowDirectEditing();
		return viewer;
	}
	
	
	private void allowDirectEditing() {

		viewer.getTree().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent event) {
				if(event.keyCode == SWT.F2) {
					canBeChanged = true; 
					IStructuredSelection selectedElement = (IStructuredSelection) viewer.getSelection();
					if(selectedElement instanceof TreeSelection) {
						if(selectedElement.getFirstElement() instanceof DwTemporalElementPropertySource) {	
							DwTemporalElement firstElement = ((DwTemporalElementPropertySource)selectedElement.getFirstElement()).getWrappedObject();
							viewer.editElement(firstElement, 0);
						}
					}
					canBeChanged = false;

				}
			}
		});
		
		final TreeViewerColumn column = new TreeViewerColumn(viewer, SWT.NONE);
		column.getColumn().setWidth(50000);
		final TextCellEditor cellEditor = new TextCellEditor(viewer.getTree());
		
		column.setEditingSupport(new EditingSupport(viewer) {
			
			@Override
			protected void setValue(Object element, Object value) {
			}
			
			@Override
			protected Object getValue(Object element) {
				return element.toString();
			}
			
			@Override
			protected TextCellEditor getCellEditor(Object element) {
				return cellEditor;
			}
			
			@Override
			protected boolean canEdit(Object element) {
				if(element instanceof DwFeatureImpl) {
					return canBeChanged;
				}
				return false;
			}
		});
	}

	public TreeViewer getViewer() {
		return viewer;
	}

}

class TreeViewerViewerComparator extends ViewerComparator {
	@Override
	public int category(Object element) {
		if (element instanceof DwFeature) {
			return 1;
		} 
		return 0;
	}

}