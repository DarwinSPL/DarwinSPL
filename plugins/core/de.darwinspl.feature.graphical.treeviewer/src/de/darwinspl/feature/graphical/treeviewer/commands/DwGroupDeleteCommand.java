package de.darwinspl.feature.graphical.treeviewer.commands;

import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.evolution.operation.factory.DwSimpleEvolutionOperationFactory;
import de.darwinspl.feature.evolution.operation.interpreter.DwEvolutionOperationInterpreter;
import de.darwinspl.feature.graphical.treeviewer.treeviewer.DwGraphicalTreeViewerEditor;
import de.darwinspl.feature.operation.DwGroupDeleteOperation;

public class DwGroupDeleteCommand extends DwCommand {

	DwGroupDeleteOperation deleteEvoOp;

	public DwGroupDeleteCommand(DwGroup group, DwGraphicalTreeViewerEditor editor) {
		DwSimpleEvolutionOperationFactory operationFactory = new DwSimpleEvolutionOperationFactory();
		this.deleteEvoOp = operationFactory.createDwGroupDeleteOperation(group.getFeatureModel(), group, editor.getCurrentSelectedDate(), true);
		this.editor = editor;
	}

	@Override
	public void execute() {
		DwEvolutionOperationInterpreter.execute(deleteEvoOp);
		editor.updateTree();
	}

	@Override
	public void undo() {
		DwEvolutionOperationInterpreter.undo(deleteEvoOp);
		editor.updateTree();
	}

}
