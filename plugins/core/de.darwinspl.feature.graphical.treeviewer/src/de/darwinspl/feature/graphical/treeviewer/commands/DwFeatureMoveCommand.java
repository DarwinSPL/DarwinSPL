package de.darwinspl.feature.graphical.treeviewer.commands;

import org.eclipse.emf.ecore.EObject;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.evolution.operation.factory.DwSimpleEvolutionOperationFactory;
import de.darwinspl.feature.evolution.operation.interpreter.DwEvolutionOperationInterpreter;
import de.darwinspl.feature.graphical.treeviewer.treeviewer.DwGraphicalTreeViewerEditor;
import de.darwinspl.feature.operation.DwFeatureMoveOperation;

public class DwFeatureMoveCommand extends DwCommand {
	
	DwFeatureMoveOperation moveEvoOp;
	DwGraphicalTreeViewerEditor editor;
	
	public DwFeatureMoveCommand(DwFeature feature, Object targetObject, DwGraphicalTreeViewerEditor editor) {
		this.editor = editor;
		DwSimpleEvolutionOperationFactory operationFactory = new DwSimpleEvolutionOperationFactory();
		moveEvoOp = operationFactory.createDwFeatureMoveOperation(feature.getFeatureModel(), feature, (EObject) targetObject, editor.getCurrentSelectedDate(), editor.isLastDateSelected());
	}
	
	@Override
	public void execute() {
		DwEvolutionOperationInterpreter.execute(moveEvoOp);
		editor.updateTree();
	}

	@Override
	public void undo() {
		DwEvolutionOperationInterpreter.undo(moveEvoOp);
		editor.updateTree();
	}

}