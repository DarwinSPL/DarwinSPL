package de.darwinspl.feature.graphical.treeviewer.action;

import java.util.Date;
import java.util.List;

import org.eclipse.emf.common.command.Command;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwRootFeatureContainer;
import de.darwinspl.feature.graphical.treeviewer.commands.DwFeatureCreateCommand;
import de.darwinspl.feature.graphical.treeviewer.treeviewer.DwGraphicalTreeViewerEditor;
import de.darwinspl.feature.util.custom.DwFeatureUtil;

public class DwFeatureCreateInNewGroupAction extends DwCommandAction {
	
	public static final String ID = DwFeatureCreateInNewGroupAction.class.getCanonicalName();
	
	public DwFeatureCreateInNewGroupAction(DwGraphicalTreeViewerEditor editor) {
		super(editor);
	}

	@Override
	protected String createText() {
		return "Create Feature In New Group";
	}
	
	@Override
	protected String createID() {
		return ID;
	}
	
	@Override
	protected String createIconPath() {
		return "icons/ActionCreateFeatureInNewGroup.png";
	}
	
	@Override
	protected boolean acceptsSelectedModel(Object selectedModel) {
		if (selectedModel instanceof DwFeature || selectedModel instanceof DwRootFeatureContainer) {
			DwFeature feature;
			if(selectedModel instanceof DwFeature) {
			    feature = (DwFeature) selectedModel;
			} else {
				feature = ((DwRootFeatureContainer) selectedModel).getFeature();
			}

			Date date = this.getDateFromEditor();
			List<DwGroup> groups = DwFeatureUtil.getChildGroupsOfFeature(feature, date);
			
			if (!groups.isEmpty()) {
				return true;
			}
		}
		
		if (selectedModel instanceof DwGroup) {
			return true;
		}
		
		return false;
	}
	
	@Override
	protected Command createCommand(Object acceptedModel) {
		return new DwFeatureCreateCommand(acceptedModel, getEditor(), true);
	}
}
