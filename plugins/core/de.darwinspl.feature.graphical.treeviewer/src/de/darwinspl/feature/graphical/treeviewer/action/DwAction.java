package de.darwinspl.feature.graphical.treeviewer.action;

import java.util.Date;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;

import de.christophseidl.util.eclipse.ui.JFaceUtil;
import de.darwinspl.feature.graphical.base.editor.DwBasicGraphicalEditorBase;
import de.darwinspl.feature.graphical.treeviewer.treeviewer.DwGraphicalTreeViewerEditor;

public abstract class DwAction extends Action {
	private DwGraphicalTreeViewerEditor editor;
	
	public DwAction(DwGraphicalTreeViewerEditor editor) {
		this.editor = editor;
		
		initialize();
		registerListeners();
	}
	
	private void initialize() {
		setId(createID());
		setText(createText());
		
		String iconPath = createIconPath();
		
		if (iconPath != null) {
			ImageDescriptor imageDescriptor = JFaceUtil.getImageDescriptorFromClassBundle(iconPath, getClass());
			setImageDescriptor(imageDescriptor);
		}
	}
	
	private void registerListeners() {
		TreeViewer viewer = editor.getTree();

		viewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				updateEnabledState();
			}
		});
	}
	
	protected abstract String createText();
	protected abstract String createID();
	
	protected String createIconPath() {
		return null;
	}
	
	public void updateEnabledState() {
	}
	
	@Override
	public abstract void run();
	
	protected DwGraphicalTreeViewerEditor getEditor() {
		return editor;
	}
	
	protected Date getDateFromEditor() {
		return this.editor.getCurrentSelectedDate();
	}

}
