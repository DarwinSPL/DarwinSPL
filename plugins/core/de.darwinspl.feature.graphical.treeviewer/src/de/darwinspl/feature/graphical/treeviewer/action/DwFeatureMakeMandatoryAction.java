package de.darwinspl.feature.graphical.treeviewer.action;

import java.util.Date;

import org.eclipse.emf.common.command.Command;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureTypeEnum;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.graphical.treeviewer.commands.DwFeatureChangeVariationTypeCommand;
import de.darwinspl.feature.graphical.treeviewer.treeviewer.DwGraphicalTreeViewerEditor;
import de.darwinspl.feature.util.custom.DwFeatureUtil;

public class DwFeatureMakeMandatoryAction extends DwCommandAction {
	public static final String ID = DwFeatureMakeMandatoryAction.class.getCanonicalName();
	
	public DwFeatureMakeMandatoryAction(DwGraphicalTreeViewerEditor editor) {
		super(editor);
	}
	
	@Override
	protected String createText() {
		return "Make Feature Mandatory";
	}

	@Override
	protected String createID() {
		return ID;
	}
	
	@Override
	protected String createIconPath() {
		return "icons/ActionMakeFeatureMandatory.png";
	}
	
	@Override
	protected boolean acceptsSelectedModel(Object selectedModel) {
		Date date = this.getDateFromEditor();
		
		if (selectedModel instanceof DwFeature) {
			DwFeature feature = (DwFeature) selectedModel;
			DwGroup parentGroup = DwFeatureUtil.getParentGroupOfFeature(feature, date);
			
			if (!DwFeatureUtil.isMandatory(feature, date) && !DwFeatureUtil.isRootFeature(feature, date) && DwFeatureUtil.isAnd(parentGroup, date)) {
				return true;
			}
		}
		return false;
	}

	@Override
	protected Command createCommand(Object acceptedModel) {
		DwFeature feature = (DwFeature) acceptedModel;
		return new DwFeatureChangeVariationTypeCommand(feature, DwFeatureTypeEnum.MANDATORY, getEditor());
	}

}
