package de.darwinspl.feature.graphical.treeviewer.commands;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.evolution.operation.factory.DwSimpleEvolutionOperationFactory;
import de.darwinspl.feature.evolution.operation.interpreter.DwEvolutionOperationInterpreter;
import de.darwinspl.feature.graphical.treeviewer.treeviewer.DwGraphicalTreeViewerEditor;
import de.darwinspl.feature.operation.DwGroupMoveOperation;

public class DwGroupMoveCommand extends DwCommand {

	DwGroupMoveOperation moveEvoOp;
	DwGraphicalTreeViewerEditor editor;
	
	public DwGroupMoveCommand(DwGroup group, DwFeature targetObject, DwGraphicalTreeViewerEditor editor) {
		DwSimpleEvolutionOperationFactory operationFactory = new DwSimpleEvolutionOperationFactory();
		moveEvoOp = operationFactory.createDwGroupMoveOperation(group.getFeatureModel(), group, targetObject, editor.getCurrentSelectedDate(), editor.isLastDateSelected());
		this.editor = editor;
	}
	
	@Override
	public void execute() {
		DwEvolutionOperationInterpreter.execute(moveEvoOp);
		editor.updateTree();
	}

	@Override
	public void undo() {
		DwEvolutionOperationInterpreter.undo(moveEvoOp);
		editor.updateTree();
	}

}
