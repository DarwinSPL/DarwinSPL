package de.darwinspl.feature.graphical.treeviewer.action;

import java.util.Date;

import org.eclipse.emf.common.command.Command;

import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupTypeEnum;
import de.darwinspl.feature.graphical.treeviewer.commands.DwGroupChangeVariationTypeCommand;
import de.darwinspl.feature.graphical.treeviewer.treeviewer.DwGraphicalTreeViewerEditor;

public abstract class DwAbstractGroupTypeChangeAction extends DwCommandAction {
	
	public DwAbstractGroupTypeChangeAction(DwGraphicalTreeViewerEditor editor) {
		super(editor);
	}
	
	protected abstract String getGroupTypeText();
	
	@Override
	protected String createText() {
		return "Make Group "+getGroupTypeText();
	}

	@Override
	protected abstract String createID();
	
	@Override
	protected abstract String createIconPath();
	
	@Override
	protected boolean acceptsSelectedModel(Object selectedModel) {
		if (selectedModel instanceof DwGroup) {
			DwGroup group = (DwGroup) selectedModel;
			
			Date date = this.getDateFromEditor();
			if (!checkIfGroupTypeIsAlreadySet(group, date)) {
				return true;
			}
			
		}
		return false;
	}
	
	protected abstract boolean checkIfGroupTypeIsAlreadySet(DwGroup group, Date date);

	@Override
	protected Command createCommand(Object acceptedModel) {
		DwGroup group = (DwGroup) acceptedModel;
		return new DwGroupChangeVariationTypeCommand(group, getTargetGroupTypeEnum(), getEditor());
	}
	
	protected abstract DwGroupTypeEnum getTargetGroupTypeEnum();

}
