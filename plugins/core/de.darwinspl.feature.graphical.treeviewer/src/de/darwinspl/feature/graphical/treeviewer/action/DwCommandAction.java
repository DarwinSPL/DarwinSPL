package de.darwinspl.feature.graphical.treeviewer.action;

import org.eclipse.emf.common.command.Command;

import de.darwinspl.feature.graphical.treeviewer.treeviewer.DwGraphicalTreeViewerEditor;

public abstract class DwCommandAction extends DwSelectionDependentAction {
	
	public DwCommandAction(DwGraphicalTreeViewerEditor editor, boolean allowMultipleSelection) {
		super(editor, allowMultipleSelection);
	}

	public DwCommandAction(DwGraphicalTreeViewerEditor editor) {
		super(editor);
	}
	
	@Override
	protected void execute(Object acceptedModel) {
		Command command = createCommand(acceptedModel);
		
		DwGraphicalTreeViewerEditor editor = getEditor();
		editor.executeCommand(command);
	}
	
	protected abstract Command createCommand(Object acceptedModel);

}