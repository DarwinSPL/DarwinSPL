package de.darwinspl.feature.graphical.treeviewer.action;

import java.util.Date;

import org.eclipse.emf.common.command.Command;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureTypeEnum;
import de.darwinspl.feature.graphical.treeviewer.commands.DwFeatureChangeVariationTypeCommand;
import de.darwinspl.feature.graphical.treeviewer.treeviewer.DwGraphicalTreeViewerEditor;
import de.darwinspl.feature.util.custom.DwFeatureUtil;

public class DwFeatureMakeOptionalAction extends DwCommandAction {
	public static final String ID = DwFeatureMakeOptionalAction.class.getCanonicalName();
	
	public DwFeatureMakeOptionalAction(DwGraphicalTreeViewerEditor editor) {
		super(editor);
	}
	
	@Override
	protected String createText() {
		return "Make Feature Optional";
	}

	@Override
	protected String createID() {
		return ID;
	}
	
	@Override
	protected String createIconPath() {
		return "icons/ActionMakeFeatureOptional.png";
	}
	
	@Override
	protected boolean acceptsSelectedModel(Object selectedModel) {
		if (selectedModel instanceof DwFeature) {
			DwFeature feature = (DwFeature) selectedModel;

			Date date = this.getDateFromEditor();
			
			if (!DwFeatureUtil.isOptional(feature, date) && !DwFeatureUtil.isRootFeature(feature, date)) {
				return true;
			}
			
		}
		return false;
	}

	@Override
	protected Command createCommand(Object acceptedModel) {
		DwFeature feature = (DwFeature) acceptedModel;
		return new DwFeatureChangeVariationTypeCommand(feature, DwFeatureTypeEnum.OPTIONAL, getEditor());
	}
}
