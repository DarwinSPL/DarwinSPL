package de.darwinspl.feature.graphical.treeviewer.action;

import java.util.Date;

import org.eclipse.emf.common.command.Command;

import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupTypeEnum;
import de.darwinspl.feature.graphical.treeviewer.commands.DwGroupChangeVariationTypeCommand;
import de.darwinspl.feature.graphical.treeviewer.treeviewer.DwGraphicalTreeViewerEditor;
import de.darwinspl.feature.util.custom.DwFeatureUtil;

public class DwGroupMakeAndAction extends DwCommandAction {
	public static final String ID = DwGroupMakeAndAction.class.getCanonicalName();
	
	public DwGroupMakeAndAction(DwGraphicalTreeViewerEditor editor) {
		super(editor);
	}
	
	@Override
	protected String createText() {
		return "Make Group And";
	}

	@Override
	protected String createID() {
		return ID;
	}
	
	@Override
	protected String createIconPath() {
		return "icons/ActionMakeGroupAnd.png";
	}
	
	@Override
	protected boolean acceptsSelectedModel(Object selectedModel) {
		Date date = this.getDateFromEditor();
		
		if (selectedModel instanceof DwGroup) {
			DwGroup group = (DwGroup) selectedModel;
			
			if (!DwFeatureUtil.isAnd(group, date)) {
				return true;
			}
			
		}
		return false;
	}

	@Override
	protected Command createCommand(Object acceptedModel) {
		DwGroup group = (DwGroup) acceptedModel;
		return new DwGroupChangeVariationTypeCommand(group, DwGroupTypeEnum.AND, getEditor());
	}

}

