package de.darwinspl.feature.graphical.treeviewer.action;

import org.eclipse.emf.common.command.Command;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.graphical.treeviewer.commands.DwFeatureMoveCommand;
import de.darwinspl.feature.graphical.treeviewer.treeviewer.DwGraphicalTreeViewerEditor;

public class DwFeatureMoveAction extends DwCommandAction{

	DwGraphicalTreeViewerEditor editor;
	Object targetObject;
	
	public DwFeatureMoveAction(DwGraphicalTreeViewerEditor editor, Object targetObject) {
		super(editor);
		this.editor = editor;
		this.targetObject = targetObject;
	}

	@Override
	protected Command createCommand(Object acceptedModel) {
		if(acceptedModel instanceof DwFeature) {
			return new DwFeatureMoveCommand((DwFeature) acceptedModel, targetObject, editor);	
		}
		return null;
	}

	@Override
	protected boolean acceptsSelectedModel(Object selectedModel) {
		return true;
	}

	@Override
	protected String createText() {
		return null;
	}

	@Override
	protected String createID() {
		return null;
	}

}
