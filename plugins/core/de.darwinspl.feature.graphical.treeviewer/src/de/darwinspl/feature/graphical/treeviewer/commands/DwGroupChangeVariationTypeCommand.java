package de.darwinspl.feature.graphical.treeviewer.commands;

import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupTypeEnum;
import de.darwinspl.feature.evolution.operation.factory.DwSimpleEvolutionOperationFactory;
import de.darwinspl.feature.evolution.operation.interpreter.DwEvolutionOperationInterpreter;
import de.darwinspl.feature.graphical.treeviewer.treeviewer.DwGraphicalTreeViewerEditor;
import de.darwinspl.feature.operation.DwGroupTypeChangeOperation;

public class DwGroupChangeVariationTypeCommand extends DwCommand {

	DwGroupTypeChangeOperation evoOp;

	public DwGroupChangeVariationTypeCommand(DwGroup group, DwGroupTypeEnum variationType, DwGraphicalTreeViewerEditor editor) {
		DwSimpleEvolutionOperationFactory operationFactory = new DwSimpleEvolutionOperationFactory();
		this.evoOp = operationFactory.createDwGroupTypeChangeOperation(group.getFeatureModel(), group, variationType, editor.getCurrentSelectedDate(), editor.isLastDateSelected());
		this.editor = editor;
	}

	@Override
	public void execute() {
		DwEvolutionOperationInterpreter.execute(evoOp);
		editor.updateTree();
	}

	@Override
	public void undo() {
		DwEvolutionOperationInterpreter.undo(evoOp);
		editor.updateTree();
	}

}
