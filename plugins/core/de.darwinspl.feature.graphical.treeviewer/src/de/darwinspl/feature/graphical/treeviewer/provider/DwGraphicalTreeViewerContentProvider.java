package de.darwinspl.feature.graphical.treeviewer.provider;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.viewers.ITreeContentProvider;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupComposition;
import de.darwinspl.feature.DwParentFeatureChildGroupContainer;
import de.darwinspl.feature.DwRootFeatureContainer;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.graphical.treeviewer.treeviewer.DwGraphicalTreeViewerEditor;
import de.darwinspl.feature.util.custom.DwFeatureUtil;
import de.darwinspl.temporal.util.DwEvolutionUtil;

public class DwGraphicalTreeViewerContentProvider implements ITreeContentProvider {

	DwGraphicalTreeViewerEditor editor;
	
	public DwGraphicalTreeViewerContentProvider(DwGraphicalTreeViewerEditor editor) {
		this.editor = editor;
	}

	@Override
	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof Resource) {
			for (EObject inputContent : (((Resource) inputElement).getContents())) {
				return new Object[] {inputContent};
			}
		}
		return null;
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		if(parentElement instanceof DwTemporalFeatureModel) {
			return new Object[] { DwEvolutionUtil.getValidTemporalElement(((DwTemporalFeatureModel) parentElement).getRootFeatures(), editor.getCurrentSelectedDate()) };
		}
		
		else if(parentElement instanceof DwRootFeatureContainer) {
			return getChildren(((DwRootFeatureContainer) parentElement).getFeature());
		}
		
		else if(parentElement instanceof DwFeature) {
			Set<DwGroup> containedGroups = new HashSet<>();
			for(DwParentFeatureChildGroupContainer featureChild :  DwEvolutionUtil.getValidTemporalElements(((DwFeature) parentElement).getParentOf(), editor.getCurrentSelectedDate())) {
				containedGroups.add(featureChild.getChildGroup());
			}
			return containedGroups.toArray();
		}
		
		else if(parentElement instanceof DwGroup) {
			Set<DwFeature> containedGroups = new HashSet<>();
			for(DwGroupComposition groupComp : DwEvolutionUtil.getValidTemporalElements(((DwGroup) parentElement).getParentOf(), editor.getCurrentSelectedDate())) {
				for(DwFeature feature : DwEvolutionUtil.getValidTemporalElements(groupComp.getChildFeatures(), editor.getCurrentSelectedDate())) {
					containedGroups.add(feature);
				}
			}
			return containedGroups.toArray();
		}
		
		return null;
	}
	
	@Override
	public Object getParent(Object element) {
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {
		if(element instanceof DwTemporalFeatureModel) {
			return !(DwEvolutionUtil.getValidTemporalElement(((DwTemporalFeatureModel) element).getRootFeatures(), editor.getCurrentSelectedDate()) == null);
		}
		
		else if(element instanceof DwRootFeatureContainer) {
			return hasChildren(((DwRootFeatureContainer) element).getFeature());
		}
		
		else if(element instanceof DwFeature) {
			return !(DwFeatureUtil.getChildFeaturesOfFeature(((DwFeature) element), editor.getCurrentSelectedDate()).size() == 0);
		}
		
		else if(element instanceof DwGroup) {
			for(DwGroupComposition groupComp : DwEvolutionUtil.getValidTemporalElements(((DwGroup) element).getParentOf(), editor.getCurrentSelectedDate())) {
				if(!groupComp.getChildFeatures().isEmpty())
					return true;
			}
			return false;
		}
		
		return false;
	}

}