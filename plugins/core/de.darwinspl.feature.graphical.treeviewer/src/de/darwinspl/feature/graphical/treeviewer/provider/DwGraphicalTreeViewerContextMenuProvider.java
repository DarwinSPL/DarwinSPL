package de.darwinspl.feature.graphical.treeviewer.provider;

import java.net.URL;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.ui.action.RedoAction;
import org.eclipse.emf.edit.ui.action.UndoAction;
import org.eclipse.emfforms.spi.swt.treemasterdetail.MenuProvider;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.internal.util.BundleUtility;
import org.osgi.framework.Bundle;

import de.darwinspl.feature.graphical.treeviewer.action.DwFeatureCreateAction;
import de.darwinspl.feature.graphical.treeviewer.action.DwFeatureCreateInNewGroupAction;
import de.darwinspl.feature.graphical.treeviewer.action.DwFeatureDeleteAction;
import de.darwinspl.feature.graphical.treeviewer.action.DwFeatureMakeMandatoryAction;
import de.darwinspl.feature.graphical.treeviewer.action.DwFeatureMakeOptionalAction;
import de.darwinspl.feature.graphical.treeviewer.action.DwGroupMakeAlternativeAction;
import de.darwinspl.feature.graphical.treeviewer.action.DwGroupMakeAndAction;
import de.darwinspl.feature.graphical.treeviewer.action.DwGroupMakeOrAction;
import de.darwinspl.feature.graphical.treeviewer.treeviewer.DwGraphicalTreeViewerEditor;


public class DwGraphicalTreeViewerContextMenuProvider implements MenuProvider{

	private DwGraphicalTreeViewerEditor editor;
	
	public DwGraphicalTreeViewerContextMenuProvider(DwGraphicalTreeViewerEditor editor) {
		this.editor = editor;
	}

	@Override
	public Menu getMenu(TreeViewer treeViewer, EditingDomain editingDomain) {
	    MenuManager contextMenu = new MenuManager("#ViewerMenu");
	    contextMenu.setRemoveAllWhenShown(true);
	    contextMenu.addMenuListener(new IMenuListener() {
	        @Override
	        public void menuAboutToShow(IMenuManager mgr) {
	    	    buildContextMenu(mgr);
	        }
	    });

	    Menu menu = contextMenu.createContextMenu(treeViewer.getControl());
	    treeViewer.getControl().setMenu(menu);
	    return menu;
	}

	@SuppressWarnings("restriction")
	public void buildContextMenu(IMenuManager menu) {
		Bundle bundle = Platform.getBundle("org.eclipse.ui");
		URL fullPathString = BundleUtility.find(bundle, "icons/full/etool16/undo_edit.png");
		UndoAction undoAction = new UndoAction(editor.getEditingDomain());
		undoAction.setImageDescriptor(ImageDescriptor.createFromURL(fullPathString));
		menu.add(undoAction);
		
		RedoAction redoAction = new RedoAction(editor.getEditingDomain());
		fullPathString = BundleUtility.find(bundle, "icons/full/etool16/redo_edit.png");
		redoAction.setImageDescriptor(ImageDescriptor.createFromURL(fullPathString));
		menu.add(redoAction);
		
		DwFeatureDeleteAction deleteAction = new DwFeatureDeleteAction(editor);
		fullPathString = BundleUtility.find(bundle, "icons/full/etool16/delete_edit.png");
		deleteAction.setImageDescriptor(ImageDescriptor.createFromURL(fullPathString));
		menu.add(deleteAction);
		menu.add(new Separator());

		menu.add(new DwFeatureCreateAction(editor));
		menu.add(new DwFeatureCreateInNewGroupAction(editor));
		menu.add(new DwFeatureMakeMandatoryAction(editor));
		menu.add(new DwFeatureMakeOptionalAction(editor));
		menu.add(new Separator());

		menu.add(new DwGroupMakeAndAction(editor));
		menu.add(new DwGroupMakeOrAction(editor));
		menu.add(new DwGroupMakeAlternativeAction(editor));
	}
}
