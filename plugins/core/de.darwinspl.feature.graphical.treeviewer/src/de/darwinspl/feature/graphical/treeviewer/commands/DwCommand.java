package de.darwinspl.feature.graphical.treeviewer.commands;

import java.util.Collection;

import org.eclipse.emf.common.command.Command;

import de.darwinspl.feature.graphical.treeviewer.treeviewer.DwGraphicalTreeViewerEditor;

public abstract class DwCommand implements Command {

	protected DwGraphicalTreeViewerEditor editor;

	@Override
	public boolean canExecute() {
		return true;
	}

	@Override
	public boolean canUndo() {
		return true;
	}

	@Override
	public void redo() {
		execute();
	}

	@Override
	public Collection<?> getResult() {
		return null;
	}

	@Override
	public Collection<?> getAffectedObjects() {
		return null;
	}

	@Override
	public String getLabel() {
		return null;
	}

	@Override
	public String getDescription() {
		return null;
	}

	@Override
	public void dispose() {
	}

	@Override
	public Command chain(Command command) {
		return null;
	}
	
}

