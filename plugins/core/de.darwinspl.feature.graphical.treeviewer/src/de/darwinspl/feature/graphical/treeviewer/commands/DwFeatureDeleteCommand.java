package de.darwinspl.feature.graphical.treeviewer.commands;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.evolution.operation.factory.DwSimpleEvolutionOperationFactory;
import de.darwinspl.feature.evolution.operation.interpreter.DwEvolutionOperationInterpreter;
import de.darwinspl.feature.graphical.treeviewer.treeviewer.DwGraphicalTreeViewerEditor;
import de.darwinspl.feature.operation.DwFeatureDeleteOperation;

public class DwFeatureDeleteCommand extends DwCommand {

	DwFeatureDeleteOperation deleteEvoOp;
	
	public DwFeatureDeleteCommand(DwFeature feature, DwGraphicalTreeViewerEditor dwGraphicalTreeViewerEditor) {
		DwSimpleEvolutionOperationFactory operationFactory = new DwSimpleEvolutionOperationFactory();
		this.deleteEvoOp = operationFactory.createDwFeatureDeleteOperation(feature.getFeatureModel(), feature, dwGraphicalTreeViewerEditor.getCurrentSelectedDate(), true);
		this.editor = dwGraphicalTreeViewerEditor;
	}
	
	@Override
	public void execute() {
		DwEvolutionOperationInterpreter.execute(deleteEvoOp);
		editor.updateTree();
	}

	@Override
	public void undo() {
		DwEvolutionOperationInterpreter.undo(deleteEvoOp);
		editor.updateTree();
	}
	
}
