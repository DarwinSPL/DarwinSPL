package de.darwinspl.feature.graphical.treeviewer.action;

import java.util.Date;

import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupTypeEnum;
import de.darwinspl.feature.graphical.treeviewer.treeviewer.DwGraphicalTreeViewerEditor;
import de.darwinspl.feature.util.custom.DwFeatureUtil;

public class DwGroupMakeAlternativeAction extends DwAbstractGroupTypeChangeAction {
	
	public static final String ID = DwGroupMakeAlternativeAction.class.getCanonicalName();

	public DwGroupMakeAlternativeAction(DwGraphicalTreeViewerEditor editor) {
		super(editor);
	}

	@Override
	protected String getGroupTypeText() {
		return "Alternative";
	}

	@Override
	protected String createID() {
		return ID;
	}

	@Override
	protected String createIconPath() {
		return "icons/ActionMakeGroupAlternative.png";
	}

	@Override
	protected boolean checkIfGroupTypeIsAlreadySet(DwGroup group, Date date) {
		return DwFeatureUtil.isAlternative(group, date);
	}

	@Override
	protected DwGroupTypeEnum getTargetGroupTypeEnum() {
		return DwGroupTypeEnum.ALTERNATIVE; 
	}
}

