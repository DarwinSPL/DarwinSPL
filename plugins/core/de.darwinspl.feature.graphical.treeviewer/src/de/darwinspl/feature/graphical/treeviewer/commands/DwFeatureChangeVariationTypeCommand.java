package de.darwinspl.feature.graphical.treeviewer.commands;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureTypeEnum;
import de.darwinspl.feature.evolution.operation.factory.DwSimpleEvolutionOperationFactory;
import de.darwinspl.feature.evolution.operation.interpreter.DwEvolutionOperationInterpreter;
import de.darwinspl.feature.graphical.treeviewer.treeviewer.DwGraphicalTreeViewerEditor;
import de.darwinspl.feature.operation.DwFeatureTypeChangeOperation;

public class DwFeatureChangeVariationTypeCommand extends DwCommand {

	DwFeatureTypeChangeOperation evoOp;

	public DwFeatureChangeVariationTypeCommand(DwFeature feature, DwFeatureTypeEnum variationType,
			DwGraphicalTreeViewerEditor editor) {
		DwSimpleEvolutionOperationFactory operationFactory = new DwSimpleEvolutionOperationFactory();
		this.editor = editor;
		evoOp = operationFactory.createDwFeatureTypeChangeOperation(feature.getFeatureModel(), feature, variationType, editor.getCurrentSelectedDate(), editor.isLastDateSelected());
	}

	@Override
	public void execute() {
		DwEvolutionOperationInterpreter.execute(evoOp);
		editor.updateTree();
	}

	@Override
	public void undo() {
		DwEvolutionOperationInterpreter.undo(evoOp);
		editor.updateTree();
	}

}
