package de.darwinspl.expression.util;

import java.util.Date;

import de.darwinspl.common.java.Tuple;
import de.darwinspl.temporal.util.DwDateResolverUtil;

public class IdentifierWithDateUtil {
	
	public static final String DATE_SEPARATOR_TOKEN = "@";
	public static final String DATE_TIME_SEPARATOR_TOKEN = "T";
	public static final String TIME_SEPARATORS = ":";
	
	
	
	/**
	 * Splits an identifier String at the date separator token. Format: <IDENTIFIER>@<DATE> . Identifier should not contain Date separator string. No guarantees what happens if format not kept.  
	 * @param identifierWithDate
	 * @return First index of array is identifier without date. If date available, second index is date string
	 */
	public static String[] splitByDateToken(String identifierWithDate) {
		String[] splittedString;
		
		if(identifierWithDate.contains(DATE_SEPARATOR_TOKEN)) {
			splittedString = identifierWithDate.split(DATE_SEPARATOR_TOKEN);
		}
		else {
			splittedString = new String[1];
			splittedString[0] = identifierWithDate;
		}
		
		return splittedString;
	}
	
	
	
	/**
	 * 
	 * @param identifierWithDate
	 * @return Date may be null if not specified
	 */
	public static Tuple<String, Date> getIdentifierAndDate(String identifierWithDate) {
		String plainIdentifier;
		Date date = null;
		
		if(identifierWithDate.contains(DATE_SEPARATOR_TOKEN)) {
			String[] split = splitByDateToken(identifierWithDate);
			String dateString;
			if(split.length==1) {
				plainIdentifier = "";
				dateString = null;
			} else {
				plainIdentifier = split[0];
				dateString = split[1];		
			}
			date = DwDateResolverUtil.resolveDateAlternative(dateString);
		} 
		else {
			plainIdentifier = identifierWithDate;
		}
		
		plainIdentifier = removeQuotationMarks(plainIdentifier);
		
		Tuple<String, Date> identifierAndDate = new Tuple<String, Date>(plainIdentifier, date);
		return identifierAndDate;
	}
	
	
	
	public static String removeQuotationMarks(String string) {
		if (string.startsWith("\"") && string.endsWith("\"")) {
			return string.substring(1, string.length() - 1);
		}
		
		return string;
	}
	
	
	
}


















