package de.darwinspl.expression.util;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

import de.darwinspl.common.java.Tuple;
import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.util.custom.DwFeatureResolverUtil;
import de.darwinspl.feature.util.custom.DwTemporalFeatureModelWellFormednessException;

public class ExpressionResolver {
	
	private static Map<Resource, DwTemporalFeatureModel> resourceToFeatureModelMap = new HashMap<>();
	

	// TODO
	// TODO
	// TODO
	// TODO
	// TODO
	// TODO
	// TODO
	// TODO
	// TODO
	// TODO
	// TODO
	// TODO
	// TODO
	
	// Update: we now EXPLICITELY reference the corresponding FM in the beginning of a CTC file
	//   -> the "getAccompanyingFeatureModel" method is non-sense here!!
	
	// TODO
	// TODO
	// TODO
	// TODO
	// TODO
	// TODO
	// TODO
	// TODO
	// TODO
	// TODO
	// TODO
	// TODO
	// TODO
	// TODO
	
	public static DwFeature resolveFeature(String identifier, EObject elementFromAccompanyingResource) {
		if (elementFromAccompanyingResource == null) {
			return null;
		}
		
		Resource containingResource = elementFromAccompanyingResource.eResource();
		
		DwTemporalFeatureModel featureModel = resourceToFeatureModelMap.get(containingResource);
		if(featureModel == null) {
			featureModel = DwFeatureResolverUtil.getAccompanyingFeatureModel(elementFromAccompanyingResource);
			resourceToFeatureModelMap.put(containingResource, featureModel);
		}

		return resolveFeatureFromFeatureModel(identifier, featureModel);
	}
	
	public static DwFeature resolveFeatureFromFeatureModel(String identifier, DwTemporalFeatureModel featureModel) {
		if (identifier == null) {
			return null;
		}
		
		Tuple<String, Date> identifierAndDate = IdentifierWithDateUtil.getIdentifierAndDate(identifier);
		if (identifierAndDate.getSecondEntry() == null) {
			identifierAndDate.setSecondEntry(new Date());
		}

		try {
			return DwFeatureResolverUtil.resolveFeature(identifierAndDate.getFirstEntry(), featureModel, identifierAndDate.getSecondEntry());
		} catch (DwTemporalFeatureModelWellFormednessException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
}
