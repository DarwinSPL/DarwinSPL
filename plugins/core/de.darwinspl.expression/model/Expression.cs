SYNTAXDEF dwexpression
FOR <http://www.darwinspl.de/expression/2.0> <Expression.genmodel>
START DwExpression


OPTIONS {
	reloadGeneratorModel = "false";
	usePredefinedTokens = "false";
	
	defaultTokenName = "IDENTIFIER_TOKEN";
	disableNewProjectWizard = "true";
	disableBuilder = "true";
	disableLaunchSupport = "true";
	disableDebugSupport = "true";
	
//	autofixSimpleLeftrecursion = "true";
}

TOKENS {
	DEFINE DATE $($ + INTEGER_LITERAL + $'/'$ + INTEGER_LITERAL + $'/'$ + INTEGER_LITERAL + $( 'T'$ + INTEGER_LITERAL + $':'$ + INTEGER_LITERAL + $(':' $ + INTEGER_LITERAL + $)?$ + $)?)$;
	DEFINE INTEGER_LITERAL $('0'..'9')+ $;
	DEFINE IDENTIFIER_TOKEN $('A'..'Z'|'a'..'z'|'_')('A'..'Z'|'a'..'z'|'_'|'0'..'9')*$;
//	DEFINE IDENTIFIER_TOKEN $('A'..'Z'|'a'..'z'|'_')('A'..'Z'|'a'..'z'|'_'|'0'..'9')* ('@'$ +DATE+ $)?$;
	// FORMAT: YYYY / MM / DD (T hh:mm(:ss)?)?
//	DEFINE DATE $($ + INTEGER_LITERAL + $'/'$ + INTEGER_LITERAL + $'/'$ + INTEGER_LITERAL + $( 'T'$ + INTEGER_LITERAL + $':'$ + INTEGER_LITERAL + $(':' $ + INTEGER_LITERAL + $)?$ + $)?)$;
//	DEFINE INTEGER_LITERAL $('0'..'9')+ $;
	
	DEFINE SL_COMMENT $'//'(~('\n'|'\r'|'\uffff'))*$;
	DEFINE ML_COMMENT $'/*'.*'*/'$;
	
	DEFINE LINEBREAK $('\r\n'|'\r'|'\n')$;
	DEFINE WHITESPACE $(' '|'\t'|'\f')$;
}

TOKENSTYLES  {
	"SL_COMMENT", "ML_COMMENT" COLOR #008000;
		
	"<->", "->", "||", "&&", "!", "/", "*", "%", "+", "-" COLOR #800040, BOLD;
	
	//"^" COLOR #800040, BOLD;
	"?" COLOR #800040, BOLD;
	
	"<", "<=", "=", ">", ">=" COLOR #800040, BOLD;
	"[", "]", "(", ")" COLOR #0000CC;
}

RULES {	
	@Operator(type="binary_left_associative",  weight="0", superclass="DwExpression")
	DwEquivalenceExpression ::= operand1 "<->" operand2;
	
	@Operator(type="binary_left_associative", weight="1", superclass="DwExpression")
	DwImpliesExpression ::= operand1 "->" operand2;
	
	@Operator(type="binary_left_associative", weight="2", superclass="DwExpression")
	DwOrExpression ::= operand1 "||" operand2;
	
	@Operator(type="binary_left_associative", weight="3", superclass="DwExpression")
	DwAndExpression ::= operand1 "&&" operand2;
	
	@Operator(type="unary_prefix", weight="14", superclass="DwExpression")
	DwNotExpression ::= "!" operand;
	
	@Operator(type="primitive", weight="15", superclass="DwExpression")
	DwNestedExpression ::= "(" operand ")";
	
	@SuppressWarnings(explicitSyntaxChoice)
	@Operator(type="primitive", weight="15", superclass="DwExpression")
	DwFeatureReferenceExpression ::= (feature['"', '"'] | feature[]);

	
	@Operator(type="primitive", weight="15", superclass="DwExpression")
	DwBooleanValueExpression ::= value["true" : "false"];
	
//	@Operator(type="primitive", weight="15", superclass="HyExpression")
//	HyMinimumExpression ::= "min(" operand ")";
//	
//	@Operator(type="primitive", weight="15", superclass="HyExpression")
//	HyMaximumExpression ::= "max(" operand ")";
//	
//	@Operator(type="primitive", weight="15", superclass="HyExpression")
//	HyIfPossibleExpression ::= "ifPossible(" operands ("," operands)* ")";
	
//	@SuppressWarnings(explicitSyntaxChoice)
//	@Operator(type="primitive", weight="15", superclass="HyExpression")
//	HyArithmeticalComparisonExpression ::= "{" operand1 operator[HyLessOperator : "<", HyLessOrEqualOperator : "<=", HyEqualOperator : "=", HyNotEqualOperator : "!=", HyGreaterOrEqualOperator : ">=", HyGreaterOperator : ">"] operand2 "}";
	
	
//	@Operator(type="binary_left_associative", weight="5", superclass="HyArithmeticalValueExpression")
//	HyAdditionExpression ::= operand1 "+" operand2;
	
//	@Operator(type="binary_left_associative", weight="6", superclass="HyArithmeticalValueExpression")
//	HySubtractionExpression ::= operand1 "-" operand2;
	
//	@Operator(type="binary_left_associative", weight="4", superclass="HyArithmeticalValueExpression")
//	HyModuloExpression ::= operand1 "%" operand2;
	
//	@Operator(type="binary_left_associative", weight="7", superclass="HyArithmeticalValueExpression")
//	HyMultiplicationExpression ::= operand1 "*" operand2;
	
//	@Operator(type="binary_left_associative", weight="8", superclass="HyArithmeticalValueExpression")
//	HyDivisionExpression ::= operand1 "/" operand2;
	
//	@Operator(type="unary_prefix", weight="14", superclass="HyArithmeticalValueExpression")
//	HyNegationExpression ::= "-" operand;
	
//	@Operator(type="primitive", weight="15", superclass="HyArithmeticalValueExpression")
//	HyNestedArithmeticalValueExpression ::= "(" operand ")";
	
//	@SuppressWarnings(explicitSyntaxChoice)
//	@SuppressWarnings(minOccurenceMismatch)
//	@Operator(type="primitive", weight="11", superclass="HyArithmeticalValueExpression")
//	HyAttributeReferenceExpression ::= (feature['"', '"'] | feature[]) "." attribute[];
	
//	@SuppressWarnings(explicitSyntaxChoice)
//	@Operator(type="primitive", weight="11", superclass="HyArithmeticalValueExpression")
//	HyContextInformationReferenceExpression ::= "context:" (contextInformation['"', '"'] | contextInformation[]);
	
//	@Operator(type="primitive", weight="11", superclass="HyArithmeticalValueExpression")
//	HyValueExpression ::= value;
	
//	HyRelativeVersionRestriction ::= "[" operator[lessThan : "<", lessThanOrEqual : "<=", equal : "=", implicitEqual : "", greaterThanOrEqual : ">=", greaterThan : ">"] version['"','"'] "]";
//	HyVersionRangeRestriction ::= "[" lowerIncluded["" : "^"] lowerVersion['"','"'] "-" upperIncluded["" : "^"] upperVersion['"','"'] "]";
	
	
	// syntax definition for class 'HyNumberValue'
//	@Operator(type="primitive", weight="15", superclass="HyValue")
//	HyNumberValue ::= value[INTEGER_LITERAL];
	
	// syntax definition for class 'AnotherMetaClass'
//	@Operator(type="primitive", weight="15", superclass="HyValue")
//	HyBooleanValue ::= value["true" : "false"];
	
//	@SuppressWarnings(minOccurenceMismatch, explicitSyntaxChoice)
//	@Operator(type="primitive", weight="15", superclass="HyValue")
//	HyEnumValue ::= "enum:"(enum['"', '"'] | enum[]) "." (enumLiteral['"', '"'] | enumLiteral[]);
	
//	@SuppressWarnings(explicitSyntaxChoice)
//	HyEnum ::= "Enum(" name[IDENTIFIER_TOKEN]"," (literals ("," literals)*)? ")" ("[" (validSince[DATE] "-" validUntil[DATE] | validSince[DATE] "-"  | "eternity" "-" validUntil[DATE])  "]")?;
	
//	@SuppressWarnings(explicitSyntaxChoice)
//	HyEnumLiteral ::= "EnumLiteral(" name[IDENTIFIER_TOKEN] "," value[INTEGER_LITERAL] ")" ("[" (validSince[DATE] "-" validUntil[DATE] | validSince[DATE] "-" "eternity" | "eternity" "-" validUntil[DATE])  "]")?;

//	@SuppressWarnings(explicitSyntaxChoice)
//	DwTemporalInterval ::= "[" (from[DATE] "-" to[DATE] | from[DATE] "-" "eternity" | "eternity" "-" to[DATE])  "]";
}
