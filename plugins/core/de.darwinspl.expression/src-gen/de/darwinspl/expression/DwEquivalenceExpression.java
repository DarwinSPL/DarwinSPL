/**
 */
package de.darwinspl.expression;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Equivalence Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.expression.DwExpressionPackage#getDwEquivalenceExpression()
 * @model
 * @generated
 */
public interface DwEquivalenceExpression extends DwBinaryExpression {
} // DwEquivalenceExpression
