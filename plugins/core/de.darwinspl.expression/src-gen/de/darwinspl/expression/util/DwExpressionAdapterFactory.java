/**
 */
package de.darwinspl.expression.util;

import de.darwinspl.expression.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see de.darwinspl.expression.DwExpressionPackage
 * @generated
 */
public class DwExpressionAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DwExpressionPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwExpressionAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = DwExpressionPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwExpressionSwitch<Adapter> modelSwitch =
		new DwExpressionSwitch<Adapter>() {
			@Override
			public Adapter caseDwExpression(DwExpression object) {
				return createDwExpressionAdapter();
			}
			@Override
			public Adapter caseDwAtomicExpression(DwAtomicExpression object) {
				return createDwAtomicExpressionAdapter();
			}
			@Override
			public Adapter caseDwFeatureReferenceExpression(DwFeatureReferenceExpression object) {
				return createDwFeatureReferenceExpressionAdapter();
			}
			@Override
			public Adapter caseDwBooleanValueExpression(DwBooleanValueExpression object) {
				return createDwBooleanValueExpressionAdapter();
			}
			@Override
			public Adapter caseDwUnaryExpression(DwUnaryExpression object) {
				return createDwUnaryExpressionAdapter();
			}
			@Override
			public Adapter caseDwNestedExpression(DwNestedExpression object) {
				return createDwNestedExpressionAdapter();
			}
			@Override
			public Adapter caseDwNotExpression(DwNotExpression object) {
				return createDwNotExpressionAdapter();
			}
			@Override
			public Adapter caseDwBinaryExpression(DwBinaryExpression object) {
				return createDwBinaryExpressionAdapter();
			}
			@Override
			public Adapter caseDwAndExpression(DwAndExpression object) {
				return createDwAndExpressionAdapter();
			}
			@Override
			public Adapter caseDwOrExpression(DwOrExpression object) {
				return createDwOrExpressionAdapter();
			}
			@Override
			public Adapter caseDwImpliesExpression(DwImpliesExpression object) {
				return createDwImpliesExpressionAdapter();
			}
			@Override
			public Adapter caseDwEquivalenceExpression(DwEquivalenceExpression object) {
				return createDwEquivalenceExpressionAdapter();
			}
			@Override
			public Adapter caseDwSetExpression(DwSetExpression object) {
				return createDwSetExpressionAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.expression.DwExpression <em>Dw Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.expression.DwExpression
	 * @generated
	 */
	public Adapter createDwExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.expression.DwAtomicExpression <em>Dw Atomic Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.expression.DwAtomicExpression
	 * @generated
	 */
	public Adapter createDwAtomicExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.expression.DwFeatureReferenceExpression <em>Dw Feature Reference Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.expression.DwFeatureReferenceExpression
	 * @generated
	 */
	public Adapter createDwFeatureReferenceExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.expression.DwBooleanValueExpression <em>Dw Boolean Value Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.expression.DwBooleanValueExpression
	 * @generated
	 */
	public Adapter createDwBooleanValueExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.expression.DwUnaryExpression <em>Dw Unary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.expression.DwUnaryExpression
	 * @generated
	 */
	public Adapter createDwUnaryExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.expression.DwNestedExpression <em>Dw Nested Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.expression.DwNestedExpression
	 * @generated
	 */
	public Adapter createDwNestedExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.expression.DwNotExpression <em>Dw Not Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.expression.DwNotExpression
	 * @generated
	 */
	public Adapter createDwNotExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.expression.DwBinaryExpression <em>Dw Binary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.expression.DwBinaryExpression
	 * @generated
	 */
	public Adapter createDwBinaryExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.expression.DwAndExpression <em>Dw And Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.expression.DwAndExpression
	 * @generated
	 */
	public Adapter createDwAndExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.expression.DwOrExpression <em>Dw Or Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.expression.DwOrExpression
	 * @generated
	 */
	public Adapter createDwOrExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.expression.DwImpliesExpression <em>Dw Implies Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.expression.DwImpliesExpression
	 * @generated
	 */
	public Adapter createDwImpliesExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.expression.DwEquivalenceExpression <em>Dw Equivalence Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.expression.DwEquivalenceExpression
	 * @generated
	 */
	public Adapter createDwEquivalenceExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.darwinspl.expression.DwSetExpression <em>Dw Set Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.darwinspl.expression.DwSetExpression
	 * @generated
	 */
	public Adapter createDwSetExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //DwExpressionAdapterFactory
