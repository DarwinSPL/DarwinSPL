/**
 */
package de.darwinspl.expression.util;

import de.darwinspl.expression.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see de.darwinspl.expression.DwExpressionPackage
 * @generated
 */
public class DwExpressionSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DwExpressionPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwExpressionSwitch() {
		if (modelPackage == null) {
			modelPackage = DwExpressionPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case DwExpressionPackage.DW_EXPRESSION: {
				DwExpression dwExpression = (DwExpression)theEObject;
				T result = caseDwExpression(dwExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwExpressionPackage.DW_ATOMIC_EXPRESSION: {
				DwAtomicExpression dwAtomicExpression = (DwAtomicExpression)theEObject;
				T result = caseDwAtomicExpression(dwAtomicExpression);
				if (result == null) result = caseDwExpression(dwAtomicExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION: {
				DwFeatureReferenceExpression dwFeatureReferenceExpression = (DwFeatureReferenceExpression)theEObject;
				T result = caseDwFeatureReferenceExpression(dwFeatureReferenceExpression);
				if (result == null) result = caseDwAtomicExpression(dwFeatureReferenceExpression);
				if (result == null) result = caseDwExpression(dwFeatureReferenceExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwExpressionPackage.DW_BOOLEAN_VALUE_EXPRESSION: {
				DwBooleanValueExpression dwBooleanValueExpression = (DwBooleanValueExpression)theEObject;
				T result = caseDwBooleanValueExpression(dwBooleanValueExpression);
				if (result == null) result = caseDwAtomicExpression(dwBooleanValueExpression);
				if (result == null) result = caseDwExpression(dwBooleanValueExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwExpressionPackage.DW_UNARY_EXPRESSION: {
				DwUnaryExpression dwUnaryExpression = (DwUnaryExpression)theEObject;
				T result = caseDwUnaryExpression(dwUnaryExpression);
				if (result == null) result = caseDwExpression(dwUnaryExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwExpressionPackage.DW_NESTED_EXPRESSION: {
				DwNestedExpression dwNestedExpression = (DwNestedExpression)theEObject;
				T result = caseDwNestedExpression(dwNestedExpression);
				if (result == null) result = caseDwUnaryExpression(dwNestedExpression);
				if (result == null) result = caseDwExpression(dwNestedExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwExpressionPackage.DW_NOT_EXPRESSION: {
				DwNotExpression dwNotExpression = (DwNotExpression)theEObject;
				T result = caseDwNotExpression(dwNotExpression);
				if (result == null) result = caseDwUnaryExpression(dwNotExpression);
				if (result == null) result = caseDwExpression(dwNotExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwExpressionPackage.DW_BINARY_EXPRESSION: {
				DwBinaryExpression dwBinaryExpression = (DwBinaryExpression)theEObject;
				T result = caseDwBinaryExpression(dwBinaryExpression);
				if (result == null) result = caseDwExpression(dwBinaryExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwExpressionPackage.DW_AND_EXPRESSION: {
				DwAndExpression dwAndExpression = (DwAndExpression)theEObject;
				T result = caseDwAndExpression(dwAndExpression);
				if (result == null) result = caseDwBinaryExpression(dwAndExpression);
				if (result == null) result = caseDwExpression(dwAndExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwExpressionPackage.DW_OR_EXPRESSION: {
				DwOrExpression dwOrExpression = (DwOrExpression)theEObject;
				T result = caseDwOrExpression(dwOrExpression);
				if (result == null) result = caseDwBinaryExpression(dwOrExpression);
				if (result == null) result = caseDwExpression(dwOrExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwExpressionPackage.DW_IMPLIES_EXPRESSION: {
				DwImpliesExpression dwImpliesExpression = (DwImpliesExpression)theEObject;
				T result = caseDwImpliesExpression(dwImpliesExpression);
				if (result == null) result = caseDwBinaryExpression(dwImpliesExpression);
				if (result == null) result = caseDwExpression(dwImpliesExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwExpressionPackage.DW_EQUIVALENCE_EXPRESSION: {
				DwEquivalenceExpression dwEquivalenceExpression = (DwEquivalenceExpression)theEObject;
				T result = caseDwEquivalenceExpression(dwEquivalenceExpression);
				if (result == null) result = caseDwBinaryExpression(dwEquivalenceExpression);
				if (result == null) result = caseDwExpression(dwEquivalenceExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DwExpressionPackage.DW_SET_EXPRESSION: {
				DwSetExpression dwSetExpression = (DwSetExpression)theEObject;
				T result = caseDwSetExpression(dwSetExpression);
				if (result == null) result = caseDwExpression(dwSetExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwExpression(DwExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Atomic Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Atomic Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwAtomicExpression(DwAtomicExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Feature Reference Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Feature Reference Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwFeatureReferenceExpression(DwFeatureReferenceExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Boolean Value Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Boolean Value Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwBooleanValueExpression(DwBooleanValueExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Unary Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Unary Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwUnaryExpression(DwUnaryExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Nested Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Nested Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwNestedExpression(DwNestedExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Not Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Not Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwNotExpression(DwNotExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Binary Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Binary Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwBinaryExpression(DwBinaryExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw And Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw And Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwAndExpression(DwAndExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Or Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Or Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwOrExpression(DwOrExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Implies Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Implies Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwImpliesExpression(DwImpliesExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Equivalence Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Equivalence Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwEquivalenceExpression(DwEquivalenceExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dw Set Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dw Set Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDwSetExpression(DwSetExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //DwExpressionSwitch
