/**
 */
package de.darwinspl.expression;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Set Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.expression.DwSetExpression#getOperands <em>Operands</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.expression.DwExpressionPackage#getDwSetExpression()
 * @model abstract="true"
 * @generated
 */
public interface DwSetExpression extends DwExpression {
	/**
	 * Returns the value of the '<em><b>Operands</b></em>' containment reference list.
	 * The list contents are of type {@link de.darwinspl.expression.DwExpression}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operands</em>' containment reference list.
	 * @see de.darwinspl.expression.DwExpressionPackage#getDwSetExpression_Operands()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<DwExpression> getOperands();

} // DwSetExpression
