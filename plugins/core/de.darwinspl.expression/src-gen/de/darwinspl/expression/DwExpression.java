/**
 */
package de.darwinspl.expression;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.expression.DwExpressionPackage#getDwExpression()
 * @model abstract="true"
 * @generated
 */
public interface DwExpression extends EObject {
} // DwExpression
