/**
 */
package de.darwinspl.expression;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.darwinspl.expression.DwExpressionPackage
 * @generated
 */
public interface DwExpressionFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DwExpressionFactory eINSTANCE = de.darwinspl.expression.impl.DwExpressionFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Dw Feature Reference Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Feature Reference Expression</em>'.
	 * @generated
	 */
	DwFeatureReferenceExpression createDwFeatureReferenceExpression();

	/**
	 * Returns a new object of class '<em>Dw Boolean Value Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Boolean Value Expression</em>'.
	 * @generated
	 */
	DwBooleanValueExpression createDwBooleanValueExpression();

	/**
	 * Returns a new object of class '<em>Dw Nested Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Nested Expression</em>'.
	 * @generated
	 */
	DwNestedExpression createDwNestedExpression();

	/**
	 * Returns a new object of class '<em>Dw Not Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Not Expression</em>'.
	 * @generated
	 */
	DwNotExpression createDwNotExpression();

	/**
	 * Returns a new object of class '<em>Dw And Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw And Expression</em>'.
	 * @generated
	 */
	DwAndExpression createDwAndExpression();

	/**
	 * Returns a new object of class '<em>Dw Or Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Or Expression</em>'.
	 * @generated
	 */
	DwOrExpression createDwOrExpression();

	/**
	 * Returns a new object of class '<em>Dw Implies Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Implies Expression</em>'.
	 * @generated
	 */
	DwImpliesExpression createDwImpliesExpression();

	/**
	 * Returns a new object of class '<em>Dw Equivalence Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dw Equivalence Expression</em>'.
	 * @generated
	 */
	DwEquivalenceExpression createDwEquivalenceExpression();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	DwExpressionPackage getDwExpressionPackage();

} //DwExpressionFactory
