/**
 */
package de.darwinspl.expression;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Binary Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.expression.DwBinaryExpression#getOperand1 <em>Operand1</em>}</li>
 *   <li>{@link de.darwinspl.expression.DwBinaryExpression#getOperand2 <em>Operand2</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.expression.DwExpressionPackage#getDwBinaryExpression()
 * @model abstract="true"
 * @generated
 */
public interface DwBinaryExpression extends DwExpression {
	/**
	 * Returns the value of the '<em><b>Operand1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operand1</em>' containment reference.
	 * @see #setOperand1(DwExpression)
	 * @see de.darwinspl.expression.DwExpressionPackage#getDwBinaryExpression_Operand1()
	 * @model containment="true" required="true"
	 * @generated
	 */
	DwExpression getOperand1();

	/**
	 * Sets the value of the '{@link de.darwinspl.expression.DwBinaryExpression#getOperand1 <em>Operand1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operand1</em>' containment reference.
	 * @see #getOperand1()
	 * @generated
	 */
	void setOperand1(DwExpression value);

	/**
	 * Returns the value of the '<em><b>Operand2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operand2</em>' containment reference.
	 * @see #setOperand2(DwExpression)
	 * @see de.darwinspl.expression.DwExpressionPackage#getDwBinaryExpression_Operand2()
	 * @model containment="true" required="true"
	 * @generated
	 */
	DwExpression getOperand2();

	/**
	 * Sets the value of the '{@link de.darwinspl.expression.DwBinaryExpression#getOperand2 <em>Operand2</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operand2</em>' containment reference.
	 * @see #getOperand2()
	 * @generated
	 */
	void setOperand2(DwExpression value);

} // DwBinaryExpression
