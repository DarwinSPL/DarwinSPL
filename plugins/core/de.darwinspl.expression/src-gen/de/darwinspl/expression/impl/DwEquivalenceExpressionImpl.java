/**
 */
package de.darwinspl.expression.impl;

import de.darwinspl.expression.DwEquivalenceExpression;
import de.darwinspl.expression.DwExpressionPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Equivalence Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DwEquivalenceExpressionImpl extends DwBinaryExpressionImpl implements DwEquivalenceExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwEquivalenceExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwExpressionPackage.Literals.DW_EQUIVALENCE_EXPRESSION;
	}

} //DwEquivalenceExpressionImpl
