/**
 */
package de.darwinspl.expression.impl;

import de.darwinspl.expression.DwExpression;
import de.darwinspl.expression.DwExpressionPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class DwExpressionImpl extends MinimalEObjectImpl.Container implements DwExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwExpressionPackage.Literals.DW_EXPRESSION;
	}

} //DwExpressionImpl
