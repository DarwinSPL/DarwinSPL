/**
 */
package de.darwinspl.expression.impl;

import de.darwinspl.expression.DwAtomicExpression;
import de.darwinspl.expression.DwExpressionPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Atomic Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class DwAtomicExpressionImpl extends DwExpressionImpl implements DwAtomicExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwAtomicExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwExpressionPackage.Literals.DW_ATOMIC_EXPRESSION;
	}

} //DwAtomicExpressionImpl
