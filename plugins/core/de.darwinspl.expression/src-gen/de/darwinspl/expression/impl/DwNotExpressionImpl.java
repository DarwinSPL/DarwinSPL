/**
 */
package de.darwinspl.expression.impl;

import de.darwinspl.expression.DwExpressionPackage;
import de.darwinspl.expression.DwNotExpression;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Not Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DwNotExpressionImpl extends DwUnaryExpressionImpl implements DwNotExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwNotExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwExpressionPackage.Literals.DW_NOT_EXPRESSION;
	}

} //DwNotExpressionImpl
