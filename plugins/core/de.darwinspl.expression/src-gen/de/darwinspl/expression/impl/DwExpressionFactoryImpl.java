/**
 */
package de.darwinspl.expression.impl;

import de.darwinspl.expression.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DwExpressionFactoryImpl extends EFactoryImpl implements DwExpressionFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DwExpressionFactory init() {
		try {
			DwExpressionFactory theDwExpressionFactory = (DwExpressionFactory)EPackage.Registry.INSTANCE.getEFactory(DwExpressionPackage.eNS_URI);
			if (theDwExpressionFactory != null) {
				return theDwExpressionFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DwExpressionFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DwExpressionFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case DwExpressionPackage.DW_FEATURE_REFERENCE_EXPRESSION: return createDwFeatureReferenceExpression();
			case DwExpressionPackage.DW_BOOLEAN_VALUE_EXPRESSION: return createDwBooleanValueExpression();
			case DwExpressionPackage.DW_NESTED_EXPRESSION: return createDwNestedExpression();
			case DwExpressionPackage.DW_NOT_EXPRESSION: return createDwNotExpression();
			case DwExpressionPackage.DW_AND_EXPRESSION: return createDwAndExpression();
			case DwExpressionPackage.DW_OR_EXPRESSION: return createDwOrExpression();
			case DwExpressionPackage.DW_IMPLIES_EXPRESSION: return createDwImpliesExpression();
			case DwExpressionPackage.DW_EQUIVALENCE_EXPRESSION: return createDwEquivalenceExpression();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwFeatureReferenceExpression createDwFeatureReferenceExpression() {
		DwFeatureReferenceExpressionImpl dwFeatureReferenceExpression = new DwFeatureReferenceExpressionImpl();
		return dwFeatureReferenceExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwBooleanValueExpression createDwBooleanValueExpression() {
		DwBooleanValueExpressionImpl dwBooleanValueExpression = new DwBooleanValueExpressionImpl();
		return dwBooleanValueExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwNestedExpression createDwNestedExpression() {
		DwNestedExpressionImpl dwNestedExpression = new DwNestedExpressionImpl();
		return dwNestedExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwNotExpression createDwNotExpression() {
		DwNotExpressionImpl dwNotExpression = new DwNotExpressionImpl();
		return dwNotExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwAndExpression createDwAndExpression() {
		DwAndExpressionImpl dwAndExpression = new DwAndExpressionImpl();
		return dwAndExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwOrExpression createDwOrExpression() {
		DwOrExpressionImpl dwOrExpression = new DwOrExpressionImpl();
		return dwOrExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwImpliesExpression createDwImpliesExpression() {
		DwImpliesExpressionImpl dwImpliesExpression = new DwImpliesExpressionImpl();
		return dwImpliesExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwEquivalenceExpression createDwEquivalenceExpression() {
		DwEquivalenceExpressionImpl dwEquivalenceExpression = new DwEquivalenceExpressionImpl();
		return dwEquivalenceExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwExpressionPackage getDwExpressionPackage() {
		return (DwExpressionPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DwExpressionPackage getPackage() {
		return DwExpressionPackage.eINSTANCE;
	}

} //DwExpressionFactoryImpl
