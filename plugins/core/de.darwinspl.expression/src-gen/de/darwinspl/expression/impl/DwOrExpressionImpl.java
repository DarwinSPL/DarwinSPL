/**
 */
package de.darwinspl.expression.impl;

import de.darwinspl.expression.DwExpressionPackage;
import de.darwinspl.expression.DwOrExpression;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Or Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DwOrExpressionImpl extends DwBinaryExpressionImpl implements DwOrExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwOrExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwExpressionPackage.Literals.DW_OR_EXPRESSION;
	}

} //DwOrExpressionImpl
