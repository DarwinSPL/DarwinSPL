/**
 */
package de.darwinspl.expression.impl;

import de.darwinspl.datatypes.DwDatatypesPackage;

import de.darwinspl.expression.DwAndExpression;
import de.darwinspl.expression.DwAtomicExpression;
import de.darwinspl.expression.DwBinaryExpression;
import de.darwinspl.expression.DwBooleanValueExpression;
import de.darwinspl.expression.DwEquivalenceExpression;
import de.darwinspl.expression.DwExpression;
import de.darwinspl.expression.DwExpressionFactory;
import de.darwinspl.expression.DwExpressionPackage;
import de.darwinspl.expression.DwFeatureReferenceExpression;
import de.darwinspl.expression.DwImpliesExpression;
import de.darwinspl.expression.DwNestedExpression;
import de.darwinspl.expression.DwNotExpression;
import de.darwinspl.expression.DwOrExpression;
import de.darwinspl.expression.DwSetExpression;
import de.darwinspl.expression.DwUnaryExpression;

import de.darwinspl.feature.DwFeaturePackage;

import de.darwinspl.temporal.DwTemporalModelPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DwExpressionPackageImpl extends EPackageImpl implements DwExpressionPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwAtomicExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwFeatureReferenceExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwBooleanValueExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwUnaryExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwNestedExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwNotExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwBinaryExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwAndExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwOrExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwImpliesExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwEquivalenceExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dwSetExpressionEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.darwinspl.expression.DwExpressionPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private DwExpressionPackageImpl() {
		super(eNS_URI, DwExpressionFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link DwExpressionPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static DwExpressionPackage init() {
		if (isInited) return (DwExpressionPackage)EPackage.Registry.INSTANCE.getEPackage(DwExpressionPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredDwExpressionPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		DwExpressionPackageImpl theDwExpressionPackage = registeredDwExpressionPackage instanceof DwExpressionPackageImpl ? (DwExpressionPackageImpl)registeredDwExpressionPackage : new DwExpressionPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		DwDatatypesPackage.eINSTANCE.eClass();
		DwFeaturePackage.eINSTANCE.eClass();
		DwTemporalModelPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theDwExpressionPackage.createPackageContents();

		// Initialize created meta-data
		theDwExpressionPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theDwExpressionPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(DwExpressionPackage.eNS_URI, theDwExpressionPackage);
		return theDwExpressionPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwExpression() {
		return dwExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwAtomicExpression() {
		return dwAtomicExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwFeatureReferenceExpression() {
		return dwFeatureReferenceExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwFeatureReferenceExpression_Feature() {
		return (EReference)dwFeatureReferenceExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwBooleanValueExpression() {
		return dwBooleanValueExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDwBooleanValueExpression_Value() {
		return (EAttribute)dwBooleanValueExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwUnaryExpression() {
		return dwUnaryExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwUnaryExpression_Operand() {
		return (EReference)dwUnaryExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwNestedExpression() {
		return dwNestedExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwNotExpression() {
		return dwNotExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwBinaryExpression() {
		return dwBinaryExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwBinaryExpression_Operand1() {
		return (EReference)dwBinaryExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwBinaryExpression_Operand2() {
		return (EReference)dwBinaryExpressionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwAndExpression() {
		return dwAndExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwOrExpression() {
		return dwOrExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwImpliesExpression() {
		return dwImpliesExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwEquivalenceExpression() {
		return dwEquivalenceExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDwSetExpression() {
		return dwSetExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDwSetExpression_Operands() {
		return (EReference)dwSetExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DwExpressionFactory getDwExpressionFactory() {
		return (DwExpressionFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		dwExpressionEClass = createEClass(DW_EXPRESSION);

		dwAtomicExpressionEClass = createEClass(DW_ATOMIC_EXPRESSION);

		dwFeatureReferenceExpressionEClass = createEClass(DW_FEATURE_REFERENCE_EXPRESSION);
		createEReference(dwFeatureReferenceExpressionEClass, DW_FEATURE_REFERENCE_EXPRESSION__FEATURE);

		dwBooleanValueExpressionEClass = createEClass(DW_BOOLEAN_VALUE_EXPRESSION);
		createEAttribute(dwBooleanValueExpressionEClass, DW_BOOLEAN_VALUE_EXPRESSION__VALUE);

		dwUnaryExpressionEClass = createEClass(DW_UNARY_EXPRESSION);
		createEReference(dwUnaryExpressionEClass, DW_UNARY_EXPRESSION__OPERAND);

		dwNestedExpressionEClass = createEClass(DW_NESTED_EXPRESSION);

		dwNotExpressionEClass = createEClass(DW_NOT_EXPRESSION);

		dwBinaryExpressionEClass = createEClass(DW_BINARY_EXPRESSION);
		createEReference(dwBinaryExpressionEClass, DW_BINARY_EXPRESSION__OPERAND1);
		createEReference(dwBinaryExpressionEClass, DW_BINARY_EXPRESSION__OPERAND2);

		dwAndExpressionEClass = createEClass(DW_AND_EXPRESSION);

		dwOrExpressionEClass = createEClass(DW_OR_EXPRESSION);

		dwImpliesExpressionEClass = createEClass(DW_IMPLIES_EXPRESSION);

		dwEquivalenceExpressionEClass = createEClass(DW_EQUIVALENCE_EXPRESSION);

		dwSetExpressionEClass = createEClass(DW_SET_EXPRESSION);
		createEReference(dwSetExpressionEClass, DW_SET_EXPRESSION__OPERANDS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		DwFeaturePackage theDwFeaturePackage = (DwFeaturePackage)EPackage.Registry.INSTANCE.getEPackage(DwFeaturePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		dwAtomicExpressionEClass.getESuperTypes().add(this.getDwExpression());
		dwFeatureReferenceExpressionEClass.getESuperTypes().add(this.getDwAtomicExpression());
		dwBooleanValueExpressionEClass.getESuperTypes().add(this.getDwAtomicExpression());
		dwUnaryExpressionEClass.getESuperTypes().add(this.getDwExpression());
		dwNestedExpressionEClass.getESuperTypes().add(this.getDwUnaryExpression());
		dwNotExpressionEClass.getESuperTypes().add(this.getDwUnaryExpression());
		dwBinaryExpressionEClass.getESuperTypes().add(this.getDwExpression());
		dwAndExpressionEClass.getESuperTypes().add(this.getDwBinaryExpression());
		dwOrExpressionEClass.getESuperTypes().add(this.getDwBinaryExpression());
		dwImpliesExpressionEClass.getESuperTypes().add(this.getDwBinaryExpression());
		dwEquivalenceExpressionEClass.getESuperTypes().add(this.getDwBinaryExpression());
		dwSetExpressionEClass.getESuperTypes().add(this.getDwExpression());

		// Initialize classes, features, and operations; add parameters
		initEClass(dwExpressionEClass, DwExpression.class, "DwExpression", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dwAtomicExpressionEClass, DwAtomicExpression.class, "DwAtomicExpression", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dwFeatureReferenceExpressionEClass, DwFeatureReferenceExpression.class, "DwFeatureReferenceExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwFeatureReferenceExpression_Feature(), theDwFeaturePackage.getDwFeature(), null, "feature", null, 1, 1, DwFeatureReferenceExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwBooleanValueExpressionEClass, DwBooleanValueExpression.class, "DwBooleanValueExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDwBooleanValueExpression_Value(), ecorePackage.getEBoolean(), "value", null, 1, 1, DwBooleanValueExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwUnaryExpressionEClass, DwUnaryExpression.class, "DwUnaryExpression", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwUnaryExpression_Operand(), this.getDwExpression(), null, "operand", null, 1, 1, DwUnaryExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwNestedExpressionEClass, DwNestedExpression.class, "DwNestedExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dwNotExpressionEClass, DwNotExpression.class, "DwNotExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dwBinaryExpressionEClass, DwBinaryExpression.class, "DwBinaryExpression", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwBinaryExpression_Operand1(), this.getDwExpression(), null, "operand1", null, 1, 1, DwBinaryExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDwBinaryExpression_Operand2(), this.getDwExpression(), null, "operand2", null, 1, 1, DwBinaryExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dwAndExpressionEClass, DwAndExpression.class, "DwAndExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dwOrExpressionEClass, DwOrExpression.class, "DwOrExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dwImpliesExpressionEClass, DwImpliesExpression.class, "DwImpliesExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dwEquivalenceExpressionEClass, DwEquivalenceExpression.class, "DwEquivalenceExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dwSetExpressionEClass, DwSetExpression.class, "DwSetExpression", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDwSetExpression_Operands(), this.getDwExpression(), null, "operands", null, 1, -1, DwSetExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //DwExpressionPackageImpl
