/**
 */
package de.darwinspl.expression.impl;

import de.darwinspl.expression.DwAndExpression;
import de.darwinspl.expression.DwExpressionPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw And Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DwAndExpressionImpl extends DwBinaryExpressionImpl implements DwAndExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwAndExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwExpressionPackage.Literals.DW_AND_EXPRESSION;
	}

} //DwAndExpressionImpl
