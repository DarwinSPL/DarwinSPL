/**
 */
package de.darwinspl.expression.impl;

import de.darwinspl.expression.DwExpressionPackage;
import de.darwinspl.expression.DwNestedExpression;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Nested Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DwNestedExpressionImpl extends DwUnaryExpressionImpl implements DwNestedExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwNestedExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwExpressionPackage.Literals.DW_NESTED_EXPRESSION;
	}

} //DwNestedExpressionImpl
