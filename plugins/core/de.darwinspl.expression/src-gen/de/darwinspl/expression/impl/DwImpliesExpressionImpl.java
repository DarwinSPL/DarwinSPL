/**
 */
package de.darwinspl.expression.impl;

import de.darwinspl.expression.DwExpressionPackage;
import de.darwinspl.expression.DwImpliesExpression;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dw Implies Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DwImpliesExpressionImpl extends DwBinaryExpressionImpl implements DwImpliesExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DwImpliesExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DwExpressionPackage.Literals.DW_IMPLIES_EXPRESSION;
	}

} //DwImpliesExpressionImpl
