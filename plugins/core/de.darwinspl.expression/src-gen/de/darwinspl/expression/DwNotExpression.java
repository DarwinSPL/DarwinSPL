/**
 */
package de.darwinspl.expression;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Not Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.expression.DwExpressionPackage#getDwNotExpression()
 * @model
 * @generated
 */
public interface DwNotExpression extends DwUnaryExpression {
} // DwNotExpression
