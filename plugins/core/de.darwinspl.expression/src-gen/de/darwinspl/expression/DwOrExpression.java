/**
 */
package de.darwinspl.expression;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Or Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.expression.DwExpressionPackage#getDwOrExpression()
 * @model
 * @generated
 */
public interface DwOrExpression extends DwBinaryExpression {
} // DwOrExpression
