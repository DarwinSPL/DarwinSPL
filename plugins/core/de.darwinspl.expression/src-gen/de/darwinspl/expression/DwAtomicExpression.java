/**
 */
package de.darwinspl.expression;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Atomic Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.expression.DwExpressionPackage#getDwAtomicExpression()
 * @model abstract="true"
 * @generated
 */
public interface DwAtomicExpression extends DwExpression {
} // DwAtomicExpression
