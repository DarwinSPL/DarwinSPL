/**
 */
package de.darwinspl.expression;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw And Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.expression.DwExpressionPackage#getDwAndExpression()
 * @model
 * @generated
 */
public interface DwAndExpression extends DwBinaryExpression {
} // DwAndExpression
