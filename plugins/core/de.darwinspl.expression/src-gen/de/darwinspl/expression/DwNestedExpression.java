/**
 */
package de.darwinspl.expression;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Nested Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.expression.DwExpressionPackage#getDwNestedExpression()
 * @model
 * @generated
 */
public interface DwNestedExpression extends DwUnaryExpression {
} // DwNestedExpression
