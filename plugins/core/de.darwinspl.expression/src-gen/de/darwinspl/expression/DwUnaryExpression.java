/**
 */
package de.darwinspl.expression;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Unary Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.expression.DwUnaryExpression#getOperand <em>Operand</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.expression.DwExpressionPackage#getDwUnaryExpression()
 * @model abstract="true"
 * @generated
 */
public interface DwUnaryExpression extends DwExpression {
	/**
	 * Returns the value of the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operand</em>' containment reference.
	 * @see #setOperand(DwExpression)
	 * @see de.darwinspl.expression.DwExpressionPackage#getDwUnaryExpression_Operand()
	 * @model containment="true" required="true"
	 * @generated
	 */
	DwExpression getOperand();

	/**
	 * Sets the value of the '{@link de.darwinspl.expression.DwUnaryExpression#getOperand <em>Operand</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operand</em>' containment reference.
	 * @see #getOperand()
	 * @generated
	 */
	void setOperand(DwExpression value);

} // DwUnaryExpression
