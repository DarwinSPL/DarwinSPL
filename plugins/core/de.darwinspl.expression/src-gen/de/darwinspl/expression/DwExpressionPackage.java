/**
 */
package de.darwinspl.expression;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.darwinspl.expression.DwExpressionFactory
 * @model kind="package"
 * @generated
 */
public interface DwExpressionPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "expression";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.darwinspl.de/expression/2.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "expression";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DwExpressionPackage eINSTANCE = de.darwinspl.expression.impl.DwExpressionPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.darwinspl.expression.impl.DwExpressionImpl <em>Dw Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.expression.impl.DwExpressionImpl
	 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwExpression()
	 * @generated
	 */
	int DW_EXPRESSION = 0;

	/**
	 * The number of structural features of the '<em>Dw Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_EXPRESSION_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Dw Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_EXPRESSION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.expression.impl.DwAtomicExpressionImpl <em>Dw Atomic Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.expression.impl.DwAtomicExpressionImpl
	 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwAtomicExpression()
	 * @generated
	 */
	int DW_ATOMIC_EXPRESSION = 1;

	/**
	 * The number of structural features of the '<em>Dw Atomic Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ATOMIC_EXPRESSION_FEATURE_COUNT = DW_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dw Atomic Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_ATOMIC_EXPRESSION_OPERATION_COUNT = DW_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.expression.impl.DwFeatureReferenceExpressionImpl <em>Dw Feature Reference Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.expression.impl.DwFeatureReferenceExpressionImpl
	 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwFeatureReferenceExpression()
	 * @generated
	 */
	int DW_FEATURE_REFERENCE_EXPRESSION = 2;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_REFERENCE_EXPRESSION__FEATURE = DW_ATOMIC_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Dw Feature Reference Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_REFERENCE_EXPRESSION_FEATURE_COUNT = DW_ATOMIC_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Dw Feature Reference Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_FEATURE_REFERENCE_EXPRESSION_OPERATION_COUNT = DW_ATOMIC_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.expression.impl.DwBooleanValueExpressionImpl <em>Dw Boolean Value Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.expression.impl.DwBooleanValueExpressionImpl
	 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwBooleanValueExpression()
	 * @generated
	 */
	int DW_BOOLEAN_VALUE_EXPRESSION = 3;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_BOOLEAN_VALUE_EXPRESSION__VALUE = DW_ATOMIC_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Dw Boolean Value Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_BOOLEAN_VALUE_EXPRESSION_FEATURE_COUNT = DW_ATOMIC_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Dw Boolean Value Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_BOOLEAN_VALUE_EXPRESSION_OPERATION_COUNT = DW_ATOMIC_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.expression.impl.DwUnaryExpressionImpl <em>Dw Unary Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.expression.impl.DwUnaryExpressionImpl
	 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwUnaryExpression()
	 * @generated
	 */
	int DW_UNARY_EXPRESSION = 4;

	/**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_UNARY_EXPRESSION__OPERAND = DW_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Dw Unary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_UNARY_EXPRESSION_FEATURE_COUNT = DW_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Dw Unary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_UNARY_EXPRESSION_OPERATION_COUNT = DW_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.expression.impl.DwNestedExpressionImpl <em>Dw Nested Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.expression.impl.DwNestedExpressionImpl
	 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwNestedExpression()
	 * @generated
	 */
	int DW_NESTED_EXPRESSION = 5;

	/**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NESTED_EXPRESSION__OPERAND = DW_UNARY_EXPRESSION__OPERAND;

	/**
	 * The number of structural features of the '<em>Dw Nested Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NESTED_EXPRESSION_FEATURE_COUNT = DW_UNARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dw Nested Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NESTED_EXPRESSION_OPERATION_COUNT = DW_UNARY_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.expression.impl.DwNotExpressionImpl <em>Dw Not Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.expression.impl.DwNotExpressionImpl
	 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwNotExpression()
	 * @generated
	 */
	int DW_NOT_EXPRESSION = 6;

	/**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NOT_EXPRESSION__OPERAND = DW_UNARY_EXPRESSION__OPERAND;

	/**
	 * The number of structural features of the '<em>Dw Not Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NOT_EXPRESSION_FEATURE_COUNT = DW_UNARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dw Not Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_NOT_EXPRESSION_OPERATION_COUNT = DW_UNARY_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.expression.impl.DwBinaryExpressionImpl <em>Dw Binary Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.expression.impl.DwBinaryExpressionImpl
	 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwBinaryExpression()
	 * @generated
	 */
	int DW_BINARY_EXPRESSION = 7;

	/**
	 * The feature id for the '<em><b>Operand1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_BINARY_EXPRESSION__OPERAND1 = DW_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Operand2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_BINARY_EXPRESSION__OPERAND2 = DW_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Dw Binary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_BINARY_EXPRESSION_FEATURE_COUNT = DW_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Dw Binary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_BINARY_EXPRESSION_OPERATION_COUNT = DW_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.expression.impl.DwAndExpressionImpl <em>Dw And Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.expression.impl.DwAndExpressionImpl
	 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwAndExpression()
	 * @generated
	 */
	int DW_AND_EXPRESSION = 8;

	/**
	 * The feature id for the '<em><b>Operand1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_AND_EXPRESSION__OPERAND1 = DW_BINARY_EXPRESSION__OPERAND1;

	/**
	 * The feature id for the '<em><b>Operand2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_AND_EXPRESSION__OPERAND2 = DW_BINARY_EXPRESSION__OPERAND2;

	/**
	 * The number of structural features of the '<em>Dw And Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_AND_EXPRESSION_FEATURE_COUNT = DW_BINARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dw And Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_AND_EXPRESSION_OPERATION_COUNT = DW_BINARY_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.expression.impl.DwOrExpressionImpl <em>Dw Or Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.expression.impl.DwOrExpressionImpl
	 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwOrExpression()
	 * @generated
	 */
	int DW_OR_EXPRESSION = 9;

	/**
	 * The feature id for the '<em><b>Operand1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_OR_EXPRESSION__OPERAND1 = DW_BINARY_EXPRESSION__OPERAND1;

	/**
	 * The feature id for the '<em><b>Operand2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_OR_EXPRESSION__OPERAND2 = DW_BINARY_EXPRESSION__OPERAND2;

	/**
	 * The number of structural features of the '<em>Dw Or Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_OR_EXPRESSION_FEATURE_COUNT = DW_BINARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dw Or Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_OR_EXPRESSION_OPERATION_COUNT = DW_BINARY_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.expression.impl.DwImpliesExpressionImpl <em>Dw Implies Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.expression.impl.DwImpliesExpressionImpl
	 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwImpliesExpression()
	 * @generated
	 */
	int DW_IMPLIES_EXPRESSION = 10;

	/**
	 * The feature id for the '<em><b>Operand1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_IMPLIES_EXPRESSION__OPERAND1 = DW_BINARY_EXPRESSION__OPERAND1;

	/**
	 * The feature id for the '<em><b>Operand2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_IMPLIES_EXPRESSION__OPERAND2 = DW_BINARY_EXPRESSION__OPERAND2;

	/**
	 * The number of structural features of the '<em>Dw Implies Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_IMPLIES_EXPRESSION_FEATURE_COUNT = DW_BINARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dw Implies Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_IMPLIES_EXPRESSION_OPERATION_COUNT = DW_BINARY_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.expression.impl.DwEquivalenceExpressionImpl <em>Dw Equivalence Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.expression.impl.DwEquivalenceExpressionImpl
	 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwEquivalenceExpression()
	 * @generated
	 */
	int DW_EQUIVALENCE_EXPRESSION = 11;

	/**
	 * The feature id for the '<em><b>Operand1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_EQUIVALENCE_EXPRESSION__OPERAND1 = DW_BINARY_EXPRESSION__OPERAND1;

	/**
	 * The feature id for the '<em><b>Operand2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_EQUIVALENCE_EXPRESSION__OPERAND2 = DW_BINARY_EXPRESSION__OPERAND2;

	/**
	 * The number of structural features of the '<em>Dw Equivalence Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_EQUIVALENCE_EXPRESSION_FEATURE_COUNT = DW_BINARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dw Equivalence Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_EQUIVALENCE_EXPRESSION_OPERATION_COUNT = DW_BINARY_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.darwinspl.expression.impl.DwSetExpressionImpl <em>Dw Set Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.darwinspl.expression.impl.DwSetExpressionImpl
	 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwSetExpression()
	 * @generated
	 */
	int DW_SET_EXPRESSION = 12;

	/**
	 * The feature id for the '<em><b>Operands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_SET_EXPRESSION__OPERANDS = DW_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Dw Set Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_SET_EXPRESSION_FEATURE_COUNT = DW_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Dw Set Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DW_SET_EXPRESSION_OPERATION_COUNT = DW_EXPRESSION_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link de.darwinspl.expression.DwExpression <em>Dw Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Expression</em>'.
	 * @see de.darwinspl.expression.DwExpression
	 * @generated
	 */
	EClass getDwExpression();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.expression.DwAtomicExpression <em>Dw Atomic Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Atomic Expression</em>'.
	 * @see de.darwinspl.expression.DwAtomicExpression
	 * @generated
	 */
	EClass getDwAtomicExpression();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.expression.DwFeatureReferenceExpression <em>Dw Feature Reference Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Feature Reference Expression</em>'.
	 * @see de.darwinspl.expression.DwFeatureReferenceExpression
	 * @generated
	 */
	EClass getDwFeatureReferenceExpression();

	/**
	 * Returns the meta object for the reference '{@link de.darwinspl.expression.DwFeatureReferenceExpression#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Feature</em>'.
	 * @see de.darwinspl.expression.DwFeatureReferenceExpression#getFeature()
	 * @see #getDwFeatureReferenceExpression()
	 * @generated
	 */
	EReference getDwFeatureReferenceExpression_Feature();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.expression.DwBooleanValueExpression <em>Dw Boolean Value Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Boolean Value Expression</em>'.
	 * @see de.darwinspl.expression.DwBooleanValueExpression
	 * @generated
	 */
	EClass getDwBooleanValueExpression();

	/**
	 * Returns the meta object for the attribute '{@link de.darwinspl.expression.DwBooleanValueExpression#isValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see de.darwinspl.expression.DwBooleanValueExpression#isValue()
	 * @see #getDwBooleanValueExpression()
	 * @generated
	 */
	EAttribute getDwBooleanValueExpression_Value();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.expression.DwUnaryExpression <em>Dw Unary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Unary Expression</em>'.
	 * @see de.darwinspl.expression.DwUnaryExpression
	 * @generated
	 */
	EClass getDwUnaryExpression();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.expression.DwUnaryExpression#getOperand <em>Operand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operand</em>'.
	 * @see de.darwinspl.expression.DwUnaryExpression#getOperand()
	 * @see #getDwUnaryExpression()
	 * @generated
	 */
	EReference getDwUnaryExpression_Operand();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.expression.DwNestedExpression <em>Dw Nested Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Nested Expression</em>'.
	 * @see de.darwinspl.expression.DwNestedExpression
	 * @generated
	 */
	EClass getDwNestedExpression();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.expression.DwNotExpression <em>Dw Not Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Not Expression</em>'.
	 * @see de.darwinspl.expression.DwNotExpression
	 * @generated
	 */
	EClass getDwNotExpression();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.expression.DwBinaryExpression <em>Dw Binary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Binary Expression</em>'.
	 * @see de.darwinspl.expression.DwBinaryExpression
	 * @generated
	 */
	EClass getDwBinaryExpression();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.expression.DwBinaryExpression#getOperand1 <em>Operand1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operand1</em>'.
	 * @see de.darwinspl.expression.DwBinaryExpression#getOperand1()
	 * @see #getDwBinaryExpression()
	 * @generated
	 */
	EReference getDwBinaryExpression_Operand1();

	/**
	 * Returns the meta object for the containment reference '{@link de.darwinspl.expression.DwBinaryExpression#getOperand2 <em>Operand2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operand2</em>'.
	 * @see de.darwinspl.expression.DwBinaryExpression#getOperand2()
	 * @see #getDwBinaryExpression()
	 * @generated
	 */
	EReference getDwBinaryExpression_Operand2();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.expression.DwAndExpression <em>Dw And Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw And Expression</em>'.
	 * @see de.darwinspl.expression.DwAndExpression
	 * @generated
	 */
	EClass getDwAndExpression();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.expression.DwOrExpression <em>Dw Or Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Or Expression</em>'.
	 * @see de.darwinspl.expression.DwOrExpression
	 * @generated
	 */
	EClass getDwOrExpression();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.expression.DwImpliesExpression <em>Dw Implies Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Implies Expression</em>'.
	 * @see de.darwinspl.expression.DwImpliesExpression
	 * @generated
	 */
	EClass getDwImpliesExpression();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.expression.DwEquivalenceExpression <em>Dw Equivalence Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Equivalence Expression</em>'.
	 * @see de.darwinspl.expression.DwEquivalenceExpression
	 * @generated
	 */
	EClass getDwEquivalenceExpression();

	/**
	 * Returns the meta object for class '{@link de.darwinspl.expression.DwSetExpression <em>Dw Set Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dw Set Expression</em>'.
	 * @see de.darwinspl.expression.DwSetExpression
	 * @generated
	 */
	EClass getDwSetExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link de.darwinspl.expression.DwSetExpression#getOperands <em>Operands</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operands</em>'.
	 * @see de.darwinspl.expression.DwSetExpression#getOperands()
	 * @see #getDwSetExpression()
	 * @generated
	 */
	EReference getDwSetExpression_Operands();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DwExpressionFactory getDwExpressionFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.darwinspl.expression.impl.DwExpressionImpl <em>Dw Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.expression.impl.DwExpressionImpl
		 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwExpression()
		 * @generated
		 */
		EClass DW_EXPRESSION = eINSTANCE.getDwExpression();

		/**
		 * The meta object literal for the '{@link de.darwinspl.expression.impl.DwAtomicExpressionImpl <em>Dw Atomic Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.expression.impl.DwAtomicExpressionImpl
		 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwAtomicExpression()
		 * @generated
		 */
		EClass DW_ATOMIC_EXPRESSION = eINSTANCE.getDwAtomicExpression();

		/**
		 * The meta object literal for the '{@link de.darwinspl.expression.impl.DwFeatureReferenceExpressionImpl <em>Dw Feature Reference Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.expression.impl.DwFeatureReferenceExpressionImpl
		 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwFeatureReferenceExpression()
		 * @generated
		 */
		EClass DW_FEATURE_REFERENCE_EXPRESSION = eINSTANCE.getDwFeatureReferenceExpression();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_FEATURE_REFERENCE_EXPRESSION__FEATURE = eINSTANCE.getDwFeatureReferenceExpression_Feature();

		/**
		 * The meta object literal for the '{@link de.darwinspl.expression.impl.DwBooleanValueExpressionImpl <em>Dw Boolean Value Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.expression.impl.DwBooleanValueExpressionImpl
		 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwBooleanValueExpression()
		 * @generated
		 */
		EClass DW_BOOLEAN_VALUE_EXPRESSION = eINSTANCE.getDwBooleanValueExpression();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DW_BOOLEAN_VALUE_EXPRESSION__VALUE = eINSTANCE.getDwBooleanValueExpression_Value();

		/**
		 * The meta object literal for the '{@link de.darwinspl.expression.impl.DwUnaryExpressionImpl <em>Dw Unary Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.expression.impl.DwUnaryExpressionImpl
		 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwUnaryExpression()
		 * @generated
		 */
		EClass DW_UNARY_EXPRESSION = eINSTANCE.getDwUnaryExpression();

		/**
		 * The meta object literal for the '<em><b>Operand</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_UNARY_EXPRESSION__OPERAND = eINSTANCE.getDwUnaryExpression_Operand();

		/**
		 * The meta object literal for the '{@link de.darwinspl.expression.impl.DwNestedExpressionImpl <em>Dw Nested Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.expression.impl.DwNestedExpressionImpl
		 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwNestedExpression()
		 * @generated
		 */
		EClass DW_NESTED_EXPRESSION = eINSTANCE.getDwNestedExpression();

		/**
		 * The meta object literal for the '{@link de.darwinspl.expression.impl.DwNotExpressionImpl <em>Dw Not Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.expression.impl.DwNotExpressionImpl
		 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwNotExpression()
		 * @generated
		 */
		EClass DW_NOT_EXPRESSION = eINSTANCE.getDwNotExpression();

		/**
		 * The meta object literal for the '{@link de.darwinspl.expression.impl.DwBinaryExpressionImpl <em>Dw Binary Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.expression.impl.DwBinaryExpressionImpl
		 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwBinaryExpression()
		 * @generated
		 */
		EClass DW_BINARY_EXPRESSION = eINSTANCE.getDwBinaryExpression();

		/**
		 * The meta object literal for the '<em><b>Operand1</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_BINARY_EXPRESSION__OPERAND1 = eINSTANCE.getDwBinaryExpression_Operand1();

		/**
		 * The meta object literal for the '<em><b>Operand2</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_BINARY_EXPRESSION__OPERAND2 = eINSTANCE.getDwBinaryExpression_Operand2();

		/**
		 * The meta object literal for the '{@link de.darwinspl.expression.impl.DwAndExpressionImpl <em>Dw And Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.expression.impl.DwAndExpressionImpl
		 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwAndExpression()
		 * @generated
		 */
		EClass DW_AND_EXPRESSION = eINSTANCE.getDwAndExpression();

		/**
		 * The meta object literal for the '{@link de.darwinspl.expression.impl.DwOrExpressionImpl <em>Dw Or Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.expression.impl.DwOrExpressionImpl
		 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwOrExpression()
		 * @generated
		 */
		EClass DW_OR_EXPRESSION = eINSTANCE.getDwOrExpression();

		/**
		 * The meta object literal for the '{@link de.darwinspl.expression.impl.DwImpliesExpressionImpl <em>Dw Implies Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.expression.impl.DwImpliesExpressionImpl
		 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwImpliesExpression()
		 * @generated
		 */
		EClass DW_IMPLIES_EXPRESSION = eINSTANCE.getDwImpliesExpression();

		/**
		 * The meta object literal for the '{@link de.darwinspl.expression.impl.DwEquivalenceExpressionImpl <em>Dw Equivalence Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.expression.impl.DwEquivalenceExpressionImpl
		 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwEquivalenceExpression()
		 * @generated
		 */
		EClass DW_EQUIVALENCE_EXPRESSION = eINSTANCE.getDwEquivalenceExpression();

		/**
		 * The meta object literal for the '{@link de.darwinspl.expression.impl.DwSetExpressionImpl <em>Dw Set Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.darwinspl.expression.impl.DwSetExpressionImpl
		 * @see de.darwinspl.expression.impl.DwExpressionPackageImpl#getDwSetExpression()
		 * @generated
		 */
		EClass DW_SET_EXPRESSION = eINSTANCE.getDwSetExpression();

		/**
		 * The meta object literal for the '<em><b>Operands</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DW_SET_EXPRESSION__OPERANDS = eINSTANCE.getDwSetExpression_Operands();

	}

} //DwExpressionPackage
