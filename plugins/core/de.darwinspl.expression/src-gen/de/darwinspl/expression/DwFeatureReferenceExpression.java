/**
 */
package de.darwinspl.expression;

import de.darwinspl.feature.DwFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Feature Reference Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.expression.DwFeatureReferenceExpression#getFeature <em>Feature</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.expression.DwExpressionPackage#getDwFeatureReferenceExpression()
 * @model
 * @generated
 */
public interface DwFeatureReferenceExpression extends DwAtomicExpression {
	/**
	 * Returns the value of the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature</em>' reference.
	 * @see #setFeature(DwFeature)
	 * @see de.darwinspl.expression.DwExpressionPackage#getDwFeatureReferenceExpression_Feature()
	 * @model required="true"
	 * @generated
	 */
	DwFeature getFeature();

	/**
	 * Sets the value of the '{@link de.darwinspl.expression.DwFeatureReferenceExpression#getFeature <em>Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature</em>' reference.
	 * @see #getFeature()
	 * @generated
	 */
	void setFeature(DwFeature value);

} // DwFeatureReferenceExpression
