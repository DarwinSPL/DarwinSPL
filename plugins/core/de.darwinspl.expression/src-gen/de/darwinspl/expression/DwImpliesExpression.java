/**
 */
package de.darwinspl.expression;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Implies Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.darwinspl.expression.DwExpressionPackage#getDwImpliesExpression()
 * @model
 * @generated
 */
public interface DwImpliesExpression extends DwBinaryExpression {
} // DwImpliesExpression
