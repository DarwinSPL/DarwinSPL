/**
 */
package de.darwinspl.expression;

import de.darwinspl.feature.DwTemporalFeatureModel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dw Feature Model Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.darwinspl.expression.DwFeatureModelReference#getTarget <em>Target</em>}</li>
 * </ul>
 *
 * @see de.darwinspl.expression.DwExpressionPackage#getDwFeatureModelReference()
 * @model
 * @generated
 */
public interface DwFeatureModelReference extends EObject {
	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(DwTemporalFeatureModel)
	 * @see de.darwinspl.expression.DwExpressionPackage#getDwFeatureModelReference_Target()
	 * @model required="true"
	 * @generated
	 */
	DwTemporalFeatureModel getTarget();

	/**
	 * Sets the value of the '{@link de.darwinspl.expression.DwFeatureModelReference#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(DwTemporalFeatureModel value);

} // DwFeatureModelReference
