package de.darwinspl.variant.deltaecore;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.deltaecore.core.decore.util.DEDeltaRequirementsCycleException;
import org.deltaecore.core.variant.DEVariantCreator;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;

import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.configuration.DwConfiguration;
import de.darwinspl.mapping.DwMappingModel;
import de.darwinspl.variant.DwVariantDerivationUnsuccessfullException;
import de.darwinspl.variant.DwVariantDeriver;

public class DwDeltaEcoreVariantDeriver implements DwVariantDeriver {

	private static List<String> SUPPORTED_FILE_EXTENSIONS = new ArrayList<String>( Arrays.asList("decore") ); 
	private static String NAME = "DarwinSPL Delta Ecore Variant Deriver";
	
	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public List<String> supportsFileExtensions() {
		return SUPPORTED_FILE_EXTENSIONS;
	}

	@Override
	public void deriveVariant(DwTemporalFeatureModel featureModel, DwMappingModel mappingModel,
			DwConfiguration configuration, List<IFile> artifacts, Date date, IFolder targetFolder) throws DwVariantDerivationUnsuccessfullException {
		
		DEVariantCreator deltaEcoreVariantCreator = new DEVariantCreator();
		try {
			deltaEcoreVariantCreator.createAndSaveVariantFromDeltaFiles(artifacts, targetFolder);
		} catch (DEDeltaRequirementsCycleException e) {
			throw new DwVariantDerivationUnsuccessfullException(e.getMessage());			
		}
	}




}
