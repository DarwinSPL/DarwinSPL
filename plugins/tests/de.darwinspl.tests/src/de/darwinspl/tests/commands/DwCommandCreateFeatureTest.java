package de.darwinspl.tests.commands;

import java.text.ParseException;
import java.util.Date;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.evolution.operation.interpreter.DwEvolutionOperationInterpreter;
import de.darwinspl.feature.operation.DwFeatureCreateOperation;
import de.darwinspl.feature.util.custom.DwSimpleFeatureFactoryCustom;
import de.darwinspl.feature.util.custom.DwFeatureUtil;
import de.darwinspl.tests.DwAbstractTestCase;

class DwCommandCreateFeatureTest extends DwAbstractTestCase {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * This method creates the expected result with help of existing utility classes:<br>
	 * - de.darwinspl.feature.util.custom.DwFeatureUtil<br>
	 * - de.darwinspl.feature.util.custom.DwFactoryCustom
	 */
	@BeforeEach
	protected void setUp() throws Exception {
		super.setUp();
		Date date = dateFormat.parse("13.1.2020");
		
		expectedModelAfter = DwSimpleFeatureFactoryCustom.createFeatureModelWithChild(date);
	}

	@AfterEach
	protected void tearDown() throws Exception {
	}


	/**
	 * This method creates some operations and executes them using the model under test.
	 * The result is then compared to the expected result before undoing the operations and
	 * comparing the resulting model to the initial model.
	 * <br><br>
	 * Helpful utility class instance:<br>
	 * - operationFactory (de.darwinspl.feature.evolution.operation.factory.DwSimpleEvolutionOperationFactory)
	 * <br><br>
	 * For further hints to operations and their use, have a look at editor command implementations
	 */
	@Test
	protected void test() throws ParseException {
		Date date = dateFormat.parse("13.1.2020");
		
		DwFeature rootFeature = DwFeatureUtil.getRootFeature(modelUnderTest, date);
		
		DwFeatureCreateOperation createEvoOp = operationFactory.createDwFeatureCreateOperation(modelUnderTest, rootFeature, false, date, true);
		
		DwEvolutionOperationInterpreter.executeWithoutAnalysis(createEvoOp);
		assertEquals(expectedModelAfter, modelUnderTest);
		
		DwEvolutionOperationInterpreter.undo(createEvoOp);
		assertEquals(initialModelBefore, modelUnderTest);
	}

}
