package de.darwinspl.tests.commands;

import java.text.ParseException;
import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.evolution.operation.interpreter.DwEvolutionOperationInterpreter;
import de.darwinspl.feature.operation.DwFeatureDeleteOperation;
import de.darwinspl.feature.util.custom.DwSimpleFeatureFactoryCustom;
import de.darwinspl.tests.DwAbstractTestCase;

public class DwCommandDeleteFeatureTest extends DwAbstractTestCase {
	
	DwFeature newFeature;
	
	@BeforeEach
	protected void setUp() throws Exception {
		super.setUp();
		Date date = dateFormat.parse("15.01.2020");
		
		modelUnderTest = DwSimpleFeatureFactoryCustom.createFeatureModelWithChild(date);
		initialModelBefore = DwSimpleFeatureFactoryCustom.createFeatureModelWithChild(date);
		
		newFeature = modelUnderTest.getFeatures().get(1);
	}
	
	@Test
	protected void test() throws ParseException {
		Date date = dateFormat.parse("15.01.2020");
		
		DwFeatureDeleteOperation deleteEvoOp = operationFactory.createDwFeatureDeleteOperation(newFeature.getFeatureModel(), newFeature, date, true);
		
		DwEvolutionOperationInterpreter.executeWithoutAnalysis(deleteEvoOp);
		assertEquals(expectedModelAfter, modelUnderTest);
		
		DwEvolutionOperationInterpreter.undo(deleteEvoOp);
		assertEquals(initialModelBefore, modelUnderTest);
	}

}
