package de.darwinspl.tests.commands;

import java.text.ParseException;
import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupComposition;
import de.darwinspl.feature.DwGroupTypeEnum;
import de.darwinspl.feature.DwParentFeatureChildGroupContainer;
import de.darwinspl.feature.evolution.operation.interpreter.DwEvolutionOperationInterpreter;
import de.darwinspl.feature.operation.DwFeatureDeleteOperation;
import de.darwinspl.feature.util.custom.DwSimpleFeatureFactoryCustom;
import de.darwinspl.feature.util.custom.DwFeatureUtil;
import de.darwinspl.tests.DwAbstractTestCase;

public class DwCommandDeleteFeatureWithChildTest extends DwAbstractTestCase {
	
	DwFeature parentFeature1;
	
	@BeforeEach
	protected void setUp() throws Exception {
		super.setUp();
		Date date = dateFormat.parse("22.02.2020");
		
		modelUnderTest = DwSimpleFeatureFactoryCustom.createFeatureModelWithChild(date);
		initialModelBefore = DwSimpleFeatureFactoryCustom.createFeatureModelWithChild(date);
		
		parentFeature1 = modelUnderTest.getFeatures().get(1);
		modelUnderTest = DwSimpleFeatureFactoryCustom.addFeatureChildUnderFeatureModel(modelUnderTest, date, parentFeature1);
		
		DwFeature parentFeature2 = initialModelBefore.getFeatures().get(1);
		initialModelBefore = DwSimpleFeatureFactoryCustom.addFeatureChildUnderFeatureModel(initialModelBefore, date, parentFeature2);
	}

	@Test
	protected void test() throws ParseException {
		Date date = dateFormat.parse("22.02.2020");

		parentFeature1 = modelUnderTest.getFeatures().get(1);
		
		DwFeatureDeleteOperation deleteEvoOp = operationFactory.createDwFeatureDeleteOperation(parentFeature1.getFeatureModel(), parentFeature1, date, true);
		
		DwEvolutionOperationInterpreter.executeWithoutAnalysis(deleteEvoOp);
		assertEquals(expectedModelAfter, modelUnderTest);
		
		DwEvolutionOperationInterpreter.undo(deleteEvoOp);
		assertEquals(initialModelBefore, modelUnderTest);
	}
}
