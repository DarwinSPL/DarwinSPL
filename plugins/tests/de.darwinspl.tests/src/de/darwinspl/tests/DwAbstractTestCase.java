package de.darwinspl.tests;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.eclipse.emf.ecore.EObject;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import de.darwinspl.feature.DwFeatureFactory;
import de.darwinspl.feature.DwGroupType;
import de.darwinspl.feature.DwName;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.evolution.operation.factory.DwSimpleEvolutionOperationFactory;
import de.darwinspl.feature.util.custom.DwSimpleFeatureFactoryCustom;
import de.darwinspl.temporal.DwTemporalInterval;

public abstract class DwAbstractTestCase {
	
	protected final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.M.yyyy");
	
	protected final DwFeatureFactory featureFactory = DwFeatureFactory.eINSTANCE;
	protected final DwSimpleEvolutionOperationFactory operationFactory = new DwSimpleEvolutionOperationFactory();

	protected DwTemporalFeatureModel modelUnderTest;

	protected DwTemporalFeatureModel initialModelBefore;
	protected DwTemporalFeatureModel expectedModelAfter;
	
	
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}
	
	
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}
	
	
	@AfterEach
	protected void tearDown() throws Exception {
	}
	
	
	/**
	 * This method creates the expected result with help of existing utility classes:<br>
	 * - de.darwinspl.feature.util.custom.DwFeatureUtil<br>
	 * - de.darwinspl.feature.util.custom.DwFactoryCustom
	 * <br><br>
	 * Override and then call this method implementation for convenient setup of all test related TFMs.
	 */
	protected void setUp() throws Exception {
		modelUnderTest = DwSimpleFeatureFactoryCustom.createNewFeatureModel(DwSimpleFeatureFactoryCustom.createNewFeature("Test Model"));
		initialModelBefore = DwSimpleFeatureFactoryCustom.createNewFeatureModel(DwSimpleFeatureFactoryCustom.createNewFeature("Test Model"));
		expectedModelAfter = DwSimpleFeatureFactoryCustom.createNewFeatureModel(DwSimpleFeatureFactoryCustom.createNewFeature("Test Model"));
	}
	
	
	/**
	 * This method should create some operations and execute them using the model under test.
	 * The result should then be compared to the expected result before undoing the operations and
	 * comparing the resulting model to the initial model.
	 * <br><br>
	 * Helpful utility class instance:<br>
	 * - operationFactory (de.darwinspl.feature.evolution.operation.factory.DwSimpleEvolutionOperationFactory)
	 * <br><br>
	 * For further hints to operations and their use, have a look at editor command implementations
	 */
	@Test
	protected void test() throws ParseException {
	}
	
	
	/**
	 * Assert that expected and actual are equal.<br>
	 * If both are null, they are considered <strong>not</strong> equal.<br>
	 * Custom implementation for an equals assertion between to DarwinSPL related EObjects that will be checked recursively.
	 * @param expected an expected EObject
	 * @param actual an EObject that is the result of a test
	 */
	protected void assertEquals(EObject expected, EObject actual) {
		assertNotNull(expected);
		assertNotNull(actual);
		
		if(!expected.getClass().equals(actual.getClass()))
			fail("EObject classes do not match! (" + expected.getClass().getName() + " vs. " + actual.getClass().getName());
		
		if(expected.eContents().size() != actual.eContents().size())
			fail("EObject contain different amount of child objects (eContents). (" + expected.getClass().getName() + ")");
		
		if(expected instanceof DwName) {
			Assertions.assertEquals(((DwName) expected).getName(), ((DwName) actual).getName(),
					"Names do not match: " + ((DwName) expected).getName() + " vs. " + ((DwName) actual).getName());
		}
		if(expected instanceof DwTemporalInterval) {
			Assertions.assertEquals(((DwTemporalInterval) expected).getFrom(), ((DwTemporalInterval) actual).getFrom(),
					"Starting dates do not match: " + ((DwTemporalInterval) expected).getFrom() + " vs. " + ((DwTemporalInterval) actual).getFrom());
			Assertions.assertEquals(((DwTemporalInterval) expected).getTo(), ((DwTemporalInterval) actual).getTo(),
					"End dates do not match: " + ((DwTemporalInterval) expected).getTo() + " vs. " + ((DwTemporalInterval) actual).getTo());
		}
		if(expected instanceof DwGroupType) {
			Assertions.assertEquals(((DwGroupType) expected).getType(), ((DwGroupType) actual).getType(),
					"Starting dates do not match: " + ((DwGroupType) expected).getType() + " vs. " + ((DwGroupType) actual).getType());
		}
		
		for(int i=0; i<expected.eContents().size(); i++) {
			assertEquals(expected.eContents().get(i), actual.eContents().get(i));
		}
	}
}
