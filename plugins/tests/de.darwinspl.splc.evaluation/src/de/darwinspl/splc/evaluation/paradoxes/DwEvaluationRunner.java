package de.darwinspl.splc.evaluation.paradoxes;

import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;

import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.evolution.operation.resource.DwEvolutionOperationModelResourceUtil;
import de.darwinspl.feature.operation.DwEvolutionOperation;
import de.darwinspl.feature.operation.DwEvolutionOperationModel;
import de.darwinspl.splc.evaluation.util.TestUtil;
import de.darwinspl.temporal.maude.analysis.DwEvolutionChecker;
import de.darwinspl.temporal.maude.analysis.DwEvolutionCheckerResultHook;
import de.darwinspl.temporal.maude.analysis.DwEvolutionCheckingResult;
import de.darwinspl.temporal.util.DwDateResolverUtil;

public class DwEvaluationRunner implements DwEvolutionCheckerResultHook {
	
	private static DwEvaluationStatistics statistics = new DwEvaluationStatistics();
	private static DwEvolutionOperation currentOperation;
	private static int missmatchCount;
	
	private DwTemporalFeatureModel tfm;
	private int numberOfOperationsToGenerate;
	
	private TestUtil utils;
	
	
	
	public DwEvaluationRunner(DwTemporalFeatureModel tfm, int numberOfOperationsToGenerate, long seed) {
		missmatchCount = 0;
		
		this.tfm = tfm;
		this.numberOfOperationsToGenerate = numberOfOperationsToGenerate;
		
		utils = new TestUtil(tfm, seed);
	}
	
	
	
	public DwEvaluationStatistics run(IProgressMonitor monitor) {
		monitor.beginTask("Loading operations...", 1);
		DwEvolutionOperationModel operationModel = DwEvolutionOperationModelResourceUtil.loadAndCacheOperationsModel(tfm);
		monitor.worked(1);
		
		List<DwEvolutionCheckerResultHook> actualObservers = DwEvolutionChecker.getInstance().getObservers();
		DwEvolutionChecker.getInstance().clearObservers();
		DwEvolutionChecker.getInstance().addObserver(this);
		
		statistics.clear();

		monitor.beginTask("Checking " + numberOfOperationsToGenerate + " randomly generated operations", numberOfOperationsToGenerate);
		long startTime = System.currentTimeMillis();
		
		int loopRuns = 0;
		try {
			while(loopRuns < numberOfOperationsToGenerate) {
				long passedTime = System.currentTimeMillis() - startTime;
				double percentage = (double)(loopRuns+1) / (double) numberOfOperationsToGenerate;
				long estimatedOverallDuration = (long) ((double) passedTime * (1.0d/percentage));
				
				monitor.subTask((loopRuns+1) + " / " + numberOfOperationsToGenerate + " (" + missmatchCount + ") - ETA: " + DwDateResolverUtil.getTimeAsString(estimatedOverallDuration-passedTime, false));
				
				currentOperation = utils.createRandomOperation();
				
				int beforeHash = utils.hashOperationModel(operationModel);
				DwEvolutionOperationModelResourceUtil.addOperationToOperationsModel(currentOperation);
				DwEvolutionChecker.getInstance().run(currentOperation, null);
				DwEvolutionOperationModelResourceUtil.undoOperation(currentOperation);
				int afterHash = utils.hashOperationModel(operationModel);
				if(beforeHash != afterHash)
					System.out.println("... weird, the operation model was changed after a test run!");
				
				if(monitor.isCanceled()) {
					statistics.setInfo("Canceled after " + (loopRuns+1) + " runs.");
					break;
				}
				
				loopRuns++;
				monitor.worked(1);
			}
			
			if(missmatchCount == 0) {
				statistics.setSuccess(true);
				statistics.setInfo(loopRuns + " random operations successfully run!");
			}
			else {
				statistics.setSuccess(false);
				statistics.setInfo(loopRuns + " random operations run. Found mismatches: " + missmatchCount);
			}
		}
		catch(Exception e) {
			statistics.setSuccess(false);
			statistics.setInfo("An Exception occured after " + (loopRuns+1) + " runs: " + e.getClass().getSimpleName());
			e.printStackTrace();
		}

		DwEvolutionChecker.getInstance().clearObservers();
		DwEvolutionChecker.getInstance().addObservers(actualObservers);
		
		DwEvolutionOperationModelResourceUtil.removeOperationModelFromCache(tfm);
		
		monitor.done();
		
		return statistics;
	}

	@Override
	public boolean isResultValid(DwEvolutionCheckingResult result) {
		if(result.isMaudeTimeout())
			// Connection problem
			return false;
		
//		if(Boolean.compare(result.isCrossCheckSuccess(), result.isMaudeSuccess()) != 0)
//			missmatchCount++;
		
		statistics.saveEntry(currentOperation, result);
		return false;
	}
	
}




