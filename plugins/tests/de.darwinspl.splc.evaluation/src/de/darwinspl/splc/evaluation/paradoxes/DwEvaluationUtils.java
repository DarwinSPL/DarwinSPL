package de.darwinspl.splc.evaluation.paradoxes;

import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.Path;

import de.darwinspl.common.eclipse.util.DwResourceUtil;
import de.darwinspl.splc.evaluation.paradoxes.DwEvaluationStatistics.Entry;
import de.darwinspl.temporal.maude.analysis.DwEvolutionCheckingResult;

public class DwEvaluationUtils {
	

	public static void writeResultLog(IFolder parentFolder, String result) {
		IFile inputFile = parentFolder.getFile(new Path("result.log")); 
		DwResourceUtil.writeToFile(inputFile, result);
	}



	public static void writeMissmatchLog(IFolder parentFolder, String failedOperations) {
		IFile inputFile = parentFolder.getFile(new Path("missmatches.log")); 
		DwResourceUtil.writeToFile(inputFile, failedOperations);
	}
	
	
	
	public static void writeFilesForR(IFolder parentFolder, List<Entry> entries) {
		String inputForR = getInputForR(entries);
		IFile inputFile = parentFolder.getFile(new Path("evaluation_results.dat")); 
		DwResourceUtil.writeToFile(inputFile, inputForR);
		
		String codeForR1 = getTimeCourseCodeForR(parentFolder);
		IFile codeFile1 = parentFolder.getFile(new Path("evaluation_results_time_course.R")); 
		DwResourceUtil.writeToFile(codeFile1, codeForR1);
		
		String codeForR2 = getBoxPlotCodeForR(parentFolder);
		IFile codeFile2 = parentFolder.getFile(new Path("evaluation_results_box_plot.R")); 
		DwResourceUtil.writeToFile(codeFile2, codeForR2);
	}

	private static String getInputForR(List<Entry> entries) {
		int numberOfEntries = entries.size();
		
		StringBuilder builder = new StringBuilder();
		builder.append("maude_online");
		builder.append("\t");
		builder.append("maude_offline");
		builder.append("\t");
		builder.append("crosscheck");
		builder.append("\n");

		for(int i=0; i<numberOfEntries; i++) {
			DwEvolutionCheckingResult result = entries.get(i).result;
			
			builder.append(result.getMaudeRuntimeOnline());
			builder.append("\t");
			builder.append(result.getMaudeRuntimeOffline());
			builder.append("\t");
			builder.append(result.getCrossCheckingRuntime());
			
			if(i<numberOfEntries-1)
				builder.append("\n");
		}
		
		return builder.toString();
	}
	

	private static String getBoxPlotCodeForR(IFolder parentFolder) {
		String parentPath = parentFolder.getRawLocation().toString();
		
		return "eval_data <- read.table(\"" + parentPath + "/evaluation_results.dat\", header=T, sep=\"\\t\") " + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"svg(filename=\"" + parentPath + "/evaluation_boxplot.svg\", height=10, width=20, pointsize=8)" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"boxplot(eval_data$maude_online,data=eval_data, ylim=c(0,900), main=\"Car Milage Data\", xlab=\"Runtime [ms]\", ylab=\"Checking Means\", horizontal=TRUE, outline=FALSE) " + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"# Turn off device driver (to flush output to svg)" + System.lineSeparator() + 
				"dev.off()";
	}
	
	
	private static String getTimeCourseCodeForR(IFolder parentFolder) {
		String parentPath = parentFolder.getRawLocation().toString();
		
		return "# Read values from tab-delimited .dat file" + System.lineSeparator() + 
				"eval_data <- read.table(\"" + parentPath + "/evaluation_results.dat\", header=T, sep=\"\\t\") " + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"# Compute the largest y value used in the data (or we could" + System.lineSeparator() + 
				"# just use range again)" + System.lineSeparator() + 
				"max_y <- max(eval_data)" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"# Define colors to be used for cars, trucks, suvs" + System.lineSeparator() + 
				"plot_colors <- c(\"blue\",\"red\",\"forestgreen\")" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"# Start PNG device driver to save output to figure.png" + System.lineSeparator() + 
				"# png(filename=\"" + parentPath + "/figure.png\", height=1000, width=2000, " + System.lineSeparator() + 
				"#  bg=\"white\")" + System.lineSeparator() + 
				"svg(filename=\"" + parentPath + "/evaluation_time_course.svg\", height=10, width=20, pointsize=8)" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"# Graph eval using y axis that ranges from 0 to max_y." + System.lineSeparator() + 
				"# Turn off axes and annotations (axis labels) so we can " + System.lineSeparator() + 
				"# specify them ourself" + System.lineSeparator() + 
				"plot(eval_data$maude_online, type=\"l\", col=plot_colors[1], " + System.lineSeparator() + 
				"   ylim=c(0,max_y), axes=FALSE, ann=FALSE)" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"axis(1, las=1)" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"# Make y axis with horizontal labels that display ticks at " + System.lineSeparator() + 
				"# every 4 marks. 4*0:max_y is equivalent to c(0,4,8,12)." + System.lineSeparator() + 
				"axis(2, las=1)" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"# Create box around plot" + System.lineSeparator() + 
				"box()" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"# Graph with red dashed line" + System.lineSeparator() + 
				"lines(eval_data$maude_offline, type=\"l\", pch=22, lty=2, " + System.lineSeparator() + 
				"   col=plot_colors[2])" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"# Graph with green dotted line" + System.lineSeparator() + 
				"lines(eval_data$crosscheck, type=\"l\", pch=23, lty=3, " + System.lineSeparator() + 
				"   col=plot_colors[3])" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"# Create a title with a bold/italic font" + System.lineSeparator() + 
				"title(main=\"Evaluation results\", col.main=\"red\", font.main=4)" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"# Label the x and y axes with dark green text" + System.lineSeparator() + 
				"title(xlab= \"Operation Number\", col.lab=rgb(0,0.5,0))" + System.lineSeparator() + 
				"title(ylab= \"Runtime [ms]\", col.lab=rgb(0,0.5,0))" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"# Create a legend at (1, max_y) with non-changed size (cex=1.0) " + System.lineSeparator() + 
				"# and uses the same line colors and points used by " + System.lineSeparator() + 
				"# the actual plots" + System.lineSeparator() + 
				"legend(1, max_y, names(eval_data), cex=1.0, col=plot_colors, pch=21:23, lty=1:3);" + System.lineSeparator() + 
				"   " + System.lineSeparator() + 
				"# Turn off device driver (to flush output to svg)" + System.lineSeparator() + 
				"dev.off()";
	}
	
}












