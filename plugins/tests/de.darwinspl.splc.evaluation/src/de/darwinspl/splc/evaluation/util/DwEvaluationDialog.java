package de.darwinspl.splc.evaluation.util;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class DwEvaluationDialog extends MessageDialog {
	
	private int loopRuns;
	private long seed = 60733231200000l;
	private boolean showSeed;

	public DwEvaluationDialog(Shell shell, int repititions, boolean showSeed) {
		super(shell, "SPLC Evaluation Setup", null, "Set up your test.", MessageDialog.QUESTION, new String[] { "Continue", "Cancel" }, 0);
		loopRuns = repititions;
		this.showSeed = showSeed;
	}
	
	@Override
	protected Control createContents(Composite parent) {
		Control control = super.createContents(parent);
		return control;
	}
	
	@Override
	public int open() {
		int result = super.open();
		
		if(result == 1)
			return -1;
		else
			return loopRuns;
	}

	public long getSeed() {
		return seed;
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout(1, false));
		
		GridData textLayoutData = new GridData(SWT.CENTER, SWT.CENTER, true, false);
		textLayoutData.widthHint = 200;
		textLayoutData.grabExcessHorizontalSpace = true;
		
		new Label(container, SWT.HORIZONTAL).setText("Choose a number of repititions:");
		final Text repetitionTextField = new Text(container, SWT.BORDER);
		repetitionTextField.setText(""+loopRuns);
		repetitionTextField.setLayoutData(textLayoutData);

		repetitionTextField.addVerifyListener(new VerifyListener() {
	        @Override
	        public void verifyText(VerifyEvent e) {
	            final String oldS = repetitionTextField.getText();
	            String newS = oldS.substring(0, e.start) + e.text + oldS.substring(e.end);

	            try {
	            	loopRuns = Integer.parseInt(newS);
	            }
	            catch(NumberFormatException ex) {
	            	e.doit = false;
	            }
	        }
	    });
		
		if(showSeed) {
			new Label(container, SWT.HORIZONTAL).setText("Optionally, set a number to use as test seed:");
			final Text seedTextField = new Text(container, SWT.BORDER);
			seedTextField.setText(""+seed);
			seedTextField.setLayoutData(textLayoutData);

			seedTextField.addVerifyListener(new VerifyListener() {
		        @Override
		        public void verifyText(VerifyEvent e) {
		            final String oldS = seedTextField.getText();
		            String newS = oldS.substring(0, e.start) + e.text + oldS.substring(e.end);

		            try {
		            	seed = Integer.parseInt(newS);
		            }
		            catch(NumberFormatException ex) {
		            	e.doit = false;
		            }
		        }
		    });
		}

		return container;
	}
	
}
