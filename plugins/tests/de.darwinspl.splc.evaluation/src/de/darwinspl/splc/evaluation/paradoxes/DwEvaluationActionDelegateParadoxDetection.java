package de.darwinspl.splc.evaluation.paradoxes;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.ICoreRunnable;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.internal.ObjectPluginAction;

import de.christophseidl.util.ecore.EcoreIOUtil;
import de.darwinspl.common.eclipse.ui.DwDialogUtil;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.splc.evaluation.util.DwEvaluationDialog;

public class DwEvaluationActionDelegateParadoxDetection implements IObjectActionDelegate {
	
	private Shell shell;

	public DwEvaluationActionDelegateParadoxDetection() {
		
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	@Override
	public void run(IAction action) {
		if(action instanceof ObjectPluginAction) {
			ISelection selection = ((ObjectPluginAction) action).getSelection();
			
			if(selection instanceof TreeSelection) {
				TreePath treePath = ((TreeSelection) selection).getPaths()[0];
				Object file = treePath.getSegment(treePath.getSegmentCount()-1);

				if(file instanceof IFile) {
					EObject tfm = EcoreIOUtil.loadModel((IFile) file);
					if(tfm instanceof DwTemporalFeatureModel) {
						
						DwEvaluationDialog dialog = new DwEvaluationDialog(shell, 100000, true);
						int probeSize = dialog.open();
						if(probeSize <= 0)
							return;
						long seed = dialog.getSeed();
						
						Job evalJob = Job.create("Evaluating \"" + ((IFile) file).getName() + "\"...", (ICoreRunnable) monitor -> {
							DwEvaluationRunner evaluationRunner = new DwEvaluationRunner((DwTemporalFeatureModel) tfm, probeSize, seed);
							DwEvaluationStatistics resultingStatistic = evaluationRunner.run(monitor);
							
							if(!resultingStatistic.getEntries().isEmpty()) {
								Display.getDefault().asyncExec(() -> {
//								System.out.println(resultingStatistic);
									int dialogType = resultingStatistic.isSuccess() ? MessageDialog.INFORMATION : MessageDialog.ERROR;
									DwDialogUtil.openDialog("Evaluation run finished", resultingStatistic.getInfo(), dialogType, "Close");
								});
							}
							
							int i=1;
							IFolder folderToWriteTo = null;
							while(folderToWriteTo == null || folderToWriteTo.exists()) {
								folderToWriteTo = ((IFile) file).getParent().getFolder(new Path("run-" + resultingStatistic.getEntries().size() + "-" + i));
								i++;
							}
							try {
								folderToWriteTo.create(true, true, monitor);
							} catch (CoreException e) {
								e.printStackTrace();
								folderToWriteTo = (IFolder) ((IFile) file).getParent();
							}
							
							monitor.beginTask("Writing evaluation results into files...", 2);
							monitor.subTask("Writing result log");
							DwEvaluationUtils.writeResultLog(folderToWriteTo, resultingStatistic.toString());
							monitor.worked(1);
//							monitor.subTask("Writing missmatches to file");
//							DwEvaluationUtils.writeMissmatchLog(folderToWriteTo, resultingStatistic.getFailedOperations());
//							monitor.worked(1);
							monitor.subTask("Writing files for R");
							DwEvaluationUtils.writeFilesForR(folderToWriteTo, resultingStatistic.getEntries());
							monitor.worked(1);
						});
						
						evalJob.setPriority(Job.INTERACTIVE);
						evalJob.schedule();
					}
				}
			}
		}
	}
	
	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		
	}

}









