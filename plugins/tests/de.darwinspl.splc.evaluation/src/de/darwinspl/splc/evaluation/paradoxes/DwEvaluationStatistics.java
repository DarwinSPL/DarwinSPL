package de.darwinspl.splc.evaluation.paradoxes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.darwinspl.feature.operation.DwEvolutionOperation;
import de.darwinspl.temporal.maude.analysis.DwEvolutionCheckingResult;

public class DwEvaluationStatistics {
	
	private List<Entry> entries = new LinkedList<>();
	private String info;
	private boolean success = false;
	
	public void saveEntry(DwEvolutionOperation operation, DwEvolutionCheckingResult result) {
		entries.add(new Entry(operation, result));
	}
	
	public List<Entry> getEntries() {
		return Collections.unmodifiableList(this.entries);
	}
	
	public void clear() {
		this.entries.clear();
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String string) {
		info = string;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	
	
	public static class Entry {
		public DwEvolutionOperation operation;
		public DwEvolutionCheckingResult result;
		
		public Entry(DwEvolutionOperation operation, DwEvolutionCheckingResult result) {
			this.operation = operation;
			this.result = result;
		}
		
		@Override
		public String toString() {
			String str = "";
			str += "-----------------------------------------------------------------------" + System.lineSeparator();
			str += result + System.lineSeparator();
			str += " - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - " + System.lineSeparator();
			str += operation + System.lineSeparator();
			str += "-----------------------------------------------------------------------";
			return str;
		}
	}



	public String toString() {
		int numberOfEntries = entries.size();
		
		Map<String, List<Entry>> operationsToResultMapping = new HashMap<>();
		
		for(int i=0; i<numberOfEntries; i++) {
			Entry entry = entries.get(i);
			DwEvolutionOperation operation = entry.operation;
			
			String operationName = operation.getClass().getSimpleName();
			List<Entry> operations = operationsToResultMapping.get(operationName);
			if(operations == null) {
				operations = new ArrayList<>();
				operationsToResultMapping.put(operationName, operations);
			}
			operations.add(entry);
		}
		
		StringBuilder builder = new StringBuilder();

		builder.append("OVERALL STATISTICS");
		builder.append(System.lineSeparator());
		builder.append(entryListToString(entries, false));
		builder.append(System.lineSeparator());
		
		builder.append("Opperation Occurences");
		builder.append(System.lineSeparator());
		for(String operationName : operationsToResultMapping.keySet()) {
			builder.append(operationName.toUpperCase());
			builder.append(System.lineSeparator());
			builder.append(entryListToString(operationsToResultMapping.get(operationName), true));
			builder.append(System.lineSeparator());
		}
		
		return builder.toString();
	}
	
	private String entryListToString(List<Entry> list, boolean printFailedOperations) {
		int numberOfEntries = list.size();
		
		int positives = 0;
		int negatives = 0;
//		int disagreedMaudePositive = 0;
//		int disagreedCrossCheckPositive = 0;

		int[] maudeOnlineTimes = new int[numberOfEntries];
		int[] maudeOfflineTimes = new int[numberOfEntries];
//		int[] crossCheckTimes = new int[numberOfEntries];
		
		StringBuilder failedOperationsBuilder = new StringBuilder();
		
		for(int i=0; i<numberOfEntries; i++) {
			Entry entry = list.get(i);
			DwEvolutionCheckingResult result = entry.result;
			
//			if(Boolean.compare(result.isCrossCheckSuccess(), result.isMaudeSuccess()) != 0) {
//				disagreedMaudePositive += result.isMaudeSuccess() ? 0 : 1;
//				disagreedCrossCheckPositive += result.isCrossCheckSuccess() ? 0 : 1;
//				
//				failedOperationsBuilder.append(entry);
//			}
//			else {
				positives += result.isSuccess() ? 0 : 1;
				negatives += result.isSuccess() ? 1 : 0;
//			}

			maudeOnlineTimes[i] = result.getMaudeRuntimeOnline();
			maudeOfflineTimes[i] = result.getMaudeRuntimeOffline();
//			crossCheckTimes[i] = result.getCrossCheckingRuntime();
		}

		StringBuilder builder = new StringBuilder();
		
		builder.append("Positives: " + positives);
		builder.append(System.lineSeparator());
		builder.append("Negatives: " + negatives);
		builder.append(System.lineSeparator());
		builder.append(System.lineSeparator());
//		builder.append("Maude Positives (where Cross-Check did not detect a paradox): " + disagreedMaudePositive);
//		builder.append(System.lineSeparator());
//		builder.append("Cross-Checked Positives (where Maude did not detect a paradox): " + disagreedCrossCheckPositive);
//		builder.append(System.lineSeparator());
//		builder.append(System.lineSeparator());

		builder.append("Maude Online Calculation");
		builder.append(System.lineSeparator());
		builder.append(getRuntimeStatistics(maudeOnlineTimes));
		builder.append(System.lineSeparator());
		builder.append("Maude Offline Calculation");
		builder.append(System.lineSeparator());
		builder.append(getRuntimeStatistics(maudeOfflineTimes));
		builder.append(System.lineSeparator());
//		builder.append("Cross-Check Calculation");
//		builder.append(System.lineSeparator());
//		builder.append(getRuntimeStatistics(crossCheckTimes));
//		builder.append(System.lineSeparator());
		
		if(printFailedOperations)
			builder.append(failedOperationsBuilder.toString());
		
		builder.append(System.lineSeparator());
		
		return builder.toString();
	}
	
	public static String getRuntimeStatistics(int[] runtimeArray) {
		int numberOfEntries = runtimeArray.length;
		if(numberOfEntries == 0)
			return "No data available";
		
		int[] runtimeArraySorted = Arrays.copyOf(runtimeArray, numberOfEntries);
		Arrays.sort(runtimeArraySorted);
		
		double averageRuntime = 0.d;
		for(int runtime : runtimeArray) {
			averageRuntime += (double) runtime;
		}
		averageRuntime /= (double) numberOfEntries;
		
		StringBuilder builder = new StringBuilder();
		builder.append("\tLongest Runtime: " + runtimeArraySorted[numberOfEntries-1]);
		builder.append(System.lineSeparator());
		builder.append("\tShortest Runtime: " + runtimeArraySorted[0]);
		builder.append(System.lineSeparator());
		builder.append("\tAverage Runtime: " + averageRuntime);
		builder.append(System.lineSeparator());
		builder.append("\tMedian Runtime: " + runtimeArraySorted[numberOfEntries/2]);
		
		return builder.toString();
	}
	
	
	
//	public String getFailedOperations() {
//		int numberOfEntries = entries.size();
//		StringBuilder failedOperationsBuilder = new StringBuilder();
//		
//		for(int i=0; i<numberOfEntries; i++) {
//			Entry entry = entries.get(i);
//			DwEvolutionCheckingResult result = entry.result;
//			
//			if(Boolean.compare(result.isCrossCheckSuccess(), result.isMaudeSuccess()) != 0) {
//				failedOperationsBuilder.append(entry);
//			}
//		}
//		
//		return failedOperationsBuilder.toString();
//	}

}












