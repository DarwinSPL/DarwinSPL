package de.darwinspl.splc.evaluation.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;

import de.darwinspl.feature.DwFeature;
import de.darwinspl.feature.DwFeatureTypeEnum;
import de.darwinspl.feature.DwGroup;
import de.darwinspl.feature.DwGroupTypeEnum;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.evolution.operation.factory.DwSimpleEvolutionOperationFactory;
import de.darwinspl.feature.operation.DwEvolutionOperation;
import de.darwinspl.feature.operation.DwEvolutionOperationModel;
import de.darwinspl.feature.util.custom.DwFeatureUtil;
import de.darwinspl.temporal.DwTemporalElement;
import de.darwinspl.temporal.util.DwEvolutionUtil;

public class TestUtil {
	private Random rand;
	private DwTemporalFeatureModel tfm;
	
	private List<Date> allDates;
	private List<Date> allIntermediateDates;
	
	private List<String> operationNames;
	
	private List<DwGroupTypeEnum> allGroupTypes;
	
	private DwSimpleEvolutionOperationFactory operationFactory;
	
	
	
	public TestUtil(DwTemporalFeatureModel tfm, long seed) {
		rand = new Random(seed); 
		
		operationFactory = new DwSimpleEvolutionOperationFactory();
		this.tfm = tfm;
		
		operationNames = Arrays.asList(
			new String[] {
					"FeatureCreateOperation",
					"FeatureDeleteOperation",
					"FeatureMoveOperation",
					"FeatureRenameOperation",
					"FeatureTypeChangeOperation",
					
					"GroupDeleteOperation",
					"GroupMoveOperation",
					"GroupTypeChangeOperation"
			}
		);
		
		allGroupTypes = Arrays.asList(
			new DwGroupTypeEnum[] {
					DwGroupTypeEnum.AND,
					DwGroupTypeEnum.ALTERNATIVE,
					DwGroupTypeEnum.OR
			}
		);
		
		allDates = DwEvolutionUtil.collectDates(tfm);
		
		allIntermediateDates = new ArrayList<>(allDates);
		allIntermediateDates.remove(allIntermediateDates.size()-1);
	}
	
	
	
	public <T> T getRandomElementFromList(List<T> list) {
        return list.get(rand.nextInt(list.size()));
	}
	
	

	public Date getRandomIntermediateDate() {
		return getRandomElementFromList(allIntermediateDates);
	}
	
	public List<Date> getAllIntermediateDates() {
		return allIntermediateDates;
	}
	
	
	
	public String getRandomOperationName() {
		return getRandomElementFromList(operationNames);
	}
	
	public DwEvolutionOperation createRandomOperation() {
		String randomOperationName = getRandomOperationName();
		
		if(randomOperationName.equals("FeatureCreateOperation"))
			return createFeatureCreateOperation();
		if(randomOperationName.equals("FeatureDeleteOperation"))
			return createFeatureDeleteOperation();
		if(randomOperationName.equals("FeatureMoveOperation"))
			return createFeatureMoveOperation();
		if(randomOperationName.equals("FeatureRenameOperation"))
			return createFeatureRenameOperation();
		if(randomOperationName.equals("FeatureTypeChangeOperation"))
			return createFeatureTypeChangeOperation();
		
		if(randomOperationName.equals("GroupDeleteOperation"))
			return createGroupDeleteOperation();
		if(randomOperationName.equals("GroupMoveOperation"))
			return createGroupMoveOperation();
		if(randomOperationName.equals("GroupTypeChangeOperation"))
			return createGroupTypeChangeOperation();
		
		throw new UnsupportedOperationException("Unrecognized Operation Name ...");
	}
	
	
	
	private DwEvolutionOperation createSpecificOperation() {
		Date date = getAllIntermediateDates().get(1); // 2017-10-20T00:00:00
//		Date date = getAllIntermediateDates().get(3); // 2017-12-22T00:00:00
//		Date date = getAllIntermediateDates().get(4); // 2018-01-23T00:00:00
//		Date date = getAllIntermediateDates().get(6); // 2018-03-26T00:00:00
		
		boolean lastDateSelected = false;

//		DwFeature feature = DwFeatureUtil.findFeatureById(tfm, "_4e0f6738-495e-4129-ad86-060e7d9a49ad");
//		return operationFactory.createDwFeatureDeleteOperation(feature.getFeatureModel(), feature, date, lastDateSelected);
		
//		DwFeature feature = DwFeatureUtil.findFeatureById(tfm, "_cc1074d2-1643-4759-92c3-9ca8206667a9");
//		String newName = "F_ItEyuQT6CjdvR/1WMwKFb1APEbtqPSdi";
//		return operationFactory.createDwFeatureRenameOperation(feature.getFeatureModel(), feature, newName, date, lastDateSelected);
		
//		DwGroup group = findGroupById("_0b5ee262-d352-4719-a986-978be83fe08a");
//		DwGroupTypeEnum newType = DwGroupTypeEnum.AND;
//		return operationFactory.createDwGroupTypeChangeOperation(tfm, group, newType, date, lastDateSelected);
		
//		DwFeature feature = DwFeatureUtil.findFeatureById(tfm, "_f2211705-d408-41bc-8b3f-8429eae81a5b");
//		EObject newParent = DwFeatureUtil.findElementById(tfm, "_6756675b-6100-4392-9cf7-d64a7aed8036");
//		return operationFactory.createDwFeatureMoveOperation(feature.getFeatureModel(), feature, newParent, date, lastDateSelected);
		
		DwGroup group = DwFeatureUtil.findGroupById(tfm, "_adc8c148-2ed5-492c-88a1-7cb722968c2f");
		DwFeature newParentFeature = DwFeatureUtil.findFeatureById(tfm, "_4d3ce9ef-4e80-4294-ac3b-84f720f5d37d");
		return operationFactory.createDwGroupMoveOperation(group.getFeatureModel(), group, newParentFeature, date, lastDateSelected);
	}
	
	
	
	private DwEvolutionOperation createFeatureCreateOperation() {
		Date date = getRandomIntermediateDate();
		boolean lastDateSelected = false;
		
		EObject parent = getRandomFeatureOrGroup(date);
		boolean forceCreateParentGroup = false;
		
		return operationFactory.createDwFeatureCreateOperation(tfm, parent, forceCreateParentGroup, date, lastDateSelected);
	}
	
	private DwEvolutionOperation createFeatureDeleteOperation() {
		Date date = getRandomIntermediateDate();
		boolean lastDateSelected = false;

		// DarwinSPL editor won't allow the root feature to be deleted
		DwFeature feature = getRandomFeatureExceptRoot(date);
		
		return operationFactory.createDwFeatureDeleteOperation(feature.getFeatureModel(), feature, date, lastDateSelected);
	}

	private DwEvolutionOperation createFeatureMoveOperation() {
		Date date = getRandomIntermediateDate();
		boolean lastDateSelected = false;

		// DarwinSPL editor won't allow the root feature to be moved
		DwFeature feature = getRandomFeatureExceptRoot(date);
		Set<DwTemporalElement> invalidElementsToMoveTo = new HashSet<>();
		DwFeatureUtil.collectFeaturesAndGroupsOfSubtree(invalidElementsToMoveTo, feature, date);
		
		// Don't move the feature to its old parent ... !
		invalidElementsToMoveTo.add(DwFeatureUtil.getParentGroupOfFeature(feature, date));
		invalidElementsToMoveTo.add(DwFeatureUtil.getParentFeatureOfFeature(feature, date));
		
		EObject newParent = getRandomFeatureOrGroup(date);
		while(invalidElementsToMoveTo.contains(newParent)) {
			newParent = getRandomFeatureOrGroup(date);
		}
		
		return operationFactory.createDwFeatureMoveOperation(feature.getFeatureModel(), feature, newParent, date, lastDateSelected);
	}

	private DwEvolutionOperation createFeatureRenameOperation() {
		Date date = getRandomIntermediateDate();
		boolean lastDateSelected = false;
		
		DwFeature feature = getRandomFeature(date);
		
		String currentName = DwFeatureUtil.getName(feature, date).getName();
		String newName = currentName;
		if(Math.random() > 0.2) {
			while(newName.equals(currentName) || newName.equals("RootFeature")) {
				newName = getRandomElementFromList(DwFeatureUtil.getAllFeatureNames(tfm)).getName();
			}
		}
		else {
			newName = java.util.UUID.randomUUID().toString().toUpperCase();
		}
		
		return operationFactory.createDwFeatureRenameOperation(feature.getFeatureModel(), feature, newName, date, lastDateSelected);
	}

	private DwEvolutionOperation createFeatureTypeChangeOperation() {
		Date date = getRandomIntermediateDate();
		boolean lastDateSelected = false;
		
		// DarwinSPL editor won't allow the root feature to be changed
		DwFeature feature = getRandomFeatureExceptRoot(date);
		
		DwFeatureTypeEnum newType;
		if(DwFeatureUtil.getType(feature, date).getType() == DwFeatureTypeEnum.OPTIONAL) {
			newType = DwFeatureTypeEnum.MANDATORY;
		}
		else {
			newType = DwFeatureTypeEnum.OPTIONAL;
		}
		
		return operationFactory.createDwFeatureTypeChangeOperation(tfm, feature, newType, date, lastDateSelected);
	}

	private DwEvolutionOperation createGroupDeleteOperation() {
		Date date = getRandomIntermediateDate();
		boolean lastDateSelected = false;
		
		DwGroup group = getRandomGroup(date);
		
		return operationFactory.createDwGroupDeleteOperation(group.getFeatureModel(), group, date, lastDateSelected);
	}

	private DwEvolutionOperation createGroupMoveOperation() {
		Date date = getRandomIntermediateDate();
		boolean lastDateSelected = false;
		
		DwGroup group = getRandomGroup(date);
		List<DwGroup> rootGroups = DwFeatureUtil.getChildGroupsOfFeature(DwFeatureUtil.getRootFeature(tfm, date), date);
		if(rootGroups.size() == 1) {
			while(rootGroups.get(0).equals(group)) {
				group = getRandomGroup(date);
			}
		}
		
		Set<DwTemporalElement> invalidElementsToMoveTo = new HashSet<>();
		DwFeatureUtil.collectFeaturesAndGroupsOfSubtree(invalidElementsToMoveTo, group, date);
		
		// Don't move the group to its old parent ... !
		invalidElementsToMoveTo.add(DwFeatureUtil.getParentFeatureOfGroup(group, date));
		
		DwFeature newParentFeature = getRandomFeature(date);
		while(invalidElementsToMoveTo.contains(newParentFeature)) {
			newParentFeature = getRandomFeature(date);
		}
		
		return operationFactory.createDwGroupMoveOperation(group.getFeatureModel(), group, newParentFeature, date, lastDateSelected);
	}

	private DwEvolutionOperation createGroupTypeChangeOperation() {
		Date date = getRandomIntermediateDate();
		boolean lastDateSelected = false;
		
		DwGroup group = getRandomGroup(date);
		DwGroupTypeEnum currentType = DwFeatureUtil.getType(group, date).getType();
		DwGroupTypeEnum newType;
		while(true) {
			newType = getRandomGroupType();
			if(!newType.equals(currentType)) {
				break;
			}
		}
		
		return operationFactory.createDwGroupTypeChangeOperation(tfm, group, newType, date, lastDateSelected);
	}
	
	
	
	public DwTemporalElement getRandomFeatureOrGroup(Date date) {
		List<DwFeature> allFeatures = DwFeatureUtil.getFeatures(tfm, date);
		List<DwGroup> allGroups = DwFeatureUtil.getGroups(tfm, date);
		
		List<DwTemporalElement> allElements = new ArrayList<>(allFeatures.size() + allGroups.size());
		allElements.addAll(allFeatures);
		allElements.addAll(allGroups);
		
		return getRandomElementFromList(allElements);
	}

	public DwFeature getRandomFeature(Date date) {
		List<DwFeature> allFeatures = DwFeatureUtil.getFeatures(tfm, date);
		return getRandomElementFromList(allFeatures);
	}

	public DwFeature getRandomFeatureExceptRoot(Date date) {
		DwFeature feature = getRandomFeature(date);
		while(DwFeatureUtil.isRootFeature(feature, date)) {
			feature = getRandomFeature(date);
		}
		return feature;
	}

	public DwGroup getRandomGroup(Date date) {
		List<DwGroup> allGroups = DwFeatureUtil.getGroups(tfm, date);
		return getRandomElementFromList(allGroups);
	}



	public DwGroupTypeEnum getRandomGroupType() {
		return getRandomElementFromList(allGroupTypes);
	}



	public int hashTFM(DwTemporalFeatureModel tfm) {
		List<DwFeature> features = tfm.getFeatures();
		List<DwGroup> groups = tfm.getGroups();
		List<Object> combined = new ArrayList<>(features.size()+groups.size());
		
		return hash(combined);
	}
	
	public int hashOperationModel(DwEvolutionOperationModel operationModel) {
		return hash(operationModel.getEvolutionOperations());
	}
	
	private int hash(List<?> list) {
		Object[] operationsArray = new Object[list.size()];
		list.toArray(operationsArray);
		
		return Arrays.hashCode(operationsArray);
	}
	
	private int hash(EObject obj) {
		int result = obj.hashCode();
		for(EObject content : obj.eContents()) {
			result += hash(content) - obj.eContainer().hashCode();
		}
		return result;
	}

	
}



