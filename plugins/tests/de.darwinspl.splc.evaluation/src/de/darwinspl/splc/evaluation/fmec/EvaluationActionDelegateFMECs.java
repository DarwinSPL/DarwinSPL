package de.darwinspl.splc.evaluation.fmec;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.ICoreRunnable;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.internal.ObjectPluginAction;

import de.christophseidl.util.ecore.EcoreIOUtil;
import de.darwinspl.common.eclipse.ui.DwDialogUtil;
import de.darwinspl.common.eclipse.util.DwResourceUtil;
import de.darwinspl.feature.DwTemporalFeatureModel;
import de.darwinspl.feature.evolution.operation.resource.DwEvolutionOperationModelResourceUtil;
import de.darwinspl.fmec.FeatureModelEvolutionConstraintModel;
import de.darwinspl.splc.evaluation.paradoxes.DwEvaluationStatistics;
import de.darwinspl.splc.evaluation.util.DwEvaluationDialog;
import de.darwinspl.temporal.maude.analysis.DwEvolutionChecker;
import de.darwinspl.temporal.maude.analysis.DwEvolutionCheckingResult;
import de.darwinspl.temporal.util.DwDateResolverUtil;

public class EvaluationActionDelegateFMECs implements IObjectActionDelegate {
	
	private Shell shell;

	public EvaluationActionDelegateFMECs() {
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	@Override
	public void run(IAction action) {
		if(action instanceof ObjectPluginAction) {
			ISelection selection = ((ObjectPluginAction) action).getSelection();
			
			if(selection instanceof TreeSelection) {
				TreePath treePath = ((TreeSelection) selection).getPaths()[0];
				Object file = treePath.getSegment(treePath.getSegmentCount()-1);

				if(file instanceof IFile) {
					EObject model = EcoreIOUtil.loadModel((IFile) file);
					
					if(model instanceof FeatureModelEvolutionConstraintModel) {
						FeatureModelEvolutionConstraintModel fmecModel = (FeatureModelEvolutionConstraintModel) model;
						DwTemporalFeatureModel tfm = fmecModel.getFeatureModel();

						DwEvaluationDialog dialog = new DwEvaluationDialog(shell, 500, false);
						int repititions = dialog.open();
						if(repititions <= 0)
							return;
						
						Job evalJob = Job.create("Evaluating \"" + ((IFile) file).getName() + "\"...", (ICoreRunnable) monitor -> {
							String info = "";
							String inputForR = "FMEC Verification Runtimes";
							int amountFMECs = fmecModel.getConstraints().size();
							
							outerLoop: for(int i=1; i<=amountFMECs; i++) {
								inputForR += System.lineSeparator() + "Runtimes for " + i + "FMECs:";
								inputForR += System.lineSeparator() + System.lineSeparator() + "=========================================================";
								inputForR += System.lineSeparator() + "=========================================================" + System.lineSeparator();
								
								int[] runtimes = new int[repititions];

								FeatureModelEvolutionConstraintModel workingCopy = EcoreUtil.copy(fmecModel);
								while(workingCopy.getConstraints().size() != i)
									workingCopy.getConstraints().remove(workingCopy.getConstraints().size()-1);
								
								monitor.beginTask("Gradually increasing amount of simultaneously verified FMECs... (FMEC "+i+"/"+amountFMECs+")", repititions);
								long startTime = System.currentTimeMillis();
								
								for(int run=0; run<repititions; run++) {
									long passedTime = System.currentTimeMillis() - startTime;
									double percentage = (double)(run+1) / (double) repititions;
									long estimatedOverallDuration = (long) ((double) passedTime * (1.0d/percentage));
									
									monitor.subTask((run+1) + " / " + repititions + " - ETA: " + DwDateResolverUtil.getTimeAsString(estimatedOverallDuration-passedTime, false));
									
									String result = DwEvolutionChecker.getInstance().checkFmecModule(tfm, workingCopy);
									String[] lineSplit = result.split(System.lineSeparator());
									
									int runtime = DwEvolutionCheckingResult.timeStringToInt(lineSplit[2]);
									runtimes[run] = runtime;
									inputForR += System.lineSeparator() + runtime;
									
									monitor.worked(1);
									
									if(monitor.isCanceled())
										return;
								}

								info += "Runtime for "+i+"/"+amountFMECs+" FMECs" + System.lineSeparator();
								info += DwEvaluationStatistics.getRuntimeStatistics(runtimes);
								info += System.lineSeparator() + System.lineSeparator();
							}
							
							monitor.done();
			
							DwResourceUtil.writeToFile(((IFile) file).getParent().getFile(new Path("FMECs.dat")), inputForR);
							
							final String infoCopy = info;
							DwResourceUtil.writeToFile(((IFile) file).getParent().getFile(new Path("FMECs_meta.dat")), infoCopy);
							Display.getDefault().asyncExec(() -> {
								DwDialogUtil.openInfoDialogWithCopyButton(((IResource) file).getProjectRelativePath().toOSString(), infoCopy);
							});
						});
						
						evalJob.setPriority(Job.INTERACTIVE);
						evalJob.schedule();
						
						DwEvolutionOperationModelResourceUtil.removeOperationModelFromCache(tfm);
					}
				}
			}
		}
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
	}

}
