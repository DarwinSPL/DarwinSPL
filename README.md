![DarwinSPL](https://www.isf.cs.tu-bs.de/data/darwinspl/logo_text.png)



# DarwinSPL
DarwinSPL is a software project to model and analyze evolving software product lines.
It offers to work with following kinds of artifacts
1. **Temporal Feature Model** (TFM):
Instead of only storing a simple feature model, a DarwinSPL TFM stores the entire evolution of an FM, i.e., its entire
past **and future** - in one artifact (`*.tfm`).
Thanks to its meta model, a TFM is compact in its size and descriptive in its structure.
At any time, a TFM can be extended by an additional edit operation for an arbirary time point in the past of future.
2. **Feature Model Evolution Constraint** (FMEC):
A FMEC allows to express logical demands to the evolution captured by a TFM, e.g.,
`"Air Exchange" starts after "AirCheck" starts;`.
DarwinSPL uses a special web service to check whether a FMEC holds.
For instance, this can be very useful when creating a plan with the use of a TFM.
FMECs are organized in FMEC modules (`*.fmec`)
3. **Cross-Tree Constraint** (CTC):
CTCs in DarwinSPL work like regular CTCs.
In addition, they are (like everything related to DarwinSPL) evolution-aware.
DarwinSPL CTCs are stored in own files (`*.tfm_ctc`).
4. **Mapping**:
With a mapping, realization artifacts can be mapped to features.
An extensible variant derivation mechanism enables to plug-in different variant derivers.
DarwinSPL provides a varaint deriver for DeltaEcore (www.delatecore.org).
DarwinSPL mappings are stored in own files (`*.dwmapping`).

For more information about the usage of these artifacts, please scroll down to "*Usage*".

> Note that in a previous version, DarwinSPL could also be used to model context-sensitive SPLs.
> This functionality is (currently) not maintained anymore as we focus on SPL evolution.
> You can find the old contex-sensitive version in the branch "branch_context_sensitivity".
> The models in that branch are not compatible anymore with the models in this branch.

### Important Folders
- `./maude-webserver/UNIX/` contains the raw Maude implementation of our FM evolution plan execution semantics.
This implementation is used to (i) prevent the introduction of inconsistent edit operations and (ii) to verify FMECs. 
- `./plugins/test/` contains a plugin that we developed to measure the performance of the above described Maude
implementation of our execution semantics for SPLC'20.
- `./examples/` contains a selection of examples for DarwinSPL TFMs, FMEC modules and CTCs.
- `./variant_deriver_plugins/` contains the plug-ins that provide concrete variant derivers (e.g., for DeltaEcore). These plug-ins may have additional dependencies.




# Update Site

Install the latest version of DarwinSPL into your Eclipse runtime via its update site
(further explanations available on the site):
>[https://www.isf.cs.tu-bs.de/data/darwinspl/update/](https://www.isf.cs.tu-bs.de/data/darwinspl/update/)




# Installation and Usage

We recorded a [**tutorial video**](https://youtu.be/krR5mQpNZlU) on how to set up and work with DarwinSPL.

[![](https://www.isf.cs.tu-bs.de/data/darwinspl/youtube_preview.png)](https://youtu.be/krR5mQpNZlU "Tutorial Video")

### Installation & Setup
There are 3 ways to install DarwinSPL (all explained in the above tutorial video).
For installation way 2 and 3, you need to use the Eclipse installation package "Eclipse Modeling Tools"
[[download](https://www.eclipse.org/downloads/packages/release/2020-06/r/eclipse-modeling-tools)].
1. Download a ready-to-use Eclipse runtime with DarwinSPL pre-installed
    - **DarwinSPL (Version 2.1)**: evolution-aware feature modeling
        - Windows (64 bit) [[download](https://www.isf.cs.tu-bs.de/data/darwinspl/eclipse-darwinspl-2.1-win64.zip)]
        - Linux (64 bit) [[download](https://www.isf.cs.tu-bs.de/data/darwinspl/eclipse-darwinspl-2.1-linux.tar.gz)]
    - **DarwinSPL (Version 2.1) + [DeltaEcore](http://deltaecore.org/) + [DeltaJava](https://deltajava.org/)**: evolution-aware feature modeling + delta-oriented programming for variant generation
        - Windows (64 bit) [[download](https://deltajava.org/releases/eclipse-darwinspl-2.1-deltaj-1.5-win64.zip)]
        - Linux (64 bit) [[download](https://deltajava.org/releases/eclipse-darwinspl-2.1-deltaj-1.5-linux.tar.gz)]
2. Install via update site (recommended if DarwinSPL is not to be changed ; instructions above):
[https://www.isf.cs.tu-bs.de/data/darwinspl/update/](https://www.isf.cs.tu-bs.de/data/darwinspl/update/)
3. Check out and run source code
    1. check out the repository: `git clone https://gitlab.com/DarwinSPL/DarwinSPL.git`
    2. import all plugins into an Eclipse workspace (your runtime must not have DarinwSPL installed already)
    3. launch an Eclipse runtime from within your development instance
        1. From the upper area within Eclipse, click the entry "*Run*" (alternatively the downwards facing arrow next to
        the green play button)
    	2. Click "*Run Configurations*"
    	3. Right-click on the entry "*Eclipse Application*"
    	4. Hit "*New Configuration*"
    	5. Configure all settings at will
    	6. Click "*Run*": An Eclipse instance will start, having installed all plugins from your parent instance's
    	workspace.
    4. (outdated as the EMFText update site is currently offline - please use Step 4.1 instead) Install EMFText to resolve errors in your Eclipse runtime via the EMFText update site:
[http://update.emftext.org/trunk/](http://update.emftext.org/trunk/)
        1. If the EMFText update site is offline:
        Go to the `emftext_jars` subfolder and copy these jars to the `plugins` folder of your eclipse installation.

Executing intermediate edit operations and verifying FMECs (see benath for brief explanation and examples) requires our
FM evolution plan execution semantics.
These can be set up in either of the following 2 ways:
1. via Docker
    - pull the latest image from our [DockerHub site](https://hub.docker.com/r/adomat/darwinspl-maude-webservice/tags)
    - run the image: `docker run -p 8080:8080 adomat/darwinspl-maude-webservice`
2. headless (only working on UNIX machines)
    - check out this repository
    - change to folder
    "[./maude-webserver/UNIX/](https://gitlab.com/DarwinSPL/DarwinSPL/-/tree/master/maude-webserver/UNIX)"
    withinin the repository
    - run the service (having installed Java): `java -jar DwMaudeWS.jar`
        - make sure the Maude reasoning engine is executable: `chmod u+x ./maude-CVC4.linux64`
        - also, the package *libtinfo5* must be installed: `sudo apt-get install libtinfo5`

### Usage
The above tutorial video gives an overview of how to use DarwinSPL.
DarwinSPL was tested in **Eclipse 2020-06**.
Beneath, we list some important remarks and shortcuts:

##### Temporal Feature Models (TFM)
- **intermediate operations**
    - DarwinSPL now supports executing operations not only at the end of a TFM time line
        - check out the publication list beneath for additional information!
    - to verify whether the execution of an intermediate edit operation is consistent (doesn't damage the TFM), you need
    to connect DarwinSPL to a reasoning engine service. The setup of this service is explained above.
    - in the upper region of the Eclipse IDE, DarwinSPL offers to open a settings panel where you can change the target
    address of the reasoning engine server
- use the three buttons in the control panel located in the upper editor region to... (from left to right)
    1. switch the current date (alternatively: use the slider)
    2. add a new date (and automatically jump to the new date)
    3. clean the TFM time line from dates where no edit operation is scheduled
- renaming features
    - use the "F2" key on your keyboard
    - double-click an already selected feature (i.e., select feature, **then** double click)
- deleting feature
    - use the delete key on your keyboard
    - use the red cross symbol within the Eclipse tool bar

##### Cross-Tree Constraints (CTC)
DarwinSPL's evolution-aware CTCs are to use like regular CTCs, denoted with a validity interval at their end, e.g.:
1. `"Android Auto" -> "Bluetooth" [ 2020/08/20T12:00:00 - eternity ]`
2. `"Apple Car Play" -> "Bluetooth" [ initial model - eternity ]`

##### Feature Model Evolution Constraints (FMEC)
DarwinSPL enables to express logical demands to the evolution captured within a TFM.
Here are a few examples:
1. `"Security" starts before "Wiper" ends;`
2. `"Milk".parentGroup == "_09ebe7f3-5356-4644-8557-42cb7eb7a5c3" valid until 1.5.2020;`
3. `"Air Exchange" starts after "AirCheck" starts;`
4. `"Blade" starts when "Sensor" starts;`
5. `"Seat" ends at 1.3.2020 00:00;`
6. `"High".type == MANDATORY valid during [ "AirCheck" starts , 1.10.2020 00:00 );`



# Publications:
1. *DarwinSPL: an integrated tool suite for modeling evolving context-aware software product lines*
[[**download**](https://www.isf.cs.tu-bs.de/cms/team/nieke/papers/2017-VaMoS_DarwinSPL.pdf)]
2. *Consistency-Preserving Evolution Planning on Feature Models*
[[**download**](https://www.tu-braunschweig.de/fileadmin/Redaktionsgruppen/Institute_Fakultaet_1/ISF/Paper/2020/ConsistencyPreservingFMPlanning.pdf)]



## Old (outdated) demo videos:
https://www.youtube.com/playlist?list=PLJZIl9YOWBqWvSW-BPbFmcesHmdx7IfYC
(important: syntax for expressions has changed since 2018/09/26. Arithmetical Comparison now do not work anymore with
features and have to be put in curly brackets `{...}`)



# Contact
If you encounter any other issues, please contact us:
- micni ((at)) itu.dk
- adho ((at)) itu.dk
