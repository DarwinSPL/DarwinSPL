header {
	/**
 * Copyright (C) The MOMENT project, 2004-2006.
 * http://moment.dsic.upv.es
 * 
 * This file is part of the Maude Simple GUI plug-in.
 * Contributed by Abel G�mez, <agomez@dsic.upv.es>.
 * 
 * The Maude Simple GUI plug-in is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * The Maude Simple GUI plug-in is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Maude Development Tools plugin, see the file license.txt.
 * If not, write to the Free Software Foundation, Inc., 51 Franklin Street, 
 * Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */
 
	
	 package es.upv.dsic.issi.moment.maudedaemon.parser;
}

class FullMaudeCommandsParser extends Parser;

options {
buildAST = true;
//classHeaderSuffix=org.norecess.antlr.TestableParser;
exportVocab=FullMaudeVocab;
}
	tokens {
	TERM;
	PROGRAM;
	}
	{
		boolean myFailure;
	boolean myQuietErrors;
	java.util.logging.Logger log = java.util.logging.Logger.getLogger(FullMaudeCommandsParser.class.getName());
	
	public boolean successfulParse() {
		return !myFailure;
	}

	public void setQuietErrors(boolean quiet) {
		myQuietErrors=quiet;
	}

	public void atEOF() {
		try{
			match(EOF);
		} catch(Exception e) {
			myFailure=true;
		}
	}
	public void reportError(RecognitionException arg0) {
		myFailure=true;
		if(!myQuietErrors)
			super.reportError(arg0);
	}
}


program : (term|!TODO)* {## = #(#[PROGRAM, "programa"], ##);};

term : (p:PARENT_AB^)(~(PARENT_AB|PARENT_CE)|intermediateTerm)* PARENT_CE 
        {#p.setType(TERM);}
        {log.finer("Term found");};

intermediateTerm : PARENT_AB (~(PARENT_AB|PARENT_CE)|intermediateTerm)* PARENT_CE
        {log.finest("Intermediate Term found");};

class FullMaudeCommandsLexer extends Lexer;

options {
		  filter = false;
		  charVocabulary = '\3'..'\377';
}
TODO_MENOS_LOS_COMENTARIOS: ("***") => COMENTARIO {$setType(Token.SKIP);}
        //                    | (NL) => NL {$setType(Token.SKIP);}
        //                    | (WS) => WS {$setType(Token.SKIP);}
                            | TODO {$setType(TODO);};
                                     
protected COMENTARIO : "***" (~('\n'|'\r'))* { $setType(Token.SKIP);} ;
protected TODO : ~('('|')') (~('('|')'|'*'))* ;

protected WS : ' ' | '\t';
//protected LETRA : 'a'..'z' | 'A'..'Z';
//protected DIGITO : '0'..'9';
protected NL : 
  (
    ("\r\n") => "\r\n"
  | '\r'
  | '\n'
  ) {newline(); $setType(Token.SKIP); } ;

PARENT_AB : '(';
PARENT_CE : ')';
