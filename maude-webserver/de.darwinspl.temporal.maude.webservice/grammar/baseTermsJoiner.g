header {
	/**
 * Copyright (C) The MOMENT project, 2004-2006.
 * http://moment.dsic.upv.es
 * 
 * This file is part of the Maude Simple GUI plug-in.
 * Contributed by Abel G�mez, <agomez@dsic.upv.es>.
 * 
 * The Maude Simple GUI plug-in is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * The Maude Simple GUI plug-in is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Maude Development Tools plugin, see the file license.txt.
 * If not, write to the Free Software Foundation, Inc., 51 Franklin Street, 
 * Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */
 
 
	
	 package es.upv.dsic.issi.moment.maudedaemon.parser;
	 
	 import java.util.List;
	 import java.util.ArrayList;
}

class BaseTermsJoinerTreeParser extends TreeParser;

options {
	buildAST = false;
	importVocab = FullMaudeVocab;
}
{
	   private String extractText(AST t) {
		   			   StringBuffer sb = new StringBuffer();
				      sb.append(t.getText());
				      AST child = t.getFirstChild();
				      while(child!=null) {
					         sb.append(extractText(child));
					         child = child.getNextSibling();
				      }
	   			   return sb.toString();
	   }
}
program returns [List<String> terms = new ArrayList<String>()] {String termino;}
       : #(PROGRAM (termino=baseterm {terms.add(termino);})*) ;
       
baseterm returns [String s=null;]
    : #(t:TERM (term|TODO)* )
    {
    	  s = extractText(#t);
    };
term : #(TERM (term|TODO)*);       