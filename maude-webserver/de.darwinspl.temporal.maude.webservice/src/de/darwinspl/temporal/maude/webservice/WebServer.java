package de.darwinspl.temporal.maude.webservice;

import java.io.IOException;
import java.net.BindException;
import java.util.Date;

import org.takes.Request;
import org.takes.Response;
import org.takes.Take;
import org.takes.http.Exit;
import org.takes.http.FtBasic;
import org.takes.rq.RqPrint;
import org.takes.rs.RsText;

public final class WebServer implements Take {
	
	private static final int MAUDE_DEFAULT_TIMEOUT_SECONDS = 300;
	private static final long MAUDE_DEFAULT_TIMEOUT_MS = MAUDE_DEFAULT_TIMEOUT_SECONDS * 1000;
	public static String ABSOLUTE_PATH_TO_MAUDE_EXECUTABLE;

	public static void start(String absolutePathToMaudeExecutable) throws Exception {
		setUnlimitedStackSize();
		
		ABSOLUTE_PATH_TO_MAUDE_EXECUTABLE = absolutePathToMaudeExecutable;
		while(true) {
			try {
				new FtBasic(new WebServer(), 8080).start(Exit.NEVER);
				break;
			} catch(BindException e) {
				System.err.println(e.getMessage());
				System.err.println("Retrying in 3 seconds ...");
				Thread.sleep(3000);
			} catch(Exception e) {
				System.err.println("Error: Could not start the web service:");
				e.printStackTrace();
				break;
			}
		}
	}

	private static void setUnlimitedStackSize() {
//		ulimit -s unlimited
//		set stacksize unlimited
		
		ProcessBuilder processBuilder = new ProcessBuilder();
		// Run a shell command
		processBuilder.command("bash", "-c", "set stacksize unlimited");

		try {
			Process process = processBuilder.start();
			int exitVal = process.waitFor();
			System.out.println("Setting unlimited stack size finished: " + exitVal + " " + (exitVal==0 ? "(success)" : "(failed)"));
		} catch (IOException | InterruptedException e) {
			System.out.println("Setting unlimited stak size failed with exception:");
			e.printStackTrace();
		}
	}

	@Override
	public Response act(Request request) throws IOException {
		RqPrint requestPrint = new RqPrint(request);
		String requestBody = requestPrint.printBody();
		
		long timeout = MAUDE_DEFAULT_TIMEOUT_MS;
		for(String headProperty : requestPrint.head()) {
			String[] headPropertySplit = headProperty.split(":");
			if(headPropertySplit.length == 2) {
				if(headPropertySplit[0].trim().equals("Maude-Timout")) {
					timeout = Long.parseUnsignedLong(headPropertySplit[1].trim());
				}
			}
		}
		
		String responseText = "Empty input";
		
		logStart();
		
		if(requestBody.length() < 1000)
			System.out.println("Received request with body:" + System.lineSeparator() + System.lineSeparator() + requestBody);
		else
			System.out.println("Received request with length " + requestBody.length());
		
		System.out.println("Timeout for Maude Calculation: " + (timeout == 0 ? "no timeout" : timeout + " ms"));

		if (requestBody != null && !requestBody.equals("")) {
			// Start Maude process and read out welcome screen
			MaudeProcess process = null;
			try {
				process = new MaudeProcess(ABSOLUTE_PATH_TO_MAUDE_EXECUTABLE);
			} catch (Exception e) {
				responseText = "LAUNCH ERROR" + System.lineSeparator() +
						"Starting Maude failed: " + e.getMessage();
			}

			long startTime = System.currentTimeMillis();
			
			if (process != null) {
				// Maude was started successfully
				
				// Forward the received HTTP body to Maude
				process.send(requestBody);
				// Now wait for it to bring in some results
				responseText = process.waitAndRead(timeout);
				if(responseText == null) {
					responseText = "TIMEOUT" + System.lineSeparator() +
							"The evolution plan checking service exceeded a maximum calculation time of " + timeout + " ms.";
				}
			}
			
			process.kill();
			
			long endTime = System.currentTimeMillis();
			responseText = "## Maude calculation time: " + getTimeAsString(endTime - startTime) + System.lineSeparator() + responseText;
		}

		// Send out HTTP response with received Maude reply
		System.out.println("Replying to client:\n\n" + responseText);
		
		logEnd();
		return new RsText(responseText);
	}
	
	
	
	private void logStart() {
		System.out.println("***********************************************");
		System.out.println("\t" + (new Date(System.currentTimeMillis())).toString());
		System.out.println();
	}
	
	private void logEnd() {
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++");
	}



	private String getTimeAsString(long millis) {
		double seconds = ((double) millis / 1000.0d);
		int minutes = (int) Math.floor(seconds / 60.0d);
		seconds = Math.floor(100.0d * (seconds  % 60.0d)) / 100.0d;
		
		if(minutes > 0)
			return millis + "ms (" + minutes + "m " + seconds + "s)";
		else
			return millis + "ms (" + seconds + "s)";
	}

}