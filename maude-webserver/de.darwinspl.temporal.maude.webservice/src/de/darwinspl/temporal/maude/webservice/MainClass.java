package de.darwinspl.temporal.maude.webservice;

import java.io.File;

public class MainClass {

	public static void main(final String[] args) {
		try {
			String absolutePathToWebServiceJar = new File(WebServer.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getPath();
			String absolutePathToMaudeExecutable = absolutePathToWebServiceJar.substring(0, absolutePathToWebServiceJar.lastIndexOf(File.separator)) + File.separator + "maude-CVC4.linux64";
			System.out.println("Determined path to Maude Executable: " + absolutePathToMaudeExecutable);
			
			WebServer.start(absolutePathToMaudeExecutable);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
