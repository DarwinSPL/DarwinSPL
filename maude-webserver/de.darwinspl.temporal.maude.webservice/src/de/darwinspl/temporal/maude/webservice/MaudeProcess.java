package de.darwinspl.temporal.maude.webservice;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.util.concurrent.TimeUnit;

public class MaudeProcess {
	
	private static File NULL_FILE = new File((System.getProperty("os.name").startsWith("Windows") ? "NUL" : "/dev/null"));
	private Process actualProcess;
	
	private BufferedWriter maudeInputWriter;
	
	private BufferedReader maudeOutputReader;
	private BufferedReader maudeErrorReader;
	
	
	
	public MaudeProcess(String absolutePathToMaudeExecutable) throws IOException {
		// ./maude-CVC4.linux64 -print-to-stderr 2>&1 1>/dev/null
		ProcessBuilder processBuilder = new ProcessBuilder(absolutePathToMaudeExecutable, "-print-to-stderr");
		
//		Redirect stdOut = processBuilder.redirectOutput();
//		processBuilder.redirectError(stdOut);
		processBuilder.redirectOutput(NULL_FILE);
		
		actualProcess = processBuilder.start();
		
		maudeInputWriter = new BufferedWriter(new OutputStreamWriter(actualProcess.getOutputStream()));
		maudeOutputReader = new BufferedReader(new InputStreamReader(actualProcess.getInputStream()));
		maudeErrorReader = new BufferedReader(new InputStreamReader(actualProcess.getErrorStream()));
	}
	
	
	
	public void send(String argument) throws IOException {
		maudeInputWriter.write(argument + System.lineSeparator());
		maudeInputWriter.flush();
	}
	
	
	
	public void kill() {
		actualProcess.destroy();
	}



	public String waitAndRead(long timeoutMillis) throws IOException {
//		String result;
//		long startTime = System.currentTimeMillis();
//		
//		while(timeoutMillis == 0 || System.currentTimeMillis() - startTime < timeoutMillis) {
//			result = readOutput();
//			if(!result.equals("")) {
//				return result;
//			}
//			
//			result = readError();
//			if(!result.equals("")) {
//				return result;
//			}
//			
//			try {
//				Thread.sleep(10);
//			} catch (InterruptedException e) {
//				return e.getMessage();
//			}
//		}
		
		try {
			actualProcess.waitFor(timeoutMillis, TimeUnit.MILLISECONDS);
			
			String result = readError();
			if(!result.equals("")) {
				return result;
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	
	public String readOutput() throws IOException {
		return readOutput(Integer.MAX_VALUE);
	}
	
	public String readOutput(int maxLength) throws IOException {
		return readReader(maudeOutputReader, maxLength);
	}
	
	
	
	public String readError() throws IOException {
		return readError(Integer.MAX_VALUE);
	}
	
	public String readError(int maxLength) throws IOException {
		return readReader(maudeErrorReader, maxLength);
	}
	
	
	
	private String readReader(Reader reader, int maxLength) throws IOException {
		String result = "";
		while (reader.ready() && result.length() < maxLength) {
			result += (char) reader.read();
		}
		return result;
	}

}
