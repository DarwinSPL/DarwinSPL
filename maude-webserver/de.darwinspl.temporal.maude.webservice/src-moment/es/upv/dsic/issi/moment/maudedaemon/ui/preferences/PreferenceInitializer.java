/**
 * Copyright (C) The MOMENT project, 2004-2006.
 * http://moment.dsic.upv.es
 * 
 * This file is part of the Maude Simple GUI plug-in.
 * Contributed by Abel G�mez, <agomez@dsic.upv.es>.
 * 
 * The Maude Simple GUI plug-in is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * The Maude Simple GUI plug-in is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Maude Development Tools plugin, see the file license.txt.
 * If not, write to the Free Software Foundation, Inc., 51 Franklin Street, 
 * Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */
 
 
package es.upv.dsic.issi.moment.maudedaemon.ui.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import es.upv.dsic.issi.moment.maudedaemon.core.MaudeDaemonPlugin;
import es.upv.dsic.issi.moment.maudedaemon.ui.Messages;

/**
 * @author Abel G�mez. agomez@dsic.upv.es
 * 
 * Class used to initialize default preference values.
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	public void initializeDefaultPreferences() {
		
		String pathLogDir = "";

		IPreferenceStore store = MaudeDaemonPlugin.getDefault().getPreferenceStore();

		store.setDefault(PreferenceConstants.P_RUNTYPE, Messages.getString("MaudeDaemon.PREF_RUNCORE_VALUE")); //$NON-NLS-1$
		store.setDefault(PreferenceConstants.P_LOGNAME, "maudeout.log");
		store.setDefault(PreferenceConstants.P_PATHLOG, pathLogDir);
		store.setDefault(PreferenceConstants.P_CONSOLESIZE, 160000);
		store.setDefault(PreferenceConstants.P_ENABLELOGGING, true);
		store.setDefault(PreferenceConstants.P_CLEARONLAUNCH, false);
		store.setDefault(PreferenceConstants.P_ENABLECONSOLE, true);
		store.setDefault(PreferenceConstants.P_FLUSHINTERVAL, 10);
		
		// Linux System
		if (System.getProperty("os.name").equals("Linux")) {
			store.setDefault(PreferenceConstants.P_SYSTYPE, Messages.getString("MaudeDaemon.PREF_LINUX_VALUE"));
			store.setDefault(PreferenceConstants.P_COMMAND, Messages.getString("MaudeDaemon.PREF_LINUX_COMMAND"));
			return;
		}
		
		//Mac OS X
		if (System.getProperty("os.name").toLowerCase().startsWith("mac os x")) {
			store.setDefault(PreferenceConstants.P_SYSTYPE, Messages.getString("MaudeDaemon.PREF_MACOSX_VALUE"));
			store.setDefault(PreferenceConstants.P_COMMAND, Messages.getString("MaudeDaemon.PREF_MACOSX_COMMAND"));
			return;
		}
		
		// Windows 95
		if (System.getProperty("os.name").equals("Windows 95")) {
			store.setDefault(PreferenceConstants.P_SYSTYPE, Messages.getString("MaudeDaemon.PREF_WIN9X_VALUE"));
			store.setDefault(PreferenceConstants.P_COMMAND, Messages.getString("MaudeDaemon.PREF_WIN9X_COMMAND"));
			return;
		}
		
		// Windows NT/2000/XP
		if (System.getProperty("os.name").startsWith("Windows")) {
			store.setDefault(PreferenceConstants.P_SYSTYPE, Messages.getString("MaudeDaemon.PREF_WINNT_VALUE"));
			store.setDefault(PreferenceConstants.P_COMMAND, Messages.getString("MaudeDaemon.PREF_WINNT_COMMAND"));
			return;
		}
	}
}
