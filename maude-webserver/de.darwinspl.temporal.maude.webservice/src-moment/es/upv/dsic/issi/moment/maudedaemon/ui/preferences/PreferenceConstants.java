/**
 * Copyright (C) The MOMENT project, 2004-2006.
 * http://moment.dsic.upv.es
 * 
 * This file is part of the Maude Simple GUI plug-in.
 * Contributed by Abel G�mez, <agomez@dsic.upv.es>.
 * 
 * The Maude Simple GUI plug-in is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * The Maude Simple GUI plug-in is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Maude Development Tools plugin, see the file license.txt.
 * If not, write to the Free Software Foundation, Inc., 51 Franklin Street, 
 * Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */
 
 
package es.upv.dsic.issi.moment.maudedaemon.ui.preferences;

/**
 * 
 * @author Abel G�mez. agomez@dsic.upv.es
 * 
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {

	public static final String P_PATHBIN = "pathMaudeBin";

	public static final String P_PATHFM = "pathFMFile";

	public static final String P_PATHLOG = "pathLogDir";
	
	public static final String P_LOGNAME = "pathLogName";
	
	public static final String P_ENABLELOGGING = "enableLogging";
	
	public static final String P_RUNTYPE = "runType";

	public static final String P_SYSTYPE = "sysType";

	public static final String P_COMMAND = "commandToRun";
	
	public static final String P_CONSOLESIZE= "consoleBufferSize";

	public static final String P_FLUSHINTERVAL = "flushInterval";

	public static final String P_ENABLECONSOLE = "enableConsole";

	public static final String P_CLEARONLAUNCH = "clearOnLaunch";
	
}
