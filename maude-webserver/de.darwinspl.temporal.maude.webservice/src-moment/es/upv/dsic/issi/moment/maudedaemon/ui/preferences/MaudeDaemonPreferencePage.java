/**
 * Copyright (C) The MOMENT project, 2004-2006.
 * http://moment.dsic.upv.es
 * 
 * This file is part of the Maude Simple GUI plug-in.
 * Contributed by Abel G�mez, <agomez@dsic.upv.es>.
 * 
 * The Maude Simple GUI plug-in is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * The Maude Simple GUI plug-in is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Maude Development Tools plugin, see the file license.txt.
 * If not, write to the Free Software Foundation, Inc., 51 Franklin Street, 
 * Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */
 
 
package es.upv.dsic.issi.moment.maudedaemon.ui.preferences;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import es.upv.dsic.issi.moment.maudedaemon.core.MaudeDaemonPlugin;
import es.upv.dsic.issi.moment.maudedaemon.ui.Messages;

/**
 * 
 * @author Abel G�mez. agomez@dsic.upv.es
 * 
 * This class represents a preference page that
 * is contributed to the Preferences dialog. By 
 * subclassing <samp>FieldEditorPreferencePage</samp>, we
 * can use the field support built into JFace that allows
 * us to create a page that is small and knows how to 
 * save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They
 * are stored in the preference store that belongs to
 * the main plug-in class. That way, preferences can
 * be accessed directly via the preference store.
 */

public class MaudeDaemonPreferencePage
	extends PreferencePage
	implements IWorkbenchPreferencePage, IPropertyChangeListener {
	
	List<FieldEditor> fields = new ArrayList<FieldEditor>();
	private FieldEditor invalidFieldEditor = null; 

	public MaudeDaemonPreferencePage() {
		super(Messages.getString("MaudeDaemon.PREF_TITLE"));
		setPreferenceStore(MaudeDaemonPlugin.getDefault().getPreferenceStore());
		setDescription(Messages.getString("MaudeDaemon.PREF_TITLE")); //$NON-NLS-1$
	}
	

	protected Control createContents(Composite parent) {

		GridLayout layout1 = new GridLayout(SWT.NONE, true);
		layout1.numColumns = 1;
		GridLayout layout2 = new GridLayout(SWT.NONE, true);
		layout2.numColumns = 2;
		GridLayout layout3 = new GridLayout(SWT.NONE, true);
		layout3.numColumns = 3;
		GridData gdFill = new GridData(GridData.FILL_HORIZONTAL);
		
		// The top composite 
		Composite top = new Composite(parent, SWT.LEFT);
		top.setLayout(layout1);

		// The control to group the Maude files preferences 
		Group groupPaths= new Group(top, SWT.NONE);
		groupPaths.setLayoutData(gdFill);
		groupPaths.setLayout(layout1);
		groupPaths.setText("Maude files");

		// This is a Composite to align correctly the file field editors 
		Composite compPaths = new Composite(groupPaths, SWT.NONE);
		gdFill = new GridData(GridData.FILL_HORIZONTAL);
		compPaths.setLayoutData(gdFill);
		compPaths.setLayout(layout3);
		
		addField(new FileFieldEditor(PreferenceConstants.P_PATHBIN, 
				Messages.getString("MaudeDaemon.PREF_MAUDEBIN"), compPaths)); //$NON-NLS-1$
		addField(new FileFieldEditor(PreferenceConstants.P_PATHFM, 
				Messages.getString("MaudeDaemon.PREF_MAUDEFM"), compPaths)); //$NON-NLS-1$

		// Control to group the logging options
		{
			Group groupLogging = new Group(top, SWT.NONE);
			gdFill = new GridData(GridData.FILL_HORIZONTAL);
			groupLogging.setLayoutData(gdFill);
			groupLogging.setLayout(layout1);
			groupLogging.setText("Logging options");
	
			// This is a Composite to align correctly the file field editors 
			Composite comp1Cols = new Composite(groupLogging, SWT.NONE);
			gdFill = new GridData(GridData.FILL_HORIZONTAL);
			comp1Cols.setLayoutData(gdFill);
			comp1Cols.setLayout(layout2);
			Composite comp3Cols = new Composite(groupLogging, SWT.NONE);
			gdFill = new GridData(GridData.FILL_HORIZONTAL);
			comp3Cols.setLayoutData(gdFill);
			comp3Cols.setLayout(layout3);
			Composite comp2Cols = new Composite(groupLogging, SWT.NONE);
			gdFill = new GridData(GridData.FILL_HORIZONTAL);
			comp2Cols.setLayoutData(gdFill);
			comp2Cols.setLayout(layout2);
			
	
			addField(new BooleanFieldEditor(PreferenceConstants.P_ENABLELOGGING, 
					Messages.getString("MaudeDaemon.PREF_ENABLELOGGING"), comp1Cols)); //$NON-NLS-1$
			addField(new BooleanFieldEditor(PreferenceConstants.P_CLEARONLAUNCH, 
					Messages.getString("MaudeDaemon.PREF_CLEARONLAUNCH"), comp1Cols)); //$NON-NLS-1$
			addField(new DirectoryFieldEditor(PreferenceConstants.P_PATHLOG, 
					Messages.getString("MaudeDaemon.PREF_LOGDIR"), comp3Cols)); //$NON-NLS-1$
			addField(new StringFieldEditor(PreferenceConstants.P_LOGNAME, 
					Messages.getString("MaudeDaemon.PREF_LOGFILE"), comp2Cols)); //$NON-NLS-1$
			IntegerFieldEditor ife = (IntegerFieldEditor) addField(new IntegerFieldEditor(PreferenceConstants.P_FLUSHINTERVAL, 
					Messages.getString("MaudeDaemon.PREF_FLUSHINTERVAL"), comp2Cols)); //$NON-NLS-1$

			ife.setValidRange(1,Integer.MAX_VALUE);

		}
		
		// Control to group the console options
		{
			Group groupConsole = new Group(top, SWT.NONE);
			gdFill = new GridData(GridData.FILL_HORIZONTAL);
			groupConsole.setLayoutData(gdFill);
			groupConsole.setLayout(layout1);
			groupConsole.setText("Console options");
	
			// This is a Composite to align correctly the file field editors 
			Composite comp1Cols = new Composite(groupConsole, SWT.NONE);
			gdFill = new GridData(GridData.FILL_HORIZONTAL);
			comp1Cols.setLayoutData(gdFill);
			comp1Cols.setLayout(layout2);
			Composite comp2Cols = new Composite(groupConsole, SWT.NONE);
			gdFill = new GridData(GridData.FILL_HORIZONTAL);
			comp2Cols.setLayoutData(gdFill);
			comp2Cols.setLayout(layout2);
			
	
			addField(new BooleanFieldEditor(PreferenceConstants.P_ENABLECONSOLE, 
					Messages.getString("MaudeDaemon.PREF_ENABLECONSOLE"), comp1Cols)); //$NON-NLS-1$
			IntegerFieldEditor ife = (IntegerFieldEditor) addField(new IntegerFieldEditor(PreferenceConstants.P_CONSOLESIZE, 
					Messages.getString("MaudeDaemon.PREF_CONSOLESIZE"), comp2Cols)); //$NON-NLS-1$
			
			ife.setValidRange(1,Integer.MAX_VALUE);
	
		}
		
		// Radio buttons of the execution type 
		addField(new RadioGroupFieldEditor(
				PreferenceConstants.P_RUNTYPE,
				Messages.getString("MaudeDaemon.PREF_MODE"), //$NON-NLS-1$
				1,
				new String[][] { { Messages.getString("MaudeDaemon.PREF_RUNCORE"), Messages.getString("MaudeDaemon.PREF_RUNCORE_VALUE") }, { //$NON-NLS-1$ //$NON-NLS-2$
					Messages.getString("MaudeDaemon.PREF_RUNFULL"), Messages.getString("MaudeDaemon.PREF_RUNFULL_VALUE") } //$NON-NLS-1$ //$NON-NLS-2$
				}, top,true));
		
		initialize();
		checkState();
		
		return top;

	}

	private FieldEditor addField(FieldEditor editor) {
		if (editor instanceof StringFieldEditor) {
			((StringFieldEditor) editor).setEmptyStringAllowed(false);
		}
		fields.add(editor);
		return editor;
	}


	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
	}
	
    public void propertyChange(PropertyChangeEvent event) {

        if (event.getProperty().equals(FieldEditor.IS_VALID)) {
            boolean newValue = ((Boolean) event.getNewValue()).booleanValue();
            // If the new value is true then we must check all field editors.
            // If it is false, then the page is invalid in any case.
            if (newValue) {
                checkState();
            } else {
                invalidFieldEditor = (FieldEditor) event.getSource();
                setValid(newValue);
            }
        }
    }
    
    
    
    /**
     * Recomputes the page's error state by calling <code>isValid</code> for
     * every field editor.
     */
    protected void checkState() {
		boolean valid = true;
		invalidFieldEditor = null;
		// The state can only be set to true if all
		// field editors contain a valid value. So we must check them all
		if (fields != null) {
			for (FieldEditor field : fields) {
				valid = valid && field.isValid();
				if (field instanceof FileFieldEditor) {
					valid = valid && ((FileFieldEditor)field).isValid();
				}
				if (!valid) {
					invalidFieldEditor = field;
					break;
				}
			}
		}
		setValid(valid);
	}
    
    /*
	 * (non-Javadoc) Method declared on IDialog.
	 */
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        if (visible && invalidFieldEditor  != null) {
            invalidFieldEditor.setFocus();
        }
    }
    
    /**
     * Initializes all field editors.
     */
    protected void initialize() {
        if (fields != null) {
            for (FieldEditor field : fields) {
            	field.setPage(this);
            	field.setPropertyChangeListener(this);
            	field.setPreferenceStore(getPreferenceStore());
            	field.load();
            }
        }
    }

    /**	
     * The field editor preference page implementation of a <code>PreferencePage</code>
     * method loads all the field editors with their default values.
     */
    protected void performDefaults() {
        if (fields != null) {
            for (FieldEditor field : fields) {
                field.loadDefault();
            }
        }
        // Force a recalculation of my error state.
        checkState();
        super.performDefaults();
    }

    /** 
     * The field editor preference page implementation of this 
     * <code>PreferencePage</code> method saves all field editors by
     * calling <code>FieldEditor.store</code>. Note that this method
     * does not save the preference store itself; it just stores the
     * values back into the preference store.
     *
     * @see FieldEditor#store()
     */
    public boolean performOk() {
        if (fields != null) {
            for (FieldEditor field : fields) {
                field.store();
            }
        }

        return true;
    }
    
    /**	
     * The field editor preference page implementation of an <code>IDialogPage</code>
     * method disposes of this page's controls and images.
     * Subclasses may override to release their own allocated SWT
     * resources, but must call <code>super.dispose</code>.
     */
    public void dispose() {
        super.dispose();
        if (fields != null) {
            Iterator e = fields.iterator();
            while (e.hasNext()) {
                FieldEditor pe = (FieldEditor) e.next();
                pe.setPage(null);
                pe.setPropertyChangeListener(null);
                pe.setPreferenceStore(null);
            }
        }
    }
    
}