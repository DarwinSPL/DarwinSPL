// $ANTLR : "baseTermsJoiner.g" -> "BaseTermsJoinerTreeParser.java"$

	/**
 * Copyright (C) The MOMENT project, 2004-2006.
 * http://moment.dsic.upv.es
 * 
 * This file is part of the Maude Simple GUI plug-in.
 * Contributed by Abel G�mez, <agomez@dsic.upv.es>.
 * 
 * The Maude Simple GUI plug-in is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * The Maude Simple GUI plug-in is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Maude Development Tools plugin, see the file license.txt.
 * If not, write to the Free Software Foundation, Inc., 51 Franklin Street, 
 * Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */
 
 
	
	 package es.upv.dsic.issi.moment.maudedaemon.parser;
	 
	 import java.util.ArrayList;
import java.util.List;

import antlr.RecognitionException;
import antlr.collections.AST;


public class BaseTermsJoinerTreeParser extends antlr.TreeParser       implements BaseTermsJoinerTreeParserTokenTypes
 {

	   private String extractText(AST t) {
		   			   StringBuffer sb = new StringBuffer();
				      sb.append(t.getText());
				      AST child = t.getFirstChild();
				      while(child!=null) {
					         sb.append(extractText(child));
					         child = child.getNextSibling();
				      }
	   			   return sb.toString();
	   }
public BaseTermsJoinerTreeParser() {
	tokenNames = _tokenNames;
}

	public final List<String>  program(AST _t) throws RecognitionException {
		List<String> terms = new ArrayList<String>();
		
		AST program_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		String termino;
		
		try {      // for error handling
			AST __t110 = _t;
			AST tmp1_AST_in = (AST)_t;
			match(_t,PROGRAM);
			_t = _t.getFirstChild();
			{
			_loop112:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==TERM)) {
					termino=baseterm(_t);
					_t = _retTree;
					terms.add(termino);
				}
				else {
					break _loop112;
				}
				
			} while (true);
			}
			_t = __t110;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			reportError(ex);
			if (_t!=null) {_t = _t.getNextSibling();}
		}
		_retTree = _t;
		return terms;
	}
	
	public final String  baseterm(AST _t) throws RecognitionException {
		String s=null;;
		
		AST baseterm_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST t = null;
		
		try {      // for error handling
			AST __t114 = _t;
			t = _t==ASTNULL ? null :(AST)_t;
			match(_t,TERM);
			_t = _t.getFirstChild();
			{
			_loop116:
			do {
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case TERM:
				{
					term(_t);
					_t = _retTree;
					break;
				}
				case TODO:
				{
					AST tmp2_AST_in = (AST)_t;
					match(_t,TODO);
					_t = _t.getNextSibling();
					break;
				}
				default:
				{
					break _loop116;
				}
				}
			} while (true);
			}
			_t = __t114;
			_t = _t.getNextSibling();
			
				  s = extractText(t);
			
		}
		catch (RecognitionException ex) {
			reportError(ex);
			if (_t!=null) {_t = _t.getNextSibling();}
		}
		_retTree = _t;
		return s;
	}
	
	public final void term(AST _t) throws RecognitionException {
		
		AST term_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t118 = _t;
			AST tmp3_AST_in = (AST)_t;
			match(_t,TERM);
			_t = _t.getFirstChild();
			{
			_loop120:
			do {
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case TERM:
				{
					term(_t);
					_t = _retTree;
					break;
				}
				case TODO:
				{
					AST tmp4_AST_in = (AST)_t;
					match(_t,TODO);
					_t = _t.getNextSibling();
					break;
				}
				default:
				{
					break _loop120;
				}
				}
			} while (true);
			}
			_t = __t118;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			reportError(ex);
			if (_t!=null) {_t = _t.getNextSibling();}
		}
		_retTree = _t;
	}
	
	
	public static final String[] _tokenNames = {
		"<0>",
		"EOF",
		"<2>",
		"NULL_TREE_LOOKAHEAD",
		"TERM",
		"PROGRAM",
		"TODO",
		"PARENT_AB",
		"PARENT_CE",
		"TODO_MENOS_LOS_COMENTARIOS",
		"COMENTARIO",
		"WS",
		"NL"
	};
	
	}
	
