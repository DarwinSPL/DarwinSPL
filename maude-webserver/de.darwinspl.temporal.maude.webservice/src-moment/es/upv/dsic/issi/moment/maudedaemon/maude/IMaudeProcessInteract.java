/**
 * Copyright (C) The MOMENT project, 2004-2006.
 * http://moment.dsic.upv.es
 * 
 * This file is part of the Maude Simple GUI plug-in.
 * Contributed by Abel G�mez, <agomez@dsic.upv.es>.
 * 
 * The Maude Simple GUI plug-in is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * The Maude Simple GUI plug-in is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Maude Development Tools plugin, see the file license.txt.
 * If not, write to the Free Software Foundation, Inc., 51 Franklin Street, 
 * Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */
 
 
package es.upv.dsic.issi.moment.maudedaemon.maude;

import java.io.BufferedReader;
import java.io.BufferedWriter;

/**
 * 
 * @author Abel G�mez. agomez@dsic.upv.es
 *
 */


public interface IMaudeProcessInteract extends IMaudeProcess {

	/**
	 * Sends the String txt to Maude Process StdIn
	 * 
	 * @param 
	 * 		String txt
	 */
	public void sendToMaude(String txt);

	/**
	 * Reads one line from StdOut from Maude Process
	 * 
	 * @return
	 * 		StdErr (String)
	 */

	public String readOutLineMaude();

	/**
	 * Reads one line from StdErr from Maude Process
	 * 
	 * @return
	 * 		StdErr (String)
	 */

	public String readErrLineMaude();

	public BufferedWriter getIn();

	public BufferedReader getErr();

	public BufferedReader getOut();

	/**
	 * Reads StdOut from Maude Process
	 * 	 
	 * @param 
	 * 		int maxLen, Maximum number of characters to be read
	 * @return
	 * 		StdOut (String)
	 */

	public String readOutMaudeNoBlock(int maxLen);

	/**
	 * Reads StdErr from Maude Process
	 * 
	 * @param 
	 * 		int maxLen, Maximum number of characters to be read
	 * @return
	 * 		StdErr (String)
	 */

	public String readErrMaudeNoBlock(int maxLen);

	/**
	 * Sends CTRL+C signal to Maude using the UNIX kill command
	 */

	public void makeCtrlC();

}