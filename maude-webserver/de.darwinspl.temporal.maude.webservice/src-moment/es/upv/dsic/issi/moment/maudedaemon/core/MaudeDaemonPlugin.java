/**
 * Copyright (C) The MOMENT project, 2004-2006.
 * http://moment.dsic.upv.es
 * 
 * This file is part of the Maude Simple GUI plug-in.
 * Contributed by Abel G�mez, <agomez@dsic.upv.es>.
 * 
 * The Maude Simple GUI plug-in is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * The Maude Simple GUI plug-in is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Maude Development Tools plugin, see the file license.txt.
 * If not, write to the Free Software Foundation, Inc., 51 Franklin Street, 
 * Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */
 
 
package es.upv.dsic.issi.moment.maudedaemon.core;

import java.util.ArrayList;
import java.util.List;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import es.upv.dsic.issi.moment.maudedaemon.maude.IMaudeProcess;
import es.upv.dsic.issi.moment.maudedaemon.maude.IMaudeProcessBatch;
import es.upv.dsic.issi.moment.maudedaemon.maude.IMaudeProcessInteract;
import es.upv.dsic.issi.moment.maudedaemon.maude.internal.MaudeProcessBatch;
import es.upv.dsic.issi.moment.maudedaemon.maude.internal.MaudeProcessInteract;

/**
 * 
 * @author Abel G�mez. agomez@dsic.upv.es
 *
 */


public class MaudeDaemonPlugin extends AbstractUIPlugin {
	// The shared instance.
	private static MaudeDaemonPlugin plugin;
	// Resource bundle.
	private ResourceBundle resourceBundle;
	// The existing processes
	private List<IMaudeProcess> processes = new ArrayList<IMaudeProcess>();
	
	/**
	 * The constructor.
	 */
	public MaudeDaemonPlugin() {
		super();
		plugin = this;
	}

	/**
	 * This method is called upon plug-in activation
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
	}

	/**
	 * This method is called when the plug-in is stopped
	 */
	public void stop(BundleContext context) throws Exception {
		super.stop(context);
		plugin = null;
		resourceBundle = null;
		for (IMaudeProcess process : processes) {
			process.killMaude();
		}
		processes = null;
	}

	/**
	 * Returns the shared instance.
	 */
	public static MaudeDaemonPlugin getDefault() {
		return plugin;
	}

	/**
	 * Returns the string from the plugin's resource bundle,
	 * or 'key' if not found.
	 */
	public static String getResourceString(String key) {
		ResourceBundle bundle = MaudeDaemonPlugin.getDefault().getResourceBundle();
		try {
			return (bundle != null) ? bundle.getString(key) : key;
		} catch (MissingResourceException e) {
			return key;
		}
	}

	/**
	 * Returns the plugin's resource bundle,
	 */
	public ResourceBundle getResourceBundle() {
		try {
			if (resourceBundle == null)
				resourceBundle = ResourceBundle.getBundle("es.upv.dsic.issi.moment.maudedaemon.ui.MaudeDaemonUiPluginResources");
		} catch (MissingResourceException x) {
			resourceBundle = null;
		}
		return resourceBundle;
	}
	
	public IMaudeProcessBatch getNewMaudeProcessBatch() {
		IMaudeProcess process = new MaudeProcessBatch();
		processes.add(process);
		return (IMaudeProcessBatch) process;
	}
	
	public IMaudeProcessInteract getNewMaudeProcessInteract() {
		IMaudeProcess process = new MaudeProcessInteract();
		processes.add(process);
		return (IMaudeProcessInteract) process;
	}
	
	public void deleteMaudeProcess(IMaudeProcess process) {
		if (process.isRunning())
			process.killMaude();
		processes.remove(process);		
	}
}
