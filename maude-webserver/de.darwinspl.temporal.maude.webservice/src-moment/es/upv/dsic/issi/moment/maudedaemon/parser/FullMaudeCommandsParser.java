// $ANTLR : "fm.g" -> "FullMaudeCommandsParser.java"$

	/**
 * Copyright (C) The MOMENT project, 2004-2006.
 * http://moment.dsic.upv.es
 * 
 * This file is part of the Maude Simple GUI plug-in.
 * Contributed by Abel G�mez, <agomez@dsic.upv.es>.
 * 
 * The Maude Simple GUI plug-in is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * The Maude Simple GUI plug-in is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Maude Development Tools plugin, see the file license.txt.
 * If not, write to the Free Software Foundation, Inc., 51 Franklin Street, 
 * Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */
 
	
	 package es.upv.dsic.issi.moment.maudedaemon.parser;

import antlr.ASTFactory;
import antlr.ASTPair;
import antlr.ParserSharedInputState;
import antlr.RecognitionException;
import antlr.Token;
import antlr.TokenBuffer;
import antlr.TokenStream;
import antlr.TokenStreamException;
import antlr.collections.AST;
import antlr.collections.impl.ASTArray;
import antlr.collections.impl.BitSet;

public class FullMaudeCommandsParser extends antlr.LLkParser       implements FullMaudeVocabTokenTypes
 {

		boolean myFailure;
	boolean myQuietErrors;
	java.util.logging.Logger log = java.util.logging.Logger.getLogger(FullMaudeCommandsParser.class.getName());
	
	public boolean successfulParse() {
		return !myFailure;
	}

	public void setQuietErrors(boolean quiet) {
		myQuietErrors=quiet;
	}

	public void atEOF() {
		try{
			match(EOF);
		} catch(Exception e) {
			myFailure=true;
		}
	}
	public void reportError(RecognitionException arg0) {
		myFailure=true;
		if(!myQuietErrors)
			super.reportError(arg0);
	}

protected FullMaudeCommandsParser(TokenBuffer tokenBuf, int k) {
  super(tokenBuf,k);
  tokenNames = _tokenNames;
  buildTokenTypeASTClassMap();
  astFactory = new ASTFactory(getTokenTypeToASTClassMap());
}

public FullMaudeCommandsParser(TokenBuffer tokenBuf) {
  this(tokenBuf,1);
}

protected FullMaudeCommandsParser(TokenStream lexer, int k) {
  super(lexer,k);
  tokenNames = _tokenNames;
  buildTokenTypeASTClassMap();
  astFactory = new ASTFactory(getTokenTypeToASTClassMap());
}

public FullMaudeCommandsParser(TokenStream lexer) {
  this(lexer,1);
}

public FullMaudeCommandsParser(ParserSharedInputState state) {
  super(state,1);
  tokenNames = _tokenNames;
  buildTokenTypeASTClassMap();
  astFactory = new ASTFactory(getTokenTypeToASTClassMap());
}

	public final void program() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST program_AST = null;
		
		try {      // for error handling
			{
			_loop79:
			do {
				switch ( LA(1)) {
				case PARENT_AB:
				{
					term();
					astFactory.addASTChild(currentAST, returnAST);
					break;
				}
				case TODO:
				{
					AST tmp1_AST = null;
					tmp1_AST = astFactory.create(LT(1));
					match(TODO);
					break;
				}
				default:
				{
					break _loop79;
				}
				}
			} while (true);
			}
			program_AST = (AST)currentAST.root;
			program_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(PROGRAM,"programa")).add(program_AST));
			currentAST.root = program_AST;
			currentAST.child = program_AST!=null &&program_AST.getFirstChild()!=null ?
				program_AST.getFirstChild() : program_AST;
			currentAST.advanceChildToEnd();
			program_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_0);
		}
		returnAST = program_AST;
	}
	
	public final void term() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST term_AST = null;
		Token  p = null;
		AST p_AST = null;
		
		try {      // for error handling
			{
			p = LT(1);
			p_AST = astFactory.create(p);
			astFactory.makeASTRoot(currentAST, p_AST);
			match(PARENT_AB);
			}
			{
			_loop84:
			do {
				switch ( LA(1)) {
				case TERM:
				case PROGRAM:
				case TODO:
				case TODO_MENOS_LOS_COMENTARIOS:
				case COMENTARIO:
				case WS:
				case NL:
				{
					{
					AST tmp2_AST = null;
					tmp2_AST = astFactory.create(LT(1));
					astFactory.addASTChild(currentAST, tmp2_AST);
					match(_tokenSet_1);
					}
					break;
				}
				case PARENT_AB:
				{
					intermediateTerm();
					astFactory.addASTChild(currentAST, returnAST);
					break;
				}
				default:
				{
					break _loop84;
				}
				}
			} while (true);
			}
			AST tmp3_AST = null;
			tmp3_AST = astFactory.create(LT(1));
			astFactory.addASTChild(currentAST, tmp3_AST);
			match(PARENT_CE);
			p_AST.setType(TERM);
			log.finer("Term found");
			term_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_2);
		}
		returnAST = term_AST;
	}
	
	public final void intermediateTerm() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST intermediateTerm_AST = null;
		
		try {      // for error handling
			AST tmp4_AST = null;
			tmp4_AST = astFactory.create(LT(1));
			astFactory.addASTChild(currentAST, tmp4_AST);
			match(PARENT_AB);
			{
			_loop88:
			do {
				switch ( LA(1)) {
				case TERM:
				case PROGRAM:
				case TODO:
				case TODO_MENOS_LOS_COMENTARIOS:
				case COMENTARIO:
				case WS:
				case NL:
				{
					{
					AST tmp5_AST = null;
					tmp5_AST = astFactory.create(LT(1));
					astFactory.addASTChild(currentAST, tmp5_AST);
					match(_tokenSet_1);
					}
					break;
				}
				case PARENT_AB:
				{
					intermediateTerm();
					astFactory.addASTChild(currentAST, returnAST);
					break;
				}
				default:
				{
					break _loop88;
				}
				}
			} while (true);
			}
			AST tmp6_AST = null;
			tmp6_AST = astFactory.create(LT(1));
			astFactory.addASTChild(currentAST, tmp6_AST);
			match(PARENT_CE);
			log.finest("Intermediate Term found");
			intermediateTerm_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_3);
		}
		returnAST = intermediateTerm_AST;
	}
	
	
	public static final String[] _tokenNames = {
		"<0>",
		"EOF",
		"<2>",
		"NULL_TREE_LOOKAHEAD",
		"TERM",
		"PROGRAM",
		"TODO",
		"PARENT_AB",
		"PARENT_CE",
		"TODO_MENOS_LOS_COMENTARIOS",
		"COMENTARIO",
		"WS",
		"NL"
	};
	
	protected void buildTokenTypeASTClassMap() {
		tokenTypeToASTClassMap=null;
	};
	
	private static final long[] mk_tokenSet_0() {
		long[] data = { 2L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_0 = new BitSet(mk_tokenSet_0());
	private static final long[] mk_tokenSet_1() {
		long[] data = { 7792L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_1 = new BitSet(mk_tokenSet_1());
	private static final long[] mk_tokenSet_2() {
		long[] data = { 194L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_2 = new BitSet(mk_tokenSet_2());
	private static final long[] mk_tokenSet_3() {
		long[] data = { 8176L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_3 = new BitSet(mk_tokenSet_3());
	
	}
