/**
 * Copyright (C) The MOMENT project, 2004-2006.
 * http://moment.dsic.upv.es
 * 
 * This file is part of the Maude Simple GUI plug-in.
 * Contributed by Abel G�mez, <agomez@dsic.upv.es>.
 * 
 * The Maude Simple GUI plug-in is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * The Maude Simple GUI plug-in is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Maude Development Tools plugin, see the file license.txt.
 * If not, write to the Free Software Foundation, Inc., 51 Franklin Street, 
 * Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */
 
 
package es.upv.dsic.issi.moment.maudedaemon.maude.internal;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import es.upv.dsic.issi.moment.maudedaemon.maude.IMaudeJob;
import es.upv.dsic.issi.moment.maudedaemon.maude.IMaudeJobCompletionListener;
import es.upv.dsic.issi.moment.maudedaemon.ui.console.ConsoleOutputView;


/**
 * 
 * @author Abel G�mez. agomez@dsic.upv.es
 *
 */


public class MaudeJob implements IMaudeJob {
	
	/**
	 * 
	 */
	private String in, err;
	private String out;
	private String redExpr = "true and false and true and false";
	private String redExprPattern = "reduce in ([\\w\\-<>]*) : " + redExpr + " .\\nrewrites: ([0-9]+) in (\\-*[0-9]+)ms cpu \\(([0-9]+)ms real\\) \\(([0-9]+|\\p{Punct}) rewrites/second\\)\\nresult Bool: false\\nMaude>\\s*";
	private String jobID = "";

	private boolean finished = false;
	private String activeModule = "";
	private long cpuTime = 0;
	private long realTime = 0;
	private long rewrites = 0;
	private boolean error = false;
	private int firstCharOfTheResult = 0;
	
	private static int NUM_CHARS = 200;
	
	private Set<IMaudeJobCompletionListener> listeners = new HashSet<IMaudeJobCompletionListener>();
	private ConsoleOutputView consoleOut;
	
	public MaudeJob(String input) {

		consoleOut = new ConsoleOutputView("Maude Jobs console");

		jobID = Integer.toString((new Random()).nextInt(128));
		
		consoleOut.getBlackStream().println("Processing job with ID " + jobID + "...");

		setInput(input);
		out = err = "";
	}

	private String convertToBoolExpression(String num) {
		String boolExpr = "";
		
		String binaryExpr = Integer.toBinaryString(Integer.getInteger(num));

		boolExpr += (binaryExpr.charAt(0) == '1' ? "true" : "false" );

		for (int i = 1; i < binaryExpr.length(); i++) {
			boolExpr += (binaryExpr.charAt(i) == '1' ? 
					" and true" : 
					" and false" );
		}
		
		return boolExpr;
	}	
	
	public void completed(String out, String err) {
		this.out = out.trim();
		this.err = err.trim();
		markFinished();
		isFailed();

		consoleOut.getBlackStream().println("Processed job with ID " + jobID);
		consoleOut.getBlackStream().println("\tActive module: " + activeModule);
		consoleOut.getBlackStream().println("\tRewrites: " + rewrites);
		consoleOut.getBlackStream().println("\tCPU Time: " + cpuTime + " ms cpu");
		consoleOut.getBlackStream().println("\tReal Time: " + realTime + " ms real");

		fireCompletionEvent();
	}

	public String getInput() {
		return in;
	}
	public void setInput(String in) {
		if (in.charAt(0) != '(')
			// The command is Core Maude
			this.in = in + "\nred " + getRedExpr() + " .\n";
		else
			// The command is Full Maude
			this.in = in;
	}
	public String getOut() {
		return out;
	}
	public String getResult() {
		return (firstCharOfTheResult < out.length()-1 ? out.substring(firstCharOfTheResult,out.length()-1) : out);
	}
	public String getError() {
		return err;
	}
	
	public boolean isFailed() {
		if (!error) {
			if (err != null && err.length() > 0) {
//				
//			    // Processing errors
//			    String patternStr = "(.*)\\nWarning: <standard input>, line ([0-9]+): skipped unexpected token: (.+)";
//			    Pattern pattern = Pattern.compile(patternStr);
//			    Matcher matcher = pattern.matcher(err);
//			    if (matcher.find()) {
//			    	error = true;
//			    	return error;
//			    }
//			    	
//			    patternStr = "(.*)\\nWarning: <standard input>, line ([0-9]+): syntax error(.*)";
//			    pattern = Pattern.compile(patternStr);
//			    matcher = pattern.matcher(err);
//			    if (matcher.find()) {
//			    	error = true;
//			    	return error;
//			    }
//			    	
//			    patternStr = "(.*)\\nWarning: <standard input>, line ([0-9]+): bad token (.+)";
//			    pattern = Pattern.compile(patternStr);
//			    matcher = pattern.matcher(err);
//			    if (matcher.find()) {
//			    	error = true;
//			    	return error;
//			    }
//			    	
//			    patternStr = "(.*)\\nWarning: <standard input>, line ([0-9]+): no parse for term\\.(.*)";
//			    pattern = Pattern.compile(patternStr);
//			    matcher = pattern.matcher(err);
//			    if (matcher.find()) {
//			    	error = true;
//			    	return error;
//			    }
//			    	
//			    patternStr = "(.*)\\nError: (.*)";
//			    pattern = Pattern.compile(patternStr);
//			    matcher = pattern.matcher(err);
//			    if (matcher.find()) {
//			    	error = true;
//			    	return error;
//			    }
//				
			} else if (
					getOut().endsWith("Error: No parse for input.") 
				 || getOut().startsWith("Error:") 
				 || getOut().endsWith("Error: No parse for input.")
				 || getOut().contains("Warning: Parse error"))
					error = true;
		}
		return error;
	}
	
	private synchronized void markFinished() {
		// We have finished. We proceed to process the Maude Output

		// Removing result added to detect the end of the command

		// Sometimes appears an "extra" prompt in the result. We have to remove also this promptto avoid problems.
		String patternStr = "(.*)Maude> " + getRedExprPattern();
	    Pattern pattern = Pattern.compile(patternStr, Pattern.DOTALL);
	    String txtToCheck = "";
	    Matcher matcher;
	    boolean matchFound;
	    
	    
	    // We will use the last NUM_CHARS to simplify the pattern matching.
	    if (out.length() >= NUM_CHARS) 
	    	txtToCheck = out.substring(out.length()-NUM_CHARS,out.length());
		else 
			txtToCheck = out;

	    if (in.charAt(0) == '(') {
	    	// The Code was in Full Maude
	    	out = out.substring(0,out.length()-("Maude>").length());
	    } else {
	    	// The code is Core Maude
	    	
		    // We check the first pattern matches with the expression
		    matcher = pattern.matcher(txtToCheck);
		    matchFound = matcher.matches();
	
		    if (!matchFound) {
		    	// The match failed, we will try with the next pattern
		    	patternStr = "(.*)" + getRedExprPattern();
			    pattern = Pattern.compile(patternStr, Pattern.DOTALL);
			    matcher = pattern.matcher(txtToCheck);
	
			    matchFound = matcher.matches();
			}
		    
		    // If everything has gone as expected, we can process the result.
		    if (matchFound) {
	            activeModule = matcher.group(2);
	            rewrites = Long.valueOf(matcher.group(3));
	            cpuTime = Long.valueOf(matcher.group(4));
	            realTime = Long.valueOf(matcher.group(5));
	            out = out.substring(0,out.length()-matcher.group(0).length()+matcher.group(1).length());
		    } else {
		    	// Error
		    	try {
					throw new Exception();
				} catch (Exception e) {
					System.err.println("Unable to post-process Maude job in " + activeModule);
					e.printStackTrace();
				}
		    }
	
			//	    // We will use the last NUM_CHARS to simplify the pattern matching.
			//	    if (out.length() >= NUM_CHARS) 
			//	    	txtToCheck = out.substring(0,NUM_CHARS);
			//		else 
			//			txtToCheck = out;
	    }
	    
	    // Saving the first information about a reduction
	    patternStr = "rewrites: ([0-9]+) in (\\-*[0-9]+)ms cpu \\(([0-9]+)ms real\\) \\(([0-9]+|\\p{Punct}) rewrites/second\\)\\nresult [^:]*:( )(.*)";
	    pattern = Pattern.compile(patternStr);
	    matcher = pattern.matcher(out);
	    matchFound = matcher.find();
	    
	    if (matchFound) {
            rewrites = Long.valueOf(matcher.group(1));
            cpuTime = Long.valueOf(matcher.group(2));
            realTime = Long.valueOf(matcher.group(3));
            firstCharOfTheResult = matcher.end(5);
	    } else {
		    patternStr = "rewrites: ([0-9]+) in (\\-*[0-9]+)ms cpu \\(([0-9]+)ms real\\) \\(([0-9]+|\\p{Punct}) rewrites/second\\)(\\n)(.*)";
		    pattern = Pattern.compile(patternStr);
		    matcher = pattern.matcher(out);
		    matchFound = matcher.find();
		    
		    if (matchFound) {
	            rewrites = Long.valueOf(matcher.group(1));
	            cpuTime = Long.valueOf(matcher.group(2));
	            realTime = Long.valueOf(matcher.group(3));
	            firstCharOfTheResult = matcher.end(5);
		    }
	    }
	    
	    
	    // The process has finished. We can notify it.
		finished = true;
		notifyAll();
	}
	public synchronized void waitUntilFinish() {
		while (!finished) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		};
	}
	public boolean isFinished() {
		return finished;
	}
	public long getCpuTime() {
		return cpuTime;
	}
	public long getRealTime() {
		return realTime;
	}
	public long getRewrites() {
		return rewrites;
	}
	private void fireCompletionEvent() {
		if (!listeners.isEmpty()) {
			List<IMaudeJobCompletionListener> listenersCopy = new ArrayList<IMaudeJobCompletionListener>(listeners);
			if (isFailed())
				for (IMaudeJobCompletionListener l : listenersCopy)
					l.completedFailure(this);
			else
				for (IMaudeJobCompletionListener l : listenersCopy)
					l.completedSuccess(this);
		}
	}
	
	public void removeCompletionListener(IMaudeJobCompletionListener tracingCompletionHandler) {
		listeners.remove(tracingCompletionHandler);
	}
	public void addCompletionListener(IMaudeJobCompletionListener tracingCompletionHandler) {
		listeners.add(tracingCompletionHandler);
		
	}
	
	public String getRedExpr() {
		return redExpr;
	}
	
	public String getRedExprPattern() {
		return redExprPattern;
	}
	
	public void forcedFinish() {
		completed("","");
		error = true;
	}
	
	public String getJobID() {
		return jobID;
	}
	
	public String getActiveModule() {
		return activeModule;
	}
}