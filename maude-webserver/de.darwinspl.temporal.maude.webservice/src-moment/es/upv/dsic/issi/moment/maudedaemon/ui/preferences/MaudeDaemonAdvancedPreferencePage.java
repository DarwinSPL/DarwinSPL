/**
 * Copyright (C) The MOMENT project, 2004-2006.
 * http://moment.dsic.upv.es
 * 
 * This file is part of the Maude Simple GUI plug-in.
 * Contributed by Abel G�mez, <agomez@dsic.upv.es>.
 * 
 * The Maude Simple GUI plug-in is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * The Maude Simple GUI plug-in is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Maude Development Tools plugin, see the file license.txt.
 * If not, write to the Free Software Foundation, Inc., 51 Franklin Street, 
 * Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */
 
 
package es.upv.dsic.issi.moment.maudedaemon.ui.preferences;

import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import es.upv.dsic.issi.moment.maudedaemon.core.MaudeDaemonPlugin;
import es.upv.dsic.issi.moment.maudedaemon.ui.Messages;

/**
 * 
 * 
 * 
 * This class represents a preference page that
 * is contributed to the Preferences dialog. By 
 * subclassing <samp>FieldEditorPreferencePage</samp>, we
 * can use the field support built into JFace that allows
 * us to create a page that is small and knows how to 
 * save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They
 * are stored in the preference store that belongs to
 * the main plug-in class. That way, preferences can
 * be accessed directly via the preference store.
 */

public class MaudeDaemonAdvancedPreferencePage
	extends PreferencePage
	implements IWorkbenchPreferencePage {
	
	Composite top = null;
	RadioGroupFieldEditor sysType = null;
	StringFieldEditor command = null;
	
	public MaudeDaemonAdvancedPreferencePage() {
		super(Messages.getString("MaudeDaemon.PREF_ADVANCED_TITLE"));
		setPreferenceStore(MaudeDaemonPlugin.getDefault().getPreferenceStore());
		setDescription(Messages.getString("MaudeDaemon.PREF_ADVANCED_TITLE")); //$NON-NLS-1$
	}
	
	public void init(IWorkbench workbench) {
	}

	protected Control createContents(Composite parent) {

		top = new Composite(parent, SWT.LEFT);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		top.setLayout(layout);

		GridData griddataList = new GridData(GridData.FILL_HORIZONTAL);
		griddataList.horizontalSpan = 2;


		sysType = new RadioGroupFieldEditor(
				PreferenceConstants.P_SYSTYPE,
			Messages.getString("MaudeDaemon.PREF_SYSTYPE"), //$NON-NLS-1$
			1,
			new String[][] {
				{ Messages.getString("MaudeDaemon.PREF_WINDOWSNT"), Messages.getString("MaudeDaemon.PREF_WINDOWSNT_VALUE") }, //$NON-NLS-1$ //$NON-NLS-2$
				{ Messages.getString("MaudeDaemon.PREF_WINDOWS9X"), Messages.getString("MaudeDaemon.PREF_WINDOWS9X_VALUE") }, //$NON-NLS-1$ //$NON-NLS-2$
				{ Messages.getString("MaudeDaemon.PREF_LINUX"), Messages.getString("MaudeDaemon.PREF_LINUX_VALUE") }, //$NON-NLS-1$ //$NON-NLS-2$
				{ Messages.getString("MaudeDaemon.PREF_MACOSX"), Messages.getString("MaudeDaemon.PREF_MACOSX_VALUE") }, //$NON-NLS-1$ //$NON-NLS-2$
				{ Messages.getString("MaudeDaemon.PREF_OTHER"), Messages.getString("MaudeDaemon.PREF_OTHER_VALUE") } //$NON-NLS-1$ //$NON-NLS-2$
		}, top, true);
	
		sysType.getRadioBoxControl(top).setLayoutData(griddataList);
		sysType.setPreferenceStore(getPreferenceStore());
		sysType.load();
		sysType.setPropertyChangeListener(new ChangeSystem());

		
		
		command = new StringFieldEditor(PreferenceConstants.P_COMMAND, 
				Messages.getString("MaudeDaemon.PREF_COMMAND"), top); //$NON-NLS-1$
		command.setPreferenceStore(getPreferenceStore());
		command.load();
		
		GridData griddataCommand = new GridData(GridData.FILL_HORIZONTAL);
		griddataCommand.horizontalSpan = 2;
		command.getLabelControl(top).setLayoutData(griddataCommand);
		
		Composite note = createNoteComposite(JFaceResources.getDialogFont(),top,"Note:","Don't change this settings unless you know what are you doing!");
		GridData griddataNote = new GridData(GridData.FILL_HORIZONTAL);
		griddataNote.horizontalSpan = 2;
		note.setLayoutData(griddataNote);
		
		if (getPreferenceStore().getString(PreferenceConstants.P_SYSTYPE).equals(Messages.getString("MaudeDaemon.PREF_OTHER_VALUE"))) {
			command.getTextControl(top).setEnabled(true);
		} else {
			command.getTextControl(top).setEnabled(false);
		}

		return top;
	}
	
	protected void performDefaults() {
		command.loadDefault();
		sysType.loadDefault();
		super.performDefaults();
	}

	public boolean performOk() {
		command.store();
		sysType.store();
		return super.performOk();
	}

	private class ChangeSystem implements IPropertyChangeListener {

		public void propertyChange(PropertyChangeEvent event) {
			sysType.store();
			if (getPreferenceStore().getString(PreferenceConstants.P_SYSTYPE).equals(Messages.getString("MaudeDaemon.PREF_WINDOWSNT_VALUE"))) {
				command.setStringValue(Messages.getString("MaudeDaemon.PREF_WINNT_COMMAND"));
				command.getTextControl(top).setEnabled(false);
				return;
			}
			if (getPreferenceStore().getString(PreferenceConstants.P_SYSTYPE).equals(Messages.getString("MaudeDaemon.PREF_WINDOWS9X_VALUE"))) {
				command.setStringValue(Messages.getString("MaudeDaemon.PREF_WIN9X_COMMAND"));
				command.getTextControl(top).setEnabled(false);
				return;
			}
			if (getPreferenceStore().getString(PreferenceConstants.P_SYSTYPE).equals(Messages.getString("MaudeDaemon.PREF_MACOSX_VALUE"))) {
				command.setStringValue(Messages.getString("MaudeDaemon.PREF_MACOSX_COMMAND"));
				command.getTextControl(top).setEnabled(false);
				return;
			}
			if (getPreferenceStore().getString(PreferenceConstants.P_SYSTYPE).equals(Messages.getString("MaudeDaemon.PREF_LINUX_VALUE"))) {
				command.setStringValue(Messages.getString("MaudeDaemon.PREF_LINUX_COMMAND"));
				command.getTextControl(top).setEnabled(false);
				return;
			}
			if (getPreferenceStore().getString(PreferenceConstants.P_SYSTYPE).equals(Messages.getString("MaudeDaemon.PREF_OTHER_VALUE"))) {
				command.getTextControl(top).setEnabled(true);
				return;
			}
		}
		
	}
}