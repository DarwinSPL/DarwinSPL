/**
 * Copyright (C) The MOMENT project, 2004-2006.
 * http://moment.dsic.upv.es
 * 
 * This file is part of the Maude Simple GUI plug-in.
 * Contributed by Abel G�mez, <agomez@dsic.upv.es>.
 * 
 * The Maude Simple GUI plug-in is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * The Maude Simple GUI plug-in is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Maude Development Tools plugin, see the file license.txt.
 * If not, write to the Free Software Foundation, Inc., 51 Franklin Street, 
 * Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */
 
 
package es.upv.dsic.issi.moment.maudedaemon.parser;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 
 * @author Abel G�mez. agomez@dsic.upv.es
 *
 */

public class ParseException extends Exception {
	int line, column;
	
	public ParseException() {
		super();
	}

	public ParseException(String message, Throwable cause) {
		super(message, cause);
		
		String causeClassName = cause.getClass().getName();
		if(causeClassName.equals("antlr.RecognitionException"))
			try {
				extractInfo(cause);
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

	private void extractInfo(Throwable cause) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		Class c = cause.getClass();
		Method Mline = c.getMethod("getLine", new Class[0]);
		this.line = (Integer)Mline.invoke(cause, new Object[0]);
		Method Mcol = c.getMethod("getLine", new Class[0]);
		this.column = (Integer)Mcol.invoke(cause, new Object[0]);
		
	}

	public ParseException(String message) {
		super(message);
	}

	public ParseException(Throwable cause) {
		this(null,cause);
	}


}
