/**
 * Copyright (C) The MOMENT project, 2004-2006.
 * http://moment.dsic.upv.es
 * 
 * This file is part of the Maude Simple GUI plug-in.
 * Contributed by Abel G�mez, <agomez@dsic.upv.es>.
 * 
 * The Maude Simple GUI plug-in is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * The Maude Simple GUI plug-in is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Maude Development Tools plugin, see the file license.txt.
 * If not, write to the Free Software Foundation, Inc., 51 Franklin Street, 
 * Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */
 
 
package es.upv.dsic.issi.moment.maudedaemon.maude;

import java.io.IOException;


/**
 * 
 * @author Abel G�mez. agomez@dsic.upv.es
 *
 */


public interface IMaudeProcess {

	/**
	 * Returns the command string to exec
	 * 
	 * @return the command string to exec
	 */

	public String[] getCommandString();

	/**
	 * Sets the Maude binary file
	 * 
	 * @param pathMaudeBin
	 *            path to Maude Executable (String)
	 */

	public void setPathMaudeBin(String pathMaudeBin);

	/**
	 * Sets the Full Maude file
	 * 
	 * @param pathFMaude
	 *            path to Full Maude file (String)
	 */

	public void setPathFMaude(String pathFMaude);

	/**
	 * Sets the log file
	 * 
	 * @param pathFMaude
	 *            path to log file (String)
	 **/

	public void setLogFile(String logFile);

	/**
	 * @return is selected Core Maude
	 */

	public boolean isCoreMaude();

	/**
	 * @return is selected Full Maude
	 */

	public boolean isFullMaude();

	/**
	 * Selects Core Maude
	 */

	public void setCoreMaude();

	/**
	 * Selects Full Maude
	 */

	public void setFullMaude();

	/**
	 * Sets if Maude must be configured acording to the preferences windows
	 */

	public void setAutoConfig(boolean value);

	/**
	 * Causes Maude not to show the wellcome banner
	 * 
	 * @param showBanner (boolean)
	 */

	public void setShowBanner(boolean showBanner);

	/**
	 * @return is running Maude
	 */

	public boolean isRunning();

	/**
	 * Return the exit value of the command exec'd
	 * 
	 * @return exitValue the exit value of the command exec'd
	 */

	public int getExitValue();

	/**
	 * Gets paths and config from the Eclipse preferences to run Maude
	 */

	public boolean configMaudeFromPrefs();

	/**
	 * Execs Maude
	 */

	public boolean execMaude() throws IOException;

	public void quitMaude();

	public void killMaude();

}