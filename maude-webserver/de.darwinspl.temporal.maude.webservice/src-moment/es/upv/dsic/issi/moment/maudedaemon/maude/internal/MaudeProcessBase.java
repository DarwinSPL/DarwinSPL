/**
 * Copyright (C) The MOMENT project, 2004-2006.
 * http://moment.dsic.upv.es
 * 
 * This file is part of the Maude Simple GUI plug-in.
 * Contributed by Abel G�mez, <agomez@dsic.upv.es>.
 * 
 * The Maude Simple GUI plug-in is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * The Maude Simple GUI plug-in is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Maude Development Tools plugin, see the file license.txt.
 * If not, write to the Free Software Foundation, Inc., 51 Franklin Street, 
 * Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * Created on 19-dic-2004
 *
 */
package es.upv.dsic.issi.moment.maudedaemon.maude.internal;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.util.StringTokenizer;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.dialogs.PreferencesUtil;
import org.eclipse.ui.internal.Workbench;

import es.upv.dsic.issi.moment.maudedaemon.core.MaudeDaemonPlugin;
import es.upv.dsic.issi.moment.maudedaemon.maude.IMaudeProcess;
import es.upv.dsic.issi.moment.maudedaemon.ui.Messages;
import es.upv.dsic.issi.moment.maudedaemon.ui.console.ConsoleOutputView;
import es.upv.dsic.issi.moment.maudedaemon.ui.preferences.PreferenceConstants;

/**
 * @author Abel G�mez. agomez@dsic.upv.es
 * 
 */
public abstract class MaudeProcessBase implements IMaudeProcess {

	protected Runtime runTime;
	// PID of Maude Process
	protected Integer pidMaude;
	// Running flag
	protected boolean running;
	
	// StdIn, StdOut, StdErr of Maude Process
	protected BufferedWriter  buffStdin, buffLogFile;
	protected BufferedReader buffStderr;
	protected BufferedReader buffStdout;
	private FlushLogFileThread thFlushLogfile;

	// Maude process
	private Process maudeProcess = null;

	// Paths needed
	private String pathMaudeBin = ""; // Path of Maude's binary
	private String pathFMaude = ""; // Path of Full Maude file
	private String logFile = ""; // Path of the log file
	private boolean showBanner = true;

	// Run type
	private boolean runCoreMaude = true; // Run CoreMaude (Full Maude in other case) 

	// Sets if Maude must get the settings from the preferences windows
	private boolean autoConfig = true;  
	
	// Command to run Maude	
	private String commandString[];

	// Exit value of Maude Process
	private int exitValue = 0;
	
	// Console
	private static ConsoleOutputView consoleOut;
	
	/**
	 * Constructor for MaudeProcess
	 */

	public MaudeProcessBase() {
		/* sets up command to exec */
		runTime = Runtime.getRuntime();
		running = false;
	}

	/**
	 * Constructor for MaudeProcess
	 */

	public MaudeProcessBase(String[] commandString) {
		/* sets up command to exec */
		this();
		setCommandString(commandString);
	}

	/**
	 * Sets the command string to exec
	 * 
	 * @param commandString
	 *            the command string to exec
	 */

	private void setCommandString(String[] commandString) {
		this.commandString = new String[commandString.length];
		System.arraycopy(commandString, 0, this.commandString, 0,
				commandString.length);
	}

	/**
	 * Returns the command string to exec
	 * 
	 * @return the command string to exec
	 */

	public String[] getCommandString() {
		return commandString;
	}

	/**
	 * Sets the Maude binary file
	 * 
	 * @param pathMaudeBin
	 *            path to Maude Executable (String)
	 */

	public void setPathMaudeBin(String pathMaudeBin) {
		this.pathMaudeBin = pathMaudeBin;
	}

	/**
	 * Sets the Full Maude file
	 * 
	 * @param pathFMaude
	 *            path to Full Maude file (String)
	 */

	public void setPathFMaude(String pathFMaude) {
		this.pathFMaude = pathFMaude;
	}
	
	/**
	 * Sets the log file
	 * 
	 * @param pathFMaude
	 *            path to log file (String)
	 **/

	public void setLogFile(String logFile) {
		this.logFile = logFile;
	}
	
	/**
	 * @return is selected Core Maude
	 */
	
	public boolean isCoreMaude() {
		return runCoreMaude;
	}

	/**
	 * @return is selected Full Maude
	 */

	public boolean isFullMaude() {
		return !runCoreMaude;
	}

	/**
	 * Selects Core Maude
	 */

	public void setCoreMaude() {
		runCoreMaude = true;
	}

	/**
	 * Selects Full Maude
	 */

	public void setFullMaude() {
		runCoreMaude = false;
	}
	
	/**
	 * Sets if Maude must be configured acording to the preferences windows
	 */

	public void setAutoConfig(boolean value) {
		autoConfig = value;
	}

	/**
	 * Causes Maude not to show the wellcome banner
	 * 
	 * @param showBanner (boolean)
	 */

	public void setShowBanner(boolean showBanner) {
		this.showBanner = showBanner;
	}
	
	/**
	 * @return is running Maude
	 */

	public boolean isRunning() {
		return running;
	}
	
	
	/**
	 * Builds the command to run Maude
	 */

	private void createCommandToRun() {

		// Check if maude binary exists
		File fileMaudeBin = new File(pathMaudeBin);

		if (!fileMaudeBin.exists() || fileMaudeBin.isDirectory()) {
			Workbench.getInstance().getDisplay().asyncExec(new Runnable() {
				public void run() {
					MessageDialog.openError(null,Messages.getString("MaudeDaemon.E_TITLE"),Messages.getString("MaudeDaemon.E_CHECK_PREFS"));
				}
			});
			return;
		}

		// If we're going to run FullMaude, we check that the Full Maude file exists too
		if (isFullMaude()) {
			File fileFullMaude = new File(pathFMaude);

			if (!fileFullMaude.exists() || fileFullMaude.isDirectory()) {
				Workbench.getInstance().getDisplay().asyncExec(new Runnable() {
					public void run() {
						MessageDialog.openError(null,Messages.getString("MaudeDaemon.E_TITLE"),Messages.getString("MaudeDaemon.E_CHECK_PREFS"));
					}});
				
				return;
			}
		}

		// Config of the paths. If the are windows paths, we call getPathCygwin.  
		String maude = pathMaudeBin;
		String fmaude = pathFMaude;
		
		if (maude.charAt(0) != '/')
			maude = getPathCygwin(maude);
		if (fmaude.charAt(0) != '/')
			fmaude = getPathCygwin(fmaude);

		// We obtain the command from the preferences store ...
		String commandFromPref = MaudeDaemonPlugin.getDefault().getPreferenceStore().getString(PreferenceConstants.P_COMMAND);
		
		// ... and begin the proces.
		String[] commandToRun = commandFromPref.split(" , ");
		
		// Replace of the maudebin key
		for (int i = 0; i< commandToRun.length; i++)  {
			commandToRun[i] = commandToRun[i].replaceAll("%maudebin%","'" + maude + "'");
		}
		
		// Replace of the fullmaude key
		for (int i = 0; i < commandToRun.length; i++)  {
			if (isCoreMaude())
				// If CoreMaude, we remove the full maude file from the command 
				commandToRun[i] = commandToRun[i].replaceAll("%fullmaude%",(!showBanner ? " -no-banner " : ""));
			else
				commandToRun[i] = commandToRun[i].replaceAll("%fullmaude%","'" + fmaude + "'" + (!showBanner ? " -no-banner " : ""));
		}

		// Finally, we set the command
		setCommandString(commandToRun);
	}
	
	
	/**
	 * @param path
	 *            Path in Windows format
	 * @return Path in Unix format
	 */

	private String getPathCygwin(String path) {
		String result = "/cygdrive/" + path.charAt(0) +"/";
		StringTokenizer stok = new StringTokenizer(path, "\\");
		stok.nextToken();
		while (stok.hasMoreTokens()) {
			result += stok.nextToken() + "/";

		}

		result = result.substring(0, result.length() - 1);
		return result;

	}

	/**
	 * Return the exit value of the command exec'd
	 * 
	 * @return exitValue the exit value of the command exec'd
	 */

	public int getExitValue() {
		return exitValue;
	}

	/**
	 * Gets paths and config from the Eclipse preferences to run Maude
	 */
	
	public boolean configMaudeFromPrefs() {
		String pathMaudeBin = MaudeDaemonPlugin.getDefault().getPreferenceStore()
				.getString(PreferenceConstants.P_PATHBIN);
		String pathFM = MaudeDaemonPlugin.getDefault().getPreferenceStore()
				.getString(PreferenceConstants.P_PATHFM);
		String runType = MaudeDaemonPlugin.getDefault().getPreferenceStore()
				.getString(PreferenceConstants.P_RUNTYPE);
		String logPath = MaudeDaemonPlugin.getDefault().getPreferenceStore()
				.getString(PreferenceConstants.P_PATHLOG);
		String logName = MaudeDaemonPlugin.getDefault().getPreferenceStore()
				.getString(PreferenceConstants.P_LOGNAME);

		if (pathMaudeBin.length() <= 0
				|| pathFM.length() <= 0
				|| runType.length() <= 0
				|| logPath.length() <= 0 
				|| logName.length() <= 0) {

			int result = PreferencesUtil.createPreferenceDialogOn(null, "es.upv.dsic.issi.moment.maudedaemon.ui.preferences.MaudeDaemonPreferencePage", null, null).open();
			
			if (result == Dialog.CANCEL)
				return false;
			else {
				pathMaudeBin = MaudeDaemonPlugin.getDefault()
						.getPreferenceStore().getString(
								PreferenceConstants.P_PATHBIN);
				pathFM = MaudeDaemonPlugin.getDefault()
						.getPreferenceStore().getString(
								PreferenceConstants.P_PATHFM);
				runType = MaudeDaemonPlugin.getDefault()
						.getPreferenceStore().getString(
								PreferenceConstants.P_RUNTYPE);
				logPath = MaudeDaemonPlugin.getDefault()
						.getPreferenceStore().getString(
								PreferenceConstants.P_PATHLOG);
				logName = MaudeDaemonPlugin.getDefault()
						.getPreferenceStore().getString(
								PreferenceConstants.P_LOGNAME);
			}
		}

		setPathMaudeBin(pathMaudeBin);
		setPathFMaude(pathFM);
		setLogFile(logPath + "//" + logName);
		
		if (Messages.getString("MaudeDaemon.PREF_RUNCORE_VALUE").equals(runType)) //$NON-NLS-1$
			setCoreMaude();
		else
			setFullMaude();

		return true;
	}
	
	
	/**
	 * Execs Maude
	 */
	
	public boolean execMaude() throws IOException {

		if (autoConfig)
			if (!configMaudeFromPrefs()) {
				Workbench.getInstance().getDisplay().asyncExec(new Runnable() {
					public void run() {
						MessageDialog.openError(null,Messages.getString("MaudeDaemon.E_TITLE"),Messages.getString("MaudeDaemon.E_CHECK_PREFS")); //$NON-NLS-1$ //$NON-NLS-2$
					}});
				return false;
			}
		
		if (!isConfigured()) {
			Workbench.getInstance().getDisplay().asyncExec(new Runnable() {
				public void run() {
					MessageDialog.openError(null,Messages.getString("MaudeDaemon.E_TITLE"),Messages.getString("MaudeDaemon.E_CHECK_PREFS")); //$NON-NLS-1$ //$NON-NLS-2$
				}});
			return false;
		}
		
		createCommandToRun();

		try {
			
			maudeProcess = runTime.exec(commandString);

			if(logFile != null && logFile!="")
				if (MaudeDaemonPlugin.getDefault().getPreferenceStore().getBoolean(PreferenceConstants.P_CLEARONLAUNCH)) {
					buffLogFile= new BufferedWriter(new FileWriter(logFile, false));
				} else {
					buffLogFile= new BufferedWriter(new FileWriter(logFile, true));
				}

			else
				buffLogFile = new BufferedWriter(new StringWriter());
			
			thFlushLogfile = new FlushLogFileThread();
			thFlushLogfile.start();
			
			buffStdin = new BufferedWriter(new OutputStreamWriter(maudeProcess
					.getOutputStream()));

			buffStdout = new BufferedReader(new InputStreamReader(maudeProcess
					.getInputStream()));

			buffStderr = new BufferedReader(new InputStreamReader(maudeProcess
					.getErrorStream()));

			consoleOut = new ConsoleOutputView("Maude console");
			
		} catch (IOException e) {
			throw new IOException(Messages.getString("MaudeProcess.E_FAIL_EXEC" + " - " + e.getMessage())); //$NON-NLS-1$
		}
			
		running = true;
		
		// The first line is the process PID. 
		// We need it to send signals to the process.
		pidMaude = Integer.valueOf(buffStdout.readLine());
		
		return running;
	}
	
	private boolean isConfigured() {
		return pathMaudeBin != "" && pathFMaude != "";
	}
	
	public void quitMaude () {
		
		if (!isRunning())
			// Maude isn't running, can't quit
			return;
		
		try {
			
			buffStdin.write(Messages.getString("MaudeDaemon.MAUDE_QUIT")); //$NON-NLS-1$

			
			buffStdin.close();
			buffStdout.close();	
			buffStderr.close();
			
			maudeProcess.waitFor();
			exitValue = maudeProcess.exitValue();
			running = false;

			sendToLog("Bye.\n");
			buffLogFile.flush();
			buffLogFile.close();
			buffLogFile = null;
			
			thFlushLogfile.finishThread();

			consoleOut = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void killMaude () {
		Process procKill = null;

		if (!isRunning())
			// Maude isn't running, can't kill
			return;

		try {

			buffStdin.close();
			buffStdout.close();	
			buffStderr.close();
						
			exitValue = 1;			
			running = false;
			
	        procKill = runTime.exec("kill -9 " + pidMaude);	        
			
			maudeProcess.destroy();
			
			sendToLog("\n============================== Process killed ==============================\n");
			buffLogFile.flush();
			buffLogFile.close();
			buffLogFile = null;
			thFlushLogfile.finishThread();

			
			consoleOut = null;
		} catch (Exception e) {
		}
	}
	
	protected void sendToLog(String txt) {
		try {
			if (MaudeDaemonPlugin.getDefault().getPreferenceStore().getBoolean(PreferenceConstants.P_ENABLELOGGING)) {
				buffLogFile.write(txt);
			}
			if (MaudeDaemonPlugin.getDefault().getPreferenceStore().getBoolean(PreferenceConstants.P_ENABLECONSOLE)) {
				consoleOut.getBlackStream().print(txt);
			}
		} catch (IOException e1) {
		} 
	}
	
	protected void finalize() throws Throwable {

		
		killMaude();

		super.finalize();
	}
	
	private class FlushLogFileThread extends Thread  {

		private boolean finish = false;
		
		public FlushLogFileThread() {
			super("FlushLogFileThread");
		}
		
		public void run() {
			while (!finish) {
				try {
					if (buffLogFile != null)
						buffLogFile.flush();
					sleep(MaudeDaemonPlugin.getDefault().getPluginPreferences().getInt(PreferenceConstants.P_FLUSHINTERVAL)*1000);
				} catch (Exception e) {
				}
			}
		}
		private void finishThread() {
			finish = true;
		}
	}
}
