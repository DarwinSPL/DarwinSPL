/**
 * Copyright (C) The MOMENT project, 2004-2006.
 * http://moment.dsic.upv.es
 * 
 * This file is part of the Maude Simple GUI plug-in.
 * Contributed by Abel G�mez, <agomez@dsic.upv.es>.
 * 
 * The Maude Simple GUI plug-in is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * The Maude Simple GUI plug-in is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Maude Development Tools plugin, see the file license.txt.
 * If not, write to the Free Software Foundation, Inc., 51 Franklin Street, 
 * Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * Created on 02-feb-2005
 *
 */
package es.upv.dsic.issi.moment.maudedaemon.ui.console;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;
import org.eclipse.ui.internal.Workbench;

import es.upv.dsic.issi.moment.maudedaemon.core.MaudeDaemonPlugin;
import es.upv.dsic.issi.moment.maudedaemon.ui.preferences.PreferenceConstants;


/**
 * @author Abel G�mez. agomez@dsic.upv.es
 *
 */
public class ConsoleOutputView {
	private String ConsoleName;
	private MessageConsoleStream blackStream;
	private MessageConsoleStream redStream;
	private MessageConsoleStream blueStream;
	private MessageConsole console;

	public ConsoleOutputView(String consoleName) {
		super();

		ConsoleName = consoleName;
	
		Workbench.getInstance().getDisplay().syncExec(new Runnable() {
			public void run() {
				blackStream = findConsole(ConsoleName).newMessageStream();
				redStream = findConsole(ConsoleName).newMessageStream();
				redStream.setColor(Display.getDefault().getSystemColor(SWT.COLOR_RED));
				blueStream = findConsole(ConsoleName).newMessageStream();
				blueStream.setColor(Display.getDefault().getSystemColor(SWT.COLOR_BLUE));
				findConsole(ConsoleName).setTabWidth(4);
			}});
	}
	/**
	 * 
	 * @return an OutputStream which can be used to print to 
	 * 	the Eclipse console view
	 */
	public MessageConsoleStream getBlackStream() {
		return blackStream;
	}
	public MessageConsoleStream getRedStream() {
		return redStream;
	}
	
	public MessageConsoleStream getBlueStream() {
		return blueStream;
	}
	
	/**
	 * 
	 * Clears the console
	 * 
	 */
	public void clear() {
		//Not available in 3.0 
		findConsole(ConsoleName).clearConsole();
	}
	
	/**
	 * 
	 * Show the console
	 * 
	 */
	public void showConsole() {
		ConsolePlugin.getDefault().getConsoleManager().showConsoleView(findConsole(ConsoleName));
	}
	
	public void updateWaterMarks() {
		console.setWaterMarks(
			MaudeDaemonPlugin.getDefault().getPreferenceStore().getInt(PreferenceConstants.P_CONSOLESIZE) - 
			(MaudeDaemonPlugin.getDefault().getPreferenceStore().getInt(PreferenceConstants.P_CONSOLESIZE) / 10),
			MaudeDaemonPlugin.getDefault().getPreferenceStore().getInt(PreferenceConstants.P_CONSOLESIZE)
		);
	}
	
	private MessageConsole findConsole(String name) {
		IConsole[] consoles =  ConsolePlugin.getDefault().getConsoleManager().getConsoles();
		for (int i = 0; i < consoles.length; i++) {
			if (consoles[i].getName().equals(name)){
				return (MessageConsole) consoles[i]; 
			}
		}
		console = new MessageConsole(name, null);

		updateWaterMarks();
		
		ConsolePlugin.getDefault().getConsoleManager().addConsoles(new IConsole[]{console});
		return console;
	}
}
