/**
 * Copyright (C) The MOMENT project, 2004-2006.
 * http://moment.dsic.upv.es
 * 
 * This file is part of the Maude Simple GUI plug-in.
 * Contributed by Abel G�mez, <agomez@dsic.upv.es>.
 * 
 * The Maude Simple GUI plug-in is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * The Maude Simple GUI plug-in is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Maude Development Tools plugin, see the file license.txt.
 * If not, write to the Free Software Foundation, Inc., 51 Franklin Street, 
 * Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * Created on 19-dic-2004
 *
 */
package es.upv.dsic.issi.moment.maudedaemon.maude.internal;

import java.io.BufferedReader;
import java.io.BufferedWriter;

import es.upv.dsic.issi.moment.maudedaemon.maude.IMaudeProcess;
import es.upv.dsic.issi.moment.maudedaemon.maude.IMaudeProcessInteract;

/**
 * @author Abel G�mez. agomez@dsic.upv.es
 * 
 */
public class MaudeProcessInteract extends MaudeProcessBase implements IMaudeProcessInteract, IMaudeProcess {

	/**
	 * Constructor for MaudeProcessInteract
	 */

	public MaudeProcessInteract() {
		/* sets up command to exec */
		super();
	}

	/**
	 * Sends the String txt to Maude Process StdIn
	 * 
	 * @param 
	 * 		String txt
	 */
	public void sendToMaude(String txt){
		try {
			buffStdin.write(txt+"\n");
			buffStdin.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Reads one line from StdOut from Maude Process
	 * 
	 * @return
	 * 		StdErr (String)
	 */

	public String readOutLineMaude(){
		String txt = "";
		try {
			txt = buffStdout.readLine();
		} catch (Exception e) {
			e.printStackTrace();		}
		sendToLog(txt);
		return txt;
	}

	
	/**
	 * Reads one line from StdErr from Maude Process
	 * 
	 * @return
	 * 		StdErr (String)
	 */
	
	public String readErrLineMaude(){
		String txt = "";
		try {
			txt = buffStderr.readLine();
		} catch (Exception e) {
			e.printStackTrace();		}
		sendToLog(txt);
		return txt;
	}
	public BufferedWriter getIn() {
		return buffStdin;
	}
	
	public BufferedReader getErr() {
		return buffStderr;
	}
	public BufferedReader getOut() {
		return buffStdout;
	}
	
	/**
	 * Reads StdOut from Maude Process
	 * 	 
	 * @param 
	 * 		int maxLen, Maximum number of characters to be read
	 * @return
	 * 		StdOut (String)
	 */
	
	public String readOutMaudeNoBlock(int maxLen){
		String txt = "";
		char c = 0;
		try {
			while (buffStdout.ready() && txt.length() < maxLen) {
				c = (char)buffStdout.read();
				txt += c;
			}
		} catch (Exception e) {
		}
		sendToLog(txt);		
		return txt;
	}
	
	/**
	 * Reads StdErr from Maude Process
	 * 
	 * @param 
	 * 		int maxLen, Maximum number of characters to be read
	 * @return
	 * 		StdErr (String)
	 */
	
	public String readErrMaudeNoBlock(int maxLen){
		String txt = "";
		char c = 0;
		try {
			while (buffStderr.ready() && txt.length() < maxLen) {
				c = (char)buffStderr.read();
				txt += c;
			}
		} catch (Exception e) {
		// Nothing to notify, stream closed
		}
		sendToLog(txt);
		return txt;
	}
	
	/**
	 * Sends CTRL+C signal to Maude using the UNIX kill command
	 */
	
	public void makeCtrlC () {
		Process procTrace = null;

		try {

	        procTrace = runTime.exec("kill -2 " + pidMaude);	        

		} catch (Exception e) {
		}
	}
}
